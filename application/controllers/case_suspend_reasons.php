<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class case_suspend_reasons extends CI_Controller
{
	function __construct()
    {
        parent::__construct();
        $this -> load -> model("common");
		if($this->session->userdata('adminid')=='') {
			redirect('admin','refresh');
		}
    }
    
	function index() {
	  
	   self::view_case_suspend_reasons();
	   
	}
	
	function view_case_suspend_reasons($delete_error=0) {
	   $data['suspend_reasons'] = $this->common->getAllRow('case_suspend_reason','');
	   $data['delete_error'] = $delete_error;
	   $this->load->view('general_admin/view_case_suspend_reasons',$data);
	}
	
	function delete_case_suspend_reason($reason_code) {
		$case_exist_count = $this->common->getCountOfField("case_id","`case`","where suspend_reason_code = '".$reason_code."'");
		$trans_exist_count = $this->common->getCountOfField("transaction_id","case_transactions","where suspend_reason_code = '".$reason_code."'");
		if ($case_exist_count > 0 || $trans_exist_count > 0) {
			redirect('case_suspend_reasons/view_case_suspend_reasons/1');
		} else {
			$where = "suspend_code = '".$reason_code."'";
			$this->common->deleteRecord('case_suspend_reason',$where);
			
			redirect('case_suspend_reasons/view_case_suspend_reasons');
		}
				
	}
	
	function add_edit_case_suspend_reason($code=0) {
		$data['suspend_code']=$code;
	    $data['suspend_reason']="";
				   		
		if(extract($_POST)) {			
			$value['suspend_reason']= $_POST['suspend_reason']; 			
			
			if($code > 0) {
				$where = "suspend_code = '".$code."'";
				$this->common->updateRecord('case_suspend_reason',$value,$where);
			} else {
				$this->common->insertRecord('case_suspend_reason',$value);
			}
			redirect('case_suspend_reasons/view_case_suspend_reasons');
		} else {
			if($code > 0) {
				$where = "where suspend_code ='".$code."'";
				$result = $this->common->getOneRow('case_suspend_reason',$where);
				$data['suspend_code']=$result['suspend_code'];
		    	$data['suspend_reason']=$result['suspend_reason'];
			}
					
			$this->load->view('general_admin/add_edit_case_suspend_reason',$data);
		}		
	}
	
	
}

