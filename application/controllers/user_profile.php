<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class user_profile extends CI_Controller
{
	function __construct()
    {
        parent::__construct();
        $this -> load -> model("common");
		if($this->session->userdata('userid')=='') {
			redirect('user_login','refresh');
		}
    }
    
	function index() {
	  
	   self::view_profile();
	   
	}
	
	function view_profile() {
		if($this->session->userdata('userid')!='') {	
		   	$user_id = $this->session->userdata('userid');
		   	self::view_edit_general_profile($user_id);
		/*   	$userRecord = $this -> common -> getOneRecField_Limit("user_type_code", "user","where user_id ='$user_id'");
			$user_type_code = $userRecord->user_type_code;	
			if ($user_type_code == "ADMIN") {
				$this->load->view('office_admin/office_admin_profile',$data);
			} else {
				 self::view_general_profile($user_id);
			}	*/		
	   } else {
		   	$data['user_name']='';
		    $this->load->view('user_login',$data);
		 
	   }
	}
	
	function view_edit_general_profile($user_id) {
		if(extract($_POST)) {		
			$value['user_name']= $_POST['user_name']; 	
			$value['first_name']= $_POST['first_name']; 
			$value['last_name']= $_POST['last_name']; 			
			$value['mobile_number']= $_POST['mobile_number'];
			$email_count = 0;
			if ($_POST['email']=="") {
				$value['email']= NULL;
			} else {
				$value['email']= $_POST['email']; 
				$new_email = $_POST['email'];
				$email_count = $this->common->getCountOfField("user_id","user","where email ='".$new_email."' AND user_id !='".$user_id."'");
			}
			
			
			$new_user_name = $value['user_name'];
			$user_name_count = $this->common->getCountOfField("user_id","user","where user_name ='".$new_user_name."' AND user_id !='".$user_id."'");
			if ($email_count == 0 && $user_name_count == 0) {
				$where = "user_id =".$user_id;
				$this->common->updateRecord('user',$value,$where);
				
				//redirect('user_profile/view_edit_general_profile/'.$user_id);
				redirect('user_profile', 'refresh');
			} else {
				$email_exists = 0;
				if ($email_count != 0) {
					$email_exists=1;
				}
				$user_name_exists = 0;
				if ($user_name_count != 0) {
					$user_name_exists=1;
				}
				self::general_profile_with_error($user_id,$value['first_name'],$value['last_name'],$value['mobile_number'],
				$value['email'], $value['user_name'], $email_exists, $user_name_exists);
			}
			
			
		} else {
		
		 $this -> load -> model("notification_model");
	   	$notifications = $this -> notification_model -> getLatestDeliveredNotificationsForUser($user_id);
	   	$data['notifications_list'] = $notifications['notifications_list'];
		$data['count_unseen'] = $notifications['count_unseen'];
		$data['page'] = "user_profile/view_edit_general_profile/".$user_id;
		
		$where = "where user_id =".$user_id;
		$result = $this->common->getOneRow('user',$where);
		$data['user_id']=$result['user_id'];
		$data['password']=$result['password'];
		$data['user_name']=$result['user_name'];
	    $data['first_name']=$result['first_name'];
		$data['last_name']=$result['last_name'];
		
		$data['profile_first_name']=$result['first_name'];
		$data['profile_last_name']=$result['last_name'];
		
		$data['mobile_number']=$result['mobile_number'];
	    $data['email']=$result['email'];
	    $user_type_code = $result['user_type_code'];
	    $userTypeRecord = $this -> common -> getOneRecField_Limit("user_type_name", "user_type","where user_type_code ='$user_type_code'");
		$data['user_type_name'] = $userTypeRecord->user_type_name;	
		$data['profile_img']=$result['profile_img'];
		$data['user_type_code']=$user_type_code;
	    	
		switch ($user_type_code) {
		    case "ADMIN":
		    	self::get_office_admin_extra_data($data, $user_id);
		        $this->load->view('utils/profile',$data);
		        break;
		    case "CUSTOMER":
				self::get_customer_extra_data($data, $user_id);
		       	$this->load->view('utils/profile',$data);
		        break;
		    case "LAWYER":
				self::get_lawyer_extra_data($data, $user_id);
		        $this->load->view('utils/profile',$data);
		        break;
		    case "SECRETARY":
		        $this->load->view('utils/profile',$data); 
		        break;
		    default:
		        $this->load->view('utils/profile',$data);
		}
		}
	}
	
	function general_profile_with_error($user_id,$first_name,$last_name,$mobile_number,$email,$user_name,$email_exists, $user_name_exists) {
		
		$this -> load -> model("notification_model");
	   	$notifications = $this -> notification_model -> getLatestDeliveredNotificationsForUser($user_id);
	   	$data['notifications_list'] = $notifications['notifications_list'];
		$data['count_unseen'] = $notifications['count_unseen'];
		$data['page'] = "user_profile/view_edit_general_profile/".$user_id;
		
		$where = "where user_id =".$user_id;
		$result = $this->common->getOneRow('user',$where);
		$data['user_id']=$result['user_id'];
		$data['password']=$result['password'];
		$data['user_name']=$user_name;
	    $data['first_name']=$first_name;
		$data['last_name']=$last_name;
		
		$data['profile_first_name']=$result['first_name'];
		$data['profile_last_name']=$result['last_name'];
		
		$data['mobile_number']=$mobile_number;
	    $data['email']=$email;
	    $user_type_code = $result['user_type_code'];
	    $userTypeRecord = $this -> common -> getOneRecField_Limit("user_type_name", "user_type","where user_type_code ='$user_type_code'");
		$data['user_type_name'] = $userTypeRecord->user_type_name;	
		$data['profile_img']=$result['profile_img'];
		$data['user_type_code']=$user_type_code;
		
		$data['profile_email_exists']= $email_exists;
		$data['profile_username_exists']= $user_name_exists;
	    	
		switch ($user_type_code) {
		    case "ADMIN":
		    	self::get_office_admin_extra_data($data, $user_id);
		        $this->load->view('utils/profile',$data);
		        break;
		    case "CUSTOMER":
				self::get_customer_extra_data($data, $user_id);
		       	$this->load->view('utils/profile',$data);
		        break;
		    case "LAWYER":
				self::get_lawyer_extra_data($data, $user_id);
		        $this->load->view('utils/profile',$data);
		        break;
		    case "SECRETARY":
		        $this->load->view('utils/profile',$data); 
		        break;
		    default:
		        $this->load->view('utils/profile',$data);
		}
	}
	
	function get_office_admin_extra_data(&$data, $user_id) {
		$userRecord = $this -> common -> getOneRecField_Limit("lawyer_office_id", "user","where user_id ='$user_id'");
		$lawyer_office_id = $userRecord->lawyer_office_id;
		$where = "where lawyer_office_id =".$lawyer_office_id;
		$result = $this->common->getOneRow('lawyer_office',$where);
		$data['logo']=$result['logo'];
		$data['lawyers_count'] = $this->common->getCountOfField("user_id","user","where user_type_code ='LAWYER' AND lawyer_office_id = '".$lawyer_office_id."'"); 
		$data['customers_count'] = $this->common->getCountOfField("user_id","user","where user_type_code ='CUSTOMER' AND lawyer_office_id = '".$lawyer_office_id."'"); 
		$data['secretaries_count'] = $this->common->getCountOfField("user_id","user","where user_type_code ='SECRETARY' AND lawyer_office_id = '".$lawyer_office_id."'"); 
	
		//get cases count
		$this->load->model ( "case_model" );
		$cases_count_res = $this->case_model->get_cases_count ( $user_id );
		$data['cases_count']= $cases_count_res['counts'] [0] ['ALL'];
		
		$data['package_code']=$result['package_code'];
		$data['name']=$result['name'];
		$data['address']=$result['address'];
		$data['city']=$result['city'];
		$data['office_phone_number']=$result['phone_number'];
		$data['office_mobile_number']=$result['mobile_number'];
		$data['office_email']=$result['email'];
		$data['contact_person_name']=$result['contact_person_name'];
		$data['position']=$result['position'];
		$this->load->library('../controllers/dates_conversion');
		$data['subscription_start_date'] = $result['subscription_start_date'];
		if ($result['subscription_start_date'] != NULL && $result['subscription_start_date'] != "") {
			$data['subscription_start_date']=$this->dates_conversion->GregorianToHijri($result['subscription_start_date']);
		}
		$data['subscription_end_date'] = $result['subscription_end_date'];
		if ($result['subscription_end_date'] != NULL && $result['subscription_end_date'] != "") {
			$data['subscription_end_date']=$this->dates_conversion->GregorianToHijri($result['subscription_end_date']);
		}
		
		$data['logo']=$result['logo'];
		$data['banner']=$result['banner'];
		
        $data['available_packages']= $this -> common -> getAllRow("package", "");
		$data['available_cities']= $this -> common -> getAllRow("city", "");
	
	}
	
	function get_lawyer_extra_data(&$data, $user_id) {
		$userRecord = $this -> common -> getOneRecField_Limit("lawyer_office_id", "user","where user_id ='$user_id'");
		$lawyer_office_id = $userRecord->lawyer_office_id;

		$lawyerRecord = $this -> common -> getOneRow("lawyer_details", "where lawyer_id ='$user_id'");
		
		$data['manager']=$lawyerRecord['manager'];
		$data['stage1_cost']=$lawyerRecord['stage1_cost'];
		$data['stage2_cost']=$lawyerRecord['stage2_cost'];
		$data['stage3_cost']=$lawyerRecord['stage3_cost'];
		$data['stage4_cost']=$lawyerRecord['stage4_cost'];
		$data['stage5_cost']=$lawyerRecord['stage5_cost'];

		$this -> load -> model("lawyer_office_model");
		$data['available_managers']= $this -> lawyer_office_model -> getAvailableManagers($user_id, $lawyer_office_id);
		
	}

	function get_customer_extra_data(&$data, $user_id) {
		$customerRecord = $this -> common -> getOneRow("customer_details", "where customer_id ='$user_id'");
		
		$data['customer_type']=$customerRecord['customer_type'];
		$data['city']=$customerRecord['city'];
		$data['address']=$customerRecord['address'];
		$data['phone_number']=$customerRecord['phone_number'];
		$data['contact_person_name']=$customerRecord['contact_person_name'];
		$data['position']=$customerRecord['position'];
		$this->load->library('../controllers/dates_conversion');
		$data['subscription_start_date'] = $customerRecord['subscription_start_date'];
		if ($customerRecord['subscription_start_date'] != NULL && $customerRecord['subscription_start_date'] != "") {
			$data['subscription_start_date']=$this->dates_conversion->GregorianToHijri($customerRecord['subscription_start_date']);
		}
		$data['subscription_end_date']=$customerRecord['subscription_end_date'];
		if ($customerRecord['subscription_end_date'] != NULL && $customerRecord['subscription_end_date'] != "") {
			$data['subscription_end_date']=$this->dates_conversion->GregorianToHijri($customerRecord['subscription_end_date']);
		}		

		$data['number_of_cases_per_month']=$customerRecord['number_of_cases_per_month'];
		$data['show_invoice_module']=$customerRecord['show_invoice_module'];
		$data['show_dashboard']=$customerRecord['show_dashboard'];

		$data['logo']=$customerRecord['logo'];
		$data['banner']=$customerRecord['banner'];

		$data['available_cities']= $this -> common -> getAllRow("city", "");
		$data['available_customer_types']= $this -> common -> getAllRow("customer_type", "");
	}


	function edit_customer_info($user_id) {
		if(extract($_POST)) {			
			$value['customer_type']= $_POST['customer_type']; 
			$value['city']= $_POST['city']; 
			//$value['address']= $_POST['address']; //TODO this need to be revisited I just comment it to avoid an error that have no time to fix			
			$value['phone_number']= $_POST['phone_number'];
			$value['contact_person_name']= $_POST['contact_person_name']; 
			$value['position']= $_POST['position']; 
			$value['subscription_start_date']= $_POST['subscription_start_date']; 
			$value['subscription_end_date']= $_POST['subscription_end_date']; 	
			$this->load->library('../controllers/dates_conversion');
			if ($_POST['subscription_start_date'] != NULL && $_POST['subscription_start_date'] != "") {
				$value['subscription_start_date']= $this->dates_conversion->HijriToGregorian($_POST['subscription_start_date']); 
			}
			if ($_POST['subscription_end_date'] != NULL && $_POST['subscription_end_date'] != "") {
				$value['subscription_end_date']= $this->dates_conversion->HijriToGregorian($_POST['subscription_end_date']); 
			}
			
			$value['number_of_cases_per_month']= $_POST['number_of_cases_per_month'];
			$value['show_invoice_module']= $_POST['show_invoice_module']; 
			$value['show_dashboard']= $_POST['show_dashboard']; 
			
			if(isset($_FILES['logo']['name']) && $_FILES['logo']['name']!="") { 
				if(!is_dir("./images/customers_logo/"))	{
					 mkdir('./images/customers_logo/');
				}								
				$image = $_FILES['logo']['name'];
				$uploadedfile = $_FILES['logo']['tmp_name'];					
				$ext = strstr($_FILES['logo']['name'],".");									
	          			
				$source = $_FILES['logo']['tmp_name'];
				$image= $user_id."-".str_replace(' ','-',$image); 					
	 		    $imagepath = $image;
	          	$save = "./images/customers_logo/".$imagepath;           			
	 		    move_uploaded_file($uploadedfile,$save);
	 		           	
	 		    $value['logo']= "images/customers_logo/".$image;
			}
			
			if(isset($_FILES['banner']['name']) && $_FILES['banner']['name']!="") { 
				if(!is_dir("./images/customers_banner/"))	{
					 mkdir('./images/customers_banner/');
				}								
				$image = $_FILES['banner']['name'];
				$uploadedfile = $_FILES['banner']['tmp_name'];					
				$ext = strstr($_FILES['banner']['name'],".");									
	          			
				$source = $_FILES['banner']['tmp_name'];
				$image= $user_id."-".str_replace(' ','-',$image); 					
	 		    $imagepath = $image;
	          	$save = "./images/customers_banner/".$imagepath;           			
	 		    move_uploaded_file($uploadedfile,$save);
	 		           	
	 		    $value['banner']= "images/customers_banner/".$image;
			}
						
			$where = "customer_id =".$user_id;
			$this->common->updateRecord('customer_details',$value,$where);
			
			redirect('user_profile', 'refresh');
			//redirect('user_profile/edit_customer_info/'.$user_id);
			
			//redirect('user_profile/view_edit_general_profile/'.$user_id);
		}
		redirect('user_profile', 'refresh');
		//redirect('user_profile/view_edit_general_profile/'.$user_id);
		
	/*	$where = "where user_id =".$user_id;
		$result = $this->common->getOneRow('user',$where);
		$data['user_id']=$result['user_id'];
		$user_type_code = $result['user_type_code'];
	    $userTypeRecord = $this -> common -> getOneRecField_Limit("user_type_name", "user_type","where user_type_code ='$user_type_code'");
		$data['user_type_name'] = $userTypeRecord->user_type_name;	
		$data['profile_img']=$result['profile_img'];
		
		$where = "where customer_id =".$user_id;
		$result = $this->common->getOneRow('customer_details',$where);
		$data['customer_type']= $result['customer_type']; 
		$data['city']= $result['city']; 
		$data['address']= $result['address']; 			
		$data['phone_number']= $result['phone_number'];
		$data['contact_person_name']= $result['contact_person_name']; 
		$data['position']= $result['position']; 
		$data['subscription_start_date']= $result['subscription_start_date']; 
		$data['subscription_end_date']= $result['subscription_end_date']; 			
		$data['number_of_cases_per_month']= $result['number_of_cases_per_month'];
		$data['logo']= $result['logo']; 
		$data['banner']= $result['banner'];
		$data['show_invoice_module']= $result['show_invoice_module']; 
		$data['show_dashboard']= $result['show_dashboard']; 
		
		$data['available_cities']= $this -> common -> getAllRow("city", "");
		$data['available_customer_types']= $this -> common -> getAllRow("customer_type", "");
		
		$this->load->view('customer/edit_customer_info',$data);*/
	   
	}
	
	function edit_lawyer_info($user_id) {
		if(extract($_POST)) {			
			$value['manager']= $_POST['manager']; 
			$value['stage1_cost']= $_POST['stage1_cost']; 			
			$value['stage2_cost']= $_POST['stage2_cost'];
			$value['stage3_cost']= $_POST['stage3_cost']; 
			$value['stage4_cost']= $_POST['stage4_cost']; 			
			$value['stage5_cost']= $_POST['stage5_cost'];
			
			$where = "lawyer_id =".$user_id;
			$this->common->updateRecord('lawyer_details',$value,$where);
			
			redirect('user_profile', 'refresh');
			//redirect('user_profile/edit_lawyer_info/'.$user_id);
			//redirect('user_profile/view_edit_general_profile/'.$user_id);
		}
		redirect('user_profile', 'refresh');
		//redirect('user_profile/view_edit_general_profile/'.$user_id);
		
	/*	$where = "where user_id =".$user_id;
		$result = $this->common->getOneRow('user',$where);
		$data['user_id']=$result['user_id'];
	    $user_type_code = $result['user_type_code'];
	    $userTypeRecord = $this -> common -> getOneRecField_Limit("user_type_name", "user_type","where user_type_code ='$user_type_code'");
		$data['user_type_name'] = $userTypeRecord->user_type_name;	
		$data['profile_img']=$result['profile_img'];
		
		$where = "where lawyer_id =".$user_id;
		$details_result = $this->common->getOneRow('lawyer_details',$where);
		$data['manager']=$details_result['manager'];
    	$data['stage1_cost']=$details_result['stage1_cost'];
		$data['stage2_cost']=$details_result['stage2_cost'];
		$data['stage3_cost']=$details_result['stage3_cost'];
    	$data['stage4_cost']=$details_result['stage4_cost'];
		$data['stage5_cost']=$details_result['stage5_cost'];
		
		$this -> load -> model("lawyer_office_model");
		$data['available_managers']= $this -> lawyer_office_model -> getAvailableManagers($user_id, $lawyer_office_id);
				
		$this->load->view('lawyer/edit_lawyer_info',$data);*/
	}
	
	function edit_office_info($user_id) {
	   $userRecord = $this -> common -> getOneRecField_Limit("lawyer_office_id", "user","where user_id ='$user_id'");
	   $lawyer_office_id = $userRecord->lawyer_office_id;
		if(extract($_POST)) {			
		//	$value['package_code']= $_POST['package_code']; 
			$value['name']= $_POST['office_name']; 			
			$value['address']= $_POST['address'];
			$value['city']= $_POST['city']; 
			$value['phone_number']= $_POST['office_phone_number']; 			
			$value['mobile_number']= $_POST['office_mobile_number'];
			$value['email']= $_POST['office_email']; 
			$value['contact_person_name']= $_POST['contact_person_name']; 			
			$value['position']= $_POST['position'];
		/*	$value['subscription_start_date']= $_POST['subscription_start_date']; 
			$value['subscription_end_date']= $_POST['subscription_end_date']; 
			
			$this->load->library('../controllers/dates_conversion');
			if ($_POST['subscription_start_date'] != NULL && $_POST['subscription_start_date'] != "") {
				$value['subscription_start_date']= $this->dates_conversion->HijriToGregorian($_POST['subscription_start_date']); 
			}
			if ($_POST['subscription_end_date'] != NULL && $_POST['subscription_end_date'] != "") {
				$value['subscription_end_date']= $this->dates_conversion->HijriToGregorian($_POST['subscription_end_date']); 
			}*/
				
			if(isset($_FILES['logo']['name']) && $_FILES['logo']['name']!="") { 
				if(!is_dir("./images/offices_logo/"))	{
					 mkdir('./images/offices_logo/');
				}								
				$image = $_FILES['logo']['name'];
				$uploadedfile = $_FILES['logo']['tmp_name'];					
				$ext = strstr($_FILES['logo']['name'],".");									
	          			
				$source = $_FILES['logo']['tmp_name'];
				$image= $user_id."-".str_replace(' ','-',$image); 					
	 		    $imagepath = $image;
	          	$save = "./images/offices_logo/".$imagepath;           			
	 		    move_uploaded_file($uploadedfile,$save);
	 		           	
	 		    $value['logo']= "images/offices_logo/".$image;
			}
			
			if(isset($_FILES['banner']['name']) && $_FILES['banner']['name']!="") { 
				if(!is_dir("./images/offices_banner/"))	{
					 mkdir('./images/offices_banner/');
				}								
				$image = $_FILES['banner']['name'];
				$uploadedfile = $_FILES['banner']['tmp_name'];					
				$ext = strstr($_FILES['banner']['name'],".");									
	          			
				$source = $_FILES['banner']['tmp_name'];
				$image= $user_id."-".str_replace(' ','-',$image); 					
	 		    $imagepath = $image;
	          	$save = "./images/offices_banner/".$imagepath;           			
	 		    move_uploaded_file($uploadedfile,$save);
	 		           	
	 		    $value['banner']= "images/offices_banner/".$image;
			}	
			
			$where = "lawyer_office_id =".$lawyer_office_id;
			$this->common->updateRecord('lawyer_office',$value,$where);
			
			redirect('user_profile', 'refresh');
			//redirect('user_profile/edit_office_info/'.$user_id);
			//redirect('user_profile/view_edit_general_profile/'.$user_id);
		}
		redirect('user_profile', 'refresh');
		//redirect('user_profile/view_edit_general_profile/'.$user_id);
		
	/*	$where = "where user_id =".$user_id;
		$result = $this->common->getOneRow('user',$where);
		$data['user_id']=$result['user_id'];
	    $user_type_code = $result['user_type_code'];
	    $userTypeRecord = $this -> common -> getOneRecField_Limit("user_type_name", "user_type","where user_type_code ='$user_type_code'");
		$data['user_type_name'] = $userTypeRecord->user_type_name;	
		$data['profile_img']=$result['profile_img'];
		
		$where = "where lawyer_office_id =".$lawyer_office_id;
		$result = $this->common->getOneRow('lawyer_office',$where);
		$data['package_code']=$result['package_code'];
    	$data['name']=$result['name'];
		$data['address']=$result['address'];
		$data['city']=$result['city'];
    	$data['phone_number']=$result['phone_number'];
		$data['mobile_number']=$result['mobile_number'];
		$data['email']=$result['email'];
    	$data['contact_person_name']=$result['contact_person_name'];
		$data['position']=$result['position'];
		$data['subscription_start_date']=$result['subscription_start_date'];
    	$data['subscription_end_date']=$result['subscription_end_date'];
    	$data['logo']=$result['logo'];
		$data['banner']=$result['banner'];
	    	
		self::get_office_admin_extra_data($data, $user_id);
		
		$data['available_packages']= $this -> common -> getAllRow("package", "");
		$data['available_cities']= $this -> common -> getAllRow("city", "");
				
		$this->load->view('office_admin/edit_office_info',$data);*/
	}
	
	function change_password($user_id) {
		if(extract($_POST)) {			
			$value['password']= $_POST['new_password']; 
			
			$where = "user_id =".$user_id;
			$this->common->updateRecord('user',$value,$where);
			
			redirect('user_profile', 'refresh');
			//redirect('user_profile/view_edit_general_profile/'.$user_id);
		}
		redirect('user_profile', 'refresh');
		//redirect('user_profile/view_edit_general_profile/'.$user_id);
		
		/*$where = "where user_id =".$user_id;
		$result = $this->common->getOneRow('user',$where);
		$data['user_id']=$result['user_id'];
		$data['user_name']=$result['user_name'];
	    $data['old_password']=$result['password'];
		$user_type_code = $result['user_type_code'];
	    $userTypeRecord = $this -> common -> getOneRecField_Limit("user_type_name", "user_type","where user_type_code ='$user_type_code'");
		$data['user_type_name'] = $userTypeRecord->user_type_name;	
		$data['profile_img']=$result['profile_img'];
	    	
		switch ($user_type_code) {
		    case "ADMIN":
		    	self::get_office_admin_extra_data($data, $user_id);
		        $this->load->view('office_admin/change_password',$data);
		        break;
		    case "CUSTOMER":
		       	$this->load->view('customer/change_password',$data);
		        break;
		    case "LAWYER":
		        $this->load->view('lawyer/change_password',$data);
		        break;
		    case "SECRETARY":
		        $this->load->view('secretary/change_password',$data); 
		        break;
		    default:
		        $this->load->view('secretary/change_password',$data);
		}
	   */
	}
	
	function change_avatar($user_id) {
			   
		if(isset($_FILES['avatar']['name']) && $_FILES['avatar']['name']!="") { 
			if(!is_dir("./images/users/"))	{
				 mkdir('./images/users/');
			}								
			$image = $_FILES['avatar']['name'];
			$uploadedfile = $_FILES['avatar']['tmp_name'];					
			$ext = strstr($_FILES['avatar']['name'],".");									
          			
			$source = $_FILES['avatar']['tmp_name'];
			$image= $user_id."-".str_replace(' ','-',$image); 					
 		    $imagepath = $image;
          	$save = "./images/users/".$imagepath;           			
 		    move_uploaded_file($uploadedfile,$save);
 		           	
 		    $img_data['profile_img']= "images/users/".$image;
		    $where="user_id = '$user_id'";
			$this->common->updateRecord('user',$img_data,$where);
			redirect('user_profile', 'refresh');
			//redirect('user_profile/view_edit_general_profile/'.$user_id, 'refresh');									
		}
		redirect('user_profile', 'refresh');	
		/*$where = "where user_id =".$user_id;
		$result = $this->common->getOneRow('user',$where);
		$data['user_id']=$result['user_id'];
		$data['user_name']=$result['user_name'];
		$user_type_code = $result['user_type_code'];
	    $userTypeRecord = $this -> common -> getOneRecField_Limit("user_type_name", "user_type","where user_type_code ='$user_type_code'");
		$data['user_type_name'] = $userTypeRecord->user_type_name;	
		$data['profile_img']=$result['profile_img'];
	    	
		switch ($user_type_code) {
		    case "ADMIN":
		    	self::get_office_admin_extra_data($data, $user_id);
		        $this->load->view('office_admin/change_avatar',$data);
		        break;
		    case "CUSTOMER":
		       	$this->load->view('customer/change_avatar',$data);
		        break;
		    case "LAWYER":
		        $this->load->view('lawyer/change_avatar',$data);
		        break;
		    case "SECRETARY":
		        $this->load->view('secretary/change_avatar',$data); 
		        break;
		    default:
		        $this->load->view('secretary/change_avatar',$data);
		}*/
	   
	}
	
	function change_banner($user_id) {			
		$where = "where user_id =".$user_id;
		$result = $this->common->getOneRow('user',$where);
		$user_type_code = $result['user_type_code'];
		$lawyer_office_id = $result['lawyer_office_id'];
	    if ($user_type_code == "ADMIN") {
	    	if(isset($_FILES['banner']['name']) && $_FILES['banner']['name']!="") { 
				if(!is_dir("./images/offices_banner/"))	{
					 mkdir('./images/offices_banner/');
				}								
				$image = $_FILES['banner']['name'];
				$uploadedfile = $_FILES['banner']['tmp_name'];					
				$ext = strstr($_FILES['banner']['name'],".");									
	          			
				$source = $_FILES['banner']['tmp_name'];
				$image= $user_id."-".str_replace(' ','-',$image); 					
	 		    $imagepath = $image;
	          	$save = "./images/offices_banner/".$imagepath;           			
	 		    move_uploaded_file($uploadedfile,$save);
	 		           	
	 		    $value['banner']= "images/offices_banner/".$image;
			}	
			
			$where = "lawyer_office_id =".$lawyer_office_id;
			$this->common->updateRecord('lawyer_office',$value,$where);
	    	
	    } else if ($user_type_code == "CUSTOMER") {
	    	if(isset($_FILES['banner']['name']) && $_FILES['banner']['name']!="") { 
				if(!is_dir("./images/customers_banner/"))	{
					 mkdir('./images/customers_banner/');
				}								
				$image = $_FILES['banner']['name'];
				$uploadedfile = $_FILES['banner']['tmp_name'];					
				$ext = strstr($_FILES['banner']['name'],".");									
	          			
				$source = $_FILES['banner']['tmp_name'];
				$image= $user_id."-".str_replace(' ','-',$image); 					
	 		    $imagepath = $image;
	          	$save = "./images/customers_banner/".$imagepath;           			
	 		    move_uploaded_file($uploadedfile,$save);
	 		           	
	 		    $value['banner']= "images/customers_banner/".$image;
			}
			$where = "customer_id =".$user_id;
			$this->common->updateRecord('customer_details',$value,$where);
	    }	
			
		redirect('user_profile', 'refresh');	
		
	}
	
	
}

