<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

require APPPATH . '/libraries/REST_Controller.php';

class cron_backup extends REST_Controller {
	function extract_backup_get() {
		// Load the DB utility class
		$this->load->dbutil();

		// Backup your entire database and assign it to a variable
		$prefs = array(
        'tables'        => array('FAQ_Category'
,'FAQs_help'
,'action'
,'admin'
,'customer_type'
,'package'
,'lawyer_office'
,'user_type'
,'user'
,'case_close_reason'
,'case_requests'
,'case_suspend_reason'
,'error_log'
,'city'
,'region'
,'cost_per_stage_per_region'
,'customer_details'
,'state'
,'notifications'
,'case'
,'delivered_notifications'
,'comment'
,'invoices'
,'lawyer_case'
,'lawyer_details'
,'region_cities'
,'stages'
,'state_transition'
,'temp_view'
,'usertype_privileges'
,'user_activities'
,'case_transactions'),  // Array of tables to backup.
'add_drop'      => FALSE,
'format'        => 'zip',                       // gzip, zip, txt
'filename'      => 'backup.sql', 
                          
		);

		$backup =& $this->dbutil->backup($prefs); 

		// Load the file helper and write the file to your server
		$this->load->helper('file');
		if (! is_dir ( FCPATH .'DB_backups' )) {
							mkdir ( FCPATH .'DB_backups', 0777, true );
		}
		date_default_timezone_set ( 'Asia/Riyadh' );
		$current_date_time = date ( "Y-m-d" );
		write_file(FCPATH .'DB_backups/backup_'.$current_date_time.'.zip', $backup); 


	}
	


}

?>
