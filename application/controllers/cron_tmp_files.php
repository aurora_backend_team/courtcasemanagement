<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

require APPPATH . '/libraries/REST_Controller.php';
class cron_tmp_files extends REST_Controller {
	function delete_tmp_files_get() {
		self::rrmdir ( FCPATH . "/cases_tmp_files/" );
	}
	function rrmdir($dir) {
		if (is_dir ( $dir )) {
			$objects = scandir ( $dir );
			foreach ( $objects as $object ) {
				if ($object != "." && $object != "..") {
					if (filetype ( $dir . "/" . $object ) == "dir")
						self::rrmdir ( $dir . "/" . $object );
					else {
						$file_to_del = $dir . "/" . $object;
						date_default_timezone_set ( 'Asia/Riyadh' );
						$date = date ( "F d Y H:i:s.", filectime ( $file_to_del ) );
						if (strtotime ( '+1 day', strtotime ( $date ) ) < strtotime ( 'now' ))
							unlink ( $file_to_del );
					}
				}
			}
			reset ( $objects );
			if (count ( glob ( "$dir/*" ) ) == 0)
				rmdir ( $dir );
		}
	}
}

?>
