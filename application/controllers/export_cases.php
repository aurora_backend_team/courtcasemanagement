<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class export_cases extends CI_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->model ( "common" );
		if ($this->session->userdata ( 'userid' ) == '' || ($this->session->userdata ( 'user_type' ) != 'ADMIN' && $this->session->userdata ( 'user_type' ) != 'CUSTOMER')) {
			// echo 'user data is null';
			redirect ( 'user_login', 'refresh' );
		}
	}
	function index() {
		self::excel_export ();
	}
	function excel_export($dataInsertion = 0) {
		/**
		 * PHPExcel
		 */
		include 'Classes/PHPExcel.php';
		
		/**
		 * PHPExcel_Writer_Excel2007
		 */
		include 'Classes/PHPExcel/Writer/Excel2007.php';
		
		$user_id = $this->session->userdata ( 'userid' );
		$where = "where user_id =" . $user_id;
		$userRecord = $this->common->getOneRow ( 'user', $where );
		$lawyer_office_id = $userRecord ['lawyer_office_id'];
		$this->load->model ( "case_model" );
		if ($dataInsertion == 0) {
			$data = $this->case_model->getExportedCasesData ( $lawyer_office_id, $userRecord ['user_type_code'], $user_id );
		} else {
			$data = $this->case_model->getExportedCasesDatabaseInsert ( $lawyer_office_id, $userRecord ['user_type_code'], $user_id );
		}
		
		$objPHPExcel = new PHPExcel ();
		$objPHPExcel->setActiveSheetIndex ( 0 );
		$objPHPExcel->getActiveSheet ()->setRightToLeft ( true );
		
		if ($data) {
			self::array_dual_sort_by_column ( $data, "عدد الأقساط المتأخرة", "تاريخ إنشاء القضية", SORT_DESC, SORT_ASC );
			$i = 0;
			$this->load->library ( '../controllers/dates_conversion' );
			if ($dataInsertion == 0) {
				foreach ( $data as $record ) {
					$dates_conversion = new dates_conversion ();
					$data [$i] ['تاريخ إنشاء القضية'] = $this->dates_conversion->GregorianToHijri ( $record ['تاريخ إنشاء القضية'] );
					$data [$i] ['تاريخ قرار 34'] = $this->dates_conversion->GregorianToHijri ( $record ['تاريخ قرار 34'] );
					$data [$i] ['تاريخ قرار 46'] = $this->dates_conversion->GregorianToHijri ( $record ['تاريخ قرار 46'] );
					$data [$i] ['تاريخ القيد'] = $this->dates_conversion->GregorianToHijri ( $record ['تاريخ القيد'] );
					$data [$i] ['تاريخ الإعلان'] = $this->dates_conversion->GregorianToHijri ( $record ['تاريخ الإعلان'] );
					$data [$i] ['تاريخ آخر تعديل بالقضية'] = $this->dates_conversion->GregorianToHijri ( $record ['تاريخ آخر تعديل بالقضية'] );
					$data [$i] ['تاريخ الامهال'] = $this->dates_conversion->GregorianToHijri ( $record ['تاريخ الامهال'] );
					$i ++;
				}
			}
		}
		
		// file name for download
		if ($dataInsertion == 0) {
			$fileName = "بيانات القضايا_" . date ( 'Ymd' ) . ".xlsx";
		} else {
			$fileName = "بيانات القضايا فى صورة قاعدة بيانات_" . date ( 'Ymd' ) . ".xlsx";
		}
		
		// headers for download
		header ( 'Content-Encoding: UTF-8' );
		header ( "Content-type: application/ms-excel" );
		header ( "Content-Type: application/vnd.ms-excel" );
		header ( "Content-Disposition: attachment; filename=\"$fileName\"" );
		header ( 'Content-Transfer-Encoding: binary' );
		header("Content-Length:". $filesize);
		
		
		$headerStyleArray = array (
				'font' => array (
						'size' => 15 
				)
				,
				'fill' => array (
						'type' => PHPExcel_Style_Fill::FILL_SOLID,
						'color' => array (
								'rgb' => '79a6d2' 
						) 
				),
				'alignment' => array (
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER 
				) 
		);
		
		$dataStyleArray = array (
				'font' => array (
						'size' => 13 
				),
				'alignment' => array (
						'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER 
				) 
		);
		$row = 1;
		$flag = false;
		$type = PHPExcel_Cell_DataType::TYPE_STRING;
		foreach ( $data as $record ) {
			if (! $flag) {
				$col = 0;
				foreach ( array_keys ( $record ) as $row_key ) {
					
					$objPHPExcel->getActiveSheet ()->setCellValueByColumnAndRow ( $col, $row, $row_key );
					$objPHPExcel->getActiveSheet ()->getStyleByColumnAndRow ( $col, $row )->applyFromArray ( $headerStyleArray );
					$objPHPExcel->getActiveSheet ()->getColumnDimensionByColumn ( $col, $row )->setAutoSize ( true );
					$col = $col + 1;
				}
				$flag = true;
				$row = $row + 1;
			}
			$col = 0;
			foreach ( array_values ( $record ) as $row_data ) {
				if (is_numeric ( $row_data ) && $numlength = strlen((string)$row_data) > 6) {
					$objPHPExcel->getActiveSheet ()->getCellByColumnAndRow ( $col, $row )->setValueExplicit ( $row_data, $type );
				} else {
					$objPHPExcel->getActiveSheet ()->setCellValueByColumnAndRow ( $col, $row, $row_data );
				}
				$objPHPExcel->getActiveSheet ()->getStyleByColumnAndRow ( $col, $row )->applyFromArray ( $dataStyleArray );
				$objPHPExcel->getActiveSheet ()->getColumnDimensionByColumn ( $col, $row )->setAutoSize ( true );
				$col = $col + 1;
			}
			$row = $row + 1;
		}
		// Instantiate a Writer to create an OfficeOpenXML Excel .xlsx file
		$objWriter = new PHPExcel_Writer_Excel2007 ( $objPHPExcel );
		// Write the Excel file to filename some_excel_file.xlsx in the current directory
		$objWriter->save ( 'php://output' );
	}
	function array_dual_sort_by_column(&$array, $column1, $column2, $dir1 = SORT_ASC, $dir2 = SORT_ASC) {
		$sort_column = array ();
		foreach ( $array as $key => $row ) {
			$sort_column [$column1] [$key] = $row [$column1];
			$sort_column [$column2] [$key] = $row [$column2];
		}
		array_multisort ( $sort_column [$column1], $dir1, $sort_column [$column2], $dir2, $array );
	}
}
