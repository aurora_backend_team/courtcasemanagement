<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class lawyer_office_customer_types extends CI_Controller
{
	function __construct()
    {
        parent::__construct();
        $this -> load -> model("common");
		if($this->session->userdata('userid')=='' || $this->session->userdata('user_type')!='ADMIN') {
			redirect('user_login','refresh');
		}
    }
    
	function index() {
	  
	   self::view_customer_types();
	   
	}

	function view_customer_types($delete_error=0) {
	   $office_admin_id = $this->session->userdata('userid');
	   $data['customer_types'] = $this->common->getAllRow('customer_type','');
	   $data['delete_error'] = $delete_error;
	   $where = "where user_id =".$office_admin_id  ;
	   $result = $this->common->getOneRow('user',$where);
	   $data['profile_first_name']=$result['first_name'];
	   $data['profile_last_name']=$result['last_name'];
	   $data['profile_img']=$result['profile_img'];
	   
	    $this -> load -> model("notification_model");
	   	$notifications = $this -> notification_model -> getLatestDeliveredNotificationsForUser($office_admin_id);
	   	$data['notifications_list'] = $notifications['notifications_list'];
		$data['count_unseen'] = $notifications['count_unseen'];
		$data['page'] = "lawyer_office_customer_types/view_customer_types/".$delete_error;
		
	   $this->load->view('office_admin/view_customer_types',$data);
	}
	
	function delete_customer_type($customer_type_id) {
		$exist_count = $this->common->getCountOfField("customer_details_id","customer_details","where customer_type ='".$customer_type_id."'");
		if ($exist_count > 0) {
			redirect('lawyer_office_customer_types/1');
		} else {
			$where = "customer_type_id =".$customer_type_id;
			$this->common->deleteRecord('customer_type',$where);
			
			redirect('lawyer_office_customer_types');
		}
		
	}
	
	function add_edit_customer_type($customer_type_id=0) {
		$office_admin_id = $this->session->userdata('userid');
		$data['customer_type_id']=$customer_type_id;
        $data['name']="";
	    $data['description']="";
	    
           $where = "where user_id =".$office_admin_id  ;
	   $result = $this->common->getOneRow('user',$where);
	   $data['profile_first_name']=$result['first_name'];
	   $data['profile_last_name']=$result['last_name'];
	   $data['profile_img']=$result['profile_img'];
	   
	   $this -> load -> model("notification_model");
	   	$notifications = $this -> notification_model -> getLatestDeliveredNotificationsForUser($office_admin_id);
	   	$data['notifications_list'] = $notifications['notifications_list'];
		$data['count_unseen'] = $notifications['count_unseen'];
		$data['page'] = "lawyer_office_customer_types/add_edit_customer_type/".$customer_type_id;
	   
	    if(extract($_POST)) {			
			$value['name']= $_POST['name']; 
			$value['description']= $_POST['description']; 			

			if($customer_type_id > 0) {
				$where = "customer_type_id =".$customer_type_id;
				$this->common->updateRecord('customer_type',$value,$where);
			} else {
				$this->common->insertRecord('customer_type',$value);
			}
			redirect('lawyer_office_customer_types');
		}
		
		if($customer_type_id > 0) {
			$where = "where customer_type_id =".$customer_type_id;
			$result = $this->common->getOneRow('customer_type',$where);
			$data['name']=$result['name'];
	    	$data['description']=$result['description'];
	    	
		}
				
		$this->load->view('office_admin/add_edit_customer_type',$data);
	}
	
}
?>