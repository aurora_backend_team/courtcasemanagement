<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class dashboard_reports extends CI_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->model ( "common" );
	}
	function index() {
		self::view_dashboard_reports (0);
	}
	function view_dashboard_reports($mode = 0) {
	    $this->load->library ( '../controllers/dates_conversion' );
		$isUser = 0;
		$isGeneralAdmin = 0;
		$filter_lawyer_office_id = '';
		$filter_lawyer_id = '';
		$user_type_code = '';
		$user_id = '';
		
		$current_page = 1;
		$has_prev = false;
		$has_next = false;
		$page_size = 25;
		$total_files_num = 0;
		$total_pages_num = 0;
		$current_page_size = 0;
		$post = '0';
		
		if ($mode =='1') { // general admin mode
			if($this->session->userdata('adminid')=='') {
				redirect('admin','refresh');
			}
			$isGeneralAdmin = 1;
		} else if ($mode =='2' || $mode =='3') {
			if($this->session->userdata('userid')=='') {
				redirect('user_login','refresh');
			}
			$isUser = 1;
			$user_id = $this->session->userdata ( 'userid' );
			$where = "where user_id =" . $user_id;
			$userRecord = $this->common->getOneRow ( 'user', $where );
			$lawyer_office_id = $userRecord ['lawyer_office_id'];
			$data ['profile_first_name'] = $userRecord ['first_name'];
			$data ['profile_last_name'] = $userRecord ['last_name'];
			$data ['profile_img'] = $userRecord ['profile_img'];
			$this->load->model ( "notification_model" );
			$notifications = $this->notification_model->getLatestDeliveredNotificationsForUser ( $user_id );
			$data ['notifications_list'] = $notifications ['notifications_list'];
			$data ['count_unseen'] = $notifications ['count_unseen'];
			$data ['page'] = "dashboard_reports/view_dashboard_reports/" . $mode;
			
			// $user_type_code = $userRecord['user_type_code'];
			$user_type_code = $this->session->userdata ( 'user_type' );
			$data ['user_type_code'] = $user_type_code;
			
			switch ($mode) {
				case "2" :
					$filter_lawyer_office_id = $lawyer_office_id;
					break;
				case "3" :
					$filter_lawyer_id = $user_id;
					break;
			}
		}
		
		if ($isGeneralAdmin == 1 || $isUser == 1) {
			
			$this->load->model ( "lawyer_office_model" );
			$this->load->model ( "case_model" );
			$this->load->model ( "dashboard_model" );
			$this->load->model ( "activities_model" );
			
			$lawyer_offices = $this->lawyer_office_model->getAllLawyerOffices ();
			$dashboard_activities = false;
			
			$from_date = '';
			$to_date = '';
			$from_date_Gre = '';
			$to_date_Gre = '';
			$from_date2_Gre = '';
			$to_date2_Gre = '';
			$filteredCasesCount = '';
			$AllCasesCount = '';
			$percentageCasesCount = '';
			$lawyers_performance = '';
			$lawyers = '';
			$lawyers_paid_total = 0;
			$lawyers_unpaid_total = 0;
			$lawyers_current_total = 0;
			$lawyers_late_total = 0;
			$report4Data = '';
			
			if (extract ( $_POST )) {
				
				if ($mode == 1) { // general admin
					$filter_lawyer_office_id = $_POST ['filter_lawyer_office_id'];
				}
				
				$from_date = $_POST ['from_date'];
				$to_date = $_POST ['to_date'];
				$from_date2_Gre = $_POST ['from_date2'];
				$to_date2_Gre = $_POST ['to_date2'];
				if ($from_date != null && $from_date != '') {
					$from_date_Gre = $this->dates_conversion->HijriToGregorian ( $from_date );
				}
				if ($to_date != null && $to_date != '') {
					$to_date_Gre = $this->dates_conversion->HijriToGregorian ( $to_date );
				}
			    $current_page = $this->input->post ( 'current_page' );
			    if($current_page <= 0){
			        $current_page = 1;
			    }
			    $post = '1';
			}else{
			    //initialize from date & to date  , مفترض عند  فتح صفحة التقارير بشكل آلي يتم اختيار آخر 30 يوم يتم احتساب حالات القضايا على أساسهم, وليس منذ البداية
			    date_default_timezone_set ( 'Asia/Riyadh' );
			    $one_month_before = date('Y-m-d H:i:s', strtotime('-30 day'));
			    $from_date = $this->dates_conversion->GregorianToHijri ( $one_month_before );
			    $from_date_Gre = $this->dates_conversion->HijriToGregorian ( $from_date );
			    $current_date = date ( "Y-m-d H:i:s" );
			    $to_date = $this->dates_conversion->GregorianToHijri ( $current_date );
			    $to_date_Gre = $this->dates_conversion->HijriToGregorian ( $to_date );
			}
			
			if ($filter_lawyer_office_id != '') {
			    $dashboard_activities = $this->activities_model->getDashboardActivities ( $filter_lawyer_office_id, $user_type_code, $user_id, $current_page, $page_size);
				if($dashboard_activities){
				    $current_page_size = count ( $dashboard_activities );
				    $total_activities_num = $dashboard_activities [0] ['count_all'];
    				$total_pages_num = ceil ( $total_activities_num / $page_size );
    				log_message('error', 'total_activities_num= '.$total_activities_num. '  total_pages_num= '.$total_pages_num. ' page_size= '.$page_size. ' $current_page_size= '.$current_page_size);
    				if ($current_page > 1) {
    				    $has_prev = true;
    				}
    				if ($current_page < $total_pages_num) {
    				    $has_next = true;
    				}
				}
			}
			
			if ($filter_lawyer_id != null && $filter_lawyer_id != '') {
				$lawyers = array ();
				$lawyers [0] = $this->dashboard_model->getLawyerData ( $filter_lawyer_id );
				$filter_lawyer_office_id = $lawyers [0] ['lawyer_office_id'];
			} else if ($filter_lawyer_office_id != '') {
				$lawyers = $this->dashboard_model->getLawyers ( $filter_lawyer_office_id );
			}
			
			/*
			 * print_r("*** lawyers:: ". var_dump($lawyers));
			 * echo '<br/>';
			 *
			 * print_r("*** filter_lawyer_office_id:: ". $filter_lawyer_office_id);
			 * echo '<br/>';
			 *
			 * print_r("*** filter_lawyer_id:: ". $filter_lawyer_id);
			 * echo '<br/>';
			 */
			
			if ($filter_lawyer_office_id != '') {
				$AllCasesCount = $this->dashboard_model->getReport1Data ( $filter_lawyer_id, $filter_lawyer_office_id, null, null );
				log_message('error', 'AllCasesCount:: ' . print_r( $AllCasesCount, true ) );
				// print_r ( 'AllCasesCount:: ' . var_dump ( $AllCasesCount ) );
				// echo '<br/>';
				
				$filteredCasesCount = $this->dashboard_model->getReport1Data ( $filter_lawyer_id, $filter_lawyer_office_id, $from_date_Gre, $to_date_Gre );
				log_message('error', 'filteredCasesCount:: ' . print_r($filteredCasesCount, true ) );
				// print_r ( 'filteredCasesCount:: ' . var_dump ( $filteredCasesCount ) );
				// echo '<br/>';
				
				$percentageCasesCount = array ();
				
				if ($AllCasesCount ['new'] == 0) {
					$percentageCasesCount ['new'] = 0;
				} else {
					$percentageCasesCount ['new'] = round ( (($filteredCasesCount ['new'] / $AllCasesCount ['new']) * 100) );
				}
				if ($AllCasesCount ['reg'] == 0) {
					$percentageCasesCount ['reg'] = 0;
				} else {
					$percentageCasesCount ['reg'] = round ( (($filteredCasesCount ['reg'] / $AllCasesCount ['reg']) * 100) );
				}
				if ($AllCasesCount ['dec34'] == 0) {
					$percentageCasesCount ['dec34'] = 0;
				} else {
					$percentageCasesCount ['dec34'] = round ( (($filteredCasesCount ['dec34'] / $AllCasesCount ['dec34']) * 100) );
				}
				if ($AllCasesCount ['adv'] == 0) {
					$percentageCasesCount ['adv'] = 0;
				} else {
					$percentageCasesCount ['adv'] = round ( (($filteredCasesCount ['adv'] / $AllCasesCount ['adv']) * 100) );
				}
				if ($AllCasesCount ['dec46'] == 0) {
					$percentageCasesCount ['dec46'] = 0;
				} else {
					$percentageCasesCount ['dec46'] = round ( (($filteredCasesCount ['dec46'] / $AllCasesCount ['dec46']) * 100) );
				}
				if ($AllCasesCount ['closed'] == 0) {
					$percentageCasesCount ['closed'] = 0;
				} else {
					$percentageCasesCount ['closed'] = round ( (($filteredCasesCount ['closed'] / $AllCasesCount ['closed']) * 100) );
				}
				log_message('error', 'percentageCasesCount:: ' . print_r ( $percentageCasesCount , true) );
				
				
				// REPORT 2 & 3
				if ($lawyers) {
					for($i = 0; $i < count ( $lawyers ); $i ++) {
						
						$lawyer_office_id = $lawyers [$i] ['lawyer_office_id'];
						$lawyer_office_name = $lawyers [$i] ['lawyer_office_name'];
						$lawyer_id = $lawyers [$i] ['lawyer_id'];
						$first_name = $lawyers [$i] ['first_name'];
						$last_name = $lawyers [$i] ['last_name'];
						$user_name = $lawyers [$i] ['user_name'];
						
						$lawyerPerformance = $this->dashboard_model->getLawyerPerformance ( $lawyer_id, $from_date_Gre, $to_date_Gre );
						// print_r ( 'lawyerPerformance:: ' . var_dump ( $lawyerPerformance) );
						// echo '<br/>';
						
						$allCasesCount = $lawyerPerformance ['all'];
						$currentCasesCount = $lawyerPerformance ['current'];
						$lateCasesCount = $lawyerPerformance ['late'];
						$closedCasesCount = $lawyerPerformance ['closed'];
						
						$lawyers [$i] ['all_cases_count'] = $allCasesCount;
						$lawyers [$i] ['current_cases_count'] = $currentCasesCount;
						$lawyers [$i] ['late_cases_count'] = $lateCasesCount;
						$lawyers [$i] ['closed_cases_count'] = $closedCasesCount;
						
						$lawyers_current_total = $lawyers_current_total + $currentCasesCount;
						$lawyers_late_total = $lawyers_late_total + $lateCasesCount;
						
						if (($currentCasesCount + $closedCasesCount) == 0 || $allCasesCount == 0 ) {
							$lawyers [$i] ['rate'] = 0;
						} else {
							$lawyers [$i] ['rate'] = round ( (($currentCasesCount + $closedCasesCount) / ($allCasesCount)) * 100 );
						}
						
						// calculate paid and unpaid invoices
						$lawyer_amounts = $this->dashboard_model->getUserSumInvoices ( $lawyer_id, $from_date_Gre, $to_date_Gre );
						$lawyers [$i] ['paid'] = $lawyer_amounts ['paid'];
						$lawyers [$i] ['unpaid'] = $lawyer_amounts ['unpaid'];
						
						$lawyers_paid_total = $lawyers_paid_total + $lawyers [$i] ['paid'];
						$lawyers_unpaid_total = $lawyers_unpaid_total + $lawyers [$i] ['unpaid'];
						// print_r(' lawyer_id:' .$lawyer_id. " *** paid :: ". $lawyers[$i]['paid'] ." *** unpaid :: ". $lawyers[$i]['unpaid'] );
					}
				}
				$lawyers_performance = $lawyers;
				//log_message('error', '$$lawyers_performance:    '.print_r($lawyers_performance, true) );
				
				// start Report 4
				if ($from_date2_Gre != null && $from_date2_Gre != '' && $to_date2_Gre != null && $to_date2_Gre != '') {
					$report4Data = $this->dashboard_model->getReport4Data ( $filter_lawyer_id, $filter_lawyer_office_id, $from_date2_Gre, $to_date2_Gre );
				}
			}
			$data ['lawyers_paid_total'] = $lawyers_paid_total;
			$data ['lawyers_unpaid_total'] = $lawyers_unpaid_total;
			$data ['lawyers_current_total'] = $lawyers_current_total;
			$data ['lawyers_late_total'] = $lawyers_late_total;
			$data ['dashboard_activities'] = $dashboard_activities;
			$data ['filter_lawyer_offices'] = $lawyer_offices;
			$data ['filter_lawyer_office_id'] = $filter_lawyer_office_id;
			$data ['from_date'] = $from_date;
			$data ['to_date'] = $to_date;
			$data ['from_date2'] = $from_date2_Gre;
			$data ['to_date2'] = $to_date2_Gre;
			$data ['dashboardReports'] = '1';
			$data ['filteredCasesCount'] = $filteredCasesCount;
			$data ['AllCasesCount'] = $AllCasesCount;
			$data ['percentageCasesCount'] = $percentageCasesCount;
			$data ['lawyers_performance'] = $lawyers_performance;
			$data ['report4Data'] = $report4Data;
			$data ['mode'] = $mode;
			$data ['current_page'] = $current_page;
			$data ['total_pages_num'] = $total_pages_num;
			$data ['page_size'] = $page_size;
			$data ['total_files_num'] = $total_files_num;
			$data ['current_page_size'] = $current_page_size;
			$data ['has_prev'] = $has_prev;
			$data ['has_next'] = $has_next;
			
			if ($post == '1') {
			    $faq_View = $this->load->view ( 'general_admin/view_dashboard_reports', $data, TRUE );
			    echo json_encode ( $faq_View );
			} else {
			    $this->load->view ( 'general_admin/view_dashboard_reports', $data );
			}
			
		} else {
			$data ['user_name'] = '';
			$this->load->view ( 'user_login', $data );
		}
	}
	
	
	function report4_chart_data() {
		if (extract ( $_POST )) {
			$from_date2_Gre = $this->input->post ( 'fromDate' );
			$to_date2_Gre = $this->input->post ( 'toDate' );
			$filter_lawyer_office_id = '';
			$filter_lawyer_id = '';
			if ($this->session->userdata ( 'adminid' ) != '') { // general admin mode
				$filter_lawyer_office_id = $this->input->post ( 'lawyerOfficeId' );
			}
			if ($this->session->userdata ( 'userid' ) != '') {
				$user_id = $this->session->userdata ( 'userid' );
				$where = "where user_id =" . $user_id;
				$userRecord = $this->common->getOneRow ( 'user', $where );
				$user_type_code = $userRecord ['user_type_code'];
				switch ($user_type_code) {
					case "ADMIN" :
						$filter_lawyer_office_id = $userRecord ['lawyer_office_id'];
						break;
					case "LAWYER" :
						$filter_lawyer_id = $user_id;
						$filter_lawyer_office_id = $userRecord ['lawyer_office_id'];
						break;
				}
			}
			$this->load->model ( "dashboard_model" );
			$report4Data = '';
			if ($from_date2_Gre != null && $from_date2_Gre != '' && $to_date2_Gre != null && $to_date2_Gre != '') {
				$report4Data = $this->dashboard_model->getReport4Data ( $filter_lawyer_id, $filter_lawyer_office_id, $from_date2_Gre, $to_date2_Gre );
				$data ['report4Data'] = $report4Data;
				// $data ['report4DataChart'] = consolidateData( $report4Data );
			}
			echo json_encode ( $report4Data );
		}
	}
	function consolidateData($report4Data) {
		// init result array
		$result = array ();
		// get the first row to be used as field names
		$cols = array_shift ( $data );
		// add rows
		foreach ( $data as $row ) {
			$newrow = array ();
			foreach ( $row as $index => $value ) {
				
				// pad months with zero if necessary
				if ($cols [$index] == 'date') {
					$value = preg_replace ( '/-([0-9]{1})$/', '-0$1', $value );
				}
				
				$newrow [$cols [$index]] = $value;
			}
			$result [] = $newrow;
		}
		
		return $result;
	}
	
}
	
	