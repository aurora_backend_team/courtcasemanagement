<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class case_view_details extends CI_Controller
{
	function __construct()
    {
     	parent::__construct();
        $this -> load -> model("common");
		if($this->session->userdata('userid')=='') {
			redirect('user_login','refresh');
		}
    }
    
	function index($case_id='') {
	  
	   self::view_case_details($case_id);
	   
	}

	function view_case_details($case_id='') {
		if($this->session->userdata('userid')!='') {	
		   	$user_id = $this->session->userdata('userid');
		   	$where = "where user_id =".$user_id;
			$userRecord = $this->common->getOneRow('user',$where);
			$data['profile_first_name']=$userRecord['first_name'];
			$data['profile_last_name']=$userRecord['last_name'];
			$data['profile_img']=$userRecord['profile_img'];
			$user_type_code = $userRecord['user_type_code'];	
			$data['user_type_code'] = $user_type_code;
			$this -> load -> model("case_model");
			
			$data['case_details'] = $this->case_model->getCaseDetails($user_type_code, $case_id);	   
			
			$current_state_code = $data['case_details']['current_state_code'];
			
			$case_transactions = $this->case_model->getCaseTransactions($case_id);
			$data['case_transactions'] = $case_transactions;
			
			$transitions = $this -> common -> getAllRow("state_transition","where source_state ='$current_state_code'");
			$available_transitions = array();
			if ($transitions) {
				foreach($transitions as $transition ) {
					$transition_id = $transition['id'];
					$count = $this->common->getCountOfField("id","usertype_privileges","where user_type_code ='".$user_type_code."' AND state_transition='".$transition_id."'");
					if ($count > 0) {
						array_push($available_transitions, $transition);
					}
				}
			}
			$data['available_transitions'] = $available_transitions;
			
			$this -> load -> model("comments_model");
	  		$data['comments'] = $this->comments_model->getCaseComments($case_id);
	  		
			$this->load->view('cases/view_case_details',$data);
	  
						
	   } else {
		   	$data['user_name']='';
		    $this->load->view('user_login',$data);
		 
	   }
	}
	
	function add_comment($case_id='') {		
		if(extract($_POST)) {			
			$value['comment']= $_POST['comment']; 	
			
			$user_id = $this->session->userdata('userid');
			$value['user_id']=$user_id;
	    	$value['case_id']=$case_id;
	    	date_default_timezone_set('Asia/Riyadh');
	    	$value['date_time']=date("Y-m-d H:i:s");

	    	$this->common->insertRecord('comment',$value);
			
		}
		redirect('case_view_details/view_case_details/'.$case_id);
	}
	
}

