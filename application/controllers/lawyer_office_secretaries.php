<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class lawyer_office_secretaries extends CI_Controller
{
	function __construct()
    {
        parent::__construct();
        $this -> load -> model("common");
		$this->load->library('encrypt');
		if($this->session->userdata('userid')=='' || $this->session->userdata('user_type')!='ADMIN') {
			redirect('user_login','refresh');
		}
    }
    
	function index() {
	  
	   self::view_secretaries();
	   
	}


	function view_secretaries() {
	   $office_admin_id = $this->session->userdata('userid');
	   
	   $this -> load -> model("notification_model");
	   	$notifications = $this -> notification_model -> getLatestDeliveredNotificationsForUser($office_admin_id);
	   	$data['notifications_list'] = $notifications['notifications_list'];
		$data['count_unseen'] = $notifications['count_unseen'];
		$data['page'] = "lawyer_office_secretaries/view_secretaries";
		
	   
	   $userRecord = $this -> common -> getOneRecField_Limit("lawyer_office_id", "user","where user_id ='$office_admin_id'");
	   $lawyer_office_id = $userRecord->lawyer_office_id;	
	   $this -> load -> model("lawyer_office_model");
	   $data['secretaries'] = $this->lawyer_office_model->getSecretaries($lawyer_office_id);
	   $where = "where user_id =".$office_admin_id ;
	   $result = $this->common->getOneRow('user',$where);
	   $data['profile_first_name']=$result['first_name'];
	   $data['profile_last_name']=$result['last_name'];
	   $data['profile_img']=$result['profile_img'];
	   $this->load->view('office_admin/view_secretaries',$data);
	}
	
	function delete_secretary($user_id) {
		
		$this->db->trans_begin();
		
		$where = "user_id =".$user_id;
		$this->common->deleteRecord('comment',$where);
		
		$where = "user_id =".$user_id;
		$this->common->deleteRecord('user_activities',$where);
		
		$where = "to_user_id =".$user_id;
		$this->common->deleteRecord('delivered_notifications',$where);
		
		$where = "user_id =".$user_id;
		$this->common->deleteRecord('user',$where);
		
		if ($this->db->trans_status() === FALSE) {
   			$this->db->trans_rollback();
		} else {
		    $this->db->trans_commit();
		}
		
		redirect('lawyer_office_secretaries');
	}
	
	function add_edit_secretary($user_id=0, $email_already_exists=0, $user_name_exists=0) {
		
		$office_admin_id = $this->session->userdata('userid');
	    $userRecord = $this -> common -> getOneRecField_Limit("lawyer_office_id", "user","where user_id ='$office_admin_id'");
	    $lawyer_office_id = $userRecord->lawyer_office_id;
	   $where = "where user_id =".$office_admin_id  ;
	   $result = $this->common->getOneRow('user',$where);
	   $data['profile_first_name']=$result['first_name'];
	   $data['profile_last_name']=$result['last_name'];
	   $data['profile_img']=$result['profile_img'];
	   
	    $this -> load -> model("notification_model");
	   	$notifications = $this -> notification_model -> getLatestDeliveredNotificationsForUser($office_admin_id);
	   	$data['notifications_list'] = $notifications['notifications_list'];
		$data['count_unseen'] = $notifications['count_unseen'];
		$data['page'] = "lawyer_office_secretaries/add_edit_secretary/".$user_id."/".$email_already_exists."/".$user_name_exists;
	   
        $data['user_id']=$user_id;
	    $data['lawyer_office_id']=$lawyer_office_id;
		$data['first_name']="";	
		$data['last_name']="";
	    $data['mobile_number']="";
		$data['email']="";
		$data['user_type_code']="SECRETARY";
	    $data['user_name']="";
	    $data['password']="";
	    $data['email_exists_error']= $email_already_exists;
	    $data['username_exists_error']= $user_name_exists;
			   		
		if(extract($_POST)) {			
			$value['lawyer_office_id']= $lawyer_office_id; 
			$value['first_name']= $_POST['first_name']; 			
			$value['last_name']= $_POST['last_name'];
			$value['mobile_number']= $_POST['mobile_number']; 
			$value['email']= $_POST['email']; 			
			$value['user_type_code']= "SECRETARY";
			$value['user_name']= $_POST['user_name']; 
			$value['password']= $this->encrypt->encode($_POST['password']);
						
			if($user_id > 0) {
				$new_email = $value['email'];
				$email_count = $this->common->getCountOfField("user_id","user","where email ='".$new_email."' AND user_id !='".$user_id."'");
				$new_user_name = $value['user_name'];
				$user_name_count = $this->common->getCountOfField("user_id","user","where user_name ='".$new_user_name."' AND user_id !='".$user_id."'");
				if ($email_count == 0 && $user_name_count == 0) {
					$where = "user_id =".$user_id;
					$this->common->updateRecord('user',$value,$where);
					redirect('lawyer_office_secretaries');
				} else {
					$email_exists = 0;
					if ($email_count != 0) {
						$email_exists=1;
					}
					$user_name_exists = 0;
					if ($user_name_count != 0) {
						$user_name_exists=1;
					}
					self::secretary_with_error($user_id,$lawyer_office_id,$value['first_name'],$value['last_name'],$value['mobile_number'],
					$value['email'], $value['user_name'],$value['password'],$data['profile_first_name'],$data['profile_last_name'],
					$data['profile_img'], $email_exists, $user_name_exists, $data['notifications_list'], $data['count_unseen'], $data['page']);
				}
				
				
			} else {
				$new_email = $value['email'];
				$email_count = $this->common->getCountOfField("user_id","user","where email ='".$new_email."'");
				$new_user_name = $value['user_name'];
				$user_name_count = $this->common->getCountOfField("user_id","user","where user_name ='".$new_user_name."'");
				if ($email_count == 0 && $user_name_count == 0) {
					$this->common->insertRecord('user',$value);
					redirect('lawyer_office_secretaries');
				} else {
					$email_exists = 0;
					if ($email_count != 0) {
						$email_exists=1;
					}
					$user_name_exists = 0;
					if ($user_name_count != 0) {
						$user_name_exists=1;
					}
					self::secretary_with_error($user_id,$lawyer_office_id,$value['first_name'],$value['last_name'],$value['mobile_number'],
					$value['email'], $value['user_name'],$value['password'],$data['profile_first_name'],$data['profile_last_name'],
					$data['profile_img'], $email_exists, $user_name_exists, $data['notifications_list'], $data['count_unseen'], $data['page']);
				}
				
			}
			
		} else {
			if($user_id > 0) {
				$where = "where user_id =".$user_id ;
				$result = $this->common->getOneRow('user',$where);
				$data['lawyer_office_id']=$result['lawyer_office_id'];
		    	$data['first_name']=$result['first_name'];
				$data['last_name']=$result['last_name'];
				$data['mobile_number']=$result['mobile_number'];
		    	$data['email']=$result['email'];
				$data['user_type_code']=$result['user_type_code'];
				$data['user_name']=$result['user_name'];
		    	$data['password']=$this->encrypt->decode($result['password']);
		    	
			}
					
			$this->load->view('office_admin/add_edit_secretary',$data);
		}
		
		
	}
	
	function secretary_with_error($user_id,$lawyer_office_id,$first_name,$last_name,$mobile_number,$email,$user_name,$password,
					$profile_first_name,$profile_last_name,$profile_img, $email_exists, $user_name_exists,
					$notifications_list, $count_unseen, $page) {
		
		$data['profile_first_name']=$profile_first_name;
	    $data['profile_last_name']=$profile_last_name;
	    $data['profile_img']=$profile_img;
	    
	    $data['notifications_list'] = $notifications_list;
	    $data['count_unseen'] = $count_unseen;
	    $data['page'] = $page;
	    
	    $data['user_id']=$user_id;
	    $data['lawyer_office_id']=$lawyer_office_id;
		$data['first_name']=$first_name;	
		$data['last_name']=$last_name;
	    $data['mobile_number']=$mobile_number;
		$data['email']=$email;
		$data['user_type_code']="SECRETARY";
	    $data['user_name']=$user_name;
	    $data['password']=$password;
		$data['email_exists_error']= $email_exists;
		$data['username_exists_error']= $user_name_exists;
				
		$this->load->view('office_admin/add_edit_secretary',$data);	
	}
	
}
?>