<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class users_activities extends CI_Controller
{
	function __construct()
    {
        parent::__construct();
        $this -> load -> model("common");
    	if($this->session->userdata('adminid')=='') {
			redirect('admin','refresh');
		}
    }
    
	function index() {
	  
	   self::view_users_activities();
	   
	}
	
	function view_users_activities() {
		
	   	$this -> load -> model("activities_model");
		$data['activities'] = $this -> activities_model -> getUsersActivities();
	   
	    $this->load->view('general_admin/view_activities',$data);
	}
	
}

