<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
require (APPPATH . '/controllers/firebase_notification.php');
require (APPPATH . '/libraries/REST_Controller.php');
class Cases extends REST_Controller {
	function get_cases_counts_post() {
		if (isset ( $_POST ['user_id'] )) {
			$this->load->model ( "case_model" );
			$data = $this->case_model->get_cases_count ( $_POST ['user_id'] );
			$this->response ( array (
					'message' => 'Cases Count returned Successfully',
					'states' => $data ['ordered_states'],
					'cases_count' => $data ['counts'],
					'success' => '1' 
			), 200 );
		} else {
			$this->response ( array (
					'message' => 'بعض البيانات مفقودة',
					'success' => '0' 
			), 200 );
		}
	}
	function get_cases_list_post() {
		if (isset ( $_POST ['user_id'] ) && isset ( $_POST ['state_code'] )) {
			if (isset ( $_POST ['page'] ))
				$page = $_POST ['page'];
			else
				$page = 1;
			
			$this->load->model ( "case_model" );
			$user_id = $_POST ['user_id'];
			$state_code = $_POST ['state_code'];
			if ($state_code == 'ALL')
				$state_code = null;
			$where = "where user_id =" . $user_id;
			$userRecord = $this->common->getOneRow ( 'user', $where );
			$user_type_code = $userRecord ['user_type_code'];
			$lawyer_office_id = $userRecord ['lawyer_office_id'];
			
			$search_data = array ();
			switch ($user_type_code) {
				case "ADMIN" :
					$admin_cases = $this->case_model->getAllCases ( $lawyer_office_id, $user_type_code, $state_code, null, $search_data, $page, 0 );
					if ($admin_cases) {
						self::array_dual_sort_by_column ( $admin_cases, "number_of_late_instalments", "creation_date", SORT_DESC, SORT_ASC );
					}
					$final_cases = $admin_cases;
					break;
				case "CUSTOMER" :
					$customer_cases = $this->case_model->getCustomerCases ( $user_id, $user_type_code, $state_code, null, $search_data, $page, 0 );
					if ($customer_cases) {
						self::array_dual_sort_by_column ( $customer_cases, "number_of_late_instalments", "creation_date", SORT_DESC, SORT_ASC );
					}
					$final_cases = $customer_cases;
					break;
				case "LAWYER" :
					$lawyer_cases = $this->case_model->getLawyerCases ( $user_id, $user_type_code, $state_code, null, $search_data, $page, 0 );
					if ($lawyer_cases)
						self::array_multi_sort_by_column ( $lawyer_cases, "state_priotity", "number_of_late_instalments", "creation_date", SORT_DESC, SORT_DESC, SORT_ASC );
					
					$final_cases = $lawyer_cases;
					break;
				case "SECRETARY" :
					$secretary_cases = $this->case_model->getAllCases ( $lawyer_office_id, $user_type_code, $state_code, null, $search_data, $page, 0 );
					if ($secretary_cases) {
						self::array_dual_sort_by_column ( $secretary_cases, "number_of_late_instalments", "creation_date", SORT_DESC, SORT_ASC );
					}
					$final_cases = $secretary_cases;
					break;
				default :
					$all_cases = $this->case_model->getAllCases ( $lawyer_office_id, $user_type_code, $state_code, null, $search_data, $page, 0 );
					if ($all_cases) {
						self::array_dual_sort_by_column ( $all_cases, "number_of_late_instalments", "creation_date", SORT_DESC, SORT_ASC );
					}
					$final_cases = $all_cases;
			}
			
			if ($final_cases) {
				
				$i = 0;
				$this->load->library ( '../controllers/dates_conversion' );
				foreach ( $final_cases as $record ) {
					
					$data [$i] ['case_id'] = $record ['case_id'];
					$data [$i] ['client_id_number'] = $record ['client_id_number'];
					$data [$i] ['client_name'] = $record ['client_name'];
					$data [$i] ['current_state_code'] = $record ['current_state_code'];
					$dates_conversion = new dates_conversion ();
					// $data [$i] ['creation_date'] = $this->dates_conversion->GregorianToHijri ( $record ['creation_date'] );
					
					/*
					 * $data [$i] ['last_update_date'] = $this->dates_conversion->GregorianToHijri ( $record ['last_update_date'] );
					 * if ($record ['decision_34_date'] != NULL && $record ['decision_34_date'] != "000-00-00") {
					 * $data [$i] ['decision_34_date'] = $this->dates_conversion->GregorianToHijri ( $record ['decision_34_date'] );
					 * }
					 * if ($record ['decision_46_date'] != NULL && $record ['decision_46_date'] != "000-00-00") {
					 * $data [$i] ['decision_46_date'] = $this->dates_conversion->GregorianToHijri ( $record ['decision_46_date'] );
					 * }
					 * if ($record ['executive_order_date'] != NULL && $record ['executive_order_date'] != "000-00-00") {
					 * $data [$i] ['executive_order_date'] = $this->dates_conversion->GregorianToHijri ( $record ['executive_order_date'] );
					 * }
					 * if ($record ['advertisement_date'] != NULL && $record ['advertisement_date'] != "000-00-00") {
					 * $data [$i] ['advertisement_date'] = $this->dates_conversion->GregorianToHijri ( $record ['advertisement_date'] );
					 * }
					 */
					
					$case_transactions = $this->case_model->getCaseTransactions ( $record ["case_id"], $lawyer_office_id );
					$last_state_date = '';
					if ($case_transactions) {
						$index = 0;
						foreach ( $case_transactions as $record ) {
							if ($record ['state_date'] != null && $record ['state_date'] != "") {
								$case_transactions [$index] ['state_date'] = $this->dates_conversion->GregorianToHijri ( $record ['state_date'] );
								$last_state_date = $case_transactions [$index] ['state_date'];
							}
							$index ++;
						}
					}
					$data [$i] ['last_trans_date'] = $last_state_date;
					
					$i ++;
				}
				
				// $data ['cases_count'] = count ( $data );
				$this->response ( array (
						'message' => 'تم تحميل القضايا',
						'cases' => $data,
						'success' => '1' 
				), 200 );
			} else
				$this->response ( array (
						'message' => 'لا يوجد قضايا',
						'success' => '1' 
				), 200 );
		} else {
			$this->response ( array (
					'message' => 'بعض البيانات مفقودة',
					'success' => '0' 
			), 200 );
		}
	}
	function array_multi_sort_by_column(&$array, $column1, $column2, $column3, $dir1 = SORT_ASC, $dir2 = SORT_ASC, $dir3 = SORT_ASC) {
		$sort_column = array ();
		foreach ( $array as $key => $row ) {
			$sort_column [$column1] [$key] = $row [$column1];
			$sort_column [$column2] [$key] = $row [$column2];
			$sort_column [$column3] [$key] = $row [$column3];
		}
		array_multisort ( $sort_column [$column1], $dir1, $sort_column [$column2], $dir2, $sort_column [$column3], $dir3, $array );
	}
	function array_dual_sort_by_column(&$array, $column1, $column2, $dir1 = SORT_ASC, $dir2 = SORT_ASC) {
		$sort_column = array ();
		foreach ( $array as $key => $row ) {
			$sort_column [$column1] [$key] = $row [$column1];
			$sort_column [$column2] [$key] = $row [$column2];
		}
		array_multisort ( $sort_column [$column1], $dir1, $sort_column [$column2], $dir2, $array );
	}
	function get_case_details_post() {
		$this->load->model ( "common" );
		
		if (isset ( $_POST ['user_id'] ) && isset ( $_POST ['case_id'] )) {
			$this->load->model ( "case_model" );
			$this->load->model ( "comments_model" );
			$user_id = $_POST ['user_id'];
			$case_id = $_POST ['case_id'];
			$where = "where user_id =" . $user_id;
			$userRecord = $this->common->getOneRow ( 'user', $where );
			
			$lawyer_office_id = $userRecord ['lawyer_office_id'];
			$user_type_code = $userRecord ['user_type_code'];
			self::getCaseDetails ( $user_type_code, $case_id, $lawyer_office_id, 1 ); // means from get case detail
		} else {
			$this->response ( array (
					'message' => 'بعض البيانات مفقودة',
					'success' => '0' 
			), 200 );
		}
	}
	function getCaseDetails($user_type_code, $case_id, $lawyer_office_id, $call_type) {
		$case_details = $this->case_model->getCaseDetails ( $user_type_code, $case_id, $lawyer_office_id, 1 );
		
		$this->load->library ( '../controllers/dates_conversion' );
		// $data['case_details']['creation_date'] = $this->dates_conversion->GregorianToHijri($data['case_details']['creation_date']);
		// $data['case_details']['last_update_date'] = $this->dates_conversion->GregorianToHijri($data['case_details']['last_update_date']);
		if ($case_details ['decision_34_date'] != NULL && $case_details ['decision_34_date'] != null && $case_details ['decision_34_date'] != "000-00-00") {
			$case_details ['decision_34_date'] = $this->dates_conversion->GregorianToHijri ( $case_details ['decision_34_date'] );
		}
		if ($case_details ['decision_46_date'] != NULL && $case_details ['decision_46_date'] != null && $case_details ['decision_46_date'] != "000-00-00") {
			$case_details ['decision_46_date'] = $this->dates_conversion->GregorianToHijri ( $case_details ['decision_46_date'] );
		}
		if ($case_details ['executive_order_date'] != NULL && $case_details ['executive_order_date'] != null && $case_details ['executive_order_date'] != "000-00-00") {
			$case_details ['executive_order_date'] = $this->dates_conversion->GregorianToHijri ( $case_details ['executive_order_date'] );
		}
		if ($case_details ['advertisement_date'] != NULL && $case_details ['advertisement_date'] != null && $case_details ['advertisement_date'] != "000-00-00") {
			$case_details ['advertisement_date'] = $this->dates_conversion->GregorianToHijri ( $case_details ['advertisement_date'] );
		}
		if ($case_details ['creation_date'] != NULL && $case_details ['creation_date'] != null && $case_details ['creation_date'] != "000-00-00") {
			$case_details ['creation_date'] = $this->dates_conversion->GregorianToHijri ( $case_details ['creation_date'] );
		}
		if ($case_details ['suspend_date'] != NULL && $case_details ['suspend_date'] != null && $case_details ['suspend_date'] != "000-00-00") {
			$case_details ['suspend_date'] = $this->dates_conversion->GregorianToHijri ( $case_details ['suspend_date'] );
		}
		$current_state_code = $case_details ['current_state_code'];
	
		
		
		
		
		
		$trans_where_conditions = "where source_state ='$current_state_code' ";
		$user_types_reopen = $this->case_model->getUserTypeWithActionPrvlg("REOPEN");
		$user_types_resume = $this->case_model->getUserTypeWithActionPrvlg("RESUME");
		$has_prvlg_reopen = false;
		$has_prvlg_resume = false;
		if($user_types_reopen != null){
			foreach ($user_types_reopen as $type_rec){
				if($user_type_code == $type_rec["user_type_code"]){
					$has_prvlg_reopen = true;
					break;
				}
			}
		}
		if($user_types_resume != null){
			foreach ($user_types_resume as $type_rec){
				if($user_type_code == $type_rec["user_type_code"]){
					$has_prvlg_resume = true;
					break;
				}
			}
		}
		if(($current_state_code == "CLOSED" && $has_prvlg_reopen !== false)|| ($current_state_code == "SUSPENDED" && $has_prvlg_resume !== false)){
			$last_trns = $this->case_model->getTransactionId ("current_state_code != '".$current_state_code."' and case_id = '" . $case_id . "' AND lawyer_office_id = '" . $lawyer_office_id . "' AND transaction_status = 'Success' AND (action_code IS NULL OR action_code != 'REASSIGN_TO_LAWYER') AND edit_action = 'N'");
			$last_state = $last_trns["current_state_code"];
			$trans_where_conditions = $trans_where_conditions . "or (source_state = '".$last_state."' and target_state != 'CLOSED' and target_state != 'SUSPENDED' and action_code != 'REASSIGN_TO_LAWYER' )";
		}
		$transitions = $this->common->getAllRow ( "state_transition", $trans_where_conditions );
		$available_transitions = array ();
		if ($transitions) {
			foreach ( $transitions as $transition ) {
				$transition_id = $transition ['id'];
				// in case no reopen and no resume display default for Admin
				$condition = " and `condition` = 0";
				// in case there is reopen or resume add special actions (lawyer actions) for admin (including all conditions 0,1)
				if((($current_state_code == "CLOSED" && $has_prvlg_reopen !== false)|| ($current_state_code == "SUSPENDED" && $has_prvlg_resume !== false)) && $user_type_code == "ADMIN"){
					
					$condition = "";
				}
				$count = $this->common->getCountOfField ( "id", "usertype_privileges", "where user_type_code ='" . $user_type_code . "' AND state_transition='" . $transition_id . "'".$condition );
				if ($count > 0) {
					// only secretary which made assign to lawyer can make reassign
					/*
					 * if ($transition['action_code'] == "REASSIGN_TO_LAWYER" && $user_type_code == "SECRETARY") {
					 * $assign_lawyer_trans = $this -> common -> getOneRecField_Limit("last_update_user", "case_transactions","where current_state_code = 'ASSIGNED_TO_LAWYER' AND case_id = '".$case_id."' AND lawyer_office_id ='.$lawyer_office_id.' AND transaction_status = 'Success' AND edit_action = 'N'");
					 * $assigner_user = $assign_lawyer_trans->last_update_user;
					 * if ($user_id == $assigner_user) {
					 * array_push($available_transitions, $transition);
					 * }
					 * } else {
					 */
					array_push ( $available_transitions, $transition );
					// }
				}
			}
		}
		
		$data ['available_transitions'] = $available_transitions;
		
		$data ['suspend_reasons'] = $this->common->getAllRow ( 'case_suspend_reason', '' );
		$data ['close_reasons'] = $this->common->getAllRow ( 'case_close_reason', '' );
		$data ['available_customers'] = $this->case_model->getAvailableCustomers ( $lawyer_office_id );
		$data ['available_customer_types'] = $this->common->getAllRow ( "customer_type", "" );
		
		$available_regions = $this->common->getAllRow ( "region", "where lawyer_office_id = " . $lawyer_office_id );
		$this->load->model ( "lawyer_office_model" );
		foreach ( $available_regions as $region ) {
			$regionid = $region ['region_id'];
			$lawyers_in_region [$regionid] = $this->lawyer_office_model->getLawyersInRegion ( $lawyer_office_id, $regionid, $case_details ['lawyer_id'] );
		}
		
		$case_transactions = $this->case_model->getCaseTransactions ( $case_id, $lawyer_office_id );
		$last_state_date = '';
		if ($case_transactions) {
			$index = 0;
			foreach ( $case_transactions as $record ) {
				if ($record ['state_date'] != null && $record ['state_date'] != "") {
					$case_transactions [$index] ['state_date'] = $this->dates_conversion->GregorianToHijri ( $record ['state_date'] );
					$last_state_date = $case_transactions [$index] ['state_date'];
				}
				$index ++;
			}
		}
		// $data['case_transactions'] = $case_transactions;
		
		if ($call_type == 1)
			$message = 'تم تحميل تفاصيل القضية';
		else if ($call_type == 2)
			$message = 'تمت العملية بنجاح';
		else if ($call_type == 3)
			$message = 'تم حفظ تفاصيل القضية';
		
		$this->response ( array (
				'message' => $message,
				'user_type' => $user_type_code,
				'case_details' => $case_details,
				
				'available_transitions' => $data ['available_transitions'],
				
				'suspend_reasons' => $data ['suspend_reasons'],
				'close_reasons' => $data ['close_reasons'],
				'available_customers' => $data ['available_customers'],
				'available_customer_types' => $data ['available_customer_types'],
				'regions' => $available_regions,
				'lawyers_per_region' => $lawyers_in_region,
				'case_transactions' => $case_transactions,
				'last_state_date' => $last_state_date,
				'success' => '1' 
		), 200 );
	}
	function quick_search_cases_post() {
		$this->load->model ( "notification_model" );
		$this->load->model ( "case_model" );
		if (isset ( $_POST ['page'] ))
			$page = $_POST ['page'];
		else
			$page = 1;
		$search_query = "";
		$search_data = array ();
		if (isset ( $_POST ['state_code'] ) && isset ( $_POST ['user_id'] ) && isset ( $_POST ['query'] )) {
			$this->load->library ( '../controllers/dates_conversion' );
			$search_query = $_POST ['query'];
			// }
			// $data['search_values'] = self::getSearchValuesFromSearchData($search_data);
			// if($this->session->userdata('userid')!='') {
			$status = $_POST ['state_code'];
			if ($status == "ALL")
				$status = '0';
			$user_id = $_POST ['user_id'];
			$where = "where user_id =" . $user_id;
			$userRecord = $this->common->getOneRow ( 'user', $where );
			$lawyer_office_id = $userRecord ['lawyer_office_id'];
			
			$user_type_code = $userRecord ['user_type_code'];
			// $data['user_type_code'] = $user_type_code;
			// $data['cases_count'] = 0;
			$cases_data = array ();
			$search_result = array ();
			$result_count = 0;
			switch ($user_type_code) {
				case "ADMIN" :
					$admin_cases = $this->case_model->getAllCases ( $lawyer_office_id, $user_type_code, $status, null, $search_data, $page, 0, 1, $search_query );
					if ($admin_cases) {
						self::array_dual_sort_by_column ( $admin_cases, "number_of_late_instalments", "creation_date", SORT_DESC, SORT_ASC );
					}
					$cases_data = $admin_cases;
					break;
				case "CUSTOMER" :
					$customer_cases = $this->case_model->getCustomerCases ( $user_id, $user_type_code, $status, null, $search_data, $page, 0, 1, $search_query );
					if ($customer_cases) {
						self::array_dual_sort_by_column ( $customer_cases, "number_of_late_instalments", "creation_date", SORT_DESC, SORT_ASC );
					}
					$cases_data = $customer_cases;
					break;
				case "LAWYER" :
					$lawyer_cases = $this->case_model->getLawyerCases ( $user_id, $user_type_code, $status, null, $search_data, $page, 0, 1, $search_query );
					if ($lawyer_cases)
						self::array_multi_sort_by_column ( $lawyer_cases, "state_priotity", "number_of_late_instalments", "creation_date", SORT_DESC, SORT_DESC, SORT_ASC );
					
					$cases_data = $lawyer_cases;
					break;
				case "SECRETARY" :
					$secretary_cases = $this->case_model->getAllCases ( $lawyer_office_id, $user_type_code, $status, null, $search_data, $page, 0, 1, $search_query );
					if ($secretary_cases) {
						self::array_dual_sort_by_column ( $secretary_cases, "number_of_late_instalments", "creation_date", SORT_DESC, SORT_ASC );
					}
					$cases_data = $secretary_cases;
					break;
				default :
					$all_cases = $this->case_model->getAllCases ( $lawyer_office_id, $user_type_code, $status, null, $search_data, $page, 0, 1, $search_query );
					if ($all_cases) {
						self::array_dual_sort_by_column ( $all_cases, "number_of_late_instalments", "creation_date", SORT_DESC, SORT_ASC );
					}
					$cases_data = $all_cases;
			}
			
			if ($cases_data) {
				
				$i = 0;
				$this->load->library ( '../controllers/dates_conversion' );
				foreach ( $cases_data as $record ) {
					$dates_conversion = new dates_conversion ();
					$cases_data [$i] ['creation_date'] = $this->dates_conversion->GregorianToHijri ( $record ['creation_date'] );
					
					$cases_data [$i] ['last_update_date'] = $this->dates_conversion->GregorianToHijri ( $record ['last_update_date'] );
					if ($record ['decision_34_date'] != NULL && $record ['decision_34_date'] != null && $record ['decision_34_date'] != "000-00-00") {
						$cases_data [$i] ['decision_34_date'] = $this->dates_conversion->GregorianToHijri ( $record ['decision_34_date'] );
					}
					if ($record ['decision_46_date'] != NULL && $record ['executive_order_date'] != null && $record ['decision_46_date'] != "000-00-00") {
						$cases_data [$i] ['decision_46_date'] = $this->dates_conversion->GregorianToHijri ( $record ['decision_46_date'] );
					}
					if ($record ['executive_order_date'] != NULL && $record ['executive_order_date'] != null && $record ['executive_order_date'] != "000-00-00") {
						$cases_data [$i] ['executive_order_date'] = $this->dates_conversion->GregorianToHijri ( $record ['executive_order_date'] );
					}
					if ($record ['advertisement_date'] != NULL && $record ['advertisement_date'] != null && $record ['advertisement_date'] != "000-00-00") {
						$cases_data [$i] ['advertisement_date'] = $this->dates_conversion->GregorianToHijri ( $record ['advertisement_date'] );
					}
					
					$case_transactions = $this->case_model->getCaseTransactions ( $cases_data [$i] ["case_id"], $lawyer_office_id );
					$last_state_date = '';
					if ($case_transactions) {
						$index = 0;
						foreach ( $case_transactions as $record ) {
							if ($record ['state_date'] != null && $record ['state_date'] != "") {
								$case_transactions [$index] ['state_date'] = $this->dates_conversion->GregorianToHijri ( $record ['state_date'] );
								$last_state_date = $case_transactions [$index] ['state_date'];
							}
							$index ++;
						}
					}
					$cases_data [$i] ['last_trans_date'] = $last_state_date;
					
					// start quick search
					$current_state_code = $cases_data [$i] ['current_state_code'];
					$state_record = $this->common->getOneRecField_Limit ( "state_name", "state", "where state_code = '" . $current_state_code . "'" );
					$current_state_name = $state_record->state_name;
					$region_id = $cases_data [$i] ['region_id'];
					if ($region_id != null && $region_id != "") {
						$region_record = $this->common->getOneRecField_Limit ( "name", "region", "where region_id = '" . $region_id . "'" );
						$region_name = $region_record->name;
					}
					$y = "dd";
					if ($search_query != "") {
						if (strpos ( $cases_data [$i] ['customer_name'], $search_query ) !== false || strpos ( $cases_data [$i] ['contract_number'], $search_query ) !== false || strpos ( $cases_data [$i] ['debtor_name'], $search_query ) !== false || strpos ( $cases_data [$i] ['client_name'], $search_query ) !== false || strpos ( $cases_data [$i] ['client_id_number'], $search_query ) !== false || strpos ( $cases_data [$i] ['client_type'], $search_query ) !== false || strpos ( $cases_data [$i] ['obligation_value'], $search_query ) !== false || strpos ( $cases_data [$i] ['number_of_late_instalments'], $search_query ) !== false || strpos ( $cases_data [$i] ['due_amount'], $search_query ) !== false || strpos ( $cases_data [$i] ['remaining_amount'], $search_query ) !== false || strpos ( $cases_data [$i] ['total_debenture_amount'], $search_query ) !== false || strpos ( $cases_data [$i] ['executive_order_number'], $search_query ) !== false || strpos ( $cases_data [$i] ['order_number'], $search_query ) !== false || strpos ( $cases_data [$i] ['circle_number'], $search_query ) !== false || strpos ( $cases_data [$i] ['decision_34_number'], $search_query ) !== false || strpos ( $cases_data [$i] ['lawyer_name'], $search_query ) !== false || strpos ( $cases_data [$i] ['suspend_reason'], $search_query ) !== false || strpos ( $cases_data [$i] ['closing_reason'], $search_query ) !== false || strpos ( $cases_data [$i] ['case_id'], $search_query ) !== false || strpos ( $cases_data [$i] ['creation_date'], $search_query ) !== false || strpos ( $cases_data [$i] ['decision_34_date'], $search_query ) !== false || strpos ( $cases_data [$i] ['decision_46_date'], $search_query ) !== false || strpos ( $cases_data [$i] ['executive_order_date'], $search_query ) !== false || strpos ( $cases_data [$i] ['advertisement_date'], $search_query ) !== false || strpos ( $cases_data [$i] ['last_update_date'], $search_query ) !== false || strpos ( $current_state_name, $search_query ) !== false || ($region_id != null && $region_id != "" && strpos ( $region_name, $search_query ) !== false)) {
							// continue;
							$case ['case_id'] = $cases_data [$i] ['case_id'];
							$case ['client_id_number'] = $cases_data [$i] ['client_id_number'];
							$case ['client_name'] = $cases_data [$i] ['client_name'];
							$case ['current_state_code'] = $cases_data [$i] ['current_state_code'];
							$case ['last_trans_date'] = $cases_data [$i] ['last_trans_date'];
							$y = 'yes';
							array_push ( $search_result, $case );
						}
					}
					// end quick search
					
					$i ++;
				}
				
				switch ($user_type_code) {
					case "ADMIN" :
						$result_count = $this->case_model->getAllCasesCount ( $lawyer_office_id, $user_type_code, $status, $search_data, $page, 0, null, 1, $search_query );
						break;
					case "CUSTOMER" :
						$result_count = $this->case_model->getCustomerCasesCount ( $user_id, $user_type_code, $status, $search_data, $page, 0, null, 1, $search_query );
						break;
					case "LAWYER" :
						$result_count = $this->case_model->getLawyerCasesCount ( $user_id, $user_type_code, $status, $search_data, $page, 0, null, 1, $search_query );
						break;
					case "SECRETARY" :
						$result_count = $this->case_model->getAllCasesCount ( $lawyer_office_id, $user_type_code, $status, $search_data, $page, 0, null, 1, $search_query );
						break;
					default :
						$result_count = $this->case_model->getAllCasesCount ( $lawyer_office_id, $user_type_code, $status, $search_data, $page, 0, null, 1, $search_query );
				}
				$this->response ( array (
						'message' => 'تم تحميل القضايا',
						'total_count' => $result_count,
						'cases' => $search_result,
						'success' => '1' 
				), 200 );
			} else
				$this->response ( array (
						'message' => 'لا يوجد قضايا',
						'success' => '1' 
				), 200 );
		} else {
			$this->response ( array (
					'message' => 'بعض البيانات مفقودة',
					'success' => '0' 
			), 200 );
		}
	}
	function edit_case_details_post() {
		if (isset ( $_POST ['user_id'] ) && isset ( $_POST ['case_id'] ) && isset ( $_POST ['customer_id'] ) && isset ( $_POST ['contract_number'] ) && isset ( $_POST ['debtor_name'] ) && isset ( $_POST ['client_id_number'] ) && isset ( $_POST ['client_type'] ) && isset ( $_POST ['remaining_amount'] ) && isset ( $_POST ['due_amount'] ) && isset ( $_POST ['number_of_late_instalments'] ) && isset ( $_POST ['total_debenture_amount'] ) && isset ( $_POST ['executive_order_number'] ) && isset ( $_POST ['order_number'] ) && isset ( $_POST ['circle_number'] )) {
			$user_id = $_POST ['user_id'];
			$case_id = $_POST ['case_id'];
			$where = "where user_id =" . $user_id;
			$this->load->model ( "common" );
			$userRecord = $this->common->getOneRow ( 'user', $where );
			$lawyer_office_id = $userRecord ['lawyer_office_id'];
			$user_type_code = $userRecord ['user_type_code'];
			$this->load->model ( "case_model" );
			
			$old_case_values = $this->case_model->getCaseDataForTransaction ( $case_id, $lawyer_office_id );
			$trans_values = array ();
			$trans_values ['case_id'] = $case_id;
			$trans_values ['lawyer_office_id'] = $lawyer_office_id;
			
			$value = array ();
			
			if ($_POST ['customer_id'] != $old_case_values ['customer_id']) {
				$value ['customer_id'] = $_POST ['customer_id'];
				$trans_values ['customer_id'] = $_POST ['customer_id'];
			}
			
			if ($_POST ['contract_number'] != $old_case_values ['contract_number']) {
				$value ['contract_number'] = $_POST ['contract_number'];
				$trans_values ['contract_number'] = $_POST ['contract_number'];
			}
			if ($_POST ['client_id_number'] != $old_case_values ['client_id_number']) {
				$value ['client_id_number'] = $_POST ['client_id_number'];
				$trans_values ['client_id_number'] = $_POST ['client_id_number'];
			}
			if (isset ( $_POST ['client_name'] ) && $_POST ['client_name'] != $old_case_values ['client_name']) {
				$value ['client_name'] = $_POST ['client_name'];
				$trans_values ['client_name'] = $_POST ['client_name'];
			}
			
			if ($_POST ['number_of_late_instalments'] != $old_case_values ['number_of_late_instalments']) {
				$value ['number_of_late_instalments'] = $_POST ['number_of_late_instalments'];
				$trans_values ['number_of_late_instalments'] = $_POST ['number_of_late_instalments'];
			}
			if ($_POST ['due_amount'] != $old_case_values ['due_amount']) {
				$value ['due_amount'] = $_POST ['due_amount'];
				$trans_values ['due_amount'] = $_POST ['due_amount'];
			}
			if ($_POST ['remaining_amount'] != $old_case_values ['remaining_amount']) {
				$value ['remaining_amount'] = $_POST ['remaining_amount'];
				$trans_values ['remaining_amount'] = $_POST ['remaining_amount'];
			}
			if ($_POST ['debtor_name'] != $old_case_values ['debtor_name']) {
				$value ['debtor_name'] = $_POST ['debtor_name'];
				$trans_values ['debtor_name'] = $_POST ['debtor_name'];
			}
			if (isset ( $_POST ['obligation_value'] ) && $_POST ['obligation_value'] != $old_case_values ['obligation_value']) {
				$value ['obligation_value'] = $_POST ['obligation_value'];
				$trans_values ['obligation_value'] = $_POST ['obligation_value'];
			}
			if ($_POST ['order_number'] != $old_case_values ['order_number']) {
				$value ['order_number'] = $_POST ['order_number'];
				$trans_values ['order_number'] = $_POST ['order_number'];
			}
			if ($_POST ['circle_number'] != $old_case_values ['circle_number']) {
				$value ['circle_number'] = $_POST ['circle_number'];
				$trans_values ['circle_number'] = $_POST ['circle_number'];
			}
			if ($_POST ['executive_order_number'] != $old_case_values ['executive_order_number']) {
				$value ['executive_order_number'] = $_POST ['executive_order_number'];
				$trans_values ['executive_order_number'] = $_POST ['executive_order_number'];
			}
			if (isset ( $_POST ['client_type'] ) && $_POST ['client_type'] != $old_case_values ['client_type']) {
				$value ['client_type'] = $_POST ['client_type'];
				$trans_values ['client_type'] = $_POST ['client_type'];
			}
			if ($_POST ['total_debenture_amount'] != $old_case_values ['total_debenture_amount']) {
				$value ['total_debenture_amount'] = $_POST ['total_debenture_amount'];
				$trans_values ['total_debenture_amount'] = $_POST ['total_debenture_amount'];
			}
			
			if (isset ( $_POST ['decision_34_number'] ) && $_POST ['decision_34_number'] != $old_case_values ['decision_34_number']) {
				$value ['decision_34_number'] = $_POST ['decision_34_number'];
				$trans_values ['decision_34_number'] = $_POST ['decision_34_number'];
			}
			
			if (isset ( $_POST ['debenture'] )) {
				$debenture = $_POST ['debenture'];
				/*
				 * $debenture_ext = $_POST ['debenture_ext'];
				 * $file_name = $_POST ['debenture_name'];
				 * $saved_path = self::upload_file ( $case_id, $debenture, $debenture_ext, $file_name, 'debenture' );
				 */
				$saved_path = self::move_from_tmp ( $case_id, $debenture, $lawyer_office_id );
				$value ['debenture'] = $saved_path;
				$trans_values ['debenture'] = $saved_path;
			}
			if (isset ( $_POST ['id'] )) {
				$id_file = $_POST ['id'];
				/*
				 * $id_file_ext = $_POST ['id_ext'];
				 * $file_name = $_POST ['id_name'];
				 * $saved_path = self::upload_file ( $case_id, $id_file, $id_file_ext, $file_name, 'id' );
				 */
				$saved_path = self::move_from_tmp ( $case_id, $id_file, $lawyer_office_id );
				$value ['id'] = $saved_path;
				$trans_values ['id'] = $saved_path;
			}
			if (isset ( $_POST ['contract'] )) {
				$contract = $_POST ['contract'];
				/*
				 * $contract_ext = $_POST ['contract_ext'];
				 * $file_name = $_POST ['contract_name'];
				 * $saved_path = self::upload_file ( $case_id, $contract, $contract_ext, $file_name, 'contract' );
				 */
				$saved_path = self::move_from_tmp ( $case_id, $contract, $lawyer_office_id );
				$value ['contract'] = $saved_path;
				$trans_values ['contract'] = $saved_path;
			}
			
			if (isset ( $_POST ['decision_34_file'] )) {
				$decision_34_file = $_POST ['decision_34_file'];
				/*
				 * $decision_34_file_ext = $_POST ['decision_34_file_ext'];
				 * $file_name = $_POST ['decision_34_file_name'];
				 * $saved_path = self::upload_file ( $case_id, $decision_34_file, $decision_34_file_ext, $file_name, 'decision_34_file' );
				 */
				$saved_path = self::move_from_tmp ( $case_id, $decision_34_file, $lawyer_office_id );
				$value ['decision_34_file'] = $saved_path;
				$trans_values ['decision_34_file'] = $saved_path;
			}
			if (isset ( $_POST ['advertisement_file'] )) {
				$advertisement_file = $_POST ['advertisement_file'];
				/*
				 * $advertisement_file_ext = $_POST ['advertisement_file_ext'];
				 * $file_name = $_POST ['advertisement_file_name'];
				 * $saved_path = self::upload_file ( $case_id, $advertisement_file, $advertisement_file_ext, $file_name, 'advertisement_file' );
				 */
				$saved_path = self::move_from_tmp ( $case_id, $advertisement_file, $lawyer_office_id );
				$value ['advertisement_file'] = $saved_path;
				$trans_values ['advertisement_file'] = $saved_path;
			}
			if (isset ( $_POST ['invoice_file'] )) {
				$invoice_file = $_POST ['invoice_file'];
				/*
				 * $invoice_file_ext = $_POST ['invoice_file_ext'];
				 * $file_name = $_POST ['invoice_file_name'];
				 * $saved_path = self::upload_file ( $case_id, $invoice_file, $invoice_file_ext, $file_name, 'invoice_file' );
				 */
				$saved_path = self::move_from_tmp ( $case_id, $invoice_file, $lawyer_office_id );
				$value ['invoice_file'] = $saved_path;
				$trans_values ['invoice_file'] = $saved_path;
			}
			if (isset ( $_POST ['referral_paper'] )) {
				$referral_paper = $_POST ['referral_paper'];
				/*
				 * $referral_paper_ext = $_POST ['referral_paper_ext'];
				 * $file_name = $_POST ['referral_paper_name'];
				 * $saved_path = self::upload_file ( $case_id, $referral_paper, $referral_paper_ext, $file_name, 'referral_paper' );
				 */
				$saved_path = self::move_from_tmp ( $case_id, $referral_paper, $lawyer_office_id );
				$value ['referral_paper'] = $saved_path;
				$trans_values ['referral_paper'] = $saved_path;
			}
			if (isset ( $_POST ['consignment_image'] )) {
				$consignment_image = $_POST ['consignment_image'];
				/*
				 * $consignment_image_ext = $_POST ['consignment_image_ext'];
				 * $file_name = $_POST ['consignment_image_name'];
				 * $saved_path = self::upload_file ( $case_id, $consignment_image, $consignment_image_ext, $file_name, 'consignment_image' );
				 */
				$saved_path = self::move_from_tmp ( $case_id, $consignment_image, $lawyer_office_id );
				$value ['consignment_image'] = $saved_path;
				$trans_values ['consignment_image'] = $saved_path;
			}
			if (isset ( $_POST ['suspend_file'] )) {
				$suspend_file = $_POST ['suspend_file'];
				/*
				 * $debenture_ext = $_POST ['debenture_ext'];
				 * $file_name = $_POST ['debenture_name'];
				 * $saved_path = self::upload_file ( $case_id, $debenture, $debenture_ext, $file_name, 'debenture' );
				 */
				$saved_path = self::move_from_tmp ( $case_id, $suspend_file, $lawyer_office_id );
				$value ['suspend_file'] = $saved_path;
				$trans_values ['suspend_file'] = $saved_path;
			}
			if (isset ( $_POST ['others_count'] )) {
				$others_count = $_POST ['others_count'];
				if ($others_count != null && $others_count > 0) {
					$others_value = ""; // $old_case_values ['others']; omneya told me that she delete old files and insert new only
					for($i = 1; $i <= $others_count; $i ++) {
						$item = "others_" . $i;
						if (isset ( $_POST [$item] )) {
							$other_file = $_POST [$item];
							/*
							 * $file_data = $_POST [$item];
							 * $file_ext = $_POST [$item . '_ext'];
							 * $file_name = $_POST [$item . '_name'];
							 * $saved_path = self::upload_file ( $case_id, $file_data, $file_ext, $file_name, $item );
							 */
							
							$saved_path = self::move_from_tmp ( $case_id, $other_file, $lawyer_office_id );
							$others_value = $others_value . "," . $saved_path;
						}
					}
					$others_value = ltrim ( $others_value, "," );
					$value ["others"] = $others_value;
					$trans_values ["others"] = $others_value;
				}
			}
			
			$this->load->library ( '../controllers/dates_conversion' );
			$decision_46_date = "";
			if ($_POST ['decision_46_date'] != null && $_POST ['decision_46_date'] != "" && $_POST ['decision_46_date'] != " ") {
				$decision_46_date = $this->dates_conversion->HijriToGregorian ( $_POST ['decision_46_date'] );
				if ($decision_46_date != $old_case_values ['decision_46_date']) {
					$value ['decision_46_date'] = $decision_46_date;
					$trans_values ['decision_46_date'] = $decision_46_date;
				}
			}
			$decision_34_date = "";
			if ($_POST ['decision_34_date'] != null && $_POST ['decision_34_date'] != "" && $_POST ['decision_34_date'] != " ") {
				$decision_34_date = $this->dates_conversion->HijriToGregorian ( $_POST ['decision_34_date'] );
				if ($decision_34_date != $old_case_values ['decision_34_date']) {
					$value ['decision_34_date'] = $decision_34_date;
					$trans_values ['decision_34_date'] = $decision_34_date;
				}
			}
			$executive_order_date = "";
			if ($_POST ['executive_order_date'] != null && $_POST ['executive_order_date'] != "" && $_POST ['executive_order_date'] != " ") {
				$executive_order_date = $this->dates_conversion->HijriToGregorian ( $_POST ['executive_order_date'] );
				if ($executive_order_date != $old_case_values ['executive_order_date']) {
					$value ['executive_order_date'] = $executive_order_date;
					$trans_values ['executive_order_date'] = $executive_order_date;
				}
			}
			$advertisement_date = "";
			if ($_POST ['advertisement_date'] != null && $_POST ['advertisement_date'] != "" && $_POST ['advertisement_date'] != " ") {
				$advertisement_date = $this->dates_conversion->HijriToGregorian ( $_POST ['advertisement_date'] );
				if ($advertisement_date != $old_case_values ['advertisement_date']) {
					$value ['advertisement_date'] = $advertisement_date;
					$trans_values ['advertisement_date'] = $advertisement_date;
				}
			}
			
			$suspend_date = "";
			if ($_POST ['suspend_date'] != null && $_POST ['suspend_date'] != "" && $_POST ['suspend_date'] != " ") {
				$suspend_date = $this->dates_conversion->HijriToGregorian ( $_POST ['suspend_date'] );
				if ($suspend_date != $old_case_values ['suspend_date']) {
					$value ['suspend_date'] = $suspend_date;
					$trans_values ['suspend_date'] = $suspend_date;
				}
			}
			
			if (isset ( $_POST ['closing_reason'] ) && $_POST ['closing_reason'] != $old_case_values ['closing_reason']) {
				$value ['closing_reason'] = $_POST ['closing_reason'];
				$trans_values ['closing_reason'] = $_POST ['closing_reason'];
			}
			if (isset ( $_POST ['suspend_reason_code'] ) && $_POST ['suspend_reason_code'] != $old_case_values ['suspend_reason_code']) {
				$value ['suspend_reason_code'] = $_POST ['suspend_reason_code'];
				$trans_values ['suspend_reason_code'] = $_POST ['suspend_reason_code'];
			}
			
			$value ['last_update_user'] = $user_id;
			$trans_values ['last_update_user'] = $value ['last_update_user'];
			date_default_timezone_set ( 'Asia/Riyadh' );
			$current_date_time = date ( "Y-m-d H:i:s" );
			$dt = new DateTime ( $current_date_time );
			$current_date = $dt->format ( 'Y-m-d' );
			$trans_values ['trans_date_time'] = $current_date_time;
			$value ['last_update_date'] = $current_date;
			$trans_values ['last_update_date'] = $value ['last_update_date'];
			if (isset ( $_POST ['notes'] )) {
				$value ['notes'] = $_POST ['notes'];
				$trans_values ['notes'] = $value ['notes'];
			}
			$lawyer_case_id = 1;
			
			$lawyer_case_record = $this->common->getOneRecField_Limit ( "lawyer_id", "lawyer_case", "where case_id = '$case_id' and lawyer_office_id = $lawyer_office_id" );
			if (isset ( $lawyer_case_record ) && $lawyer_case_record != null) {
				$trans_values ['lawyer_id'] = $lawyer_case_record->lawyer_id;
			}
			
			$trans_values ['edit_action'] = 'Y';
			
			$where = "case_id ='$case_id' and lawyer_office_id = $lawyer_office_id";
			$affected_rows = $this->common->updateRecord ( 'case', $value, $where );
			if ($affected_rows == 1) {
				self::insert_transaction ( $trans_values, "Success" );
				$activity = "تم التعديل فى القضية " . $case_id;
				self::insert_activity ( $user_id, $activity );
			} else {
				self::insert_transaction ( $trans_values, "Failure" );
			}
			self::getCaseDetails ( $user_type_code, $case_id, $lawyer_office_id, 3 );
		} else {
			$this->response ( array (
					'message' => 'بعض البيانات مفقودة',
					'success' => '0' 
			), 200 );
		}
	}
	function insert_transaction($values, $status) {
		$this->load->model ( "case_model" );
		
		$values ['transaction_id'] = $this->case_model->getTransactionId ();
		$values ['transaction_status'] = $status;
		if ($status == "Failure") {
			$values ['error_code'] = 1;
		}
		$trns = $this->common->insertRecord ( 'case_transactions', $values );
		return $trns;
	}
	function insert_activity($user_id, $activity) {
		$values = array ();
		$values ['user_id'] = $user_id;
		$values ['activity'] = $activity;
		$this->load->library ( '../controllers/util' );
		$values ['user_IP'] = $this->util->get_client_ip ();
		// $values['user_IP'] = self::get_client_ip();
		date_default_timezone_set ( 'Asia/Riyadh' );
		$current_date_time = date ( "Y-m-d H:i:s" );
		$values ['date_time'] = $current_date_time;
		$this->common->insertRecord ( 'user_activities', $values );
	}
	function do_action_post() {
		if (isset ( $_POST ['user_id'] ) && isset ( $_POST ['case_id'] ) && isset ( $_POST ['action_code'] )) {
			$user_id = $_POST ['user_id'];
			$case_id = $_POST ['case_id'];
			$action_code = $_POST ['action_code'];
			$this->load->model ( "common" ); // ////
			
			$where = "where user_id =" . $user_id;
			$userRecord = $this->common->getOneRow ( 'user', $where );
			$user_type_code = $userRecord ['user_type_code'];
			
			$lawyer_office_id = $userRecord ['lawyer_office_id'];
			$user_type_code = $userRecord ['user_type_code'];
			$this->load->model ( "case_model" );
			
			$old_case_values = $this->case_model->getCaseDataForTransaction ( $case_id, $lawyer_office_id );
			$trans_values = array ();
			$trans_values ['case_id'] = $case_id;
			$trans_values ['lawyer_office_id'] = $lawyer_office_id;
			$lawyer_case_record = $this->common->getOneRecField_Limit ( "lawyer_id", "lawyer_case", "where case_id ='$case_id' and lawyer_office_id = $lawyer_office_id " );
			if (isset ( $lawyer_case_record ) && $lawyer_case_record != null) {
				$trans_values ['lawyer_id'] = $lawyer_case_record->lawyer_id;
			}
			$lawyer_case_id = 1;
			if ($action_code != 'SUSPEND_REQUEST' && $action_code != 'RESUME_REQUEST') {
				if ($action_code == 'ASSIGN_TO_LAWYER') {
					// ASSIGN_TO_LAWYER اسناد إلى محامي
					if (isset ( $_POST ['lawyer_id'] ) && isset ( $_POST ['region_id'] )) {
						$lawyer_case_id = $this->common->insertRecord ( "lawyer_case", array (
								"lawyer_id" => $_POST ['lawyer_id'],
								"case_id" => $case_id,
								"lawyer_office_id" => $lawyer_office_id 
						) );
						$trans_values ['lawyer_id'] = $_POST ['lawyer_id'];
						$trans_values ['region_id'] = $_POST ['region_id'];
						$value ['region_id'] = $_POST ['region_id'];
						if (isset ( $_POST ['consignment_image'] )) {
							// save الصورة الارسالية
							/*
							 * $consignment_file = $_POST ['consignment_image'];
							 * $consignment_file_ext = $_POST ['consignment_image_ext'];
							 * $file_name = $_POST ['consignment_image_name'];
							 * $saved_path = self::upload_file ( $case_id, $consignment_file, $consignment_file_ext, $file_name, 'consignment_image' );
							 * $value ['consignment_image'] = $saved_path;
							 * $trans_values ['consignment_image'] = $saved_path;
							 */
							$consignment_image = $_POST ['consignment_image'];
							$saved_path = self::move_from_tmp ( $case_id, $consignment_image, $lawyer_office_id );
							$value ['consignment_image'] = $saved_path;
							$trans_values ['consignment_image'] = $saved_path;
						}
					} else
						$this->response ( array (
								'message' => 'بعض البيانات مفقودة',
								'success' => '0' 
						), 200 );
				} elseif ($action_code == 'NEED_MODIFICATIONS') {
					// NEED_MODIFICATIONS مطلوب تعديل
					if (isset ( $_POST ['notes'] )) {
						$value ['notes'] = $_POST ['notes'];
						$trans_values ['notes'] = $_POST ['notes'];
					} else
						$this->response ( array (
								'message' => 'بعض البيانات مفقودة',
								'success' => '0' 
						), 200 );
				} elseif ($action_code == 'MODIFICATIONS_DONE') {
					// MODIFICATIONS_DONE تم التعديل المطلوب
					if (isset ( $_POST ['contract_number'] ) && isset ( $_POST ['debtor_name'] ) && isset ( $_POST ['client_id_number'] ) && isset ( $_POST ['remaining_amount'] ) && isset ( $_POST ['due_amount'] ) && isset ( $_POST ['number_of_late_instalments'] ) && isset ( $_POST ['total_debenture_amount'] )) {
						if ($old_case_values ['contract_number'] != $_POST ['contract_number']) {
							$value ['contract_number'] = $_POST ['contract_number'];
							$trans_values ['contract_number'] = $_POST ['contract_number'];
						}
						if ($old_case_values ['debtor_name'] != $_POST ['debtor_name']) {
							$value ['debtor_name'] = $_POST ['debtor_name'];
							$trans_values ['debtor_name'] = $_POST ['debtor_name'];
						}
						if ($old_case_values ['client_id_number'] != $_POST ['client_id_number']) {
							$value ['client_id_number'] = $_POST ['client_id_number'];
							$trans_values ['client_id_number'] = $_POST ['client_id_number'];
						}
						if ($old_case_values ['remaining_amount'] != $_POST ['remaining_amount']) {
							$value ['remaining_amount'] = $_POST ['remaining_amount'];
							$trans_values ['remaining_amount'] = $_POST ['remaining_amount'];
						}
						if ($old_case_values ['due_amount'] != $_POST ['due_amount']) {
							$value ['due_amount'] = $_POST ['due_amount'];
							$trans_values ['due_amount'] = $_POST ['due_amount'];
						}
						if ($old_case_values ['number_of_late_instalments'] != $_POST ['number_of_late_instalments']) {
							$value ['number_of_late_instalments'] = $_POST ['number_of_late_instalments'];
							$trans_values ['number_of_late_instalments'] = $_POST ['number_of_late_instalments'];
						}
						if ($old_case_values ['total_debenture_amount'] != $_POST ['total_debenture_amount']) {
							$value ['total_debenture_amount'] = $_POST ['total_debenture_amount'];
							$trans_values ['total_debenture_amount'] = $_POST ['total_debenture_amount'];
						}
						
						if (isset ( $_POST ['client_name'] ) && $old_case_values ['client_name'] != $_POST ['client_name']) {
							$value ['client_name'] = $_POST ['client_name'];
							$trans_values ['client_name'] = $_POST ['client_name'];
						}
						if (isset ( $_POST ['obligation_value'] ) && $old_case_values ['obligation_value'] != $_POST ['obligation_value']) {
							$value ['obligation_value'] = $_POST ['obligation_value'];
							$trans_values ['obligation_value'] = $_POST ['obligation_value'];
						}
					} else
						$this->response ( array (
								'message' => 'بعض البيانات مفقودة',
								'success' => '0' 
						), 200 );
				} elseif ($action_code == 'REGISTER') {
					// REGISTER تقييد القضية
					if (isset ( $_POST ['executive_order_number'] ) && isset ( $_POST ['executive_order_date'] ) && isset ( $_POST ['circle_number'] ) && isset ( $_POST ['order_number'] )) {
						$value ['executive_order_number'] = $_POST ['executive_order_number'];
						$trans_values ['executive_order_number'] = $_POST ['executive_order_number'];
						$this->load->library ( '../controllers/dates_conversion' );
						$executive_order_g_date = "";
						$executive_order_g_date = $this->dates_conversion->HijriToGregorian ( $_POST ['executive_order_date'] );
						$value ['executive_order_date'] = $executive_order_g_date;
						$trans_values ['executive_order_date'] = $executive_order_g_date;
						
						$value ['circle_number'] = $_POST ['circle_number'];
						$trans_values ['circle_number'] = $_POST ['circle_number'];
						
						$value ['order_number'] = $_POST ['order_number'];
						$trans_values ['order_number'] = $_POST ['order_number'];
						
						if (isset ( $_POST ['referral_paper'] )) {
							$referral_paper = $_POST ['referral_paper'];
							$saved_path = self::move_from_tmp ( $case_id, $referral_paper, $lawyer_office_id );
							$value ['referral_paper'] = $saved_path;
							$trans_values ['referral_paper'] = $saved_path;
						}
					} else
						$this->response ( array (
								'message' => 'بعض البيانات مفقودة',
								'success' => '0' 
						), 200 );
				} elseif ($action_code == 'MAKE_DECISION_34') {
					// MAKE_DECISION_34 قرار 34
					$this->load->library ( '../controllers/dates_conversion' );
					if (isset ( $_POST ['decision_34_date'] )) {
						$decision_34_g_date = $this->dates_conversion->HijriToGregorian ( $_POST ['decision_34_date'] );
						$value ['decision_34_date'] = $decision_34_g_date;
						$trans_values ['decision_34_date'] = $decision_34_g_date;
						if (isset ( $_POST ['decision_34_number'] )) {
							$value ['decision_34_number'] = $_POST ['decision_34_number'];
							$trans_values ['decision_34_number'] = $_POST ['decision_34_number'];
						}
					} else
						$this->response ( array (
								'message' => 'بعض البيانات مفقودة',
								'success' => '0' 
						), 200 );
				} elseif ($action_code == 'ADVERTISE') {
					// ADVERTISE الإعلان
					if (isset ( $_POST ['invoice_file'] ) && isset ( $_POST ['advertisement_date'] ) && isset ( $_POST ['invoice_file'] )) {
						$this->load->library ( '../controllers/dates_conversion' );
						$advertisement_g_date = $this->dates_conversion->HijriToGregorian ( $_POST ['advertisement_date'] );
						$value ['advertisement_date'] = $advertisement_g_date;
						$trans_values ['advertisement_date'] = $advertisement_g_date;
						
						/*
						 * $invoice_file = $_POST ['invoice_file'];
						 * $invoice_file_ext = $_POST ['invoice_file_ext'];
						 * $file_name = $_POST ['invoice_file_name'];
						 * $saved_path = self::upload_file ( $case_id, $invoice_file, $invoice_file_ext, $file_name, 'invoice_file' );
						 * $value ['invoice_file'] = $saved_path;
						 * $trans_values ['invoice_file'] = $saved_path;
						 */
						$invoice_file = $_POST ['invoice_file'];
						$saved_path = self::move_from_tmp ( $case_id, $invoice_file, $lawyer_office_id );
						$value ['invoice_file'] = $saved_path;
						$trans_values ['invoice_file'] = $saved_path;
						
						if (isset ( $_POST ['advertisement_file'] )) {
							/*
							 * $advertisement_file = $_POST ['advertisement_file'];
							 * $advertisement_file_ext = $_POST ['advertisement_file_ext'];
							 * $advertisement_file_name = $_POST ['advertisement_file_name'];
							 * $advertisement_file_saved_path = self::upload_file ( $case_id, $advertisement_file, $advertisement_file_ext, $advertisement_file_name, 'advertisement_file' );
							 * $value ['advertisement_file'] = $advertisement_file_saved_path;
							 * $trans_values ['advertisement_file'] = $advertisement_file_saved_path;
							 */
							$advertisement_file = $_POST ['advertisement_file'];
							$saved_path = self::move_from_tmp ( $case_id, $advertisement_file, $lawyer_office_id );
							$value ['advertisement_file'] = $saved_path;
							$trans_values ['advertisement_file'] = $saved_path;
						}
					} else
						$this->response ( array (
								'message' => 'بعض البيانات مفقودة',
								'success' => '0' 
						), 200 );
				} elseif ($action_code == 'MAKE_DECISION_46') {
					// MAKE_DECISION_46 قرار 46
					$this->load->library ( '../controllers/dates_conversion' );
					if (isset ( $_POST ['decision_46_date'] )) {
						$decision_46_g_date = $this->dates_conversion->HijriToGregorian ( $_POST ['decision_46_date'] );
						$value ['decision_46_date'] = $decision_46_g_date;
						$trans_values ['decision_46_date'] = $decision_46_g_date;
					} else
						$this->response ( array (
								'message' => 'بعض البيانات مفقودة',
								'success' => '0' 
						), 200 );
				} elseif ($action_code == 'CLOSE') {
					// CLOSE إنهاء
					if (isset ( $_POST ['closing_reason'] )) {
						$source_state = $old_case_values ['current_state_code'];
						$value ['last_state_code'] = $source_state;
						$trans_values ['last_state_code'] = $source_state;
						$value ['closing_reason'] = $_POST ['closing_reason'];
						$trans_values ['closing_reason'] = $_POST ['closing_reason'];
					} else
						$this->response ( array (
								'message' => 'بعض البيانات مفقودة',
								'success' => '0' 
						), 200 );
				} elseif ($action_code == 'REASSIGN_TO_LAWYER') {
					// REASSIGN_TO_LAWYER تغيير المحامي
					if (isset ( $_POST ['reassigned_lawyer_id'] ) && isset ( $_POST ['region_id'] )) {
						$lawyer_case_data = array ();
						$lawyer_case_data ['lawyer_id'] = $_POST ['reassigned_lawyer_id'];
						$where = "case_id ='$case_id' and lawyer_office_id = $lawyer_office_id";
						$this->common->updateRecord ( 'lawyer_case', $lawyer_case_data, $where );
						$lawyer_case_record = $this->common->getOneRecField_Limit ( "id", "lawyer_case", "where case_id ='$case_id' and lawyer_office_id = $lawyer_office_id" );
						$lawyer_case_id = $lawyer_case_record->id;
						$trans_values ['lawyer_id'] = $_POST ['reassigned_lawyer_id'];
						$trans_values ['region_id'] = $_POST ['region_id'];
						$value ['region_id'] = $_POST ['region_id'];
					} else
						$this->response ( array (
								'message' => 'بعض البيانات مفقودة',
								'success' => '0' 
						), 200 );
				} elseif ($action_code == 'SUSPEND') {
					// SUSPEND امهال القضية
					if (isset ( $_POST ['suspend_reason'] )) {
						$source_state = $old_case_values ['current_state_code'];
						$value ['last_state_code'] = $source_state;
						$trans_values ['last_state_code'] = $source_state;
						$value ['suspend_reason_code'] = $_POST ['suspend_reason'];
						$trans_values ['suspend_reason_code'] = $_POST ['suspend_reason'];
						if (isset ( $_POST ['suspend_date'] )) {
							$this->load->library ( '../controllers/dates_conversion' );
							$suspend_date = $this->dates_conversion->HijriToGregorian ( $_POST ['suspend_date'] );
							$value ['suspend_date'] = $suspend_date;
							$trans_values ['suspend_date'] = $suspend_date;
						} else {
							date_default_timezone_set ( 'Asia/Riyadh' );
							$current_date_time = date ( "Y-m-d H:i:s" );
							$dt = new DateTime ( $current_date_time );
							$current_date = $dt->format ( 'Y-m-d' );
							$value ['suspend_date'] = $current_date;
							$trans_values ['suspend_date'] = $current_date;
						}
						if (isset ( $_POST ['suspend_file'] )) {
							/*
							 * $advertisement_file = $_POST ['advertisement_file'];
							 * $advertisement_file_ext = $_POST ['advertisement_file_ext'];
							 * $advertisement_file_name = $_POST ['advertisement_file_name'];
							 * $advertisement_file_saved_path = self::upload_file ( $case_id, $advertisement_file, $advertisement_file_ext, $advertisement_file_name, 'advertisement_file' );
							 * $value ['advertisement_file'] = $advertisement_file_saved_path;
							 * $trans_values ['advertisement_file'] = $advertisement_file_saved_path;
							 */
							$suspend_file = $_POST ['suspend_file'];
							$saved_path = self::move_from_tmp ( $case_id, $suspend_file, $lawyer_office_id );
							$value ['suspend_file'] = $saved_path;
							$trans_values ['suspend_file'] = $saved_path;
						}
					} else
						$this->response ( array (
								'message' => 'بعض البيانات مفقودة',
								'success' => '0' 
						), 200 );
				}/*  elseif ($action_code == 'RESUME') {
					// RESUME استكمال القضية
					$value ['current_state_code'] = $old_case_values ['last_state_code'];
					if ($old_case_values ['last_state_code'] == NULL || $old_case_values ['last_state_code'] == null || $old_case_values ['last_state_code'] == "") {
						$this->load->model ( "case_model" );
						$value ['current_state_code'] = $this->case_model->getCaseLastStateFromTransactions ( $case_id, $lawyer_office_id );
						if ($value ['current_state_code'] == "") {
							$value ['current_state_code'] = "DECISION_46";
						}
					}
				} elseif ($action_code == 'REOPEN') {
					// REOPEN إعادة فتح القضية
					$value ['current_state_code'] = $old_case_values ['last_state_code'];
					if ($old_case_values ['last_state_code'] == NULL || $old_case_values ['last_state_code'] == null || $old_case_values ['last_state_code'] == "") {
						$this->load->model ( "case_model" );
						$value ['current_state_code'] = $this->case_model->getCaseLastStateFromTransactions ( $case_id, $lawyer_office_id );
						if ($value ['current_state_code'] == "") {
							$value ['current_state_code'] = "DECISION_46";
						}
					}
				} */
				$source_state = $old_case_values ['current_state_code'];
				$newStateRecord = $this->common->getOneRecField_Limit ( "target_state", "state_transition", "where action_code ='$action_code' AND source_state= '$source_state'" );
				// not resume case
				if (isset ( $newStateRecord->target_state ) && $newStateRecord->target_state != NULL && $newStateRecord->target_state != "" && $newStateRecord->target_state != null) {
					$value ['current_state_code'] = $newStateRecord->target_state;
				} else if($action_code == "REOPEN" || $action_code == "RESUME"){
					if(isset ( $_POST ['return_date'] )){
						$this->load->library ( '../controllers/dates_conversion' );
						$return_date = "";
						$return_date = $this->dates_conversion->HijriToGregorian ( $_POST ['return_date'] );
					// incase of resume AND REOPEN
					$value ['current_state_code'] = $old_case_values ['last_state_code'];//=========
					if ($old_case_values ['last_state_code'] == NULL || $old_case_values ['last_state_code'] == null || $old_case_values ['last_state_code'] == "") {
						$value ['current_state_code'] = $this->case_model->getCaseLastStateFromTransactions ( $case_id, $lawyer_office_id );
						if ($value ['current_state_code'] == "") {
							$value ['current_state_code'] = "DECISION_46";
						}
					}
					switch ($value ['current_state_code']) {
						case "UNDER_REVIEW" :
							$value ['last_update_date'] = $return_date;
							$trans_values ['last_update_date'] = $return_date;
							break;
						case "REGISTERED" :
							$value ['executive_order_date'] = $return_date;
							$trans_values ['executive_order_date'] = $return_date;
							break;
						case "DECISION_34" :
							$value ['decision_34_date'] = $return_date;
							$trans_values ['decision_34_date'] = $return_date;
							break;
						case "PUBLICLY_ADVERTISED" :
							$value ['advertisement_date'] = $return_date;
							$trans_values ['advertisement_date'] = $return_date;
							break;
						case "DECISION_46" :
							$value ['decision_46_date'] = $return_date;
							$trans_values ['decision_46_date'] = $return_date;
							break;
						default :
							$value ['last_update_date'] = $return_date;
							$trans_values ['last_update_date'] = $return_date;
							break;
					}
					}else $this->response ( array (
							'message' => 'بعض البيانات مفقودة',
							'success' => '0'
					), 200 );
				} else {
					// case of current is CLOSED or SUSPENDED and the action is other than REOPEN and RESUME
					$last_trns = $this->case_model->getTransactionId ("current_state_code != '".$source_state."' and case_id = '" . $case_id . "' AND lawyer_office_id = '" . $lawyer_office_id . "' AND transaction_status = 'Success' AND (action_code IS NULL OR action_code != 'REASSIGN_TO_LAWYER') AND edit_action = 'N'");
					$last_state =  $last_trns["current_state_code"];
					$newStateRecord = $this->common->getOneRecField_Limit ( "target_state", "state_transition", "where action_code ='$action_code' AND source_state= '" . $last_state . "'" );
					$value ['current_state_code'] = $newStateRecord->target_state;
				}
				$trans_values ['current_state_code'] = $value ['current_state_code'];
				$value ['last_update_user'] = $user_id;
				$trans_values ['last_update_user'] = $user_id;
				date_default_timezone_set ( 'Asia/Riyadh' );
				$current_date_time = date ( "Y-m-d H:i:s" );
				$dt = new DateTime ( $current_date_time );
				$current_date = $dt->format ( 'Y-m-d' );
				$trans_values ['trans_date_time'] = $current_date_time;
				$value ['last_update_date'] = $current_date;
				$trans_values ['last_update_date'] = $current_date;
				$trans_values ['action_code'] = $action_code;
				
				// update case
				$where = "case_id ='$case_id' and lawyer_office_id = $lawyer_office_id";
				$affected_rows = $this->common->updateRecord ( 'case', $value, $where );
				
				// insert transaction and invoice
				
				$ignoreCreatingInvoices = 0;
				if ($action_code == "REASSIGN_TO_LAWYER" || $action_code == 'RESUME' || $action_code == 'REOPEN') {
					$ignoreCreatingInvoices = 1;
				}
				$ignoreCreatingLawyerInvoice = 0;
				if ($user_type_code != 'LAWYER') {
					$ignoreCreatingLawyerInvoice = 1;
				}
				$where = "case_id ='$case_id' and lawyer_office_id = $lawyer_office_id";
				
				if (($affected_rows == 1 || $action_code == "REASSIGN_TO_LAWYER") && ($lawyer_case_id != null && $lawyer_case_id > 0)) {
					self::insert_transaction ( $trans_values, "Success" );
					if ($ignoreCreatingInvoices == 0) {
						if (isset ( $value ['region_id'] ))
							$region = $value ['region_id'];
						else
							$region = $old_case_values ['region_id'];
						
						$this->case_model->create_invoice ( $trans_values, $old_case_values, $region, $ignoreCreatingLawyerInvoice );
						
						// self::create_invoice ( $trans_values, $old_case_values, $ignoreCreatingLawyerInvoice );
					}
				} else {
					self::insert_transaction ( $trans_values, "Failure" );
				}
			} else { // ($action_code == 'SUSPEND_REQUEST' || $action_code == 'RESUME_REQUEST')
				date_default_timezone_set ( 'Asia/Riyadh' );
				$current_date_time = date ( "Y-m-d H:i:s" );
				$request_id = 0;
				if ($action_code == 'SUSPEND_REQUEST') {
					$request_values ['request_type'] = 'S';
					if (isset ( $_POST ['req_suspend_reason_code'] )) {
						$request_values ['suspend_reason_code'] = $_POST ['req_suspend_reason_code'];
						$request_values ['notes'] = $_POST ['suspend_notes'];
					} else {
						$this->response ( array (
								'message' => 'بعض البيانات مفقودة',
								'success' => '0' 
						), 200 );
					}
				} else {
					$request_values ['request_type'] = 'R';
					$request_values ['notes'] = $_POST ['resume_notes'];
				}
				// if (isset ( $request_id )) {
				$trans_val ['case_id'] = $case_id;
				$trans_val ['lawyer_office_id'] = $lawyer_office_id;
				if ($action_code == 'SUSPEND_REQUEST') {
					$trans_val ['suspend_reason_code'] = $_POST ['req_suspend_reason_code'];
					$trans_val ['notes'] = $_POST ['suspend_notes'];
				} else if ($action_code == 'RESUME_REQUEST')
					$trans_val ['notes'] = $_POST ['resume_notes'];
				$trans_val ['trans_date_time'] = $current_date_time;
				$trans_val ['customer_id'] = $user_id;
				
				if ($action_code == 'SUSPEND_REQUEST') {
					$trans_val ['action_code'] = 'SUSPEND_REQUEST';
				} else {
					$trans_val ['action_code'] = 'RESUME_REQUEST';
				}
				
				$trans_id = self::insert_transaction ( $trans_val, "Success" );
				// }
				$request_values ['case_id'] = $case_id;
				$request_values ['lawyer_office_id'] = $lawyer_office_id;
				$request_values ['request_date'] = $current_date_time;
				$request_values ['trans_id'] = $trans_id;
				$update_req ['request_done'] = 'Y'; // for other requests on the same case must set the request as DONE
				$this->common->updateRecord ( 'case_requests', $update_req, "case_id = '$case_id' and lawyer_office_id = $lawyer_office_id and request_done = 'N'" );
				$request_id = $this->common->insertRecord ( 'case_requests', $request_values );
				
				self::process_request_notification ( $request_id, $trans_id );
			}
			// return case details
			self::getCaseDetails ( $user_type_code, $case_id, $lawyer_office_id, 2 );
		} else {
			$this->response ( array (
					'message' => 'بعض البيانات مفقودة',
					'success' => '0' 
			), 200 );
		}
	}
	function do_action_file_post() {
		if (isset ( $_POST ['user_id'] ) && isset ( $_POST ['case_id'] ) && isset ( $_POST ['action_code'] )) {
			$user_id = $_POST ['user_id'];
			$case_id = $_POST ['case_id'];
			$action_code = $_POST ['action_code'];
			$this->load->model ( "common" ); // ////
			
			$where = "where user_id =" . $user_id;
			$userRecord = $this->common->getOneRow ( 'user', $where );
			$user_type_code = $userRecord ['user_type_code'];
			
			$lawyer_office_id = $userRecord ['lawyer_office_id'];
			$user_type_code = $userRecord ['user_type_code'];
			$this->load->model ( "case_model" );
			
			$old_case_values = $this->case_model->getCaseDataForTransaction ( $case_id, $lawyer_office_id );
			$trans_values = array ();
			$trans_values ['case_id'] = $case_id;
			$trans_values ['lawyer_office_id'] = $lawyer_office_id;
			$lawyer_case_record = $this->common->getOneRecField_Limit ( "lawyer_id", "lawyer_case", "where case_id ='$case_id' and lawyer_office_id = $lawyer_office_id " );
			if (isset ( $lawyer_case_record ) && $lawyer_case_record != null) {
				$trans_values ['lawyer_id'] = $lawyer_case_record->lawyer_id;
			}
			$lawyer_case_id = 1;
			if ($action_code != 'SUSPEND_REQUEST' && $action_code != 'RESUME_REQUEST') {
				if ($action_code == 'ASSIGN_TO_LAWYER') {
					// ASSIGN_TO_LAWYER اسناد إلى محامي
					if (isset ( $_POST ['lawyer_id'] ) && isset ( $_POST ['region_id'] )) {
						$lawyer_case_id = $this->common->insertRecord ( "lawyer_case", array (
								"lawyer_id" => $_POST ['lawyer_id'],
								"case_id" => $case_id,
								"lawyer_office_id" => $lawyer_office_id
						) );
						$trans_values ['lawyer_id'] = $_POST ['lawyer_id'];
						$trans_values ['region_id'] = $_POST ['region_id'];
						$value ['region_id'] = $_POST ['region_id'];
						if (isset ( $_POST ['consignment_image'] ) && isset ( $_POST ['consignment_image_ext'] ) && isset ( $_POST ['consignment_image_name'] )) {
							// save الصورة الارسالية
							$consignment_file = $_POST ['consignment_image'];
							$consignment_file_ext = $_POST ['consignment_image_ext'];
							$file_name = $_POST ['consignment_image_name'];
							$saved_path = self::upload_file ( $user_id, $case_id, $consignment_file, $consignment_file_ext, $file_name, 'consignment_image' );
							if ($saved_path == "EXT"){
								$this->response ( array (
										'message' => 'غير مسموح بهذا النوع من الملفات. مسموح فقط بالصور وملفات الpdf.',
										'success' => '0'
								), 200 );
							}
							$value ['consignment_image'] = $saved_path;
							$trans_values ['consignment_image'] = $saved_path;
							
						}
					} else
						$this->response ( array (
								'message' => 'بعض البيانات مفقودة',
								'success' => '0'
						), 200 );
				} elseif ($action_code == 'NEED_MODIFICATIONS') {
					// NEED_MODIFICATIONS مطلوب تعديل
					if (isset ( $_POST ['notes'] )) {
						$value ['notes'] = $_POST ['notes'];
						$trans_values ['notes'] = $_POST ['notes'];
					} else
						$this->response ( array (
								'message' => 'بعض البيانات مفقودة',
								'success' => '0'
						), 200 );
				} elseif ($action_code == 'MODIFICATIONS_DONE') {
					// MODIFICATIONS_DONE تم التعديل المطلوب
					if (isset ( $_POST ['contract_number'] ) && isset ( $_POST ['debtor_name'] ) && isset ( $_POST ['client_id_number'] ) && isset ( $_POST ['remaining_amount'] ) && isset ( $_POST ['due_amount'] ) && isset ( $_POST ['number_of_late_instalments'] ) && isset ( $_POST ['total_debenture_amount'] )) {
						if ($old_case_values ['contract_number'] != $_POST ['contract_number']) {
							$value ['contract_number'] = $_POST ['contract_number'];
							$trans_values ['contract_number'] = $_POST ['contract_number'];
						}
						if ($old_case_values ['debtor_name'] != $_POST ['debtor_name']) {
							$value ['debtor_name'] = $_POST ['debtor_name'];
							$trans_values ['debtor_name'] = $_POST ['debtor_name'];
						}
						if ($old_case_values ['client_id_number'] != $_POST ['client_id_number']) {
							$value ['client_id_number'] = $_POST ['client_id_number'];
							$trans_values ['client_id_number'] = $_POST ['client_id_number'];
						}
						if ($old_case_values ['remaining_amount'] != $_POST ['remaining_amount']) {
							$value ['remaining_amount'] = $_POST ['remaining_amount'];
							$trans_values ['remaining_amount'] = $_POST ['remaining_amount'];
						}
						if ($old_case_values ['due_amount'] != $_POST ['due_amount']) {
							$value ['due_amount'] = $_POST ['due_amount'];
							$trans_values ['due_amount'] = $_POST ['due_amount'];
						}
						if ($old_case_values ['number_of_late_instalments'] != $_POST ['number_of_late_instalments']) {
							$value ['number_of_late_instalments'] = $_POST ['number_of_late_instalments'];
							$trans_values ['number_of_late_instalments'] = $_POST ['number_of_late_instalments'];
						}
						if ($old_case_values ['total_debenture_amount'] != $_POST ['total_debenture_amount']) {
							$value ['total_debenture_amount'] = $_POST ['total_debenture_amount'];
							$trans_values ['total_debenture_amount'] = $_POST ['total_debenture_amount'];
						}
						
						if (isset ( $_POST ['client_name'] ) && $old_case_values ['client_name'] != $_POST ['client_name']) {
							$value ['client_name'] = $_POST ['client_name'];
							$trans_values ['client_name'] = $_POST ['client_name'];
						}
						if (isset ( $_POST ['obligation_value'] ) && $old_case_values ['obligation_value'] != $_POST ['obligation_value']) {
							$value ['obligation_value'] = $_POST ['obligation_value'];
							$trans_values ['obligation_value'] = $_POST ['obligation_value'];
						}
					} else
						$this->response ( array (
								'message' => 'بعض البيانات مفقودة',
								'success' => '0'
						), 200 );
				} elseif ($action_code == 'REGISTER') {
					// REGISTER تقييد القضية
					if (isset ( $_POST ['executive_order_number'] ) && isset ( $_POST ['executive_order_date'] ) && isset ( $_POST ['circle_number'] ) && isset ( $_POST ['order_number'] )) {
						$value ['executive_order_number'] = $_POST ['executive_order_number'];
						$trans_values ['executive_order_number'] = $_POST ['executive_order_number'];
						$this->load->library ( '../controllers/dates_conversion' );
						$executive_order_g_date = "";
						$executive_order_g_date = $this->dates_conversion->HijriToGregorian ( $_POST ['executive_order_date'] );
						$value ['executive_order_date'] = $executive_order_g_date;
						$trans_values ['executive_order_date'] = $executive_order_g_date;
						
						$value ['circle_number'] = $_POST ['circle_number'];
						$trans_values ['circle_number'] = $_POST ['circle_number'];
						
						$value ['order_number'] = $_POST ['order_number'];
						$trans_values ['order_number'] = $_POST ['order_number'];
						
						if (isset ( $_POST ['referral_paper'] ) && isset ( $_POST ['referral_paper_ext'] ) && isset ( $_POST ['referral_paper_name'] )) {
							$referral_paper_file = $_POST ['referral_paper'];
							$referral_paper_ext = $_POST ['referral_paper_ext'];
							$file_name = $_POST ['referral_paper_name'];
							$saved_path = self::upload_file ( $user_id, $case_id, $referral_paper_file, $referral_paper_ext, $file_name, 'referral_paper' );
							if ($saved_path == "EXT"){
								$this->response ( array (
										'message' => 'غير مسموح بهذا النوع من الملفات. مسموح فقط بالصور وملفات الpdf.',
										'success' => '0'
								), 200 );
							}
							$value ['referral_paper'] = $saved_path;
							$trans_values ['referral_paper'] = $saved_path;
						}
					} else
						$this->response ( array (
								'message' => 'بعض البيانات مفقودة',
								'success' => '0'
						), 200 );
				} elseif ($action_code == 'MAKE_DECISION_34') {
					// MAKE_DECISION_34 قرار 34
					$this->load->library ( '../controllers/dates_conversion' );
					if (isset ( $_POST ['decision_34_date'] )) {
						$decision_34_g_date = $this->dates_conversion->HijriToGregorian ( $_POST ['decision_34_date'] );
						$value ['decision_34_date'] = $decision_34_g_date;
						$trans_values ['decision_34_date'] = $decision_34_g_date;
						if (isset ( $_POST ['decision_34_number'] )) {
							$value ['decision_34_number'] = $_POST ['decision_34_number'];
							$trans_values ['decision_34_number'] = $_POST ['decision_34_number'];
						}
					} else
						$this->response ( array (
								'message' => 'بعض البيانات مفقودة',
								'success' => '0'
						), 200 );
				} elseif ($action_code == 'ADVERTISE') {
					// ADVERTISE الإعلان
					if (isset ( $_POST ['invoice_file'] ) && isset ( $_POST ['advertisement_date'] ) && isset ( $_POST ['invoice_file_ext'] ) && isset ( $_POST ['invoice_file_name'] )) {
						$this->load->library ( '../controllers/dates_conversion' );
						$advertisement_g_date = $this->dates_conversion->HijriToGregorian ( $_POST ['advertisement_date'] );
						$value ['advertisement_date'] = $advertisement_g_date;
						$trans_values ['advertisement_date'] = $advertisement_g_date;
						
						$invoice_file = $_POST ['invoice_file'];
						$invoice_file_ext = $_POST ['invoice_file_ext'];
						$file_name = $_POST ['invoice_file_name'];
						$saved_path = self::upload_file ( $user_id, $case_id, $invoice_file, $invoice_file_ext, $file_name, 'invoice_file' );
						if ($saved_path == "EXT"){
							$this->response ( array (
									'message' => 'غير مسموح بهذا النوع من الملفات. مسموح فقط بالصور وملفات الpdf.',
									'success' => '0'
							), 200 );
						}
						$value ['invoice_file'] = $saved_path;
						$trans_values ['invoice_file'] = $saved_path;
						
						if (isset ( $_POST ['advertisement_file'] ) && isset ( $_POST ['advertisement_file_ext'] ) && isset ( $_POST ['advertisement_file_name'] )) {
							
							$advertisement_file = $_POST ['advertisement_file'];
							$advertisement_file_ext = $_POST ['advertisement_file_ext'];
							$advertisement_file_name = $_POST ['advertisement_file_name'];
							$advertisement_file_saved_path = self::upload_file ( $user_id, $case_id, $advertisement_file, $advertisement_file_ext, $advertisement_file_name, 'advertisement_file' );
							if ($advertisement_file_saved_path == "EXT"){
								$this->response ( array (
										'message' => 'غير مسموح بهذا النوع من الملفات. مسموح فقط بالصور وملفات الpdf.',
										'success' => '0'
								), 200 );
							}
							$value ['advertisement_file'] = $advertisement_file_saved_path;
							$trans_values ['advertisement_file'] = $advertisement_file_saved_path;
						}
					} else
						$this->response ( array (
								'message' => 'بعض البيانات مفقودة',
								'success' => '0'
						), 200 );
				} elseif ($action_code == 'MAKE_DECISION_46') {
					// MAKE_DECISION_46 قرار 46
					$this->load->library ( '../controllers/dates_conversion' );
					if (isset ( $_POST ['decision_46_date'] )) {
						$decision_46_g_date = $this->dates_conversion->HijriToGregorian ( $_POST ['decision_46_date'] );
						$value ['decision_46_date'] = $decision_46_g_date;
						$trans_values ['decision_46_date'] = $decision_46_g_date;
					} else
						$this->response ( array (
								'message' => 'بعض البيانات مفقودة',
								'success' => '0'
						), 200 );
				} elseif ($action_code == 'CLOSE') {
					// CLOSE إنهاء
					if (isset ( $_POST ['closing_reason'] )) {
						$source_state = $old_case_values ['current_state_code'];
						$value ['last_state_code'] = $source_state;
						$trans_values ['last_state_code'] = $source_state;
						$value ['closing_reason'] = $_POST ['closing_reason'];
						$trans_values ['closing_reason'] = $_POST ['closing_reason'];
					} else
						$this->response ( array (
								'message' => 'بعض البيانات مفقودة',
								'success' => '0'
						), 200 );
				} elseif ($action_code == 'REASSIGN_TO_LAWYER') {
					// REASSIGN_TO_LAWYER تغيير المحامي
					if (isset ( $_POST ['reassigned_lawyer_id'] ) && isset ( $_POST ['region_id'] )) {
						$lawyer_case_data = array ();
						$lawyer_case_data ['lawyer_id'] = $_POST ['reassigned_lawyer_id'];
						$where = "case_id ='$case_id' and lawyer_office_id = $lawyer_office_id";
						$this->common->updateRecord ( 'lawyer_case', $lawyer_case_data, $where );
						$lawyer_case_record = $this->common->getOneRecField_Limit ( "id", "lawyer_case", "where case_id ='$case_id' and lawyer_office_id = $lawyer_office_id" );
						$lawyer_case_id = $lawyer_case_record->id;
						$trans_values ['lawyer_id'] = $_POST ['reassigned_lawyer_id'];
						$trans_values ['region_id'] = $_POST ['region_id'];
						$value ['region_id'] = $_POST ['region_id'];
					} else
						$this->response ( array (
								'message' => 'بعض البيانات مفقودة',
								'success' => '0'
						), 200 );
				} elseif ($action_code == 'SUSPEND') {
					// SUSPEND امهال القضية
					if (isset ( $_POST ['suspend_reason'] )) {
						$source_state = $old_case_values ['current_state_code'];
						$value ['last_state_code'] = $source_state;
						$trans_values ['last_state_code'] = $source_state;
						$value ['suspend_reason_code'] = $_POST ['suspend_reason'];
						$trans_values ['suspend_reason_code'] = $_POST ['suspend_reason'];
						if (isset ( $_POST ['suspend_date'] )) {
							$this->load->library ( '../controllers/dates_conversion' );
							$suspend_date = $this->dates_conversion->HijriToGregorian ( $_POST ['suspend_date'] );
							$value ['suspend_date'] = $suspend_date;
							$trans_values ['suspend_date'] = $suspend_date;
						} else {
							date_default_timezone_set ( 'Asia/Riyadh' );
							$current_date_time = date ( "Y-m-d H:i:s" );
							$dt = new DateTime ( $current_date_time );
							$current_date = $dt->format ( 'Y-m-d' );
							$value ['suspend_date'] = $current_date;
							$trans_values ['suspend_date'] = $current_date;
						}
						if (isset ( $_POST ['suspend_file'] )) {
							
							$suspend_file = $_POST ['suspend_file'];
							$suspend_file_ext = $_POST ['suspend_file_ext'];
							$suspend_file_name = $_POST ['suspend_file_name'];
							$suspend_file_saved_path = self::upload_file ( $user_id, $case_id, $suspend_file, $suspend_file_ext, $suspend_file_name, 'suspend_file' );
							if ($suspend_file_saved_path == "EXT"){
								$this->response ( array (
										'message' => 'غير مسموح بهذا النوع من الملفات. مسموح فقط بالصور وملفات الpdf.',
										'success' => '0'
								), 200 );
							}
							$value ['suspend_file'] = $suspend_file_saved_path;
							$trans_values ['suspend_file'] = $suspend_file_saved_path;
						}
					} else
						$this->response ( array (
								'message' => 'بعض البيانات مفقودة',
								'success' => '0'
						), 200 );
				} /* elseif ($action_code == 'RESUME') {
				// RESUME استكمال القضية
				$value ['current_state_code'] = $old_case_values ['last_state_code'];
				if ($old_case_values ['last_state_code'] == NULL || $old_case_values ['last_state_code'] == null || $old_case_values ['last_state_code'] == "") {
				$this->load->model ( "case_model" );
				$value ['current_state_code'] = $this->case_model->getCaseLastStateFromTransactions ( $case_id, $lawyer_office_id );
				if ($value ['current_state_code'] == "") {
				$value ['current_state_code'] = "DECISION_46";
				}
				}
				} elseif ($action_code == 'REOPEN') {
				// REOPEN إعادة فتح القضية
				$value ['current_state_code'] = $old_case_values ['last_state_code'];
				if ($old_case_values ['last_state_code'] == NULL || $old_case_values ['last_state_code'] == null || $old_case_values ['last_state_code'] == "") {
				$this->load->model ( "case_model" );
				$value ['current_state_code'] = $this->case_model->getCaseLastStateFromTransactions ( $case_id, $lawyer_office_id );
				if ($value ['current_state_code'] == "") {
				$value ['current_state_code'] = "DECISION_46";
				}
				}
				} */
				$source_state = $old_case_values ['current_state_code'];
				$newStateRecord = $this->common->getOneRecField_Limit ( "target_state", "state_transition", "where action_code ='$action_code' AND source_state= '$source_state'" );
				// not resume case
				if (isset ( $newStateRecord->target_state ) && $newStateRecord->target_state != NULL && $newStateRecord->target_state != "" && $newStateRecord->target_state != null) {
					$value ['current_state_code'] = $newStateRecord->target_state;
				} else if($action_code == "REOPEN" || $action_code == "RESUME"){
					if(isset ( $_POST ['return_date'] )){
						$this->load->library ( '../controllers/dates_conversion' );
						$return_date = "";
						$return_date = $this->dates_conversion->HijriToGregorian ( $_POST ['return_date'] );
						// incase of resume AND REOPEN
						$value ['current_state_code'] = $old_case_values ['last_state_code'];
						if ($old_case_values ['last_state_code'] == NULL || $old_case_values ['last_state_code'] == null || $old_case_values ['last_state_code'] == "") {
							$value ['current_state_code'] = $this->case_model->getCaseLastStateFromTransactions ( $case_id, $lawyer_office_id );
							if ($value ['current_state_code'] == "") {
								$value ['current_state_code'] = "DECISION_46";
							}
						}
						switch ($value ['current_state_code']) {
							case "UNDER_REVIEW" :
								$value ['last_update_date'] = $return_date;
								$trans_values ['last_update_date'] = $return_date;
								break;
							case "REGISTERED" :
								$value ['executive_order_date'] = $return_date;
								$trans_values ['executive_order_date'] = $return_date;
								break;
							case "DECISION_34" :
								$value ['decision_34_date'] = $return_date;
								$trans_values ['decision_34_date'] = $return_date;
								break;
							case "PUBLICLY_ADVERTISED" :
								$value ['advertisement_date'] = $return_date;
								$trans_values ['advertisement_date'] = $return_date;
								break;
							case "DECISION_46" :
								$value ['decision_46_date'] = $return_date;
								$trans_values ['decision_46_date'] = $return_date;
								break;
							default :
								$value ['last_update_date'] = $return_date;
								$trans_values ['last_update_date'] = $return_date;
								break;
						}
					}
				} else {
					// case of current is CLOSED or SUSPENDED and the action is other than REOPEN and RESUME
					$last_trns = $this->case_model->getTransactionId ("current_state_code != '".$source_state."' and case_id = '" . $case_id . "' AND lawyer_office_id = '" . $lawyer_office_id . "' AND transaction_status = 'Success' AND (action_code IS NULL OR action_code != 'REASSIGN_TO_LAWYER') AND edit_action = 'N'");
					$last_state =  $last_trns["current_state_code"];
					$newStateRecord = $this->common->getOneRecField_Limit ( "target_state", "state_transition", "where action_code ='$action_code' AND source_state= '" . $last_state . "'" );
					$value ['current_state_code'] = $newStateRecord->target_state;
				}
				$trans_values ['current_state_code'] = $value ['current_state_code'];
				$value ['last_update_user'] = $user_id;
				$trans_values ['last_update_user'] = $user_id;
				date_default_timezone_set ( 'Asia/Riyadh' );
				$current_date_time = date ( "Y-m-d H:i:s" );
				$dt = new DateTime ( $current_date_time );
				$current_date = $dt->format ( 'Y-m-d' );
				$trans_values ['trans_date_time'] = $current_date_time;
				$value ['last_update_date'] = $current_date;
				$trans_values ['last_update_date'] = $current_date;
				$trans_values ['action_code'] = $action_code;
				
				// update case
				$where = "case_id ='$case_id' and lawyer_office_id = $lawyer_office_id";
				$affected_rows = $this->common->updateRecord ( 'case', $value, $where );
				
				// insert transaction and invoice
				
				$ignoreCreatingInvoices = 0;
				if ($action_code == "REASSIGN_TO_LAWYER" || $action_code == 'RESUME' || $action_code == 'REOPEN') {
					$ignoreCreatingInvoices = 1;
				}
				$ignoreCreatingLawyerInvoice = 0;
				if ($user_type_code != 'LAWYER') {
					$ignoreCreatingLawyerInvoice = 1;
				}
				$where = "case_id ='$case_id' and lawyer_office_id = $lawyer_office_id";
				
				if (($affected_rows == 1 || $action_code == "REASSIGN_TO_LAWYER") && ($lawyer_case_id != null && $lawyer_case_id > 0)) {
					self::insert_transaction ( $trans_values, "Success" );
					if ($ignoreCreatingInvoices == 0) {
						if (isset ( $value ['region_id'] ))
							$region = $value ['region_id'];
							else
								$region = $old_case_values ['region_id'];
								
								$this->case_model->create_invoice ( $trans_values, $old_case_values, $region, $ignoreCreatingLawyerInvoice );
								
								// self::create_invoice ( $trans_values, $old_case_values, $ignoreCreatingLawyerInvoice );
					}
				} else {
					self::insert_transaction ( $trans_values, "Failure" );
				}
			} else {
				date_default_timezone_set ( 'Asia/Riyadh' );
				$current_date_time = date ( "Y-m-d H:i:s" );
				$request_id = 0;
				if ($action_code == 'SUSPEND_REQUEST') {
					$request_values ['request_type'] = 'S';
					if (isset ( $_POST ['req_suspend_reason_code'] )) {
						$request_values ['suspend_reason_code'] = $_POST ['req_suspend_reason_code'];
						$request_values ['notes'] = $_POST ['suspend_notes'];
					} else {
						$this->response ( array (
								'message' => 'بعض البيانات مفقودة',
								'success' => '0'
						), 200 );
					}
				} else {
					$request_values ['request_type'] = 'R';
					$request_values ['notes'] = $_POST ['resume_notes'];
				}
				// if (isset ( $request_id )) {
				$trans_val ['case_id'] = $case_id;
				$trans_val ['lawyer_office_id'] = $lawyer_office_id;
				if ($action_code == 'SUSPEND_REQUEST') {
					$trans_val ['suspend_reason_code'] = $_POST ['req_suspend_reason_code'];
					$trans_val ['notes'] = $_POST ['suspend_notes'];
				} else if ($action_code == 'RESUME_REQUEST')
					$trans_val ['notes'] = $_POST ['resume_notes'];
					$trans_val ['trans_date_time'] = $current_date_time;
					$trans_val ['customer_id'] = $user_id;
					
					if ($action_code == 'SUSPEND_REQUEST') {
						$trans_val ['action_code'] = 'SUSPEND_REQUEST';
					} else {
						$trans_val ['action_code'] = 'RESUME_REQUEST';
					}
					
					$trans_id = self::insert_transaction ( $trans_val, "Success" );
					// }
					$request_values ['case_id'] = $case_id;
					$request_values ['lawyer_office_id'] = $lawyer_office_id;
					$request_values ['request_date'] = $current_date_time;
					$request_values ['trans_id'] = $trans_id;
					$update_req ['request_done'] = 'Y'; // for other requests on the same case must set the request as DONE
					$this->common->updateRecord ( 'case_requests', $update_req, "case_id = '$case_id' and lawyer_office_id = $lawyer_office_id and request_done = 'N'" );
					$request_id = $this->common->insertRecord ( 'case_requests', $request_values );
					
					self::process_request_notification ( $request_id, $trans_id );
			}
			// return case details
			self::getCaseDetails ( $user_type_code, $case_id, $lawyer_office_id, 2 );
		} else {
			$this->response ( array (
					'message' => 'بعض البيانات مفقودة',
					'success' => '0'
			), 200 );
		}
	}
	function move_from_tmp($case_id, $from, $lawyer_office_id) {
		if (substr ( $from, 0, 1 ) == '/')
			$from = substr ( $from, 1 );
		
		if (strpos ( $from, 'cases_tmp_files' ) !== false) {
			/*
			 * if (! is_dir ( "./cases_files/" . $case_id )) {
			 * mkdir ( './cases_files/' . $case_id, 0777, true );
			 * copy ( './cases_files/index.html', './cases_files/' . $case_id . '/index.html' );
			 * }
			 */
			if (! is_dir ( "./cases_files/" . $lawyer_office_id . "/" . $case_id )) {
				mkdir ( './cases_files/' . $lawyer_office_id . "/" . $case_id, 0777, true );
				copy ( './cases_files/index.html', './cases_files/' . $lawyer_office_id . "/" . $case_id . '/index.html' );
			}
			$to = str_replace ( "cases_tmp_files", "cases_files", $from );
			if (copy ( FCPATH . $from, FCPATH . $to )) {
				unlink ( FCPATH . $from );
				return $to;
			} else
				return "";
		} else
			return $from;
	}
	function logToFile($msg) {
		$filename = 'log_import.txt';
		$fd = fopen ( $filename, "a" );
		$str = "[" . date ( "Y/m/d h:i:s", mktime () ) . "] " . $msg;
		fwrite ( $fd, $str . "\n" );
		fclose ( $fd );
	}
	/*
	 * function upload_file($case_id, $file, $file_ext, $file_name, $param_name) {
	 * if (! is_dir ( "./cases_files/" . $case_id )) {
	 * mkdir ( './cases_files/' . $case_id, 0777, true );
	 * copy ( './cases_files/index.html', './cases_files/' . $case_id . '/index.html' );
	 * }
	 * $file_data = base64_decode ( $file );
	 * $file_name = $param_name . "-" . str_replace ( ' ', '-', $file_name );
	 * $filepath = $file_name;
	 * $web_path = FCPATH . "/cases_files/" . $case_id . "/" . $file_name . "." . $file_ext;
	 * // $save = "./cases_files/".$case_id."/".$filepath;
	 * $done = file_put_contents ( $web_path, $file_data );
	 * return "cases_files/" . $case_id . "/" . $file_name . "." . $file_ext;
	 * }
	 */
	function create_invoice($trans_values, $old_case_values, $ignoreCreatingLawyerInvoice) {
		$case_id = $trans_values ['case_id'];
		$stage_code = "";
		switch ($trans_values ['current_state_code']) {
			case "REGISTERED" :
				$stage_code = "Registered";
				break;
			case "DECISION_34" :
				$stage_code = "Decision 34";
				break;
			case "PUBLICLY_ADVERTISED" :
				$stage_code = "Public Advertised";
				break;
			case "DECISION_46" :
				$stage_code = "Decision 46";
				break;
			case "CLOSED" :
				$stage_code = "Closed";
				break;
			default :
				$stage_code = "";
		}
		if ($stage_code != "") {
			$stageRecord = $this->common->getOneRecField_Limit ( "stage_order", "stages", "where stage_code ='$stage_code'" );
			$stage_order = $stageRecord->stage_order;
			
			$this->load->model ( "invoices_model" );
			$values ['invoice_id'] = $this->invoices_model->getInvoiceId ();
			$values ['invoice_date'] = $trans_values ['last_update_date'];
			$values ['customer_id'] = $old_case_values ['customer_id'];
			if (isset ( $trans_values ['customer_id'] ) && $trans_values ['customer_id'] != null && $trans_values ['customer_id'] != "") {
				$values ['customer_id'] = $trans_values ['customer_id'];
			}
			
			$lawyerRecord = $this->common->getOneRecField_Limit ( "lawyer_id", "lawyer_case", "where case_id ='$case_id' and lawyer_office_id = " . $lawyer_office_id );
			if ($lawyerRecord) {
				$values ['lawyer_id'] = $lawyerRecord->lawyer_id;
				$values ['case_id'] = $trans_values ['case_id'];
				$values ['case_state_code'] = $trans_values ['current_state_code'];
				$values ['billing_value'] = $this->invoices_model->getLawyerCost ( $stage_order, $values ['lawyer_id'] );
				$values ['is_paid'] = "N";
				$values ['invoice_user_id'] = $values ['lawyer_id'];
				
				$this->db->trans_begin ();
				
				if ($ignoreCreatingLawyerInvoice == 0) {
					$this->common->insertRecord ( 'invoices', $values );
				}
				$this->response ( array (
						'message' => 'بعض البيانات مفقودة',
						'success' => '0' 
				), 200 );
				
				$values ['invoice_id'] = $this->invoices_model->getInvoiceId ();
				$values ['billing_value'] = $this->invoices_model->getCustomerCost ( $stage_code, $values ['lawyer_id'], $values ['customer_id'] );
				$values ['invoice_user_id'] = $values ['customer_id'];
				
				$this->common->insertRecord ( 'invoices', $values );
				
				if ($this->db->trans_status () === FALSE) {
					$this->db->trans_rollback ();
				} else {
					$this->db->trans_commit ();
				}
			}
		}
	}
	function temp_post() {
		if (isset ( $_POST ['user_id'] )) {
			$this->load->model ( "common" );
			$user_id = $_POST ['user_id'];
			$where = "where user_id =" . $user_id;
			$userRecord = $this->common->getOneRow ( 'user', $where );
			$gcmId = $userRecord ['gcm_id'];
			$firebaseNotification = new firebase_notification ();
			date_default_timezone_set ( 'Asia/Riyadh' );
			$current_date = date ( "Y-m-d H:i:s" );
			$fb_message = array (
					"message" => "رسالة للتجربة فقط",
					"type" => "C00100",
					"date" => $current_date 
			);
			$jsonString = $firebaseNotification->sendPushNotificationToGCMSever ( $gcmId, 'التنفيذ', $fb_message );
			$this->response ( array (
					'message' => $jsonString,
					'success' => '0' 
			), 200 );
		} else
			$this->response ( array (
					'message' => 'بعض البيانات مفقودة',
					'success' => '0' 
			), 200 );
	}
	function get_accepted_file_types() {
		$this->load->model ( "case_model" );
		return $this->case_model->get_accepted_file_types();

	}
	function upload_file_post() {
		if (isset ( $_POST ['case_id'] ) && isset ( $_POST ['file_data'] ) && isset ( $_POST ['file_name'] ) && isset ( $_POST ['file_ext'] ) && isset ( $_POST ['file_type'] ) && isset ( $_POST ['user_id'] )) {
			$file_ext = $_POST ['file_ext'];
			$accepted_file_types = self::get_accepted_file_types ();
			if (! in_array ( strtolower ( $file_ext ), $accepted_file_types )) {
				$this->response ( array (
						'message' => 'غير مسموح بهذا النوع من الملفات. مسموح فقط بالصور وملفات الpdf.',
						'success' => '0' 
				), 200 );
			} else {
				$case_id = $_POST ['case_id'];
				$file = $_POST ['file_data'];
				$file_name = $_POST ['file_name'];
				$param_name = $_POST ['file_type'];
				$user_id = $_POST ['user_id'];
				$where = "where user_id =" . $user_id;
				$this->load->model ( "common" );
				$userRecord = $this->common->getOneRow ( 'user', $where );
				$lawyer_office_id = $userRecord ['lawyer_office_id'];
				if ($param_name == 'others')
					if (! isset ( $_POST ['others_count'] ))
						$this->response ( array (
								'message' => 'بعض البيانات مفقودة',
								'success' => '0' 
						), 200 );
					else
						$param_name = $param_name . $_POST ['others_count'];
				if (! is_dir ( "./cases_tmp_files/" . $lawyer_office_id )) {
					mkdir ( './cases_tmp_files/' . $lawyer_office_id, 0777, true );
					// copy ( './cases_files/index.html', './cases_files/' . $case_id . '/index.html' );
				}
				if (! is_dir ( "./cases_tmp_files/" . $lawyer_office_id . "/" . $case_id )) {
					mkdir ( './cases_tmp_files/' . $lawyer_office_id . '/' . $case_id, 0777, true );
				}
				$file_data = base64_decode ( $file );
				$file_name = $param_name . "-" . str_replace ( ' ', '-', $file_name );
				$filepath = $file_name;
				$web_path = FCPATH . "/cases_tmp_files/" . $lawyer_office_id . "/" . $case_id . "/" . $file_name . "." . $file_ext;
				// $save = "./cases_files/".$case_id."/".$filepath;
				$done = file_put_contents ( $web_path, $file_data );
				
				if ($done != FALSE) {
					
					$this->response ( array (
							'message' => 'تم التحميل بنجاح',
							'file_path' => "/cases_tmp_files/" . $lawyer_office_id . "/" . $case_id . "/" . $file_name . "." . $file_ext,
							'success' => '1' 
					), 200 );
				} else {
					$this->response ( array (
							'message' => 'حدث خطأ أثناء التحميل',
							'success' => '0' 
					), 200 );
				}
			}
		} else {
			$this->response ( array (
					'message' => 'بعض البيانات مفقودة',
					'success' => '0' 
			), 200 );
		}
	}
	function upload_progress_file_post() {
		if (isset ( $_POST ['case_id'] ) /* && isset ( $_POST ['file_data'] )  */&& isset ( $_POST ['file_name'] ) && isset ( $_POST ['file_ext'] ) && isset ( $_POST ['file_type'] ) && isset ( $_FILES ['temp'] ) && isset ( $_FILES ['user_id'] )) {
			$case_id = $_POST ['case_id'];
			// $file = $_POST ['file_data'];
			$file_name = $_POST ['file_name'];
			$file_ext = $_POST ['file_ext'];
			$accepted_file_types = self::get_accepted_file_types ();
			if (! in_array ( strtolower ( $file_ext ), $accepted_file_types )) {
				$this->response ( array (
						'message' => 'غير مسموح بهذا النوع من الملفات. مسموح فقط بالصور وملفات الpdf.',
						'success' => '0' 
				), 200 );
			} else {
				$param_name = $_POST ['file_type'];
				$user_id = $_POST ['file_type'];
				$where = "where user_id =" . $user_id;
				$this->load->model ( "common" );
				$userRecord = $this->common->getOneRow ( 'user', $where );
				$lawyer_office_id = $userRecord ['lawyer_office_id'];
				if ($param_name == 'others')
					if (! isset ( $_POST ['others_count'] ))
						$this->response ( array (
								'message' => 'بعض البيانات مفقودة',
								'success' => '0' 
						), 200 );
					else
						$param_name = $param_name . $_POST ['others_count'];
					
					/*
				 * if (! is_dir ( "./cases_tmp_files/" . $case_id )) {
				 * mkdir ( './cases_tmp_files/' . $case_id, 0777, true );
				 * // copy ( './cases_files/index.html', './cases_files/' . $case_id . '/index.html' );
				 * }
				 */
				if (! is_dir ( "./cases_files/" . $lawyer_office_id . "/" . $case_id )) {
					mkdir ( './cases_files/' . $lawyer_office_id . '/' . $case_id, 0777, true );
					copy ( './cases_files/index.html', './cases_files/' . $lawyer_office_id . '/' . $case_id . '/index.html' );
				}
				/*
				 * $file_data = base64_decode ( $file );
				 * $file_name = $param_name . "-" . str_replace ( ' ', '-', $file_name );
				 * $filepath = $file_name;
				 * $web_path = FCPATH . "/cases_tmp_files/" . $case_id . "/" . $file_name . "." . $file_ext;
				 * // $save = "./cases_files/".$case_id."/".$filepath;
				 * $done = file_put_contents ( $web_path, $file_data );
				 *
				 * if ($done != FALSE) {
				 */
				if (is_uploaded_file ( $_FILES ['temp'] ['tmp_name'] )) {
					
					$tmp_name = $_FILES ['temp'] ['tmp_name'];
					$pic_name = $_FILES ['temp'] ['name'];
					$source = $tmp_name;
					$file = $param_name . "-" . str_replace ( ' ', '-', $pic_name );
					$filepath = $file;
					$save = "./cases_tmp_files/" . $lawyer_office_id . '/' . $case_id . "/" . $filepath;
					move_uploaded_file ( $source, $save );
				} else {
					echo "File not uploaded successfully.";
				}
				$this->response ( array (
						'message' => 'تم حفظ الملف',
						'file_path' => $save,
						'success' => '1' 
				), 200 );
				/*
				 * } else {
				 * $this->response ( array (
				 * 'message' => 'حدث خطأ أثناء التحميل',
				 * 'success' => '0'
				 * ), 200 );
				 * }
				 */
			}
		} else {
			$this->response ( array (
					'message' => 'بعض البيانات مفقودة',
					'success' => '0' 
			), 200 );
		}
	}
	function upload_file($user_id, $case_id, $file, $file_ext, $file_name, $param_name) {
		$accepted_file_types = self::get_accepted_file_types ();
		if (! in_array ( strtolower ( $file_ext ), $accepted_file_types )) {
			return "EXT";
		} else {
		$where = "where user_id =" . $user_id;
		$this->load->model ( "common" );
		$userRecord = $this->common->getOneRow ( 'user', $where );
		$lawyer_office_id = $userRecord ['lawyer_office_id'];
		if (! is_dir ( "./cases_files/" . $lawyer_office_id )) {
			mkdir ( './cases_files/' . $lawyer_office_id, 0777, true );
			// copy ( './cases_files/index.html', './cases_files/' . $case_id . '/index.html' );
		}
		if (! is_dir ( "./cases_files/" . $lawyer_office_id . "/" . $case_id )) {
			mkdir ( './cases_files/' . $lawyer_office_id . '/' . $case_id, 0777, true );
		}
		$file_data = base64_decode ( $file );
		$file_name = $param_name . "-" . str_replace ( ' ', '-', $file_name );
		$filepath = $file_name;
		$web_path = FCPATH . "/cases_files/" . $lawyer_office_id . "/" . $case_id . "/" . $file_name . "." . $file_ext;
		// $save = "./cases_files/".$case_id."/".$filepath;
		$done = file_put_contents ( $web_path, $file_data );
		if ($done)
			return "/cases_files/" . $lawyer_office_id . "/" . $case_id . "/" . $file_name . "." . $file_ext;
		else
			return "";
		}
	}
	function delete_case_file_post() {
		if (isset ( $_POST ['case_id'] ) && isset ( $_POST ['file_type'] ) && isset ( $_POST ['user_id'] )) {
			$case_id = $_POST ['case_id'];
			// $file = $_POST ['file_data'];
			$file_type = $_POST ['file_type'];
			$this->load->model ( "case_model" );
			$user_id = $_POST ['user_id'];
			$where = "where user_id =" . $user_id;
			$this->load->model ( "common" );
			$userRecord = $this->common->getOneRow ( 'user', $where );
			$lawyer_office_id = $userRecord ['lawyer_office_id'];
			$old_case_values = $this->case_model->getCaseDataForTransaction ( $case_id, $lawyer_office_id );
			// $trans_values = array ();
			// $trans_values ['case_id'] = $case_id;
			$value [$file_type] = null;
			
			$where = "case_id ='$case_id' and lawyer_office_id = $lawyer_office_id";
			$affected_rows = $this->common->updateRecord ( 'case', $value, $where );
			if ($affected_rows > 0) {
				if ($file_type == 'others') {
					$files_paths = explode ( ',', $old_case_values ['others'] );
					foreach ( $files_paths as $path ) {
						unlink ( FCPATH . $path );
					}
				} else
					unlink ( FCPATH . $old_case_values [$file_type] );
				$this->response ( array (
						'message' => 'تم حذف الملف',
						'success' => '1' 
				), 200 );
			} else {
				$this->response ( array (
						'message' => 'لا يوجد قضيه',
						'success' => '0' 
				), 200 );
			}
		} else {
			$this->response ( array (
					'message' => 'بعض البيانات مفقودة',
					'success' => '0' 
			), 200 );
		}
	}
	function process_request_notification($case_request_id, $trans_id) {
		$this->load->model ( "notification_model" );
		$notification_data = $this->notification_model->get_case_request_notification_data ( $case_request_id );
		
		if ($notification_data && sizeof ( $notification_data ) > 0) {
			$records = $notification_data ['records'];
			$suspend_reason = $records ['suspend_reason'];
			$request_type = $records ['request_type'];
			$case_id = $records ['case_id'];
			$lawyer_office_id = $records ['lawyer_office_id'];
			$customer_first_name = $records ['customer_first_name'];
			$customer_last_name = $records ['customer_last_name'];
			$admin_email = $records ['admin_email'];
			$admin_mobile = $records ['admin_mobile'];
			$values = array ();
			$values ['case_id'] = $case_id;
			$values ['lawyer_office_id'] = $lawyer_office_id;
			$values ['notification_type_code'] = 'CASE_REQ';
			date_default_timezone_set ( 'Asia/Riyadh' );
			$current_date = date ( "Y-m-d H:i:s" );
			$values ['sent_date_time'] = $current_date;
			$values ['waiting_days'] = '0';
			
			$values ['trans_id'] = $trans_id;
			// admin (notif, email)
			$admin_records = $notification_data ['admin_record'];
			if ($admin_records && sizeof ( $admin_records ) > 0) {
				$admin_first_name = $admin_records ['admin_first_name'];
				$admin_last_name = $admin_records ['admin_last_name'];
				$admin_gcm_id = $admin_records ['admin_gcm_id'];
				$values ['to_user_id'] = $admin_records ['user_id'];
				$values ['notification_type_code'] = 'M008';
				$this->load->model ( "common" );
				$where = "where notification_code = 'M008'";
				$notRecord = $this->common->getOneRow ( "notifications", $where );
				self::send_notification_case_request ( $notRecord ['active'], $notRecord ['sms_active'], $admin_email, $admin_mobile, $admin_gcm_id, $admin_first_name, $admin_last_name, $case_id, $customer_first_name, $customer_last_name, $request_type, $suspend_reason, $values );
			}
			
			// lawyer (notif, email, mobile)
			$lawyer_records = $notification_data ['lawyer_record'];
			if ($lawyer_records && sizeof ( $lawyer_records )) {
				$lawyer_email = $lawyer_records ['lawyer_email'];
				$lawyer_first_name = $lawyer_records ['lawyer_first_name'];
				$lawyer_last_name = $lawyer_records ['lawyer_last_name'];
				$lawyer_gcm_id = $lawyer_records ['lawyer_gcm_id'];
				$lawyer_mobile = $lawyer_records ['lawyer_mobile'];
				$values ['to_user_id'] = $lawyer_records ['user_id'];
				$values ['notification_type_code'] = 'M009';
				
				$this->load->model ( "common" );
				$where = "where notification_code = 'M009'";
				$notRecord = $this->common->getOneRow ( "notifications", $where );
				self::send_notification_case_request ( $notRecord ['active'], $notRecord ['sms_active'], $lawyer_email, $lawyer_mobile, $lawyer_gcm_id, $lawyer_first_name, $lawyer_last_name, $case_id, $customer_first_name, $customer_last_name, $request_type, $suspend_reason, $values );
			}
			// secretaries
			
			$secretaries_records = $notification_data ['secretaries_emails'];
			if ($secretaries_records && sizeof ( $secretaries_records ) > 0) {
				foreach ( $secretaries_records as $secretary_email ) {
					$values ['to_user_id'] = $secretary_email ['secretary_id'];
					$values ['notification_type_code'] = 'M010';
					$this->load->model ( "common" );
					$where = "where notification_code = 'M010'";
					$notRecord = $this->common->getOneRow ( "notifications", $where );
					self::send_notification_case_request ( $notRecord ['active'], $notRecord ['sms_active'], $secretary_email ['secretary_email'], $secretary_email ['secretary_mobile'], $secretary_email ['secretary_gcm_id'], $secretary_email ['secretary_first_name'], $secretary_email ['secretary_last_name'], $case_id, $customer_first_name, $customer_last_name, $request_type, $suspend_reason, $values );
				}
			}
			
			return $notification_data;
		} else {
			return false;
		}
	}
	function send_email($email_active, $user_message, $subject, $to_user_email) {
		if ($email_active == "Y") {
			$this->load->library ( 'email' );
			$config ['mailtype'] = 'html';
			$config ['charset'] = 'utf-8';
			$this->email->initialize ( $config );
			
			$this->email->from ( "info@altanfeeth.com", 'نظام قضاء التنفيذ' );
			$this->email->to ( $to_user_email );
			$this->email->subject ( $subject );
			$this->email->message ( $user_message );
			$sent = $this->email->send ();
		}
	}
	function insert_delivered_notification($values, $email_active, $sms_active) {
		if ($email_active == "Y" || $sms_active == "Y") {
			$this->load->model ( "notification_model" );
			$values ['email_id'] = $this->notification_model->getDeliveredNotificationId ();
			
			$num = $this->common->insertRecord ( 'delivered_notifications', $values );
			
			// for android firebase messaging
			$firebaseNotification = new firebase_notification ();
			$this->load->model ( "common" );
			$where = "where user_id =" . $values ['to_user_id'];
			$userRecord = $this->common->getOneRecField_Limit ( "gcm_id", "user", $where );
			$token = $userRecord->gcm_id;
			$fb_message = array (
					"message" => $values ['message'],
					"type" => $values ['case_id'],
					"date" => $values ['sent_date_time'] 
			);
			$jsonString = $firebaseNotification->sendPushNotificationToGCMSever ( $token, 'التنفيذ', $fb_message );
			
			return $values ['email_id'];
		}
	}
	function send_notification_case_request($email_active, $sms_active, $to_user_email, $to_user_mobile, $token, $to_user_first_name, $to_user_last_name, $case_id, $customer_first_name, $customer_last_name, $request_type, $suspend_reason, $values) {
		$to_user_name = $to_user_first_name . " " . $to_user_last_name;
		$customer_name = $customer_first_name . " " . $customer_last_name;
		
		$user_message = '<div style="text-align:right; direction:rtl;">الأستاذ / ' . $to_user_name . '<br>بعد التحية و التقدير ...<br>';
		$sms_message = 'الأستاذ / ' . $to_user_name . '%0aبعد التحية و التقدير ...%0a';
		
		if ($request_type == 'S') {
			$subject = "طلب تعليق قضية";
			$msg = "رجاء تعليق القضية رقم $case_id الخاصة بالعميل $customer_name و سبب التعليق $suspend_reason";
			$user_message .= 'رجاء تعليق القضية رقم ( ' . $case_id . ' ) الخاصة بالعميل ( ' . $customer_name . ' ) و سبب التعليق ( ' . $suspend_reason . ' )</div>';
			$sms_message .= 'رجاء تعليق القضية رقم ( ' . $case_id . ' ) الخاصة بالعميل ( ' . $customer_name . ' ) و سبب التعليق ( ' . $suspend_reason . ' )';
		} elseif ($request_type == 'R') {
			$subject = "طلب أستكمال قضية";
			$msg = "يرجى استكمال القضية $case_id الخاصة بالعميل $customer_name ";
			$user_message .= 'رجاء استكمال القضية رقم ( ' . $case_id . ' ) الخاصة بالعميل ( ' . $customer_name . ' ) </div>';
			$sms_message .= 'رجاء استكمال القضية رقم ( ' . $case_id . ' ) الخاصة بالعميل ( ' . $customer_name . ' ) ';
		}
		
		self::send_email ( $email_active, $user_message, $subject, $to_user_email );
		self::sendSMS ( $sms_active, $to_user_mobile, $sms_message );
		$firebaseNotification = new firebase_notification ();
		$values ['message'] = $msg;
		
		// $date_diff = self::getTimeDiff ( $values ['sent_date_time'] );
		$email_id = self::insert_delivered_notification ( $values, $email_active, $sms_active );
	}
	function sendSMS($sms_active, $mobile, $msg) {
		if ($sms_active == "Y") {
			global $arraySendMsg;
			$url = "www.mobily.ws/api/msgSend.php";
			$numbers = substr_replace ( $mobile, "966", 0, 1 );
			$userAccount = "966504211994";
			$passAccount = "live123";
			$applicationType = "68";
			$sender = "IbnAlshaikh";
			$MsgID = "15176";
			$timeSend = 0;
			$dateSend = 0;
			$deleteKey = 0;
			$viewResult = 1;
			
			$sender = urlencode ( $sender );
			$domainName = "altanfeeth.com";
			$stringToPost = "mobile=" . $userAccount . "&password=" . $passAccount . "&numbers=" . $numbers . "&sender=" . $sender . "&msg=" . $msg . "&timeSend=" . $timeSend . "&dateSend=" . $dateSend . "&applicationType=" . $applicationType . "&domainName=" . $domainName . "&msgId=" . $MsgID . "&deleteKey=" . $deleteKey . "&lang=3";
			
			$ch = curl_init ();
			curl_setopt ( $ch, CURLOPT_URL, $url );
			curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
			curl_setopt ( $ch, CURLOPT_HEADER, 0 );
			curl_setopt ( $ch, CURLOPT_TIMEOUT, 5 );
			curl_setopt ( $ch, CURLOPT_POST, 1 );
			curl_setopt ( $ch, CURLOPT_POSTFIELDS, $stringToPost );
			$result = curl_exec ( $ch );
			
			// if($viewResult)
			// $result = printStringResult(trim($result) , $arraySendMsg);
			return $result;
		}
	}
	function GregorianToHijri_post() {
		$gregorian_date = $_POST ['date']; // "2017-02-26";
		if ($gregorian_date == null || $gregorian_date == "" || $gregorian_date == " " || $gregorian_date == "0000-00-00") {
			return "";
		} else {
			$date_parts = explode ( "-", $gregorian_date );
			$y = ltrim ( $date_parts [0], '0' );
			$m = ltrim ( $date_parts [1], '0' );
			$d = ltrim ( $date_parts [2], '0' );
			
			$jd = GregorianToJD ( $m, $d, $y );
			$jd = $jd - 1948440 + 10632;
			$n = ( int ) (($jd - 1) / 10631);
			$jd = $jd - 10631 * $n + 354;
			$j = (( int ) ((10985 - $jd) / 5316)) * (( int ) (50 * $jd / 17719)) + (( int ) ($jd / 5670)) * (( int ) (43 * $jd / 15238));
			$jd = $jd - (( int ) ((30 - $j) / 15)) * (( int ) ((17719 * $j) / 50)) - (( int ) ($j / 16)) * (( int ) ((15238 * $j) / 43)) + 29;
			$m = ( int ) (24 * $jd / 709);
			$d = $jd - ( int ) (709 * $m / 24);
			$y = 30 * $n + $j - 30;
			
			if (strlen ( $d ) < 2) {
				$d = "0" . $d;
			}
			if (strlen ( $m ) < 2) {
				$m = "0" . $m;
			}
			$hijri_date = $y . "/" . $m . "/" . $d;
			$this->response ( array (
					'message' => $hijri_date,
					'success' => '0' 
			), 200 );
		}
	}
}
?>