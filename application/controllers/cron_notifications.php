<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

require APPPATH . '/libraries/REST_Controller.php';
require (APPPATH . '/controllers/firebase_notification.php');
class cron_notifications extends REST_Controller {
	function notify_users_check_dev_get() {
		self::checkNotify ();
	}
	/* function tmp_post() {
		$this->load->model ( "notification_model" );
		$undoneRequests = $this->notification_model->getAllUnDoneRequests ();
		if ($undoneRequests) {
			foreach ( $undoneRequests as $case_request ) {
				$repeatNotification = false;
				date_default_timezone_set ( 'Asia/Riyadh' );
				$current_date = date ( "Y-m-d H:i:s" );
				$sent_date = new DateTime ( $case_request ['request_date'] );
				$today = new DateTime ( $current_date );
	
				$diff_value = $sent_date->diff ( $today );
				self::logToFile(" diff_values = $diff_value->d");
				// if the diff is 3 days repeat
				if ($diff_value->y == 0 && $diff_value->m == 0 && $diff_value->d == 3) {
					$repeatNotification = true;
				}
				if($repeatNotification){
					self::process_request_notification($case_request['case_request_id'], $case_request['trans_id']);
				}
			}
		}
		
		$this->response ( array (
					'message' => $undoneRequests,
					'success' => $repeatNotification 
			), 200 );
	} */
	function repeat_case_request_notifs() {
	
	$this->load->model ( "notification_model" );
		$undoneRequests = $this->notification_model->getAllUnDoneRequests ();

		if ($undoneRequests) {
			foreach ( $undoneRequests as $case_request ) {
				$repeatNotification = false;
				date_default_timezone_set ( 'Asia/Riyadh' );
				$current_date = date ( "Y-m-d H:i:s" );
				$sent_date = new DateTime ( $case_request ['request_date'] );
				$today = new DateTime ( $current_date );
	
				$diff_value = $sent_date->diff ( $today );
			
				// if the diff is 3 days repeat
				if ($diff_value->y == 0 && $diff_value->m == 0 && $diff_value->d == 3) {
					$repeatNotification = true;
				}
				if($repeatNotification){
					self::process_request_notification($case_request['case_request_id'], $case_request['trans_id']);
				}
			}
		}
	
	}
	function repeatNotif($last_trans_id, $notif_code, $current_date) {
		$repeatNotification = false;
		$this->load->model ( "notification_model" );
		$del_notif_data = $this->notification_model->getLastDeliveredNotificationForCase ( $last_trans_id, $notif_code );
		$case_id = $del_notif_data ['case_id'];
		$sent_date_time = $del_notif_data ['sent_date_time'];
		$trans_id = $del_notif_data ['trans_id'];
		$case_state = $del_notif_data ['case_state'];
		$trans_state = $del_notif_data ['trans_state'];
		
		// check the diff btw the current date and last date the notif sent
		$sent_date = new DateTime ( $sent_date_time );
		$today = new DateTime ( $current_date );
		// $sent_date->setTime(0,0,0);
		// $today->setTime(0,0,0);
		
		$diff_value = $sent_date->diff ( $today );
		
		// if the diff is 3 days repeat
		if ($diff_value->y == 0 && $diff_value->m == 0 && $diff_value->d == 3) {
			$repeatNotification = true;
		}
		return $repeatNotification;
	}
	
	function logToFile( $msg)
	{
		$filename = 'log.txt';
		$fd = fopen($filename, "a");
		$str = "[" . date("Y/m/d h:i:s", mktime()) . "] " . $msg;
		fwrite($fd, $str . "\n");
		fclose($fd);
	}
	
	function checkNotify() {
		date_default_timezone_set ( 'Asia/Riyadh' );
		$current_date = date ( "Y-m-d H:i:s" );
		$notification_types = $this->common->getAllRow ( "notifications", " where notification_code not in ('M008', 'M009', 'M010')" );
		$this->load->model ( "notification_model" );
		$all_cases = $this->notification_model->getAllCaseIds ();
		self::repeat_case_request_notifs ();
		if ($all_cases) {
			foreach ( $all_cases as $case ) {
				// get last trans to check if the lawyer make action on it or not to repeat notify if the diff is 3 days
				$tran = $this->notification_model->getLastSuccessfulTransForCase ( $case ['case_id'], $case ['lawyer_office_id'] );
				
				$tran_date = new DateTime ( $tran ['trans_date_time'] );
				$diff_value = $tran_date->diff ( new DateTime ( $current_date ) );
				
				foreach ( $notification_types as $notification ) {
					if ($tran ['current_state_code'] == $notification ['case_state']) {
						$last_trans_id = $tran ['transaction_id'];
						$notif_code = $notification ['notification_code'];
						
						$delivered_notifs = $this->common->getAllRow ( "delivered_notifications", "where trans_id = '" . $last_trans_id . "' AND notification_type_code = '" . $notif_code . "'" );
						
						$repeatNotification = false;
						// M007 => "close" not repeat
						// this for check if i need repeat notif or not for actions not done
						if ($delivered_notifs && $delivered_notifs != null && count ( $delivered_notifs ) > 0 && $notif_code != "M007") {
							$repeatNotification = self::repeatNotif ( $last_trans_id, $notif_code, $current_date );
						}
						
						// cases for insert notifications not notif or repeat flag is true
						if (! $delivered_notifs || $delivered_notifs == null || count ( $delivered_notifs ) == 0 || $repeatNotification) {
							
							if (($diff_value->y == 0 && $diff_value->m == 0 && $diff_value->d == $notification ['send_time']) || $repeatNotification) {
								
								$values = array ();
								$values ['trans_id'] = $tran ['transaction_id'];
								$values ['case_id'] = $case ['case_id'];
								$values ['lawyer_office_id'] = $case ['lawyer_office_id'];
								$values ['notification_type_code'] = $notification ['notification_code'];
								$values ['sent_date_time'] = $current_date;
								$values ['waiting_days'] = $notification ['send_time'];
								$mail_sent = false;
								$this->load->library ( '../controllers/dates_conversion' );
								
								// depending on the current state notification type switch on its notif code to know the message text
								switch ($notification ['notification_code']) {
									case "M001" : // assign_to_lawyer
										$values ['to_user_id'] = $tran ['lawyer_id'];
										$last_update_date = $this->dates_conversion->GregorianToHijri ( $tran ['last_update_date'] );
										self::send_notification_new_case ( $notification ['active'], $notification ['sms_active'], $tran ['lawyer_email'], $tran ['lawyer_mobile'], $tran ['lawyer_gcm_id'], $tran ['lawyer_first_name'], $tran ['lawyer_last_name'], $tran ['case_id'], $last_update_date, $tran ['customer_first_name'], $tran ['customer_last_name'], $tran ['region_name'], $values );
										$mail_sent = true;
										break;
									case "M002" : // registeration
									              // case is registered then there is action of "REASSIGN_TO_LAWYER"
									              // may be there is a problem in this condition
										if ($tran ['action_code'] != "REASSIGN_TO_LAWYER") {
											$values ['to_user_id'] = $tran ['lawyer_id'];
											self::send_notification_register_needed ( $notification ['active'], $notification ['sms_active'], $tran ['lawyer_email'], $tran ['lawyer_mobile'], $tran ['lawyer_gcm_id'], $tran ['lawyer_first_name'], $tran ['lawyer_last_name'], $tran ['case_id'], $tran ['customer_first_name'], $tran ['customer_last_name'], $values );
											$mail_sent = true;
										}
										break;
									case "M003" : // dec 34
										$values ['to_user_id'] = $tran ['lawyer_id'];
										self::send_notification_decision34_needed ( $notification ['active'], $notification ['sms_active'], $tran ['lawyer_email'], $tran ['lawyer_mobile'], $tran ['lawyer_gcm_id'], $tran ['lawyer_first_name'], $tran ['lawyer_last_name'], $tran ['case_id'], $tran ['customer_first_name'], $tran ['customer_last_name'], $values );
										$mail_sent = true;
										break;
									case "M004" : // advertisment
										$values ['to_user_id'] = $tran ['lawyer_id'];
										self::send_notification_advertise_needed ( $notification ['active'], $notification ['sms_active'], $tran ['lawyer_email'], $tran ['lawyer_mobile'], $tran ['lawyer_gcm_id'], $tran ['lawyer_first_name'], $tran ['lawyer_last_name'], $tran ['case_id'], $tran ['customer_first_name'], $tran ['customer_last_name'], $values );
										$mail_sent = true;
										break;
									case "M005" : // dec 46
										$values ['to_user_id'] = $tran ['lawyer_id'];
										self::send_notification_decision46_needed ( $notification ['active'], $notification ['sms_active'], $tran ['lawyer_email'], $tran ['lawyer_mobile'], $tran ['lawyer_gcm_id'], $tran ['lawyer_first_name'], $tran ['lawyer_last_name'], $tran ['case_id'], $tran ['customer_first_name'], $tran ['customer_last_name'], $values );
										$mail_sent = true;
										break;
									case "M006" : // sent to all secretary when create new case to review
										if ($tran ['secretaries_emails']) {
											foreach ( $tran ['secretaries_emails'] as $secretary_email ) {
												$values ['to_user_id'] = $secretary_email ['secretary_id'];
												self::send_notification_review_needed ( $notification ['active'], $notification ['sms_active'], $secretary_email ['secretary_email'], $secretary_email ['secretary_mobile'], $secretary_email ['secretary_gcm_id'], $secretary_email ['secretary_first_name'], $secretary_email ['secretary_last_name'], $tran ['case_id'], $tran ['customer_first_name'], $tran ['customer_last_name'], $values );
												$mail_sent = true;
											}
										}
										break;
									case "M007" : // close
										$values ['to_user_id'] = $tran ['customer_id'];
										$last_update_date = $this->dates_conversion->GregorianToHijri ( $tran ['last_update_date'] );
										self::send_notification_case_closed ( $notification ['active'], $notification ['sms_active'], $tran ['customer_email'], $tran ['customer_mobile'], $tran ['customer_gcm_id'], $tran ['customer_first_name'], $tran ['customer_last_name'], $tran ['case_id'], $last_update_date, $tran ['closing_reason'], $values );
										$mail_sent = true;
										break;
									default :
								}
							}
						}
					}
				}
			}
		}
	}
	function send_notification_new_case($email_active, $sms_active, $to_user_email, $to_user_mobile, $token, $to_user_first_name, $to_user_last_name, $case_id, $last_update_date, $customer_first_name, $customer_last_name, $region_name, &$values) {
		$to_user_name = $to_user_first_name . " " . $to_user_last_name;
		$customer_name = $customer_first_name . " " . $customer_last_name;
		$subject = "إحالة قضية جديدة إليك";
		$user_message = '<div style="text-align:right; direction:rtl;">الأستاذ / ' . $to_user_name . '<br>بعد التحية و التقدير ...<br>';
		$user_message .= 'لقد تم إحالة قضية جديدة إليك<br>رقم القضية: ' . $case_id . '<br>بتاريخ: ' . $last_update_date . '<br>اسم العميل: ' . $customer_name . '<br>المنطقة: ' . $region_name . '<br>يرجى الدخول على النظام لعمل الإجراءات اللازمة<br></div>';
		
		self::send_email ( $email_active, $user_message, $subject, $to_user_email );
		
		$sms_message = 'الأستاذ / ' . $to_user_name . '%0aبعد التحية و التقدير ...%0a';
		$sms_message .= 'لقد تم إحالة قضية جديدة إليك%0aرقم القضية: ' . $case_id . '%0aبتاريخ: ' . $last_update_date . '%0aاسم العميل: ' . $customer_name . '%0aالمنطقة: ' . $region_name . '%0aيرجى الدخول على النظام لعمل الإجراءات اللازمة';
		
		self::sendSMS ( $sms_active, $to_user_mobile, $sms_message );
		$firebaseNotification = new firebase_notification ();
		$msg = 'لقد تم إحالة قضية جديدة إليك.رقم القضية: (' . $case_id . ') اسم العميل: (' . $customer_name . ')';
		$date_diff = self::getTimeDiff ( $values ['sent_date_time'] );
		$values ['message'] = $msg;
		$email_id = self::insert_delivered_notification ( $values, $email_active, $sms_active );
	}
	function send_notification_register_needed($email_active, $sms_active, $to_user_email, $to_user_mobile, $token, $to_user_first_name, $to_user_last_name, $case_id, $customer_first_name, $customer_last_name, &$values) {
		$to_user_name = $to_user_first_name . " " . $to_user_last_name;
		$customer_name = $customer_first_name . " " . $customer_last_name;
		$subject = "تذكير بتنفيذ/بتقييد قضية";
		$user_message = '<div style="text-align:right; direction:rtl;">الأستاذ / ' . $to_user_name . '<br>بعد التحية و التقدير ...<br>';
		$user_message .= 'من فضلك قم بتنفيذ واضافة رقم القيد للقضية رقم ( ' . $case_id . ' ) والتي تخص العميل ( ' . $customer_name . ' )</div>';
		
		self::send_email ( $email_active, $user_message, $subject, $to_user_email );
		
		$sms_message = 'الأستاذ / ' . $to_user_name . '%0aبعد التحية و التقدير ...%0a';
		$sms_message .= 'من فضلك قم بتنفيذ واضافة رقم القيد للقضية رقم ( ' . $case_id . ' ) والتي تخص العميل ( ' . $customer_name . ' )';
		
		self::sendSMS ( $sms_active, $to_user_mobile, $sms_message );
		$firebaseNotification = new firebase_notification ();
		$msg = 'من فضلك قم بتنفيذ واضافة رقم القيد للقضية رقم ( ' . $case_id . ' ) والتي تخص العميل ( ' . $customer_name . ' )';
		$date_diff = self::getTimeDiff ( $values ['sent_date_time'] );
		$values ['message'] = $msg;
		$email_id = self::insert_delivered_notification ( $values, $email_active, $sms_active );
	}
	function send_notification_decision34_needed($email_active, $sms_active, $to_user_email, $to_user_mobile, $token, $to_user_first_name, $to_user_last_name, $case_id, $customer_first_name, $customer_last_name, &$values) {
		$to_user_name = $to_user_first_name . " " . $to_user_last_name;
		$customer_name = $customer_first_name . " " . $customer_last_name;
		$subject = "تذكير بتنفيذ قرار 34 لقضية";
		$user_message = '<div style="text-align:right; direction:rtl;">الأستاذ / ' . $to_user_name . '<br>بعد التحية و التقدير ...<br>';
		$user_message .= 'من فضلك قم بتنفيذ قرار 34 للقضية رقم ( ' . $case_id . ' ) والتي تخص العميل ( ' . $customer_name . ' )</div>';
		
		self::send_email ( $email_active, $user_message, $subject, $to_user_email );
		
		$sms_message = 'الأستاذ / ' . $to_user_name . '%0aبعد التحية و التقدير ...%0a';
		$sms_message .= 'من فضلك قم بتنفيذ قرار 34 للقضية رقم ( ' . $case_id . ' ) والتي تخص العميل ( ' . $customer_name . ' )';
		
		self::sendSMS ( $sms_active, $to_user_mobile, $sms_message );
		$firebaseNotification = new firebase_notification ();
		$msg = 'من فضلك قم بتنفيذ قرار 34 للقضية رقم ( ' . $case_id . ' ) والتي تخص العميل ( ' . $customer_name . ' )';
		$date_diff = self::getTimeDiff ( $values ['sent_date_time'] );
		$values ['message'] = $msg;
		$email_id = self::insert_delivered_notification ( $values, $email_active, $sms_active );
	}
	function send_notification_advertise_needed($email_active, $sms_active, $to_user_email, $to_user_mobile, $token, $to_user_first_name, $to_user_last_name, $case_id, $customer_first_name, $customer_last_name, &$values) {
		$to_user_name = $to_user_first_name . " " . $to_user_last_name;
		$customer_name = $customer_first_name . " " . $customer_last_name;
		$subject = "تذكير بإعلان قضية";
		$user_message = '<div style="text-align:right; direction:rtl;">الأستاذ / ' . $to_user_name . '<br>بعد التحية و التقدير ...<br>';
		$user_message .= 'من فضلك قم بإعلان القضية رقم ( ' . $case_id . ' ) والتي تخص العميل ( ' . $customer_name . ' )</div>';
		
		self::send_email ( $email_active, $user_message, $subject, $to_user_email );
		
		$sms_message = 'الأستاذ / ' . $to_user_name . '%0aبعد التحية و التقدير ...%0a';
		$sms_message .= 'من فضلك قم بإعلان القضية رقم ( ' . $case_id . ' ) والتي تخص العميل ( ' . $customer_name . ' )';
		
		self::sendSMS ( $sms_active, $to_user_mobile, $sms_message );
		$firebaseNotification = new firebase_notification ();
		$msg = 'من فضلك قم بإعلان القضية رقم ( ' . $case_id . ' ) والتي تخص العميل ( ' . $customer_name . ' )';
		$date_diff = self::getTimeDiff ( $values ['sent_date_time'] );
		$values ['message'] = $msg;
		$email_id = self::insert_delivered_notification ( $values, $email_active, $sms_active );
	}
	function send_notification_decision46_needed($email_active, $sms_active, $to_user_email, $to_user_mobile, $token, $to_user_first_name, $to_user_last_name, $case_id, $customer_first_name, $customer_last_name, &$values) {
		$to_user_name = $to_user_first_name . " " . $to_user_last_name;
		$customer_name = $customer_first_name . " " . $customer_last_name;
		$subject = "تذكير بتنفيذ قرار 46 لقضية";
		$user_message = '<div style="text-align:right; direction:rtl;">الأستاذ / ' . $to_user_name . '<br>بعد التحية و التقدير ...<br>';
		$user_message .= 'من فضلك قم بتنفيذ قرار 46 للقضية رقم ( ' . $case_id . ' ) والتي تخص العميل ( ' . $customer_name . ' )</div>';
		
		self::send_email ( $email_active, $user_message, $subject, $to_user_email );
		
		$sms_message = 'الأستاذ / ' . $to_user_name . '%0aبعد التحية و التقدير ...%0a';
		$sms_message .= 'من فضلك قم بتنفيذ قرار 46 للقضية رقم ( ' . $case_id . ' ) والتي تخص العميل ( ' . $customer_name . ' )';
		
		self::sendSMS ( $sms_active, $to_user_mobile, $sms_message );
		$firebaseNotification = new firebase_notification ();
		$msg = 'من فضلك قم بتنفيذ قرار 46 للقضية رقم ( ' . $case_id . ' ) والتي تخص العميل ( ' . $customer_name . ' )';
		$date_diff = self::getTimeDiff ( $values ['sent_date_time'] );
		$values ['message'] = $msg;
		$email_id = self::insert_delivered_notification ( $values, $email_active, $sms_active );
	}
	function send_notification_review_needed($email_active, $sms_active, $to_user_email, $to_user_mobile, $token, $to_user_first_name, $to_user_last_name, $case_id, $customer_first_name, $customer_last_name, &$values) {
		$to_user_name = $to_user_first_name . " " . $to_user_last_name;
		$customer_name = $customer_first_name . " " . $customer_last_name;
		$subject = "تذكير بمراجعة بيانات قضية";
		$user_message = '<div style="text-align:right; direction:rtl;">الأستاذ / ' . $to_user_name . '<br>بعد التحية و التقدير ...<br>';
		$user_message .= 'من فضلك قم بمراجعة بيانات القضية رقم ( ' . $case_id . ' ) والتي تخص العميل ( ' . $customer_name . ' )</div>';
		
		self::send_email ( $email_active, $user_message, $subject, $to_user_email );
		
		$sms_message = 'الأستاذ / ' . $to_user_name . '%0aبعد التحية و التقدير ...%0a';
		$sms_message .= 'من فضلك قم بمراجعة بيانات القضية رقم ( ' . $case_id . ' ) والتي تخص العميل ( ' . $customer_name . ' )';
		
		self::sendSMS ( $sms_active, $to_user_mobile, $sms_message );
		$firebaseNotification = new firebase_notification ();
		$msg = 'من فضلك قم بمراجعة بيانات القضية رقم ( ' . $case_id . ' ) والتي تخص العميل ( ' . $customer_name . ' )';
		$date_diff = self::getTimeDiff ( $values ['sent_date_time'] );
		$values ['message'] = $msg;
		$email_id = self::insert_delivered_notification ( $values, $email_active, $sms_active );
	}
	function send_notification_case_closed($email_active, $sms_active, $to_user_email, $to_user_mobile, $token, $to_user_first_name, $to_user_last_name, $case_id, $last_update_date, $closing_reason, &$values) {
		$to_user_name = $to_user_first_name . " " . $to_user_last_name;
		$subject = "انهاء القضية";
		$user_message = '<div style="text-align:right; direction:rtl;">الأستاذ / ' . $to_user_name . '<br>بعد التحية و التقدير ...<br>';
		$user_message .= 'لقد تم انهاء قضيتك رقم ( ' . $case_id . ' ) بتاريخ ( ' . $last_update_date . ' ). سبب الإنهاء هو ' . $closing_reason . '</div>';
		
		self::send_email ( $email_active, $user_message, $subject, $to_user_email );
		
		$sms_message = 'الأستاذ / ' . $to_user_name . '%0aبعد التحية و التقدير ...%0a';
		$sms_message .= 'لقد تم انهاء قضيتك رقم ( ' . $case_id . ' ) بتاريخ ( ' . $last_update_date . ' ). سبب الإنهاء هو ' . $closing_reason;
		
		self::sendSMS ( $sms_active, $to_user_mobile, $sms_message );
		$firebaseNotification = new firebase_notification ();
		$msg = 'لقد تم انهاء قضيتك رقم ( ' . $case_id . ' ) ';
		$date_diff = self::getTimeDiff ( $values ['sent_date_time'] );
		$values ['message'] = $msg;
		
		// insert only if email is active or sms is active
		$email_id = self::insert_delivered_notification ( $values, $email_active, $sms_active );
	}
	function send_email($email_active, $user_message, $subject, $to_user_email) {
		if ($email_active == "Y") {
			$this->load->library ( 'email' );
			$config ['mailtype'] = 'html';
			$config ['charset'] = 'utf-8';
			$this->email->initialize ( $config );
			
			$this->email->from ( "info@altanfeeth.com", 'نظام قضاء التنفيذ' );
			$this->email->to ( $to_user_email );
			$this->email->subject ( $subject );
			$this->email->message ( $user_message );
			$sent = $this->email->send ();
		}
	}
	function insert_delivered_notification($values, $email_active, $sms_active) {
		if ($email_active == "Y" || $sms_active == "Y") {
			$this->load->model ( "notification_model" );
			$values ['email_id'] = $this->notification_model->getDeliveredNotificationId ();
			
			$this->common->insertRecord ( 'delivered_notifications', $values );
			
			// for android firebase messaging
			$firebaseNotification = new firebase_notification ();
			$this->load->model ( "common" );
			$where = "where user_id =" . $values ['to_user_id'];
			$userRecord = $this->common->getOneRecField_Limit ( "gcm_id", "user", $where );
			$token = $userRecord->gcm_id;
			$fb_message = array (
					"message" => $values ['message'],
					"type" => $values ['case_id'],
					"date" => $values ['sent_date_time'] 
			);
			$jsonString = $firebaseNotification->sendPushNotificationToGCMSever ( $token, 'التنفيذ', $fb_message );
			
			return $values ['email_id'];
		}
	}
	function getTimeDiff($dateString) {
		date_default_timezone_set ( 'Asia/Riyadh' );
		$date = new DateTime ( $dateString );
		$diff = self::formatDateDiff ( $date );
		return $diff;
	}
	
	/**
	 * A sweet interval formatting, will use the two biggest interval parts.
	 * On small intervals, you get minutes and seconds.
	 * On big intervals, you get months and days.
	 * Only the two biggest parts are used.
	 *
	 * @param DateTime $start        	
	 * @param DateTime|null $end        	
	 * @return string
	 */
	function formatDateDiff($start, $end = null) {
		date_default_timezone_set ( 'Asia/Riyadh' );
		if (! ($start instanceof DateTime)) {
			$start = new DateTime ( $start );
		}
		
		if ($end === null) {
			$end = new DateTime ();
		}
		
		if (! ($end instanceof DateTime)) {
			$end = new DateTime ( $start );
		}
		// $end->setTimezone(new DateTimeZone('Asia/Riyadh'));
		$interval = $end->diff ( $start );
		// echo $end->format('Y-m-d H:i:s').' '.$start->format('Y-m-d H:i:s').' '.($end->format('Y-m-d H:i:s') - $start->format('Y-m-d H:i:s'));
		
		$getTimeString = function ($nb, $str, $value, $from_str) {
			
			$doPlural = function ($nb, $str) {
				if ($nb == 0) {
					return "الان";
				} else if ($nb == 1 || $nb > 10) {
					return $str;
				} else if ($nb == 2) {
					switch ($str) {
						case "سنة" :
							return "سنتان";
							break;
						case "شهر" :
							return "شهران";
							break;
						case "يوم" :
							return "يومان";
							break;
						case "ساعة" :
							return "ساعتان";
							break;
						case "دقيقة" :
							return "دقيقتان";
							break;
						case "ثانية" :
							return "ثانيتان";
							break;
						default :
							return "ثانيتان";
					}
				} else if ($nb >= 3 && $nb <= 10) {
					switch ($str) {
						case "سنة" :
							return "سنين";
							break;
						case "شهر" :
							return "شهور";
							break;
						case "يوم" :
							return "أيام";
							break;
						case "ساعة" :
							return "ساعات";
							break;
						case "دقيقة" :
							return "دقائق";
							break;
						case "ثانية" :
							return "ثوانى";
							break;
						default :
							return "ثوانى";
					}
				} else {
					return $str;
				}
			}; // adds plurals
			
			if ($nb == 0) {
				return $doPlural ( $nb, $str );
			} else if ($nb == 1) {
				return $from_str . $doPlural ( $nb, $str );
			} else if ($nb > 10) {
				return $from_str . $value . $doPlural ( $nb, $str );
			} else if ($nb == 2) {
				return $from_str . $doPlural ( $nb, $str );
			} else if ($nb >= 3 && $nb <= 10) {
				return $from_str . $value . $doPlural ( $nb, $str );
			}
		}; // getTimeString
		
		$format = array ();
		if ($interval->y !== 0) {
			$format [] = $getTimeString ( $interval->y, "سنة", "%y ", "منذ " );
		} else if ($interval->m !== 0) {
			$format [] = $getTimeString ( $interval->m, "شهر", "%m ", "منذ " );
		} else if ($interval->d !== 0) {
			$format [] = $getTimeString ( $interval->d, "يوم", "%d ", "منذ " );
		} else if ($interval->h !== 0) {
			$format [] = $getTimeString ( $interval->h, "ساعة", "%h ", "منذ " );
		} else if ($interval->i !== 0) {
			$format [] = $getTimeString ( $interval->i, "دقيقة", "%i ", "منذ " );
		} else if ($interval->s !== 0) {
			/*
			 * if(!count($format)) {
			 * return "منذ أقل من دقيقة";
			 * } else {
			 * $format[] = "%s ".$doPlural($interval->s, "ثانية");
			 * }
			 */
			$format [] = $getTimeString ( $interval->s, "ثانية", "%s ", "منذ " );
		} else {
			$format [] = $getTimeString ( $interval->s, "ثانية", "%s ", "منذ " );
		}
		
		// We use the two biggest parts
		if (count ( $format ) > 1) {
			$format = array_shift ( $format ) . " و " . array_shift ( $format );
		} else {
			$format = array_pop ( $format );
		}
		$format;
		// Prepend 'since ' or whatever you like
		return $interval->format ( $format );
	}
	
	// Send SMS API using CURL method
	function sendSMS($sms_active, $mobile, $msg) {
		if ($sms_active == "Y") {
			global $arraySendMsg;
			$url = "www.mobily.ws/api/msgSend.php";
			$numbers = substr_replace ( $mobile, "966", 0, 1 );
			$userAccount = "966504211994";
			$passAccount = "live123";
			$applicationType = "68";
			$sender = "IbnAlshaikh";
			$MsgID = "15176";
			$timeSend = 0;
			$dateSend = 0;
			$deleteKey = 0;
			$viewResult = 1;
			
			$sender = urlencode ( $sender );
			$domainName = "altanfeeth.com";
			$stringToPost = "mobile=" . $userAccount . "&password=" . $passAccount . "&numbers=" . $numbers . "&sender=" . $sender . "&msg=" . $msg . "&timeSend=" . $timeSend . "&dateSend=" . $dateSend . "&applicationType=" . $applicationType . "&domainName=" . $domainName . "&msgId=" . $MsgID . "&deleteKey=" . $deleteKey . "&lang=3";
			
			$ch = curl_init ();
			curl_setopt ( $ch, CURLOPT_URL, $url );
			curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
			curl_setopt ( $ch, CURLOPT_HEADER, 0 );
			curl_setopt ( $ch, CURLOPT_TIMEOUT, 5 );
			curl_setopt ( $ch, CURLOPT_POST, 1 );
			curl_setopt ( $ch, CURLOPT_POSTFIELDS, $stringToPost );
			$result = curl_exec ( $ch );
			
			// if($viewResult)
			// $result = printStringResult(trim($result) , $arraySendMsg);
			return $result;
		}
	}
	function process_request_notification($case_request_id, $trans_id) {
		$this->load->model ( "notification_model" );
		$notification_data = $this->notification_model->get_case_request_notification_data ( $case_request_id );
		
		if ($notification_data && sizeof ( $notification_data ) > 0) {
			$records = $notification_data ['records'];
			$suspend_reason = $records ['suspend_reason'];
			$request_type = $records ['request_type'];
			$case_id = $records ['case_id'];
			$lawyer_office_id = $records ['lawyer_office_id'];
			$customer_first_name = $records ['customer_first_name'];
			$customer_last_name = $records ['customer_last_name'];
			$admin_email = $records ['admin_email'];
			$admin_mobile = $records ['admin_mobile'];
			$values = array ();
			$values ['case_id'] = $case_id;
			$values ['lawyer_office_id'] = $lawyer_office_id;
			$values ['notification_type_code'] = 'CASE_REQ';
			date_default_timezone_set ( 'Asia/Riyadh' );
			$current_date = date ( "Y-m-d H:i:s" );
			$values ['sent_date_time'] = $current_date;
			$values ['waiting_days'] = '0';
			
			$values ['trans_id'] = $trans_id;
			// admin (notif, email)
			$admin_records = $notification_data ['admin_record'];
			if ($admin_records && sizeof ( $admin_records ) > 0) {
				$admin_first_name = $admin_records ['admin_first_name'];
				$admin_last_name = $admin_records ['admin_last_name'];
				$admin_gcm_id = $admin_records ['admin_gcm_id'];
				$values ['to_user_id'] = $admin_records ['user_id'];
				$values ['notification_type_code'] = 'M008';
				$this->load->model ( "common" );
				$where = "where notification_code = 'M008'" ;
				$notRecord = $this->common->getOneRow ("notifications", $where );
				self::send_notification_case_request ( $notRecord['active'], $notRecord['sms_active'], $admin_email, $admin_mobile, $admin_gcm_id, $admin_first_name, $admin_last_name, $case_id, $customer_first_name, $customer_last_name, $request_type, $suspend_reason, $values );
			}
			
			// lawyer (notif, email, mobile)
			$lawyer_records = $notification_data ['lawyer_record'];
			if ($lawyer_records && sizeof ( $lawyer_records )) {
				$lawyer_email = $lawyer_records ['lawyer_email'];
				$lawyer_first_name = $lawyer_records ['lawyer_first_name'];
				$lawyer_last_name = $lawyer_records ['lawyer_last_name'];
				$lawyer_gcm_id = $lawyer_records ['lawyer_gcm_id'];
				$lawyer_mobile = $lawyer_records ['lawyer_mobile'];
				$values ['to_user_id'] = $lawyer_records['user_id'];
				$values ['notification_type_code'] = 'M009';

				$this->load->model ( "common" );
				$where = "where notification_code = 'M009'" ;
				$notRecord = $this->common->getOneRow ("notifications", $where );
				self::send_notification_case_request ( $notRecord['active'], $notRecord['sms_active'], $lawyer_email, $lawyer_mobile, $lawyer_gcm_id, $lawyer_first_name, $lawyer_last_name, $case_id, $customer_first_name, $customer_last_name, $request_type, $suspend_reason, $values );
			}
			// secretaries
			
			$secretaries_records = $notification_data ['secretaries_emails'];
			if ($secretaries_records && sizeof ( $secretaries_records ) > 0) {
				foreach ( $secretaries_records as $secretary_email ) {
					$values ['to_user_id'] = $secretary_email ['secretary_id'];
					$values ['notification_type_code'] = 'M010';
					$this->load->model ( "common" );
					$where = "where notification_code = 'M010'" ;
					$notRecord = $this->common->getOneRow ("notifications", $where );
					self::send_notification_case_request ( $notRecord['active'], $notRecord['sms_active'], $secretary_email ['secretary_email'], $secretary_email ['secretary_mobile'], $secretary_email ['secretary_gcm_id'], $secretary_email ['secretary_first_name'], $secretary_email ['secretary_last_name'], $case_id, $customer_first_name, $customer_last_name, $request_type, $suspend_reason, $values );
				}
			}
			
			return $notification_data;
		} else {
			return false;
		}
	}
	function send_notification_case_request($email_active, $sms_active, $to_user_email, $to_user_mobile, $token, $to_user_first_name, $to_user_last_name, $case_id, $customer_first_name, $customer_last_name, $request_type, $suspend_reason, $values) {
		$to_user_name = $to_user_first_name . " " . $to_user_last_name;
		$customer_name = $customer_first_name . " " . $customer_last_name;
		
		$user_message = '<div style="text-align:right; direction:rtl;">الأستاذ / ' . $to_user_name . '<br>بعد التحية و التقدير ...<br>';
		$sms_message = 'الأستاذ / ' . $to_user_name . '%0aبعد التحية و التقدير ...%0a';
		
		if ($request_type == 'S') {
			$subject = "طلب تعليق قضية";
			$msg = "رجاء تعليق القضية رقم $case_id الخاصة بالعميل $customer_name و سبب التعليق $suspend_reason";
			$user_message .= 'رجاء تعليق القضية رقم ( ' . $case_id . ' ) الخاصة بالعميل ( ' . $customer_name . ' ) و سبب التعليق ( '.$suspend_reason. ' )</div>';
			$sms_message .= 'رجاء تعليق القضية رقم ( ' . $case_id . ' ) الخاصة بالعميل ( ' . $customer_name . ' ) و سبب التعليق ( '.$suspend_reason. ' )';
		} elseif ($request_type == 'R') {
			$subject = "طلب أستكمال قضية";
			$msg = "يرجى استكمال القضية $case_id الخاصة بالعميل $customer_name ";
			$user_message .= 'رجاء استكمال القضية رقم ( ' . $case_id . ' ) الخاصة بالعميل ( ' . $customer_name . ' ) </div>';
			$sms_message .= 'رجاء استكمال القضية رقم ( ' . $case_id . ' ) الخاصة بالعميل ( ' . $customer_name . ' ) ';
	
		}
		
		self::send_email ( $email_active, $user_message, $subject, $to_user_email );
		self::sendSMS ( $sms_active, $to_user_mobile, $sms_message );
		$firebaseNotification = new firebase_notification ();
		$values ['message'] = $msg;
		// $date_diff = self::getTimeDiff ( $values ['sent_date_time'] );
		$email_id = self::insert_delivered_notification ( $values, $email_active, $sms_active );
	}
}

?>
