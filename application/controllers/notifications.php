<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class notifications extends CI_Controller
{
	function __construct()
    {
        parent::__construct();
        $this -> load -> model("common");
		if($this->session->userdata('userid')=='' || $this->session->userdata('user_type')!='ADMIN') {
                        //echo 'user data  is null';
			redirect('user_login','refresh');
		}
    }
    
	function index() {
	  
	   self::view_notifications();
	   
	}
	
	
	function view_notifications() {
	   $session_user_id = $this->session->userdata('userid');
	   $where = "where user_id =".$session_user_id ;
	   $result = $this->common->getOneRow('user',$where);
	   $data['profile_first_name']=$result['first_name'];
	   $data['profile_last_name']=$result['last_name'];
	   $data['profile_img']=$result['profile_img'];	
	   
		$this -> load -> model("notification_model");
	   	$notifications = $this -> notification_model -> getLatestDeliveredNotificationsForUser($session_user_id);
	   	$data['notifications_list'] = $notifications['notifications_list'];
		$data['count_unseen'] = $notifications['count_unseen'];
		$data['page'] = "notifications/view_notifications";
	   
	   $data['notifications'] = $this -> notification_model -> getNotifications();
	   
	   $this->load->view('notifications/view_notifications',$data);
	}
	
	function edit_notification($notification_code="") {
		
	   $session_user_id = $this->session->userdata('userid');
		
       $where = "where user_id =".$session_user_id ;
	   $result = $this->common->getOneRow('user',$where);
	   $data['profile_first_name']=$result['first_name'];
	   $data['profile_last_name']=$result['last_name'];
	   $data['profile_img']=$result['profile_img'];
	   
	   $this -> load -> model("notification_model");
	   	$notifications = $this -> notification_model -> getLatestDeliveredNotificationsForUser($session_user_id);
	   	$data['notifications_list'] = $notifications['notifications_list'];
		$data['count_unseen'] = $notifications['count_unseen'];
		$data['page'] = "notifications/edit_notification/".$notification_code;
	   
	   $data['notification_code']=$notification_code;
	   $data['send_time']="";	
	   $data['active']="";
	   $data['sms_active']="";
	   $data['notification_name']="";	
	   $data['state_code']="";
	   $data['state_name']="";	
	   $data['user_type_code']="";
	   $data['user_type_name']="";	
	    		   		
		if(extract($_POST)) {			
			$value['send_time']= $_POST['send_time'];
			$value['active']= $_POST['active'];
			$value['sms_active']= $_POST['sms_active']; 	
			
			if($notification_code != "") {
				$where = "notification_code ='".$notification_code."'";
				$this->common->updateRecord('notifications',$value,$where);
			}
				
			redirect("notifications/view_notifications");
		} else {
			if($notification_code != "") {
				$this -> load -> model("notification_model");
	   			$notification_data = $this -> notification_model -> getNotificationData($notification_code);
	   			$data['send_time']=$notification_data['send_time'];	
	   			$data['active']=$notification_data['active'];
	   			$data['sms_active']=$notification_data['sms_active'];
				$data['notification_name']=$notification_data['notification_name'];	
	   			$data['state_code']=$notification_data['state_code'];
	   			$data['state_name']=$notification_data['state_name'];	
	   			$data['user_type_code']=$notification_data['user_type_code'];
	   			$data['user_type_name']=$notification_data['user_type_name'];	
			}
							
			$this->load->view('notifications/edit_notification',$data);
		}
		
		
	}
	
	function update_seen_notifications() {
		//echo "here1";
		if(extract($_POST)) {	
			$last_id= $_POST['last_id'];
			//echo "here=".$last_id;
			$user_id = $this->session->userdata('userid');
			$value['seen'] = "Y";
			$where = "to_user_id = '".$user_id."' AND ( email_id < '".$last_id."' OR email_id = '".$last_id."' )";
			$this->common->updateRecord('delivered_notifications',$value,$where);
			//redirect($_POST['page']);
		}	
	}
	
	function update_user_token() {
		if(extract($_POST)) {			
			$user_id = $this->session->userdata('userid');
			$update['gcm_id'] = $_POST['token'];
			$this -> common -> updateRecord("user",$update,"user_id ='$user_id'");
			//redirect($_POST['page']);
		}
	}
	
	function update_notification_active_value() {
		if(extract($_POST)) {			
			$notification_code= $_POST['notification_code'];
			$value['active'] = $_POST['active'];
			if($notification_code != "") {
				$where = "notification_code ='".$notification_code."'";
				$this->common->updateRecord('notifications',$value,$where);
			}
		}	
	}
	
	function update_notification_sms_active_value() {
		if(extract($_POST)) {			
			$notification_code= $_POST['notification_code'];
			$value['sms_active'] = $_POST['sms_active'];
			if($notification_code != "") {
				$where = "notification_code ='".$notification_code."'";
				$this->common->updateRecord('notifications',$value,$where);
			}
		}	
	}
	
}
