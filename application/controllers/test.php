<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class test extends REST_Controller
{

	function manage_case_post($case_id='', $action_code='') {
		//$user_id = $this->session->userdata('userid');
		$user_id = 2;
		$where = "where user_id =".$user_id;
		$userRecord = $this->common->getOneRow('user',$where);
		$case_id = $_POST['case_id'];
		$action_code = $_POST['action_code'];
		//if(extract($_POST)) {	
			$user_type_code = $userRecord['user_type_code'];
			$this -> load -> model("case_model");
			
			$trans_values = $this->case_model->getCaseDataForTransaction($case_id);	 
			
			$value = array();
			/*self::getNewParameterValue($value, $trans_values, $_POST['customer_id'], 'customer_id');
			self::getNewParameterValue($value, $trans_values, $_POST['contract_number'], 'contract_number');
			self::getNewParameterValue($value, $trans_values, $_POST['client_id_number'], 'client_id_number');
			self::getNewParameterValue($value, $trans_values, $_POST['client_type'], 'client_type');
			self::getNewParameterValue($value, $trans_values, $_POST['number_of_late_instalments'], 'number_of_late_instalments');
			self::getNewParameterValue($value, $trans_values, $_POST['due_amount'], 'due_amount');
			self::getNewParameterValue($value, $trans_values, $_POST['remaining_amount'], 'remaining_amount');*/
			/*if(isset($_FILES['debenture']['name']) && $_FILES['debenture']['name']!="") { 
				self::upload_file($value, $trans_values, 'debenture', $case_id, $_FILES['debenture']['name'], $_FILES['debenture']['tmp_name']);
			}*/
			if(isset($_FILES['id']['name']) && $_FILES['id']['name']!="") { 
				self::upload_file($value, $trans_values, 'id', $case_id, $_FILES['id']['name'], $_FILES['id']['tmp_name']);
			}
		/*	if(isset($_FILES['contract']['name']) && $_FILES['contract']['name']!="") { 
				self::upload_file($value, $trans_values, 'contract', $case_id, $_FILES['contract']['name'], $_FILES['contract']['tmp_name']);
			}
			*/
			$others_count = $_POST['others_count'];
			if ($others_count != null && $others_count > 0) { 
				$others_value = "";
				for ($i = 1; $i <= $others_count; $i++) {
					$item = "others_".$i;
					$name = $_FILES[$item]['name'];
					$tmp_name = $_FILES[$item]['tmp_name'];
					if(isset($name) && $name !="") { 
						if(!is_dir("./cases_files/".$case_id))	{
							 mkdir('./cases_files/'.$case_id, 0777, true);
						}								
						$file =$name;
						$uploadedfile =$tmp_name;					
						$ext = strstr($name,".");									
			          			
						$source = $tmp_name;
						$file= $item."-".str_replace(' ','-',$file); 					
			 		    $filepath = $file;
			          	$save = "./cases_files/".$case_id."/".base64_encode($filepath);           			
			 		    move_uploaded_file($uploadedfile,$save);

			 		    $others_value = $others_value.","."cases_files/".$case_id."/".$file;
			 		   								
					}
				}
				$others_value = ltrim($others_value,",");
				$value["others"]= $others_value;
				$trans_values["others"]= $others_value;	
			}
			
		//	self::getNewParameterValue($value, $trans_values, $_POST['creation_date'], 'creation_date');
		//	self::getNewParameterValue($value, $trans_values, $_POST['creation_user'], 'creation_user');
		/*	self::getNewParameterValue($value, $trans_values, $_POST['executive_order_number'], 'executive_order_number');
			self::getNewParameterValue($value, $trans_values, $_POST['decision_34_number'], 'decision_34_number');
			if(isset($_FILES['decision_34_file']['name']) && $_FILES['decision_34_file']['name']!="") { 
				self::upload_file($value, $trans_values, 'decision_34_file', $case_id, $_FILES['decision_34_file']['name'], $_FILES['decision_34_file']['tmp_name']);
			}
			if(isset($_FILES['advertisement_file']['name']) && $_FILES['advertisement_file']['name']!="") {
				self::upload_file($value, $trans_values, 'advertisement_file', $case_id, $_FILES['advertisement_file']['name'], $_FILES['advertisement_file']['tmp_name']);
			}
			if(isset($_FILES['invoice_file']['name']) && $_FILES['invoice_file']['name']!="") {
				self::upload_file($value, $trans_values, 'invoice_file', $case_id, $_FILES['invoice_file']['name'], $_FILES['invoice_file']['tmp_name']);
			}
			self::getNewParameterValue($value, $trans_values, $_POST['decision_46_status'], 'decision_46_status');
			self::getNewParameterValue($value, $trans_values, $_POST['closing_reason'], 'closing_reason');*/
			
			$source_state = $trans_values['current_state_code'];
			$newStateRecord = $this -> common -> getOneRecField_Limit("target_state", "state_transition","where action_code ='$action_code' AND source_state= '".$source_state."'");
			$value['current_state_code']= $newStateRecord->target_state; 
			$trans_values['current_state_code'] = $value['current_state_code'];
			$value['last_update_user']= $this->session->userdata('userid');
			$trans_values['last_update_user'] = $value['last_update_user'];
			date_default_timezone_set('Asia/Riyadh');
			$current_date = date("Y-m-d");
			$value['last_update_date']= $current_date;
			$trans_values['last_update_date'] = $value['last_update_date'];
			
			self::insert_transaction($trans_values, "Pending");
			$lawyer_case_id = 1;
			if ($action_code == "ASSIGN_TO_LAWYER") {
				 $lawyer_case_id = $this -> common -> insertRecord("lawyer_case",
								array
								(
									"lawyer_id" => $_POST['lawyer_id'],
									"case_id" => $case_id
								));  
			}
			
			$where = "case_id ='".$case_id."'";
			$affected_rows = $this->common->updateRecord('case',$value,$where);
			
			if ($affected_rows == 1 && $lawyer_case_id != null && $lawyer_case_id > 0) {
				self::insert_transaction($trans_values, "Success");
			} else {
				self::insert_transaction($trans_values, "Failure");
			}
				
		//	redirect('case_management/view_case_details/'.$case_id);
		$this->response(array(
				'files' => "ok",
				'success' => '1'), 200);		
		
	}
	
	function getNewParameterValue(&$value, &$trans_values, $param_value, $param_name) {
		if ($param_value != null && isset($param_value) && $param_value !="") {
			//echo "value:".$param_name."=".$param_value."*";
			$value[$param_name] = $param_value;
			$trans_values[$param_name] = $param_value;
		}
	}
	
	function upload_file(&$value, &$trans_values, $param_name, $case_id, $name, $tmp_name) {
	echo "here";
		if(isset($name) && $name !="") { 
			if(!is_dir("./cases_files/".$case_id))	{
				 mkdir('./cases_files/'.$case_id, 0777, true);
			}								
			$file =$name;
			$uploadedfile =$tmp_name;					
			$ext = strstr($name,".");									
          			
			$source = $tmp_name;
			$file= $param_name."-".str_replace(' ','-',$file); 					
 		    $filepath = $file;
          	$save = "./cases_files/".$case_id."/".base64_encode($filepath);           			
 		    move_uploaded_file($uploadedfile,$save);
 		           	
 		    $value[$param_name]= "cases_files/".$case_id."/".$file;
		    $trans_values[$param_name]= "cases_files/".$case_id."/".$file;									
		}
	}
	
	function insert_transaction($values, $status) {
			date_default_timezone_set('Asia/Riyadh');
			$current_date = date("Y-m-d");
			$current_date_time = new DateTime($current_date);
			$current_formatted_date = $current_date_time->format('dmY');
			$current_timestamp = date("His");
			$random_value = str_pad(rand(0,99), 2, "0", STR_PAD_LEFT);
			
			$values['transaction_id']="TRN".$current_formatted_date."T".$current_timestamp.$random_value;
			$values['transaction_status']=$status;
			if ($status == "Failure") {
				$values['error_code']=1;
			}			
			$this->common->insertRecord('case_transactions',$values);
	}
	
	function HijriToGregorian_post() {
		echo "here";
		$hijri_date = "1438/05/29";
		$date_parts = explode("/", $hijri_date);
	/*	$m = $_POST['m'];
		$d = $_POST['d'];
		$y = $_POST['y'];
		*/
		$y = ltrim($date_parts[0], '0');
		$m = ltrim($date_parts[1], '0');
		$d = ltrim($date_parts[2], '0');
		
   		$date = (int)((11 * $y + 3) / 30) + 354 * $y + 30 * $m - (int)(($m - 1) / 2) + $d + 1948440 - 385;
   		//$date = ((11 * $y + 3) / 30) + 354 * $y + 30 * $m - (($m - 1) / 2) + $d + 1948440 - 385;
     
    // $date = HijriToJD(09, 13, 1436); without leading zeros 5/5/1438 gives 2/2/2017 and 29/5/1438 gives 2/26/2017

		$gregorian_date = jdtogregorian($date);
		$date_parts = explode("/", $gregorian_date);
		$d = $date_parts[1];
		if (strlen($d) < 2) {
			$d = "0".$d;
		}
		$m = $date_parts[0];
		if (strlen($m) < 2) {
			$m = "0".$m;
		}
		$y = $date_parts[2];
		
		$gregorian_date = $y."-".$m."-".$d;
		echo $gregorian_date;
		
	/*	$jd = GregorianToJD(10, 11, 1970);
echo "$jd\n";
$gregorian = JDToGregorian($jd);
echo "$gregorian\n";*/
		//int gregoriantojd ( int $month , int $day , int $year )
	}
	
 // Julian Day Count To Hijri
  function GregorianToHijri_post() {
  	
  	  $gregorian_date = "2017-02-26";
  	  
  	  $date_parts = explode("-", $gregorian_date);
	  $y = ltrim($date_parts[0], '0');
	  $m = ltrim($date_parts[1], '0');
	  $d = ltrim($date_parts[2], '0');
  	  
  	  $jd = GregorianToJD($m, $d, $y);
      $jd = $jd - 1948440 + 10632;
      $n  = (int)(($jd - 1) / 10631);
      $jd = $jd - 10631 * $n + 354;
      $j  = ((int)((10985 - $jd) / 5316)) *
          ((int)(50 * $jd / 17719)) +
          ((int)($jd / 5670)) *
          ((int)(43 * $jd / 15238));
      $jd = $jd - ((int)((30 - $j) / 15)) *
          ((int)((17719 * $j) / 50)) -
          ((int)($j / 16)) *
          ((int)((15238 * $j) / 43)) + 29;
      $m  = (int)(24 * $jd / 709);
      $d  = $jd - (int)(709 * $m / 24);
      $y  = 30*$n + $j - 30;
      
  	  if (strlen($d) < 2) {
			$d = "0".$d;
	  }
      if (strlen($m) < 2) {
			$m = "0".$m;
	  }
	  $hijri_date = $y."/".$m."/".$d;
      echo  $hijri_date;
      //return array($m, $d, $y);
  }
	
	function test_notify_get() {
			$body = "test";
			$subject = "hello test";
			$msg = "test ok";
			$headers = "From: FareShare <samar.moursi@aurorasdp.com> \r\n";
		/*	$mailSent = mail("omneya.khaled@aurorasdp.com", $subject, $msg, $headers);
			if ($mailSent) {
				$mail_sent = true;
			}*/
		/*	
			$this->load->library('email');

			$config['mailtype'] = 'html';
			$config['charset'] = 'utf-8';
		

			$this->email->initialize($config);
			
	
			
			$user_message = '<div style="text-align:right; direction:rtl;"> أهلا و مرحبا<br><br>  </div>';
			$confirm_subject = "أهلا و مرحبا";
			$this->email->from("samar.moursi@aurorasdp.com",'new');
			$this->email->to("omneya.khaled@aurorasdp.com");
			$this->email->subject($confirm_subject);
			$this->email->message($user_message);
			$sent = $this->email->send();
			echo "sent=".$sent;*/
	}

}
?>