<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class lawyer_office_regions extends CI_Controller
{
	function __construct()
    {
        parent::__construct();
        $this -> load -> model("common");
		if($this->session->userdata('userid')=='' || $this->session->userdata('user_type')!='ADMIN') {
			redirect('user_login','refresh');
		}
    }
    
	function index() {
	  
	   self::view_regions();
	   
	}

	
	function view_regions($delete_error=0) {
	$office_admin_id = $this->session->userdata('userid');
	$userRecord = $this -> common -> getOneRecField_Limit("lawyer_office_id", "user","where user_id ='$office_admin_id'");
	$lawyer_office_id = $userRecord->lawyer_office_id;
	
		$this -> load -> model("notification_model");
	   	$notifications = $this -> notification_model -> getLatestDeliveredNotificationsForUser($office_admin_id);
	   	$data['notifications_list'] = $notifications['notifications_list'];
		$data['count_unseen'] = $notifications['count_unseen'];
		$data['page'] = "lawyer_office_regions/view_regions/".$delete_error;
		
	   $this -> load -> model("lawyer_office_model");
	   $regions_data = $this -> lawyer_office_model -> getRegions($lawyer_office_id);
	   $data['regions'] = $regions_data;
	   $data['delete_error'] = $delete_error;
	   $where = "where user_id =".$office_admin_id  ;
	   $result = $this->common->getOneRow('user',$where);
	   $data['profile_first_name']=$result['first_name'];
	   $data['profile_last_name']=$result['last_name'];
	   $data['profile_img']=$result['profile_img'];
	   $this->load->view('office_admin/view_regions',$data);
	}
	
	function delete_region($region_id) {
		$exist_count = $this->common->getCountOfField("id","cost_per_stage_per_region","where region_id ='".$region_id."'");
		$lawyers_count = $this->common->getCountOfField("lawyer_details_id","lawyer_details","where region_id ='".$region_id."'");
		if ($exist_count > 0 || $lawyers_count > 0) {
			redirect('lawyer_office_regions/view_regions/1');
		} else {
			$this->db->trans_begin();
			
			#$where = "region_id =".$region_id;
			#$this->common->deleteRecord('region_cities',$where);
			
			$where = "region_id =".$region_id;
			$this->common->deleteRecord('region',$where);
			
			if ($this->db->trans_status() === FALSE) {
	   			$this->db->trans_rollback();
			} else {
			    $this->db->trans_commit();
			}
	
			redirect('lawyer_office_regions');
		}
		
	}
	
	function add_edit_region($region_id=0) {
	$office_admin_id = $this->session->userdata('userid');
	$userRecord = $this -> common -> getOneRecField_Limit("lawyer_office_id", "user","where user_id ='$office_admin_id'");
	$lawyer_office_id = $userRecord->lawyer_office_id;
	
	$this -> load -> model("notification_model");
	   	$notifications = $this -> notification_model -> getLatestDeliveredNotificationsForUser($office_admin_id);
	   	$data['notifications_list'] = $notifications['notifications_list'];
		$data['count_unseen'] = $notifications['count_unseen'];
		$data['page'] = "lawyer_office_regions/add_edit_region/".$region_id;
		
        $data['region_id']=$region_id;
	    $data['name']="";
	    $data['lawyer_office_id'] = $lawyer_office_id;
		#$data['city_id']="";
		#$data['city_name']="";	
	   $where = "where user_id =".$office_admin_id  ;
	   $result = $this->common->getOneRow('user',$where);
	   $data['profile_first_name']=$result['first_name'];
	   $data['profile_last_name']=$result['last_name'];
	   $data['profile_img']=$result['profile_img'];  
	   		
		if(extract($_POST)) {			
			$value['name']= $_POST['name']; 
			$value['lawyer_office_id'] = $_POST['lawyer_office_id']; 
			#$city_value['region_city_id']= $_POST['region_city_id']; 			
						
			if($region_id > 0) {
				$where = "region_id =".$region_id;
				$this->common->updateRecord('region',$value,$where);
				
				#$where = "region_id =".$region_id;
				#$this->common->updateRecord('region_cities',$city_value,$where);
			} else {
				$new_region_id = $this->common->insertRecord('region',$value);
				#$city_value['region_id']= $new_region_id; 	
				#$this->common->insertRecord('region_cities',$city_value);
			}
			redirect('lawyer_office_regions');
		}
		
		if($region_id > 0) {
			$this -> load -> model("lawyer_office_model");
			$result = $this->lawyer_office_model->getRegionData($region_id);
			$data['name']=$result['name'];
	    	#$data['city_id']=$result['city_id'];
			#$data['city_name']=$result['city_name'];
				    	
		}
		
		#$data['available_cities']= $this -> common -> getAllRow("city", "");
				
		$this->load->view('office_admin/add_edit_region',$data);
	}
	
	function add_edit_region_list($region_id=0) {
		$office_admin_id = $this->session->userdata('userid');	
		$userRecord = $this -> common -> getOneRecField_Limit("lawyer_office_id", "user","where user_id ='$office_admin_id'");
		$lawyer_office_id = $userRecord->lawyer_office_id;
		
		$this -> load -> model("notification_model");
	   	$notifications = $this -> notification_model -> getLatestDeliveredNotificationsForUser($office_admin_id);
	   	$data['notifications_list'] = $notifications['notifications_list'];
		$data['count_unseen'] = $notifications['count_unseen'];
		$data['page'] = "lawyer_office_regions/add_edit_region_list/".$region_id;
		
        $data['region_id']=$region_id;
        $data['lawyer_office_id']=$lawyer_office_id;
	    $data['name']="";
		$where = "where user_id =".$office_admin_id  ;
	    $result = $this->common->getOneRow('user',$where);
	    $data['profile_first_name']=$result['first_name'];
	    $data['profile_last_name']=$result['last_name'];
	    $data['profile_img']=$result['profile_img'];  
	    $data['region_cities'] = '';
	   	$this -> load -> model("lawyer_office_model");
		if(extract($_POST)) {			
			$value['name']= $_POST['name']; 
			$value['lawyer_office_id'] = $lawyer_office_id;
			//$cities = $_POST['region_city_ids']; 	
			//$cities_array = explode(",", $cities);
			//$cities_array =$_POST['region_city_ids'];
				
			if($region_id > 0) {
				$this->db->trans_begin();
				$city_value['region_id']=$region_id;
				$where = "region_id =".$region_id;
				$this->common->updateRecord('region',$value,$where);
				
				/*$old_cities = $this->lawyer_office_model->getRegionData($region_id);
				$old_cities_array = array();
				if ($old_cities) {
					foreach($old_cities as $record) {
						if ($cities_array == null || !in_array($record['city_id'], $cities_array)) {
							//delete the old if not used
							$exist_count = $this->common->getCountOfField("id","cost_per_stage_per_region","where region_id ='".$region_id."'");
							$lawyers_count = $this->common->getCountOfField("lawyer_details_id","lawyer_details","where region_id ='".$region_id."'");
							if ($exist_count == 0 && $lawyers_count == 0) {
								$where = "region_id =".$region_id;
								$this->common->deleteRecord('region_cities',$where);
								
								$region_cities_count = $this->common->getCountOfField("region_id","region_cities","where region_id ='".$region_id."'");
								if ($region_cities_count == 0) {
									$where = "region_id =".$region_id;
									$this->common->deleteRecord('region',$where);
								}
							}
						} else {
							array_push($old_cities_array,$record['city_id']);
						}					
					} 
				}
				foreach($cities_array as $city_id) {
					if ($city_id != "" && !in_array($city_id, $old_cities_array) ) {
						$city_value['region_city_id']= $city_id;
						$this->common->insertRecord('region_cities',$city_value);
					}	
				}*/
				if ($this->db->trans_status() === FALSE) {
		   			$this->db->trans_rollback();
				} else {
				    $this->db->trans_commit();
				}
			} else {
				$this->db->trans_begin();
				
				$new_region_id = $this->common->insertRecord('region',$value);
				/*$city_value['region_id']= $new_region_id;
				foreach($cities_array as $city_id) {
					if ($city_id != "") {
						$city_value['region_city_id']= $city_id;
						$this->common->insertRecord('region_cities',$city_value);
					}	
				}*/
				if ($this->db->trans_status() === FALSE) {
		   			$this->db->trans_rollback();
				} else {
				    $this->db->trans_commit();
				}
			}
			redirect('lawyer_office_regions');
		}
		
		if($region_id > 0) {
			$this -> load -> model("lawyer_office_model");
			$result = $this->lawyer_office_model->getRegionData($region_id);
			$data['name']= $result[0]['name'];
			/*$region_cities = "";
			foreach($result as $city) {
				$region_cities .= $city['city_id'].",";
			}
			$data['region_cities'] = rtrim($region_cities, ",");*/
		}
		
		//$data['available_cities']= $this -> common -> getAllRow("city", "");
				
		$this->load->view('office_admin/add_edit_region',$data);
	}
	
	function getAllRegions() {
		$this->db->select ( 'region.region_id, region.name' );
		$this->db->from ( 'region' );
		$query = $this->db->get ();
		$regions = array ();
		$sum = 0;
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			$i = 0;
			foreach ( $records as $record ) {
				$regions [$i] ["region_id"] = $record ['region_id'];
				$regions [$i] ["name"] = $record ['name'];
				switch ($user_type_code) {
					case "ADMIN" :
						$regions  [$i] ["count"] = self::getCasesRegionCount ( $lawyer_office_id, $regions [$i] ["region_id"] );
						break;
					case "CUSTOMER" :
						$regions [$i] ["count"] = self::getCustomerCasesRegionCount ( $user_id, $regions [$i] ["region_id"] );
						break;
					case "LAWYER" :
						$regions [$i] ["count"] = self::getLawyerCasesRegionCount ( $user_id, $regions [$i] ["region_id"] );
						break;
					case "SECRETARY" :
						$regions [$i] ["count"] = self::getCasesRegionCount ( $lawyer_office_id, $regions [$i] ["region_id"] );
						break;
					default :
						$regions [$i] ["count"] = self::getCasesRegionCount ( $lawyer_office_id, $regions [$i] ["region_id"] );
				}
					
			}
			return $regions;
		}
		return false;
	}
}
?>