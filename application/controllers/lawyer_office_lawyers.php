<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class lawyer_office_lawyers extends CI_Controller
{
	function __construct()
    {
        parent::__construct();
        $this -> load -> model("common");
		$this->load->library('encrypt');
		if($this->session->userdata('userid')=='' || $this->session->userdata('user_type')!='ADMIN') {
			redirect('user_login','refresh');
		}
    }
    
	function index() {
	  
	   self::view_lawyers();
	   
	}

	function view_lawyers($delete_error='') {
	   $office_admin_id = $this->session->userdata('userid');
	   $userRecord = $this -> common -> getOneRecField_Limit("lawyer_office_id", "user","where user_id ='$office_admin_id'");
	   $lawyer_office_id = $userRecord->lawyer_office_id;	
	   $this -> load -> model("lawyer_office_model");
	   $data['lawyers'] = $this -> lawyer_office_model -> getLawyers($lawyer_office_id);
	   
	    $this -> load -> model("notification_model");
	   	$notifications = $this -> notification_model -> getLatestDeliveredNotificationsForUser($office_admin_id);
	   	$data['notifications_list'] = $notifications['notifications_list'];
		$data['count_unseen'] = $notifications['count_unseen'];
		$data['page'] = "lawyer_office_lawyers/view_lawyers/".$delete_error;
	   
	   $where = "where user_id =".$office_admin_id  ;
	   $result = $this->common->getOneRow('user',$where);
	   $data['profile_first_name']=$result['first_name'];
	   $data['profile_last_name']=$result['last_name'];
	   $data['profile_img']=$result['profile_img'];
	   
	   $data['delete_error'] = $delete_error;
	   $this->load->view('office_admin/view_lawyers',$data);
	}
	
	function delete_lawyer($user_id) {
		$exist_manager_count = $this->common->getCountOfField("lawyer_details_id","lawyer_details","where manager ='".$user_id."'");
		$exist_case_count = $this->common->getCountOfField("id","lawyer_case","where lawyer_id ='".$user_id."'");
		$exist_trans_count = $this->common->getCountOfField("id","case_transactions","where lawyer_id ='".$user_id."'");
		if ($exist_manager_count > 0 && ($exist_case_count || $exist_trans_count)) {
			redirect('lawyer_office_lawyers/view_lawyers/manager_case_exist');
		} else if ($exist_manager_count > 0) {
			redirect('lawyer_office_lawyers/view_lawyers/manager_exist');
		} else if ($exist_case_count > 0 || $exist_trans_count > 0) {
			redirect('lawyer_office_lawyers/view_lawyers/case_exist');
		}else {
			$this->db->trans_begin();
			
			$where = "lawyer_id =".$user_id;
			$this->common->deleteRecord('lawyer_details',$where);
			
			$where = "user_id =".$user_id;
			$this->common->deleteRecord('comment',$where);
			
			$where = "user_id =".$user_id;
			$this->common->deleteRecord('user_activities',$where);
			
			$where = "to_user_id =".$user_id;
			$this->common->deleteRecord('delivered_notifications',$where);
			
			$where = "user_id =".$user_id;
			$this->common->deleteRecord('user',$where);
			
			if ($this->db->trans_status() === FALSE) {
	   			 $this->db->trans_rollback();
			} else {
			    $this->db->trans_commit();
			}
			
			redirect('lawyer_office_lawyers/view_lawyers');
		}
		
	}
	
	function add_edit_lawyer($user_id=0, $email_already_exists=0, $user_name_exists=0) {
		
		$office_admin_id = $this->session->userdata('userid');
	    $userRecord = $this -> common -> getOneRecField_Limit("lawyer_office_id", "user","where user_id ='$office_admin_id'");
	    $lawyer_office_id = $userRecord->lawyer_office_id;
		
        $data['user_id']=$user_id;
        
           $where = "where user_id =".$office_admin_id  ;
	   $result = $this->common->getOneRow('user',$where);
	   $data['profile_first_name']=$result['first_name'];
	   $data['profile_last_name']=$result['last_name'];
	   $data['profile_img']=$result['profile_img'];
	   
	    $this -> load -> model("notification_model");
	   	$notifications = $this -> notification_model -> getLatestDeliveredNotificationsForUser($office_admin_id);
	   	$data['notifications_list'] = $notifications['notifications_list'];
		$data['count_unseen'] = $notifications['count_unseen'];
		$data['page'] = "lawyer_office_lawyers/add_edit_lawyer/".$user_id."/".$email_already_exists."/".$user_name_exists;
	   
	    $data['lawyer_office_id']=$lawyer_office_id;
		$data['first_name']="";	
		$data['last_name']="";
	    $data['mobile_number']="";
		$data['email']="";
		$data['user_type_code']="LAWYER";
	    $data['user_name']="";
	    $data['password']="";
		$data['manager']="";	
		$data['stage1_cost']="";
	    $data['stage2_cost']="";
		$data['stage3_cost']="";
		$data['stage4_cost']="";
	    $data['stage5_cost']="";
	    $data['region_ids']=array();
	    $data['email_exists_error']= $email_already_exists;
	    $data['username_exists_error']= $user_name_exists;
		   		
		if(extract($_POST)) {			
			$value['lawyer_office_id']=$lawyer_office_id; 
			$value['first_name']= $_POST['first_name']; 			
			$value['last_name']= $_POST['last_name'];
			$value['mobile_number']= $_POST['mobile_number']; 
			$value['email']= $_POST['email']; 			
			$value['user_type_code']= "LAWYER";
			$value['user_name']= $_POST['user_name']; 
			$value['password']= $this->encrypt->encode($_POST['password']);
			$details_value['lawyer_id']= $user_id;
			$details_value['manager']= $_POST['manager']; 
			$details_value['stage1_cost']= $_POST['stage1_cost']; 			
			$details_value['stage2_cost']= $_POST['stage2_cost'];
			$details_value['stage3_cost']= $_POST['stage3_cost']; 
			$details_value['stage4_cost']= $_POST['stage4_cost']; 			
			$details_value['stage5_cost']= $_POST['stage5_cost'];
			$region_ids= $_POST['region_ids'];
			$region_string = '';
			foreach ($region_ids as $region_id){
				$region_string = $region_string . $region_id .',';
			}
			$details_value['region_ids']=rtrim($region_string,",");
			if($user_id > 0) {
				$new_email = $value['email'];
				$email_count = $this->common->getCountOfField("user_id","user","where email ='".$new_email."' AND user_id !='".$user_id."'");
				$new_user_name = $value['user_name'];
				$user_name_count = $this->common->getCountOfField("user_id","user","where user_name ='".$new_user_name."' AND user_id !='".$user_id."'");
				if ($email_count == 0 && $user_name_count == 0) {
					$this->db->trans_begin();
					
					$where = "user_id =".$user_id;
					$this->common->updateRecord('user',$value,$where);
					
					$where = "lawyer_id =".$user_id;
					$this->common->updateRecord('lawyer_details',$details_value,$where);
					
					if ($this->db->trans_status() === FALSE) {
			   			 $this->db->trans_rollback();
					} else {
					    $this->db->trans_commit();
					}
					redirect('lawyer_office_lawyers');
				} else {
					$email_exists = 0;
					if ($email_count != 0) {
						$email_exists=1;
					}
					$user_name_exists = 0;
					if ($user_name_count != 0) {
						$user_name_exists=1;
					}
					self::lawyer_with_error($user_id,$lawyer_office_id,$value['first_name'],$value['last_name'],$value['mobile_number'],
					$value['email'], $value['user_name'],$value['password'],$details_value['manager'],$details_value['stage1_cost'],
					$details_value['stage2_cost'],$details_value['stage3_cost'],$details_value['stage4_cost'],$details_value['stage5_cost'],
					$details_value['region_ids'], $data['profile_first_name'],$data['profile_last_name'],$data['profile_img'],
					$email_exists, $user_name_exists, $data['notifications_list'], $data['count_unseen'], $data['page']);
				}
				
			} else {
				$new_email = $value['email'];
				$email_count = $this->common->getCountOfField("user_id","user","where email ='".$new_email."'");
				$new_user_name = $value['user_name'];
				$user_name_count = $this->common->getCountOfField("user_id","user","where user_name ='".$new_user_name."'");
				
				if ($email_count == 0 && $user_name_count == 0) {
					$this->db->trans_begin();
					
					$new_lawyer_id = $this->common->insertRecord('user',$value);
					$details_value['lawyer_id']= $new_lawyer_id;
					$this->common->insertRecord('lawyer_details',$details_value);
					
					if ($this->db->trans_status() === FALSE) {
			   			 $this->db->trans_rollback();
					} else {
					    $this->db->trans_commit();
					}
					redirect('lawyer_office_lawyers');
				} else {
					$email_exists = 0;
					if ($email_count != 0) {
						$email_exists=1;
					}
					$user_name_exists = 0;
					if ($user_name_count != 0) {
						$user_name_exists=1;
					}
					
					self::lawyer_with_error($user_id,$lawyer_office_id,$value['first_name'],$value['last_name'],$value['mobile_number'],
					$value['email'], $value['user_name'],$value['password'],$details_value['manager'],$details_value['stage1_cost'],
					$details_value['stage2_cost'],$details_value['stage3_cost'],$details_value['stage4_cost'],$details_value['stage5_cost'],
					$details_value['region_ids'], $data['profile_first_name'],$data['profile_last_name'],$data['profile_img'],
					$email_exists, $user_name_exists, $data['notifications_list'], $data['count_unseen'], $data['page']);
				}
				
			}
			
		} else {
			if($user_id > 0) {
				$where = "where user_id =".$user_id;
				$result = $this->common->getOneRow('user',$where);
				$data['lawyer_office_id']=$result['lawyer_office_id'];
		    	$data['first_name']=$result['first_name'];
				$data['last_name']=$result['last_name'];
				$data['mobile_number']=$result['mobile_number'];
		    	$data['email']=$result['email'];
				$data['user_type_code']=$result['user_type_code'];
				$data['user_name']=$result['user_name'];
		    	$data['password']=$this->encrypt->decode($result['password']);
		    	
		    	$where = "where lawyer_id =".$user_id;
				$details_result = $this->common->getOneRow('lawyer_details',$where);
				$data['manager']=$details_result['manager'];
		    	$data['stage1_cost']=$details_result['stage1_cost'];
				$data['stage2_cost']=$details_result['stage2_cost'];
				$data['stage3_cost']=$details_result['stage3_cost'];
		    	$data['stage4_cost']=$details_result['stage4_cost'];
				$data['stage5_cost']=$details_result['stage5_cost'];
				$data['regions_str'] =$details_result['region_ids'];
			}else{
				//check that number of lawyers accept to add a new one
				$this -> load -> model("lawyer_office_model");
				$lawyerOfficePackage = $this -> lawyer_office_model -> getLawyerOfficePackage($lawyer_office_id);
				$maxNumOfLawyers = $lawyerOfficePackage['num_of_lawyer'];
				$lawyers_count = $this -> lawyer_office_model -> getNumOflawyers($lawyer_office_id);
				
				if ($lawyers_count >= $maxNumOfLawyers) {
					redirect('lawyer_office_lawyers/view_lawyers/max_lawyer_count_reached');
				}
			}
			
			$data['available_regions']= $this -> common -> getAllRow("region", "where lawyer_office_id = '" . $lawyer_office_id."'");
			$this -> load -> model("lawyer_office_model");
			$data['available_managers']= $this -> lawyer_office_model -> getAvailableManagers($user_id, $lawyer_office_id);
					
			$this->load->view('office_admin/add_edit_lawyer',$data);
		}
		
		
	}
	
	function lawyer_with_error($user_id,$lawyer_office_id,$first_name,$last_name,$mobile_number,$email,$user_name,$password,
					$manager,$stage1_cost,$stage2_cost,$stage3_cost,$stage4_cost,$stage5_cost,$region_id,
					$profile_first_name,$profile_last_name,$profile_img,$email_exists, $user_name_exists,
					$notifications_list, $count_unseen, $page) {
		
		$data['profile_first_name']=$profile_first_name;
	    $data['profile_last_name']=$profile_last_name;
	    $data['profile_img']=$profile_img;
	    
	    $data['notifications_list'] = $notifications_list;
	    $data['count_unseen'] = $count_unseen;
	    $data['page'] = $page;
	    
	    $data['user_id']=$user_id;
	    $data['lawyer_office_id']=$lawyer_office_id;
		$data['first_name']=$first_name;	
		$data['last_name']=$last_name;
	    $data['mobile_number']=$mobile_number;
		$data['email']=$email;
		$data['user_type_code']="LAWYER";
	    $data['user_name']=$user_name;
	    $data['password']=$password;
	    $data['manager']=$manager;	
		$data['stage1_cost']=$stage1_cost;
	    $data['stage2_cost']=$stage2_cost;
		$data['stage3_cost']=$stage3_cost;
		$data['stage4_cost']=$stage4_cost;
	    $data['stage5_cost']=$stage5_cost;
	    $data['region_id']=$region_id;
		$data['email_exists_error']= $email_exists;
		$data['username_exists_error']= $user_name_exists;

		$data['available_regions']= $this -> common -> getAllRow("region", "where lawyer_office_id = '" . $lawyer_office_id."'");
		$this -> load -> model("lawyer_office_model");
		$data['available_managers']= $this -> lawyer_office_model -> getAvailableManagers($user_id, $lawyer_office_id);
					
		$this->load->view('office_admin/add_edit_lawyer',$data);
	}
	
}
?>