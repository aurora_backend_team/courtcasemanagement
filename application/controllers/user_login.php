<?php

if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class user_login extends CI_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->model ( "common" );
		$this->load->library('encrypt');
	}
	function index() {
		$data ['msg'] = "";
		if ($this->session->userdata ( 'userid' ) != '') {
			$user_id = $this->session->userdata ( 'userid' );
			self::insert_activity ( $user_id, 'تسجيل دخول' );
			$userRecord = $this->common->getOneRecField_Limit ( "user_type_code", "user", "where user_id ='$user_id'" );
			$user_type_code = $userRecord->user_type_code;
			switch ($user_type_code) {
				case "ADMIN" :
					// echo 'user data not null';
					redirect ( 'lawyer_office_panel', 'refresh' );
					break;
				case "CUSTOMER" :
					// redirect('user_profile','refresh');
					redirect ( 'case_management', 'refresh' );
					break;
				case "LAWYER" :
					// redirect('user_profile','refresh');
					redirect ( 'case_management', 'refresh' );
					break;
				case "SECRETARY" :
					// redirect('user_profile','refresh');
					redirect ( 'case_management', 'refresh' );
					break;
				default :
					// redirect('user_profile','refresh');
					$redirect ( 'case_management', 'refresh' );
			}
		} else {
			$data ['user_name'] = '';
			$this->load->view ( 'user_login', $data );
		}
	}
	function login() {
		if (extract ( $_POST )) {
			
			$username = $this->common->mysql_safe_string ( $this->input->post ( 'user_name' ) );
			
			$password = $this->common->mysql_safe_string ( $this->input->post ( 'password' ) );
			
			if ($username != "" && $password != "") {
				$table = "user";
				$where = "where user_name='" . $username . "'";# and password='" . $password . "'";
				$row_user = $this->common->getOneRow ( $table, $where );
				if (isset($row_user)&& isset($row_user['password']) && $password == $this->encrypt->decode($row_user['password'])) {
					/*$table = "user";
					$where = "where user_name='" . $username . "' and password='" . $password . "'";
					$row_user = $this->common->getOneRow ( $table, $where );*/
					
					// $update['gcm_id'] = $_POST['token'];
					$user_id = $row_user ['user_id'];
					// $this -> common -> updateRecord("user",$update,"user_id ='$user_id'");
					
					$userdata = array (
							'userid' => $row_user ['user_id'],
							'user_name' => $row_user ['user_name'],
							'user_logged' => TRUE,
							'user_type' => $row_user ['user_type_code'] 
					);
					$this->session->set_userdata ( $userdata );
					$data ['wrong'] = "";
					redirect ( 'user_login', 'refresh' );
				} else {
					$data ['wrong'] = "Username or password is  incorrect.";
					$this->load->view ( 'user_login', $data );
				}
			} else {
				$data ['msg'] = "Required fields cannot be left blank.";
				$this->load->view ( 'user_login', $data ); // Executes if username is entered incorrect.
			}
		} else {
			$data ['msg'] = "Required fields cannot be left blank.";
			$this->load->view ( 'user_login', $data );
		}
	}
	function logoff() {
		
		// $update['gcm_id'] = "";
		$user_id = $this->session->userdata ( 'userid' );
		// $this -> common -> updateRecord("user",$update,"user_id ='$user_id'");
		$array_items = array (
				'userid' => '',
				'user_name' => '',
				'user_logged' => '',
				'user_type' => '' 
		);
		$this->session->unset_userdata ( $array_items );
		$userdata = array (
				'userid' => '',
				'user_name' => '',
				'user_logged' => FALSE,
				'user_type' => '' 
		);
		$this->session->set_userdata ( $userdata );
		self::insert_activity ( $user_id, 'تسجيل خروج' );
		redirect ( 'user_login', 'refresh' );
	}
	function insert_activity($user_id, $activity) {
		$values = array ();
		$values ['user_id'] = $user_id;
		$values ['activity'] = $activity;
		$this->load->library ( '../controllers/util' );
		$values ['user_IP'] = $this->util->get_client_ip ();
		// $values['user_IP'] = self::get_client_ip();
		date_default_timezone_set ( 'Asia/Riyadh' );
		$current_date_time = date ( "Y-m-d H:i:s" );
		$values ['date_time'] = $current_date_time;
		$this->common->insertRecord ( 'user_activities', $values );
	}
	function resendPassword() {
		if (extract ( $_POST )) {
			$user_name = $_POST ['user_name'];
			$table = "user";
			$where = "where user_name='" . $user_name . "' or email = '". $user_name."'" ;
			$row_user = $this->common->getOneRow ( $table, $where );
			if (isset ( $row_user ) && isset($row_user ['first_name'])) {
				$sms_message = 'بيانات الدخول على حسابك في برنامج قضاء التنفيذ هي:'.
						'%0a'.
						'اسم المستخدم:'.
								'%0a'.
				 $user_name.
				 '%0a'.
				'كلمة السر:'
				 		.'%0a'
				. $this->encrypt->decode($row_user ['password']) ;
				
				$result = $this->common->sendSMS ( 'Y', $row_user ['mobile_number'], $sms_message );
				$data["success"] = "1";
				$this->load->view('forget_password',$data);
			} else {
				$data["wrong"] = "1";
				$this->load->view('forget_password',$data);
			}
		} else {
			$data["user_name"] = "";
			$this->load->view('forget_password',$data);
		}
	}
}

