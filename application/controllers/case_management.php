<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
	class case_management extends CI_Controller {
		function __construct() {
			parent::__construct ();
			$this->load->model ( "common" );
			$this->load->model ( "case_model" );
			if ($this->session->userdata ( 'userid' ) == '') {
				redirect ( 'user_login', 'refresh' );
			}
		}
		function index($status = '0') {
			self::view_cases ( $status );
		}
		function view_cases($status = '0', $search = 0, $page_number = 1, $pagination = 0, $region = '0') {
			$this->load->model ( "notification_model" );
			$search_mode = '0';
			
			// Advanced Search
			if ($search == 1) {
				self::open_search_dialog ();
				// اعادة البحث
			} else if ($search == 2) {
				$search_data = array ();
				if (extract ( $_POST )) {
					// $search_data for send to model to update with gerogian dates
					$search_mode = $_POST ['search_mode'];
					$search_data ['customer_id'] = $_POST ['customer_id'];
					$search_data ['contract_number'] = $_POST ['contract_number'];
					$search_data ['debtor_name'] = $_POST ['debtor_name'];
					$search_data ['client_name'] = $_POST ['client_name'];
					$search_data ['client_id_number'] = $_POST ['client_id_number'];
					$search_data ['client_type'] = $_POST ['client_type'];
					$search_data ['obligation_value'] = $_POST ['obligation_value'];
					$search_data ['number_of_late_instalments'] = $_POST ['number_of_late_instalments'];
					$search_data ['due_amount'] = $_POST ['due_amount'];
					$search_data ['remaining_amount'] = $_POST ['remaining_amount'];
					$search_data ['total_debenture_amount'] = $_POST ['total_debenture_amount'];
					$search_data ['creation_date'] = $_POST ['creation_date'];
					$search_data ['executive_order_number'] = $_POST ['executive_order_number'];
					$search_data ['order_number'] = $_POST ['order_number'];
					$search_data ['circle_number'] = $_POST ['circle_number'];
					$search_data ['decision_34_number'] = $_POST ['decision_34_number'];
					$search_data ['decision_34_date'] = $_POST ['decision_34_date'];
					$search_data ['decision_46_date'] = $_POST ['decision_46_date'];
					$search_data ['state_code'] = $_POST ['state_code'];
					$search_data ['region_id'] = $_POST ['region_id'];
					$search_data ['lawyer_id'] = $_POST ['lawyer_id'];
					$search_data ['executive_order_date'] = $_POST ['executive_order_date'];
					$search_data ['advertisement_date'] = $_POST ['advertisement_date'];
					$search_data ['suspend_reason_code'] = $_POST ['suspend_reason_code'];
					$search_data ['suspend_date'] = $_POST ['suspend_date'];
					$search_data ['closing_reason'] = $_POST ['closing_reason'];
					$search_data ['case_id'] = $_POST ['case_id'];
				}
				// $search_values sent to view to have any old values in case of تعديل البحث with hijri date
				$search_values = self::getSearchValuesFromSearchData ( $search_data );
				self::open_search_dialog_for_edit ( $search_values );
			} else {
				$this->load->model ( "notification_model" );
				$this->load->model ( "case_model" );
				$search_data = array ();
				if (extract ( $_POST )) {
					$this->load->library ( '../controllers/dates_conversion' );
					$search_mode = $_POST ['search_mode'];
					$search_data ['customer_id'] = $_POST ['customer_id'];
					$search_data ['contract_number'] = $_POST ['contract_number'];
					$search_data ['debtor_name'] = $_POST ['debtor_name'];
					$search_data ['client_name'] = $_POST ['client_name'];
					$search_data ['client_id_number'] = $_POST ['client_id_number'];
					$search_data ['client_type'] = $_POST ['client_type'];
					$search_data ['obligation_value'] = $_POST ['obligation_value'];
					$search_data ['number_of_late_instalments'] = $_POST ['number_of_late_instalments'];
					$search_data ['due_amount'] = $_POST ['due_amount'];
					$search_data ['remaining_amount'] = $_POST ['remaining_amount'];
					$search_data ['total_debenture_amount'] = $_POST ['total_debenture_amount'];
					$search_data ['creation_date'] = $_POST ['creation_date'];
					$search_data ['executive_order_number'] = $_POST ['executive_order_number'];
					$search_data ['order_number'] = $_POST ['order_number'];
					$search_data ['circle_number'] = $_POST ['circle_number'];
					$search_data ['decision_34_number'] = $_POST ['decision_34_number'];
					$search_data ['decision_34_date'] = $_POST ['decision_34_date'];
					$search_data ['decision_46_date'] = $_POST ['decision_46_date'];
					$search_data ['state_code'] = $_POST ['state_code'];
					$search_data ['region_id'] = $_POST ['region_id'];
					$search_data ['lawyer_id'] = $_POST ['lawyer_id'];
					$search_data ['executive_order_date'] = $_POST ['executive_order_date'];
					$search_data ['advertisement_date'] = $_POST ['advertisement_date'];
					$search_data ['suspend_reason_code'] = $_POST ['suspend_reason_code'];
					$search_data ['closing_reason'] = $_POST ['closing_reason'];
					$search_data ['case_id'] = $_POST ['case_id'];
				}
				$data ['search_values'] = self::getSearchValuesFromSearchData ( $search_data );
				if (extract ( $_POST )) {
					$search_data ['creation_date'] = $this->dates_conversion->HijriToGregorian ( $_POST ['creation_date'] );
					$search_data ['decision_34_date'] = $this->dates_conversion->HijriToGregorian ( $_POST ['decision_34_date'] );
					$search_data ['decision_46_date'] = $this->dates_conversion->HijriToGregorian ( $_POST ['decision_46_date'] );
					$search_data ['executive_order_date'] = $this->dates_conversion->HijriToGregorian ( $_POST ['executive_order_date'] );
					$search_data ['advertisement_date'] = $this->dates_conversion->HijriToGregorian ( $_POST ['advertisement_date'] );
				}
				if ($this->session->userdata ( 'userid' ) != '') {
					$user_id = $this->session->userdata ( 'userid' );
					$where = "where user_id =" . $user_id;
					$userRecord = $this->common->getOneRow ( 'user', $where );
					$lawyer_office_id = $userRecord ['lawyer_office_id'];
					$data ['profile_first_name'] = $userRecord ['first_name'];
					$data ['profile_last_name'] = $userRecord ['last_name'];
					$data ['profile_img'] = $userRecord ['profile_img'];
					
					$notifications = $this->notification_model->getLatestDeliveredNotificationsForUser ( $user_id );
					$data ['notifications_list'] = $notifications ['notifications_list'];
					$data ['count_unseen'] = $notifications ['count_unseen'];
					$data ['page'] = "case_management/view_cases/" . $status;
					
					$data ['search_mode'] = $search_mode;
					
					$user_type_code = $userRecord ['user_type_code'];
					$data ['user_type_code'] = $user_type_code;
					$data ['cases_count'] = 0;
					
					// to get the add button name from state transition
					$initialState = $this->case_model->getInitialStateData ( $user_type_code );
					if ($initialState) {
						$data ['can_add'] = 1;
						$data ['add_button_name'] = $initialState;
					} else {
						$data ['can_add'] = 0;
						$data ['add_button_name'] = "";
					}
					$case_region_count = $this->case_model->getAllRegions ( $user_type_code, $lawyer_office_id, $user_id );
					$data ['case_region_count'] = $case_region_count;
					$data ['selected_region'] = $region;
					$data ['selected_state'] = $status;
					$ordered_states = $this->case_model->getAllOrderedStates ();
					$data ['ordered_states'] = $ordered_states;
					foreach ( $ordered_states as $state ) {
						$state_code = $state ['state_code'];
						$state_count_name = $state_code . "_count";
						$data [$state_count_name] = 0;
						switch ($user_type_code) {
							case "ADMIN" :
								$data [$state_count_name] = $this->case_model->getCasesStatusCount ( $lawyer_office_id, $state_code );
								break;
							case "CUSTOMER" :
								$data [$state_count_name] = $this->case_model->getCustomerCasesStatusCount ( $user_id, $state_code );
								break;
							case "LAWYER" :
								$data [$state_count_name] = $this->case_model->getLawyerCasesStatusCount ( $user_id, $lawyer_office_id, $state_code );
								break;
							case "SECRETARY" :
								$data [$state_count_name] = $this->case_model->getCasesStatusCount ( $lawyer_office_id, $state_code );
								break;
							default :
								$data [$state_count_name] = $this->case_model->getCasesStatusCount ( $lawyer_office_id, $state_code );
						}
					}
					
					switch ($user_type_code) {
						case "ADMIN" :
							$admin_cases = $this->case_model->getAllCases ( $lawyer_office_id, $user_type_code, $status, $region, $search_data, $page_number, $pagination );
							$count_all = 0;
							if ($admin_cases) {
								if (isset ( $admin_cases [0] ['count_all'] )) {
									$count_all = $admin_cases [0] ['count_all'];
								}
								self::array_dual_sort_by_column ( $admin_cases, "number_of_late_instalments", "creation_date", SORT_DESC, SORT_ASC );
							}
							$data ['cases'] = $admin_cases;
							$data ['cases_count'] = $this->case_model->getAllCasesCount ( $lawyer_office_id, $user_type_code, $status, $search_data, $page_number, $pagination, $region );
							$data ['cases_count_all'] = $count_all;
							break;
						case "CUSTOMER" :
							$customer_cases = $this->case_model->getCustomerCases ( $user_id, $user_type_code, $status, $region, $search_data, $page_number, $pagination );
							$count_all = 0;
							if ($customer_cases) {
								if (isset ( $customer_cases [0] ['count_all'] )) {
									$count_all = $customer_cases [0] ['count_all'];
								}
								self::array_dual_sort_by_column ( $customer_cases, "number_of_late_instalments", "creation_date", SORT_DESC, SORT_ASC );
							}
							$data ['cases'] = $customer_cases;
							$data ['cases_count'] = $this->case_model->getCustomerCasesCount ( $user_id, $user_type_code, $status, $search_data, $page_number, $pagination, $region );
							$data ['cases_count_all'] = $count_all;
							break;
						case "LAWYER" :
							$lawyer_cases = $this->case_model->getLawyerCases ( $user_id, $user_type_code, $status, $region, $search_data, $page_number, $pagination );
							$count_all = 0;
							if ($lawyer_cases) {
								if (isset ( $lawyer_cases [0] ['count_all'] )) {
									$count_all = $lawyer_cases [0] ['count_all'];
								}
								self::array_multi_sort_by_column ( $lawyer_cases, "state_priotity", "number_of_late_instalments", "creation_date", SORT_DESC, SORT_DESC, SORT_ASC );
							}
							$data ['cases'] = $lawyer_cases;
							$data ['cases_count'] = $this->case_model->getLawyerCasesCount ( $user_id, $user_type_code, $status, $search_data, $page_number, $pagination, $region );
							$data ['cases_count_all'] = $count_all;
							break;
						case "SECRETARY" :
							$secretary_cases = $this->case_model->getAllCases ( $lawyer_office_id, $user_type_code, $status, $region, $search_data, $page_number, $pagination );
							$count_all = 0;
							if ($secretary_cases) {
								if (isset ( $secretary_cases [0] ['count_all'] )) {
									$count_all = $secretary_cases [0] ['count_all'];
								}
								self::array_dual_sort_by_column ( $secretary_cases, "number_of_late_instalments", "creation_date", SORT_DESC, SORT_ASC );
							}
							$data ['cases'] = $secretary_cases;
							$data ['cases_count'] = $this->case_model->getAllCasesCount ( $lawyer_office_id, $user_type_code, $status, $search_data, $page_number, $pagination, $region );
							$data ['cases_count_all'] = $count_all;
							break;
						default :
							$all_cases = $this->case_model->getAllCases ( $lawyer_office_id, $user_type_code, $status, $region, $search_data, $page_number, $pagination );
							$count_all = 0;
							if ($all_cases) {
								if (isset ( $all_cases [0] ['count_all'] )) {
									$count_all = $all_cases [0] ['count_all'];
								}
								self::array_dual_sort_by_column ( $all_cases, "number_of_late_instalments", "creation_date", SORT_DESC, SORT_ASC );
							}
							$data ['cases'] = $all_cases;
							$data ['cases_count'] = $this->case_model->getAllCasesCount ( $lawyer_office_id, $user_type_code, $status, $search_data, $page_number, $pagination, $region );
							$data ['cases_count_all'] = $count_all;
					}
					
					if ($data ['cases']) {
						$i = 0;
						$this->load->library ( '../controllers/dates_conversion' );
						foreach ( $data ['cases'] as $record ) {
							$dates_conversion = new dates_conversion ();
							$data ['cases'] [$i] ['creation_date'] = $this->dates_conversion->GregorianToHijri ( $record ['creation_date'] );
							
							$data ['cases'] [$i] ['last_update_date'] = $this->dates_conversion->GregorianToHijri ( $record ['last_update_date'] );
							if ($record ['decision_34_date'] != NULL && $record ['decision_34_date'] != "000-00-00") {
								$data ['cases'] [$i] ['decision_34_date'] = $this->dates_conversion->GregorianToHijri ( $record ['decision_34_date'] );
							}
							if ($record ['decision_46_date'] != NULL && $record ['decision_46_date'] != "000-00-00") {
								$data ['cases'] [$i] ['decision_46_date'] = $this->dates_conversion->GregorianToHijri ( $record ['decision_46_date'] );
							}
							if ($record ['executive_order_date'] != NULL && $record ['executive_order_date'] != "000-00-00") {
								$data ['cases'] [$i] ['executive_order_date'] = $this->dates_conversion->GregorianToHijri ( $record ['executive_order_date'] );
							}
							if ($record ['advertisement_date'] != NULL && $record ['advertisement_date'] != "000-00-00") {
								$data ['cases'] [$i] ['advertisement_date'] = $this->dates_conversion->GregorianToHijri ( $record ['advertisement_date'] );
							}
							
							$case_transactions = $this->case_model->getCaseTransactions ( $data ['cases'] [$i] ["case_id"], $lawyer_office_id );
							$last_state_date = '';
							if ($case_transactions) {
								$index = 0;
								foreach ( $case_transactions as $record ) {
									if ($record ['state_date'] != null && $record ['state_date'] != "") {
										$case_transactions [$index] ['state_date'] = $this->dates_conversion->GregorianToHijri ( $record ['state_date'] );
										$last_state_date = $case_transactions [$index] ['state_date'];
									}
									$index ++;
								}
							}
							$data ['cases'] [$i] ['last_trans_date'] = $last_state_date;
							
							$i ++;
						}
					}
					if ($pagination == 0) {
						$this->load->view ( 'cases/view_cases', $data );
					} else {
						echo json_encode ( $data );
					}
				} else {
					$data ['user_name'] = '';
					$this->load->view ( 'user_login', $data );
				}
			}
		}
		function quick_search_cases($status = "0", $page_number = 1, $pagination = 0) {
			$this->load->model ( "notification_model" );
			$this->load->model ( "case_model" );
			if ($status == '0' || $status === 0) {
				$status = '';
			}
			$search_query = "";
			$search_data = array ();
			if (extract ( $_POST )) {
				$this->load->library ( '../controllers/dates_conversion' );
				$search_query = $_POST ['query'];
				
			}
			$data ['search_values'] = self::getSearchValuesFromSearchData ( $search_data );
			if ($this->session->userdata ( 'userid' ) != '') {
				$user_id = $this->session->userdata ( 'userid' );
				$where = "where user_id =" . $user_id;
				$userRecord = $this->common->getOneRow ( 'user', $where );
				$lawyer_office_id = $userRecord ['lawyer_office_id'];
				$data ['profile_first_name'] = $userRecord ['first_name'];
				$data ['profile_last_name'] = $userRecord ['last_name'];
				$data ['profile_img'] = $userRecord ['profile_img'];
				
				$notifications = $this->notification_model->getLatestDeliveredNotificationsForUser ( $user_id );
				$data ['notifications_list'] = $notifications ['notifications_list'];
				$data ['count_unseen'] = $notifications ['count_unseen'];
				$data ['page'] = "case_management/view_cases/" . $status;
				
				$data ['search_mode'] = "quick_search";
				$data ['quick_search_query'] = $search_query;
				
				$user_type_code = $userRecord ['user_type_code'];
				$data ['user_type_code'] = $user_type_code;
				$data ['cases_count'] = 0;
				$cases_data = array ();
				$search_result = array ();
				/*
				 * $initialState = $this->case_model->getInitialStateData($user_type_code);
				 * if ($initialState) {
				 * $data['can_add'] = 1;
				 * $data['add_button_name'] = $initialState;
				 * } else {
				 * $data['can_add'] = 0;
				 * $data['add_button_name'] = "";
				 * }
				 * $data['selected_state'] = "";
				 * $ordered_states = $this -> case_model -> getAllOrderedStates();
				 * $data['ordered_states']= $ordered_states;
				 * foreach ($ordered_states as $state) {
				 * $state_code = $state['state_code'];
				 * $state_count_name = $state_code."_count";
				 * $data[$state_count_name] = 0;
				 * switch ($user_type_code) {
				 * case "ADMIN":
				 * $data[$state_count_name] = $this->case_model->getCasesStatusCount($lawyer_office_id, $state_code);
				 * break;
				 * case "CUSTOMER":
				 * $data[$state_count_name] = $this->case_model->getCustomerCasesStatusCount($user_id, $state_code);
				 * break;
				 * case "LAWYER":
				 * $data[$state_count_name] = $this->case_model->getLawyerCasesStatusCount($user_id, $state_code);
				 * break;
				 * case "SECRETARY":
				 * $data[$state_count_name] = $this->case_model->getCasesStatusCount($lawyer_office_id, $state_code);
				 * break;
				 * default:
				 * $data[$state_count_name] = $this->case_model->getCasesStatusCount($lawyer_office_id, $state_code);
				 * }
				 *
				 * }
				 */
				$quick_search = 1;
				switch ($user_type_code) {
					case "ADMIN" :
						$admin_cases = $this->case_model->getAllCases ( $lawyer_office_id, $user_type_code, $status, null, $search_data, $page_number, $pagination, $quick_search,$search_query );
						if ($admin_cases) {
							self::array_dual_sort_by_column ( $admin_cases, "number_of_late_instalments", "creation_date", SORT_DESC, SORT_ASC );
						}
						$cases_data = $admin_cases;
						$data ['cases_count'] = count($admin_cases);
						$data ['cases_count_all'] = $this->case_model->getAllCasesCount ( $lawyer_office_id, $user_type_code, $status, $search_data, $page_number, $pagination, null,$quick_search,$search_query );
						break;
					case "CUSTOMER" :
						$customer_cases = $this->case_model->getCustomerCases ( $user_id, $user_type_code, $status, null, $search_data, $page_number, $pagination, $quick_search,$search_query  );
						if ($customer_cases) {
							self::array_dual_sort_by_column ( $customer_cases, "number_of_late_instalments", "creation_date", SORT_DESC, SORT_ASC );
						}
						$cases_data = $customer_cases;
						$data ['cases_count'] = count($customer_cases);
						$data ['cases_count_all'] = $this->case_model->getCustomerCasesCount ( $user_id, $user_type_code, $status, $search_data, $page_number, $pagination, null ,$quick_search,$search_query );
						break;
					case "LAWYER" :
						$lawyer_cases = $this->case_model->getLawyerCases ( $user_id, $user_type_code, $status, null, $search_data, $page_number, $pagination, $quick_search,$search_query  );
						if ($lawyer_cases)
							self::array_multi_sort_by_column ( $lawyer_cases, "state_priotity", "number_of_late_instalments", "creation_date", SORT_DESC, SORT_DESC, SORT_ASC );
							
							$cases_data = $lawyer_cases;
							$data ['cases_count'] = count($lawyer_cases);
							$data ['cases_count_all'] = $this->case_model->getLawyerCasesCount ( $user_id, $user_type_code, $status, $search_data, $page_number, $pagination, null,$quick_search,$search_query );
							break;
					case "SECRETARY" :
						$secretary_cases = $this->case_model->getAllCases ( $lawyer_office_id, $user_type_code, $status, null, $search_data, $page_number, $pagination, $quick_search,$search_query  );
						if ($secretary_cases) {
							self::array_dual_sort_by_column ( $secretary_cases, "number_of_late_instalments", "creation_date", SORT_DESC, SORT_ASC );
						}
						$cases_data = $secretary_cases;
						$data ['cases_count'] = count($secretary_cases);
						$data ['cases_count_all'] = $this->case_model->getAllCasesCount ( $lawyer_office_id, $user_type_code, $status, $search_data, $page_number, $pagination, null,$quick_search,$search_query );
						break;
					default :
						$all_cases = $this->case_model->getAllCases ( $lawyer_office_id, $user_type_code, $status, null, $search_data, $page_number, $pagination );
						if ($all_cases) {
							self::array_dual_sort_by_column ( $all_cases, "number_of_late_instalments", "creation_date", SORT_DESC, SORT_ASC );
						}
						$cases_data = $all_cases;
						$data ['cases_count'] = count($all_cases);
						$data ['cases_count_all'] = $this->case_model->getAllCasesCount ( $lawyer_office_id, $user_type_code, $status, $search_data, $page_number, $pagination, null,$quick_search,$search_query );
				}
				
				if ($cases_data) {
					$i = 0;
					$this->load->library ( '../controllers/dates_conversion' );
					foreach ( $cases_data as $record ) {
						$dates_conversion = new dates_conversion ();
						$cases_data [$i] ['creation_date'] = $this->dates_conversion->GregorianToHijri ( $record ['creation_date'] );
						
						$cases_data [$i] ['last_update_date'] = $this->dates_conversion->GregorianToHijri ( $record ['last_update_date'] );
						if ($record ['decision_34_date'] != NULL && $record ['decision_34_date'] != "000-00-00") {
							$cases_data [$i] ['decision_34_date'] = $this->dates_conversion->GregorianToHijri ( $record ['decision_34_date'] );
						}
						if ($record ['decision_46_date'] != NULL && $record ['decision_46_date'] != "000-00-00") {
							$cases_data [$i] ['decision_46_date'] = $this->dates_conversion->GregorianToHijri ( $record ['decision_46_date'] );
						}
						if ($record ['executive_order_date'] != NULL && $record ['executive_order_date'] != "000-00-00") {
							$cases_data [$i] ['executive_order_date'] = $this->dates_conversion->GregorianToHijri ( $record ['executive_order_date'] );
						}
						if ($record ['advertisement_date'] != NULL && $record ['advertisement_date'] != "000-00-00") {
							$cases_data [$i] ['advertisement_date'] = $this->dates_conversion->GregorianToHijri ( $record ['advertisement_date'] );
						}
						
						/*$case_transactions = $this->case_model->getCaseTransactions ( $cases_data [$i] ["case_id"], $lawyer_office_id );
						
						if ($case_transactions) {
						$index = 0;
						foreach ( $case_transactions as $record ) {
						if ($record ['state_date'] != null && $record ['state_date'] != "") {
						$case_transactions [$index] ['state_date'] = $this->dates_conversion->GregorianToHijri ( $record ['state_date'] );
						$last_state_date = $case_transactions [$index] ['state_date'];
						}
						$index ++;
						}
						}*/
						$last_state_date = $this->case_model->get_last_state_date ($cases_data [$i] ["case_id"],$lawyer_office_id);
						if ($last_state_date != null){
							$cases_data [$i] ['last_trans_date'] = $this->dates_conversion->GregorianToHijri ($last_state_date);
						} else {
							$cases_data [$i] ['last_trans_date'] ="";
						}
						// start quick search
						/*$current_state_code = $cases_data [$i] ['current_state_code'];
						 $state_record = $this->common->getOneRecField_Limit ( "state_name", "state", "where state_code = '" . $current_state_code . "'" );
						 $current_state_name = $state_record->state_name;
						 $region_id = $cases_data [$i] ['region_id'];
						 if ($region_id != null && $region_id != "") {
						 $region_record = $this->common->getOneRecField_Limit ( "name", "region", "where region_id = '" . $region_id . "'" );
						 $region_name = $region_record->name;
						 }
						 if ($search_query != "") {
						 if (strpos ( $cases_data [$i] ['customer_name'], $search_query ) !== false || strpos ( $cases_data [$i] ['contract_number'], $search_query ) !== false || strpos ( $cases_data [$i] ['debtor_name'], $search_query ) !== false || strpos ( $cases_data [$i] ['client_name'], $search_query ) !== false || strpos ( $cases_data [$i] ['client_id_number'], $search_query ) !== false || strpos ( $cases_data [$i] ['client_type'], $search_query ) !== false || strpos ( $cases_data [$i] ['obligation_value'], $search_query ) !== false || strpos ( $cases_data [$i] ['number_of_late_instalments'], $search_query ) !== false || strpos ( $cases_data [$i] ['due_amount'], $search_query ) !== false || strpos ( $cases_data [$i] ['remaining_amount'], $search_query ) !== false || strpos ( $cases_data [$i] ['total_debenture_amount'], $search_query ) !== false || strpos ( $cases_data [$i] ['executive_order_number'], $search_query ) !== false || strpos ( $cases_data [$i] ['order_number'], $search_query ) !== false || strpos ( $cases_data [$i] ['circle_number'], $search_query ) !== false || strpos ( $cases_data [$i] ['decision_34_number'], $search_query ) !== false || strpos ( $cases_data [$i] ['lawyer_name'], $search_query ) !== false || strpos ( $cases_data [$i] ['suspend_reason'], $search_query ) !== false || strpos ( $cases_data [$i] ['closing_reason'], $search_query ) !== false || strpos ( $cases_data [$i] ['case_id'], $search_query ) !== false || strpos ( $cases_data [$i] ['creation_date'], $search_query ) !== false || strpos ( $cases_data [$i] ['decision_34_date'], $search_query ) !== false || strpos ( $cases_data [$i] ['decision_46_date'], $search_query ) !== false || strpos ( $cases_data [$i] ['executive_order_date'], $search_query ) !== false || strpos ( $cases_data [$i] ['advertisement_date'], $search_query ) !== false || strpos ( $cases_data [$i] ['last_update_date'], $search_query ) !== false || strpos ( $current_state_name, $search_query ) !== false || ($region_id != null && $region_id != "" && strpos ( $region_name, $search_query ) !== false)) {
						 // continue;
						 array_push ( $search_result, $cases_data [$i] );
						 }
						 }*/
						// end quick search
						
						$i ++;
					}
					/*
					 * $page_result = array();
					 * $offset = ($page_number*50)-50;
					 * $limit = ($offset + 50) < count($search_result)? ($offset + 50):count($search_result);
					 * for ($x = $offset; $x < $limit; $x++) {
					 * array_push($page_result,$search_result[$x]);
					 * }
					 *
					 * $data['cases'] = $page_result;
					 */
					$data ['cases'] = $cases_data;
					$data ['cases_count'] = count ( $cases_data );
				}
				if ($pagination == 0) {
					$this->load->view ( 'cases/view_cases', $data );
				} else {
					echo json_encode ( $data );
				}
			} else {
				$data ['user_name'] = '';
				$this->load->view ( 'user_login', $data );
			}
		}
		function getSearchValuesFromSearchData($search_data) {
			$search_values = array ();
			if (count ( $search_data ) == 0) {
				$search_data ["customer_id"] = "";
				$search_data ["contract_number"] = "";
				$search_data ["debtor_name"] = "";
				$search_data ["client_name"] = "";
				$search_data ["client_id_number"] = "";
				$search_data ["client_type"] = "";
				$search_data ["obligation_value"] = "";
				$search_data ["number_of_late_instalments"] = "";
				$search_data ["due_amount"] = "";
				$search_data ["remaining_amount"] = "";
				$search_data ['total_debenture_amount'] = "";
				$search_data ["creation_date"] = "";
				$search_data ["executive_order_number"] = "";
				$search_data ['order_number'] = "";
				$search_data ["circle_number"] = "";
				$search_data ["decision_34_number"] = "";
				$search_data ["decision_34_date"] = "";
				$search_data ["decision_46_date"] = "";
				$search_data ["state_code"] = "";
				$search_data ["region_id"] = "";
				$search_data ["lawyer_id"] = "";
				$search_data ["executive_order_date"] = "";
				$search_data ["advertisement_date"] = "";
				$search_data ["suspend_reason_code"] = "";
				$search_data ["suspend_date"] = "";
				$search_data ["closing_reason"] = "";
				$search_data ["case_id"] = "";
			}
			$search_values ['customer_id'] = $search_data ['customer_id'];
			$search_values ['contract_number'] = $search_data ['contract_number'];
			$search_values ['debtor_name'] = $search_data ['debtor_name'];
			$search_values ['client_name'] = $search_data ['client_name'];
			$search_values ['client_id_number'] = $search_data ['client_id_number'];
			$search_values ['client_type'] = $search_data ['client_type'];
			$search_values ['obligation_value'] = $search_data ['obligation_value'];
			$search_values ['number_of_late_instalments'] = $search_data ['number_of_late_instalments'];
			$search_values ['due_amount'] = $search_data ['due_amount'];
			$search_values ['remaining_amount'] = $search_data ['remaining_amount'];
			$search_values ['total_debenture_amount'] = $search_data ['total_debenture_amount'];
			$search_values ['creation_date'] = $search_data ['creation_date'];
			$search_values ['executive_order_number'] = $search_data ['executive_order_number'];
			$search_values ['order_number'] = $search_data ['order_number'];
			$search_values ['circle_number'] = $search_data ['circle_number'];
			$search_values ['decision_34_number'] = $search_data ['decision_34_number'];
			$search_values ['decision_34_date'] = $search_data ['decision_34_date'];
			$search_values ['decision_46_date'] = $search_data ['decision_46_date'];
			$search_values ['state_code'] = $search_data ['state_code'];
			$search_values ['region_id'] = $search_data ['region_id'];
			$search_values ['lawyer_id'] = $search_data ['lawyer_id'];
			$search_values ['executive_order_date'] = $search_data ['executive_order_date'];
			$search_values ['advertisement_date'] = $search_data ['advertisement_date'];
			$search_values ['suspend_reason_code'] = $search_data ['suspend_reason_code'];
			$search_values ['suspend_date'] = $search_data ['suspend_date'];
			$search_values ['closing_reason'] = $search_data ['closing_reason'];
			$search_values ['case_id'] = $search_data ['case_id'];
			return $search_values;
		}
		
		// Advanced Search
		function open_search_dialog() {
			$user_id = $this->session->userdata ( 'userid' );
			$where = "where user_id =" . $user_id;
			$userRecord = $this->common->getOneRow ( 'user', $where );
			$lawyer_office_id = $userRecord ['lawyer_office_id'];
			$this->load->model ( "case_model" );
			$this->load->model ( "lawyer_office_model" );
			$search_data = array ();
			$search_data ["customer_id"] = "";
			$search_data ["contract_number"] = "";
			$search_data ["debtor_name"] = "";
			$search_data ["client_name"] = "";
			$search_data ["client_id_number"] = "";
			$search_data ["client_type"] = "";
			$search_data ["obligation_value"] = "";
			$search_data ["number_of_late_instalments"] = "";
			$search_data ["due_amount"] = "";
			$search_data ["remaining_amount"] = "";
			$search_data ['total_debenture_amount'] = "";
			$search_data ["creation_date"] = "";
			$search_data ["executive_order_number"] = "";
			$search_data ["order_number"] = "";
			$search_data ["circle_number"] = "";
			$search_data ["decision_34_number"] = "";
			$search_data ["decision_34_date"] = "";
			$search_data ["decision_46_date"] = "";
			$search_data ["state_code"] = "";
			$search_data ["region_id"] = "";
			$search_data ["lawyer_id"] = "";
			$search_data ["executive_order_date"] = "";
			$search_data ["advertisement_date"] = "";
			$search_data ["suspend_reason_code"] = "";
			$search_data ["suspend_date"] = "";
			$search_data ["closing_reason"] = "";
			$search_data ["case_id"] = "";
			$search_data ["notes"] = "";
			
			$data ['available_customers'] = $this->case_model->getAvailableCustomers ( $lawyer_office_id );
			$data ['available_customer_types'] = $this->common->getAllRow ( "customer_type", "" );
			// $data['available_states'] = $this -> common -> getAllRow("state", "");
			$data ['available_states'] = $this->case_model->getAvailableStatesOrdered ();
			$data ['suspend_reasons'] = $this->common->getAllRow ( 'case_suspend_reason', '' );
			$data ['close_reasons'] = $this->common->getAllRow ( 'case_close_reason', '' );
			$data ['available_regions'] = $this->common->getAllRow ( "region", "where lawyer_office_id = '" . $lawyer_office_id . "'" );
			$data ['lawyers_per_region'] = array ();
			$lawyers_in_region = array ();
			foreach ( $data ['available_regions'] as $region ) {
				$regionid = $region ['region_id'];
				$lawyers_in_region [$regionid] = $this->lawyer_office_model->getLawyersInRegion ( $lawyer_office_id, $regionid );
			}
			$data ['lawyers_per_region'] = $lawyers_in_region;
			if ($search_data ["region_id"] == "") {
				// $search_data["region_id"] = $data['available_regions'][0]['region_id'];
			}
			$data ['search_data'] = $search_data;
			$data ['search_values'] = $search_data;
			
			$data ['profile_first_name'] = $userRecord ['first_name'];
			$data ['profile_last_name'] = $userRecord ['last_name'];
			$data ['profile_img'] = $userRecord ['profile_img'];
			
			$user_type_code = $userRecord ['user_type_code'];
			$data ['user_type_code'] = $user_type_code;
			
			$notifications = $this->notification_model->getLatestDeliveredNotificationsForUser ( $user_id );
			$data ['notifications_list'] = $notifications ['notifications_list'];
			$data ['count_unseen'] = $notifications ['count_unseen'];
			$data ['page'] = "case_management/view_cases";
			
			$this->load->view ( 'cases/advanced_search', $data );
		}
		function open_search_dialog_for_edit($search_values) {
			$user_id = $this->session->userdata ( 'userid' );
			$where = "where user_id =" . $user_id;
			$userRecord = $this->common->getOneRow ( 'user', $where );
			$lawyer_office_id = $userRecord ['lawyer_office_id'];
			$this->load->model ( "case_model" );
			$this->load->model ( "lawyer_office_model" );
			$data ['available_customers'] = $this->case_model->getAvailableCustomers ( $lawyer_office_id );
			$data ['available_customer_types'] = $this->common->getAllRow ( "customer_type", "" );
			$data ['available_states'] = $this->case_model->getAvailableStatesOrdered ();
			$data ['suspend_reasons'] = $this->common->getAllRow ( 'case_suspend_reason', '' );
			$data ['close_reasons'] = $this->common->getAllRow ( 'case_close_reason', '' );
			$data ['available_regions'] = $this->common->getAllRow ( "region", "where lawyer_office_id = '" . $lawyer_office_id . "'" );
			$data ['lawyers_per_region'] = array ();
			$lawyers_in_region = array ();
			foreach ( $data ['available_regions'] as $region ) {
				$regionid = $region ['region_id'];
				$lawyers_in_region [$regionid] = $this->lawyer_office_model->getLawyersInRegion ( $lawyer_office_id, $regionid );
			}
			$data ['lawyers_per_region'] = $lawyers_in_region;
			if ($search_values ["region_id"] == "") {
				// $search_values["region_id"] = $data['available_regions'][0]['region_id'];
			}
			$data ['search_data'] = $search_values;
			$data ['search_values'] = $search_values;
			
			$data ['profile_first_name'] = $userRecord ['first_name'];
			$data ['profile_last_name'] = $userRecord ['last_name'];
			$data ['profile_img'] = $userRecord ['profile_img'];
			
			$user_type_code = $userRecord ['user_type_code'];
			$data ['user_type_code'] = $user_type_code;
			
			$notifications = $this->notification_model->getLatestDeliveredNotificationsForUser ( $user_id );
			$data ['notifications_list'] = $notifications ['notifications_list'];
			$data ['count_unseen'] = $notifications ['count_unseen'];
			$data ['page'] = "case_management/view_cases";
			
			$this->load->view ( 'cases/advanced_search', $data );
		}
		function array_multi_sort_by_column(&$array, $column1, $column2, $column3, $dir1 = SORT_ASC, $dir2 = SORT_ASC, $dir3 = SORT_ASC) {
			$sort_column = array ();
			foreach ( $array as $key => $row ) {
				$sort_column [$column1] [$key] = $row [$column1];
				$sort_column [$column2] [$key] = $row [$column2];
				$sort_column [$column3] [$key] = $row [$column3];
			}
			array_multisort ( $sort_column [$column1], $dir1, $sort_column [$column2], $dir2, $sort_column [$column3], $dir3, $array );
		}
		function array_dual_sort_by_column(&$array, $column1, $column2, $dir1 = SORT_ASC, $dir2 = SORT_ASC) {
			$sort_column = array ();
			foreach ( $array as $key => $row ) {
				$sort_column [$column1] [$key] = $row [$column1];
				$sort_column [$column2] [$key] = $row [$column2];
			}
			array_multisort ( $sort_column [$column1], $dir1, $sort_column [$column2], $dir2, $array );
		}
		function view_case_details($case_id = '', $search_mode = 0) {
			$this->load->model ( "comments_model" );
			$this->load->model ( "case_model" );
			if ($this->session->userdata ( 'userid' ) != '') {
				$user_id = $this->session->userdata ( 'userid' );
				$where = "where user_id =" . $user_id;
				$userRecord = $this->common->getOneRow ( 'user', $where );
				$lawyer_office_id = $userRecord ['lawyer_office_id'];
				$data ['profile_first_name'] = $userRecord ['first_name'];
				$data ['profile_last_name'] = $userRecord ['last_name'];
				$data ['profile_img'] = $userRecord ['profile_img'];
				
				$this->load->model ( "notification_model" );
				$notifications = $this->notification_model->getLatestDeliveredNotificationsForUser ( $user_id );
				$data ['notifications_list'] = $notifications ['notifications_list'];
				$data ['count_unseen'] = $notifications ['count_unseen'];
				$data ['page'] = "case_management/view_case_details/" . $case_id;
				
				$user_type_code = $userRecord ['user_type_code'];
				$data ['user_type_code'] = $user_type_code;
				
				$data ['case_details'] = $this->case_model->getCaseDetails ( $user_type_code, $case_id, $lawyer_office_id );
				
				$this->load->library ( '../controllers/dates_conversion' );
				$data ['case_details'] ['creation_date'] = $this->dates_conversion->GregorianToHijri ( $data ['case_details'] ['creation_date'] );
				$data ['case_details'] ['last_update_date'] = $this->dates_conversion->GregorianToHijri ( $data ['case_details'] ['last_update_date'] );
				if ($data ['case_details'] ['decision_34_date'] != NULL && $data ['case_details'] ['decision_34_date'] != "000-00-00") {
					$data ['case_details'] ['decision_34_date'] = $this->dates_conversion->GregorianToHijri ( $data ['case_details'] ['decision_34_date'] );
				}
				if ($data ['case_details'] ['decision_46_date'] != NULL && $data ['case_details'] ['decision_46_date'] != "000-00-00") {
					$data ['case_details'] ['decision_46_date'] = $this->dates_conversion->GregorianToHijri ( $data ['case_details'] ['decision_46_date'] );
				}
				if ($data ['case_details'] ['executive_order_date'] != NULL && $data ['case_details'] ['executive_order_date'] != "000-00-00") {
					$data ['case_details'] ['executive_order_date'] = $this->dates_conversion->GregorianToHijri ( $data ['case_details'] ['executive_order_date'] );
				}
				if ($data ['case_details'] ['advertisement_date'] != NULL && $data ['case_details'] ['advertisement_date'] != "000-00-00") {
					$data ['case_details'] ['advertisement_date'] = $this->dates_conversion->GregorianToHijri ( $data ['case_details'] ['advertisement_date'] );
				}
				if ($data ['case_details'] ['suspend_date'] != NULL && $data ['case_details'] ['suspend_date'] != "000-00-00") {
					$data ['case_details'] ['suspend_date'] = $this->dates_conversion->GregorianToHijri ( $data ['case_details'] ['suspend_date'] );
				}
				$current_state_code = $data ['case_details'] ['current_state_code'];
				
				$case_transactions = $this->case_model->getCaseTransactions ( $case_id, $lawyer_office_id );
				if ($case_transactions) {
					$i = 0;
					$last_state = "";
					$this->load->library ( '../controllers/dates_conversion' );
					foreach ( $case_transactions as $record ) {
						if ($record ['state_date'] != null && $record ['state_date'] != "") {
							$case_transactions [$i] ['state_date'] = $this->dates_conversion->GregorianToHijri ( $record ['state_date'] );
							if(($current_state_code == "CLOSED" || $current_state_code == "SUSPENDED") &&
									($case_transactions [$i]['state_code'] != "CLOSED" && $case_transactions [$i]['state_code'] != "SUSPENDED")){
										$last_state = $case_transactions [$i]['state_code'];
							}
						}
						$i ++;
					}
				}
				
				$data ['case_transactions'] = $case_transactions;
				$trans_where_conditions = "where source_state ='$current_state_code' ";
				$user_types_reopen = $this->case_model->getUserTypeWithActionPrvlg("REOPEN");
				$user_types_resume = $this->case_model->getUserTypeWithActionPrvlg("RESUME");
				$has_prvlg_reopen = false;
				$has_prvlg_resume = false;
				if($user_types_reopen != null){
					foreach ($user_types_reopen as $type_rec){
						if($user_type_code == $type_rec["user_type_code"]){
							$has_prvlg_reopen = true;
							break;
						}
					}
				}
				if($user_types_resume != null){
					foreach ($user_types_resume as $type_rec){
						if($user_type_code == $type_rec["user_type_code"]){
							$has_prvlg_resume = true;
							break;
						}
					}
				}
				if(($current_state_code == "CLOSED" && $has_prvlg_reopen !== false)|| ($current_state_code == "SUSPENDED" && $has_prvlg_resume !== false)){
					$this->load->model ( "case_model" );
					$last_trns = $this->case_model->getTransactionId ("current_state_code != '".$current_state_code."' and case_id = '" . $case_id . "' AND lawyer_office_id = '" . $lawyer_office_id . "' AND transaction_status = 'Success' AND (action_code IS NULL OR action_code != 'REASSIGN_TO_LAWYER') AND edit_action = 'N'");
					$last_state = $last_trns["current_state_code"];
					if($last_state != "DECISION_34" && $last_state != "PUBLICLY_ADVERTISED"){
						$trans_where_conditions = $trans_where_conditions . "or (source_state = '".$last_state."' and target_state != 'CLOSED' and target_state != 'SUSPENDED' and action_code != 'REASSIGN_TO_LAWYER' )";
					} else if ($last_state == "DECISION_34"){
						$trans_where_conditions = "where source_state = '".$last_state."' and target_state != 'CLOSED' and target_state != 'SUSPENDED' and target_state!= 'DECISION_46' and action_code != 'REASSIGN_TO_LAWYER' ";
					} else {
						$trans_where_conditions = "where source_state = '".$last_state."' and target_state != 'CLOSED' and target_state != 'SUSPENDED' and action_code != 'REASSIGN_TO_LAWYER' ";
					}
				}
				$transitions = $this->common->getAllRow ( "state_transition", $trans_where_conditions );
				$available_transitions = array ();
				if ($transitions) {
					foreach ( $transitions as $transition ) {
						$transition_id = $transition ['id'];
						// in case no reopen and no resume display default for Admin
						$condition = " and `condition` = 0";
						// in case there is reopen or resume add special actions (lawyer actions) for admin (including all conditions 0,1)
						if((($current_state_code == "CLOSED" && $has_prvlg_reopen !== false)|| ($current_state_code == "SUSPENDED" && $has_prvlg_resume !== false)) && $user_type_code == "ADMIN"){
							
							$condition = "";
						}
						$count = $this->common->getCountOfField ( "id", "usertype_privileges", "where user_type_code ='" . $user_type_code . "' AND state_transition='" . $transition_id . "'".$condition );
						if ($count > 0) {
							// only secretary which made assign to lawyer can make reassign
							/*
							 * if ($transition['action_code'] == "REASSIGN_TO_LAWYER" && $user_type_code == "SECRETARY") {
							 * $assign_lawyer_trans = $this -> common -> getOneRecField_Limit("last_update_user", "case_transactions","where current_state_code = 'ASSIGNED_TO_LAWYER' AND case_id = '".$case_id."' AND lawyer_office_id ='.$lawyer_office_id.' AND transaction_status = 'Success' AND edit_action = 'N'");
							 * $assigner_user = $assign_lawyer_trans->last_update_user;
							 * if ($user_id == $assigner_user) {
							 * array_push($available_transitions, $transition);
							 * }
							 * } else {
							 */
							array_push ( $available_transitions, $transition );
							// }
						}
					}
				}
				$data ['available_transitions'] = $available_transitions;
				
				$data ['comments'] = $this->comments_model->getCaseComments ( $case_id, $lawyer_office_id );
				if ($data ['comments']) {
					$i = 0;
					$this->load->library ( '../controllers/dates_conversion' );
					foreach ( $data ['comments'] as $record ) {
						$date_parts = explode ( " ", $record ['date_time'] );
						$hijri_date = $this->dates_conversion->GregorianToHijri ( $date_parts [0] );
						$data ['comments'] [$i] ['date_time'] = $hijri_date . " " . $date_parts [1];
						$i ++;
					}
				}
				
				$data ['suspend_reasons'] = $this->common->getAllRow ( 'case_suspend_reason', '' );
				$data ['close_reasons'] = $this->common->getAllRow ( 'case_close_reason', '' );
				$data ['available_customers'] = $this->case_model->getAvailableCustomers ( $lawyer_office_id );
				$data ['available_customer_types'] = $this->common->getAllRow ( "customer_type", "" );
				$data ['search_mode'] = $search_mode;
				
				$this->load->view ( 'cases/view_case_details', $data );
			} else {
				$data ['user_name'] = '';
				$this->load->view ( 'user_login', $data );
			}
		}
		function add_comment($case_id = '') {
			if (extract ( $_POST )) {
				$user_id = $this->session->userdata ( 'userid' );
				$where = "where user_id =" . $user_id;
				$userRecord = $this->common->getOneRow ( 'user', $where );
				$lawyer_office_id = $userRecord ["lawyer_office_id"];
				
				$value ['comment'] = $_POST ['comment'];
				
				$user_id = $this->session->userdata ( 'userid' );
				$value ['user_id'] = $user_id;
				$value ['case_id'] = $case_id;
				$value ['lawyer_office_id'] = $lawyer_office_id;
				date_default_timezone_set ( 'Asia/Riyadh' );
				$value ['date_time'] = date ( "Y-m-d H:i:s" );
				
				$this->common->insertRecord ( 'comment', $value );
			}
			redirect ( 'case_management/view_case_details/' . $case_id );
		}
		function add_case() {
			$this->load->model ( "case_model" );
			$user_id = $this->session->userdata ( 'userid' );
			$where = "where user_id =" . $user_id;
			$userRecord = $this->common->getOneRow ( 'user', $where );
			$lawyer_office_id = $userRecord ['lawyer_office_id'];
			$user_name = $userRecord ['user_name'];
			$data ['profile_first_name'] = $userRecord ['first_name'];
			$data ['profile_last_name'] = $userRecord ['last_name'];
			$data ['profile_img'] = $userRecord ['profile_img'];
			
			$this->load->model ( "notification_model" );
			$notifications = $this->notification_model->getLatestDeliveredNotificationsForUser ( $user_id );
			$data ['notifications_list'] = $notifications ['notifications_list'];
			$data ['count_unseen'] = $notifications ['count_unseen'];
			$data ['page'] = "case_management/add_case";
			
			$user_type_code = $userRecord ['user_type_code'];
			$data ['user_type_code'] = $user_type_code;
			
			$case_id = $this->case_model->getCaseId ( $lawyer_office_id );
			$data ['case_id'] = $case_id;
			$data ['customer_id'] = "";
			$data ['contract_number'] = "";
			$data ['client_name'] = "";
			$data ['client_id_number'] = "";
			$data ['client_type'] = "";
			$data ['number_of_late_instalments'] = "";
			$data ['due_amount'] = "";
			$data ['remaining_amount'] = "";
			$data ['total_debenture_amount'] = "";
			$data ['debtor_name'] = "";
			$data ['obligation_value'] = "";
			
			$data ['creation_date'] = "";
			$data ['creation_user'] = $user_id;
			$data ['current_state_code'] = "";
			
			if (extract ( $_POST )) {
				if ($user_type_code == "CUSTOMER") {
					$value ['customer_id'] = $user_id;
				} else {
					$value ['customer_id'] = $_POST ['customer_id'];
				}
				$value ["lawyer_office_id"] = $lawyer_office_id;
				$value ['contract_number'] = $_POST ['contract_number'];
				$value ['client_id_number'] = $_POST ['client_id_number'];
				$value ['client_name'] = $_POST ['client_name'];
				$value ['client_type'] = $_POST ['client_type'];
				$value ['number_of_late_instalments'] = $_POST ['number_of_late_instalments'];
				$value ['due_amount'] = $_POST ['due_amount'];
				$value ['remaining_amount'] = $_POST ['remaining_amount'];
				
				$value ['debtor_name'] = $_POST ['debtor_name'];
				if ($_POST ['obligation_value'] != null && isset ( $_POST ['obligation_value'] ) && $_POST ['obligation_value'] != "") {
					$value ['obligation_value'] = $_POST ['obligation_value'];
				}
				
				$value ['current_state_code'] = $this->case_model->getCaseStateAfterCreation ();
				$value ['creation_user'] = $user_id;
				$value ['last_update_user'] = $user_id;
				
				date_default_timezone_set ( 'Asia/Riyadh' );
				$current_date_time = date ( "Y-m-d H:i:s" );
				$dt = new DateTime ( $current_date_time );
				$current_date = $dt->format ( 'Y-m-d' );
				$value ['creation_date'] = $current_date;
				$value ['last_update_date'] = $current_date;
				$value ['case_id'] = $case_id;
				
				$value ['total_debenture_amount'] = $_POST ['total_debenture_amount'];
				$empty_array = array ();
				if (isset ( $_POST ['debenture_hidden'] ) && $_POST ['debenture_hidden'] != NULL && $_POST ['debenture_hidden'] != "") {
					$tmp_path = $_POST ['debenture_hidden'];
					$tmp_path = str_replace ( "cases_tmp_files", "cases_files", $tmp_path );
					copy ( $_POST ['debenture_hidden'], $tmp_path );
					$value ['debenture'] = $tmp_path;
					$empty_array ['debenture'] = $tmp_path;
				} else {
					if (isset ( $_FILES ['debenture'] ['name'] ) && $_FILES ['debenture'] ['name'] != "") {
						self::upload_file ( $value, $empty_array, 'debenture', $value ['case_id'], $_FILES ['debenture'] ['name'], $_FILES ['debenture'] ['tmp_name'] );
					}
				}
				if (isset ( $_POST ['id_hidden'] ) && $_POST ['id_hidden'] != NULL && $_POST ['id_hidden'] != "") {
					$tmp_path = $_POST ['id_hidden'];
					$tmp_path = str_replace ( "cases_tmp_files", "cases_files", $tmp_path );
					copy ( $_POST ['id_hidden'], $tmp_path );
					$value ['id'] = $tmp_path;
					$empty_array ['id'] = $tmp_path;
				} else {
					if (isset ( $_FILES ['id'] ['name'] ) && $_FILES ['id'] ['name'] != "") {
						self::upload_file ( $value, $empty_array, 'id', $value ['case_id'], $_FILES ['id'] ['name'], $_FILES ['id'] ['tmp_name'] );
					}
				}
				if (isset ( $_POST ['contract_hidden'] ) && $_POST ['contract_hidden'] != NULL && $_POST ['contract_hidden'] != "") {
					$tmp_path = $_POST ['contract_hidden'];
					$tmp_path = str_replace ( "cases_tmp_files", "cases_files", $tmp_path );
					copy ( $_POST ['contract_hidden'], $tmp_path );
					$value ['contract'] = $tmp_path;
					$empty_array ['contract'] = $tmp_path;
				} else {
					if (isset ( $_FILES ['contract'] ['name'] ) && $_FILES ['contract'] ['name'] != "") {
						self::upload_file ( $value, $empty_array, 'contract', $value ['case_id'], $_FILES ['contract'] ['name'], $_FILES ['contract'] ['tmp_name'] );
					}
				}
				if (isset ( $_POST ['others_hidden'] ) && $_POST ['others_hidden'] != NULL && $_POST ['others_hidden'] != "") {
					$tmp_path = $_POST ['others_hidden'];
					$tmp_path = str_replace ( "cases_tmp_files", "cases_files", $tmp_path );
					$tmp_arr = explode ( ",", $_POST ['others_hidden'] );
					$final_arr = explode ( ",", $tmp_path );
					for($i = 0; $i < count ( $final_arr ); $i ++) {
						copy ( $tmp_arr [$i], $final_arr [$i] );
					}
					$value ['others'] = $tmp_path;
					$empty_array ['others'] = $tmp_path;
				} else {
					$others_value = "";
					if (isset ( $_FILES ['others'] ['name'] ) && count ( $_FILES ["others"] ['name'] ) > 0) {
						for($i = 0; $i < count ( $_FILES ["others"] ['name'] ); $i ++) {
							$item = "others_" . ($i + 1);
							$name = $_FILES ['others'] ['name'] [$i];
							$tmp_name = $_FILES ['others'] ['tmp_name'] [$i];
							if (isset ( $name ) && $name != "") {
								if (! is_dir ( "./cases_files/" . $value ['case_id'] )) {
									mkdir ( './cases_files/' . $value ['case_id'], 0777, true );
								}
								$file = $name;
								$uploadedfile = $tmp_name;
								$ext = strstr ( $name, "." );
								
								$source = $tmp_name;
								$file = $item . "-" . str_replace ( ' ', '-', $file );
								$filepath = $file;
								$save = "./cases_files/" . $value ['case_id'] . "/" . $filepath;
								move_uploaded_file ( $uploadedfile, $save );
								
								$others_value = $others_value . "," . "cases_files/" . $value ['case_id'] . "/" . $file;
							}
						}
						$others_value = trim ( $others_value, "," );
						$value ["others"] = $others_value;
					}
				}
				$this->common->insertRecord ( 'case', $value );
				$case_id = $value ['case_id'];
				
				$valid_case = $this->case_model->isCaseValid ( $value ['case_id'] );
				$value ['trans_date_time'] = $current_date_time;
				if ($valid_case) {
					self::insert_transaction ( $value, "Success" );
				} else {
					self::insert_transaction ( $value, "Failure" );
				}
				
				redirect ( 'case_management' );
			}
			
			if ($user_type_code != "CUSTOMER") {
				$data ['available_customers'] = $this->case_model->getAvailableCustomers ( $lawyer_office_id );
			}
			$data ['available_customer_types'] = $this->common->getAllRow ( "customer_type", "" );
			$ordered_states = $this->case_model->getOrderedStates ();
			$data ['ordered_states'] = $ordered_states;
			
			$this->load->view ( 'cases/add_case', $data );
		}
		function insert_transaction($values, $status) {
			$this->load->model ( "case_model" );
			$values ['transaction_id'] = $this->case_model->getTransactionId ();
			$values ['transaction_status'] = $status;
			if ($status == "Failure") {
				$values ['error_code'] = 1;
			}
			$id = $this->common->insertRecord ( 'case_transactions', $values );
			return $id;
		}
		function do_case_request($case_id, $request_type = 'S') {
			$user_id = $this->session->userdata ( 'userid' );
			$where = "where user_id =" . $user_id;
			$userRecord = $this->common->getOneRow ( 'user', $where );
			$lawyer_office_id = $userRecord ['lawyer_office_id'];
			if (extract ( $_POST )) {
				date_default_timezone_set ( 'Asia/Riyadh' );
				$current_date_time = date ( "Y-m-d H:i:s" );
				$request_id = 0;
				if ($request_type == 'S') {
					$request_values ['request_type'] = 'S';
					
					$request_values ['suspend_reason_code'] = $_POST ['req_suspend_reason_code'];
					$request_values ['notes'] = $_POST ['suspend_notes'];
				} else {
					$request_values ['request_type'] = 'R';
					$request_values ['notes'] = $_POST ['resume_notes'];
				}
				// if (isset ( $request_id )) {
				$trans_val ['case_id'] = $case_id;
				$trans_val ['lawyer_office_id'] = $lawyer_office_id;
				if ($request_type == 'S') {
					$trans_val ['suspend_reason_code'] = $_POST ['req_suspend_reason_code'];
					$trans_val ['notes'] = $_POST ['suspend_notes'];
				} else if ($request_type == 'R')
					$trans_val ['notes'] = $_POST ['resume_notes'];
					$trans_val ['trans_date_time'] = $current_date_time;
					$trans_val ['lawyer_id'] = $_POST ['lawyer_id'];
					$trans_val ['customer_id'] = $user_id;
					
					if ($request_type == 'S') {
						$trans_val ['action_code'] = 'SUSPEND_REQUEST';
					} else {
						$trans_val ['action_code'] = 'RESUME_REQUEST';
					}
					
					$trans_id = self::insert_transaction ( $trans_val, "Success" );
					// }
					$request_values ['case_id'] = $case_id;
					$request_values ['lawyer_office_id'] = $lawyer_office_id;
					$request_values ['request_date'] = $current_date_time;
					$request_values ['trans_id'] = $trans_id;
					$update_req['request_done'] = 'Y'; // for other requests on the same case must set the request as DONE
					$this->common->updateRecord ( 'case_requests', $update_req, "case_id = '$case_id' and lawyer_office_id = $lawyer_office_id and request_done = 'N'");
					$request_id = $this->common->insertRecord ( 'case_requests', $request_values );
					
					$this->load->library ( '../controllers/cron_notifications' );
					$this->cron_notifications->process_request_notification ( $request_id, $trans_id );
					
					echo "<script>window.close(); </script>";
			} else {
				$data ['case_id'] = $case_id;
				$data ['lawyer_office_id'] = $lawyer_office_id;
				
				$this->load->model ( "case_model" );
				$case_transactions = $this->case_model->getCaseTransactions ( $case_id, $lawyer_office_id );
				$data ['case_transactions'] = $case_transactions;
				
				$data ['suspend_reasons'] = $this->common->getAllRow ( 'case_suspend_reason', '' );
				
				// get last state code in order to validate with the new transition date
				$last_state_date = '';
				if ($case_transactions) {
					$i = 0;
					$this->load->library ( '../controllers/dates_conversion' );
					foreach ( $case_transactions as $record ) {
						if ($record ['state_date'] != null && $record ['state_date'] != "") {
							$case_transactions [$i] ['state_date'] = $this->dates_conversion->GregorianToHijri ( $record ['state_date'] );
							$last_state_date = $case_transactions [$i] ['state_date'];
						}
						$i ++;
					}
				}
				$data ['last_state_date'] = $last_state_date;
				
				if ($request_type == 'S') {
					$data ['action_code'] = 'SUSPEND_REQUEST';
				} else {
					$data ['action_code'] = 'RESUME_REQUEST';
				}
				$this->load->view ( 'cases/action', $data );
			}
		}
		function manage_case($case_id = '', $action_code = '', $region_id = '') {
			$user_id = $this->session->userdata ( 'userid' );
			$where = "where user_id =" . $user_id;
			$userRecord = $this->common->getOneRow ( 'user', $where );
			$lawyer_office_id = $userRecord ["lawyer_office_id"];
			if (extract ( $_POST )) {
				
				$user_type_code = $userRecord ['user_type_code'];
				$this->load->model ( "case_model" );
				
				$old_case_values = $this->case_model->getCaseDataForTransaction ( $case_id, $lawyer_office_id );
				$trans_values = array ();
				$trans_values ['case_id'] = $case_id;
				$trans_values ['lawyer_office_id'] = $lawyer_office_id;
				$nullFields = '';
				if (isset ( $_POST ['nullFields'] )) {
					$nullFields = $_POST ['nullFields'];
				}
				$value = array ();
				self::getNewParameterValue ( $value, $trans_values, $_POST ['customer_id'], 'customer_id', $nullFields );
				self::getNewParameterValue ( $value, $trans_values, $_POST ['contract_number'], 'contract_number', $nullFields );
				self::getNewParameterValue ( $value, $trans_values, $_POST ['client_id_number'], 'client_id_number', $nullFields );
				self::getNewParameterValue ( $value, $trans_values, $_POST ['client_name'], 'client_name', $nullFields );
				// self::getNewParameterValue($value, $trans_values, $_POST['client_type'], 'client_type');
				self::getNewParameterValue ( $value, $trans_values, $_POST ['number_of_late_instalments'], 'number_of_late_instalments', $nullFields );
				self::getNewParameterValue ( $value, $trans_values, $_POST ['due_amount'], 'due_amount', $nullFields );
				self::getNewParameterValue ( $value, $trans_values, $_POST ['remaining_amount'], 'remaining_amount', $nullFields );
				self::getNewParameterValue ( $value, $trans_values, $_POST ['debtor_name'], 'debtor_name', $nullFields );
				self::getNewParameterValue ( $value, $trans_values, $_POST ['obligation_value'], 'obligation_value', $nullFields );
				
				if (isset ( $_POST ['debenture_hidden'] ) && $_POST ['debenture_hidden'] != NULL && $_POST ['debenture_hidden'] != "") {
					$tmp_path = $_POST ['debenture_hidden'];
					$tmp_path = str_replace ( "cases_tmp_files", "cases_files", $tmp_path );
					copy ( $_POST ['debenture_hidden'], $tmp_path );
					$value ['debenture'] = $tmp_path;
					$trans_values ['debenture'] = $tmp_path;
				} else {
					if (isset ( $_FILES ['debenture'] ['name'] ) && $_FILES ['debenture'] ['name'] != "") {
						self::upload_file ( $value, $trans_values, 'debenture', $case_id, $_FILES ['debenture'] ['name'], $_FILES ['debenture'] ['tmp_name'] );
					}
				}
				
				if (isset ( $_POST ['id_hidden'] ) && $_POST ['id_hidden'] != NULL && $_POST ['id_hidden'] != "") {
					$tmp_path = $_POST ['id_hidden'];
					$tmp_path = str_replace ( "cases_tmp_files", "cases_files", $tmp_path );
					copy ( $_POST ['id_hidden'], $tmp_path );
					$value ['id'] = $tmp_path;
					$trans_values ['id'] = $tmp_path;
				} else {
					if (isset ( $_FILES ['id'] ['name'] ) && $_FILES ['id'] ['name'] != "") {
						self::upload_file ( $value, $trans_values, 'id', $case_id, $_FILES ['id'] ['name'], $_FILES ['id'] ['tmp_name'] );
					}
				}
				
				if (isset ( $_POST ['contract_hidden'] ) && $_POST ['contract_hidden'] != NULL && $_POST ['contract_hidden'] != "") {
					$tmp_path = $_POST ['contract_hidden'];
					$tmp_path = str_replace ( "cases_tmp_files", "cases_files", $tmp_path );
					copy ( $_POST ['contract_hidden'], $tmp_path );
					$value ['contract'] = $tmp_path;
					$trans_values ['contract'] = $tmp_path;
				} else {
					if (isset ( $_FILES ['contract'] ['name'] ) && $_FILES ['contract'] ['name'] != "") {
						self::upload_file ( $value, $trans_values, 'contract', $case_id, $_FILES ['contract'] ['name'], $_FILES ['contract'] ['tmp_name'] );
					}
				}
				
				if (isset ( $_POST ['others_hidden'] ) && $_POST ['others_hidden'] != NULL && $_POST ['others_hidden'] != "") {
					$tmp_path = $_POST ['others_hidden'];
					$tmp_path = str_replace ( "cases_tmp_files", "cases_files", $tmp_path );
					$tmp_arr = explode ( ",", $_POST ['others_hidden'] );
					$final_arr = explode ( ",", $tmp_path );
					for($i = 0; $i < count ( $final_arr ); $i ++) {
						copy ( $tmp_arr [$i], $final_arr [$i] );
					}
					$value ['others'] = $tmp_path;
					$trans_values ['others'] = $tmp_path;
				} else {
					$others_count = $_POST ['others_count'];
					if ($others_count != null && $others_count > 0) {
						$others_value = "";
						for($i = 1; $i <= $others_count; $i ++) {
							$item = "others_" . $i;
							$name = $_FILES [$item] ['name'];
							$tmp_name = $_FILES [$item] ['tmp_name'];
							if (isset ( $name ) && $name != "") {
								if (! is_dir ( "./cases_files/" . $lawyer_office_id . "/" . $case_id )) {
									mkdir ( './cases_files/' . $lawyer_office_id . "/" . $case_id, 0777, true );
								}
								$file = $name;
								$uploadedfile = $tmp_name;
								$ext = strstr ( $name, "." );
								
								$source = $tmp_name;
								$file = $item . "-" . str_replace ( ' ', '-', $file );
								$filepath = $file;
								$save = "./cases_files/" . $lawyer_office_id . "/" . $case_id . "/" . $filepath;
								move_uploaded_file ( $uploadedfile, $save );
								
								$others_value = $others_value . "," . "cases_files/" . $lawyer_office_id . "/" . $case_id . "/" . $file;
							}
						}
						$others_value = ltrim ( $others_value, "," );
						$value ["others"] = $others_value;
						$trans_values ["others"] = $others_value;
					}
				}
				// self::getNewParameterValue($value, $trans_values, $_POST['creation_date'], 'creation_date');
				self::getNewParameterValue ( $value, $trans_values, $_POST ['order_number'], 'order_number', $nullFields );
				self::getNewParameterValue ( $value, $trans_values, $_POST ['circle_number'], 'circle_number', $nullFields );
				self::getNewParameterValue ( $value, $trans_values, $_POST ['executive_order_number'], 'executive_order_number', $nullFields );
				self::getNewParameterValue ( $value, $trans_values, $_POST ['decision_34_number'], 'decision_34_number', $nullFields );
				if (isset ( $_FILES ['decision_34_file'] ['name'] ) && $_FILES ['decision_34_file'] ['name'] != "") {
					self::upload_file ( $value, $trans_values, 'decision_34_file', $case_id, $_FILES ['decision_34_file'] ['name'], $_FILES ['decision_34_file'] ['tmp_name'] );
				}
				
				if (isset ( $_POST ['advertisement_file_hidden'] ) && $_POST ['advertisement_file_hidden'] != NULL && $_POST ['advertisement_file_hidden'] != "") {
					$tmp_path = $_POST ['advertisement_file_hidden'];
					$tmp_path = str_replace ( "cases_tmp_files", "cases_files", $tmp_path );
					copy ( $_POST ['advertisement_file_hidden'], $tmp_path );
					$value ['advertisement_file'] = $tmp_path;
					$trans_values ['advertisement_file'] = $tmp_path;
				} else {
					if (isset ( $_FILES ['advertisement_file'] ['name'] ) && $_FILES ['advertisement_file'] ['name'] != "") {
						self::upload_file ( $value, $trans_values, 'advertisement_file', $case_id, $_FILES ['advertisement_file'] ['name'], $_FILES ['advertisement_file'] ['tmp_name'] );
					}
				}
				
				if (isset ( $_POST ['invoice_file_hidden'] ) && $_POST ['invoice_file_hidden'] != NULL && $_POST ['invoice_file_hidden'] != "") {
					$tmp_path = $_POST ['invoice_file_hidden'];
					$tmp_path = str_replace ( "cases_tmp_files", "cases_files", $tmp_path );
					copy ( $_POST ['invoice_file_hidden'], $tmp_path );
					$value ['invoice_file'] = $tmp_path;
					$trans_values ['invoice_file'] = $tmp_path;
				} else {
					if (isset ( $_FILES ['invoice_file'] ['name'] ) && $_FILES ['invoice_file'] ['name'] != "") {
						self::upload_file ( $value, $trans_values, 'invoice_file', $case_id, $_FILES ['invoice_file'] ['name'], $_FILES ['invoice_file'] ['tmp_name'] );
					}
				}
				
				if (isset ( $_POST ['referral_paper_hidden'] ) && $_POST ['referral_paper_hidden'] != NULL && $_POST ['referral_paper_hidden'] != "") {
					$tmp_path = $_POST ['referral_paper_hidden'];
					$tmp_path = str_replace ( "cases_tmp_files", "cases_files", $tmp_path );
					copy ( $_POST ['referral_paper_hidden'], $tmp_path );
					$value ['referral_paper'] = $tmp_path;
					$trans_values ['referral_paper'] = $tmp_path;
				} else {
					if (isset ( $_FILES ['referral_paper'] ['name'] ) && $_FILES ['referral_paper'] ['name'] != "") {
						self::upload_file ( $value, $trans_values, 'referral_paper', $case_id, $_FILES ['referral_paper'] ['name'], $_FILES ['referral_paper'] ['tmp_name'] );
					}
				}
				if (isset ( $_POST ['suspend_file_hidden'] ) && $_POST ['suspend_file_hidden'] != NULL && $_POST ['suspend_file_hidden'] != "") {
					$tmp_path = $_POST ['suspend_file_hidden'];
					$tmp_path = str_replace ( "cases_tmp_files", "cases_files", $tmp_path );
					copy ( $_POST ['suspend_file_hidden'], $tmp_path );
					$value ['suspend_file'] = $tmp_path;
					$trans_values ['suspend_file'] = $tmp_path;
				} else {
					if (isset ( $_FILES ['suspend_file'] ['name'] ) && $_FILES ['suspend_file'] ['name'] != "") {
						self::upload_file ( $value, $trans_values, 'suspend_file', $case_id, $_FILES ['suspend_file'] ['name'], $_FILES ['suspend_file'] ['tmp_name'] );
					}
				}
				if (isset ( $_POST ['consignment_image_hidden'] ) && $_POST ['consignment_image_hidden'] != NULL && $_POST ['consignment_image_hidden'] != "") {
					$tmp_path = $_POST ['consignment_image_hidden'];
					$tmp_path = str_replace ( "cases_tmp_files", "cases_files", $tmp_path );
					copy ( $_POST ['consignment_image_hidden'], $tmp_path );
					$value ['consignment_image'] = $tmp_path;
					$trans_values ['consignment_image'] = $tmp_path;
				} else {
					if (isset ( $_FILES ['consignment_image'] ['name'] ) && $_FILES ['consignment_image'] ['name'] != "") {
						self::upload_file ( $value, $trans_values, 'consignment_image', $case_id, $_FILES ['consignment_image'] ['name'], $_FILES ['consignment_image'] ['tmp_name'] );
					}
				}
				
				if (isset ( $_POST ['suspend_file_hidden'] ) && $_POST ['suspend_file_hidden'] != NULL && $_POST ['suspend_file_hidden'] != "") {
					$tmp_path = $_POST ['suspend_file_hidden'];
					$tmp_path = str_replace ( "cases_tmp_files", "cases_files", $tmp_path );
					copy ( $_POST ['suspend_file_hidden'], $tmp_path );
					$value ['suspend_file'] = $tmp_path;
					$trans_values ['suspend_file'] = $tmp_path;
				} else {
					if (isset ( $_FILES ['suspend_file'] ['name'] ) && $_FILES ['suspend_file'] ['name'] != "") {
						self::upload_file ( $value, $trans_values, 'suspend_file', $case_id, $_FILES ['suspend_file'] ['name'], $_FILES ['suspend_file'] ['tmp_name'] );
					}
				}
				$this->load->library ( '../controllers/dates_conversion' );
				$decision_46_date = "";
				if ($_POST ['decision_46_date'] != null && $_POST ['decision_46_date'] != "" && $_POST ['decision_46_date'] != " ") {
					$decision_46_date = $this->dates_conversion->HijriToGregorian ( $_POST ['decision_46_date'] );
				}
				$decision_34_date = "";
				if ($_POST ['decision_34_date'] != null && $_POST ['decision_34_date'] != "" && $_POST ['decision_34_date'] != " ") {
					$decision_34_date = $this->dates_conversion->HijriToGregorian ( $_POST ['decision_34_date'] );
				}
				$executive_order_date = "";
				if ($_POST ['executive_order_date'] != null && $_POST ['executive_order_date'] != "" && $_POST ['executive_order_date'] != " ") {
					$executive_order_date = $this->dates_conversion->HijriToGregorian ( $_POST ['executive_order_date'] );
				}
				$advertisement_date = "";
				if ($_POST ['advertisement_date'] != null && $_POST ['advertisement_date'] != "" && $_POST ['advertisement_date'] != " ") {
					$advertisement_date = $this->dates_conversion->HijriToGregorian ( $_POST ['advertisement_date'] );
				}
				$suspend_date = "";
				if ($_POST ['suspend_date'] != null && $_POST ['suspend_date'] != "" && $_POST ['suspend_date'] != " ") {
					$suspend_date = $this->dates_conversion->HijriToGregorian ( $_POST ['suspend_date'] );
				}
				// in case of resume or reopen
				$return_date = "";
				if ($_POST ['return_date'] != null && $_POST ['return_date'] != "" && $_POST ['return_date'] != " ") {
					$return_date = $this->dates_conversion->HijriToGregorian ( $_POST ['return_date'] );
				}
				self::getNewParameterValue ( $value, $trans_values, $decision_46_date, 'decision_46_date', $nullFields );
				self::getNewParameterValue ( $value, $trans_values, $decision_34_date, 'decision_34_date', $nullFields );
				self::getNewParameterValue ( $value, $trans_values, $executive_order_date, 'executive_order_date', $nullFields );
				self::getNewParameterValue ( $value, $trans_values, $advertisement_date, 'advertisement_date', $nullFields );
				self::getNewParameterValue ( $value, $trans_values, $_POST ['closing_reason'], 'closing_reason', $nullFields );
				self::getNewParameterValue ( $value, $trans_values, $_POST ['suspend_reason_code'], 'suspend_reason_code', $nullFields );
				self::getNewParameterValue ( $value, $trans_values, $suspend_date, 'suspend_date', $nullFields );
				self::getNewParameterValue ( $value, $trans_values, $_POST ['region_id'], 'region_id', $nullFields );
				
				$source_state = $old_case_values ['current_state_code'];
				$newStateRecord = $this->common->getOneRecField_Limit ( "target_state", "state_transition", "where action_code ='$action_code' AND source_state= '" . $source_state . "'" );
				date_default_timezone_set ( 'Asia/Riyadh' );
				$current_date_time = date ( "Y-m-d H:i:s" );
				$dt = new DateTime ( $current_date_time );
				$current_date = $dt->format ( 'Y-m-d' );
				$value ['last_update_date'] = $current_date;
				
				// not resume case
				if (isset ( $newStateRecord->target_state ) && $newStateRecord->target_state != NULL && $newStateRecord->target_state != "" && $newStateRecord->target_state != null) {
					$value ['current_state_code'] = $newStateRecord->target_state;
				} else if($action_code == "REOPEN" || $action_code == "RESUME"){
					// incase of resume AND REOPEN
					$value ['current_state_code'] = $old_case_values ['last_state_code'];//=========
					if ($old_case_values ['last_state_code'] == NULL || $old_case_values ['last_state_code'] == null || $old_case_values ['last_state_code'] == "") {
						$value ['current_state_code'] = $this->case_model->getCaseLastStateFromTransactions ( $case_id, $lawyer_office_id );
						if ($value ['current_state_code'] == "") {
							$value ['current_state_code'] = "DECISION_46";
						}
					}
					switch ($value ['current_state_code']) {
						case "UNDER_REVIEW" :
							$value ['last_update_date'] = $return_date;
							$trans_values ['last_update_date'] = $return_date;
							break;
						case "REGISTERED" :
							$value ['executive_order_date'] = $return_date;
							$trans_values ['executive_order_date'] = $return_date;
							break;
						case "DECISION_34" :
							$value ['decision_34_date'] = $return_date;
							$trans_values ['decision_34_date'] = $return_date;
							break;
						case "PUBLICLY_ADVERTISED" :
							$value ['advertisement_date'] = $return_date;
							$trans_values ['advertisement_date'] = $return_date;
							break;
						case "DECISION_46" :
							$value ['decision_46_date'] = $return_date;
							$trans_values ['decision_46_date'] = $return_date;
							break;
						default :
							$value ['last_update_date'] = $return_date;
							$trans_values ['last_update_date'] = $return_date;
							break;
					}
				} else {
					// case of current is CLOSED or SUSPENDED and the action is other than REOPEN and RESUME
					$last_trns = $this->case_model->getTransactionId ("current_state_code != '".$source_state."' and case_id = '" . $case_id . "' AND lawyer_office_id = '" . $lawyer_office_id . "' AND transaction_status = 'Success' AND (action_code IS NULL OR action_code != 'REASSIGN_TO_LAWYER') AND edit_action = 'N'");
					$last_state =  $last_trns["current_state_code"];
					$newStateRecord = $this->common->getOneRecField_Limit ( "target_state", "state_transition", "where action_code ='$action_code' AND source_state= '" . $last_state . "'" );
					$value ['current_state_code'] = $newStateRecord->target_state;
				}
				$trans_values ['current_state_code'] = $value ['current_state_code'];
				if ($action_code == "SUSPEND" || $action_code == "CLOSE") {
					$value ['last_state_code'] = $source_state;
					$trans_values ['last_state_code'] = $source_state;
				} else {
					$value ['last_state_code'] = NULL;
					$trans_values ['last_state_code'] = NULL;
				}
				
				$value ['last_update_user'] = $this->session->userdata ( 'userid' );
				$trans_values ['last_update_user'] = $value ['last_update_user'];
				$trans_values ['trans_date_time'] = $current_date_time;
				$trans_values ['last_update_date'] = $value ['last_update_date'];
				$value ['notes'] = $_POST ['notes'];
				$trans_values ['notes'] = $value ['notes'];
				
				$lawyer_case_id = 1;
				if ($action_code == "ASSIGN_TO_LAWYER") {
					$lawyer_case_id = $this->common->insertRecord ( "lawyer_case", array (
							"lawyer_id" => $_POST ['lawyer_id'],
							"case_id" => $case_id,
							"lawyer_office_id" => $lawyer_office_id
					) );
					
					$lawyer_case_data = array ();
					$lawyer_case_data ['lawyer_id'] = $_POST ['reassigned_lawyer_id'];
					$value ['region_id'] = $_POST ['region_id'];
					$trans_values ['region_id'] = $_POST ['region_id'];
					$trans_values ['lawyer_id'] = $_POST ['lawyer_id'];
				} else if ($action_code == "REASSIGN_TO_LAWYER") {
					$lawyer_case_data = array ();
					$lawyer_case_data ['lawyer_id'] = $_POST ['reassigned_lawyer_id'];
					$where = "case_id ='" . $case_id . "' and lawyer_office_id = '" . $lawyer_office_id . "'";
					$this->common->updateRecord ( 'lawyer_case', $lawyer_case_data, $where );
					$lawyer_case_record = $this->common->getOneRecField_Limit ( "id", "lawyer_case", "where case_id ='" . $case_id . "' and lawyer_office_id = " . $lawyer_office_id );
					$lawyer_case_id = $lawyer_case_record->id;
					$trans_values ['lawyer_id'] = $_POST ['reassigned_lawyer_id'];
					$trans_values ['region_id'] = $_POST ['region_id'];
					$value ['region_id'] = $_POST ['region_id'];
				} else {
					$lawyer_case_record = $this->common->getOneRecField_Limit ( "lawyer_id", "lawyer_case", "where case_id ='" . $case_id . "' and lawyer_office_id = '" . $lawyer_office_id . "'" );
					if (isset ( $lawyer_case_record ) && $lawyer_case_record != null) {
						$trans_values ['lawyer_id'] = $lawyer_case_record->lawyer_id;
					}
				}
				$trans_values ['action_code'] = $action_code;
				
				$ignoreCreatingInvoices = 0;
				if ($action_code == "REASSIGN_TO_LAWYER" || $action_code == 'RESUME' || $action_code == 'REOPEN') {
					$ignoreCreatingInvoices = 1;
				}
				$ignoreCreatingLawyerInvoice = 0;
				if ($user_type_code != 'LAWYER') {
					$ignoreCreatingLawyerInvoice = 1;
				}
				$where = "case_id ='" . $case_id . "' and lawyer_office_id = '" . $lawyer_office_id . "'";
				
				$affected_rows = $this->common->updateRecord ( 'case', $value, $where );
				if (($affected_rows == 1 || $action_code == "REASSIGN_TO_LAWYER") && ($lawyer_case_id != null && $lawyer_case_id > 0)) {
					self::insert_transaction ( $trans_values, "Success" );
					if ($ignoreCreatingInvoices === 0) {
						$this->case_model->create_invoice ( $trans_values, $old_case_values, $_POST ['region_id'], $ignoreCreatingLawyerInvoice );
					}
				} else {
					self::insert_transaction ( $trans_values, "Failure" );
				}
				// for new suspend / resume request.
				if ($action_code == 'SUSPEND') {
					// set all suspend requests on this case to done
					$where = "case_id = '$case_id' and lawyer_office_id = $lawyer_office_id and request_type = 'S'";
					$req_value ['request_done'] = 'Y';
					$this->common->updateRecord ( 'case_requests', $req_value, $where );
				}
				if ($action_code == 'RESUME') {
					// set all resume (complete) requests on this case to done
					$where = "case_id = '$case_id' and lawyer_office_id = $lawyer_office_id and request_type = 'R'";
					$req_value ['request_done'] = 'Y';
					$this->common->updateRecord ( 'case_requests', $req_value, $where );
				}
				// end for new suspend / resume request.
				
				if ($action_code == 'MODIFICATIONS_DONE') {
					redirect ( 'case_management/view_cases' );
					//} else if ($action_code == 'RESUME' || $action_code == 'REOPEN') {
					//redirect ( 'case_management/view_case_details/' . $case_id );
				} else {
					// parentPopupCallback in view_case_details.php
					echo "<script>window.close(); window.opener.parentPopupCallback();</script>";
				}
				// redirect('case_management/view_case_details/'.$case_id);
			} else {
				$lawyer_office_id = $userRecord ['lawyer_office_id'];
				$data ['case_id'] = $case_id;
				$data ['lawyer_office_id'] = $lawyer_office_id;
				// send customer_id for resume or reopen action from user other than "ADMIN" for example "LAWYER"
				$data ['customer_id'] = "";
				$user_type_code = $userRecord ['user_type_code'];
				if (($action_code == 'REOPEN' || $action_code == 'RESUME') && $user_type_code != "ADMIN"){
					$case = $this->case_model->getCaseDataForTransaction($case_id,$lawyer_office_id);
					if( isset($case)){
						$data ['customer_id'] = $case['customer_id'];
					}
				}
				$data ['action_code'] = $action_code;
				$available_regions = $this->common->getAllRow ( "region", "where lawyer_office_id = '" . $lawyer_office_id . "'" );
				$data ['available_regions'] = $available_regions;
				$data ['lawyers_per_region'] = array ();
				$this->load->model ( "lawyer_office_model" );
				
				$this->load->model ( "case_model" );
				$case_transactions = $this->case_model->getCaseTransactions ( $case_id, $lawyer_office_id );
				$data ['case_transactions'] = $case_transactions;
				
				// foreach ($available_regions as $region) {
				$regionid = null;
				if ($region_id != null && isset ( $region_id ))
					$regionid = $region_id;
					else if (isset ( $data ['available_regions'] [0] ['region_id'] ))
						$regionid = $data ['available_regions'] [0] ['region_id'];
						if (isset ( $regionid )) {
							$lawyers_in_region = $this->lawyer_office_model->getLawyersInRegion ( $lawyer_office_id, $regionid );
							$data ['lawyers_per_region'] = $lawyers_in_region;
							$data ['region_id'] = $regionid;
						} else {
							$data ['lawyers_per_region'] = null;
							$data ['region_id'] = null;
						}
						// }
						
						$lawyer_case_record = $this->common->getOneRecField_Limit ( "lawyer_id", "lawyer_case", "where case_id ='" . $case_id . "'" );
						$data ['assigned_lawyer'] = "";
						if ($lawyer_case_record != null) {
							$data ['assigned_lawyer'] = $lawyer_case_record->lawyer_id;
						}
						
						$data ['suspend_reasons'] = $this->common->getAllRow ( 'case_suspend_reason', '' );
						$data ['close_reasons'] = $this->common->getAllRow ( 'case_close_reason', '' );
						
						$this->load->model ( "case_model" );
						$data ['debenture'] = $this->case_model->getCaseDebenture ( $case_id, $lawyer_office_id );
						
						// get last state code in order to validate with the new transition date
						$case_transactions = $this->case_model->getCaseTransactions ( $case_id, $lawyer_office_id );
						$last_state_date = '';
						if ($case_transactions) {
							$i = 0;
							$this->load->library ( '../controllers/dates_conversion' );
							foreach ( $case_transactions as $record ) {
								if ($record ['state_date'] != null && $record ['state_date'] != "") {
									$case_transactions [$i] ['state_date'] = $this->dates_conversion->GregorianToHijri ( $record ['state_date'] );
									$last_state_date = $case_transactions [$i] ['state_date'];
								}
								$i ++;
							}
						}
						$data ['last_state_date'] = $last_state_date;
						
						$this->load->view ( 'cases/action', $data );
			}
		}
		function getNewParameterValue(&$value, &$trans_values, $param_value, $param_name, $nullFields) {
			if ($param_value != null && isset ( $param_value ) && $param_value != "") {
				$value [$param_name] = $param_value;
				$trans_values [$param_name] = $param_value;
			}
			
			if ($nullFields != null && isset ( $nullFields ) && $nullFields != "" && strpos ( $nullFields, $param_name ) !== false) {
				$value [$param_name] = '';
				$trans_values [$param_name] = '';
			}
		}
		function upload_file(&$value, &$trans_values, $param_name, $case_id, $name, $tmp_name) {
			$user_id = $this->session->userdata ( 'userid' );
			$where = "where user_id =" . $user_id;
			$userRecord = $this->common->getOneRow ( 'user', $where );
			$lawyer_office_id = $userRecord ["lawyer_office_id"];
			if (isset ( $name ) && $name != "") {
				if (! is_dir ( "./cases_files/" . $lawyer_office_id . "/" . $case_id )) {
					mkdir ( './cases_files/' . $lawyer_office_id . '/' . $case_id, 0777, true );
					copy ( './cases_files/index.html', './cases_files/' . $lawyer_office_id . '/' . $case_id . '/index.html' );
				}
				$file = $name;
				$uploadedfile = $tmp_name;
				$ext = strstr ( $name, "." );
				
				$source = $tmp_name;
				$file = $param_name . "-" . str_replace ( ' ', '-', $file );
				$filepath = $file;
				$save = "./cases_files/" . $lawyer_office_id . "/" . $case_id . "/" . $filepath;
				move_uploaded_file ( $uploadedfile, $save );
				
				$value [$param_name] = "cases_files/" . $lawyer_office_id . "/" . $case_id . "/" . $file;
				$trans_values [$param_name] = "cases_files/" . $lawyer_office_id . "/" . $case_id . "/" . $file;
			}
		}
		function verifyAddCase() {
			$this->load->model ( "case_model" );
			$user_id = $this->session->userdata ( 'userid' );
			$where = "where user_id =" . $user_id;
			$userRecord = $this->common->getOneRow ( 'user', $where );
			$lawyer_office_id = $userRecord ['lawyer_office_id'];
			$user_name = $userRecord ['user_name'];
			$user_type_code = $userRecord ['user_type_code'];
			
			// check that number of cases per month accept to add a new one
			$this->load->model ( "lawyer_office_model" );
			$lawyerOfficePackage = $this->lawyer_office_model->getLawyerOfficePackage ( $lawyer_office_id );
			$maxNumOfCasesPerMonth = $lawyerOfficePackage ['num_of_cases_a_month'];
			$currCount = 0;
			$this->load->library ( '../controllers/dates_conversion' );
			$dates_conversion = new dates_conversion ();
			date_default_timezone_set ( 'Asia/Riyadh' );
			$current_date_time = date ( "Y-m-d H:i:s" );
			$dt = new DateTime ( $current_date_time );
			$currDateG = $dt->format ( 'Y-m-d' );
			$currDateH = $this->dates_conversion->GregorianToHijri ( $currDateG );
			$hDate_parts = explode ( "/", $currDateH );
			$currMonthH = ltrim ( $hDate_parts [1], '0' );
			$all_cases = $this->case_model->getAllOfficeCases ( $lawyer_office_id );
			if ($all_cases) {
				$i = 0;
				foreach ( $all_cases as $record ) {
					$creationdate = $this->dates_conversion->GregorianToHijri ( $record ['creation_date'] );
					$hParts = explode ( "/", $creationdate );
					$m = ltrim ( $hParts [1], '0' );
					if ($m == $currMonthH) {
						$currCount ++;
					}
					$i ++;
				}
			}
			
			if ($currCount < $maxNumOfCasesPerMonth) {
				$this->output->set_content_type ( "application/json" )->set_output ( json_encode ( array (
						'status' => true
				) ) );
			} else {
				$this->output->set_content_type ( "application/json" )->set_output ( json_encode ( array (
						'status' => false
				) ) );
			}
		}
		function upload_file_tmp($param_name, $case_id, $name, $tmp_name) {
			$user_id = $this->session->userdata ( 'userid' );
			$where = "where user_id =" . $user_id;
			$userRecord = $this->common->getOneRow ( 'user', $where );
			$lawyer_office_id = $userRecord ["lawyer_office_id"];
			if (isset ( $name ) && $name != "") {
				if (! is_dir ( "./cases_tmp_files/" . $lawyer_office_id . "/" . $case_id )) {
					mkdir ( './cases_tmp_files/' . $lawyer_office_id . "/" . $case_id, 0777, true );
					copy ( './cases_files/index.html', './cases_tmp_files/' . $lawyer_office_id . "/" . $case_id . '/index.html' );
				}
				if (! is_dir ( "./cases_files/" . $lawyer_office_id . "/" . $case_id )) {
					mkdir ( './cases_files/' . $lawyer_office_id . "/" . $case_id, 0777, true );
					copy ( './cases_files/index.html', './cases_files/' . $lawyer_office_id . "/" . $case_id . '/index.html' );
				}
				$file = $name;
				$uploadedfile = $tmp_name;
				$ext = strstr ( $name, "." );
				
				$source = $tmp_name;
				$file = $param_name . "-" . str_replace ( ' ', '-', $file );
				$filepath = $file;
				$save = "./cases_tmp_files/" . $lawyer_office_id . "/" . $case_id . "/" . $filepath;
				move_uploaded_file ( $uploadedfile, $save );
				
				return "cases_tmp_files/" . $lawyer_office_id . "/" . $case_id . "/" . $file;
			}
		}
		function upload_file_get_max() {
			$ini_PostSize = preg_replace ( "/[^0-9,.]/", "", ini_get ( 'post_max_size' ) ) * (1024 * 1024);
			$ini_FileSize = preg_replace ( "/[^0-9,.]/", "", ini_get ( 'upload_max_filesize' ) ) * (1024 * 1024);
			$maxFileSize = ($ini_PostSize < $ini_FileSize ? $ini_PostSize : $ini_FileSize);
			$this->output->set_content_type ( "application/json" )->set_output ( json_encode ( array (
					'max_size' => $maxFileSize
			) ) );
		}
		function upload_file_progress() {
			$ini_PostSize = preg_replace ( "/[^0-9,.]/", "", ini_get ( 'post_max_size' ) ) * (1024 * 1024);
			$ini_FileSize = preg_replace ( "/[^0-9,.]/", "", ini_get ( 'upload_max_filesize' ) ) * (1024 * 1024);
			$maxFileSize = ($ini_PostSize < $ini_FileSize ? $ini_PostSize : $ini_FileSize);
			$x = '0';
			$others_value = '';
			
			$i = 1;
			
			foreach ( $_FILES as $file ) {
				if (!$file) { // if file not chosen
					die("خطأ: برجاء إدخال الملف اولا");
				}
				
				if($file["size"]>$maxFileSize){
					die("خطأ: حجم الملف كبير (اقصي حجم للتحميل هو  ".$maxFileSize/(1024*1024)."MB)");
				}
				if($file["error"]) {
					die("خطأ: الملف لا يمكن معالجته");
				}
				
				$field_name = $_POST ["field_name"];
				if ($field_name == "others") {
					$field_name = $field_name . "_" . $i;
				}
				$uploaded_name = self::upload_file_tmp ( $field_name, $_POST ["case_id"], $file ["name"], $file ["tmp_name"] );
				$others_value = $others_value . "," . $uploaded_name;
				if ($uploaded_name != NULL && $uploaded_name != '') {
					$x = "1";
				} else {
					$x = "0";
				}
				$i ++;
			}
			
			$others_value = trim ( $others_value, "," );
			
			if ($x == "1") {
				echo $others_value;
			} else {
				echo '0';
			}
		}
		function rearrange_cases_files($law_offc_id = '') {
			try {
				$old_upload_path = "./cases_files/*";
				$directories = glob ( $old_upload_path, GLOB_ONLYDIR );
				$dirs = array_filter ( glob ( $old_upload_path ), 'is_dir' );
				
				for($i = 0; $i < count ( $directories ); $i ++) {
					$folder_name = substr ( $directories [$i], strrpos ( $directories [$i], "/" ) + 1 );
					// echo 'folder_name:=' . $folder_name . '<br/>';
					if ($folder_name != null && strlen ( $folder_name ) == 6 && substr ( $folder_name, 0, 1 ) === 'C') {
						// echo 'Considered folder_name:=' . $folder_name . '<br/>';
						$case_id = $folder_name;
						$query_str = "select * from `case` where `case`.`case_id` = '" . $case_id . "'";
						$query_result = $this->db->query ( $query_str );
						if ($query_result->num_rows () > 0) {
							$records = $query_result->result_array ();
							$case_record = $records [0];
							// echo 'case_record:=' . var_dump ( $case_record ) . '<br/>';
							// echo 'case_record not null' . '<br/>';
							$lawyer_office_id = $case_record ['lawyer_office_id'];
							// echo 'lawyer_office_id:=' . $lawyer_office_id . '<br/>';
							
							log_message ( 'error', 'Case:: ' . $case_id . '  lawyer_office_id::' . $lawyer_office_id );
							if ($law_offc_id != '' && $lawyer_office_id != $law_offc_id) {
								log_message ( 'error', 'Case:: ' . $case_id . '  lawyer_office_id::' . $lawyer_office_id . '  Will be ignored' );
								continue;
							}
							log_message ( 'error', 'Case:: ' . $case_id . '  lawyer_office_id::' . $lawyer_office_id . '  Will be MOVED' );
							// 3 steps
							// step 1: move cases folder to the new folder after create it
							if (! is_dir ( "./cases_files/" . $lawyer_office_id . "/" . $case_id )) {
								mkdir ( './cases_files/' . $lawyer_office_id . '/' . $case_id, 0777, true );
								copy ( './cases_files/index.html', './cases_files/' . $lawyer_office_id . '/' . $case_id . '/index.html' );
							}
							
							$old_path = "./cases_files/" . $case_id . "/*";
							foreach ( glob ( $old_path ) as $file ) {
								if (! is_dir ( $file )) {
									$file_name = basename ( $file );
									rename ( "./cases_files/" . $case_id . "/" . $file_name, "./cases_files/" . $lawyer_office_id . '/' . $case_id . "/" . $file_name );
								}
							}
							rmdir ( "./cases_files/" . $case_id );
							
							// step 2: update case record to modify file paths
							$replace_from = 'cases_files/C';
							$replace_to = 'cases_files/' . $lawyer_office_id . '/C';
							
							$replace_from2 = "https://altanfeeth.com/court_case_management/";
							$replace_to2 = "";
							// echo 'replace from: ' . $replace_from. ' replace to: ' . $replace_to . '<br/>';
							$values = array ();
							if ($case_record ['debenture'] != NULL && trim ( $case_record ['debenture'] ) != '') {
								$values ['debenture'] = str_replace ( $replace_from, $replace_to, trim ( $case_record ['debenture'] ) );
								$values ['debenture'] = str_replace ( $replace_from2, $replace_to2, trim ( $values ['debenture'] ) );
							}
							if ($case_record ['id'] != NULL && trim ( $case_record ['id'] ) != '') {
								$values ['id'] = str_replace ( $replace_from, $replace_to, trim ( $case_record ['id'] ) );
								$values ['id'] = str_replace ( $replace_from2, $replace_to2, trim ( $values ['id'] ) );
							}
							if ($case_record ['contract'] != NULL && trim ( $case_record ['contract'] ) != '') {
								$values ['contract'] = str_replace ( $replace_from, $replace_to, trim ( $case_record ['contract'] ) );
								$values ['contract'] = str_replace ( $replace_from2, $replace_to2, trim ( $values ['contract'] ) );
							}
							if ($case_record ['decision_34_file'] != NULL && trim ( $case_record ['decision_34_file'] ) != '') {
								$values ['decision_34_file'] = str_replace ( $replace_from, $replace_to, trim ( $case_record ['decision_34_file'] ) );
								$values ['decision_34_file'] = str_replace ( $replace_from2, $replace_to2, trim ( $values ['decision_34_file'] ) );
							}
							if ($case_record ['advertisement_file'] != NULL && trim ( $case_record ['advertisement_file'] ) != '') {
								$values ['advertisement_file'] = str_replace ( $replace_from, $replace_to, trim ( $case_record ['advertisement_file'] ) );
								$values ['advertisement_file'] = str_replace ( $replace_from2, $replace_to2, trim ( $values ['advertisement_file'] ) );
							}
							if ($case_record ['invoice_file'] != NULL && trim ( $case_record ['invoice_file'] ) != '') {
								$values ['invoice_file'] = str_replace ( $replace_from, $replace_to, trim ( $case_record ['invoice_file'] ) );
								$values ['invoice_file'] = str_replace ( $replace_from2, $replace_to2, trim ( $values ['invoice_file'] ) );
							}
							if ($case_record ['referral_paper'] != NULL && trim ( $case_record ['referral_paper'] ) != '') {
								$values ['referral_paper'] = str_replace ( $replace_from, $replace_to, trim ( $case_record ['referral_paper'] ) );
								$values ['referral_paper'] = str_replace ( $replace_from2, $replace_to2, trim ( $values ['referral_paper'] ) );
							}
							
							if ($case_record ['suspend_file'] != NULL && trim ( $case_record ['suspend_file'] ) != '') {
								$values ['suspend_file'] = str_replace ( $replace_from, $replace_to, trim ( $case_record ['suspend_file'] ) );
								$values ['suspend_file'] = str_replace ( $replace_from2, $replace_to2, trim ( $values ['suspend_file'] ) );
							}
							if ($case_record ['consignment_image'] != NULL && trim ( $case_record ['consignment_image'] ) != '') {
								$values ['consignment_image'] = str_replace ( $replace_from, $replace_to, trim ( $case_record ['consignment_image'] ) );
								$values ['consignment_image'] = str_replace ( $replace_from2, $replace_to2, trim ( $values ['consignment_image'] ) );
							}
							if ($case_record ['others'] != NULL && trim ( $case_record ['others'] ) != '') {
								$values ['others'] = str_replace ( $replace_from, $replace_to, trim ( $case_record ['others'] ) );
								$values ['others'] = str_replace ( $replace_from2, $replace_to2, trim ( $values ['others'] ) );
							}
							
							// echo 'New Values: ' . var_dump ( $values ) . '<br/>';
							
							$where = "case_id ='" . $case_id . "' and lawyer_office_id = '" . $lawyer_office_id . "'";
							$affected_rows = $this->common->updateRecord ( 'case', $values, $where );
							
							// step 3: update case transactions records to modify file paths
							// loop to update each single record in order not to distort values
							$query_str = "select * from `case_transactions` where `case_transactions`.`case_id` = '" . $case_id . "' and `case_transactions`.`lawyer_office_id` = '" . $lawyer_office_id . "'";
							$query_result = $this->db->query ( $query_str );
							if ($query_result->num_rows () > 0) {
								$transactions = $query_result->result_array ();
								for($j = 0; $j < count ( $transactions ); $j ++) {
									$trans = $transactions [$j];
									$values = array ();
									if ($trans ['debenture'] != NULL && trim ( $trans ['debenture'] ) != '') {
										$values ['debenture'] = str_replace ( $replace_from, $replace_to, trim ( $trans ['debenture'] ) );
										$values ['debenture'] = str_replace ( $replace_from2, $replace_to2, trim ( $values ['debenture'] ) );
									}
									if ($trans ['id'] != NULL && trim ( $trans ['id'] ) != '') {
										$values ['id'] = str_replace ( $replace_from, $replace_to, trim ( $trans ['id'] ) );
										$values ['id'] = str_replace ( $replace_from2, $replace_to2, trim ( $values ['id'] ) );
									}
									if ($trans ['contract'] != NULL && trim ( $trans ['contract'] ) != '') {
										$values ['contract'] = str_replace ( $replace_from, $replace_to, trim ( $trans ['contract'] ) );
										$values ['contract'] = str_replace ( $replace_from2, $replace_to2, trim ( $values ['contract'] ) );
									}
									if ($trans ['decision_34_file'] != NULL && trim ( $trans ['decision_34_file'] ) != '') {
										$values ['decision_34_file'] = str_replace ( $replace_from, $replace_to, trim ( $trans ['decision_34_file'] ) );
										$values ['decision_34_file'] = str_replace ( $replace_from2, $replace_to2, trim ( $values ['decision_34_file'] ) );
									}
									if ($trans ['advertisement_file'] != NULL && trim ( $trans ['advertisement_file'] ) != '') {
										$values ['advertisement_file'] = str_replace ( $replace_from, $replace_to, trim ( $trans ['advertisement_file'] ) );
										$values ['advertisement_file'] = str_replace ( $replace_from2, $replace_to2, trim ( $values ['advertisement_file'] ) );
									}
									if ($trans ['invoice_file'] != NULL && trim ( $trans ['invoice_file'] ) != '') {
										$values ['invoice_file'] = str_replace ( $replace_from, $replace_to, trim ( $trans ['invoice_file'] ) );
										$values ['invoice_file'] = str_replace ( $replace_from2, $replace_to2, trim ( $values ['invoice_file'] ) );
									}
									if ($trans ['referral_paper'] != NULL && trim ( $trans ['referral_paper'] ) != '') {
										$values ['referral_paper'] = str_replace ( $replace_from, $replace_to, trim ( $trans ['referral_paper'] ) );
										$values ['referral_paper'] = str_replace ( $replace_from2, $replace_to2, trim ( $values ['referral_paper'] ) );
									}
									if ($trans ['suspend_file'] != NULL && trim ( $trans ['suspend_file'] ) != '') {
										$values ['suspend_file'] = str_replace ( $replace_from, $replace_to, trim ( $trans ['suspend_file'] ) );
										$values ['suspend_file'] = str_replace ( $replace_from2, $replace_to2, trim ( $values ['suspend_file'] ) );
									}
									if ($trans ['consignment_image'] != NULL && trim ( $trans ['consignment_image'] ) != '') {
										$values ['consignment_image'] = str_replace ( $replace_from, $replace_to, trim ( $trans ['consignment_image'] ) );
										$values ['consignment_image'] = str_replace ( $replace_from2, $replace_to2, trim ( $values ['consignment_image'] ) );
									}
									if ($trans ['others'] != NULL && trim ( $trans ['others'] ) != '') {
										$values ['others'] = str_replace ( $replace_from, $replace_to, trim ( $trans ['others'] ) );
										$values ['others'] = str_replace ( $replace_from2, $replace_to2, trim ( $values ['others'] ) );
									}
									
									$where = "case_id ='" . $case_id . "' and lawyer_office_id = '" . $lawyer_office_id . "' and transaction_id = '" . $trans ['transaction_id'] . "'";
									$affected_rows = $this->common->updateRecord ( 'case_transactions', $values, $where );
								}
							}
						}
					}
				}
			} catch ( Exception $e ) {
				log_message ( 'error', 'ERROR:: ' . $e->getMessage () . "\n" );
			}
		}
	}
	