<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class help extends CI_Controller
{
	function __construct()
    {
        parent::__construct();
        $this -> load -> model("common");
    }
   	
	function index() {
		self::view_FAQs();
	}
	
	function view_FAQs($viewHeaders='0') {
		
		if($viewHeaders == '1'){
			
			if($this->session->userdata('userid')!='') {
				$user_id = $this->session->userdata('userid');
				$where = "where user_id =".$user_id;
				$userRecord = $this->common->getOneRow('user',$where);
				$lawyer_office_id=$userRecord['lawyer_office_id'];
				$data['profile_first_name']=$userRecord['first_name'];
				$data['profile_last_name']=$userRecord['last_name'];
				$data['profile_img']=$userRecord['profile_img'];
				
				$this -> load -> model("notification_model");
				$notifications = $this -> notification_model -> getLatestDeliveredNotificationsForUser($user_id);
				$data['notifications_list'] = $notifications['notifications_list'];
				$data['count_unseen'] = $notifications['count_unseen'];
				$data['page'] = "help/view_FAQs/".$viewHeaders;
				
				$user_type_code = $userRecord['user_type_code'];
				$data['user_type_code'] = $user_type_code;

			} else {
				$data['user_name']='';
				$this->load->view('user_login',$data);
				
			}
			
		}
		
		 if(extract($_POST)) {
				$searchTextVal= $this->input->post('searchTextVal');
			 	$allData = array();
			 	$this -> load -> model("help_model");
			 	$cats = $this->help_model->getAllCategories();
			 	$i = 0;
			 	if($cats){
			 		foreach($cats as $cat) {
			 			$catId = $cat['category_id'];
			 			$FAQs = $this->help_model->getFrequentlyAskedQuestionsWithFilter($catId, $searchTextVal);
			 			if($FAQs){
			 				$allData[$i] = $FAQs;
			 				$i++;
			 			}
			 		}
			 	}
					
				$data['categories'] = $allData;
				$data['viewHeaders'] = $viewHeaders;
				//$this->load->view('help',$data);
				$faq_View=$this->load->view('help',$data, TRUE);
				echo json_encode($faq_View);
		    }
		    else
		    {
		        $allData = array();
				$this -> load -> model("help_model");
				$cats = $this->help_model->getAllCategories();
				$i = 0;
				if($cats){
					foreach($cats as $cat) {
						$catId = $cat['category_id'];
						$FAQs = $this->help_model->getFrequentlyAskedQuestions($catId);
						if($FAQs){
							$allData[$i] = $FAQs;
							$i++;
						}
					}
				}
				$data['categories'] = $allData;
				$data['viewHeaders'] = $viewHeaders;
				//$this->load->view('help',$data);
				$dataAjax=$this->load->view('help',$data, TRUE);
				echo $dataAjax;
		    }
    

	}
}

