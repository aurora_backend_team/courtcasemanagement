<?php
class firebase_notification {
	
	public function __construct(){
	}
	
	public function sendPushNotificationToGCMSever($token, $title, $message){
		
		$msg = array
		(
				'message' 	=> 'here is a message. message',
				'title'		=> 'This is a title. title',
				'subtitle'	=> 'This is a subtitle. subtitle',
				'tickerText'	=> 'Ticker text here...Ticker text here...Ticker text here',
				'vibrate'	=> 1,
				'sound'		=> 1,
				'largeIcon'	=> 'large_icon',
				'smallIcon'	=> 'small_icon'
		);
		
		$registrationIds = array($token);
		
		$fields = array(
				
				//'notification' => array('title' => 'Working Good', 'body' => 'That is all we want'),
				// 'data' => array('message' => $message)
				// 'registration_ids' 	=> $registrationIds,
				//  'data' => $msg
				"content_available"=> true,
				"priority"=> "high",
				'data' => $message,
				"notification"=> array(
				"title"=> "التنفيذ",
				"body"=> $message['message']
				),
				"content_available"=> true,
				"priority"=> "high",
				'to' => $token,
		);
		
		$headers = array(
				'Authorization:key=AAAAcvLyq2U:APA91bGLhyq1sf3puj19rUTWfNnXlgdKWnwwQJo17A2VZRawAzZg2Zr9dIqxmvsPlSbEqovV159be_D1i-seRYXkRqT_JjzUICRQQjhsD_u5LY32NIY8I_R67c045wZMMKi4-gTu1Y7_' ,
				'Content-Type:application/json'
		);
		
		
		
		////
		/*    $registrationIds = array( $_GET['id'] );
		 // prep the bundle
		 $msg = array
		 (
		 'message' 	=> 'here is a message. message',
		 'title'		=> 'This is a title. title',
		 'subtitle'	=> 'This is a subtitle. subtitle',
		 'tickerText'	=> 'Ticker text here...Ticker text here...Ticker text here',
		 'vibrate'	=> 1,
		 'sound'		=> 1,
		 'largeIcon'	=> 'large_icon',
		 'smallIcon'	=> 'small_icon'
		 );
		 $fields = array
		 (
		 'registration_ids' 	=> $registrationIds,
		 'data'			=> $msg
		 );
		 
		 $headers = array
		 (
		 'Authorization: key=' . API_ACCESS_KEY,
		 'Content-Type: application/json'
		 );
		 
		 ///////////*/
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		
		$result = curl_exec($ch);
		
		curl_close($ch);
		return $result;
	}
	
	
	public function sendPushNotificationToTopic($topic_name, $title, $message){
		
		
		
		
		$fields = array(
				'to' => "/topics/".$topic_name,
				//'notification' => array('title' => 'Working Good', 'body' => 'That is all we want'),
				// 'data' => array('message' => $message)
				// 'registration_ids' 	=> $registrationIds,
				//  'data' => $msg
				'data' => $message
		);
		
		$headers = array(
				'Authorization:key=AIzaSyArt12b0ayJD5uEE0VXqnhHmN3oiLl-GPo' ,
				'Content-Type:application/json'
		);
		
		
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		
		$result = curl_exec($ch);
		
		curl_close($ch);
		return $result;
	}
}