<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
require (APPPATH . '/libraries/REST_Controller.php');
class User extends REST_Controller {
	function login_post() {
		$this->load->library('encrypt');
		if (isset ( $_POST ['user_name'] ) && isset ( $_POST ['password'] ) && isset ( $_POST ['gcmid'] ) && isset ( $_POST ['user_ip'] ) && isset ( $_POST ['for_count'] )) {
			$for_count = $_POST ['for_count'];
			$username = $_POST ['user_name'];
			$password = $_POST ['password'];
			// $user_ip = $_POST ['user_ip'];
			$gcmid = $_POST ['gcmid'];
			
			if ($username != "" && $password != "") {
				$table = "user";
				$where = "where user_name='" . $username . "'";# and password='" . $password . "'";
				$row_user = $this->common->getOneRow ( $table, $where );
				if (isset($row_user) && count($row_user) > 0 && $password == $this->encrypt->decode($row_user['password'])) {
					//$table = "user";
					//$where = "where user_name='" . $username . "' and password='" . $password . "'";
					//$row_user = $this->common->getOneRow ( $table, $where );
					
					$update ['gcm_id'] = $gcmid;
					$user_id = $row_user ['user_id'];
					$count = $this->common->updateRecord ( "user", $update, "user_id ='$user_id'" );
					
					$this->load->model ( "case_model" );
					$data = $this->case_model->get_cases_count ( $user_id );
					self::insert_activity ( $user_id, 'تسجيل  دخول', $_POST ['user_ip'] );
					$this->load->model ( "notification_model" );
					// $notifications = $this->notification_model->getLatestDeliveredNotificationsForUser ( $user_id );
					$notifications = self::getLatestDeliveredNotificationsForUser ( $user_id );
					if ($for_count == 0)
						$messg = 'تم تسجيل الدخول بنجاح';
					else
						$messg = 'Counts Updated';
					date_default_timezone_set ( 'Asia/Riyadh' );
		            $current_date_time = date ( "Y-m-d H:i:s" );
                    $this->load->library ( '../controllers/dates_conversion' );
                    $hijri_date = $this->dates_conversion->GregorianToHijri ( $current_date_time  );
					$this->response ( array (
							'message' => $messg,
							'user_id' => $user_id,
							'user_type' => $row_user ['user_type_code'],
							'user_name' => $row_user ['first_name'] . " " . $row_user ['last_name'],
							'records_per_page' => 50,
							'notifications' => $notifications ['notifications_list'],
							'unseen_notifications_count' => $notifications ['count_unseen'],
							'states' => $data ['ordered_states'],
							'cases_count' => $data ['counts'],
							'hijri_date'=> $hijri_date,
							'success' => '1' 
					), 200 );
				} else {
					
					$this->response ( array (
							'message' => 'اسم المستخدم او كلمة المرور غير صحيحة',
							'success' => '0' 
					), 200 );
				}
			} else {
				$this->response ( array (
						'message' => 'بعض البيانات مفقودة',
						'success' => '0' 
				), 200 );
			}
		} else {
			$this->response ( array (
					'message' => 'بعض البيانات مفقودة',
					'success' => '0' 
			), 200 );
		}
	}
	function logout_post() {
		if (isset ( $_POST ['user_id'] ) && isset ( $_POST ['user_ip'] )) {
			$update ['gcm_id'] = "";
			$user_id = $_POST ['user_id'];
			$count = $this->common->updateRecord ( "user", $update, "user_id ='$user_id'" );
			if ($count > 0) {
				self::insert_activity ( $user_id, 'تسجيل خروج', $_POST ['user_ip'] );
				$this->response ( array (
						'message' => 'تم تسجيل الخروج بنجاح',
						'success' => '1' 
				), 200 );
			} else
				$this->response ( array (
						'message' => 'المستخدم موجود او سجل الدخول بالفعل',
						'success' => '1' 
				), 200 );
		} else
			$this->response ( array (
					'message' => 'بعض البيانات مفقودة',
					'success' => '0' 
			), 200 );
	}
	function refresh_token_post() {
		if (isset ( $_POST ['user_id'] ) && isset ( $_POST ['gcmid'] )) {
			$update ['gcm_id'] = $_POST ['gcmid'];
			$user_id = $_POST ['user_id'];
			$this->common->updateRecord ( "user", $update, "user_id ='$user_id'" );
			$this->response ( array (
					'message' => '',
					'success' => '1' 
			), 200 );
		} else
			$this->response ( array (
					'message' => 'بعض البيانات مفقودة',
					'success' => '0' 
			), 200 );
	}
	function insert_activity($user_id, $activity, $user_ip) {
		$values = array ();
		$values ['user_id'] = $user_id;
		$values ['activity'] = $activity;
		$this->load->library ( '../controllers/util' );
		$values ['user_IP'] = $this->util->get_client_ip ();
		// $values['user_IP'] = self::get_client_ip();
		date_default_timezone_set ( 'Asia/Riyadh' );
		$current_date_time = date ( "Y-m-d H:i:s" );
		$values ['date_time'] = $current_date_time;
		$this->common->insertRecord ( 'user_activities', $values );
	}
	function mark_notifications_seen_post() {
		if (isset ( $_POST ['user_id'] )) {
			$user_id = $_POST ['user_id'];
			$value ['seen'] = "Y";
			$where = "to_user_id = '" . $user_id . "' AND  seen = 'N' ";
			$this->common->updateRecord ( 'delivered_notifications', $value, $where );
			$this->response ( array (
					'message' => 'كل الاشعارات مرئية الان ',
					'success' => '1' 
			), 200 );
		} else {
			$this->response ( array (
					'message' => 'بعض البيانات مفقودة',
					'success' => '0' 
			), 200 );
		}
	}
	function getLatestDeliveredNotificationsForUser($user_id) {
		$conditionsString = "delivered_notifications.to_user_id = '" . $user_id . "' AND delivered_notifications.message != ''";
		$this->db->select ( 'delivered_notifications.email_id, delivered_notifications.notification_type_code, delivered_notifications.case_id,
						 delivered_notifications.sent_date_time, delivered_notifications.waiting_days, delivered_notifications.seen,
						 delivered_notifications.message' );
		$this->db->from ( 'delivered_notifications' );
		$this->db->where ( $conditionsString );
		$this->db->order_by ( "email_id", "desc" );
		$query = $this->db->get ();
		
		$data = array ();
		$notifications = array ();
		$count_unseen = 0;
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			$i = 0;
			foreach ( $records as $record ) {
				if ($record ['seen'] == 'Y' && $i == 10) {
					break;
				} else {
					$notifications [$i] ['email_id'] = $record ['email_id'];
					$notifications [$i] ['notification_type_code'] = $record ['notification_type_code'];
					$notifications [$i] ['case_id'] = $record ['case_id'];
					$notifications [$i] ['sent_date_time'] = $record ['sent_date_time'];
					$notifications [$i] ['waiting_days'] = $record ['waiting_days'];
					$notifications [$i] ['seen'] = $record ['seen'];
					$notifications [$i] ['message'] = $record ['message'];
					if ($record ['seen'] == 'N') {
						$count_unseen ++;
					}
				}
				$i ++;
			}
		}
		$data ['notifications_list'] = $notifications;
		$data ['count_unseen'] = $count_unseen;
		return $data;
	}
	function get_user_notif_post() {
		if (isset ( $_POST ['user_id'] )) {
			$user_id = $_POST ['user_id'];
			$this->load->model ( "notification_model" );
			// $notifications = $this->notification_model->getLatestDeliveredNotificationsForUser ( $user_id );
			$notifications = self::getLatestDeliveredNotificationsForUser ( $user_id );
			
			$this->response ( array (
					'message' => "تم تحميل الاشعارات",
					'notifications' => $notifications ['notifications_list'],
					'unseen_notifications_count' => $notifications ['count_unseen'],
					'success' => '1' 
			), 200 );
		} else
			$this->response ( array (
					'message' => 'بعض البيانات مفقودة',
					'success' => '0' 
			), 200 );
	}
}

?>