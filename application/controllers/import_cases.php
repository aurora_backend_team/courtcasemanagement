<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class import_cases extends CI_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->model ( "common" );
		$this->load->model ( "case_model" );
		if ($this->session->userdata ( 'userid' ) == '' || ($this->session->userdata ( 'user_type' ) != 'ADMIN' && $this->session->userdata ( 'user_type' ) != 'CUSTOMER')) {
			redirect ( 'user_login', 'refresh' );
		}
	}
	function index() {
		self::excel_import ();
	}
	function excel_import() {
		self::logToFile ( "--------------------------------------------" );
		self::logToFile ( " start " . date ( 'y-m-d h:m' ) );
		
			$user_id = $this->session->userdata ( 'userid' );
			$where = "where user_id =" . $user_id;
			$userRecord = $this->common->getOneRow ( 'user', $where );
			$lawyer_office_id = $userRecord ['lawyer_office_id'];
			
			$file_path = $_FILES ["upload"] ["tmp_name"];
			
			include 'Classes/PHPExcel/IOFactory.php';
			$inputFileName = $file_path;
			$objPHPExcel = PHPExcel_IOFactory::load ( $inputFileName );
			$allDataInSheet = $objPHPExcel->getActiveSheet ()->toArray ( null, true, true, true );
			$arrayCount = count ( $allDataInSheet ); // Here get total count of row in that Excel sheet
			date_default_timezone_set ( 'Asia/Riyadh' ); // set time zone for all current dates set
			$inserted_count = 0;
			$updated_count = 0;
			$failed_count = 0;
			$trans_itr = 0;
			$trans_values = array ();
			$failed_record_numbers = array ();
			$all_states = array ();
			$all_states ["UNDER_REVIEW"] = 0;
			$all_states ["ASSIGNED_TO_LAWYER"] = 1;
			$all_states ["REGISTERED"] = 2;
			$all_states ["DECISION_34"] = 3;
			$all_states ["PUBLICLY_ADVERTISED"] = 4;
			$all_states ["DECISION_46"] = 5;
			$incorrect = false;
			// looping on Excel sheet data
			for($i = 2; $i <= $arrayCount; $i ++) {
				$incorrect = false;
				$case_values = array ();
				$inserted_case = array ();
				$updated_lawyer_case = array ();
				$inserted_lawyer_case = array ();
				
				// for not inserting empty records as case_id may be empty while
				// inserting new case
				if ($allDataInSheet [$i] ["B"] == "" /*customer_id*/){
					continue;
				}
				$case_values ["case_id"] = $allDataInSheet [$i] ["A"];
				$case_values ["lawyer_office_id"] = $lawyer_office_id;
				$case_values ["customer_id"] = $allDataInSheet [$i] ["B"];
				if (! $incorrect) {
					$incorrect = self::checkCorrection ( $case_values ["customer_id"], "fk", $case_values ["case_id"], $lawyer_office_id );
				}
				$case_values ["contract_number"] = $allDataInSheet [$i] ["C"];
				$case_values ["debtor_name"] = $allDataInSheet [$i] ["D"];
				$case_values ["client_name"] = $allDataInSheet [$i] ["E"];
				$case_values ["client_id_number"] = $allDataInSheet [$i] ["F"];
				$case_values ["client_type"] = $allDataInSheet [$i] ["G"];
				if (! $incorrect) {
					$incorrect = self::checkCorrection ( $case_values ["client_type"], "fk", $case_values ["case_id"], $lawyer_office_id );
				}
				$case_values ["obligation_value"] = $allDataInSheet [$i] ["H"];
				$case_values ["number_of_late_instalments"] = $allDataInSheet [$i] ["I"];
				$case_values ["due_amount"] = $allDataInSheet [$i] ["J"];
				$case_values ["remaining_amount"] = $allDataInSheet [$i] ["K"];
				$case_values ["total_debenture_amount"] = $allDataInSheet [$i] ["L"];
				$this->db->trans_strict ( FALSE );
				$this->db->trans_begin ();
				self::logToFile ( "STATUS ------------------------------ " . $this->db->trans_status () );
				try {
					// to make dates with accurate format and if it is empty
					// use it as it is with empty value in db
					if (isset ( $allDataInSheet [$i] ["M"] )) {
						$date = new DateTime ( $allDataInSheet [$i] ["M"] );
						$case_values ["creation_date"] = $date->format ( 'Y-m-d' );
					} else {
						$case_values ["creation_date"] = $allDataInSheet [$i] ["M"];
					}
					$case_values ["executive_order_number"] = $allDataInSheet [$i] ["N"];
					
					$case_values ["order_number"] = $allDataInSheet [$i] ["O"];
					$case_values ["circle_number"] = $allDataInSheet [$i] ["P"];
					$case_values ["decision_34_number"] = $allDataInSheet [$i] ["Q"];
					// to make dates with accurate format and if it is empty
					// use it as it is with empty value in db
					if (isset ( $allDataInSheet [$i] ["R"] )) {
						$date = new DateTime ( $allDataInSheet [$i] ["R"] );
						$case_values ["decision_34_date"] = $date->format ( 'Y-m-d' );
					} else {
						$case_values ["decision_34_date"] = $allDataInSheet [$i] ["R"];
					}
					// to make dates with accurate format and if it is empty
					// use it as it is with empty value in db
					if (isset ( $allDataInSheet [$i] ["S"] )) {
						$date = new DateTime ( $allDataInSheet [$i] ["S"] );
						$case_values ["decision_46_date"] = $date->format ( 'Y-m-d' );
					} else {
						$case_values ["decision_46_date"] = $allDataInSheet [$i] ["S"];
					}
					$case_values ["current_state_code"] = $allDataInSheet [$i] ["T"];
					$case_values ["region_id"] = $allDataInSheet [$i] ["U"];
					if (! $incorrect) {
						$incorrect = self::checkCorrection ( $case_values ["region_id"], "region_id", $case_values ["case_id"], $lawyer_office_id );
					}
					// to make dates with accurate format and if it is empty
					// use it as it is with empty value in db
					if (isset ( $allDataInSheet [$i] ["W"] )) {
						$date = new DateTime ( $allDataInSheet [$i] ["W"] );
						$case_values ["executive_order_date"] = $date->format ( 'Y-m-d' );
					} else {
						$case_values ["executive_order_date"] = $allDataInSheet [$i] ["W"];
					}
					// to make dates with accurate format and if it is empty
					// use it as it is with empty value in db
					if (isset ( $allDataInSheet [$i] ["X"] )) {
						$date = new DateTime ( $allDataInSheet [$i] ["X"] );
						$case_values ["advertisement_date"] = $date->format ( 'Y-m-d' );
					} else {
						$case_values ["advertisement_date"] = $allDataInSheet [$i] ["X"];
					}
					$case_values ["suspend_reason_code"] = $allDataInSheet [$i] ["Y"];
					$case_values ["consignment_image"] = $allDataInSheet [$i] ["Z"];
					if (! $incorrect) {
						$incorrect = self::checkCorrection ( $case_values ["consignment_image"], "file", $case_values ["case_id"], $lawyer_office_id );
					}
					$case_values ["debenture"] = $allDataInSheet [$i] ["AA"];
					if (! $incorrect) {
						$incorrect = self::checkCorrection ( $case_values ["debenture"], "file", $case_values ["case_id"], $lawyer_office_id );
					}
					$case_values ["referral_paper"] = $allDataInSheet [$i] ["AB"];
					if (! $incorrect) {
						$incorrect = self::checkCorrection ( $case_values ["referral_paper"], "file", $case_values ["case_id"], $lawyer_office_id );
					}
					$case_values ["id"] = $allDataInSheet [$i] ["AC"];
					if (! $incorrect) {
						$incorrect = self::checkCorrection ( $case_values ["id"], "file", $case_values ["case_id"], $lawyer_office_id );
					}
					$case_values ["contract"] = $allDataInSheet [$i] ["AD"];
					if (! $incorrect) {
						$incorrect = self::checkCorrection ( $case_values ["contract"], "file", $case_values ["case_id"], $lawyer_office_id );
					}
					$case_values ["others"] = $allDataInSheet [$i] ["AE"];
					// to resolve extra space problem while exporting files
					$case_values ["others"] = str_replace ( ", ", ",", $case_values ["others"] );
					$othersArray = explode ( ',', $case_values ["others"] );
					if (! $incorrect) {
						foreach ( $othersArray as $other ) {
							$incorrect = self::checkCorrection ( $other, "file", $case_values ["case_id"], $lawyer_office_id );
							if ($incorrect) {
								break;
							}
						}
					}
					$case_values ["advertisement_file"] = $allDataInSheet [$i] ["AF"];
					if (! $incorrect) {
						$incorrect = self::checkCorrection ( $case_values ["advertisement_file"], "file", $case_values ["case_id"], $lawyer_office_id );
					}
					$case_values ["suspend_file"] = $allDataInSheet [$i] ["AN"];
					if (! $incorrect) {
						$incorrect = self::checkCorrection ( $case_values ["suspend_file"], "file", $case_values ["case_id"], $lawyer_office_id );
					}
					$case_values ["invoice_file"] = $allDataInSheet [$i] ["AG"];
					if (! $incorrect) {
						$incorrect = self::checkCorrection ( $case_values ["invoice_file"], "file", $case_values ["case_id"], $lawyer_office_id );
					}
					$case_values ["closing_reason"] = $allDataInSheet [$i] ["AH"];
					// to make dates with accurate format and if it is empty
					// use it as it is with empty value in db
					if (isset ( $allDataInSheet [$i] ["AI"] )) {
						$date = new DateTime ( $allDataInSheet [$i] ["AI"] );
						$case_values ["suspend_date"] = $date->format ( 'Y-m-d' );
					} else
						$case_values ["suspend_date"] = $allDataInSheet [$i] ["AI"];
					$case_values ["creation_user"] = $allDataInSheet [$i] ["AJ"];
					if (! $incorrect) {
						$incorrect = self::checkCorrection ( $case_values ["creation_user"], "fk", $case_values ["case_id"], $lawyer_office_id );
					}
					$case_values ["last_update_user"] = $allDataInSheet [$i] ["AK"];
					if (! $incorrect) {
						$incorrect = self::checkCorrection ( $case_values ["last_update_user"], "fk", $case_values ["case_id"], $lawyer_office_id );
					}
					// to make dates with accurate format and if it is empty
					// use it as it is with empty value in db
					if (isset ( $allDataInSheet [$i] ["AL"] )) {
						
						$date = new DateTime ( $allDataInSheet [$i] ["AL"] );
						$case_values ["last_update_date"] = $date->format ( 'Y-m-d' );
					} else {
						$case_values ["last_update_date"] = $allDataInSheet [$i] ["AL"];
					}
					$case_values ["notes"] = $allDataInSheet [$i] ["AM"];
					
					$caseRecord = null;
					// if case_id is not empty select it first from db if exist then update (if there is a change "described later") else insert
					// if case_id is empty generate a new case id and insert it
					
					if (isset ( $case_values ["case_id"] )) {
						// check if record exist update it
						$this->db->select ( '*' );
						$this->db->from ( 'case' );
						$this->db->where ( "case_id ='" . $case_values ["case_id"] . "' and lawyer_office_id = " . $lawyer_office_id );
						$query = $this->db->get ();
						if ($query->num_rows () > 0) {
							$records = $query->result_array ();
							$caseRecord = $records [0];
						}
					} else {
						$case_values ["case_id"] = $this->case_model->getCaseId ( $lawyer_office_id );
					}
					
					if (isset ( $caseRecord )) {
						self::logToFile ( "Update: " . $case_values ["case_id"] );
						// if case is exist get its lawyer and update it if it is changed else insert
						$where = "case_id = '" . $case_values ["case_id"] . "' and lawyer_office_id = " . $lawyer_office_id;
						$lawyerRecord = null;
						if (isset ( $allDataInSheet [$i] ["V"] )/*lawyer_id*/) {
							// check if record exist update it
							$this->db->select ( '*' );
							$this->db->from ( 'lawyer_case' );
							$this->db->where ( "case_id ='" . $case_values ["case_id"] . "' and lawyer_office_id = " . $lawyer_office_id );
							$query = $this->db->get ();
							
							if ($query->num_rows () > 0) {
								$records = $query->result_array ();
								$lawyerRecord = $records [0];
								if ($lawyerRecord ['lawyer_id'] != $allDataInSheet [$i] ["V"]) {
									$updated_lawyer_case ['lawyer_id'] = $allDataInSheet [$i] ["V"];
									$updated_lawyer_case ['case_id'] = $case_values ["case_id"];
									$where = "case_id ='" . $case_values ["case_id"] . "' and lawyer_office_id = " . $lawyer_office_id;
									self::logToFile ( "update lawyer_case: " . $case_values ["case_id"] . " " . $allDataInSheet [$i] ["V"] );
									$trans_values [$trans_itr] ["action_code"] = 'REASSIGN_TO_LAWYER';
									$insrtd_lawyer_case = $this->common->updateRecord ( 'lawyer_case', $updated_lawyer_case, $where );
								} else
									$insrtd_lawyer_case = 1;
							} else {
								$inserted_lawyer_case ['lawyer_id'] = $allDataInSheet [$i] ["V"];
								$inserted_lawyer_case ['case_id'] = $case_values ["case_id"];
								$inserted_lawyer_case ['lawyer_office_id'] = $lawyer_office_id;
								self::logToFile ( "insert lawyer case " . $case_values ["case_id"] . " " . $allDataInSheet [$i] ["V"] );
								$insrtd_lawyer_case = $this->common->insertRecord ( 'lawyer_case', $inserted_lawyer_case );
							}
							if (! isset ( $insrtd_lawyer_case ) || $insrtd_lawyer_case <= 0) {
								$incorrect = true;
								self::logToFile ( "problem in insert lawyer case $lawyer_office_id" );
							}
							// check current state is before ASSIGNED_TO_LAWYER
						} else if (! $incorrect) {
							
							if (array_key_exists ( $case_values ["current_state_code"], $all_states )) {
								$incorrect = $all_states [$case_values ["current_state_code"]] >= $all_states ["ASSIGNED_TO_LAWYER"];
								self::logToFile ( "$incorrect 245 " . $all_states [$case_values ["current_state_code"]] . ' ' . $all_states ["ASSIGNED_TO_LAWYER"] );
							} else if ($case_values ["current_state_code"] == "SUSPENDED" || $case_values ["current_state_code"] == "CLOSED") {
								$last_state = self::updateCloseSuspendPrevStates ( $all_states, $trans_values, $trans_itr, $case_values, $caseRecord, null, $lawyer_office_id, false );
								if (array_key_exists ( $last_state, $all_states )) {
									$incorrect = $last_state != "MODIFICATION_REQUIRED" && $all_states [$last_state] >= $all_states ["ASSIGNED_TO_LAWYER"];
								} else if ($last_state != "MODIFICATION_REQUIRED")
									$incorrect = true;
								self::logToFile ( "$incorrect 249 " . $last_state . ' ' . $all_states ["ASSIGNED_TO_LAWYER"] );
							}
							if ($incorrect)
								self::logToFile ( "incorrect 260" );
						}
						$lawyer_id = null;
						if (isset ( $lawyerRecord ) && isset ( $lawyerRecord ['lawyer_id'] )) {
							$lawyer_id = $lawyerRecord ['lawyer_id'];
						}
						// get changed fields and if there are then update case and make case transactions for that change
						$changed_fields = array ();
						$changed_fields = self::checkNewFieldsValues ( $all_states, $case_values, $caseRecord, $allDataInSheet [$i] ["V"], $lawyer_id );
						if (count ( $changed_fields ) > 0 && $incorrect) {
							self::logToFile ( "change but incorrect" );
							$failed_record_numbers [$failed_count] = $i - 1;
							$failed_count ++;
							continue;
						}
						$trans_values [$trans_itr] ["case_id"] = $case_values ["case_id"];
						$trans_values [$trans_itr] ["lawyer_office_id"] = $case_values ["lawyer_office_id"];
						// must set lawyer id in each trans
						if (isset ( $allDataInSheet [$i] ["V"] )/*lawyer_id*/){
							$trans_values [$trans_itr] ["lawyer_id"] = $allDataInSheet [$i] ["V"];
						} else
							$trans_values [$trans_itr] ["lawyer_id"] = $lawyer_id;
						
						foreach ( $changed_fields as $field ) {
							// bec the lawyer_id is not a direct field in case table
							if ($field != "lawyer_id" && isset ( $case_values [$field] )) {
								$trans_values [$trans_itr] [$field] = $case_values [$field];
							}
						}
						// in case of changes in state then should make multiple records one for each state
						// with there changed fields
						// checking the state order is correct
						if ($caseRecord ['current_state_code'] != $case_values ["current_state_code"]) {
							self::logToFile ( "Doing action " . $caseRecord ['current_state_code'] . '-->' . $case_values ["current_state_code"] );
							$last_state = $caseRecord ['current_state_code'];
							$current_state = $case_values ['current_state_code'];
							
							if ($caseRecord ['current_state_code'] == "SUSPENDED" || $caseRecord ['current_state_code'] == "CLOSED") {
								$this->db->select ( '*' );
								$this->db->from ( 'case_transactions' );
								$this->db->order_by ( 'trans_date_time', 'desc', 'transaction_id', 'desc' );
								$this->db->where ( "case_id = '" . $case_values ["case_id"] . "' and lawyer_office_id = $lawyer_office_id and current_state_code ='" . $caseRecord ["current_state_code"] . "'" );
								$this->db->limit ( 1 );
								$query = $this->db->get ();
								$caseTrnsRecord = null;
								if ($query->num_rows () > 0) {
									$records = $query->result_array ();
									$caseTrnsRecord = $records [0];
									$last_state = $caseTrnsRecord ["last_state_code"];
								}
							} else if ($caseRecord ["current_state_code"] == "MODIFICATION_REQUIRED") {
								$last_state = "UNDER_REVIEW";
							}
							
							if ($case_values ['current_state_code'] == "SUSPENDED" || $case_values ['current_state_code'] == "CLOSED") {
								$current_state = self::updateCloseSuspendPrevStates ( $all_states, $trans_values, $trans_itr, $case_values, $caseRecord, null, $lawyer_office_id, false );
							} else if ($case_values ["current_state_code"] == "MODIFICATION_REQUIRED") {
								$current_state = "UNDER_REVIEW";
							}
							self::logToFile ( "trans status 1--->  " . $this->db->trans_status () );
							$incorrect = self::checkStateArrangment ( $current_state, $last_state, $all_states );
							self::logToFile ( "trans status 2--->  " . $this->db->trans_status () );
							if ($incorrect) {
								$failed_record_numbers [$failed_count] = $i - 1;
								self::logToFile ( "failed: $incorrect. checkStateArrangment $current_state" );
								$failed_count ++;
								continue;
							}
							$updated_cases = $case_values;
							
							unset ( $updated_cases ["case_id"] );
							self::logToFile ( "update case: " . $case_values ["case_id"] );
							self::logToFile ( "trans status 3--->  " . $this->db->trans_status () );
							$this->common->updateRecord ( 'case', $updated_cases, $where );
							self::logToFile ( "Done update--->  " . $case_values ['case_id'] );
							self::logToFile ( "trans status 4--->  " . $this->db->trans_status () );
							$updated_count = $updated_count + 1;
							self::logToFile ( "trans status 5--->  " . $this->db->trans_status () );
							self::checkAndReflectCaseState ( $all_states, $trans_values, $trans_itr, $case_values, $caseRecord, $allDataInSheet [$i] ["V"], $lawyer_office_id );
						} else if (count ( $changed_fields ) > 0) {
							self::logToFile ( "trans status 21--->  " . $this->db->trans_status () );
							$updated_cases = $case_values;
							unset ( $updated_cases ["case_id"] );
							self::logToFile ( "update case: " . $case_values ["case_id"] );
							$this->common->updateRecord ( 'case', $updated_cases, $where );
							self::logToFile ( "Done update--->  " . $case_values ['case_id'] );
							$updated_count = $updated_count + 1;
							$trans_values [$trans_itr] ["last_update_date"] = $case_values ["last_update_date"];
							$trans_values [$trans_itr] ["transaction_status"] = "Success";
							$trans_values [$trans_itr] ["trans_date_time"] = date ( "Y-m-d H:i:s" );
							$trans_values [$trans_itr] ["manual_insert"] = "N";
							if ($changed_fields [0] == 'lawyer_id')
								$trans_values [$trans_itr] ["edit_action"] = "N";
							else
								$trans_values [$trans_itr] ["edit_action"] = "Y";
							$trans_inserted = $this->common->insertRecord ( 'case_transactions', $trans_values [$trans_itr] );
							self::logToFile ( "insert transaction " . $trans_inserted );
							$trans_itr = $trans_itr + 1;
						}
						self::logToFile ( "trans status 20--->  " . $this->db->trans_status () );
						
						if ($this->db->trans_status () === FALSE && ! $incorrect) {
							$this->db->trans_rollback ();
							$failed_record_numbers [$failed_count] = $i - 1;
							$failed_count ++;
							self::logToFile ( "failed: $incorrect. 334" );
							$updated_count = $updated_count - 1;
							continue;
						}
						$this->db->trans_complete ();
					} else {
						self::logToFile ( "trans status 22--->  " . $this->db->trans_status () );
						if (! isset ( $allDataInSheet [$i] ["V"] ) && ! $incorrect) {
							if (array_key_exists ( $case_values ["current_state_code"], $all_states )) {
								$incorrect = $all_states [$case_values ["current_state_code"]] >= $all_states ["ASSIGNED_TO_LAWYER"];
								self::logToFile ( "$incorrect 341 " . $case_values ["current_state_code"] . ' ' . $all_states [$case_values ["current_state_code"]] . ' ' . $all_states ["ASSIGNED_TO_LAWYER"] );
							} else if ($case_values ["current_state_code"] == "SUSPENDED" || $case_values ["current_state_code"] == "CLOSED") {
								$last_state = self::updateCloseSuspendPrevStates ( $all_states, $trans_values, $trans_itr, $case_values, $caseRecord, null, $lawyer_office_id, false );
								$incorrect = $last_state != "MODIFICATION_REQUIRED" && $all_states [$last_state] >= $all_states ["ASSIGNED_TO_LAWYER"];
								self::logToFile ( "$incorrect 345 " . $last_state . ' ' . $all_states [$last_state] . ' ' . $all_states ["ASSIGNED_TO_LAWYER"] );
							}
						}
						if ($incorrect) {
							$failed_record_numbers [$failed_count] = $i - 1;
							self::logToFile ( "failed: $incorrect 350" );
							$failed_count ++;
							continue;
						}
						$inserted_case = $case_values;
						$inserted_case ["current_state_code"] = "UNDER_REVIEW";
						self::logToFile ( "inserted case  " . implode ( " , ", array_keys ( $inserted_case ) ) . " -- " . implode ( " , ", $inserted_case ) );
						$insertes = $this->common->insertRecord ( 'case', $inserted_case );
						self::logToFile ( "Done insert--->  $insertes  -  " . $inserted_case ['case_id'] . " - " . $this->db->trans_status () );
						// if (isset ( $insertes ) && $insertes > 0) {
						$inserted_count = $inserted_count + 1;
						if (isset ( $allDataInSheet [$i] ["V"] )/*lawyer_id*/) {
							$inserted_lawyer_case ['lawyer_id'] = $allDataInSheet [$i] ["V"];
							$inserted_lawyer_case ['case_id'] = $case_values ["case_id"];
							$inserted_lawyer_case ['lawyer_office_id'] = $lawyer_office_id;
							$insrtd_lawyer_case = $this->common->insertRecord ( 'lawyer_case', $inserted_lawyer_case );
							self::logToFile ( "insert lawyer case - $lawyer_office_id - " . $case_values ["case_id"] . " - " . $allDataInSheet [$i] ["V"] );
							if (! isset ( $insrtd_lawyer_case ) || $insrtd_lawyer_case <= 0) {
								$incorrect = true;
								self::logToFile ( "problem in insert lawyer case " );
							}
							$trans_values [$trans_itr] = $case_values;
							$trans_values [$trans_itr] ["lawyer_id"] = $allDataInSheet [$i] ["V"];
							$trans_values [$trans_itr] ["current_state_code"] = "UNDER_REVIEW";
							$trans_values [$trans_itr] ["region_id"] = null;
							$trans_values [$trans_itr] ["executive_order_number"] = null;
							$trans_values [$trans_itr] ["order_number"] = null;
							$trans_values [$trans_itr] ["circle_number"] = null;
							$trans_values [$trans_itr] ["executive_order_date"] = null;
							$trans_values [$trans_itr] ["referral_paper"] = null;
							$trans_values [$trans_itr] ["decision_34_number"] = null;
							$trans_values [$trans_itr] ["decision_34_date"] = null;
							$trans_values [$trans_itr] ["decision_46_date"] = null;
							$trans_values [$trans_itr] ["advertisement_date"] = null;
							$trans_values [$trans_itr] ["advertisement_file"] = null;
							$trans_values [$trans_itr] ["invoice_file"] = null;
						} else {
							$trans_values [$trans_itr] = $case_values;
							$trans_values [$trans_itr] ["current_state_code"] = "UNDER_REVIEW";
							$trans_values [$trans_itr] ["region_id"] = null;
							$trans_values [$trans_itr] ["executive_order_number"] = null;
							$trans_values [$trans_itr] ["order_number"] = null;
							$trans_values [$trans_itr] ["circle_number"] = null;
							$trans_values [$trans_itr] ["executive_order_date"] = null;
							$trans_values [$trans_itr] ["referral_paper"] = "";
							$trans_values [$trans_itr] ["decision_34_number"] = null;
							$trans_values [$trans_itr] ["decision_34_date"] = null;
							$trans_values [$trans_itr] ["decision_46_date"] = null;
							$trans_values [$trans_itr] ["advertisement_date"] = null;
							$trans_values [$trans_itr] ["advertisement_file"] = null;
							$trans_values [$trans_itr] ["invoice_file"] = null;
						}
						
						// insert first record in case trans with state under_review
						$trans_values [$trans_itr] ["last_update_date"] = $case_values ["last_update_date"];
						$trans_values [$trans_itr] ["transaction_status"] = "Success";
						$trans_values [$trans_itr] ["trans_date_time"] = date ( "Y-m-d H:i:s" );
						$trans_values [$trans_itr] ["manual_insert"] = "N";
						$trans_values [$trans_itr] ["edit_action"] = "N";
						$trans_inserted = $this->common->insertRecord ( 'case_transactions', $trans_values [$trans_itr] );
						self::logToFile ( "insert transaction $trans_inserted " . implode ( " , ", array_keys ( $trans_values [$trans_itr] ) ) . " -- " . implode ( " , ", $trans_values [$trans_itr] ) . "  " . $this->db->trans_status () );
						$trans_itr = $trans_itr + 1;
						
						// add update record for the created case with its current state
						$where = "case_id = '" . $case_values ["case_id"] . "' and lawyer_office_id = " . $lawyer_office_id;
						$updated_cases = $case_values;
						unset ( $updated_cases ["case_id"] );
						self::logToFile ( "update case: " . $case_values ["case_id"] );
						$updates = $this->common->updateRecord ( 'case', $updated_cases, $where );
						self::logToFile ( "Done update--->  $updates - " . $case_values ['case_id'] );
						// to record new record creation in case transactions
						self::logToFile ( "trans status 113--->  " . $this->db->trans_status () );
						self::checkAndReflectCaseState ( $all_states, $trans_values, $trans_itr, $case_values, $caseRecord, $allDataInSheet [$i] ["V"], $lawyer_office_id );
						
						if ($this->db->trans_status () === FALSE || $incorrect) {
							self::logToFile ( "trans status 114--->  " . $this->db->trans_status () );
							$this->db->trans_rollback ();
							$failed_record_numbers [$failed_count] = $i - 1;
							$failed_count ++;
							self::logToFile ( "failed: $incorrect. trans_rollback" );
							$inserted_count = $inserted_count - 1;
						}
						$this->db->trans_complete ();
						// } else {
						// $failed_record_numbers [$failed_count] = $i - 1;
						// $failed_count ++;
						// }
					}
					self::logToFile ( "trans status 23--->  " . $this->db->trans_status () );
					
					// }
					// updating on current state
				} catch ( Exception $e ) {
					self::logToFile ( "failed: exception $e" );
					if (! $incorrect) {
						$failed_record_numbers [$failed_count] = $i - 1;
						$failed_count ++;
						self::logToFile ( "failed: correct exception $e" );
					}
				}
			}
			
			// $confirmation_message = "inseted (" . $inserted_count . ") updates (" . $updated_count . ") failed (" . $failed_count . ")";
			$confirmation_message = "�� ����� ($inserted_count) ���� �����  , � ����� ($updated_count) ���� , � �� ����� ($failed_count) ";
			if (count ( $failed_record_numbers ) > 0) {
				$failed_num_str = implode ( ", ", $failed_record_numbers );
				// $confirmation_message = $confirmation_message . " record number " . $failed_num_str;
				$confirmation_message = $confirmation_message . ", � �� ������� ������� �� ������� ������: $failed_num_str";
			}
			self::logToFile ( $confirmation_message );
			self::logToFile ( "--------------------------------------------" );
			$this->session->set_flashdata ( 'confirmation_message', $confirmation_message );
			
		
	}
	function checkNewFieldsValues(&$all_states, $current_values, $previous_values, $current_lawyer_id, $previous_lawyer_id) {
		$current_state_code = $previous_values ["current_state_code"];
		$changed_fields = array ();
		$changed_itr = 0;
		
		if ($current_values ["customer_id"] != $previous_values ["customer_id"]) {
			$changed_fields [$changed_itr] = "customer_id";
			$changed_itr = $changed_itr + 1;
		}
		if ($current_values ["contract_number"] != $previous_values ["contract_number"]) {
			$changed_fields [$changed_itr] = "contract_number";
			$changed_itr = $changed_itr + 1;
		}
		if ($current_values ["debtor_name"] != $previous_values ["debtor_name"]) {
			$changed_fields [$changed_itr] = "debtor_name";
			$changed_itr = $changed_itr + 1;
		}
		if ($current_values ["client_name"] != $previous_values ["client_name"]) {
			$changed_fields [$changed_itr] = "client_name";
			$changed_itr = $changed_itr + 1;
		}
		if ($current_values ["client_id_number"] != $previous_values ["client_id_number"]) {
			$changed_fields [$changed_itr] = "client_id_number";
			$changed_itr = $changed_itr + 1;
		}
		if ($current_values ["client_type"] != $previous_values ["client_type"]) {
			$changed_fields [$changed_itr] = "client_type";
			$changed_itr = $changed_itr + 1;
		}
		if ($current_values ["obligation_value"] != $previous_values ["obligation_value"]) {
			$changed_fields [$changed_itr] = "obligation_value";
			$changed_itr = $changed_itr + 1;
		}
		if ($current_values ["number_of_late_instalments"] != $previous_values ["number_of_late_instalments"]) {
			$changed_fields [$changed_itr] = "number_of_late_instalments";
			$changed_itr = $changed_itr + 1;
		}
		if ($current_values ["due_amount"] != $previous_values ["due_amount"]) {
			$changed_fields [$changed_itr] = "due_amount";
			$changed_itr = $changed_itr + 1;
		}
		
		if ($current_values ["remaining_amount"] != $previous_values ["remaining_amount"]) {
			$changed_fields [$changed_itr] = "remaining_amount";
			$changed_itr = $changed_itr + 1;
		}
		if ($current_values ["total_debenture_amount"] != $previous_values ["total_debenture_amount"]) {
			$changed_fields [$changed_itr] = "total_debenture_amount";
			$changed_itr = $changed_itr + 1;
		}
		if ($current_values ["creation_date"] != $previous_values ["creation_date"]) {
			$changed_fields [$changed_itr] = "creation_date";
			$changed_itr = $changed_itr + 1;
		}
		// todooooo if lawyer id changed may need to be >= assign to lawyer
		// todooooo lawyer id must be in all transactions
		// if (isset ( $all_states [$current_state_code] ) && $all_states [$current_state_code] >= $all_states ["ASSIGNED_TO_LAWYER"]) {
		if ($current_lawyer_id != $previous_lawyer_id) {
			$changed_fields [$changed_itr] = "lawyer_id";
			$changed_itr = $changed_itr + 1;
		}
		if ($current_values ["consignment_image"] != $previous_values ["consignment_image"]) {
			$changed_fields [$changed_itr] = "consignment_image";
			$changed_itr = $changed_itr + 1;
		}
		if ($current_values ["region_id"] != $previous_values ["region_id"]) {
			$changed_fields [$changed_itr] = "region_id";
			$changed_itr = $changed_itr + 1;
		}
		// }
		// if (isset ( $all_states [$current_state_code] ) && $all_states [$current_state_code] >= $all_states ["REGISTERED"]) {
		self::logToFile ( "executive_order_number" . $current_values ["executive_order_number"] . ' ' . $previous_values ["executive_order_number"] );
		if ($current_values ["executive_order_number"] != $previous_values ["executive_order_number"]) {
			$changed_fields [$changed_itr] = "executive_order_number";
			self::logToFile ( "executive_order_number changed" );
			$changed_itr = $changed_itr + 1;
		}
		if ($current_values ["order_number"] != $previous_values ["order_number"]) {
			$changed_fields [$changed_itr] = "order_number";
			$changed_itr = $changed_itr + 1;
		}
		if ($current_values ["circle_number"] != $previous_values ["circle_number"]) {
			$changed_fields [$changed_itr] = "circle_number";
			$changed_itr = $changed_itr + 1;
		}
		if ($current_values ["executive_order_date"] != $previous_values ["executive_order_date"]) {
			$changed_fields [$changed_itr] = "executive_order_date";
			$changed_itr = $changed_itr + 1;
		}
		if ($current_values ["referral_paper"] != $previous_values ["referral_paper"]) {
			$changed_fields [$changed_itr] = "referral_paper";
			$changed_itr = $changed_itr + 1;
		}
		// }
		// if (isset ( $all_states [$current_state_code] ) && $all_states [$current_state_code] >= $all_states ["DECISION_34"]) {
		if ($current_values ["decision_34_number"] != $previous_values ["decision_34_number"]) {
			$changed_fields [$changed_itr] = "decision_34_number";
			$changed_itr = $changed_itr + 1;
		}
		if ($current_values ["decision_34_date"] != $previous_values ["decision_34_date"]) {
			$changed_fields [$changed_itr] = "decision_34_date";
			$changed_itr = $changed_itr + 1;
		}
		/*
		 * if ($current_values ["decision_34_file"] != $previous_values ["decision_34_file"]) {
		 * $changed_fields [$changed_itr] = "decision_34_file";
		 * $changed_itr = $changed_itr + 1;
		 * }
		 */
		// }
		// if (isset ( $all_states [$current_state_code] ) && $all_states [$current_state_code] >= $all_states ["DECISION_46"]) {
		if ($current_values ["decision_46_date"] != $previous_values ["decision_46_date"]) {
			$changed_fields [$changed_itr] = "decision_46_date";
			$changed_itr = $changed_itr + 1;
		}
		// }
		// if (isset ( $all_states [$current_state_code] ) && $all_states [$current_state_code] >= $all_states ["PUBLICLY_ADVERTISED"]) {
		if ($current_values ["advertisement_date"] != $previous_values ["advertisement_date"]) {
			$changed_fields [$changed_itr] = "advertisement_date";
			$changed_itr = $changed_itr + 1;
		}
		if ($current_values ["advertisement_file"] != $previous_values ["advertisement_file"]) {
			$changed_fields [$changed_itr] = "advertisement_file";
			$changed_itr = $changed_itr + 1;
		}
		if ($current_values ["invoice_file"] != $previous_values ["invoice_file"]) {
			$changed_fields [$changed_itr] = "invoice_file";
			$changed_itr = $changed_itr + 1;
		}
		// }
		if ($current_values ["current_state_code"] != $previous_values ["current_state_code"]) {
			$changed_fields [$changed_itr] = "current_state_code";
			$changed_itr = $changed_itr + 1;
		}
		if ($current_values ["suspend_date"] != $previous_values ["suspend_date"]) {
			$changed_fields [$changed_itr] = "suspend_date";
			$changed_itr = $changed_itr + 1;
		}
		if ($current_values ["suspend_file"] != $previous_values ["suspend_file"]) {
			$changed_fields [$changed_itr] = "suspend_file";
			$changed_itr = $changed_itr + 1;
		}
		if ($current_values ["suspend_reason_code"] != $previous_values ["suspend_reason_code"]) {
			$changed_fields [$changed_itr] = "suspend_reason_code";
			$changed_itr = $changed_itr + 1;
		}
		if ($current_values ["debenture"] != $previous_values ["debenture"]) {
			
			$changed_fields [$changed_itr] = "debenture";
			$changed_itr = $changed_itr + 1;
		}
		if ($current_values ["id"] != $previous_values ["id"]) {
			$changed_fields [$changed_itr] = "id";
			$changed_itr = $changed_itr + 1;
		}
		if ($current_values ["contract"] != $previous_values ["contract"]) {
			$changed_fields [$changed_itr] = "contract";
			$changed_itr = $changed_itr + 1;
		}
		if ($current_values ["others"] != $previous_values ["others"]) {
			$changed_fields [$changed_itr] = "others";
			$changed_itr = $changed_itr + 1;
		}
		if ($current_values ["closing_reason"] != $previous_values ["closing_reason"]) {
			$changed_fields [$changed_itr] = "closing_reason";
			$changed_itr = $changed_itr + 1;
		}
		if ($current_values ["notes"] != $previous_values ["notes"]) {
			$changed_fields [$changed_itr] = "notes";
			$changed_itr = $changed_itr + 1;
		}
		if ($current_values ["creation_user"] != $previous_values ["creation_user"]) {
			$changed_fields [$changed_itr] = "creation_user";
			$changed_itr = $changed_itr + 1;
		}
		if ($current_values ["last_update_user"] != $previous_values ["last_update_user"]) {
			$changed_fields [$changed_itr] = "last_update_user";
			$changed_itr = $changed_itr + 1;
		}
		if ($current_values ["last_update_date"] != $previous_values ["last_update_date"]) {
			$changed_fields [$changed_itr] = "last_update_date";
		}
		self::logToFile ( "changed feilds : " . implode ( " ", $changed_fields ) );
		return $changed_fields;
	}
	
	/*
	 * function checkNewFieldsValues(&$all_states, $current_values, $previous_values, $current_lawyer_id, $previous_lawyer_id) {
	 * $current_state_code = $previous_values ["current_state_code"];
	 * $changed_fields = array ();
	 * $changed_itr = 0;
	 *
	 * if ($current_values ["customer_id"] != $previous_values ["customer_id"]) {
	 * $changed_fields [$changed_itr] = "customer_id";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * if ($current_values ["contract_number"] != $previous_values ["contract_number"]) {
	 * $changed_fields [$changed_itr] = "contract_number";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * if ($current_values ["debtor_name"] != $previous_values ["debtor_name"]) {
	 * $changed_fields [$changed_itr] = "debtor_name";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * if ($current_values ["client_name"] != $previous_values ["client_name"]) {
	 * $changed_fields [$changed_itr] = "client_name";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * if ($current_values ["client_id_number"] != $previous_values ["client_id_number"]) {
	 * $changed_fields [$changed_itr] = "client_id_number";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * if ($current_values ["client_type"] != $previous_values ["client_type"]) {
	 * $changed_fields [$changed_itr] = "client_type";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * if ($current_values ["obligation_value"] != $previous_values ["obligation_value"]) {
	 * $changed_fields [$changed_itr] = "obligation_value";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * if ($current_values ["number_of_late_instalments"] != $previous_values ["number_of_late_instalments"]) {
	 * $changed_fields [$changed_itr] = "number_of_late_instalments";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * if ($current_values ["due_amount"] != $previous_values ["due_amount"]) {
	 * $changed_fields [$changed_itr] = "due_amount";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 *
	 * if ($current_values ["remaining_amount"] != $previous_values ["remaining_amount"]) {
	 * $changed_fields [$changed_itr] = "remaining_amount";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * if ($current_values ["total_debenture_amount"] != $previous_values ["total_debenture_amount"]) {
	 * $changed_fields [$changed_itr] = "total_debenture_amount";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * if ($current_values ["creation_date"] != $previous_values ["creation_date"]) {
	 * $changed_fields [$changed_itr] = "creation_date";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * // todooooo if lawyer id changed may need to be >= assign to lawyer
	 * // todooooo lawyer id must be in all transactions
	 * if (isset ( $all_states [$current_state_code] ) && $all_states [$current_state_code] >= $all_states ["ASSIGNED_TO_LAWYER"]) {
	 * if ($current_lawyer_id != $previous_lawyer_id) {
	 * $changed_fields [$changed_itr] = "lawyer_id";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * if ($current_values ["consignment_image"] != $previous_values ["consignment_image"]) {
	 * $changed_fields [$changed_itr] = "consignment_image";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * if ($current_values ["region_id"] != $previous_values ["region_id"]) {
	 * $changed_fields [$changed_itr] = "region_id";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * }
	 * if (isset ( $all_states [$current_state_code] ) && $all_states [$current_state_code] >= $all_states ["REGISTERED"]) {
	 * self::logToFile("executive_order_number". $current_values ["executive_order_number"]. ' ' .$previous_values ["executive_order_number"]);
	 * if ($current_values ["executive_order_number"] != $previous_values ["executive_order_number"]) {
	 * $changed_fields [$changed_itr] = "executive_order_number";
	 * self::logToFile("executive_order_number changed" );
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * if ($current_values ["order_number"] != $previous_values ["order_number"]) {
	 * $changed_fields [$changed_itr] = "order_number";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * if ($current_values ["circle_number"] != $previous_values ["circle_number"]) {
	 * $changed_fields [$changed_itr] = "circle_number";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * if ($current_values ["executive_order_date"] != $previous_values ["executive_order_date"]) {
	 * $changed_fields [$changed_itr] = "executive_order_date";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * if ($current_values ["referral_paper"] != $previous_values ["referral_paper"]) {
	 * $changed_fields [$changed_itr] = "referral_paper";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * }
	 * if (isset ( $all_states [$current_state_code] ) && $all_states [$current_state_code] >= $all_states ["DECISION_34"]) {
	 * if ($current_values ["decision_34_number"] != $previous_values ["decision_34_number"]) {
	 * $changed_fields [$changed_itr] = "decision_34_number";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * if ($current_values ["decision_34_date"] != $previous_values ["decision_34_date"]) {
	 * $changed_fields [$changed_itr] = "decision_34_date";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * /* if ($current_values ["decision_34_file"] != $previous_values ["decision_34_file"]) {
	 * $changed_fields [$changed_itr] = "decision_34_file";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * }
	 * if (isset ( $all_states [$current_state_code] ) && $all_states [$current_state_code] >= $all_states ["DECISION_46"]) {
	 * if ($current_values ["decision_46_date"] != $previous_values ["decision_46_date"]) {
	 * $changed_fields [$changed_itr] = "decision_46_date";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * }
	 * if (isset ( $all_states [$current_state_code] ) && $all_states [$current_state_code] >= $all_states ["PUBLICLY_ADVERTISED"]) {
	 * if ($current_values ["advertisement_date"] != $previous_values ["advertisement_date"]) {
	 * $changed_fields [$changed_itr] = "advertisement_date";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * if ($current_values ["advertisement_file"] != $previous_values ["advertisement_file"]) {
	 * $changed_fields [$changed_itr] = "advertisement_file";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * if ($current_values ["invoice_file"] != $previous_values ["invoice_file"]) {
	 * $changed_fields [$changed_itr] = "invoice_file";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * }
	 * if ($current_values ["current_state_code"] != $previous_values ["current_state_code"]) {
	 * $changed_fields [$changed_itr] = "current_state_code";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * if ($current_values ["suspend_date"] != $previous_values ["suspend_date"]) {
	 * $changed_fields [$changed_itr] = "suspend_date";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * if ($current_values ["suspend_file"] != $previous_values ["suspend_file"]) {
	 * $changed_fields [$changed_itr] = "suspend_file";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * if ($current_values ["suspend_reason_code"] != $previous_values ["suspend_reason_code"]) {
	 * $changed_fields [$changed_itr] = "suspend_reason_code";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * if ($current_values ["debenture"] != $previous_values ["debenture"]) {
	 *
	 * $changed_fields [$changed_itr] = "debenture";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * if ($current_values ["id"] != $previous_values ["id"]) {
	 * $changed_fields [$changed_itr] = "id";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * if ($current_values ["contract"] != $previous_values ["contract"]) {
	 * $changed_fields [$changed_itr] = "contract";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * if ($current_values ["others"] != $previous_values ["others"]) {
	 * $changed_fields [$changed_itr] = "others";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * if ($current_values ["closing_reason"] != $previous_values ["closing_reason"]) {
	 * $changed_fields [$changed_itr] = "closing_reason";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * if ($current_values ["notes"] != $previous_values ["notes"]) {
	 * $changed_fields [$changed_itr] = "notes";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * if ($current_values ["creation_user"] != $previous_values ["creation_user"]) {
	 * $changed_fields [$changed_itr] = "creation_user";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * if ($current_values ["last_update_user"] != $previous_values ["last_update_user"]) {
	 * $changed_fields [$changed_itr] = "last_update_user";
	 * $changed_itr = $changed_itr + 1;
	 * }
	 * if ($current_values ["last_update_date"] != $previous_values ["last_update_date"]) {
	 * $changed_fields [$changed_itr] = "last_update_date";
	 * }
	 * self::logToFile("changed feilds : ". implode(" ",$changed_fields));
	 * return $changed_fields;
	 *
	 * }
	 */
	
	// in case of changes in state then should make multiple records one for each state
	// with there changed fields
	function checkAndReflectCaseState(&$all_states, &$trans_values, &$trans_itr, $current_values, $previous_values, $lawyer_id, $lawyer_office_id) {
		self::logToFile ( "In checkAndReflectCaseState " . $previous_values ["current_state_code"] . " - " . $current_values ["current_state_code"] );
		// for all current dates set
		date_default_timezone_set ( 'Asia/Riyadh' );
		// self::logToFile("trans status 3ss5---> ".$this->db->trans_status ()."-- ".implode(" , ", array_keys($trans_values [$trans_itr]))." -- ".implode (" , ",$trans_values [$trans_itr])." ".$this->db->trans_status ());
		if (! isset ( $previous_values )) { // bec new record should start with under review
		                                    // no need since we now insert first record by state = under_review so will not happen
			$previous_values ["current_state_code"] = "UNDER_REVIEW";
		}
		
		// in case of not paused states like MODIFICATION_REQUIRED, SUSPENDED and CLOSED
		// apply all states actions from current state to the entered state
		if ($current_values ["current_state_code"] != "MODIFICATION_REQUIRED" && $current_values ["current_state_code"] != "SUSPENDED" && $current_values ["current_state_code"] != "CLOSED" && $previous_values ["current_state_code"] != "MODIFICATION_REQUIRED" && $previous_values ["current_state_code"] != "SUSPENDED" && $previous_values ["current_state_code"] != "CLOSED") {
			self::logToFile ( "----------> " . $current_values ["current_state_code"] . "  - " . $previous_values ["current_state_code"] );
			// apply all states from prev to current
			$current_state_indx = $all_states [$current_values ["current_state_code"]];
			$previous_state_indx = $all_states [$previous_values ["current_state_code"]];
			
			for($i = $previous_state_indx + 1; $i <= $current_state_indx; $i ++) {
				$trans_values [$trans_itr] ['case_id'] = $current_values ["case_id"];
				$trans_values [$trans_itr] ['lawyer_office_id'] = $lawyer_office_id;
				$trans_values [$trans_itr] ['lawyer_id'] = $lawyer_id;
				self::logToFile ( "trans status 6--->  " . $this->db->trans_status () );
				switch ($i) {
					case $all_states ["ASSIGNED_TO_LAWYER"] :
						$trans_values [$trans_itr] ['current_state_code'] = "ASSIGNED_TO_LAWYER";
						$trans_values [$trans_itr] ['region_id'] = $current_values ["region_id"];
						$trans_values [$trans_itr] ['action_code'] = "ASSIGN_TO_LAWYER";
						break;
					case $all_states ["REGISTERED"] :
						$trans_values [$trans_itr] ['current_state_code'] = "REGISTERED";
						$trans_values [$trans_itr] ['executive_order_number'] = $current_values ["executive_order_number"];
						$trans_values [$trans_itr] ['executive_order_date'] = $current_values ["executive_order_date"];
						$trans_values [$trans_itr] ['order_number'] = $current_values ["order_number"];
						$trans_values [$trans_itr] ['circle_number'] = $current_values ["circle_number"];
						$trans_values [$trans_itr] ['referral_paper'] = $current_values ["referral_paper"];
						$trans_values [$trans_itr] ["action_code"] = "REGISTER";
						break;
					case $all_states ["DECISION_34"] :
						$trans_values [$trans_itr] ['current_state_code'] = "DECISION_34";
						$trans_values [$trans_itr] ['decision_34_number'] = $current_values ["decision_34_number"];
						$trans_values [$trans_itr] ['decision_34_date'] = $current_values ["decision_34_date"];
						$trans_values [$trans_itr] ["action_code"] = "MAKE_DECISION_34";
						break;
					case $all_states ["PUBLICLY_ADVERTISED"] :
						$trans_values [$trans_itr] ['current_state_code'] = "PUBLICLY_ADVERTISED";
						$trans_values [$trans_itr] ['advertisement_date'] = $current_values ["advertisement_date"];
						$trans_values [$trans_itr] ['advertisement_file'] = $current_values ["advertisement_file"];
						$trans_values [$trans_itr] ['invoice_file'] = $current_values ["invoice_file"];
						$trans_values [$trans_itr] ["action_code"] = "ADVERTISE";
						break;
					case $all_states ["DECISION_46"] :
						$trans_values [$trans_itr] ['current_state_code'] = "DECISION_46";
						$trans_values [$trans_itr] ['decision_46_date'] = $current_values ["decision_46_date"];
						$trans_values [$trans_itr] ["action_code"] = "MAKE_DECISION_46";
						break;
				}
				$trans_values [$trans_itr] ["last_update_date"] = $current_values ["last_update_date"];
				$trans_values [$trans_itr] ["transaction_status"] = "Success";
				$trans_values [$trans_itr] ["trans_date_time"] = date ( "Y-m-d H:i:s" );
				$trans_values [$trans_itr] ["manual_insert"] = "N"; // any new records will always be N
				$trans_values [$trans_itr] ["edit_action"] = "N"; //
				                                                  // create invoice for only customer
				$old_case_values = $this->case_model->getCaseDataForTransaction ( $current_values ["case_id"], $lawyer_office_id );
				self::logToFile ( "trans status 7--->  " . $this->db->trans_status () );
				$this->case_model->create_invoice ( $trans_values [$trans_itr], $old_case_values, $current_values ["region_id"], 1 );
				// insert new case trns
				self::logToFile ( "insert case transaction 904: " . $trans_values [$trans_itr] ['case_id'] . "-" . $trans_values [$trans_itr] ['lawyer_id'] . "-" . $trans_values [$trans_itr] ['action_code'] );
				$trns_inserted = $this->common->insertRecord ( 'case_transactions', $trans_values [$trans_itr] );
				self::logToFile ( "insert transaction $trns_inserted" );
				self::logToFile ( "trans status 8--->  " . $this->db->trans_status () );
				$trans_itr = $trans_itr + 1;
			}
		} else if ($current_values ["current_state_code"] == "MODIFICATION_REQUIRED" && $previous_values ["current_state_code"] != "MODIFICATION_REQUIRED") {
			// the case before this sure was under review
			
			$trans_values [$trans_itr] ['notes'] = $current_values ["notes"];
			$trans_values [$trans_itr] ['current_state_code'] = $current_values ["current_state_code"];
			$trans_values [$trans_itr] ['case_id'] = $current_values ["case_id"];
			$trans_values [$trans_itr] ['lawyer_office_id'] = $lawyer_office_id;
			$trans_values [$trans_itr] ["last_update_date"] = $current_values ["last_update_date"];
			$trans_values [$trans_itr] ["transaction_status"] = "Success";
			$trans_values [$trans_itr] ["trans_date_time"] = date ( "Y-m-d H:i:s" );
			$trans_values [$trans_itr] ["manual_insert"] = "N";
			$trans_values [$trans_itr] ["edit_action"] = "N";
			$trans_values [$trans_itr] ["last_state_code"] = $previous_values ["current_state_code"];
			$trans_values [$trans_itr] ["action_code"] = "NEED_MODIFICATIONS";
			$trns_inserted = $this->common->insertRecord ( 'case_transactions', $trans_values [$trans_itr] );
			self::logToFile ( "insert transaction $trns_inserted" );
			self::logToFile ( "trans status 9--->  " . $this->db->trans_status () );
			$trans_itr = $trans_itr + 1;
		} else if ($current_values ["current_state_code"] == "SUSPENDED" && $previous_values ["current_state_code"] != "SUSPENDED") {
			self::logToFile ( "suspended" );
			// check for prev states
			self::logToFile ( "trans status 10--->  " . $this->db->trans_status () );
			$previousState = self::updateCloseSuspendPrevStates ( $all_states, $trans_values, $trans_itr, $current_values, $previous_values, $lawyer_id, $lawyer_office_id, true );
			self::logToFile ( "trans status 11--->  " . $this->db->trans_status () );
			
			$trans_values [$trans_itr] ['suspend_reason_code'] = $current_values ["suspend_reason_code"];
			$trans_values [$trans_itr] ['current_state_code'] = $current_values ["current_state_code"];
			$trans_values [$trans_itr] ['case_id'] = $current_values ["case_id"];
			$trans_values [$trans_itr] ['lawyer_office_id'] = $lawyer_office_id;
			$trans_values [$trans_itr] ["last_state_code"] = $previousState;
			$trans_values [$trans_itr] ["transaction_status"] = "Success";
			$trans_values [$trans_itr] ["trans_date_time"] = date ( "Y-m-d H:i:s" );
			// if(isset($current_values ["suspend_date"]))
			// $trans_values [$trans_itr] ["suspend_date"] = $current_values ["suspend_date"];
			/*
			 * else {
			 * date_default_timezone_set ( 'Asia/Riyadh' );
			 * $current_date_time = date ( "Y-m-d H:i:s" );
			 * $dt = new DateTime ( $current_date_time );
			 * $current_date = $dt->format ( 'Y-m-d' );
			 * $trans_values [$trans_itr] ["suspend_date"] = $current_date;
			 * }
			 */
			// $trans_values [$trans_itr] ["suspend_file"] = $current_values ["suspend_file"];
			$trans_values [$trans_itr] ["last_update_date"] = $current_values ["last_update_date"];
			$trans_values [$trans_itr] ["manual_insert"] = "N";
			$trans_values [$trans_itr] ["edit_action"] = "N";
			$trans_values [$trans_itr] ["action_code"] = "SUSPEND";
// 			self::logToFile ( "trans status 12--->  " . $this->db->trans_status () );
// 			self::logToFile ( "insert case transaction 941: " . $trans_values [$trans_itr] ['case_id'] . "-" . $trans_values [$trans_itr] ['lawyer_id'] . "-" . $trans_values [$trans_itr] ['action_code'] );
			$trns_inserted = $this->common->insertRecord ( 'case_transactions', $trans_values [$trans_itr] );
			self::logToFile ( "insert transaction " . $trns_inserted );
			self::logToFile ( "trans status 13---> $trns_inserted  " . implode ( " , ", array_keys ( $trans_values [$trans_itr] ) ) . " -- " . implode ( " , ", $trans_values [$trans_itr] ) . "  " . $this->db->trans_status () );
			$trans_itr = $trans_itr + 1;
		} else if ($current_values ["current_state_code"] == "CLOSED" && $previous_values ["current_state_code"] != "CLOSED") {
			self::logToFile ( "Closed" );
			
			// check for prev states
			$previousState = self::updateCloseSuspendPrevStates ( $all_states, $trans_values, $trans_itr, $current_values, $previous_values, $lawyer_id, $lawyer_office_id, $lawyer_office_id, $lawyer_office_id, true );
			
			$trans_values [$trans_itr] ['closing_reason'] = $current_values ["closing_reason"];
			$trans_values [$trans_itr] ['current_state_code'] = $current_values ["current_state_code"];
			$trans_values [$trans_itr] ['case_id'] = $current_values ["case_id"];
			$trans_values [$trans_itr] ['lawyer_office_id'] = $lawyer_office_id;
			$trans_values [$trans_itr] ["last_state_code"] = $previousState;
			$trans_values [$trans_itr] ["last_update_date"] = $current_values ["last_update_date"];
			$trans_values [$trans_itr] ["transaction_status"] = "Success";
			$trans_values [$trans_itr] ["trans_date_time"] = date ( "Y-m-d H:i:s" );
			$trans_values [$trans_itr] ["manual_insert"] = "N";
			$trans_values [$trans_itr] ["edit_action"] = "N";
			$trans_values [$trans_itr] ["action_code"] = "CLOSE";
			
			$trns_inserted = $this->common->insertRecord ( 'case_transactions', $trans_values [$trans_itr] );
			self::logToFile ( "insert transaction " . $trns_inserted );
			$trans_itr = $trans_itr + 1;
		} else if ($previous_values ["current_state_code"] == "MODIFICATION_REQUIRED" && array_key_exists ( $current_values ["current_state_code"], $all_states )) {
			$trans_values [$trans_itr] ["last_update_date"] = $current_values ["last_update_date"];
			$trans_values [$trans_itr] ["transaction_status"] = "Success";
			$trans_values [$trans_itr] ["trans_date_time"] = date ( "Y-m-d H:i:s" );
			$trans_values [$trans_itr] ["manual_insert"] = "N";
			$trans_values [$trans_itr] ["edit_action"] = "N";
			// do the modification done action
			$trans_values [$trans_itr] ['current_state_code'] = "UNDER_REVIEW";
			$trans_values [$trans_itr] ["action_code"] = "MODIFICATIONS_DONE";
			$trans_values [$trans_itr] ['case_id'] = $current_values ["case_id"];
			$trans_values [$trans_itr] ['lawyer_office_id'] = $lawyer_office_id;
			self::logToFile ( "insert case transaction 975: " . "-- " . implode ( " , ", array_keys ( $trans_values [$trans_itr] ) ) . " -- " . implode ( " , ", $trans_values [$trans_itr] ) . "  " . $this->db->trans_status () );
			$trns_inserted = $this->common->insertRecord ( 'case_transactions', $trans_values [$trans_itr] );
			self::logToFile ( "insert case transaction 975: " . "-- " . implode ( " , ", array_keys ( $trans_values [$trans_itr] ) ) . " -- " . implode ( " , ", $trans_values [$trans_itr] ) . "  " . $this->db->trans_status () );
			self::logToFile ( "insert transaction " . $trns_inserted );
			$trans_itr = $trans_itr + 1;
			$modify_req_prev_values = $previous_values;
			$modify_req_prev_values ["current_state_code"] = "UNDER_REVIEW";
			self::checkAndReflectCaseState ( $all_states, $trans_values, $trans_itr, $current_values, $modify_req_prev_values, $lawyer_id, $lawyer_office_id );
		} else {
			// if the saved case with one of paused states like MODIFICATION_REQUIRED, SUSPENDED or CLOSED
			// then get the last transaction and check the last_state_code field in it
			$trans_values [$trans_itr] ['current_state_code'] = $current_values ["current_state_code"];
			$trans_values [$trans_itr] ['case_id'] = $current_values ["case_id"];
			$trans_values [$trans_itr] ['lawyer_office_id'] = $lawyer_office_id;
			
			self::logToFile ( "trans status 35--->  " . $this->db->trans_status () . "-- " . implode ( " , ", array_keys ( $trans_values [$trans_itr] ) ) . " -- " . implode ( " , ", $trans_values [$trans_itr] ) . "  " . $this->db->trans_status () );
			$this->db->select ( '*' );
			$this->db->from ( 'case_transactions' );
			$this->db->order_by ( 'trans_date_time', 'desc', 'transaction_id', 'desc' );
			// todooooo must add case id and lawyer office id in the where condition
			// in function change feilds do not check that value == must be >=
			$this->db->where ( "case_id = '" . $current_values ["case_id"] . "' and lawyer_office_id = $lawyer_office_id and current_state_code ='" . $previous_values ["current_state_code"] . "'" );
			self::logToFile ( "case_id = '" . $current_values ["case_id"] . "' and lawyer_office_id = $lawyer_office_id and current_state_code ='" . $previous_values ["current_state_code"] . "'" );
			$this->db->limit ( 1 );
			$query = $this->db->get ();
			$caseTrnsRecord = null;
			if ($query->num_rows () > 0) {
				$records = $query->result_array ();
				$caseTrnsRecord = $records [0];
				$prev_stat_code = $previous_values ["current_state_code"];
				// apply the state transaction from it to the current state entered except UNDER_REVIEW(in the sheet) and last state code is DECISION_46 (before chnge).
				if (isset ( $caseTrnsRecord ["last_state_code"] ) && $current_values ["current_state_code"] != "UNDER_REVIEW" && $caseTrnsRecord ["last_state_code"] != "DECISION_46") {
					$previous_values ["current_state_code"] = $caseTrnsRecord ["last_state_code"];
					self::checkAndReflectCaseState ( $all_states, $trans_values, $trans_itr, $current_values, $previous_values, $lawyer_id, $lawyer_office_id );
				} else {
					// if($prev_stat_code == "SUSPENDED" || $prev_stat_code == "MODIFICATION_REQUIRED" || $prev_stat_code == "CLOSED"){
					self::logToFile ( "trans status 14--->  " . $this->db->trans_status () );
					// self::logToFile("trans status 25---> ".$this->db->trans_status ()."-- ".implode(" , ", array_keys($trans_values [$trans_itr]))." -- ".implode (" , ",$trans_values [$trans_itr])." ".$this->db->trans_status ());
					
					$trans_values [$trans_itr] ["last_update_date"] = $current_values ["last_update_date"];
					$trans_values [$trans_itr] ["transaction_status"] = "Success";
					$trans_values [$trans_itr] ["trans_date_time"] = date ( "Y-m-d H:i:s" );
					$trans_values [$trans_itr] ["manual_insert"] = "N";
					$trans_values [$trans_itr] ["edit_action"] = "N";
					
					if ($prev_stat_code == "SUSPENDED") {
						$trans_values [$trans_itr] ["action_code"] = "RESUME";
					} else if ($prev_stat_code == "MODIFICATION_REQUIRED") {
						$trans_values [$trans_itr] ["action_code"] = "MODIFICATIONS_DONE";
					} else if ($prev_stat_code == "CLOSED") {
						$trans_values [$trans_itr] ["action_code"] = "REOPEN";
					}
					// self::logToFile("insert case transaction 1019: ".$trans_values [$trans_itr]["case_id"]."-".$trans_values [$trans_itr]["lawyer_id"]."-".$prev_stat_code."-".$trans_values [$trans_itr]["action_code"]);
					$trns_inserted = $this->common->insertRecord ( 'case_transactions', $trans_values [$trans_itr] );
					self::logToFile ( "insert transaction " . $trns_inserted );
					self::logToFile ( "trans status 15--->  " . $this->db->trans_status () . "-- " . implode ( " , ", array_keys ( $trans_values [$trans_itr] ) ) . " -- " . implode ( " , ", $trans_values [$trans_itr] ) . "  " . $this->db->trans_status () );
					$trans_itr = $trans_itr + 1;
				}
			}
		}
		// self::logToFile("trans status 3ssss5---> ".$this->db->trans_status ()."-- ".implode(" , ", array_keys($trans_values [$trans_itr]))." -- ".implode (" , ",$trans_values [$trans_itr])." ".$this->db->trans_status ());
		return false;
	}
	
	// check for prev states
	function updateCloseSuspendPrevStates(&$all_states, &$trans_values, &$trans_itr, $current_values, $previous_values, $lawyer_id, $lawyer_office_id, $apply_sim) {
		try {
			self::logToFile ( "In updateCloseSuspendPrevStates" );
			$current_values_prev_state = $current_values;
			$caseTrnsRecord = null;
			$trns_last_state = null;
			$predicted_state_code = null;
			
			// get last state code if exist
			$this->db->select ( 'current_state_code' );
			$this->db->from ( 'case_transactions' );
			self::logToFile ( "case to search " . $current_values ["case_id"] );
			$this->db->where ( "case_id ='" . $current_values ["case_id"] . "' and lawyer_office_id = " . $lawyer_office_id . " AND edit_action = 'N' " );
			$this->db->order_by ( 'transaction_id', 'desc' );
			$this->db->limit ( 1 );
			$query = $this->db->get ();
			$caseTrnsRecord = null;
			if ($query->num_rows () > 0) {
				$records = $query->result_array ();
				$caseTrnsRecord = $records [0];
				self::logToFile ( "transaction got " . $caseTrnsRecord ['current_state_code'] );
				if ($caseTrnsRecord ["current_state_code"] == "MODIFICATION_REQUIRED") {
					$trns_last_state = "UNDER_REVIEW";
				} else {
					$trns_last_state = $caseTrnsRecord ["current_state_code"];
				}
			}
			
			// get the predict last state code from importing
			if (isset ( $current_values ["decision_46_date"] )) {
				$predicted_state_code = "DECISION_46";
			} else if (isset ( $current_values ["advertisement_date"] )) {
				$predicted_state_code = "PUBLICLY_ADVERTISED";
			} else if (isset ( $current_values ["decision_34_date"] )) {
				$predicted_state_code = "DECISION_34";
			} else if (isset ( $current_values ["executive_order_date"] )) {
				$predicted_state_code = "REGISTERED";
			} else if (isset ( $lawyer_id )) {
				$predicted_state_code = "ASSIGNED_TO_LAWYER";
			} else if (isset ( $current_values ["notes"] )) {
				$predicted_state_code = "MODIFICATION_REQUIRED";
			} else {
				$predicted_state_code = "UNDER_REVIEW";
			}
			self::logToFile ( "predicted_state_code = $predicted_state_code" );
			self::logToFile ( "beforess: " . $current_values_prev_state ["current_state_code"] );
			if ($trns_last_state != 'SUSPENDED' && $trns_last_state != 'CLOSED') {
				if (($predicted_state_code == "MODIFICATION_REQUIRED" && $all_states [$trns_last_state] >= $all_states ["UNDER_REVIEW"]) || (isset ( $trns_last_state ) && $all_states [$trns_last_state] >= $all_states [$predicted_state_code]))
					// to handle "Modification Required" state
					$current_values_prev_state ["current_state_code"] = $caseTrnsRecord ["current_state_code"];
				else {
					$current_values_prev_state ["current_state_code"] = $predicted_state_code;
				}
			} else {
				$current_values_prev_state ["current_state_code"] = $predicted_state_code;
			}
			// for generating states from the last state to current enterd
			self::logToFile ( "before: " . $current_values_prev_state ["current_state_code"] . "-" . $previous_values ["current_state_code"] . "-" . $trns_last_state . "-" . $predicted_state_code );
			if (/* $current_values_prev_state ["current_state_code"]  != 'SUSPENDED' && $current_values_prev_state ["current_state_code"]  != 'CLOSED' && */ $current_values_prev_state ["current_state_code"] != $previous_values ["current_state_code"] && $apply_sim == true)
				self::checkAndReflectCaseState ( $all_states, $trans_values, $trans_itr, $current_values_prev_state, $previous_values, $lawyer_id, $lawyer_office_id );
			self::logToFile ( "return: " . $current_values_prev_state ["current_state_code"] );
		} catch ( Exception $e ) {
			self::logToFile ( "Exception in updateCloseSuspendPrevStates" );
		}
		return $current_values_prev_state ["current_state_code"];
	}
	function checkCorrection($field_value, $field_type, $case_id, $lawyer_office_id) {
		$incorrect = false;
		if (isset ( $field_value ) && $field_value != "") {
			if ($field_type == "fk" && ! is_numeric ( $field_value )) {
				$incorrect = true;
			} else if ($field_type == "file") {
				$incorrect = false;//substr ( $field_value, 0, 12 + strlen ( $lawyer_office_id ) + strlen ( $case_id ) + 1 ) !== "cases_files/" . $lawyer_office_id . "/" . $case_id;
			} else if ($field_type == "region_id") {
				if (! is_numeric ( $field_value ))
					$incorrect = true;
				else {
					$regionRecord = $this->common->getOneRow ( 'region', "where region_id = $field_value" );
					if (! isset ( $regionRecord ) || sizeof ( $regionRecord ) <= 0)
						$incorrect = true;
				}
			}
		}
		if ($incorrect)
			self::logToFile ( "checkCorrection  $field_value    $field_type   $case_id  " . substr ( $field_value, 0, 12 + strlen ( $lawyer_office_id ) + strlen ( $case_id ) + 1 ) );
		return $incorrect;
	}
	function logToFile($msg) {
		$filename = 'log_import.txt';
		$fd = fopen ( $filename, "a" );
		$str = "[" . date ( "Y/m/d h:i:s", mktime () ) . "] " . $msg;
		fwrite ( $fd, $str . "\n" );
		fclose ( $fd );
	}
	function checkStateArrangment($current_values, $previous_values, &$all_states) {
		self::logToFile ( "in checkStateArrangment $current_values   $previous_values" );
		if ($current_values == 'MODIFICATION_REQUIRED' || $current_values == 'UNDER_REVIEW') {
			if ($previous_values == 'UNDER_REVIEW' || $previous_values == 'MODIFICATION_REQUIRED') {
				self::logToFile ( "true" );
				return false;
			} else {
				return true;
			}
		}
		// apply all states from prev to current
		$current_state_indx = $all_states [$current_values];
		$previous_state_indx = $all_states [$previous_values];
		if ($current_state_indx < $previous_state_indx) {
			// incorrect
			return true;
		}
		return false;
	}
}