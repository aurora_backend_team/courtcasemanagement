<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class admin_panel extends CI_Controller
{
	function __construct()
    {
        parent::__construct();
        $this -> load -> model("common");
		$this->load->library('encrypt');
		if($this->session->userdata('adminid')=='') {
			redirect('admin','refresh');
		}
    }
    
	function index() {
	  
	   self::view_packages();
	   
	}
	
	function view_packages($delete_error=0) {
	   $data['packages'] = $this->common->getAllRow('package','');
	   $data['delete_error'] = $delete_error;
	   $this->load->view('general_admin/view_packages',$data);
	}
	
	function delete_package($package_code) {
		$package_code = urldecode($package_code);
		$exist_count = $this->common->getCountOfField("lawyer_office_id","lawyer_office","where package_code = '".$package_code."'");
		if ($exist_count > 0) {
			redirect('admin_panel/view_packages/1');
		} else {
			$where = "package_code = '".$package_code."'";
			$this->common->deleteRecord('package',$where);
			
			redirect('admin_panel/view_packages');
		}
				
	}
	
	function add_edit_package($code='') {
		$code = urldecode($code);
		$data['package_code']=$code;
	    $data['package_name']="";
		$data['package_description']="";	
		$data['num_of_cases_a_month']="";
	    $data['num_of_region']="";
		$data['num_of_lawyer']="";
		$data['num_of_customer']="";
	    $data['price']="";
		   		
		if(extract($_POST)) {			
			$value['package_code']= $_POST['package_code']; 
			$value['package_name']= $_POST['package_name']; 			
			$value['package_description']= $_POST['package_description'];
			$value['num_of_cases_a_month']= $_POST['num_of_cases_a_month']; 
			$value['num_of_region']= $_POST['num_of_region']; 			
			$value['num_of_lawyer']= $_POST['num_of_lawyer'];
			$value['num_of_customer']= $_POST['num_of_customer']; 
			$value['price']= $_POST['price']; 			
			if($code != '' && isset($code)) {
				$where = "package_code ='".$code."'";
				$this->common->updateRecord('package',$value,$where);
			} else {
				$this->common->insertRecord('package',$value);
			}
			redirect('admin_panel/view_packages');
		}
		
		if($code != '' && isset($code)) {
			$where = "where package_code ='".$code."'";
			$result = $this->common->getOneRow('package',$where);
			$data['package_code']=$result['package_code'];
	    	$data['package_name']=$result['package_name'];
			$data['package_description']=$result['package_description'];
			$data['num_of_cases_a_month']=$result['num_of_cases_a_month'];
	    	$data['num_of_region']=$result['num_of_region'];
			$data['num_of_lawyer']=$result['num_of_lawyer'];
			$data['num_of_customer']=$result['num_of_customer'];
	    	$data['price']=$result['price'];
		}
				
		$this->load->view('general_admin/add_edit_package',$data);
	}
	
	function view_lawyer_offices($delete_error=0) {
	   $this -> load -> model("lawyer_office_model");
	   $lawyer_offices = $this->lawyer_office_model->getAllLawyerOffices();
	   
	   if ($lawyer_offices) {
		   $i = 0;
		   $this->load->library('../controllers/dates_conversion');
		   foreach($lawyer_offices as $record ) {
				$lawyer_offices[$i]['subscription_start_date'] = $this->dates_conversion->GregorianToHijri($record['subscription_start_date']);
				$lawyer_offices[$i]['subscription_end_date'] = $this->dates_conversion->GregorianToHijri($record['subscription_end_date']);
				$i++;
			}
	   }
	   
	   $data['lawyer_offices'] = $lawyer_offices;
	   $data['delete_error'] = $delete_error;
	   $this->load->view('general_admin/view_lawyer_offices',$data);
	}
	
	function delete_lawyer_office($lawyer_office_id) {
		$exist_count = $this->common->getCountOfField("user_id","user","where lawyer_office_id ='".$lawyer_office_id."'");
		if ($exist_count > 0) {
			redirect('admin_panel/view_lawyer_offices/1');
		} else {
			$where = "lawyer_office_id =".$lawyer_office_id;
			$this->common->deleteRecord('lawyer_office',$where);
			
			redirect('admin_panel/view_lawyer_offices');
		}
		
	}
	
	function add_edit_lawyer_office($lawyer_office_id=0,$email_already_exists=0) {
		
		$data['lawyer_office_id']=$lawyer_office_id;
        $data['package_code']="";
	    $data['name']="";
		$data['address']="";	
		$data['city']=0;
	    $data['phone_number']="";
		$data['mobile_number']="";	
		$data['email']="";
	    $data['contact_person_name']="";
		$data['position']="";	
		$data['subscription_start_date']="";	
		$data['subscription_end_date']="";
		$data['user_name'] = "";
		$data['password'] = "";
		$data['first_name'] = "";
		$data['last_name'] = "";
		$data['email_exists_error']= $email_already_exists;
		
		if(extract($_POST)) {			
			$value['package_code']= $_POST['package_code']; 
			$value['name']= $_POST['name']; 			
			$value['address']= $_POST['address'];
			$value['city']= $_POST['city']; 
			$value['phone_number']= $_POST['phone_number']; 			
			$value['mobile_number']= $_POST['mobile_number'];
			$value['email']= $_POST['email']; 
			$value['contact_person_name']= $_POST['contact_person_name']; 			
			$value['position']= $_POST['position'];
			$this->load->library('../controllers/dates_conversion');
			$value['subscription_start_date']= $this->dates_conversion->HijriToGregorian($_POST['subscription_start_date']); 
			$value['subscription_end_date']= $this->dates_conversion->HijriToGregorian($_POST['subscription_end_date']); 	
			
			$new_user_name = $_POST['user_name']; 
			$new_password = $this->encrypt->encode($_POST['password']); 

			
			if($lawyer_office_id > 0) {
				$new_email = $value['email'];
				$email_count = $this->common->getCountOfField("lawyer_office_id","lawyer_office","where email ='".$new_email."' AND lawyer_office_id !='".$lawyer_office_id."'");
				$user_count = 0;

				//Check if the username or password are changed.
				$where = "where lawyer_office_id ='".$lawyer_office_id."' AND  user_type_code='ADMIN'";
				$user_record = $this->common->getOneRow('user',$where);
				$old_user_name = $user_record['user_name'];
				$old_password = $this->encrypt->decode($user_record['password']);
				
				if($new_user_name != $old_user_name){
					$user_count =  $this->common->getCountOfField("user_id","user","where user_name ='".$new_user_name."'");
				}
				if ($email_count == 0 && $user_count == 0) {
					$where = "lawyer_office_id =".$lawyer_office_id;
					$this->common->updateRecord('lawyer_office',$value,$where);
					
					if(($old_user_name != $new_user_name) || ($old_password != $new_password)){
						$admin_office_value['user_name']= $new_user_name; 
						$admin_office_value['password']= $new_password; 
						$where = "lawyer_office_id ='".$lawyer_office_id."' AND  user_type_code='ADMIN'";
						$this->common->updateRecord('user',$admin_office_value,$where);
						//send sms
						$to_user_mobile = $user_record['mobile_number'];
						$to_user_name = $user_record['first_name'];
						if(isset($to_user_mobile) && $to_user_mobile !="" && $to_user_mobile != NULL){
							$sms_message = 'الأستاذ / '.$to_user_name.'%0aبعد التحية و التقدير ...%0a';
							$sms_message .= 'لقد تم تغيير بيانات الدخول الى اسم المستخدم ('.$new_user_name.' ) وكلمة المرور ('.$this->encrypt->decode($new_password).' )';
							self::sendSMS("Y", $to_user_mobile, $sms_message);
						}
						
					}
					
					redirect('admin_panel/view_lawyer_offices');
					
				} else {
					$email_exists = 0;
					$user_name_exists = 0;
					
					if ($email_count != 0) {
						$email_exists = 1;
					}
					if ($user_count!= 0) {
						$user_name_exists = 1;
					}
					
					self::lawyer_office_with_email_error($lawyer_office_id,$value['package_code'],$value['name'],$value['address'],$value['city'],
					$value['phone_number'],$value['mobile_number'],$value['email'],$value['contact_person_name'],$value['position'],
					$_POST['subscription_start_date'],$_POST['subscription_end_date'], "", "",
					"", "", $email_exists, $user_name_exists);
				}
				
			} else {
				$new_email = $value['email'];
				$email_count = $this->common->getCountOfField("lawyer_office_id","lawyer_office","where email ='".$new_email."'");
				$user_count =  $this->common->getCountOfField("user_id","user","where user_name ='".$new_user_name."'");
				
				$admin_office_value['user_name']= $_POST['user_name']; 
				$admin_office_value['password']= $this->encrypt->encode($_POST['password']); 
				$admin_office_value['first_name']= $_POST['first_name']; 
				$admin_office_value['last_name']= $_POST['last_name']; 
				$admin_office_value['user_type_code']= "ADMIN"; 
				
				if ($email_count == 0 && $user_count == 0) {
					$this->db->trans_begin();
					
					$new_lawyer_office_id = $this->common->insertRecord('lawyer_office',$value);
					$admin_office_value['lawyer_office_id']= $new_lawyer_office_id;
					
					$this->common->insertRecord('user',$admin_office_value);
					
					if ($this->db->trans_status() === FALSE) {
			   			$this->db->trans_rollback();
					} else {
					    $this->db->trans_commit();
					}
					redirect('admin_panel/view_lawyer_offices');
				} else {
					$email_exists = 0;
					$user_name_exists = 0;
					
					if ($email_count != 0) {
						$email_exists = 1;
					}
					if ($user_count!= 0) {
						$user_name_exists = 1;
					}
					
					self::lawyer_office_with_email_error($lawyer_office_id,$value['package_code'],$value['name'],$value['address'],$value['city'],
					$value['phone_number'],$value['mobile_number'],$value['email'],$value['contact_person_name'],$value['position'],
					$_POST['subscription_start_date'],$_POST['subscription_end_date'], $admin_office_value['user_name'], $admin_office_value['password'],
					$admin_office_value['first_name'], $admin_office_value['last_name'], $email_exists, $user_name_exists);
				}		
				
			}
			
			
		} else {
			if($lawyer_office_id > 0) {
				$where = "where lawyer_office_id =".$lawyer_office_id;
				$result = $this->common->getOneRow('lawyer_office',$where);
				$data['package_code']=$result['package_code'];
		    	$data['name']=$result['name'];
				$data['address']=$result['address'];
				$data['city']=$result['city'];
		    	$data['phone_number']=$result['phone_number'];
				$data['mobile_number']=$result['mobile_number'];
				$data['email']=$result['email'];
		    	$data['contact_person_name']=$result['contact_person_name'];
				$data['position']=$result['position'];
		    	$this->load->library('../controllers/dates_conversion');
				$data['subscription_start_date']=$this->dates_conversion->GregorianToHijri($result['subscription_start_date']);
		    	$data['subscription_end_date']=$this->dates_conversion->GregorianToHijri($result['subscription_end_date']);
				
				$where = "where lawyer_office_id ='".$lawyer_office_id."' AND  user_type_code='ADMIN'";
				$user_record = $this->common->getOneRow('user',$where);
				$data['user_name'] = $user_record['user_name'];
				$data['password'] = $this->encrypt->decode($user_record['password']);
			}
					
			$data['available_packages']= $this -> common -> getAllRow("package", "");
			$data['available_cities']= $this -> common -> getAllRow("city", "");
			
			$this->load->view('general_admin/add_edit_lawyer_office',$data);
		}
		
		
	}
	
	function lawyer_office_with_email_error($lawyer_office_id,$package_code,$name,$address,$city,
					$phone_number,$mobile_number,$email,$contact_person_name,$position,
					$subscription_start_date,$subscription_end_date, $user_name, $password,
					$first_name, $last_name, $email_exists, $user_name_exists) {
		
		$data['lawyer_office_id']=$lawyer_office_id;
        $data['package_code']=$package_code;
	    $data['name']=$name;
		$data['address']=$address;	
		$data['city']=$city;
	    $data['phone_number']=$phone_number;
		$data['mobile_number']=$mobile_number;	
		$data['email']=$email;
	    $data['contact_person_name']=$contact_person_name;
		$data['position']=$position;	
		$data['subscription_start_date']=$subscription_start_date;	
		$data['subscription_end_date']=$subscription_end_date;
		$data['user_name']=$user_name;
		$data['password']=$password;
		$data['first_name']=$first_name;
		$data['last_name']=$last_name;
		$data['last_name']=$last_name;
		$data['user_name_exists']=$user_name_exists;
		$data['email_exists_error']= $email_exists;
				
		$data['available_packages']= $this -> common -> getAllRow("package", "");
		$data['available_cities']= $this -> common -> getAllRow("city", "");
			
		$this->load->view('general_admin/add_edit_lawyer_office',$data);
	}
	
	function view_cities($delete_error=0) {
	   $data['cities'] = $this->common->getAllRow('city','');
	   $data['delete_error'] = $delete_error;
	   $this->load->view('general_admin/view_cities',$data);
	}
	
	function delete_city($city_id) {
		$region_cities_exist_count = $this->common->getCountOfField("region_id","region_cities","where region_city_id ='".$city_id."'");
		$customer_details_exist_count = $this->common->getCountOfField("customer_details_id","customer_details","where city ='".$city_id."'");
		$lawyer_office_exist_count = $this->common->getCountOfField("lawyer_office_id","lawyer_office","where city ='".$city_id."'");
		if ($region_cities_exist_count > 0 || $customer_details_exist_count > 0 || $lawyer_office_exist_count > 0) {
			redirect('admin_panel/view_cities/1');
		} else {
			$where = "city_id =".$city_id;
			$this->common->deleteRecord('city',$where);
			
			redirect('admin_panel/view_cities');
		}
		
	}
	
	function add_edit_city($city_id=0) {
		
		$data['city_id']=$city_id;
        $data['city_name']="";
	   
		if(extract($_POST)) {			
			$value['city_name']= $_POST['city_name']; 
						
			if($city_id > 0) {
				$where = "city_id =".$city_id;
				$this->common->updateRecord('city',$value,$where);
			} else {
				$this->common->insertRecord('city',$value);
			}
			redirect('admin_panel/view_cities');
		}
		
		if($city_id > 0) {
			$where = "where city_id =".$city_id;
			$result = $this->common->getOneRow('city',$where);
			$data['city_name']=$result['city_name'];
	    	
		}
				
		$this->load->view('general_admin/add_edit_city',$data);
	}
	
	//Send SMS API using CURL method
	function sendSMS($sms_active, $mobile, $msg)
	{
		if ($sms_active == "Y") {
			global $arraySendMsg;
			$url = "www.mobily.ws/api/msgSend.php";
			$numbers = substr_replace($mobile,"966",0,1);
			$userAccount = "966504211994";
			$passAccount = "live123";
			$applicationType = "68"; 
			$sender = "IbnAlshaikh";
			$MsgID = "15176";
			$timeSend=0;
			$dateSend=0;
			$deleteKey=0;
			$viewResult=1;
			
			$sender = urlencode($sender);
			$domainName = "altanfeeth.com";
			$stringToPost = "mobile=".$userAccount."&password=".$passAccount."&numbers=".$numbers."&sender=".$sender."&msg=".$msg."&timeSend=".$timeSend."&dateSend=".$dateSend."&applicationType=".$applicationType."&domainName=".$domainName."&msgId=".$MsgID."&deleteKey=".$deleteKey."&lang=3";
		
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_TIMEOUT, 5);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $stringToPost);
			$result = curl_exec($ch);
		
			//if($viewResult)
			//	$result = printStringResult(trim($result) , $arraySendMsg);
			return $result;
		}
		
	}
	
	
}

