<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class invoices extends CI_Controller
{
	function __construct()
    {
        parent::__construct();
        $this -> load -> model("common");
		if($this->session->userdata('userid')=='' || $this->session->userdata('user_type')!='ADMIN') {
                        //echo 'user data  is null';
			redirect('user_login','refresh');
		}
    }
    
	function index($user_id=0) {
	  
	   self::get_invoices($user_id);
	   
	}
	
	
	function get_invoices($user_id=0) {
	   $session_user_id = $this->session->userdata('userid');
	   $this -> load -> model("invoices_model");
	   $invoices_arr = $this -> invoices_model -> getUserInvoices($user_id);
	   
	   $this -> load -> model("notification_model");
	   	$notifications = $this -> notification_model -> getLatestDeliveredNotificationsForUser($session_user_id);
	   	$data['notifications_list'] = $notifications['notifications_list'];
		$data['count_unseen'] = $notifications['count_unseen'];
		$data['page'] = "invoices/get_invoices";
		
		/* initialize pagination parameters */
		$current_page = 1;
		$has_prev = false;
		$has_next = false;
		$page_size = 25;
		$total_files_num = 0;
		$total_pages_num = 0;
		$current_page_size = 0;
		$post = '0';
		if (extract ( $_POST )) {
			$post = '1';
			$current_page = $this->input->post ( 'current_page' );
		}
		if ($invoices_arr) {
			$files_count = count ($invoices_arr);
		} else {
			$files_count = 0;
		}
		$total_files_num = $files_count;
		$total_pages_num = ceil ( $total_files_num / $page_size );
		$offset = ($current_page - 1) * $page_size;
		if ($invoices_arr) {
			while ( $offset >= $total_files_num ) { // special case happen when delete last file in some page
				$current_page = $current_page - 1;
				$offset = ($current_page - 1) * $page_size;
			}
			while ( $offset < 0 ) {
				$current_page = $current_page + 1;
				$offset = ($current_page - 1) * $page_size;
			}
			$invoices_arr = array_slice ( $invoices_arr, $offset, $page_size );
			$current_page_size = count ( $invoices_arr );
		}
		if ($current_page > 1) {
			$has_prev = true;
		}
		if ($current_page < $total_pages_num) {
			$has_next = true;
		}
		$data ['current_page'] = $current_page;
		$data ['total_pages_num'] = $total_pages_num;
		$data ['page_size'] = $page_size;
		$data ['total_files_num'] = $total_files_num;
		$data ['current_page_size'] = $current_page_size;
		$data ['has_prev'] = $has_prev;
		$data ['has_next'] = $has_next;
		$data ['user_id_param'] = $user_id;
		
		
		$data['invoices'] = $invoices_arr;
		
	   if ($data['invoices']) {
		   $i = 0;
		   $this->load->library('../controllers/dates_conversion');
		   foreach($data['invoices'] as $record ) {
				$data['invoices'][$i]['invoice_date'] = $this->dates_conversion->GregorianToHijri($record['invoice_date']);
				$i++;
			}
	   }
	
	   
	   $where = "where user_id =".$session_user_id ;
	   $result = $this->common->getOneRow('user',$where);
	   $data['profile_first_name']=$result['first_name'];
	   $data['profile_last_name']=$result['last_name'];
	   $data['profile_img']=$result['profile_img'];	
	   
	   
	   if ($post == '1') {
		   	$faq_View = $this->load->view ( 'accounting/view_invoices', $data, TRUE );
		   	echo json_encode ( $faq_View );
	   } else {
	   		$this->load->view('accounting/view_invoices',$data);
	   }
	   
	}
	
	function edit_invoice($invoice_id="") {
		
	   $session_user_id = $this->session->userdata('userid');
	   
	   $this -> load -> model("notification_model");
	   	$notifications = $this -> notification_model -> getLatestDeliveredNotificationsForUser($session_user_id);
	   	$data['notifications_list'] = $notifications['notifications_list'];
		$data['count_unseen'] = $notifications['count_unseen'];
		$data['page'] = "invoices/edit_invoice/".$invoice_id;
		
       $where = "where user_id =".$session_user_id ;
	   $result = $this->common->getOneRow('user',$where);
	   $data['profile_first_name']=$result['first_name'];
	   $data['profile_last_name']=$result['last_name'];
	   $data['profile_img']=$result['profile_img'];
	   
	   $data['invoice_id']=$invoice_id;
	   $data['is_paid']="";	
	   $data['billing_file']="";
	   $data['invoice_date']="";	
   	   $data['customer_first_name']="";
   	   $data['customer_last_name']="";	
   	   $data['lawyer_first_name']="";
   	   $data['lawyer_last_name']="";	
   	   $data['case_id']="";
   	   $data['state_name']="";	
   	   $data['case_state_code']="";
   	   $data['billing_value']="";	
   	   $data['invoice_user_id']="";
	    		   		
		if(extract($_POST)) {			
			$value['is_paid']= $_POST['is_paid']; 	
			
			if(isset($_FILES['billing_file']['name']) && $_FILES['billing_file']['name']!="") { 
				self::upload_file($value, 'billing_file', $invoice_id, $_FILES['billing_file']['name'], $_FILES['billing_file']['tmp_name']);
			}
			
			
			if($invoice_id != "") {
				$where = "invoice_id ='".$invoice_id."'";
				$this->common->updateRecord('invoices',$value,$where);
			}
			$this -> load -> model("invoices_model");
	   		$invoice_data = $this -> invoices_model -> getInvoiceData($invoice_id);
	   		$user_id=$invoice_data['invoice_user_id'];	
			redirect("invoices/get_invoices/".$user_id);
		} else {
			if($invoice_id != "") {
				$this -> load -> model("invoices_model");
	   			$invoice_data = $this -> invoices_model -> getInvoiceData($invoice_id);
	   			$data['is_paid']=$invoice_data['is_paid'];	
	   			$data['billing_file']=$invoice_data['billing_file'];
	   			$this->load->library('../controllers/dates_conversion');
				$data['invoice_date'] = $this->dates_conversion->GregorianToHijri($invoice_data['invoice_date']);
	   			$data['customer_first_name']=$invoice_data['customer_first_name'];
	   			$data['customer_last_name']=$invoice_data['customer_last_name'];	
	   			$data['lawyer_first_name']=$invoice_data['lawyer_first_name'];
	   			$data['lawyer_last_name']=$invoice_data['lawyer_last_name'];	
	   			$data['case_id']=$invoice_data['case_id'];
	   			$data['state_name']=$invoice_data['state_name'];	
	   			$data['case_state_code']=$invoice_data['case_state_code'];
	   			$data['billing_value']=$invoice_data['billing_value'];	
	   			$data['invoice_user_id']=$invoice_data['invoice_user_id'];
			}
							
			$this->load->view('accounting/edit_invoice',$data);
		}
		
		
	}
	
	function upload_file(&$value, $param_name, $invoice_id, $name, $tmp_name) {
		if(isset($name) && $name !="") { 
			if(!is_dir("./invoices_files/".$invoice_id))	{
				 mkdir('./invoices_files/'.$invoice_id, 0777, true);
				 copy( './invoices_files/index.html', './invoices_files/'.$invoice_id.'/index.html' );
			}								
			$file =$name;
			$uploadedfile =$tmp_name;					
			$ext = strstr($name,".");									
          			
			$source = $tmp_name;
			$file= $param_name."-".str_replace(' ','-',$file); 					
 		    $filepath = $file;
          	$save = "./invoices_files/".$invoice_id."/".$filepath;           			
 		    move_uploaded_file($uploadedfile,$save);
 		           	
 		    $value[$param_name]= "invoices_files/".$invoice_id."/".$file;
		}
	}
	
	function update_is_paid_value() {
		if(extract($_POST)) {			
			$invoice_id= $_POST['invoice_id'];
			$value['is_paid'] = $_POST['is_paid'];
			if($invoice_id != "") {
				$where = "invoice_id ='".$invoice_id."'";
				$this->common->updateRecord('invoices',$value,$where);
			}
		}	
	}
  
}
