<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class dates_conversion 
{
	function __construct()
    {
       // parent::__construct();
       error_reporting(E_ERROR);
       
    }
    
	function HijriToGregorian($hijri_date) {
		//$hijri_date = "1438/05/29";
		if ($hijri_date == null || $hijri_date == "" || $hijri_date == " " || $hijri_date == "0000-00-00") {
			return "";
		} else {
			$date_parts = explode("/", $hijri_date);
			$y = ltrim($date_parts[0], '0');
			$m = ltrim($date_parts[1], '0');
			$d = ltrim($date_parts[2], '0');
			
	   		$date = (int)((11 * $y + 3) / 30) + 354 * $y + 30 * $m - (int)(($m - 1) / 2) + $d + 1948440 - 385;
	
			$gregorian_date = jdtogregorian($date);
			$date_parts = explode("/", $gregorian_date);
			$d = $date_parts[1];
			if (strlen($d) < 2) {
				$d = "0".$d;
			}
			$m = $date_parts[0];
			if (strlen($m) < 2) {
				$m = "0".$m;
			}
			$y = $date_parts[2];
			
			$gregorian_date = $y."-".$m."-".$d;
			return $gregorian_date;
		}
		
	}
	
  function GregorianToHijri($gregorian_date) {
  	 // $gregorian_date = "2017-02-26";
  		if ($gregorian_date == null || $gregorian_date == "" || $gregorian_date == " " || $gregorian_date == "0000-00-00") {
			return "";
		} else {
			$date_parts = explode("-", $gregorian_date);
			  $y = ltrim($date_parts[0], '0');
			  $m = ltrim($date_parts[1], '0');
			  $d = ltrim($date_parts[2], '0');
		  	  
		  	  $jd = GregorianToJD($m, $d, $y);
		      $jd = $jd - 1948440 + 10632;
		      $n  = (int)(($jd - 1) / 10631);
		      $jd = $jd - 10631 * $n + 354;
		      $j  = ((int)((10985 - $jd) / 5316)) *
		          ((int)(50 * $jd / 17719)) +
		          ((int)($jd / 5670)) *
		          ((int)(43 * $jd / 15238));
		      $jd = $jd - ((int)((30 - $j) / 15)) *
		          ((int)((17719 * $j) / 50)) -
		          ((int)($j / 16)) *
		          ((int)((15238 * $j) / 43)) + 29;
		      $m  = (int)(24 * $jd / 709);
		      $d  = $jd - (int)(709 * $m / 24);
		      $y  = 30*$n + $j - 30;
		      
		  	  if (strlen($d) < 2) {
					$d = "0".$d;
			  }
		      if (strlen($m) < 2) {
					$m = "0".$m;
			  }
			  $hijri_date = $y."/".$m."/".$d;
		      return $hijri_date;
		}
  	  
  }
  
}
