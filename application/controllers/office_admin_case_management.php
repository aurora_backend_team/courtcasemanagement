<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class office_admin_case_management extends CI_Controller
{
	function __construct()
    {
     	parent::__construct();
        $this -> load -> model("common");
		// Admin privilege
		if($this->session->userdata('userid')=='' || $this->session->userdata('user_type')!='ADMIN' ) {
			redirect('user_login','refresh');
		}
    }
    
	function index($case_id='') {
	  
	   self::edit_case($case_id);
	   
	}

	function insert_transaction($values, $status) {
			$this -> load -> model("case_model");
			$values['transaction_id'] = $this -> case_model -> getTransactionId();
			$values['transaction_status']=$status;
			if ($status == "Failure") {
				$values['error_code']=1;
			}			
			$this->common->insertRecord('case_transactions',$values);
	}
	// like manage_case
	function edit_case($case_id='') {
		$user_id = $this->session->userdata('userid');
		$where = "where user_id =".$user_id;
		$userRecord = $this->common->getOneRow('user',$where);
		$lawyer_office_id = $userRecord["lawyer_office_id"];
		if(extract($_POST)) {	
			$user_type_code = $userRecord['user_type_code'];
			$this -> load -> model("case_model");
			
			$trans_values = array();
			$trans_values['case_id'] = $case_id;
			$trans_values['lawyer_office_id'] = $lawyer_office_id;
			$nullFields = $_POST['nullFields'];
			
			$value = array();
			self::getNewParameterValue($value, $trans_values, $_POST['customer_id'], 'customer_id', $nullFields);
			self::getNewParameterValue($value, $trans_values, $_POST['contract_number'], 'contract_number', $nullFields);
			self::getNewParameterValue($value, $trans_values, $_POST['client_id_number'], 'client_id_number', $nullFields);
			self::getNewParameterValue($value, $trans_values, $_POST['client_name'], 'client_name', $nullFields);
			self::getNewParameterValue($value, $trans_values, $_POST['client_type'], 'client_type', $nullFields);
			self::getNewParameterValue($value, $trans_values, $_POST['number_of_late_instalments'], 'number_of_late_instalments', $nullFields);
			self::getNewParameterValue($value, $trans_values, $_POST['due_amount'], 'due_amount', $nullFields);
			self::getNewParameterValue($value, $trans_values, $_POST['remaining_amount'], 'remaining_amount', $nullFields);
			self::getNewParameterValue($value, $trans_values, $_POST['total_debenture_amount'], 'total_debenture_amount', $nullFields);
			self::getNewParameterValue($value, $trans_values, $_POST['debtor_name'], 'debtor_name', $nullFields);
			self::getNewParameterValue($value, $trans_values, $_POST['obligation_value'], 'obligation_value', $nullFields);

			if(isset($_POST['debenture_hidden']) && $_POST['debenture_hidden'] != NULL && $_POST['debenture_hidden'] !="" ){
				$tmp_path = $_POST['debenture_hidden'];
				$tmp_path = str_replace("cases_tmp_files","cases_files",$tmp_path);
				copy($_POST['debenture_hidden'], $tmp_path);
				$value['debenture']= $tmp_path;
				$trans_values['debenture']= $tmp_path;
			}else{
				if(isset($_FILES['debenture']['name']) && $_FILES['debenture']['name']!="") {
					self::upload_file($value, $trans_values, 'debenture', $case_id, $_FILES['debenture']['name'], $_FILES['debenture']['tmp_name']);
				}
			}
			if(isset($_POST['id_hidden']) && $_POST['id_hidden'] != NULL && $_POST['id_hidden'] !="" ){
				$tmp_path = $_POST['id_hidden'];
				$tmp_path = str_replace("cases_tmp_files","cases_files",$tmp_path);
				copy($_POST['id_hidden'], $tmp_path);
				$value['id']= $tmp_path;
				$trans_values['id']= $tmp_path;
			}else{
				if(isset($_FILES['id']['name']) && $_FILES['id']['name']!="") {
					self::upload_file($value, $trans_values, 'id', $case_id, $_FILES['id']['name'], $_FILES['id']['tmp_name']);
				}
			}
			if(isset($_POST['contract_hidden']) && $_POST['contract_hidden'] != NULL && $_POST['contract_hidden'] !="" ){
				$tmp_path = $_POST['contract_hidden'];
				$tmp_path = str_replace("cases_tmp_files","cases_files",$tmp_path);
				copy($_POST['contract_hidden'], $tmp_path);
				$value['contract']= $tmp_path;
				$trans_values['contract']= $tmp_path;
			}else{
				if(isset($_FILES['contract']['name']) && $_FILES['contract']['name']!="") {
					self::upload_file($value, $trans_values, 'contract', $case_id, $_FILES['contract']['name'], $_FILES['contract']['tmp_name']);
				}
			}
			if(isset($_POST['others_hidden']) && $_POST['others_hidden'] != NULL && $_POST['others_hidden'] !="" ){
				$tmp_path = $_POST['others_hidden'];
				$tmp_path = str_replace("cases_tmp_files","cases_files",$tmp_path);
				$tmp_arr = explode(",", $_POST['others_hidden']);
				$final_arr = explode(",", $tmp_path);
				for($i=0;$i<count($final_arr);$i++){
					copy($tmp_arr[$i], $final_arr[$i]);
				}
				$value['others']= $tmp_path;
				$trans_values['others']= $tmp_path;
			}else{
				$others_value = "";
				if(isset($_FILES['others']['name']) && count($_FILES["others"]['name'])>0) { 
					for ($i = 0; $i < count($_FILES["others"]['name']); $i++) {
						$item = "others_".($i+1);
						$name = $_FILES['others']['name'][$i];
						$tmp_name = $_FILES['others']['tmp_name'][$i];
						if(isset($name) && $name !="") { 
							if(!is_dir("./cases_files/".$lawyer_office_id."/".$value['case_id']))	{
								 mkdir('./cases_files/'.$lawyer_office_id."/".$value['case_id'], 0777, true);
							}								
							$file =$name;
							$uploadedfile =$tmp_name;					
							$ext = strstr($name,".");									
				          			
							$source = $tmp_name;
							$file= $item."-".str_replace(' ','-',$file); 					
				 		    $filepath = $file;
				          	$save = "./cases_files/".$lawyer_office_id."/".$value['case_id']."/".$filepath;           			
				 		    move_uploaded_file($uploadedfile,$save);
	
				 		    $others_value = $others_value.","."cases_files/".$lawyer_office_id."/".$value['case_id']."/".$file;
				 		   								
						}
					}
					$others_value = trim($others_value,",");
					$value["others"]= $others_value;
					$trans_values["others"]= $others_value;	
				}
			}
						
		//	self::getNewParameterValue($value, $trans_values, $_POST['creation_date'], 'creation_date');
			self::getNewParameterValue($value, $trans_values, $_POST['order_number'], 'order_number', $nullFields);
			self::getNewParameterValue($value, $trans_values, $_POST['circle_number'], 'circle_number', $nullFields);
			self::getNewParameterValue($value, $trans_values, $_POST['executive_order_number'], 'executive_order_number', $nullFields);
			self::getNewParameterValue($value, $trans_values, $_POST['decision_34_number'], 'decision_34_number', $nullFields);
			if(isset($_FILES['decision_34_file']['name']) && $_FILES['decision_34_file']['name']!="") { 
				self::upload_file($value, $trans_values, 'decision_34_file', $case_id, $_FILES['decision_34_file']['name'], $_FILES['decision_34_file']['tmp_name']);
			}
			if(isset($_POST['advertisement_file_hidden']) && $_POST['advertisement_file_hidden'] != NULL && $_POST['advertisement_file_hidden'] !="" ){
				$tmp_path = $_POST['advertisement_file_hidden'];
				$tmp_path = str_replace("cases_tmp_files","cases_files",$tmp_path);
				copy($_POST['advertisement_file_hidden'], $tmp_path);
				$value['advertisement_file']= $tmp_path;
				$trans_values['advertisement_file']= $tmp_path;
			}else{
				if(isset($_FILES['advertisement_file']['name']) && $_FILES['advertisement_file']['name']!="") {
					self::upload_file($value, $trans_values, 'advertisement_file', $case_id, $_FILES['advertisement_file']['name'], $_FILES['advertisement_file']['tmp_name']);
				}
			}

			if(isset($_POST['invoice_file_hidden']) && $_POST['invoice_file_hidden'] != NULL && $_POST['invoice_file_hidden'] !="" ){
				$tmp_path = $_POST['invoice_file_hidden'];
				$tmp_path = str_replace("cases_tmp_files","cases_files",$tmp_path);
				copy($_POST['invoice_file_hidden'], $tmp_path);
				$value['invoice_file']= $tmp_path;
				$trans_values['invoice_file']= $tmp_path;
			}else{
				if(isset($_FILES['invoice_file']['name']) && $_FILES['invoice_file']['name']!="") {
					self::upload_file($value, $trans_values, 'invoice_file', $case_id, $_FILES['invoice_file']['name'], $_FILES['invoice_file']['tmp_name']);
				}
			}

			if(isset($_POST['referral_paper_hidden']) && $_POST['referral_paper_hidden'] != NULL && $_POST['referral_paper_hidden'] !="" ){
				$tmp_path = $_POST['referral_paper_hidden'];
				$tmp_path = str_replace("cases_tmp_files","cases_files",$tmp_path);
				copy($_POST['referral_paper_hidden'], $tmp_path);
				$value['referral_paper']= $tmp_path;
				$trans_values['referral_paper']= $tmp_path;
			}else{
				if(isset($_FILES['referral_paper']['name']) && $_FILES['referral_paper']['name']!="") {
					self::upload_file($value, $trans_values, 'referral_paper', $case_id, $_FILES['referral_paper']['name'], $_FILES['referral_paper']['tmp_name']);
				}
			}
			
			if(isset($_POST['consignment_image_hidden']) && $_POST['consignment_image_hidden'] != NULL && $_POST['consignment_image_hidden'] !="" ){
				$tmp_path = $_POST['consignment_image_hidden'];
				$tmp_path = str_replace("cases_tmp_files","cases_files",$tmp_path);
				copy($_POST['consignment_image_hidden'], $tmp_path);
				$value['consignment_image']= $tmp_path;
				$trans_values['consignment_image']= $tmp_path;
			}else{
				if(isset($_FILES['consignment_image']['name']) && $_FILES['consignment_image']['name']!="") {
					self::upload_file($value, $trans_values, 'consignment_image', $case_id, $_FILES['consignment_image']['name'], $_FILES['consignment_image']['tmp_name']);
				}
			}
			if(isset($_POST['suspend_file_hidden']) && $_POST['suspend_file_hidden'] != NULL && $_POST['suspend_file_hidden'] !="" ){
				$tmp_path = $_POST['suspend_file_hidden'];
				$tmp_path = str_replace("cases_tmp_files","cases_files",$tmp_path);
				copy($_POST['suspend_file_hidden'], $tmp_path);
				$value['suspend_file']= $tmp_path;
				$trans_values['suspend_file']= $tmp_path;
			}else{
				if(isset($_FILES['suspend_file']['name']) && $_FILES['suspend_file']['name']!="") {
					self::upload_file($value, $trans_values, 'suspend_file', $case_id, $_FILES['suspend_file']['name'], $_FILES['suspend_file']['tmp_name']);
				}
			}

			$this->load->library('../controllers/dates_conversion');
			$decision_46_date = "";
			if ($_POST['decision_46_date'] != null && $_POST['decision_46_date'] != ""  && $_POST['decision_46_date'] != " ") {
				$decision_46_date = $this->dates_conversion->HijriToGregorian($_POST['decision_46_date']);
			}
			$decision_34_date = "";
			if ($_POST['decision_34_date'] != null && $_POST['decision_34_date'] != "" && $_POST['decision_34_date'] != " ") {
				$decision_34_date = $this->dates_conversion->HijriToGregorian($_POST['decision_34_date']);
			}
			$executive_order_date = "";
			if ($_POST['executive_order_date'] != null && $_POST['executive_order_date'] != ""  && $_POST['executive_order_date'] != " ") {
				$executive_order_date = $this->dates_conversion->HijriToGregorian($_POST['executive_order_date']);
			}
			$advertisement_date = "";
			if ($_POST['advertisement_date'] != null && $_POST['advertisement_date'] != "" && $_POST['advertisement_date'] != " ") {
				$advertisement_date = $this->dates_conversion->HijriToGregorian($_POST['advertisement_date']);
			}
			$suspend_date = "";
			if ($_POST['suspend_date'] != null && $_POST['suspend_date'] != "" && $_POST['suspend_date'] != " ") {
				$suspend_date = $this->dates_conversion->HijriToGregorian($_POST['suspend_date']);
			}		
			self::getNewParameterValue($value, $trans_values, $decision_46_date, 'decision_46_date', $nullFields);
			self::getNewParameterValue($value, $trans_values, $decision_34_date, 'decision_34_date', $nullFields);
			self::getNewParameterValue($value, $trans_values, $executive_order_date, 'executive_order_date', $nullFields);
			self::getNewParameterValue($value, $trans_values, $advertisement_date, 'advertisement_date', $nullFields);
			self::getNewParameterValue($value, $trans_values, $suspend_date, 'suspend_date', $nullFields);
			self::getNewParameterValue($value, $trans_values, $_POST['closing_reason'], 'closing_reason', $nullFields);
			self::getNewParameterValue($value, $trans_values, $_POST['suspend_reason_code'], 'suspend_reason_code', $nullFields);
			
			$trans_values['last_state_code']= NULL; 
			
			$value['last_update_user']= $this->session->userdata('userid');
			$trans_values['last_update_user'] = $value['last_update_user'];
			date_default_timezone_set('Asia/Riyadh');
			$current_date_time = date("Y-m-d H:i:s");
			$dt = new DateTime($current_date_time);
			$current_date = $dt->format('Y-m-d');
			$trans_values['trans_date_time'] = $current_date_time;
			$value['last_update_date']= $current_date;
			$trans_values['last_update_date'] = $value['last_update_date'];
			$value['notes'] = $_POST['notes'];
			$trans_values['notes'] = $value['notes'];
			$trans_values['edit_action'] = 'Y';			
			
			$where = "case_id ='".$case_id."' and lawyer_office_id = '".$lawyer_office_id."'";
			$affected_rows = $this->common->updateRecord('case',$value,$where);
			if ($affected_rows == 1) {
				self::insert_transaction($trans_values, "Success");
				$activity = "تم التعديل فى القضية ".$case_id;
				self::insert_activity($user_id, $activity);
			} else {
				self::insert_transaction($trans_values, "Failure");
			}
			
			redirect('case_management/view_case_details/'.$case_id);
		} else {
			redirect('case_management/view_cases/');
		}
	}
	
	function getNewParameterValue(&$value, &$trans_values, $param_value, $param_name, $nullFields) {
		if ($param_value != null && isset($param_value) && $param_value !=""){
			$value[$param_name] = $param_value;
			$trans_values[$param_name] = $param_value;
		}
		
		if ($nullFields != null && isset($nullFields) && $nullFields != "" && strpos($nullFields, $param_name) !== false){
			$value[$param_name] = '';
			$trans_values[$param_name] = '';
		}
	}
	
	function upload_file(&$value, &$trans_values, $param_name, $case_id, $name, $tmp_name) {
		$user_id = $this->session->userdata('userid');
		$where = "where user_id =".$user_id;
		$userRecord = $this->common->getOneRow('user',$where);
		$lawyer_office_id = $userRecord["lawyer_office_id"];
		if(isset($name) && $name !="") { 
			if(!is_dir("./cases_files/".$lawyer_office_id."/".$case_id))	{
				 mkdir('./cases_files/'.$lawyer_office_id.'/'.$case_id, 0777, true);
				 copy( './cases_files/index.html', './cases_files/'.$lawyer_office_id.'/'.$case_id.'/index.html' );
			}								
			$file =$name;
			$uploadedfile =$tmp_name;					
			$ext = strstr($name,".");									
          			
			$source = $tmp_name;
			$file= $param_name."-".str_replace(' ','-',$file); 					
 		    $filepath = $file;
          	$save = "./cases_files/".$lawyer_office_id."/".$case_id."/".$filepath;           			
 		    move_uploaded_file($uploadedfile,$save);
 		           	
 		    $value[$param_name]= "cases_files/".$lawyer_office_id."/".$case_id."/".$file;
		    $trans_values[$param_name]= "cases_files/".$lawyer_office_id."/".$case_id."/".$file;									
		}
	}
	
	function insert_activity($user_id, $activity) {
			$values = array();
			$values['user_id'] = $user_id;
			$values['activity'] = $activity;
			$this->load->library('../controllers/util');
			$values['user_IP'] = $this->util->get_client_ip();
			//$values['user_IP'] = self::get_client_ip();	
			date_default_timezone_set('Asia/Riyadh');
			$current_date_time = date("Y-m-d H:i:s");
			$values['date_time'] = $current_date_time;	
			$this->common->insertRecord('user_activities',$values);
	}
	
}

