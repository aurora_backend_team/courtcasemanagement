<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//require (APPPATH.'/controllers/dates_conversion.php');


class lawyer_office_panel extends CI_Controller
{
	function __construct()
    {
        parent::__construct();
        $this -> load -> model("common");
		$this->load->library('encrypt');
		if($this->session->userdata('userid')=='' || $this->session->userdata('user_type')!='ADMIN') {
                        //echo 'user data  is null';
			redirect('user_login','refresh');
		}
    }
    
	function index() {
	  
	   self::view_customers();
	   
	}
	
	
	function view_customers($delete_error=0) {
	   $office_admin_id = $this->session->userdata('userid');
	   
	   $this -> load -> model("notification_model");
	   	$notifications = $this -> notification_model -> getLatestDeliveredNotificationsForUser($office_admin_id);
	   	$data['notifications_list'] = $notifications['notifications_list'];
		$data['count_unseen'] = $notifications['count_unseen'];
		$data['page'] = "lawyer_office_panel/view_customers/".$delete_error;
	   
	   $userRecord = $this -> common -> getOneRecField_Limit("lawyer_office_id", "user","where user_id ='$office_admin_id'");
	   $lawyer_office_id = $userRecord->lawyer_office_id;	
	   $this -> load -> model("lawyer_office_model");
	   $customers_data = $this -> lawyer_office_model -> getCustomers($lawyer_office_id);
	   
	   $i = 0;
	   $this->load->library('../controllers/dates_conversion');
	   if ($customers_data) {
		    foreach($customers_data as $record ) {
				$customers_data[$i]['subscription_start_date'] = $this->dates_conversion->GregorianToHijri($record['subscription_start_date']);
				$customers_data[$i]['subscription_end_date'] = $this->dates_conversion->GregorianToHijri($record['subscription_end_date']);
				$i++;
			}
	   }	   
	   
	   $where = "where user_id =".$office_admin_id ;
	   $result = $this->common->getOneRow('user',$where);
	   $data['profile_first_name']=$result['first_name'];
	   $data['profile_last_name']=$result['last_name'];
	   $data['profile_img']=$result['profile_img'];	
	   
	   $data['delete_error'] = $delete_error;
	   $data['customers'] = $customers_data;
	   $this->load->view('office_admin/view_customers',$data);
	}
	
	function delete_customer($user_id) {
		$this -> load -> model("lawyer_office_model");
		$exist_case = $this->lawyer_office_model->customerHasCase($user_id);
		if ($exist_case) {
			redirect('lawyer_office_panel/view_customers/1');
		} else {
			$this->db->trans_begin();
			
			$where = "customer_id =".$user_id;
			$this->common->deleteRecord('customer_details',$where);
			
			$where = "customer_id =".$user_id;
			$this->common->deleteRecord('cost_per_stage_per_region',$where);
			
			$where = "user_id =".$user_id;
			$this->common->deleteRecord('comment',$where);
			
			$where = "user_id =".$user_id;
			$this->common->deleteRecord('user_activities',$where);
			
			$where = "to_user_id =".$user_id;
			$this->common->deleteRecord('delivered_notifications',$where);
			
			$where = "user_id =".$user_id;
			$this->common->deleteRecord('user',$where);
			
			if ($this->db->trans_status() === FALSE) {
	   			$this->db->trans_rollback();
			} else {
			    $this->db->trans_commit();
			}
			
			redirect('lawyer_office_panel');
		}
		
	}
	
	function add_edit_customer($user_id=0,$email_already_exists=0,$user_name_exists=0) {
		
		$office_admin_id = $this->session->userdata('userid');
	    $userRecord = $this -> common -> getOneRecField_Limit("lawyer_office_id", "user","where user_id ='$office_admin_id'");
	    $lawyer_office_id = $userRecord->lawyer_office_id;	
	    
	     $this -> load -> model("notification_model");
	   	$notifications = $this -> notification_model -> getLatestDeliveredNotificationsForUser($office_admin_id);
	   	$data['notifications_list'] = $notifications['notifications_list'];
		$data['count_unseen'] = $notifications['count_unseen'];
		$data['page'] = "lawyer_office_panel/add_edit_customer/".$user_id."/".$email_already_exists."/".$user_name_exists;
	    
            $where = "where user_id =".$office_admin_id ;
	    $result = $this->common->getOneRow('user',$where);
	    $data['profile_first_name']=$result['first_name'];
	    $data['profile_last_name']=$result['last_name'];
	    $data['profile_img']=$result['profile_img'];
	    
	    $data['user_id']=$user_id;
	    $data['lawyer_office_id']=$lawyer_office_id;
		$data['first_name']="";	
		$data['last_name']="";
	    $data['mobile_number']="";
		$data['email']="";
		$data['user_type_code']="CUSTOMER";
	    $data['user_name']="";
	    $data['password']="";
		$data['customer_type']="";	
		$data['region']="";
	    $data['address']="";
		$data['phone_number']="";
		$data['contact_person_name']="";
	    $data['position']="";
	    $data['subscription_start_date']="";
		$data['subscription_end_date']="";	
		$data['number_of_cases_per_month']="";
	   /* $data['logo']="";
		$data['banner']="";*/
		$data['show_invoice_module']="";
	    $data['show_dashboard']="";
	    $data['email_exists_error']= $email_already_exists;
	    $data['username_exists_error']= $user_name_exists;
	    
	    $data['stages_data'] = "";
	   
		   		
		if(extract($_POST)) {			
			$value['lawyer_office_id']= $lawyer_office_id; 
			$value['first_name']= $_POST['first_name']; 			
			$value['last_name']= $_POST['last_name'];
			$value['mobile_number']= $_POST['mobile_number']; 
			$value['email']= $_POST['email']; 			
			$value['user_type_code']= "CUSTOMER";
			$value['user_name']= $_POST['user_name']; 
			$value['password']= $this->encrypt->encode($_POST['password'] );
			$details_value['customer_id']= $user_id;
			$details_value['customer_type']= $_POST['customer_type']; 
			$details_value['region']= $_POST['region']; 			
			$details_value['address']= $_POST['address'];
			$details_value['phone_number']= $_POST['phone_number']; 
			$details_value['contact_person_name']= $_POST['contact_person_name']; 			
			$details_value['position']= $_POST['position'];
			$details_value['subscription_start_date']= $_POST['subscription_start_date']; 
			$details_value['subscription_end_date']= $_POST['subscription_end_date']; 
			$this->load->library('../controllers/dates_conversion');
			if ($_POST['subscription_start_date'] != NULL && $_POST['subscription_start_date'] != "") {
				$details_value['subscription_start_date']= $this->dates_conversion->HijriToGregorian($_POST['subscription_start_date']); 
			}
			if ($_POST['subscription_end_date'] != NULL && $_POST['subscription_end_date'] != "") {
				$details_value['subscription_end_date']= $this->dates_conversion->HijriToGregorian($_POST['subscription_end_date']); 
			}
			
			$details_value['number_of_cases_per_month']= $_POST['number_of_cases_per_month']; 
			/*$details_value['logo']= $_POST['logo']; 
			$details_value['banner']= $_POST['banner']; 	*/		
			$details_value['show_invoice_module']= $_POST['show_invoice_module'];
			$details_value['show_dashboard']= $_POST['show_dashboard']; 	

			$stages1['region_id']= $_POST['stage_region1']; 
			$stages1['stage_code']= $_POST['stage_code1']; 
			$stages1['cost']= $_POST['stage_cost1']; 
			
			$stages2['region_id']= $_POST['stage_region2']; 
			$stages2['stage_code']= $_POST['stage_code2']; 
			$stages2['cost']= $_POST['stage_cost2']; 
			
			$stages3['region_id']= $_POST['stage_region3']; 
			$stages3['stage_code']= $_POST['stage_code3']; 
			$stages3['cost']= $_POST['stage_cost3']; 
			
			$stages4['region_id']= $_POST['stage_region4']; 
			$stages4['stage_code']= $_POST['stage_code4']; 
			$stages4['cost']= $_POST['stage_cost4']; 
			
			$stages5['region_id']= $_POST['stage_region5']; 
			$stages5['stage_code']= $_POST['stage_code5']; 
			$stages5['cost']= $_POST['stage_cost5']; 
			
			$new_stages_data = array();

	    	$new_stages_data[0] = self::get_new_stages_data($stages1);
   			$new_stages_data[1] = self::get_new_stages_data($stages2);
   			$new_stages_data[2] = self::get_new_stages_data($stages3);
   			$new_stages_data[3] = self::get_new_stages_data($stages4);
   			$new_stages_data[4]= self::get_new_stages_data($stages5);

			if($user_id > 0) {
				$new_email = $value['email'];
				$email_count = $this->common->getCountOfField("user_id","user","where email ='".$new_email."' AND user_id !='".$user_id."'");
				$new_user_name = $value['user_name'];
				$user_name_count = $this->common->getCountOfField("user_id","user","where user_name ='".$new_user_name."' AND user_id !='".$user_id."'");
				if ($email_count == 0 && $user_name_count == 0) {
					$this->db->trans_begin();
					
					$where = "user_id =".$user_id;
					$this->common->updateRecord('user',$value,$where);
					
					$where = "customer_id =".$user_id;
					$this->common->updateRecord('customer_details',$details_value,$where);
					
					self::delete_old_stages_data($user_id);
					
					$stages1['customer_id'] = $user_id;
					self::insertCostStageRegions($stages1);
					
					$stages2['customer_id'] = $user_id;
					self::insertCostStageRegions($stages2);
					
					$stages3['customer_id'] = $user_id;
					self::insertCostStageRegions($stages3);
					
					$stages4['customer_id'] = $user_id;
					self::insertCostStageRegions($stages4);
					
					$stages5['customer_id'] = $user_id;
					self::insertCostStageRegions($stages5);
					
					if ($this->db->trans_status() === FALSE) {
			   			$this->db->trans_rollback();
					} else {
					    $this->db->trans_commit();
					}
					redirect('lawyer_office_panel');
				} else {
					$email_exists = 0;
					if ($email_count != 0) {
						$email_exists=1;
					}
					$user_name_exists = 0;
					if ($user_name_count != 0) {
						$user_name_exists=1;
					}
					self::customer_with_error($user_id,$lawyer_office_id,$value['first_name'],$value['last_name'],$value['mobile_number'],
					$value['email'], $value['user_name'],$value['password'],$details_value['customer_type'], $details_value['region'],
					$details_value['address'],$details_value['phone_number'],$details_value['contact_person_name'],$details_value['position'],
					$_POST['subscription_start_date'],$_POST['subscription_end_date'],$details_value['number_of_cases_per_month'],
					$details_value['show_invoice_module'],$details_value['show_dashboard'],$data['profile_first_name'],$data['profile_last_name'],
					$data['profile_img'],$email_exists, $user_name_exists, $new_stages_data, $data['notifications_list'], $data['count_unseen'], $data['page']);
					
				}
				
				
			} else {
				$new_email = $value['email'];
				$email_count = $this->common->getCountOfField("user_id","user","where email ='".$new_email."'");
				$new_user_name = $value['user_name'];
				$user_name_count = $this->common->getCountOfField("user_id","user","where user_name ='".$new_user_name."'");
				if ($email_count == 0 && $user_name_count == 0) {
					$this->db->trans_begin();
					
					$new_customer_id = $this->common->insertRecord('user',$value);
					
					$stages1['customer_id'] = $new_customer_id;
					self::insertCostStageRegions($stages1);
					
					$stages2['customer_id'] = $new_customer_id;
					self::insertCostStageRegions($stages2);
					
					$stages3['customer_id'] = $new_customer_id;
					self::insertCostStageRegions($stages3);
					
					$stages4['customer_id'] = $new_customer_id;
					self::insertCostStageRegions($stages4);
					
					$stages5['customer_id'] = $new_customer_id;
					self::insertCostStageRegions($stages5);
					
					$details_value['customer_id']=$new_customer_id;
					$this->common->insertRecord('customer_details',$details_value);
					
					if ($this->db->trans_status() === FALSE) {
			   			$this->db->trans_rollback();
					} else {
					    $this->db->trans_commit();
					}
					redirect('lawyer_office_panel');
				} else {
					$email_exists = 0;
					if ($email_count != 0) {
						$email_exists=1;
					}
					$user_name_exists = 0;
					if ($user_name_count != 0) {
						$user_name_exists=1;
					}
					self::customer_with_error($user_id,$lawyer_office_id,$value['first_name'],$value['last_name'],$value['mobile_number'],
					$value['email'], $value['user_name'],$value['password'],$details_value['customer_type'], $details_value['region'],
					$details_value['address'],$details_value['phone_number'],$details_value['contact_person_name'],$details_value['position'],
					$_POST['subscription_start_date'],$_POST['subscription_end_date'],$details_value['number_of_cases_per_month'],
					$details_value['show_invoice_module'],$details_value['show_dashboard'],$data['profile_first_name'],$data['profile_last_name'],
					$data['profile_img'], $email_exists, $user_name_exists, $new_stages_data, $data['notifications_list'], $data['count_unseen'], $data['page']);
				}
				
			}
			
		} else {
			if($user_id > 0) {
				$where = "where user_id =".$user_id;
				$result = $this->common->getOneRow('user',$where);
				$data['lawyer_office_id']=$result['lawyer_office_id'];
		    	$data['first_name']=$result['first_name'];
				$data['last_name']=$result['last_name'];
				$data['mobile_number']=$result['mobile_number'];
		    	$data['email']=$result['email'];
				$data['user_type_code']=$result['user_type_code'];
				$data['user_name']=$result['user_name'];
		    	$data['password']=$this->encrypt->decode($result['password']);
		    	
		    	$where = "where customer_id =".$user_id;
				$details_result = $this->common->getOneRow('customer_details',$where);
				$data['customer_type']=$details_result['customer_type'];
		    	$data['region']=$details_result['region'];
				$data['address']=$details_result['address'];
				$data['phone_number']=$details_result['phone_number'];
		    	$data['contact_person_name']=$details_result['contact_person_name'];
				$data['position']=$details_result['position'];
				$this -> load -> model("lawyer_office_model");
			/*	$dates_conversion = new dates_conversion();	
				$data['subscription_start_date']=$dates_conversion->GregorianToHijri($details_result['subscription_start_date']);
		    	$data['subscription_end_date']=$dates_conversion->GregorianToHijri($details_result['subscription_end_date']);*/
		    	$data['number_of_cases_per_month']=$details_result['number_of_cases_per_month'];
		    /*	$data['logo']=$details_result['logo'];
				$data['banner']=$details_result['banner'];*/
				$data['show_invoice_module']=$details_result['show_invoice_module'];
		    	$data['show_dashboard']=$details_result['show_dashboard'];
		    	
		    	$data['stages_data'] = array();

		    	$data['stages_data'][0] = $this -> lawyer_office_model -> getCostPerStagePerRegionData($user_id, 1);
	   			$data['stages_data'][1] = $this -> lawyer_office_model -> getCostPerStagePerRegionData($user_id, 2);
	   			$data['stages_data'][2] = $this -> lawyer_office_model -> getCostPerStagePerRegionData($user_id, 3);
	   			$data['stages_data'][3] = $this -> lawyer_office_model -> getCostPerStagePerRegionData($user_id, 4);
	   			$data['stages_data'][4]= $this -> lawyer_office_model -> getCostPerStagePerRegionData($user_id, 5);
	   
		    	
		    	$this->load->library('../controllers/dates_conversion');
				$data['subscription_start_date']=$this->dates_conversion->GregorianToHijri($details_result['subscription_start_date']);
		    	$data['subscription_end_date']=$this->dates_conversion->GregorianToHijri($details_result['subscription_end_date']);
	   			
	   			
			}else{
				//check that number of customers accept to add a new one
				$this -> load -> model("lawyer_office_model");
				$lawyerOfficePackage = $this -> lawyer_office_model -> getLawyerOfficePackage($lawyer_office_id);
				$maxNumOfCustomers = $lawyerOfficePackage['num_of_customer'];
				$customers_count = $this -> lawyer_office_model -> getNumOfCustomers($lawyer_office_id);
				
				if ($customers_count>= $maxNumOfCustomers) {
					redirect('lawyer_office_panel/view_customers/2');
				}
			}
			
			$data['available_regions']= $this -> common -> getAllRow("region", "where lawyer_office_id = '" . $lawyer_office_id."'");
			$data['available_customer_types']= $this -> common -> getAllRow("customer_type", "");
			
			$data['ordered_stages']= $this -> common -> getAllRow("stages", " ORDER BY stage_order ASC");
					
			$this->load->view('office_admin/add_edit_customer',$data);
			
		}
		
		
	}
	
	function insertCostStageRegions($stages) {
		$data = array();
		$new_regions = array();
		for ($i=0; $i<count($stages['region_id']); $i++) {
			if ($stages['cost'][$i] != "" && !in_array($stages['region_id'][$i], $new_regions)) {
				$data['region_id'] = $stages['region_id'][$i];
				$data['stage_code']= $stages['stage_code']; 
				$data['cost']= $stages['cost'][$i]; 
				$data['customer_id'] = $stages['customer_id'];
			//	echo $data['region_id'].",".$data['stage_code'].",".$data['cost'].",".$data['customer_id']."#";
				$data_id = $this->common->insertRecord('cost_per_stage_per_region',$data);
				array_push($new_regions, $data['region_id']);
			}
		}
	}
	
	function get_new_stages_data($stages) {
		
		$new_stages_data = array();
		for ($i=0; $i<count($stages['region_id']); $i++) {
			$data = array();
			$data['region_id'] = $stages['region_id'][$i];
			$data['stage_code']= $stages['stage_code']; 
			$data['cost']= $stages['cost'][$i]; 
			//$data['region_name'] = $stages['customer_id'];
			array_push($new_stages_data, $data);
		}
		return $new_stages_data;
	}
	
	function update_customer_cost_stage_region($old_id, $user_id, &$stages, &$tobe_deleted_ids) {
		$stages['customer_id'] = $user_id;
		if ($old_id != NULL && $old_id > 0) {
			array_push($tobe_deleted_ids, $old_id);
		}		
		$new_id = $this->common->insertRecord('cost_per_stage_per_region',$stages);
		return $new_id;
	}
	
	function delete_old_cost_stage_regions ($tobe_deleted_ids){
		if (sizeof($tobe_deleted_ids) > 0) {
			foreach($tobe_deleted_ids as $old_id) {
				$where = "id =".$old_id;
				$this->common->deleteRecord('cost_per_stage_per_region',$where);
			}
		}
	}
	
	function delete_old_stages_data ($customer_id){
		$where = "customer_id =".$customer_id;
		$this->common->deleteRecord('cost_per_stage_per_region',$where);
	}
	
	function customer_with_error($user_id,$lawyer_office_id,$first_name,$last_name,$mobile_number,$email,$user_name,$password,
					$customer_type,$region,$address,$phone_number,$contact_person_name,$position,$subscription_start_date,
					$subscription_end_date,$number_of_cases_per_month,$show_invoice_module,$show_dashboard,$profile_first_name,
					$profile_last_name,$profile_img, $email_exists, $user_name_exists, $new_stages_data,
					$notifications_list, $count_unseen, $page) {
		
		$data['profile_first_name']=$profile_first_name;
	    $data['profile_last_name']=$profile_last_name;
	    $data['profile_img']=$profile_img;
	    
	    $data['notifications_list'] = $notifications_list;
	    $data['count_unseen'] = $count_unseen;
	    $data['page'] = $page;
	    
	    $data['user_id']=$user_id;
	    $data['lawyer_office_id']=$lawyer_office_id;
		$data['first_name']=$first_name;	
		$data['last_name']=$last_name;
	    $data['mobile_number']=$mobile_number;
		$data['email']=$email;
		$data['user_type_code']="CUSTOMER";
	    $data['user_name']=$user_name;
	    $data['password']=$password;
		$data['customer_type']=$customer_type;	
		$data['region']=$region;
	    $data['address']=$address;
		$data['phone_number']=$phone_number;
		$data['contact_person_name']=$contact_person_name;
	    $data['position']=$position;
	    $data['subscription_start_date']=$subscription_start_date;
		$data['subscription_end_date']=$subscription_end_date;	
		$data['number_of_cases_per_month']=$number_of_cases_per_month;
	  	$data['show_invoice_module']=$show_invoice_module;
	    $data['show_dashboard']=$show_dashboard;
	    $data['email_exists_error']= $email_exists;
		$data['username_exists_error']= $user_name_exists;
		
		$data['available_regions']= $this -> common -> getAllRow("region", "where lawyer_office_id = '" . $lawyer_office_id."'");
		$data['available_customer_types']= $this -> common -> getAllRow("customer_type", "");
		$data['ordered_stages']= $this -> common -> getAllRow("stages", " ORDER BY stage_order ASC");
		$data['available_regions']= $this -> common -> getAllRow("region",  "where lawyer_office_id = '" . $lawyer_office_id."'");
		$data['stages_data']= $new_stages_data;
				
		$this->load->view('office_admin/add_edit_customer',$data);	
	}	
}
