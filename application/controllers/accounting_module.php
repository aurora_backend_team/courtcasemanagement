<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class accounting_module extends CI_Controller
{
	function __construct()
    {
        parent::__construct();
        $this -> load -> model("common");
		if($this->session->userdata('userid')=='' || $this->session->userdata('user_type')!='ADMIN') {
                        //echo 'user data  is null';
			redirect('user_login','refresh');
		}
    }
    
	function index() {
	  
	   self::get_customers_and_lawyers();
	   
	}
	
	
	function get_customers_and_lawyers() {
	   $office_admin_id = $this->session->userdata('userid');
	   $userRecord = $this -> common -> getOneRecField_Limit("lawyer_office_id", "user","where user_id ='$office_admin_id'");
	   $lawyer_office_id = $userRecord->lawyer_office_id;	
	   $this -> load -> model("invoices_model");
	   $data['customers'] = $this -> invoices_model -> getCustomers($lawyer_office_id);
	   $data['lawyers'] = $this -> invoices_model -> getLawyers($lawyer_office_id);
	   
	   $this -> load -> model("notification_model");
	   	$notifications = $this -> notification_model -> getLatestDeliveredNotificationsForUser($office_admin_id);
	   	$data['notifications_list'] = $notifications['notifications_list'];
		$data['count_unseen'] = $notifications['count_unseen'];
		$data['page'] = "accounting_module/get_customers_and_lawyers";
	   
	   
	   $where = "where user_id =".$office_admin_id ;
	   $result = $this->common->getOneRow('user',$where);
	   $data['profile_first_name']=$result['first_name'];
	   $data['profile_last_name']=$result['last_name'];
	   $data['profile_img']=$result['profile_img'];	
	   
	   $this->load->view('accounting/view_customers_lawyers',$data);
	}
	
  
}
