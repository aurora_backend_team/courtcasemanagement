<?php
class notification_model extends CI_Model {
	function getNotifications() {
		$conditionsString = "notifications.notification_code not in ('M008', 'M009', 'M010')";
		$this->db->select ( 'notifications.notification_code, notifications.notification_name, notifications.send_time, notifications.active,
							 notifications.sms_active, state.state_code, state.state_name, user_type.user_type_code, user_type.user_type_name' );
		$this->db->from ( 'notifications' );
		$this->db->join ( 'state', 'state.state_code = notifications.case_state' );
		$this->db->join ( 'user_type', 'user_type.user_type_code = notifications.receiver' );
		$this->db->order_by ( "notification_code" );
		$this->db->where($conditionsString);
		$query = $this->db->get ();
		
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			return $records;
		} else {
			return false;
		}
	}
	function getNotificationData($notification_code) {
		$conditionsString = "notifications.notification_code = '" . $notification_code . "'";
		$this->db->select ( 'notifications.notification_code, notifications.notification_name, notifications.send_time, notifications.active,
							 notifications.sms_active, state.state_code, state.state_name, user_type.user_type_code, user_type.user_type_name' );
		$this->db->from ( 'notifications' );
		$this->db->join ( 'state', 'state.state_code = notifications.case_state' );
		$this->db->join ( 'user_type', 'user_type.user_type_code = notifications.receiver' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			return $records [0];
		} else {
			return false;
		}
	}
	function getLastSuccessfulTransForCase($case_id, $lawyer_office_id) {
		$conditionsString = "case_transactions.case_id = '" . $case_id . "' AND case_transactions.lawyer_office_id = " . $lawyer_office_id . " AND case_transactions.transaction_status = 'Success' AND edit_action = 'N' AND case_transactions.action_code not in ('SUSPEND_REQUEST', 'RESUME_REQUEST')";
		$this->db->select ( 'case_transactions.transaction_id, case_transactions.case_id, case.customer_id, case.creation_date, case.last_update_date,
						case.current_state_code, case.closing_reason, case_transactions.trans_date_time, user.mobile_number as customer_mobile,
						user.last_name as customer_last_name, user.first_name as customer_first_name, user.email as customer_email,
						region.name as region_name, lawyer_case.lawyer_id, user.lawyer_office_id, user.gcm_id as customer_gcm_id,
						case_transactions.action_code' );
		$this->db->from ( 'case_transactions' );
		$this->db->join ( 'case', 'case.case_id = case_transactions.case_id and case.lawyer_office_id = case_transactions.lawyer_office_id' );
		$this->db->join ( 'user', 'user.user_id = case.customer_id' );
		$this->db->join ( 'lawyer_case', 'lawyer_case.case_id = case_transactions.case_id and lawyer_case.lawyer_office_id = case_transactions.lawyer_office_id', 'left' );
		$this->db->join ( 'lawyer_details', 'lawyer_case.lawyer_id = lawyer_details.lawyer_id', 'left' );
		$this->db->join ( 'region', 'region.region_id = lawyer_details.region_id', 'left' );
		$this->db->where ( $conditionsString );
		$this->db->order_by ( "trans_date_time", "desc" );
		$query = $this->db->get ();
		
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			$notification_data = array ();
			$notification_data ['transaction_id'] = $records [0] ['transaction_id'];
			$notification_data ['case_id'] = $records [0] ['case_id'];
			$notification_data ['creation_date'] = $records [0] ['creation_date'];
			$notification_data ['last_update_date'] = $records [0] ['last_update_date'];
			$notification_data ['current_state_code'] = $records [0] ['current_state_code'];
			$notification_data ['action_code'] = $records [0] ['action_code'];
			if ($records [0] ['action_code'] != null && $records [0] ['action_code'] == "REASSIGN_TO_LAWYER") {
				$notification_data ['current_state_code'] = "ASSIGNED_TO_LAWYER";
			}
			$notification_data ['closing_reason'] = $records [0] ['closing_reason'];
			$notification_data ['trans_date_time'] = $records [0] ['trans_date_time'];
			$notification_data ['customer_id'] = $records [0] ['customer_id'];
			$notification_data ['customer_first_name'] = $records [0] ['customer_first_name'];
			$notification_data ['customer_last_name'] = $records [0] ['customer_last_name'];
			$notification_data ['customer_email'] = $records [0] ['customer_email'];
			$notification_data ['customer_mobile'] = $records [0] ['customer_mobile'];
			$notification_data ['region_name'] = $records [0] ['region_name'];
			$notification_data ['customer_gcm_id'] = $records [0] ['customer_gcm_id'];
			$notification_data ['lawyer_id'] = $records [0] ['lawyer_id'];
			$notification_data ['lawyer_office_id'] = $records [0] ['lawyer_office_id'];
			
			$lawyer_id = $records [0] ['lawyer_id'];
			$conditionsString = "user.user_id = '" . $lawyer_id . "'";
			$this->db->select ( 'user.email as lawyer_email, user.first_name as lawyer_first_name, user.last_name as lawyer_last_name,
								user.gcm_id as lawyer_gcm_id, user.mobile_number as lawyer_mobile' );
			$this->db->from ( 'user' );
			$this->db->where ( $conditionsString );
			$query = $this->db->get ();
			if ($query->num_rows () > 0) {
				$lawyer_records = $query->result_array ();
				$notification_data ['lawyer_email'] = $lawyer_records [0] ['lawyer_email'];
				$notification_data ['lawyer_first_name'] = $lawyer_records [0] ['lawyer_first_name'];
				$notification_data ['lawyer_last_name'] = $lawyer_records [0] ['lawyer_last_name'];
				$notification_data ['lawyer_gcm_id'] = $lawyer_records [0] ['lawyer_gcm_id'];
				$notification_data ['lawyer_mobile'] = $lawyer_records [0] ['lawyer_mobile'];
			}
			
			$notification_data ['secretaries_emails'] = false;
			$lawyer_office_id = $records [0] ['lawyer_office_id'];
			$conditionsString = "user.lawyer_office_id = '" . $lawyer_office_id . "' AND user.user_type_code = 'SECRETARY'";
			$this->db->select ( 'user.email as secretary_email, user.user_id as secretary_id, user.first_name as secretary_first_name,
								 user.last_name as secretary_last_name, user.gcm_id as secretary_gcm_id, user.mobile_number as secretary_mobile' );
			$this->db->from ( 'user' );
			$this->db->where ( $conditionsString );
			$query = $this->db->get ();
			if ($query->num_rows () > 0) {
				$secretaries_records = $query->result_array ();
				$notification_data ['secretaries_emails'] = $secretaries_records;
			}
			
			return $notification_data;
		} else {
			return false;
		}
	}
	function getAllCaseIds() {
		$this->db->select ( 'case_id,lawyer_office_id' );
		$this->db->from ( 'case' );
		$query = $this->db->get ();
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			return $records;
		} else {
			false;
		}
	}
	function getDeliveredNotificationId() {
		$this->db->select ( 'email_id' );
		$this->db->from ( 'delivered_notifications' );
		$this->db->order_by ( 'email_id', 'asc' );
		$query = $this->db->get ();
		$tran_num = 0;
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			foreach ( $records as $record ) {
				if (strlen ( $record ['email_id'] ) == 8) {
					$tran_num = substr ( $record ['email_id'], 1, 7 );
					$tran_num ++;
				}
			}
		}
		$tran_num = sprintf ( "%'.07d", $tran_num );
		return "E" . $tran_num;
	}
	function getLatestDeliveredNotificationsForUser($user_id) {
		$conditionsString = "delivered_notifications.to_user_id = '" . $user_id . "' AND delivered_notifications.message != ''";
		$this->db->select ( 'delivered_notifications.email_id, delivered_notifications.notification_type_code, delivered_notifications.case_id,
						 delivered_notifications.sent_date_time, delivered_notifications.waiting_days, delivered_notifications.seen,
						 delivered_notifications.message' );
		$this->db->from ( 'delivered_notifications' );
		$this->db->where ( $conditionsString );
		$this->db->order_by ( "email_id", "desc" );
		$query = $this->db->get ();
		
		$data = array ();
		$notifications = array ();
		$count_unseen = 0;
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			$i = 0;
			foreach ( $records as $record ) {
				if ($record ['seen'] == 'Y' && $i == 10) {
					break;
				} else {
					$notifications [$i] ['email_id'] = $record ['email_id'];
					$notifications [$i] ['notification_type_code'] = $record ['notification_type_code'];
					$notifications [$i] ['case_id'] = $record ['case_id'];
					$notifications [$i] ['sent_date_time'] = self::getTimeDiff ( $record ['sent_date_time'] );
					$notifications [$i] ['waiting_days'] = $record ['waiting_days'];
					$notifications [$i] ['seen'] = $record ['seen'];
					$notifications [$i] ['message'] = $record ['message'];
					if ($record ['seen'] == 'N') {
						$count_unseen ++;
					}
				}
				$i ++;
			}
		}
		$data ['notifications_list'] = $notifications;
		$data ['count_unseen'] = $count_unseen;
		return $data;
	}
	function getUnseenNotificationsForUser($user_id) {
		$conditionsString = "delivered_notifications.to_user_id = '" . $user_id . "' AND delivered_notifications.message != '' AND delivered_notifications.seen = 'N'";
		$this->db->select ( 'delivered_notifications.email_id, delivered_notifications.notification_type_code, delivered_notifications.case_id,
						 delivered_notifications.sent_date_time, delivered_notifications.waiting_days, delivered_notifications.seen,
						 delivered_notifications.message' );
		$this->db->from ( 'delivered_notifications' );
		$this->db->where ( $conditionsString );
		$this->db->order_by ( "email_id", "desc" );
		$query = $this->db->get ();
		
		$data = array ();
		$notifications = array ();
		$count_unseen = 0;
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			$i = 0;
			foreach ( $records as $record ) {
				$notifications [$i] ['email_id'] = $record ['email_id'];
				$notifications [$i] ['notification_type_code'] = $record ['notification_type_code'];
				$notifications [$i] ['case_id'] = $record ['case_id'];
				$notifications [$i] ['sent_date_time'] = self::getTimeDiff ( $record ['sent_date_time'] );
				$notifications [$i] ['waiting_days'] = $record ['waiting_days'];
				$notifications [$i] ['seen'] = $record ['seen'];
				$notifications [$i] ['message'] = $record ['message'];
				if ($record ['seen'] == 'N') {
					$count_unseen ++;
				}
				$i ++;
			}
		}
		$data ['notifications_list'] = $notifications;
		$data ['count_unseen'] = $count_unseen;
		return $data;
	}
	function getTimeDiff($dateString) {
		date_default_timezone_set ( 'Asia/Riyadh' );
		$date = new DateTime ( $dateString );
		$diff = self::formatDateDiff ( $date );
		return $diff;
	}
	
	/**
	 * A sweet interval formatting, will use the two biggest interval parts.
	 * On small intervals, you get minutes and seconds.
	 * On big intervals, you get months and days.
	 * Only the two biggest parts are used.
	 *
	 * @param DateTime $start        	
	 * @param DateTime|null $end        	
	 * @return string
	 */
	function formatDateDiff($start, $end = null) {
		date_default_timezone_set ( 'Asia/Riyadh' );
		if (! ($start instanceof DateTime)) {
			$start = new DateTime ( $start );
		}
		
		if ($end === null) {
			$end = new DateTime ();
		}
		
		if (! ($end instanceof DateTime)) {
			$end = new DateTime ( $start );
		}
		// $end->setTimezone(new DateTimeZone('Asia/Riyadh'));
		$interval = $end->diff ( $start );
		// echo $end->format('Y-m-d H:i:s').' '.$start->format('Y-m-d H:i:s').' '.($end->format('Y-m-d H:i:s') - $start->format('Y-m-d H:i:s'));
		
		$getTimeString = function ($nb, $str, $value, $from_str) {
			
			$doPlural = function ($nb, $str) {
				if ($nb == 0) {
					return "الان";
				} else if ($nb == 1 || $nb > 10) {
					return $str;
				} else if ($nb == 2) {
					switch ($str) {
						case "سنة" :
							return "سنتان";
							break;
						case "شهر" :
							return "شهران";
							break;
						case "يوم" :
							return "يومان";
							break;
						case "ساعة" :
							return "ساعتان";
							break;
						case "دقيقة" :
							return "دقيقتان";
							break;
						case "ثانية" :
							return "ثانيتان";
							break;
						default :
							return "ثانيتان";
					}
				} else if ($nb >= 3 && $nb <= 10) {
					switch ($str) {
						case "سنة" :
							return "سنين";
							break;
						case "شهر" :
							return "شهور";
							break;
						case "يوم" :
							return "أيام";
							break;
						case "ساعة" :
							return "ساعات";
							break;
						case "دقيقة" :
							return "دقائق";
							break;
						case "ثانية" :
							return "ثوانى";
							break;
						default :
							return "ثوانى";
					}
				} else {
					return $str;
				}
			}; // adds plurals
			
			if ($nb == 0) {
				return $doPlural ( $nb, $str );
			} else if ($nb == 1) {
				return $from_str . $doPlural ( $nb, $str );
			} else if ($nb > 10) {
				return $from_str . $value . $doPlural ( $nb, $str );
			} else if ($nb == 2) {
				return $from_str . $doPlural ( $nb, $str );
			} else if ($nb >= 3 && $nb <= 10) {
				return $from_str . $value . $doPlural ( $nb, $str );
			}
		}; // getTimeString
		
		$format = array ();
		if ($interval->y !== 0) {
			$format [] = $getTimeString ( $interval->y, "سنة", "%y ", "منذ " );
		} else if ($interval->m !== 0) {
			$format [] = $getTimeString ( $interval->m, "شهر", "%m ", "منذ " );
		} else if ($interval->d !== 0) {
			$format [] = $getTimeString ( $interval->d, "يوم", "%d ", "منذ " );
		} else if ($interval->h !== 0) {
			$format [] = $getTimeString ( $interval->h, "ساعة", "%h ", "منذ " );
		} else if ($interval->i !== 0) {
			$format [] = $getTimeString ( $interval->i, "دقيقة", "%i ", "منذ " );
		} else if ($interval->s !== 0) {
			/*
			 * if(!count($format)) {
			 * return "منذ أقل من دقيقة";
			 * } else {
			 * $format[] = "%s ".$doPlural($interval->s, "ثانية");
			 * }
			 */
			$format [] = $getTimeString ( $interval->s, "ثانية", "%s ", "منذ " );
		} else {
			$format [] = $getTimeString ( $interval->s, "ثانية", "%s ", "منذ " );
		}
		
		// We use the two biggest parts
		if (count ( $format ) > 1) {
			$format = array_shift ( $format ) . " و " . array_shift ( $format );
		} else {
			$format = array_pop ( $format );
		}
		$format;
		// Prepend 'since ' or whatever you like
		return $interval->format ( $format );
	}
	function getLastDeliveredNotificationForCase($last_trans_id, $notif_code) {
		$conditionsString = "delivered_notifications.trans_id = '" . $last_trans_id . "'";
		$conditionsString = $conditionsString . " AND " . "delivered_notifications.notification_type_code = '" . $notif_code . "'";
		$this->db->select ( 'delivered_notifications.case_id as case_id, delivered_notifications.sent_date_time as sent_date_time,
						delivered_notifications.trans_id as trans_id, case.current_state_code as case_state, 
						case_transactions.current_state_code as trans_state' );
		$this->db->from ( 'delivered_notifications' );
		$this->db->join ( 'case', 'delivered_notifications.case_id = case.case_id' );
		$this->db->join ( 'case_transactions', 'delivered_notifications.trans_id = case_transactions.transaction_id' );
		$this->db->where ( $conditionsString );
		$this->db->order_by ( "sent_date_time", "desc" );
		$query = $this->db->get ();
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			return $records [0];
		} else {
			return false;
		}
	}
	function get_case_request_notification_data($case_request_id) {
		$conditionsString = "case_requests.case_request_id = $case_request_id";
		$this->db->select ( 'case_requests.case_request_id, case_requests.case_id, case_requests.lawyer_office_id, case_requests.suspend_reason_code,
						case_suspend_reason.suspend_reason, case.customer_id, case_requests.request_date, case_requests.request_type,
						user.last_name as customer_last_name, user.first_name as customer_first_name,
						lawyer_case.lawyer_id, lawyer_office.email as admin_email, lawyer_office.mobile_number as admin_mobile' );
		$this->db->from ( 'case_requests' );
		$this->db->join ( 'case', 'case.case_id = case_requests.case_id and case.lawyer_office_id = case_requests.lawyer_office_id' );
		$this->db->join ( 'user', 'user.user_id = case.customer_id' );
		$this->db->join ( 'lawyer_office', 'lawyer_office.lawyer_office_id = case.lawyer_office_id' );
		$this->db->join ( 'lawyer_case', 'lawyer_case.case_id = case_requests.case_id and lawyer_case.lawyer_office_id = case_requests.lawyer_office_id', 'left' );
		$this->db->join ( 'case_suspend_reason', 'case_suspend_reason.suspend_code = case_requests.suspend_reason_code', 'left' );
		$this->db->where ( $conditionsString );
		// $this->db->order_by ( "request_date", "desc" );
		
		$query = $this->db->get ();
		
		$notification_data = false;
		if($query)
		if ($query && $query->num_rows () > 0) {
			$records = $query->result_array ();
			
			$notification_data ['records'] = $records [0];
			$lawyer_office_id = $records [0] ['lawyer_office_id'];
			// admin (notif, email)
			$conditionsString = "user.lawyer_office_id = $lawyer_office_id and user.user_type_code = 'ADMIN'";
			$this->db->select ( 'user.user_id, user.first_name as admin_first_name, user.last_name as admin_last_name,
								user.gcm_id as admin_gcm_id' );
			$this->db->from ( 'user' );
			$this->db->where ( $conditionsString );
			$query = $this->db->get ();
			if ($query->num_rows () > 0) {
				$admin_records = $query->result_array ();
				$notification_data ['admin_record'] = $admin_records [0];
			}
			// lawyer (notif, email, mobile)
			$lawyer_id = $records [0] ['lawyer_id'];
			$conditionsString = "user.user_id = '" . $lawyer_id . "'";
			$this->db->select ( 'user.user_id, user.email as lawyer_email, user.first_name as lawyer_first_name, user.last_name as lawyer_last_name,
								user.gcm_id as lawyer_gcm_id, user.mobile_number as lawyer_mobile' );
			$this->db->from ( 'user' );
			$this->db->where ( $conditionsString );
			$notification_data ['lawyer_record'] = false;
			$query = $this->db->get ();
			if ($query->num_rows () > 0) {
				$lawyer_records = $query->result_array ();
				$notification_data ['lawyer_record'] = $lawyer_records [0];
			}
			// secretaries
			// $notification_data ['secretaries_emails'] = false;
			$conditionsString = "user.lawyer_office_id = '" . $lawyer_office_id . "' AND user.user_type_code = 'SECRETARY'";
			$this->db->select ( 'user.email as secretary_email, user.user_id as secretary_id, user.first_name as secretary_first_name,
								 user.last_name as secretary_last_name, user.gcm_id as secretary_gcm_id, user.mobile_number as secretary_mobile' );
			$this->db->from ( 'user' );
			$this->db->where ( $conditionsString );
			$query = $this->db->get ();
			if ($query->num_rows () > 0) {
				$secretaries_records = $query->result_array ();
				$notification_data ['secretaries_emails'] = $secretaries_records;
			}
		}
		return $notification_data;
	}
	function getAllUnDoneRequests() {
		$conditionsString = "case_requests.request_done = 'N'";
		$this->db->select ( 'case_requests.case_request_id,case_requests.trans_id, case_requests.request_date' );
		$this->db->from ( 'case_requests' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		
		$undoneRequests = false;
		if ($query->num_rows () > 0) {
			$undoneRequests = $query->result_array ();
		}
		return $undoneRequests;
	}
}

?>