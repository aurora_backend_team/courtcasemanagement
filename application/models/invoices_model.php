<?php
class invoices_model extends CI_Model
{

	function getCustomers($lawyer_office_id) {
		$conditionsString = "user.lawyer_office_id = '".$lawyer_office_id."' AND user_type_code = 'CUSTOMER'";
		$this->db->select('user.lawyer_office_id, user.user_id, user.first_name, user.last_name');
		$this->db->from('user');
		$this->db->where($conditionsString);
		$query = $this->db->get();
		
		if($query->num_rows()>0) {
			$records = $query->result_array();
			return $records;
		} else {
			return false;
		}
	}

	function getLawyers($lawyer_office_id) {
		$conditionsString = "user.lawyer_office_id = '".$lawyer_office_id."' AND user_type_code = 'LAWYER'";
		$this->db->select('user.lawyer_office_id as user_lawyer_office_id, user.user_id, user.first_name, user.last_name');
		$this->db->from('user');
		$this->db->where($conditionsString);
		$query = $this->db->get();
		
		if($query->num_rows()>0) {
			$records = $query->result_array();
			$i = 0;
			$lawyers_data = array();
			foreach($records as $record ) {
				   $lawyers_data[$i]['lawyer_office_id'] = $record['user_lawyer_office_id'];
	               $lawyers_data[$i]['user_id'] = $record['user_id'];
	               $lawyers_data[$i]['first_name'] = $record['first_name'];
	               $lawyers_data[$i]['last_name'] = $record['last_name'];
	               $i = $i + 1;
	        }
			return $lawyers_data;
		} else {
			return false;
		}
	}
	
	function getUserInvoices($user_id) {
		$conditionsString = "invoices.invoice_user_id = '".$user_id."'";
		$this->db->select('invoices.invoice_id, invoices.invoice_date, invoices.case_id, invoices.case_state_code, invoices.billing_value,
						 invoices.is_paid, invoices.billing_file, invoices.customer_id, invoices.lawyer_id, state.state_name,
						  user.last_name as customer_last_name,	user.first_name as customer_first_name');
		$this->db->from('invoices');
		$this->db->join('state', 'state.state_code = invoices.case_state_code');
		$this->db->join('user', 'user.user_id = invoices.customer_id');
		$this->db->where($conditionsString);
		$query = $this->db->get();
		
		if($query->num_rows()>0) {
			$records = $query->result_array();
			$i = 0;
			$invoices_data = array();
			foreach($records as $record ) {
				   $invoices_data[$i]['invoice_id'] = $record['invoice_id'];
	               $invoices_data[$i]['invoice_date'] = $record['invoice_date'];
	               $invoices_data[$i]['customer_first_name'] = $record['customer_first_name'];
	               $invoices_data[$i]['customer_last_name'] = $record['customer_last_name'];
	               
	               $lawyer_id = $record['lawyer_id'];
	               $conditionsString = "user.user_id = '".$lawyer_id."'";
				   $this->db->select('user.last_name as lawyer_last_name,	user.first_name as lawyer_first_name');
				   $this->db->from('user');
				   $this->db->where($conditionsString);
				   $query = $this->db->get();
		
				   if($query->num_rows()>0) {
						$lawyer_records = $query->result_array();
						$invoices_data[$i]['lawyer_first_name'] = $lawyer_records[0]['lawyer_first_name'];
						$invoices_data[$i]['lawyer_last_name'] = $lawyer_records[0]['lawyer_last_name'];
				   }
				   
	               $invoices_data[$i]['case_id'] = $record['case_id'];
	               $invoices_data[$i]['state_name'] = $record['state_name'];
	               $invoices_data[$i]['case_state_code'] = $record['case_state_code'];
	               $invoices_data[$i]['billing_value'] = $record['billing_value'];
	               $invoices_data[$i]['is_paid'] = $record['is_paid'];
	               $invoices_data[$i]['billing_file'] = $record['billing_file'];
	               
	               $i = $i + 1;
	        }
			return $invoices_data;
		} else {
			return false;
		}
	}
	
	function getInvoiceData($invoice_id) {
		$conditionsString = "invoices.invoice_id = '".$invoice_id."'";
		$this->db->select('invoices.invoice_id, invoices.invoice_date, invoices.case_id, invoices.case_state_code, invoices.billing_value,
						 invoices.is_paid, invoices.billing_file, invoices.customer_id, invoices.lawyer_id, state.state_name,
						  user.last_name as customer_last_name,	user.first_name as customer_first_name, invoices.invoice_user_id');
		$this->db->from('invoices');
		$this->db->join('state', 'state.state_code = invoices.case_state_code');
		$this->db->join('user', 'user.user_id = invoices.customer_id');
		$this->db->where($conditionsString);
		$query = $this->db->get();
		
		if($query->num_rows()>0) {
			$records = $query->result_array();
			$invoices_data = array();
			$invoices_data['invoice_id'] = $records[0]['invoice_id'];
            $invoices_data['invoice_date'] = $records[0]['invoice_date'];
            $invoices_data['customer_first_name'] = $records[0]['customer_first_name'];
            $invoices_data['customer_last_name'] = $records[0]['customer_last_name'];
               
            $lawyer_id = $records[0]['lawyer_id'];
            $conditionsString = "user.user_id = '".$lawyer_id."'";
			$this->db->select('user.last_name as lawyer_last_name,	user.first_name as lawyer_first_name');
			$this->db->from('user');
			$this->db->where($conditionsString);
			$query = $this->db->get();
	
			if($query->num_rows()>0) {
				$lawyer_records = $query->result_array();
				$invoices_data['lawyer_first_name'] = $lawyer_records[0]['lawyer_first_name'];
				$invoices_data['lawyer_last_name'] = $lawyer_records[0]['lawyer_last_name'];
			}
			   
            $invoices_data['case_id'] = $records[0]['case_id'];
            $invoices_data['state_name'] = $records[0]['state_name'];
            $invoices_data['case_state_code'] = $records[0]['case_state_code'];
            $invoices_data['billing_value'] = $records[0]['billing_value'];
            $invoices_data['is_paid'] = $records[0]['is_paid'];
            $invoices_data['billing_file'] = $records[0]['billing_file'];
            $invoices_data['invoice_user_id'] = $records[0]['invoice_user_id'];
			
			return $invoices_data;
		} else {
			return false;
		}
	}
	
	function getInvoiceId() {
		$this->db->select('invoice_id');
		$this->db->from('invoices');
		$this->db->order_by('invoice_id', 'asc');
		$query = $this->db->get();
		$tran_num = 0;
		if($query->num_rows()>0) {
			$records = $query->result_array();
			foreach($records as $record ) {
			   if (strlen($record['invoice_id']) == 10) {
			  	 $tran_num = substr($record['invoice_id'],3,7);
			  	 $tran_num++;
			   }			   
			}			
		}
		$tran_num = sprintf("%'.07d", $tran_num);
		return "INV".$tran_num;
	}
	
	
	function getLawyerCost($stage_order, $lawyer_id) {
		$conditionsString = "lawyer_id = '".$lawyer_id."'";
		$this->db->select('stage1_cost, stage2_cost, stage3_cost, stage4_cost, stage5_cost');
		$this->db->from('lawyer_details');
		$this->db->where($conditionsString);
		$query = $this->db->get();
		$lawyer_cost = 0;
		if($query->num_rows()>0) {
			$records = $query->result_array();
			$stage_cost_name = 'stage'.$stage_order.'_cost';
			$lawyer_cost = $records[0][$stage_cost_name];
		} 
		return $lawyer_cost;
	}
	
	function getCustomerCost($stage_code, $region_id, $customer_id) {
		$conditionsString = "cost_per_stage_per_region.customer_id = '".$customer_id."' AND cost_per_stage_per_region.stage_code = '".$stage_code."' AND cost_per_stage_per_region.region_id = '".$region_id."'";
		$this->db->select('cost_per_stage_per_region.cost');
		$this->db->from('cost_per_stage_per_region');
		$this->db->where($conditionsString);
		$query = $this->db->get();
		$customer_cost = 0;
		if($query->num_rows()>0) {
			$records = $query->result_array();
			$customer_cost = $records[0]['cost'];
		}
		return $customer_cost;
	}
	
}
?>