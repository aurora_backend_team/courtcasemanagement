<?php
class help_model extends CI_Model
{
	
	function getAllCategories() {
		$this->db->select('FAQ_Category.category_id, FAQ_Category.category_descAr, FAQ_Category.category_descEn');
		$this->db->from('FAQ_Category');
		$query = $this->db->get();
		if($query->num_rows()>0) {
			$records = $query->result_array();
			return $records;
		} else {
			return false;
		}
	}
	
	
	function getAllFrequentlyAskedQuestions() {
		$this->db->select('FAQs_help.id, FAQs_help.category_id as category_id, FAQs_help.question, FAQs_help.answer,
			FAQ_Category.category_descAr, FAQ_Category.category_descEn');
		$this->db->from('FAQs_help');
		$this->db->join('FAQ_Category', 'FAQs_help.category_id = FAQ_Category.category_id');
		$this->db->order_by('FAQs_help.category_id,FAQs_help.order', 'asc');
		$query = $this->db->get();
		
		if($query->num_rows()>0) {
			$records = $query->result_array();
			return $records;
		} else {
			return false;
		}
	}
	
	function getFrequentlyAskedQuestions($category_id) {
		$conditionsString = "FAQs_help.category_id = '".$category_id."'";
		$this->db->select('FAQs_help.id, FAQs_help.category_id as category_id, FAQs_help.question, FAQs_help.answer,
			FAQ_Category.category_descAr, FAQ_Category.category_descEn');
		$this->db->from('FAQs_help');
		$this->db->join('FAQ_Category', 'FAQs_help.category_id = FAQ_Category.category_id');
		$this->db->order_by('FAQs_help.category_id,FAQs_help.order', 'asc');
		$this->db->where($conditionsString);
		$query = $this->db->get();
		if($query->num_rows()>0) {
			$records = $query->result_array();
			return $records;
		} else {
			return false;
		}
	}
	
	function getFrequentlyAskedQuestionsWithFilter($category_id, $searchStr) {
		$select = 'FAQs_help.id, FAQs_help.category_id as category_id, FAQs_help.question, FAQs_help.answer,
			FAQ_Category.category_descAr, FAQ_Category.category_descEn';
		$table = 'FAQs_help join FAQ_Category on FAQs_help.category_id = FAQ_Category.category_id';
		$where= "FAQs_help.category_id = '".$category_id."'";
		if ($searchStr!= null && $searchStr!= "") {
			$where = $where. " AND ( FAQs_help.question LIKE '%".$searchStr."%' OR FAQs_help.answer LIKE '%".$searchStr."%' )";
		}
		$q = "select $select from $table where $where";
		$query=$this->db->query($q);
		if($query->num_rows()>0) {
			$records = $query->result_array();
			return $records;
		} else {
			return false;
		}
		
	}
	
}
?>