<?php
class dashboard_model extends CI_Model {
    
    function getlawyerOfficeCases($lawyer_office_id) {
        try {
            if ($lawyer_office_id != null && $lawyer_office_id != '') {
                $conditionsString = "case.lawyer_office_id = '" . $lawyer_office_id . "'";
            }
            $this->db->select ( 'case.case_id, case.creation_date, case.last_update_date, case.current_state_code, case.executive_order_date, case.advertisement_date, case.decision_46_date, case.decision_34_date' );
            $this->db->from ( 'case' );
            if ($lawyer_office_id != null && $lawyer_office_id != '') {
                $this->db->where ( $conditionsString );
            }
            $query = $this->db->get ();
            
            /* echo '<br/>';
             print_r ( ' query:: ' . $this->db->last_query () );
             echo '<br/>'; */
            
            if ($query->num_rows () > 0) {
                $records = $query->result_array ();
                return $records;
            } else {
                return false;
            }
        } catch ( Exception $e ) {
            // print_r ( $e->getMessage () );
            // echo '<br/>';
            // log_message ( 'debug', $e->getMessage () ); // use codeigniters built in logging library
            // show_error ( $e->getMessage () ); // or echo $e->getMessage()
        }
    }
    function getlawyerCases($lawyer_id) {
        try {
            $conditionsString = "lawyer_case.lawyer_id = '" . $lawyer_id . "'";
            $this->db->select ( 'case.case_id, case.creation_date, case.last_update_date, case.current_state_code, case.executive_order_date, case.advertisement_date, case.decision_46_date, case.decision_34_date' );
            $this->db->from ( 'lawyer_case' );
            $this->db->join ( 'case', 'lawyer_case.case_id = case.case_id' );
            $this->db->where ( $conditionsString );
            $query = $this->db->get ();
            
            if ($query->num_rows () > 0) {
                $records = $query->result_array ();
                return $records;
            } else {
                return false;
            }
        } catch ( Exception $e ) {
            // print_r ( $e->getMessage () );
            // echo '<br/>';
            // log_message ( 'debug', $e->getMessage () ); // use codeigniters built in logging library
            // show_error ( $e->getMessage () ); // or echo $e->getMessage()
        }
    }
    
    function getLawyers($lawyer_office_id) {
        $conditionsString = "user_type_code = 'LAWYER'";
        if ($lawyer_office_id != null && $lawyer_office_id != '') {
            $conditionsString = $conditionsString . " AND " . "user.lawyer_office_id = '" . $lawyer_office_id . "'";
        }
        $this->db->select ( 'user.lawyer_office_id as user_lawyer_office_id, user.user_id, user.first_name, user.last_name, user.user_name,lawyer_office.name as lawyer_office_name' );
        $this->db->from ( 'user' );
        $this->db->join ( 'lawyer_office', 'user.lawyer_office_id = lawyer_office.lawyer_office_id' );
        $this->db->where ( $conditionsString );
        $query = $this->db->get ();
        
        if ($query->num_rows () > 0) {
            $records = $query->result_array ();
            $i = 0;
            $lawyers_data = array ();
            foreach ( $records as $record ) {
                $lawyers_data [$i] ['lawyer_office_id'] = $record ['user_lawyer_office_id'];
                $lawyers_data [$i] ['lawyer_office_name'] = $record ['lawyer_office_name'];
                $lawyers_data [$i] ['lawyer_id'] = $record ['user_id'];
                $lawyers_data [$i] ['first_name'] = $record ['first_name'];
                $lawyers_data [$i] ['last_name'] = $record ['last_name'];
                $lawyers_data [$i] ['user_name'] = $record ['user_name'];
                
                $i = $i + 1;
            }
            return $lawyers_data;
        } else {
            return false;
        }
    }
    
    function getLawyerData($lawyer_id) {
        $conditionsString = "user_type_code = 'LAWYER'";
        if ($lawyer_id!= null && $lawyer_id!= '') {
            $conditionsString = $conditionsString . " AND " . "user.user_id = '" . $lawyer_id . "'";
        }
        $this->db->select ( 'user.lawyer_office_id as user_lawyer_office_id, user.user_id, user.first_name, user.last_name, user.user_name,lawyer_office.name as lawyer_office_name' );
        $this->db->from ( 'user' );
        $this->db->join ( 'lawyer_office', 'user.lawyer_office_id = lawyer_office.lawyer_office_id' , 'left');
        $this->db->where ( $conditionsString );
        $query = $this->db->get ();
        
        if ($query->num_rows () > 0) {
            $records = $query->result_array ();
            $lawyers_data = array ();
            $lawyers_data ['lawyer_office_id'] = $records[0]['user_lawyer_office_id'];
            $lawyers_data ['lawyer_office_name'] = $records[0]['lawyer_office_name'];
            $lawyers_data ['lawyer_id'] = $records[0]['user_id'];
            $lawyers_data ['first_name'] = $records[0]['first_name'];
            $lawyers_data ['last_name'] = $records[0]['last_name'];
            $lawyers_data ['user_name'] = $records[0]['user_name'];
            
            return $lawyers_data;
        } else {
            return false;
        }
    }
    
    
    function getUserSumInvoices($lawyer_id, $from_date_Gre, $to_date_Gre) {
        $amounts = array ();
        $amounts ['paid'] = 0;
        $amounts ['unpaid'] = 0;
        
        /*
         * print_r('- from_date_Gre='.$from_date_Gre);
         * echo '<br/>';
         * print_r('- to_date_Gre ='.$to_date_Gre);
         * echo '<br/>';
         */
        $conditionsString = "invoices.lawyer_id = '" . $lawyer_id . "' AND  invoices.is_paid = 'Y'";
        if ($from_date_Gre != null && $from_date_Gre != '') {
            $conditionsString = $conditionsString . " AND invoices.invoice_date >= '" . $from_date_Gre . "'";
        }
        if ($to_date_Gre != null && $to_date_Gre != '') {
            $conditionsString = $conditionsString . " AND invoices.invoice_date <= '" . $to_date_Gre . "'";
        }
        
        // print_r ( ' conditionsString ::' . $conditionsString );
        
        $this->db->select ( 'sum(invoices.billing_value)  AS paid_sum' );
        $this->db->from ( 'invoices' );
        $this->db->where ( $conditionsString );
        $query = $this->db->get ();
        //log_message('error', ' query:: ' . $this->db->last_query () );
        
        
        // print_r ( ' query:: ' . $this->db->last_query () );
        // echo '<br/>';
        if ($query->num_rows () > 0) {
            $records = $query->result_array ();
            // print_r ( ' records :' . var_dump ( $records [0] ['paid_sum']) );
            // echo '<br/>';
            if ($records [0] ['paid_sum'] != null && $records [0] ['paid_sum'] != '') {
                $amounts ['paid'] = $records [0] ['paid_sum'];
            }
        }
        
        $conditionsString = "invoices.lawyer_id = '" . $lawyer_id . "' AND  invoices.is_paid = 'N'";
        if ($from_date_Gre != null && $from_date_Gre != '') {
            $conditionsString = $conditionsString . " AND invoices.invoice_date >= '" . $from_date_Gre . "'";
        }
        if ($to_date_Gre != null && $to_date_Gre != '') {
            $conditionsString = $conditionsString . " AND invoices.invoice_date <= '" . $to_date_Gre . "'";
        }
        $this->db->select ( 'sum(invoices.billing_value)  AS unpaid_sum' );
        $this->db->from ( 'invoices' );
        $this->db->where ( $conditionsString );
        $query = $this->db->get ();
        //log_message('error', ' query:: ' . $this->db->last_query () );
        
        if ($query->num_rows () > 0) {
            $records = $query->result_array ();
            // print_r ( ' records :' . var_dump ( $records [0] ['unpaid_sum']) );
            // echo '<br/>';
            if ($records [0] ['unpaid_sum'] != null && $records [0] ['unpaid_sum'] != '') {
                $amounts ['unpaid'] = $records [0] ['unpaid_sum'];
            }
        }
        
        //log_message('error', ' $amounts for lawyer '.$lawyer_id.' are :: ' . print_r($amounts, true) );
        return $amounts;
    }
    
    
    function getLawyerPerformance ($lawyer_id, $from_date_Gre, $to_date_Gre){
        
        try {
            $lawyerPerformance = array();
            $lawyerPerformance['all'] = 0;
            $lawyerPerformance['closed'] = 0;
            $lawyerPerformance['current'] = 0;
            $lawyerPerformance['late'] = 0;
            
            $closed_count = 0;
            $current_count = 0;
            $late_count = 0;
            $conditionsString = "lawyer_case.lawyer_id = '" . $lawyer_id . "'";
            if ($from_date_Gre != null && $from_date_Gre != '') {
                $conditionsString = $conditionsString . " AND case.creation_date >= '" . $from_date_Gre . "'";
            }
            if ($to_date_Gre != null && $to_date_Gre != '') {
                $conditionsString = $conditionsString . " AND case.creation_date <= '" . $to_date_Gre . "'";
            }
            $this->db->select ( 'case.case_id' );
            $this->db->from ( 'lawyer_case' );
            $this->db->join ( 'case', 'lawyer_case.case_id = case.case_id' , 'left');
            $this->db->where ( $conditionsString );
            $this->db->distinct();
            $query = $this->db->get ();
            
            //log_message('error', 'lawyer performance query:: ' . $this->db->last_query () );
            
            //print_r ( ' query:: ' . $this->db->last_query () );
            //echo '<br/>';
            if ($query->num_rows () > 0) {
                $records = $query->result_array ();
                $i = 0;
                foreach($records as $record ) {
                    $case_id = $record['case_id'];
                    $current_state_code = '';
                    
                    $this->db->select ( 'case.case_id, case.current_state_code' );
                    $this->db->from ( 'case' );
                    $this->db->where("case.case_id = '$case_id'");
                    $query2 = $this->db->get ();
                    if ($query2->num_rows () > 0) {
                        $result = $query2->result_array ();
                        $current_state_code = $result[0]['current_state_code'];
                    }
                    
                    //print_r ( 'record:: ' . var_dump ( $record) );
                    //echo '<br/>';
                    
                    if($current_state_code == 'CLOSED' || $current_state_code == 'SUSPENDED'){
                        $closed_count = $closed_count+1;
                    }else{
                        $case_type = self::getCasePerformanceType($case_id);
                        if ($case_type == 'CURRENT') {
                            $current_count++;
                        } else if ($case_type == 'LATE') {
                            $late_count++;
                        }
                        //print_r ( 'case_type:: ' . var_dump ( $case_type) );
                        //echo '<br/>';
                    }
                    $i = $i + 1;
                }
                
                $lawyerPerformance['all'] = $i;
                $lawyerPerformance['closed'] = $closed_count;
                $lawyerPerformance['current'] = $current_count;
                $lawyerPerformance['late'] = $late_count;
                
            }
            return $lawyerPerformance;
        } catch ( Exception $e ) {
            // print_r ( $e->getMessage () );
            // echo '<br/>';
            // log_message ( 'debug', $e->getMessage () ); // use codeigniters built in logging library
            // show_error ( $e->getMessage () ); // or echo $e->getMessage()
        }
    }
    
    
    
    
    function getCasePerformanceType($case_id) {
        try {
            $type1 = 'CURRENT';
            $type2 = 'LATE';
            $type = $type1;
            
            $notif_data = array ();
            
            $conditionsString = "delivered_notifications.case_id = '" . $case_id . "'";
            $this->db->select ( 'delivered_notifications.email_id, delivered_notifications.case_id , delivered_notifications.sent_date_time, delivered_notifications.trans_id' );
            $this->db->from ( 'case' );
            $this->db->join ( 'delivered_notifications', 'case.case_id = delivered_notifications.case_id' , 'left');
            $this->db->where ( $conditionsString );
            $this->db->order_by ( "email_id", "desc" );
            $query = $this->db->get ();
            
            if ($query->num_rows () > 0) {
                $records = $query->result_array ();
                $notif_data ['case_id'] = $records [0] ['case_id'];
                $notif_data ['sent_date_time'] = $records [0] ['sent_date_time'];
                $notif_data ['trans_id'] = $records [0] ['trans_id'];
                $notif_data ['email_id'] = $records [0] ['email_id'];
                
                //log_message('error', '**** case_id = ' . $case_id . '*** trans_id: ' . $notif_data ['trans_id'] . "***** email_id::" . $notif_data ['email_id']. '  sent_date_time= ' . $notif_data ['sent_date_time']);
                // print_r ( '**** case_id = ' . $case_id . '*** trans_id: ' . $notif_data ['trans_id'] . "***** email_id::" . $notif_data ['email_id'] );
                // echo '<br/>';
                // print_r ( '**** sent_date_time = ' . $notif_data ['sent_date_time']);
                // echo '<br/>';
                
                if ($notif_data ['trans_id'] != null && $notif_data ['trans_id'] != '') {
                    // get its date
                    $conditionsString = "case_transactions.transaction_id = '" . $notif_data ['trans_id'] . "' AND case_id = '" . $case_id . "'";
                    $this->db->select ( 'case_transactions.transaction_id, case_transactions.trans_date_time' );
                    $this->db->from ( 'case_transactions' );
                    $this->db->where ( $conditionsString );
                    $query = $this->db->get ();
                    if ($query->num_rows () > 0) {
                        $records = $query->result_array ();
                        $notif_data ['trans_date_time'] = $records [0] ['trans_date_time'];
                    }
                    
                    //log_message('error', '**** case_id = ' . $case_id . '*** trans_id: ' . $notif_data ['trans_id'] . "***** email_id::" . $notif_data ['email_id']. '  sent_date_time= '. $notif_data ['sent_date_time'] . "  ***** trans date::" . $notif_data ['trans_date_time'] );
                    //print_r ( '**** case_id = ' . $case_id . '*** trans_id: ' . $notif_data ['trans_id'] . "***** trans date::" . $notif_data ['trans_date_time'] );
                    // echo '<br/>';
                    $recent_transactions = '0';
                    
                    if (isset($notif_data ['trans_date_time']) && $notif_data ['trans_date_time'] != NULl && $notif_data ['trans_date_time'] != ''){
                        $conditionsString = "case_transactions.case_id = '" . $case_id . "' AND case_transactions.trans_date_time > '" . $notif_data ['trans_date_time'] . "' AND case_transactions.transaction_status = 'Success' AND edit_action = 'N' AND manual_insert =  'N'";
                        $this->db->select ( 'case_transactions.transaction_id, case_transactions.case_id, case_transactions.trans_date_time' );
                        $this->db->from ( 'case_transactions' );
                        $this->db->where ( $conditionsString );
                        $this->db->order_by ( "trans_date_time", "ASC" );
                        $query = $this->db->get ();
                        if ($query->num_rows () > 0) {
                            $recent_transactions = '1';
                        }
                    }
                    
                    //print_r ( ' query next transaction :: ' . $this->db->last_query () );
                    //echo '<br/>';
                    
                    if ($recent_transactions == '1') {
                        
                    } else {
                        //print_r ( '**** case_id = ' . $case_id . '*** trans_id: ' . $notif_data ['trans_id'] . "***** trans date::" . $notif_data ['trans_date_time'] );
                        //echo '<br/>';
                        
                        // compare date of notification with current date
                        date_default_timezone_set ( 'Asia/Riyadh' );
                        $current_date = date ( "Y-m-d H:i:s" );
                        $sent_date_time = new DateTime ( $notif_data ['sent_date_time'] );
                        $diff_value = $sent_date_time->diff ( new DateTime ( $current_date ) );
                        //print_r ( '**** diff y = ' . $diff_value->y . '***diff m: ' . $diff_value->m . "***** diff d::" . $diff_value->d );
                        //echo '<br/>';
                        
                        $now =  strtotime($current_date);
                        $your_date = strtotime($notif_data ['sent_date_time']);
                        $datediff = $now - $your_date;
                        $daysDiff =  floor($datediff / (60 * 60 * 24));
                        //print_r (' *** daysDiff: '.$daysDiff);
                        //echo '<br/>';
                        
                        if ($daysDiff >= 1) {
                            $type = $type2;
                            // print_r ( '****2* case_id = ' . $case_id . ' is late' );
                            // echo '<br/>';
                        }
                    }
                }else{
                    $type='UNKNOWN';
                }
            }
            //log_message('error', '****final * case_id = ' . $case_id . ' is :' . $type );
            // print_r ( '****final * case_id = ' . $case_id . ' is :' . $type );
            // echo '<br/>';
            
            return $type;
        } catch ( Exception $e ) {
            // print_r ( $e->getMessage () );
            // echo '<br/>';
            // log_message ( 'debug', $e->getMessage () ); // use codeigniters built in logging library
            // show_error ( $e->getMessage () ); // or echo $e->getMessage()
        }
    }
    
    function getReport1Data($lawyer_id, $lawyer_office_id, $fromDate, $toDate) {
        
        $report1Data = array ();
        
        $action_1 = 'NEED_MODIFICATIONS';
        $state_code_1 = 'MODIFICATION_REQUIRED';
        
        $action_2 = '';
        $state_code_2 = 'UNDER_REVIEW';
        
        $action_3 = 'ASSIGN_TO_LAWYER';
        $state_code_3 = 'ASSIGNED_TO_LAWYER';
        
        $action_4 = 'REGISTER';
        $state_code_4 = 'REGISTERED';
        
        $action_5 = 'MAKE_DECISION_34';
        $state_code_5 = 'DECISION_34';
        
        $action_6 = 'ADVERTISE';
        $state_code_6 = 'PUBLICLY_ADVERTISED';
        
        $action_7 = 'MAKE_DECISION_46';
        $state_code_7 = 'DECISION_46';
        
        $action_8 = 'CLOSE';
        $state_code_8 = 'CLOSED';
        
        $action_9 = 'SUSPEND';
        $state_code_9 = 'SUSPENDED';
        
        
        $report1Data ['new'] = 0;
        $report1Data ['reg'] = 0;
        $report1Data ['dec34'] = 0;
        $report1Data ['adv'] = 0;
        $report1Data ['dec46'] = 0;
        $report1Data ['closed'] = 0;
        
        $trans_count_1 = self::getStatesCountInRange($lawyer_id, $lawyer_office_id, $action_1, $state_code_1, $fromDate, $toDate);
        $trans_count_2 = self::getStatesCountInRange($lawyer_id, $lawyer_office_id, $action_2, $state_code_2, $fromDate, $toDate);
        $trans_count_3 = self::getStatesCountInRange($lawyer_id, $lawyer_office_id, $action_3, $state_code_3, $fromDate, $toDate);
        $trans_count_4 = self::getStatesCountInRange($lawyer_id, $lawyer_office_id, $action_4, $state_code_4, $fromDate, $toDate);
        $trans_count_5 = self::getStatesCountInRange($lawyer_id, $lawyer_office_id, $action_5, $state_code_5, $fromDate, $toDate);
        $trans_count_6 = self::getStatesCountInRange($lawyer_id, $lawyer_office_id, $action_6, $state_code_6, $fromDate, $toDate);
        $trans_count_7 = self::getStatesCountInRange($lawyer_id, $lawyer_office_id, $action_7, $state_code_7, $fromDate, $toDate);
        $trans_count_8 = self::getStatesCountInRange($lawyer_id, $lawyer_office_id, $action_8, $state_code_8, $fromDate, $toDate);
        $trans_count_9 = self::getStatesCountInRange($lawyer_id, $lawyer_office_id, $action_9, $state_code_9, $fromDate, $toDate);
        
        $report1Data ['new'] = $trans_count_1 + $trans_count_2 + $trans_count_3;
        $report1Data ['reg'] = $trans_count_4;
        $report1Data ['dec34'] = $trans_count_5;
        $report1Data ['adv'] = $trans_count_6;
        $report1Data ['dec46'] = $trans_count_7;
        $report1Data ['closed'] = $trans_count_8 + $trans_count_9;
        
        
        /* print_r('New:: '. $report1Data ['new']);
         echo '<br/>';
         print_r('reg:: '. $report1Data ['reg']);
         echo '<br/>';
         print_r('dec34:: '. $report1Data ['dec34']);
         echo '<br/>';
         print_r('adv:: '. $report1Data ['adv']);
         echo '<br/>';
         print_r('dec46:: '. $report1Data ['dec46']);
         echo '<br/>';
         print_r('closed:: '. $report1Data ['closed']);
         echo '<br/>'; */
        
        return $report1Data;
    }
    
    
    function getReport4Data($lawyer_id, $lawyer_office_id, $from_date_Gre, $to_date_Gre) {
        
        // Set timezone
        date_default_timezone_set ( 'Asia/Riyadh' );
        
        // Start date
        $date = $from_date_Gre;
        // End date
        $end_date = $to_date_Gre;
        
        $report4Data = array ();
        $i = 0;
        
        $action_1 = 'NEED_MODIFICATIONS';
        $state_code_1 = 'MODIFICATION_REQUIRED';
        
        $action_2 = '';
        $state_code_2 = 'UNDER_REVIEW';
        
        $action_3 = 'ASSIGN_TO_LAWYER';
        $state_code_3 = 'ASSIGNED_TO_LAWYER';
        
        $action_4 = 'REGISTER';
        $state_code_4 = 'REGISTERED';
        
        $action_5 = 'MAKE_DECISION_34';
        $state_code_5 = 'DECISION_34';
        
        $action_6 = 'ADVERTISE';
        $state_code_6 = 'PUBLICLY_ADVERTISED';
        
        $action_7 = 'MAKE_DECISION_46';
        $state_code_7 = 'DECISION_46';
        
        $action_8 = 'CLOSE';
        $state_code_8 = 'CLOSED';
        
        $action_9 = 'SUSPEND';
        $state_code_9 = 'SUSPENDED';
        
        while ( strtotime ( $end_date) >=  strtotime ( $date ) ) {
            $end_date = date ( "Y-m-d", strtotime ( $end_date ) );
            // echo '<br/>';
            // echo "$date";
            
            $report4Data [$i] ['date'] = $end_date;
            $report4Data [$i] ['created']=0;
            $report4Data [$i] ['new'] = 0;
            $report4Data [$i] ['reg'] = 0;
            $report4Data [$i] ['dec34'] = 0;
            $report4Data [$i] ['adv'] = 0;
            $report4Data [$i] ['dec46'] = 0;
            $report4Data [$i] ['closed'] = 0;
            
            $createdCasesCount = self::getCreatedCasesCount ($lawyer_id, $lawyer_office_id, $end_date);
            $trans_count_1 = self::getStatesCount ($lawyer_id, $lawyer_office_id, $action_1, $state_code_1, $end_date);
            $trans_count_2 = self::getStatesCount ($lawyer_id, $lawyer_office_id, $action_2, $state_code_2, $end_date);
            $trans_count_3 = self::getStatesCount ($lawyer_id, $lawyer_office_id, $action_3, $state_code_3, $end_date);
            $trans_count_4 = self::getStatesCount ($lawyer_id, $lawyer_office_id, $action_4, $state_code_4, $end_date);
            $trans_count_5 = self::getStatesCount ($lawyer_id, $lawyer_office_id, $action_5, $state_code_5, $end_date);
            $trans_count_6 = self::getStatesCount ($lawyer_id, $lawyer_office_id, $action_6, $state_code_6, $end_date);
            $trans_count_7 = self::getStatesCount ($lawyer_id, $lawyer_office_id, $action_7, $state_code_7, $end_date);
            $trans_count_8 = self::getStatesCount ($lawyer_id, $lawyer_office_id, $action_8, $state_code_8, $end_date);
            $trans_count_9 = self::getStatesCount ($lawyer_id, $lawyer_office_id, $action_9, $state_code_9, $end_date);
            
            $report4Data [$i] ['created'] = $createdCasesCount;
            $report4Data [$i] ['new'] = $trans_count_1 + $trans_count_2 + $trans_count_3;
            $report4Data [$i] ['reg'] = $trans_count_4;
            $report4Data [$i] ['dec34'] = $trans_count_5;
            $report4Data [$i] ['adv'] = $trans_count_6;
            $report4Data [$i] ['dec46'] = $trans_count_7;
            $report4Data [$i] ['closed'] = $trans_count_8 + $trans_count_9;
            
            $end_date= date ( "Y-m-d", strtotime ( "-1 day", strtotime ( $end_date) ) );
            $i ++;
        }
        
        return $report4Data;
    }
    
    
    function getStatesCount($lawyer_id, $lawyer_office_id, $action_code, $state_code, $date) {
        $conditionsString = "case_transactions.transaction_status = 'Success' AND edit_action = 'N' AND manual_insert =  'N'";
        $conditionsString = $conditionsString. " AND case_transactions.last_update_date = '".$date."'";
        if ($lawyer_office_id != null && $lawyer_office_id != '') {
            $conditionsString = $conditionsString. " AND case.lawyer_office_id = '" . $lawyer_office_id . "'";
        }
        if ($lawyer_id != null && $lawyer_id != '') {
            $conditionsString = $conditionsString. " AND lawyer_case.lawyer_id = '" . $lawyer_id . "'";
        }
        if($action_code != null && $action_code !=''){
            $conditionsString = $conditionsString. " AND case_transactions.action_code='".$action_code."'";
        }else{
            $conditionsString = $conditionsString. " AND case_transactions.action_code IS NULL ";
        }
        if($state_code!= null && $state_code!=''){
            $conditionsString = $conditionsString." AND case_transactions.current_state_code='".$state_code."'";
        }
        $this->db->select ( 'case.case_id' );
        $this->db->from ( 'case_transactions' );
        $this->db->join ( 'case', 'case.case_id = case_transactions.case_id' , 'left');
        if ($lawyer_id != null && $lawyer_id != '') {
            $this->db->join ( 'lawyer_case', 'lawyer_case.case_id = case.case_id' , 'left');
        }
        $this->db->where ( $conditionsString );
        $this->db->distinct();
        $query = $this->db->get ();
        //log_message('error', ' getStatesCount query:: ' . $this->db->last_query () );
        
        /*  echo '<br/>';
         print_r ( ' query:: ' . $this->db->last_query () );
         echo '<br/>' ; */
        
        if ($query->num_rows () > 0) {
            $records = $query->result_array ();
            $trans_count = count($records);
            return $trans_count;
        }else{
            return 0;
        }
    }
    
    function getStatesCountInRange($lawyer_id, $lawyer_office_id, $action_code, $state_code, $fromDate, $toDate) {
        $conditionsString = "case_transactions.transaction_status = 'Success' AND edit_action = 'N' AND manual_insert =  'N'";
        
        if ($lawyer_office_id != null && $lawyer_office_id != '') {
            $conditionsString = $conditionsString. " AND case.lawyer_office_id = '" . $lawyer_office_id . "'";
        }
        if ($lawyer_id != null && $lawyer_id != '') {
            $conditionsString = $conditionsString. " AND lawyer_case.lawyer_id = '" . $lawyer_id . "'";
        }
        if ($fromDate != null && $fromDate != '') {
            $conditionsString = $conditionsString . " AND case_transactions.last_update_date >= '" . $fromDate. "'";
        }
        if ($toDate != null && $toDate != '') {
            $conditionsString = $conditionsString . " AND case_transactions.last_update_date <= '" . $toDate. "'";
        }
        if($action_code != null && $action_code !=''){
            $conditionsString = $conditionsString. " AND case_transactions.action_code='".$action_code."'";
        }else{
            $conditionsString = $conditionsString. " AND case_transactions.action_code IS NULL ";
        }
        if($state_code!= null && $state_code!=''){
            $conditionsString = $conditionsString." AND case_transactions.current_state_code='".$state_code."'";
        }
        $this->db->select ( 'case.case_id' );
        $this->db->from ( 'case_transactions' );
        $this->db->join ( 'case', 'case.case_id = case_transactions.case_id' , 'left' );
        if ($lawyer_id != null && $lawyer_id != '') {
            $this->db->join ( 'lawyer_case', 'lawyer_case.case_id = case.case_id' , 'left');
        }
        $this->db->where ( $conditionsString );
        $this->db->distinct();
        $query = $this->db->get ();
        
        //log_message('error', 'count query:: ' . $this->db->last_query () );
        
        /* echo '<br/>';
         print_r ( ' query:: ' . $this->db->last_query () );
         echo '<br/>'; */
        
        if ($query->num_rows () > 0) {
            $records = $query->result_array ();
            $trans_count = count($records);
            return $trans_count;
        }else{
            return 0;
        }
    }
    
    
    function getCreatedCasesCount ($lawyer_id, $lawyer_office_id, $date){
        $conditionsString = "case.creation_date = '".$date."'";
        $conditionsString = $conditionsString. " AND case.lawyer_office_id = '" . $lawyer_office_id . "'";
        if ($lawyer_id != null && $lawyer_id != '') {
            $conditionsString = $conditionsString. " AND lawyer_case.lawyer_id = '" . $lawyer_id . "'";
        }
        $this->db->select('case.case_id');
        $this->db->from('case');
        if ($lawyer_id != null && $lawyer_id != '') {
            $this->db->join ( 'lawyer_case', 'lawyer_case.case_id = case.case_id', 'left');
        }
        $this->db->where ( $conditionsString );
        $this->db->distinct();
        $query = $this->db->get ();
        //log_message('error', ' Created cases query:: ' . $this->db->last_query () );
        
        if ($query->num_rows () > 0) {
            $records = $query->result_array ();
            $trans_count = count($records);
            return $trans_count;
        }else{
            return 0;
        }
        
    }
    
    function getReport1Data_temp($lawyer_id, $lawyer_office_id, $fromDate, $toDate) {
        $report1Data = array ();
        $cases = '';
        if ($lawyer_office_id != null && $lawyer_office_id != '') {
            $cases = self::getlawyerOfficeCases($lawyer_office_id);
        }
        if ($lawyer_id != null && $lawyer_id != '') {
            $cases = self::getlawyerCases($lawyer_id);
        }
        
        $state_code_1 = 'MODIFICATION_REQUIRED';
        $state_code_2 = 'UNDER_REVIEW';
        $state_code_3 = 'ASSIGNED_TO_LAWYER';
        $state_code_4 = 'REGISTERED';
        $state_code_5 = 'DECISION_34';
        $state_code_6 = 'PUBLICLY_ADVERTISED';
        $state_code_7 = 'DECISION_46';
        $state_code_8 = 'CLOSED';
        $state_code_9 = 'SUSPENDED';
        
        $report1Data ['new'] = 0;
        $report1Data ['reg'] = 0;
        $report1Data ['dec34'] = 0;
        $report1Data ['adv'] = 0;
        $report1Data ['dec46'] = 0;
        $report1Data ['closed'] = 0;
        
        if ($cases) {
            for($i = 0; $i < count ( $cases ); $i ++) {
                $case_id = $cases[$i] ['case_id'];
                $creation_date = $cases[$i] ['creation_date'];
                $last_update_date = $cases[$i] ['last_update_date'];
                $current_state_code = $cases[$i] ['current_state_code'];
                $executive_order_date = $cases[$i] ['executive_order_date'];
                $decision_34_date = $cases[$i] ['decision_34_date'];
                $decision_46_date = $cases[$i] ['decision_46_date'];
                $advertisement_date = $cases[$i] ['advertisement_date'];
                
                
                $res = self::dateInRange($executive_order_date, $fromDate, $toDate);
                if($res==1){
                    $report1Data ['reg']++;
                }
                $res = self::dateInRange($decision_34_date, $fromDate, $toDate);
                if($res==1){
                    $report1Data ['dec34']++;
                }
                $res = self::dateInRange($decision_46_date, $fromDate, $toDate);
                if($res==1){
                    $report1Data ['dec46']++;
                }
                $res = self::dateInRange($advertisement_date, $fromDate, $toDate);
                if($res==1){
                    $report1Data ['adv']++;
                }
                
                if($current_state_code == $state_code_1 || $current_state_code == $state_code_2 || $current_state_code == $state_code_3){
                    $report1Data ['new']++;
                }
                if($current_state_code == $state_code_8 || $current_state_code == $state_code_9){
                    $report1Data ['closed']++;
                }
                
            }
        }
        
        return $report1Data;
    }
    
    
    function dateInRange($targetDate, $fromDate, $toDate){
        $result=1;
        if($targetDate== null || $targetDate== '' || $targetDate == '0000-00-00'){
            $result=0;
        }
        if ($fromDate != null && $fromDate != '') {
            if($targetDate<$fromDate){
                $result=0;
            }
        }
        if ($toDate != null && $toDate != '') {
            if($targetDate>$toDate){
                $result=0;
            }
        }
        return $result;
    }
    
    
}
?>