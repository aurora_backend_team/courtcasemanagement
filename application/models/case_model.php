<?php
class case_model extends CI_Model {
	function getLawyerCases($lawyer_id, $user_type_code, $status, $region, $search_data, $page_number, $pagination, $quick_search = 0,$search_query="") {
		$statusCondition = "";
		$regionCondition = "";
		if ($status != null && $status != '0') {
			$statusCondition = " AND case.current_state_code = '" . $status . "'";
		}
		if ($region != null && $region != '0') {
			$regionCondition = " AND case.region_id = '" . $region . "'";
		}
		$conditionsString = "lawyer_case.lawyer_office_id = (select lawyer_office_id from user where user_id = $lawyer_id) and lawyer_case.lawyer_id = '" . $lawyer_id . "'" . $statusCondition . $regionCondition;
		if($quick_search == 0){
		$this->db->select ( 'case.case_id, case.lawyer_office_id, case.customer_id, user.user_name as customer_name, user.last_name as customer_last_name,
							user.first_name as customer_first_name, case.contract_number, case.client_id_number, case.client_name,
							case.client_type, customer_type.name as customer_type_name, case.number_of_late_instalments, case.due_amount, case.remaining_amount, case.debenture,
							case.id, case.contract, case.others, case.creation_date, case.last_update_date,	case.creation_user,
							case.last_update_user, case.current_state_code, state.state_name, case.executive_order_number, case.decision_34_number,
							case.decision_34_file, case.advertisement_file, case.invoice_file, case.decision_46_date, case.closing_reason, case.notes,
							case.circle_number, case.order_number, case.referral_paper, case.consignment_image, case.decision_34_date, case.suspend_reason_code,
							case_suspend_reason.suspend_reason,case.suspend_date,case.suspend_file, case.executive_order_date, case.advertisement_date, case.debtor_name,
							case.obligation_value, case.total_debenture_amount, case.region_id, region.name as region_name' );
		$this->db->from ( 'lawyer_case' );
		$this->db->join ( 'case', 'lawyer_case.case_id = case.case_id and lawyer_case.lawyer_office_id = case.lawyer_office_id', 'left' );
		$this->db->join ( 'user', 'user.user_id = case.customer_id' );
		$this->db->join ( 'state', 'state.state_code = case.current_state_code' );
		$this->db->join ( 'customer_type', 'customer_type.customer_type_id = case.client_type' );
		$this->db->join ( 'case_suspend_reason', 'case_suspend_reason.suspend_code = case.suspend_reason_code', 'left' );
		$this->db->join ( 'region', 'region.region_id = case.region_id', 'left' );
		$this->db->group_by ( 'case.case_id' ); // add group_by
		                                        // if( $pagination == 0){
		                                        // $offset = ($page_number*200)-200;
		                                        // $this->db->limit(200,$offset);
		                                        // } else {
		                                        
		// }
		}
		$offset = ($page_number * 50) - 50;
		if($quick_search != 0){
			$query = $this->db->query(self::quick_search_query($search_query, $conditionsString,1, 1, $offset));
		}else if ($search_data != null && count ( $search_data ) > 0) {
			if ($search_data ['customer_id'] != null && $search_data ['customer_id'] != "") {
				$this->db->like ( 'case.customer_id', $search_data ['customer_id'], 'none' );
			}
			if ($search_data ['contract_number'] != null && $search_data ['contract_number'] != "") {
				$this->db->like ( 'case.contract_number', $search_data ['contract_number'], 'none' );
			}
			if ($search_data ['debtor_name'] != null && $search_data ['debtor_name'] != "") {
				$this->db->like ( 'case.debtor_name', $search_data ['debtor_name'] );
			}
			if ($search_data ['client_name'] != null && $search_data ['client_name'] != "") {
				$this->db->like ( 'case.client_name', $search_data ['client_name'] );
			}
			if ($search_data ['client_id_number'] != null && $search_data ['client_id_number'] != "") {
				$this->db->like ( 'case.client_id_number', $search_data ['client_id_number'], 'none' );
			}
			if ($search_data ['client_type'] != null && $search_data ['client_type'] != "") {
				$this->db->like ( 'case.client_type', $search_data ['client_type'], 'none' );
			}
			if ($search_data ['obligation_value'] != null && $search_data ['obligation_value'] != "") {
				$this->db->like ( 'case.obligation_value', $search_data ['obligation_value'], 'none' );
			}
			if ($search_data ['number_of_late_instalments'] != null && $search_data ['number_of_late_instalments'] != "") {
				$this->db->like ( 'case.number_of_late_instalments', $search_data ['number_of_late_instalments'], 'none' );
			}
			if ($search_data ['due_amount'] != null && $search_data ['due_amount'] != "") {
				$this->db->like ( 'case.due_amount', $search_data ['due_amount'], 'none' );
			}
			if ($search_data ['remaining_amount'] != null && $search_data ['remaining_amount'] != "") {
				$this->db->like ( 'case.remaining_amount', $search_data ['remaining_amount'], 'none' );
			}
			if ($search_data ['total_debenture_amount'] != null && $search_data ['total_debenture_amount'] != "") {
				$this->db->like ( 'case.total_debenture_amount', $search_data ['total_debenture_amount'], 'none' );
			}
			if ($search_data ['creation_date'] != null && $search_data ['creation_date'] != "") {
				$this->db->like ( 'case.creation_date', $search_data ['creation_date'], 'none' );
			}
			if ($search_data ['executive_order_number'] != null && $search_data ['executive_order_number'] != "") {
				$this->db->like ( 'case.executive_order_number', $search_data ['executive_order_number'], 'none' );
			}
			if ($search_data ['order_number'] != null && $search_data ['order_number'] != "") {
				$this->db->like ( 'case.order_number', $search_data ['order_number'], 'none' );
			}
			if ($search_data ['circle_number'] != null && $search_data ['circle_number'] != "") {
				$this->db->like ( 'case.circle_number', $search_data ['circle_number'], 'none' );
			}
			if ($search_data ['decision_34_number'] != null && $search_data ['decision_34_number'] != "") {
				$this->db->like ( 'case.decision_34_number', $search_data ['decision_34_number'], 'none' );
			}
			if ($search_data ['decision_34_date'] != null && $search_data ['decision_34_date'] != "") {
				$this->db->like ( 'case.decision_34_date', $search_data ['decision_34_date'], 'none' );
			}
			if ($search_data ['decision_46_date'] != null && $search_data ['decision_46_date'] != "") {
				$this->db->like ( 'case.decision_46_date', $search_data ['decision_46_date'], 'none' );
			}
			if ($search_data ['state_code'] != null && $search_data ['state_code'] != "") {
				$this->db->like ( 'case.current_state_code', $search_data ['state_code'], 'none' );
			}
			if ($search_data ['region_id'] != null && $search_data ['region_id'] != "") {
				$this->db->like ( 'case.region_id', $search_data ['region_id'], 'none' );
			}
			if ($search_data ['lawyer_id'] != null && $search_data ['lawyer_id'] != "") {
				$this->db->like ( 'lawyer_case.lawyer_id', $search_data ['lawyer_id'], 'none' );
			}
			if ($search_data ['executive_order_date'] != null && $search_data ['executive_order_date'] != "") {
				$this->db->like ( 'case.executive_order_date', $search_data ['executive_order_date'], 'none' );
			}
			if ($search_data ['advertisement_date'] != null && $search_data ['advertisement_date'] != "") {
				$this->db->like ( 'case.advertisement_date', $search_data ['advertisement_date'], 'none' );
			}
			if ($search_data ['suspend_date'] != null && $search_data ['suspend_date'] != "") {
				$this->db->like ( 'case.suspend_date', $search_data ['suspend_date'], 'none' );
			}
			if ($search_data ['suspend_reason_code'] != null && $search_data ['suspend_reason_code'] != "") {
				$this->db->like ( 'case.suspend_reason_code', $search_data ['suspend_reason_code'], 'none' );
			}
			if ($search_data ['closing_reason'] != null && $search_data ['closing_reason'] != "") {
				$this->db->like ( 'case.closing_reason', $search_data ['closing_reason'] );
			}
			if ($search_data ['case_id'] != null && $search_data ['case_id'] != "") {
				$this->db->like ( 'case.case_id', $search_data ['case_id'] );
			}
		}
		
		if($quick_search == 0){
			$this->db->where($conditionsString);
			
			$num_results = 0;
			try {
				// open1 here we copy $this->db in to tempdb and apply count_all_results() function on to this tempdb
				$tempdb = clone $this->db;
				$temp_query = $tempdb->get ();
				$num_results = $temp_query->num_rows ();
				log_message ( 'error', 'num_results = ' . $num_results );
			} catch ( Exception $e ) {
				log_message ( 'error', $e->getMessage () );
			}
		
			//$offset = ($page_number * 50) - 50;
			$this->db->limit ( 50, $offset );
		
		
			$query = $this->db->get ();
		}
		
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			// if ($android == 0)
			$cases_data = self::getCasesData ( $records, $user_type_code );
			// else
			// $cases_data = self::getCasesItemData ( $records, $user_type_code );
			if($quick_search == 0){
				$cases_data [0] ['count_all'] = $num_results;
				log_message ( 'error', 'count-all = ' . $cases_data [0] ['count_all'] );
			}
			return $cases_data;
		} else {
			return false;
		}
	}
	function getCustomerCases($customer_id, $user_type_code, $status, $region, $search_data, $page_number, $pagination, $quick_search = 0,$search_query="") {
		$statusCondition = "";
		$regionCondition = "";
		if ($status != null && $status != '0') {
			$statusCondition = " AND case.current_state_code = '" . $status . "'";
		}
		if ($region != null && $region != '0') {
			$regionCondition = " AND case.region_id = '" . $region . "'";
		}
		$conditionsString = "case.customer_id = '" . $customer_id . "'" . $statusCondition . $regionCondition;
		if($quick_search == 0){
		$this->db->select ( 'case.case_id, case.lawyer_office_id, case.customer_id, user.user_name as customer_name, user.last_name as customer_last_name,
							user.first_name as customer_first_name, case.contract_number, case.client_id_number, case.client_name,
							case.client_type, customer_type.name as customer_type_name, case.number_of_late_instalments, case.due_amount, case.remaining_amount, case.debenture,
							case.id, case.contract, case.others, case.creation_date, case.last_update_date,	case.creation_user,
							case.last_update_user, case.current_state_code, state.state_name, case.executive_order_number, case.decision_34_number,
							case.decision_34_file, case.advertisement_file, case.invoice_file, case.decision_46_date, case.closing_reason, case.notes,
							case.circle_number, case.order_number, case.referral_paper, case.consignment_image, case.decision_34_date, case.suspend_reason_code,
							case_suspend_reason.suspend_reason,case.suspend_date,case.suspend_file, case.executive_order_date, case.advertisement_date, case.debtor_name,
							case.obligation_value, case.total_debenture_amount, case.region_id, region.name as region_name' );
		$this->db->from ( 'case' );
		$this->db->join ( 'user', 'user.user_id = case.customer_id' );
		$this->db->join ( 'state', 'state.state_code = case.current_state_code' );
		$this->db->join ( 'customer_type', 'customer_type.customer_type_id = case.client_type' );
		$this->db->join ( 'case_suspend_reason', 'case_suspend_reason.suspend_code = case.suspend_reason_code', 'left' );
		$this->db->join ( 'lawyer_case', 'lawyer_case.case_id = case.case_id and lawyer_case.lawyer_office_id = case.lawyer_office_id', 'left' );
		$this->db->join ( 'region', 'region.region_id = case.region_id', 'left' );
		$this->db->group_by ( 'case.case_id' ); // add group_by
		                                        // if( $pagination == 0){
		                                        // $offset = ($page_number*200)-200;
		                                        // $this->db->limit(200,$offset);
		                                        // } else {
		                                        
		// }
		}
		$offset = ($page_number * 50) - 50;
		if($quick_search != 0){
			$query = $this->db->query(self::quick_search_query($search_query, $conditionsString,0, 1, $offset));
		}else if ($search_data != null && count ( $search_data ) > 0) {
			if ($search_data ['customer_id'] != null && $search_data ['customer_id'] != "") {
				$this->db->like ( 'case.customer_id', $search_data ['customer_id'], 'none' );
			}
			if ($search_data ['contract_number'] != null && $search_data ['contract_number'] != "") {
				$this->db->like ( 'case.contract_number', $search_data ['contract_number'], 'none' );
			}
			if ($search_data ['debtor_name'] != null && $search_data ['debtor_name'] != "") {
				$this->db->like ( 'case.debtor_name', $search_data ['debtor_name'] );
			}
			if ($search_data ['client_name'] != null && $search_data ['client_name'] != "") {
				$this->db->like ( 'case.client_name', $search_data ['client_name'] );
			}
			if ($search_data ['client_id_number'] != null && $search_data ['client_id_number'] != "") {
				$this->db->like ( 'case.client_id_number', $search_data ['client_id_number'], 'none' );
			}
			if ($search_data ['client_type'] != null && $search_data ['client_type'] != "") {
				$this->db->like ( 'case.client_type', $search_data ['client_type'], 'none' );
			}
			if ($search_data ['obligation_value'] != null && $search_data ['obligation_value'] != "") {
				$this->db->like ( 'case.obligation_value', $search_data ['obligation_value'], 'none' );
			}
			if ($search_data ['number_of_late_instalments'] != null && $search_data ['number_of_late_instalments'] != "") {
				$this->db->like ( 'case.number_of_late_instalments', $search_data ['number_of_late_instalments'], 'none' );
			}
			if ($search_data ['due_amount'] != null && $search_data ['due_amount'] != "") {
				$this->db->like ( 'case.due_amount', $search_data ['due_amount'], 'none' );
			}
			if ($search_data ['remaining_amount'] != null && $search_data ['remaining_amount'] != "") {
				$this->db->like ( 'case.remaining_amount', $search_data ['remaining_amount'], 'none' );
			}
			if ($search_data ['total_debenture_amount'] != null && $search_data ['total_debenture_amount'] != "") {
				$this->db->like ( 'case.total_debenture_amount', $search_data ['total_debenture_amount'], 'none' );
			}
			if ($search_data ['creation_date'] != null && $search_data ['creation_date'] != "") {
				$this->db->like ( 'case.creation_date', $search_data ['creation_date'], 'none' );
			}
			if ($search_data ['executive_order_number'] != null && $search_data ['executive_order_number'] != "") {
				$this->db->like ( 'case.executive_order_number', $search_data ['executive_order_number'], 'none' );
			}
			if ($search_data ['order_number'] != null && $search_data ['order_number'] != "") {
				$this->db->like ( 'case.order_number', $search_data ['order_number'], 'none' );
			}
			if ($search_data ['circle_number'] != null && $search_data ['circle_number'] != "") {
				$this->db->like ( 'case.circle_number', $search_data ['circle_number'], 'none' );
			}
			if ($search_data ['decision_34_number'] != null && $search_data ['decision_34_number'] != "") {
				$this->db->like ( 'case.decision_34_number', $search_data ['decision_34_number'], 'none' );
			}
			if ($search_data ['decision_34_date'] != null && $search_data ['decision_34_date'] != "") {
				$this->db->like ( 'case.decision_34_date', $search_data ['decision_34_date'], 'none' );
			}
			if ($search_data ['decision_46_date'] != null && $search_data ['decision_46_date'] != "") {
				$this->db->like ( 'case.decision_46_date', $search_data ['decision_46_date'], 'none' );
			}
			if ($search_data ['state_code'] != null && $search_data ['state_code'] != "") {
				$this->db->like ( 'case.current_state_code', $search_data ['state_code'], 'none' );
			}
			if ($search_data ['region_id'] != null && $search_data ['region_id'] != "") {
				$this->db->like ( 'case.region_id', $search_data ['region_id'], 'none' );
			}
			if ($search_data ['lawyer_id'] != null && $search_data ['lawyer_id'] != "") {
				$this->db->like ( 'lawyer_case.lawyer_id', $search_data ['lawyer_id'], 'none' );
			}
			if ($search_data ['executive_order_date'] != null && $search_data ['executive_order_date'] != "") {
				$this->db->like ( 'case.executive_order_date', $search_data ['executive_order_date'], 'none' );
			}
			if ($search_data ['advertisement_date'] != null && $search_data ['advertisement_date'] != "") {
				$this->db->like ( 'case.advertisement_date', $search_data ['advertisement_date'], 'none' );
			}
			if ($search_data ['suspend_date'] != null && $search_data ['suspend_date'] != "") {
				$this->db->like ( 'case.suspend_date', $search_data ['suspend_date'], 'none' );
			}
			if ($search_data ['suspend_reason_code'] != null && $search_data ['suspend_reason_code'] != "") {
				$this->db->like ( 'case.suspend_reason_code', $search_data ['suspend_reason_code'], 'none' );
			}
			if ($search_data ['closing_reason'] != null && $search_data ['closing_reason'] != "") {
				$this->db->like ( 'case.closing_reason', $search_data ['closing_reason'] );
			}
			if ($search_data ['case_id'] != null && $search_data ['case_id'] != "") {
				$this->db->like ( 'case.case_id', $search_data ['case_id'], 'none' );
			}
		}
		
		if($quick_search == 0){
			$this->db->where($conditionsString);
			
			$num_results = 0;
			try {
				// open1 here we copy $this->db in to tempdb and apply count_all_results() function on to this tempdb
				$tempdb = clone $this->db;
				$temp_query = $tempdb->get ();
				$num_results = $temp_query->num_rows ();
				log_message ( 'error', 'num_results = ' . $num_results );
			} catch ( Exception $e ) {
				log_message ( 'error', $e->getMessage () );
			}
		
			//$offset = ($page_number * 50) - 50;
			$this->db->limit ( 50, $offset );
		
		
			$query = $this->db->get ();
		}
		
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			// if ($android == 0)
			$cases_data = self::getCasesData ( $records, $user_type_code );
			// else
			// $cases_data = self::getCasesItemData ( $records, $user_type_code );
			if($quick_search == 0){
				$cases_data [0] ['count_all'] = $num_results;
				log_message ( 'error', 'count-all = ' . $cases_data [0] ['count_all'] );
			}
			return $cases_data;
		} else {
			return false;
		}
	}
	function getAllCases($lawyer_office_id, $user_type_code, $status, $region, $search_data, $page_number, $pagination, $quick_search = 0,$search_query="") {
		$statusCondition = "";
		$regionCondition = "";
		if ($status != null && $status != '0') {
			$statusCondition = " AND case.current_state_code = '" . $status . "'";
		}
		if ($region != null && $region != '0') {
			$regionCondition = " AND case.region_id = '" . $region . "'";
		}
		$conditionsString = "case.lawyer_office_id = '" . $lawyer_office_id . "'" . $statusCondition . $regionCondition;
		if($quick_search == 0){
		$this->db->select ( 'case.case_id, case.lawyer_office_id, case.customer_id, user.user_name as customer_name, user.last_name as customer_last_name,
							user.first_name as customer_first_name, case.contract_number, case.client_id_number, case.client_name,
							case.client_type, customer_type.name as customer_type_name, case.number_of_late_instalments, case.due_amount, case.remaining_amount, case.debenture,
							case.id, case.contract, case.others, case.creation_date, case.last_update_date,	case.creation_user,
							case.last_update_user, case.current_state_code, state.state_name, case.executive_order_number, case.decision_34_number,
							case.decision_34_file, case.advertisement_file, case.invoice_file, case.decision_46_date, case.closing_reason, case.notes,
							case.circle_number, case.order_number, case.referral_paper, case.consignment_image, case.decision_34_date, case.suspend_reason_code,
							case_suspend_reason.suspend_reason,case.suspend_date,case.suspend_file, case.executive_order_date, case.advertisement_date, case.debtor_name,
							case.obligation_value, case.total_debenture_amount, case.region_id, region.name as region_name' );
		$this->db->from ( 'case' );
		$this->db->join ( 'user', 'user.user_id = case.customer_id' );
		$this->db->join ( 'state', 'state.state_code = case.current_state_code' );
		$this->db->join ( 'customer_type', 'customer_type.customer_type_id = case.client_type' );
		$this->db->join ( 'case_suspend_reason', 'case_suspend_reason.suspend_code = case.suspend_reason_code', 'left' );
		$this->db->join ( 'lawyer_case', 'lawyer_case.case_id = case.case_id and lawyer_case.lawyer_office_id = case.lawyer_office_id', 'left' );
		$this->db->join ( 'region', 'region.region_id = case.region_id', 'left' );
		$this->db->group_by ( 'case.case_id' ); // add group_by
		                                        // if( $pagination == 0){
		                                        // $offset = ($page_number*200)-200;
		                                        // $this->db->limit(200,$offset);
		                                        // } else {
		                                        
		// }
		}
		$offset = ($page_number * 50) - 50;
		if($quick_search != 0){
			//if($page_number > 1 ){
			//echo self::quick_search_query($search_query, $conditionsString,0, 1, $offset);
			//}
			$query = $this->db->query(self::quick_search_query($search_query, $conditionsString,0, 1, $offset));
		}else if ($search_data != null && count ( $search_data ) > 0) {
			if ($search_data ['customer_id'] != null && $search_data ['customer_id'] != "") {
				$this->db->like ( 'case.customer_id', $search_data ['customer_id'], 'none' );
			}
			if ($search_data ['contract_number'] != null && $search_data ['contract_number'] != "") {
				$this->db->like ( 'case.contract_number', $search_data ['contract_number'], 'none' );
			}
			if ($search_data ['debtor_name'] != null && $search_data ['debtor_name'] != "") {
				$this->db->like ( 'case.debtor_name', $search_data ['debtor_name'] );
			}
			if ($search_data ['client_name'] != null && $search_data ['client_name'] != "") {
				$this->db->like ( 'case.client_name', $search_data ['client_name'] );
			}
			if ($search_data ['client_id_number'] != null && $search_data ['client_id_number'] != "") {
				$this->db->like ( 'case.client_id_number', $search_data ['client_id_number'], 'none' );
			}
			if ($search_data ['client_type'] != null && $search_data ['client_type'] != "") {
				$this->db->like ( 'case.client_type', $search_data ['client_type'], 'none' );
			}
			if ($search_data ['obligation_value'] != null && $search_data ['obligation_value'] != "") {
				$this->db->like ( 'case.obligation_value', $search_data ['obligation_value'], 'none' );
			}
			if ($search_data ['number_of_late_instalments'] != null && $search_data ['number_of_late_instalments'] != "") {
				$this->db->like ( 'case.number_of_late_instalments', $search_data ['number_of_late_instalments'], 'none' );
			}
			if ($search_data ['due_amount'] != null && $search_data ['due_amount'] != "") {
				$this->db->like ( 'case.due_amount', $search_data ['due_amount'], 'none' );
			}
			if ($search_data ['remaining_amount'] != null && $search_data ['remaining_amount'] != "") {
				$this->db->like ( 'case.remaining_amount', $search_data ['remaining_amount'], 'none' );
			}
			if ($search_data ['total_debenture_amount'] != null && $search_data ['total_debenture_amount'] != "") {
				$this->db->like ( 'case.total_debenture_amount', $search_data ['total_debenture_amount'], 'none' );
			}
			if ($search_data ['creation_date'] != null && $search_data ['creation_date'] != "") {
				$this->db->like ( 'case.creation_date', $search_data ['creation_date'], 'none' );
			}
			if ($search_data ['executive_order_number'] != null && $search_data ['executive_order_number'] != "") {
				$this->db->like ( 'case.executive_order_number', $search_data ['executive_order_number'], 'none' );
			}
			if ($search_data ['order_number'] != null && $search_data ['order_number'] != "") {
				$this->db->like ( 'case.order_number', $search_data ['order_number'], 'none' );
			}
			if ($search_data ['circle_number'] != null && $search_data ['circle_number'] != "") {
				$this->db->like ( 'case.circle_number', $search_data ['circle_number'], 'none' );
			}
			if ($search_data ['decision_34_number'] != null && $search_data ['decision_34_number'] != "") {
				$this->db->like ( 'case.decision_34_number', $search_data ['decision_34_number'], 'none' );
			}
			if ($search_data ['decision_34_date'] != null && $search_data ['decision_34_date'] != "") {
				$this->db->like ( 'case.decision_34_date', $search_data ['decision_34_date'], 'none' );
			}
			if ($search_data ['decision_46_date'] != null && $search_data ['decision_46_date'] != "") {
				$this->db->like ( 'case.decision_46_date', $search_data ['decision_46_date'], 'none' );
			}
			if ($search_data ['state_code'] != null && $search_data ['state_code'] != "") {
				$this->db->like ( 'case.current_state_code', $search_data ['state_code'], 'none' );
			}
			if ($search_data ['region_id'] != null && $search_data ['region_id'] != "") {
				$this->db->like ( 'case.region_id', $search_data ['region_id'], 'none' );
			}
			if ($search_data ['lawyer_id'] != null && $search_data ['lawyer_id'] != "") {
				$this->db->like ( 'lawyer_case.lawyer_id', $search_data ['lawyer_id'], 'none' );
			}
			if ($search_data ['executive_order_date'] != null && $search_data ['executive_order_date'] != "") {
				$this->db->like ( 'case.executive_order_date', $search_data ['executive_order_date'], 'none' );
			}
			if ($search_data ['advertisement_date'] != null && $search_data ['advertisement_date'] != "") {
				$this->db->like ( 'case.advertisement_date', $search_data ['advertisement_date'], 'none' );
			}
			if ($search_data ['suspend_date'] != null && $search_data ['suspend_date'] != "") {
				$this->db->like ( 'case.suspend_date', $search_data ['suspend_date'], 'none' );
			}
			if ($search_data ['suspend_reason_code'] != null && $search_data ['suspend_reason_code'] != "") {
				$this->db->like ( 'case.suspend_reason_code', $search_data ['suspend_reason_code'], 'none' );
			}
			if ($search_data ['closing_reason'] != null && $search_data ['closing_reason'] != "") {
				$this->db->like ( 'case.closing_reason', $search_data ['closing_reason'] );
			}
			if ($search_data ['case_id'] != null && $search_data ['case_id'] != "") {
				$this->db->like ( 'case.case_id', $search_data ['case_id'] );
			}
		}
		
		if($quick_search == 0){
			$this->db->where($conditionsString);
			
			$num_results = 0;
			try {
				// open1 here we copy $this->db in to tempdb and apply count_all_results() function on to this tempdb
				$tempdb = clone $this->db;
				$temp_query = $tempdb->get ();
				$num_results = $temp_query->num_rows ();
				log_message ( 'error', 'num_results = ' . $num_results );
			} catch ( Exception $e ) {
				log_message ( 'error', $e->getMessage () );
			}
		
			$this->db->limit ( 50, $offset );
		
		
			$query = $this->db->get ();
		}
		
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
		// if ($android == 0)
			$cases_data = self::getCasesData ( $records, $user_type_code );
			// else
			// $cases_data = self::getCasesItemData ( $records, $user_type_code );
			if($quick_search == 0){
				$cases_data [0] ['count_all'] = $num_results;
			}
//			log_message ( 'error', 'count-all = ' . $cases_data [0] ['count_all'] );
			return $cases_data;
		} else {
			return false;
		}
	}
	function getLawyerCasesCount($lawyer_id, $user_type_code, $status, $search_data, $page_number, $pagination, $region = 0, $quick_search = 0,$search_query="") {
		
		$statusCondition = "";
		$regionCondition = "";
		if ($status != null && $status != '0') {
			$statusCondition = " AND case.current_state_code = '" . $status . "'";
		}
		if ($region != null && $region != '0') {
			$regionCondition = " AND case.region_id = '" . $region . "'";
		}
		$conditionsString = "lawyer_case.lawyer_office_id = (select lawyer_office_id from user where user_id = $lawyer_id) and lawyer_case.lawyer_id = '" . $lawyer_id . "'" . $statusCondition . $regionCondition;
		
		$this->db->select ( 'case.case_id, case.lawyer_office_id, case.customer_id, user.user_name as customer_name, user.last_name as customer_last_name,
							user.first_name as customer_first_name, case.contract_number, case.client_id_number, case.client_name,
							case.client_type, customer_type.name as customer_type_name, case.number_of_late_instalments, case.due_amount, case.remaining_amount, case.debenture,
							case.id, case.contract, case.others, case.creation_date, case.last_update_date,	case.creation_user,
							case.last_update_user, case.current_state_code, state.state_name, case.executive_order_number, case.decision_34_number,
							case.decision_34_file, case.advertisement_file, case.invoice_file, case.decision_46_date, case.closing_reason, case.notes,
							case.circle_number, case.order_number, case.referral_paper, case.consignment_image, case.decision_34_date, case.suspend_reason_code,
							case.suspend_date,case_suspend_reason.suspend_reason,case.suspend_file, case.executive_order_date, case.advertisement_date, case.debtor_name,
							case.obligation_value, case.total_debenture_amount, case.region_id' );
		$this->db->from ( 'lawyer_case' );
		$this->db->join ( 'case', 'lawyer_case.case_id = case.case_id and lawyer_case.lawyer_office_id = case.lawyer_office_id' );
		$this->db->join ( 'user', 'user.user_id = case.customer_id' );
		$this->db->join ( 'state', 'state.state_code = case.current_state_code' );
		$this->db->join ( 'customer_type', 'customer_type.customer_type_id = case.client_type' );
		$this->db->join ( 'case_suspend_reason', 'case_suspend_reason.suspend_code = case.suspend_reason_code', 'left' );
		
		if($quick_search != 0){
			$query = $this->db->query(self::quick_search_query($search_query, $conditionsString,1, 0, 0));
		}else if ($search_data != null && count ( $search_data ) > 0) {
			if ($search_data ['customer_id'] != null && $search_data ['customer_id'] != "") {
				$this->db->like ( 'case.customer_id', $search_data ['customer_id'], 'none' );
			}
			if ($search_data ['contract_number'] != null && $search_data ['contract_number'] != "") {
				$this->db->like ( 'case.contract_number', $search_data ['contract_number'], 'none' );
			}
			if ($search_data ['debtor_name'] != null && $search_data ['debtor_name'] != "") {
				$this->db->like ( 'case.debtor_name', $search_data ['debtor_name'] );
			}
			if ($search_data ['client_name'] != null && $search_data ['client_name'] != "") {
				$this->db->like ( 'case.client_name', $search_data ['client_name'] );
			}
			if ($search_data ['client_id_number'] != null && $search_data ['client_id_number'] != "") {
				$this->db->like ( 'case.client_id_number', $search_data ['client_id_number'], 'none' );
			}
			if ($search_data ['client_type'] != null && $search_data ['client_type'] != "") {
				$this->db->like ( 'case.client_type', $search_data ['client_type'], 'none' );
			}
			if ($search_data ['obligation_value'] != null && $search_data ['obligation_value'] != "") {
				$this->db->like ( 'case.obligation_value', $search_data ['obligation_value'], 'none' );
			}
			if ($search_data ['number_of_late_instalments'] != null && $search_data ['number_of_late_instalments'] != "") {
				$this->db->like ( 'case.number_of_late_instalments', $search_data ['number_of_late_instalments'], 'none' );
			}
			if ($search_data ['due_amount'] != null && $search_data ['due_amount'] != "") {
				$this->db->like ( 'case.due_amount', $search_data ['due_amount'], 'none' );
			}
			if ($search_data ['remaining_amount'] != null && $search_data ['remaining_amount'] != "") {
				$this->db->like ( 'case.remaining_amount', $search_data ['remaining_amount'], 'none' );
			}
			if ($search_data ['total_debenture_amount'] != null && $search_data ['total_debenture_amount'] != "") {
				$this->db->like ( 'case.total_debenture_amount', $search_data ['total_debenture_amount'], 'none' );
			}
			if ($search_data ['creation_date'] != null && $search_data ['creation_date'] != "") {
				$this->db->like ( 'case.creation_date', $search_data ['creation_date'], 'none' );
			}
			if ($search_data ['executive_order_number'] != null && $search_data ['executive_order_number'] != "") {
				$this->db->like ( 'case.executive_order_number', $search_data ['executive_order_number'], 'none' );
			}
			if ($search_data ['order_number'] != null && $search_data ['order_number'] != "") {
				$this->db->like ( 'case.order_number', $search_data ['order_number'], 'none' );
			}
			if ($search_data ['circle_number'] != null && $search_data ['circle_number'] != "") {
				$this->db->like ( 'case.circle_number', $search_data ['circle_number'], 'none' );
			}
			if ($search_data ['decision_34_number'] != null && $search_data ['decision_34_number'] != "") {
				$this->db->like ( 'case.decision_34_number', $search_data ['decision_34_number'], 'none' );
			}
			if ($search_data ['decision_34_date'] != null && $search_data ['decision_34_date'] != "") {
				$this->db->like ( 'case.decision_34_date', $search_data ['decision_34_date'], 'none' );
			}
			if ($search_data ['decision_46_date'] != null && $search_data ['decision_46_date'] != "") {
				$this->db->like ( 'case.decision_46_date', $search_data ['decision_46_date'], 'none' );
			}
			if ($search_data ['state_code'] != null && $search_data ['state_code'] != "") {
				$this->db->like ( 'case.current_state_code', $search_data ['state_code'], 'none' );
			}
			if ($search_data ['region_id'] != null && $search_data ['region_id'] != "") {
				$this->db->like ( 'case.region_id', $search_data ['region_id'], 'none' );
			}
			if ($search_data ['lawyer_id'] != null && $search_data ['lawyer_id'] != "") {
				$this->db->like ( 'lawyer_case.lawyer_id', $search_data ['lawyer_id'], 'none' );
			}
			if ($search_data ['executive_order_date'] != null && $search_data ['executive_order_date'] != "") {
				$this->db->like ( 'case.executive_order_date', $search_data ['executive_order_date'], 'none' );
			}
			if ($search_data ['advertisement_date'] != null && $search_data ['advertisement_date'] != "") {
				$this->db->like ( 'case.advertisement_date', $search_data ['advertisement_date'], 'none' );
			}
			if ($search_data ['suspend_date'] != null && $search_data ['suspend_date'] != "") {
				$this->db->like ( 'case.suspend_date', $search_data ['suspend_date'], 'none' );
			}
			if ($search_data ['suspend_reason_code'] != null && $search_data ['suspend_reason_code'] != "") {
				$this->db->like ( 'case.suspend_reason_code', $search_data ['suspend_reason_code'], 'none' );
			}
			if ($search_data ['closing_reason'] != null && $search_data ['closing_reason'] != "") {
				$this->db->like ( 'case.closing_reason', $search_data ['closing_reason'] );
			}
			if ($search_data ['case_id'] != null && $search_data ['case_id'] != "") {
				$this->db->like ( 'case.case_id', $search_data ['case_id'] );
			}
		}
		if($quick_search == 0){
			$this->db->where ( $conditionsString );
			$query = $this->db->get ();
		}
		return $query->num_rows ();
	}
	function getCustomerCasesCount($customer_id, $user_type_code, $status, $search_data, $page_number, $pagination, $region = 0, $quick_search = 0,$search_query="") {
		/* if ($status == null || $status == '') {
			$conditionsString = "case.customer_id = '" . $customer_id . "'";
		} else {
			$conditionsString = "case.customer_id = '" . $customer_id . "' AND case.current_state_code = '" . $status . "'";
		} */
		$statusCondition = "";
		$regionCondition = "";
		if ($status != null && $status != '0') {
			$statusCondition = " AND case.current_state_code = '" . $status . "'";
		}
		if ($region != null && $region != '0') {
			$regionCondition = " AND case.region_id = '" . $region . "'";
		}
		$conditionsString = "case.customer_id = '" . $customer_id . "'" . $statusCondition . $regionCondition;
		
		$this->db->select ( 'case.case_id, case.lawyer_office_id, case.customer_id, user.user_name as customer_name, user.last_name as customer_last_name,
							user.first_name as customer_first_name, case.contract_number, case.client_id_number, case.client_name,
							case.client_type, customer_type.name as customer_type_name, case.number_of_late_instalments, case.due_amount, case.remaining_amount, case.debenture,
							case.id, case.contract, case.others, case.creation_date, case.last_update_date,	case.creation_user,
							case.last_update_user, case.current_state_code, state.state_name, case.executive_order_number, case.decision_34_number,
							case.decision_34_file, case.advertisement_file, case.invoice_file, case.decision_46_date, case.closing_reason, case.notes,
							case.circle_number, case.order_number, case.referral_paper, case.consignment_image, case.decision_34_date, case.suspend_reason_code,
							case.suspend_date,case_suspend_reason.suspend_reason,case.suspend_file, case.executive_order_date, case.advertisement_date, case.debtor_name,
							case.obligation_value, case.total_debenture_amount' );
		$this->db->from ( 'case' );
		$this->db->join ( 'user', 'user.user_id = case.customer_id' );
		$this->db->join ( 'state', 'state.state_code = case.current_state_code' );
		$this->db->join ( 'customer_type', 'customer_type.customer_type_id = case.client_type' );
		$this->db->join ( 'case_suspend_reason', 'case_suspend_reason.suspend_code = case.suspend_reason_code', 'left' );
		$this->db->join ( 'lawyer_case', 'lawyer_case.case_id = case.case_id and lawyer_case.lawyer_office_id = case.lawyer_office_id', 'left' );
		
		if($quick_search != 0){
			$query = $this->db->query(self::quick_search_query($search_query, $conditionsString,0, 0, 0));
		}else if ($search_data != null && count ( $search_data ) > 0) {
			if ($search_data ['customer_id'] != null && $search_data ['customer_id'] != "") {
				$this->db->like ( 'case.customer_id', $search_data ['customer_id'], 'none' );
			}
			if ($search_data ['contract_number'] != null && $search_data ['contract_number'] != "") {
				$this->db->like ( 'case.contract_number', $search_data ['contract_number'], 'none' );
			}
			if ($search_data ['debtor_name'] != null && $search_data ['debtor_name'] != "") {
				$this->db->like ( 'case.debtor_name', $search_data ['debtor_name'] );
			}
			if ($search_data ['client_name'] != null && $search_data ['client_name'] != "") {
				$this->db->like ( 'case.client_name', $search_data ['client_name'] );
			}
			if ($search_data ['client_id_number'] != null && $search_data ['client_id_number'] != "") {
				$this->db->like ( 'case.client_id_number', $search_data ['client_id_number'], 'none' );
			}
			if ($search_data ['client_type'] != null && $search_data ['client_type'] != "") {
				$this->db->like ( 'case.client_type', $search_data ['client_type'], 'none' );
			}
			if ($search_data ['obligation_value'] != null && $search_data ['obligation_value'] != "") {
				$this->db->like ( 'case.obligation_value', $search_data ['obligation_value'], 'none' );
			}
			if ($search_data ['number_of_late_instalments'] != null && $search_data ['number_of_late_instalments'] != "") {
				$this->db->like ( 'case.number_of_late_instalments', $search_data ['number_of_late_instalments'], 'none' );
			}
			if ($search_data ['due_amount'] != null && $search_data ['due_amount'] != "") {
				$this->db->like ( 'case.due_amount', $search_data ['due_amount'], 'none' );
			}
			if ($search_data ['remaining_amount'] != null && $search_data ['remaining_amount'] != "") {
				$this->db->like ( 'case.remaining_amount', $search_data ['remaining_amount'], 'none' );
			}
			if ($search_data ['total_debenture_amount'] != null && $search_data ['total_debenture_amount'] != "") {
				$this->db->like ( 'case.total_debenture_amount', $search_data ['total_debenture_amount'], 'none' );
			}
			if ($search_data ['creation_date'] != null && $search_data ['creation_date'] != "") {
				$this->db->like ( 'case.creation_date', $search_data ['creation_date'], 'none' );
			}
			if ($search_data ['executive_order_number'] != null && $search_data ['executive_order_number'] != "") {
				$this->db->like ( 'case.executive_order_number', $search_data ['executive_order_number'], 'none' );
			}
			if ($search_data ['order_number'] != null && $search_data ['order_number'] != "") {
				$this->db->like ( 'case.order_number', $search_data ['order_number'], 'none' );
			}
			if ($search_data ['circle_number'] != null && $search_data ['circle_number'] != "") {
				$this->db->like ( 'case.circle_number', $search_data ['circle_number'], 'none' );
			}
			if ($search_data ['decision_34_number'] != null && $search_data ['decision_34_number'] != "") {
				$this->db->like ( 'case.decision_34_number', $search_data ['decision_34_number'], 'none' );
			}
			if ($search_data ['decision_34_date'] != null && $search_data ['decision_34_date'] != "") {
				$this->db->like ( 'case.decision_34_date', $search_data ['decision_34_date'], 'none' );
			}
			if ($search_data ['decision_46_date'] != null && $search_data ['decision_46_date'] != "") {
				$this->db->like ( 'case.decision_46_date', $search_data ['decision_46_date'], 'none' );
			}
			if ($search_data ['state_code'] != null && $search_data ['state_code'] != "") {
				$this->db->like ( 'case.current_state_code', $search_data ['state_code'], 'none' );
			}
			if ($search_data ['region_id'] != null && $search_data ['region_id'] != "") {
				$this->db->like ( 'case.region_id', $search_data ['region_id'], 'none' );
			}
			if ($search_data ['lawyer_id'] != null && $search_data ['lawyer_id'] != "") {
				$this->db->like ( 'lawyer_case.lawyer_id', $search_data ['lawyer_id'], 'none' );
			}
			if ($search_data ['executive_order_date'] != null && $search_data ['executive_order_date'] != "") {
				$this->db->like ( 'case.executive_order_date', $search_data ['executive_order_date'], 'none' );
			}
			if ($search_data ['advertisement_date'] != null && $search_data ['advertisement_date'] != "") {
				$this->db->like ( 'case.advertisement_date', $search_data ['advertisement_date'], 'none' );
			}
			if ($search_data ['suspend_date'] != null && $search_data ['suspend_date'] != "") {
				$this->db->like ( 'case.suspend_date', $search_data ['suspend_date'], 'none' );
			}
			if ($search_data ['suspend_reason_code'] != null && $search_data ['suspend_reason_code'] != "") {
				$this->db->like ( 'case.suspend_reason_code', $search_data ['suspend_reason_code'], 'none' );
			}
			if ($search_data ['closing_reason'] != null && $search_data ['closing_reason'] != "") {
				$this->db->like ( 'case.closing_reason', $search_data ['closing_reason'] );
			}
			if ($search_data ['case_id'] != null && $search_data ['case_id'] != "") {
				$this->db->like ( 'case.case_id', $search_data ['case_id'] );
			}
		}
		if($quick_search == 0){
			$this->db->where ( $conditionsString );
			$query = $this->db->get ();
		}
		return $query->num_rows ();
	}
	function getAllCasesCount($lawyer_office_id, $user_type_code, $status, $search_data, $page_number, $pagination, $region, $quick_search = 0,$search_query="") {
		/* if ($status != null && $status != '') {
			$conditionsString = "case.current_state_code = '" . $status . "' AND user.lawyer_office_id = '" . $lawyer_office_id . "'";
		} else {
			$conditionsString = "case.lawyer_office_id = '" . $lawyer_office_id . "'";
		} */
		$statusCondition = "";
		$regionCondition = "";
		if ($status != null && $status != '0') {
			$statusCondition = " AND case.current_state_code = '" . $status . "'";
		}
		if ($region != null && $region != '0') {
			$regionCondition = " AND case.region_id = '" . $region . "'";
		}
		$conditionsString = "case.lawyer_office_id = '" . $lawyer_office_id . "'" . $statusCondition . $regionCondition;
		$this->db->select ( 'case.case_id, case.lawyer_office_id, case.customer_id, user.user_name as customer_name, user.last_name as customer_last_name,
							user.first_name as customer_first_name, case.contract_number, case.client_id_number, case.client_name,
							case.client_type, customer_type.name as customer_type_name, case.number_of_late_instalments, case.due_amount, case.remaining_amount, case.debenture,
							case.id, case.contract, case.others, case.creation_date, case.last_update_date,	case.creation_user,
							case.last_update_user, case.current_state_code, state.state_name, case.executive_order_number, case.decision_34_number,
							case.decision_34_file, case.advertisement_file, case.invoice_file, case.decision_46_date, case.closing_reason, case.notes,
							case.circle_number, case.order_number, case.referral_paper, case.consignment_image, case.decision_34_date, case.suspend_reason_code,
							case.suspend_date,case_suspend_reason.suspend_reason,case.suspend_file, case.executive_order_date, case.advertisement_date, case.debtor_name,
							case.obligation_value, case.total_debenture_amount' );
		$this->db->from ( 'case' );
		$this->db->join ( 'user', 'user.user_id = case.customer_id' );
		$this->db->join ( 'state', 'state.state_code = case.current_state_code' );
		$this->db->join ( 'customer_type', 'customer_type.customer_type_id = case.client_type' );
		$this->db->join ( 'case_suspend_reason', 'case_suspend_reason.suspend_code = case.suspend_reason_code', 'left' );
		$this->db->join ( 'lawyer_case', 'lawyer_case.case_id = case.case_id and lawyer_case.lawyer_office_id = case.lawyer_office_id', 'left' );
		
		if($quick_search != 0){
			$query = $this->db->query(self::quick_search_query($search_query, $conditionsString,0, 0, 0));
		}else if ($search_data != null && count ( $search_data ) > 0) {
			if ($search_data ['customer_id'] != null && $search_data ['customer_id'] != "") {
				$this->db->like ( 'case.customer_id', $search_data ['customer_id'], 'none' );
			}
			if ($search_data ['contract_number'] != null && $search_data ['contract_number'] != "") {
				$this->db->like ( 'case.contract_number', $search_data ['contract_number'], 'none' );
			}
			if ($search_data ['debtor_name'] != null && $search_data ['debtor_name'] != "") {
				$this->db->like ( 'case.debtor_name', $search_data ['debtor_name'] );
			}
			if ($search_data ['client_name'] != null && $search_data ['client_name'] != "") {
				$this->db->like ( 'case.client_name', $search_data ['client_name'] );
			}
			if ($search_data ['client_id_number'] != null && $search_data ['client_id_number'] != "") {
				$this->db->like ( 'case.client_id_number', $search_data ['client_id_number'], 'none' );
			}
			if ($search_data ['client_type'] != null && $search_data ['client_type'] != "") {
				$this->db->like ( 'case.client_type', $search_data ['client_type'], 'none' );
			}
			if ($search_data ['obligation_value'] != null && $search_data ['obligation_value'] != "") {
				$this->db->like ( 'case.obligation_value', $search_data ['obligation_value'], 'none' );
			}
			if ($search_data ['number_of_late_instalments'] != null && $search_data ['number_of_late_instalments'] != "") {
				$this->db->like ( 'case.number_of_late_instalments', $search_data ['number_of_late_instalments'], 'none' );
			}
			if ($search_data ['due_amount'] != null && $search_data ['due_amount'] != "") {
				$this->db->like ( 'case.due_amount', $search_data ['due_amount'], 'none' );
			}
			if ($search_data ['remaining_amount'] != null && $search_data ['remaining_amount'] != "") {
				$this->db->like ( 'case.remaining_amount', $search_data ['remaining_amount'], 'none' );
			}
			if ($search_data ['total_debenture_amount'] != null && $search_data ['total_debenture_amount'] != "") {
				$this->db->like ( 'case.total_debenture_amount', $search_data ['total_debenture_amount'], 'none' );
			}
			if ($search_data ['creation_date'] != null && $search_data ['creation_date'] != "") {
				$this->db->like ( 'case.creation_date', $search_data ['creation_date'], 'none' );
			}
			if ($search_data ['executive_order_number'] != null && $search_data ['executive_order_number'] != "") {
				$this->db->like ( 'case.executive_order_number', $search_data ['executive_order_number'], 'none' );
			}
			if ($search_data ['order_number'] != null && $search_data ['order_number'] != "") {
				$this->db->like ( 'case.order_number', $search_data ['order_number'], 'none' );
			}
			if ($search_data ['circle_number'] != null && $search_data ['circle_number'] != "") {
				$this->db->like ( 'case.circle_number', $search_data ['circle_number'], 'none' );
			}
			if ($search_data ['decision_34_number'] != null && $search_data ['decision_34_number'] != "") {
				$this->db->like ( 'case.decision_34_number', $search_data ['decision_34_number'], 'none' );
			}
			if ($search_data ['decision_34_date'] != null && $search_data ['decision_34_date'] != "") {
				$this->db->like ( 'case.decision_34_date', $search_data ['decision_34_date'], 'none' );
			}
			if ($search_data ['decision_46_date'] != null && $search_data ['decision_46_date'] != "") {
				$this->db->like ( 'case.decision_46_date', $search_data ['decision_46_date'], 'none' );
			}
			if ($search_data ['state_code'] != null && $search_data ['state_code'] != "") {
				$this->db->like ( 'case.current_state_code', $search_data ['state_code'], 'none' );
			}
			if ($search_data ['region_id'] != null && $search_data ['region_id'] != "") {
				$this->db->like ( 'case.region_id', $search_data ['region_id'], 'none' );
			}
			if ($search_data ['lawyer_id'] != null && $search_data ['lawyer_id'] != "") {
				$this->db->like ( 'lawyer_case.lawyer_id', $search_data ['lawyer_id'], 'none' );
			}
			if ($search_data ['executive_order_date'] != null && $search_data ['executive_order_date'] != "") {
				$this->db->like ( 'case.executive_order_date', $search_data ['executive_order_date'], 'none' );
			}
			if ($search_data ['advertisement_date'] != null && $search_data ['advertisement_date'] != "") {
				$this->db->like ( 'case.advertisement_date', $search_data ['advertisement_date'], 'none' );
			}
			if ($search_data ['suspend_date'] != null && $search_data ['suspend_date'] != "") {
				$this->db->like ( 'case.suspend_date', $search_data ['suspend_date'], 'none' );
			}
			if ($search_data ['suspend_reason_code'] != null && $search_data ['suspend_reason_code'] != "") {
				$this->db->like ( 'case.suspend_reason_code', $search_data ['suspend_reason_code'], 'none' );
			}
			if ($search_data ['closing_reason'] != null && $search_data ['closing_reason'] != "") {
				$this->db->like ( 'case.closing_reason', $search_data ['closing_reason'] );
			}
			if ($search_data ['case_id'] != null && $search_data ['case_id'] != "") {
				$this->db->like ( 'case.case_id', $search_data ['case_id'] );
			}
		}
		if($quick_search == 0){
			$this->db->where ( $conditionsString );
			$query = $this->db->get ();
		}
		return $query->num_rows ();
	}
	function getCasesRegionCount($lawyer_office_id, $region_id) {
		$conditionsString = "case.region_id =" . $region_id . " AND case.lawyer_office_id = '" . $lawyer_office_id . "'";
		$this->db->select ( 'count(case.case_id)' );
		$this->db->from ( 'case' );
		$this->db->join ( 'user', 'user.user_id = case.customer_id' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		$result = $query->row_array ();
		$count = $result ['count(case.case_id)'];
		return $count;
	}
	function getCustomerCasesRegionCount($customer_id, $region_id) {
		$conditionsString = "case.region_id =" . $region_id . " AND case.customer_id = '" . $customer_id . "'";
		$this->db->select ( 'count(case.case_id)' );
		$this->db->from ( 'case' );
		$this->db->join ( 'user', 'user.user_id = case.customer_id' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		$result = $query->row_array ();
		$count = $result ['count(case.case_id)'];
		return $count;
	}
	function getLawyerCasesRegionCount($lawyer_id, $region_id, $lawyer_office_id) {
		$conditionsString = "case.region_id = $region_id  AND lawyer_case.lawyer_id = $lawyer_id and lawyer_case.lawyer_office_id = $lawyer_office_id ";
		;
		$this->db->select ( 'count(case.case_id)' );
		$this->db->from ( 'case' );
		$this->db->join ( 'lawyer_case', 'lawyer_case.case_id = case.case_id and lawyer_case.lawyer_office_id = case.lawyer_office_id', 'left' );
		$this->db->join ( 'user', 'user.user_id = case.customer_id' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		$result = $query->row_array ();
		$count = $result ['count(case.case_id)'];
		return $count;
	}
	function getCasesStatusCount($lawyer_office_id, $status) {
		$conditionsString = "current_state_code = '" . $status . "' AND case.lawyer_office_id = '" . $lawyer_office_id . "'";
		$this->db->select ( 'case.case_id, case.current_state_code, user.lawyer_office_id' );
		$this->db->from ( 'case' );
		$this->db->join ( 'user', 'user.user_id = case.customer_id' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		return $query->num_rows ();
	}
	function getCustomerCasesStatusCount($customer_id, $status) {
		$conditionsString = "case.customer_id = '" . $customer_id . "' AND case.current_state_code = '" . $status . "'";
		$this->db->select ( 'case.case_id' );
		$this->db->from ( 'case' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		return $query->num_rows ();
	}
	function getLawyerCasesStatusCount($lawyer_id, $lawyer_office_id, $status) {
		$conditionsString = "lawyer_case.lawyer_id = $lawyer_id AND case.current_state_code = '$status' and lawyer_case.lawyer_office_id = $lawyer_office_id";
		$this->db->select ( 'case.case_id' );
		$this->db->from ( 'lawyer_case' );
		$this->db->join ( 'case', 'lawyer_case.case_id = case.case_id and lawyer_case.lawyer_office_id = case.lawyer_office_id' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		return $query->num_rows ();
	}
	function getCasesData($records, $user_type_code) {
		$i = 0;
		$cases_data = array ();
		foreach ( $records as $record ) {
			$cases_data [$i] ['case_id'] = $record ['case_id'];
			$cases_data [$i] ['customer_id'] = $record ['customer_id'];
			$cases_data [$i] ['customer_name'] = $record ['customer_first_name'] . " " . $record ['customer_last_name'];
			$cases_data [$i] ['contract_number'] = $record ['contract_number'];
			$cases_data [$i] ['client_id_number'] = $record ['client_id_number'];
			$cases_data [$i] ['client_name'] = $record ['client_name'];
			$cases_data [$i] ['client_type'] = $record ['client_type'];
			$cases_data [$i] ['customer_type_name'] = $record ['customer_type_name'];
			$cases_data [$i] ['number_of_late_instalments'] = $record ['number_of_late_instalments'];
			$cases_data [$i] ['due_amount'] = $record ['due_amount'];
			$cases_data [$i] ['remaining_amount'] = $record ['remaining_amount'];
			$cases_data [$i] ['total_debenture_amount'] = $record ['total_debenture_amount'];
			$cases_data [$i] ['debtor_name'] = $record ['debtor_name'];
			$cases_data [$i] ['obligation_value'] = $record ['obligation_value'];
			$cases_data [$i] ['debenture'] = $record ['debenture'];
			$cases_data [$i] ['id'] = $record ['id'];
			$cases_data [$i] ['contract'] = $record ['contract'];
			$cases_data [$i] ['others'] = $record ['others'];
			$cases_data [$i] ['creation_date'] = $record ['creation_date'];
			$cases_data [$i] ['last_update_date'] = $record ['last_update_date'];
			$cases_data [$i] ['creation_user'] = $record ['creation_user'];
			$cases_data [$i] ['creation_user_name'] = "";
			$cases_data [$i] ['last_update_user'] = $record ['last_update_user'];
			$cases_data [$i] ['last_update_user_name'] = "";
			$cases_data [$i] ['current_state_code'] = $record ['current_state_code'];
			if ($record ['current_state_code'] == "ASSIGNED_TO_LAWYER") {
				$cases_data [$i] ['state_priotity'] = 1;
			} else {
				$cases_data [$i] ['state_priotity'] = 0;
			}
			$cases_data [$i] ['state_name'] = $record ['state_name'];
			$cases_data [$i] ['executive_order_number'] = $record ['executive_order_number'];
			$cases_data [$i] ['order_number'] = $record ['order_number'];
			$cases_data [$i] ['circle_number'] = $record ['circle_number'];
			$cases_data [$i] ['referral_paper'] = $record ['referral_paper'];
			$cases_data [$i] ['consignment_image'] = $record ['consignment_image'];
			$cases_data [$i] ['decision_34_date'] = $record ['decision_34_date'];
			if (isset ( $record ['decision_34_number'] ) && $record ['decision_34_number'] == "0")
				$cases_data [$i] ['decision_34_number'] = "";
			else
				$cases_data [$i] ['decision_34_number'] = $record ['decision_34_number'];
			$cases_data [$i] ['decision_34_file'] = $record ['decision_34_file'];
			$cases_data [$i] ['advertisement_file'] = $record ['advertisement_file'];
			$cases_data [$i] ['invoice_file'] = $record ['invoice_file'];
			$cases_data [$i] ['decision_46_date'] = $record ['decision_46_date'];
			$cases_data [$i] ['closing_reason'] = $record ['closing_reason'];
			$cases_data [$i] ['suspend_reason_code'] = $record ['suspend_reason_code'];
			$cases_data [$i] ['suspend_date'] = $record ['suspend_date'];
			$cases_data [$i] ['suspend_reason'] = $record ['suspend_reason'];
			$cases_data [$i] ['suspend_file'] = $record ['suspend_file'];
			$cases_data [$i] ['executive_order_date'] = $record ['executive_order_date'];
			$cases_data [$i] ['advertisement_date'] = $record ['advertisement_date'];
			$cases_data [$i] ['region_id'] = $record ['region_id'];
			$cases_data [$i] ['region_name'] = $record ['region_name'];
			$cases_data [$i] ['notes'] = $record ['notes'];
			
			$creation_user_id = $record ['creation_user'];
			if ($creation_user_id != null && $creation_user_id > 0) {
				$conditionsString = "user.user_id = '" . $creation_user_id . "'";
				$this->db->select ( 'user.user_name as creation_user_name, user.last_name,	user.first_name' );
				$this->db->from ( 'user' );
				$this->db->where ( $conditionsString );
				$query = $this->db->get ();
				
				if ($query->num_rows () > 0) {
					$creation_user_records = $query->result_array ();
					$cases_data [$i] ['creation_user_name'] = $creation_user_records [0] ['first_name'] . " " . $creation_user_records [0] ['last_name'];
				}
			}
			
			$last_update_user_id = $record ['last_update_user'];
			if ($last_update_user_id != null && $last_update_user_id > 0) {
				$conditionsString = "user.user_id = '" . $last_update_user_id . "'";
				$this->db->select ( 'user.user_name as last_update_user_name, user.last_name,	user.first_name' );
				$this->db->from ( 'user' );
				$this->db->where ( $conditionsString );
				$query = $this->db->get ();
				
				if ($query->num_rows () > 0) {
					$last_update_user_records = $query->result_array ();
					$cases_data [$i] ['last_update_user_name'] = $last_update_user_records [0] ['first_name'] . " " . $last_update_user_records [0] ['last_name'];
				}
			}
			
			$cases_data [$i] ['lawyer_name'] = "";
			$case_id = $record ['case_id'];
			$lawyer_office_id = $record ['lawyer_office_id'];
			$conditionsString = "lawyer_case.case_id = '$case_id' and lawyer_case.lawyer_office_id = $lawyer_office_id";
			$this->db->select ( 'user.user_name as lawyer_user_name, user.last_name, user.first_name' );
			$this->db->from ( 'lawyer_case' );
			$this->db->join ( 'user', 'user.user_id = lawyer_case.lawyer_id' );
			$this->db->where ( $conditionsString );
			$query = $this->db->get ();
			
			if ($query && $query->num_rows () > 0) {
				$lawyer_user_records = $query->result_array ();
				$cases_data [$i] ['lawyer_name'] = $lawyer_user_records [0] ['first_name'] . " " . $lawyer_user_records [0] ['last_name'];
			}
			
			$cases_data [$i] ['can_edit'] = 0;
			$cases_data [$i] ['action_button_name'] = "";
			if ($record ['current_state_code'] == "MODIFICATION_REQUIRED") {
				$edit_state = self::getEditStateData ( $user_type_code );
				if ($edit_state) {
					$cases_data [$i] ['can_edit'] = 1;
					$cases_data [$i] ['action_button_name'] = $edit_state;
				}
			}
			
			$i = $i + 1;
		}
		return $cases_data;
	}
	function getCasesDataToShow($records, $user_type_code) {
		$i = 0;
		$cases_data = array ();
		foreach ( $records as $record ) {
			if ($record ['suspend_date'] != null)
				$cases_data [$i] ['suspend_date'] = $record ['suspend_date'];
			if ($record ['suspend_file'] != null)
				$cases_data [$i] ['suspend_file'] = $record ['suspend_file'];
			if ($record ['customer_first_name'] != null || $record ['customer_last_name'] != null)
				$cases_data [$i] ['customer_name'] = $record ['customer_first_name'] . " " . $record ['customer_last_name'];
			if ($record ['contract_number'] != null)
				$cases_data [$i] ['contract_number'] = $record ['contract_number'];
			if ($record ['debtor_name'] != null)
				$cases_data [$i] ['debtor_name'] = $record ['debtor_name'];
			if ($record ['client_name'] != null)
				$cases_data [$i] ['client_name'] = $record ['client_name'];
			if ($record ['customer_id'] != null)
				$cases_data [$i] ['customer_id'] = $record ['customer_id'];
			if ($record ['client_id_number'] != null)
				$cases_data [$i] ['client_id_number'] = $record ['client_id_number'];
			if ($record ['client_type'] != null)
				$cases_data [$i] ['client_type'] = $record ['client_type'];
			if ($record ['customer_type_name'] != null)
				$cases_data [$i] ['customer_type_name'] = $record ['customer_type_name'];
			if ($record ['obligation_value'] != null)
				$cases_data [$i] ['obligation_value'] = $record ['obligation_value'];
			if ($record ['remaining_amount'] != null)
				$cases_data [$i] ['remaining_amount'] = $record ['remaining_amount'];
			if ($record ['due_amount'] != null)
				$cases_data [$i] ['due_amount'] = $record ['due_amount'];
			if ($record ['number_of_late_instalments'] != null)
				$cases_data [$i] ['number_of_late_instalments'] = $record ['number_of_late_instalments'];
			if ($record ['total_debenture_amount'] != null)
				$cases_data [$i] ['total_debenture_amount'] = $record ['total_debenture_amount'];
			if ($record ['creation_date'] != null)
				$cases_data [$i] ['creation_date'] = $record ['creation_date'];
			if ($record ['state_name'] != null)
				$cases_data [$i] ['state_name'] = $record ['state_name'];
			if ($record ['executive_order_number'] != null)
				$cases_data [$i] ['executive_order_number'] = $record ['executive_order_number'];
			if ($record ['decision_34_number'] != null)
				if (isset ( $record ['decision_34_number'] ) && $record ['decision_34_number'] == "0")
					$cases_data [$i] ['decision_34_number'] = "";
				else
					$cases_data [$i] ['decision_34_number'] = $record ['decision_34_number'];
			if ($record ['executive_order_date'] != null)
				$cases_data [$i] ['executive_order_date'] = $record ['executive_order_date'];
			if ($record ['decision_34_date'] != null)
				$cases_data [$i] ['decision_34_date'] = $record ['decision_34_date'];
			if ($record ['order_number'] != null)
				$cases_data [$i] ['order_number'] = $record ['order_number'];
			if (isset ( $record ['circle_number'] ))
				$cases_data [$i] ['circle_number'] = $record ['circle_number'];
			if (isset ( $record ['advertisement_date'] ))
				
				$cases_data [$i] ['advertisement_date'] = $record ['advertisement_date'];
			if (isset ( $record ['current_state_code'] ))
				$cases_data [$i] ['current_state_code'] = $record ['current_state_code'];
			
			$cases_data [$i] ['lawyer_name'] = "";
			$cases_data [$i] ['lawyer_id'] = "";
			$case_id = $record ['case_id'];
			$lawyer_office_id = $record ['lawyer_office_id'];
			$conditionsString = "lawyer_case.case_id = '$case_id' and lawyer_case.lawyer_office_id = $lawyer_office_id";
			$this->db->select ( 'user.user_name as lawyer_user_name, user.last_name, user.first_name, user.user_id as lawyer_id' );
			$this->db->from ( 'lawyer_case' );
			$this->db->join ( 'user', 'user.user_id = lawyer_case.lawyer_id' );
			$this->db->where ( $conditionsString );
			$query = $this->db->get ();
			
			if ($query && $query->num_rows () > 0) {
				$lawyer_user_records = $query->result_array ();
				$cases_data [$i] ['lawyer_name'] = $lawyer_user_records [0] ['first_name'] . " " . $lawyer_user_records [0] ['last_name'];
				$cases_data [$i] ['lawyer_id'] = $lawyer_user_records [0] ['lawyer_id'];
			}
			if (isset ( $record ['decision_46_date'] ))
				$cases_data [$i] ['decision_46_date'] = $record ['decision_46_date'];
			if (isset ( $record ['suspend_reason_code'] ))
				$cases_data [$i] ['suspend_reason_code'] = $record ['suspend_reason_code'];
			if (isset ( $record ['suspend_reason'] ))
				$cases_data [$i] ['suspend_reason'] = $record ['suspend_reason'];
				// files
			if (isset ( $record ['consignment_image'] ))
				$cases_data [$i] ['consignment_image'] = $record ['consignment_image'];
			if (isset ( $record ['debenture'] ))
				$cases_data [$i] ['debenture'] = $record ['debenture'];
			if (isset ( $record ['referral_paper'] ))
				$cases_data [$i] ['referral_paper'] = $record ['referral_paper'];
			if (isset ( $record ['id'] ))
				$cases_data [$i] ['id'] = $record ['id'];
			if (isset ( $record ['contract'] ))
				$cases_data [$i] ['contract'] = $record ['contract'];
			if (isset ( $record ['others'] ))
				$cases_data [$i] ['others'] = $record ['others'];
			if (isset ( $record ['advertisement_file'] ))
				$cases_data [$i] ['advertisement_file'] = $record ['advertisement_file'];
			if (isset ( $record ['invoice_file'] ))
				$cases_data [$i] ['invoice_file'] = $record ['invoice_file'];
			if (isset ( $record ['region_name'] ))
				$cases_data [$i] ['region_name'] = $record ['region_name'];
			if (isset ( $record ['closing_reason'] ))
				$cases_data [$i] ['closing_reason'] = $record ['closing_reason'];
			if (isset ( $record ['notes'] ))
				$cases_data [$i] ['notes'] = $record ['notes'];
			if (isset ( $record ['lawyer_office_id'] ))
				$cases_data [$i] ['lawyer_office_id'] = $record ['lawyer_office_id'];
			$cases_data [$i] ['can_edit'] = 0;
			
			$cases_data [$i] ['action_button_name'] = "";
			if ($record ['current_state_code'] == "MODIFICATION_REQUIRED") {
				$edit_state = self::getEditStateData ( $user_type_code );
				if ($edit_state) {
					$cases_data [$i] ['can_edit'] = 1;
					$cases_data [$i] ['action_button_name'] = $edit_state;
				}
			}
			
			$i = $i + 1;
		}
		return $cases_data;
	}
	function getCasesItemData($records, $user_type_code) {
		$i = 0;
		$cases_data = array ();
		foreach ( $records as $record ) {
			$cases_data [$i] ['case_id'] = $record ['case_id'];
			$cases_data [$i] ['client_id_number'] = $record ['client_id_number'];
			$cases_data [$i] ['client_name'] = $record ['client_name'];
			$cases_data [$i] ['last_update_date'] = $record ['last_update_date'];
			if ($record ['current_state_code'] == "ASSIGNED_TO_LAWYER") {
				$cases_data [$i] ['state_priotity'] = 1;
			} else {
				$cases_data [$i] ['state_priotity'] = 0;
			}
			$cases_data [$i] ['state_date'] = $record ['state_date'];
			$cases_data [$i] ['creation_date'] = $record ['creation_date'];
			$cases_data [$i] ['number_of_late_instalments'] = $record ['number_of_late_instalments'];
			$cases_data [$i] ['current_state_code'] = $record ['current_state_code'];
			$i = $i + 1;
		}
		return $cases_data;
	}
	function getInitialStateData($user_type_code) {
		$conditionsString = "state.is_initial = '1' AND usertype_privileges.user_type_code ='" . $user_type_code . "'";
		$this->db->select ( 'state_transition.action_button_text' );
		$this->db->from ( 'state' );
		$this->db->join ( 'state_transition', 'state.state_code = state_transition.source_state' );
		$this->db->join ( 'usertype_privileges', 'usertype_privileges.state_transition = state_transition.id' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			return $records [0] ['action_button_text'];
		} else {
			return false;
		}
	}
	function getEditStateData($user_type_code) {
		$conditionsString = "state.state_code = 'MODIFICATION_REQUIRED' AND usertype_privileges.user_type_code ='" . $user_type_code . "'";
		$this->db->select ( 'state_transition.action_button_text' );
		$this->db->from ( 'state' );
		$this->db->join ( 'state_transition', 'state.state_code = state_transition.target_state  AND state_transition.source_state != state_transition.target_state' );
		$this->db->join ( 'usertype_privileges', 'usertype_privileges.state_transition = state_transition.id' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			return $records [0] ['action_button_text'];
		} else {
			return false;
		}
	}
	function getCaseDetails($user_type_code, $case_id, $lawyer_office_id, $android = 0) {
		$conditionsString = "case.case_id = '" . $case_id . "' and case.lawyer_office_id =  $lawyer_office_id ";
		$this->db->select ( 'case.case_id, case.lawyer_office_id, case.customer_id, user.user_name as customer_name, user.last_name as customer_last_name,
							user.first_name as customer_first_name, case.contract_number, case.client_id_number, case.client_name,
							case.client_type, customer_type.name as customer_type_name, case.number_of_late_instalments, case.due_amount, case.remaining_amount, case.debenture,
							case.id, case.contract, case.others, case.creation_date, case.last_update_date,	case.creation_user,
							case.last_update_user, case.current_state_code, state.state_name, case.executive_order_number, case.decision_34_number,
							case.decision_34_file, case.advertisement_file, case.invoice_file, case.decision_46_date, case.closing_reason, case.notes,
							case.circle_number, case.order_number, case.referral_paper, case.consignment_image, case.decision_34_date, case.suspend_reason_code,
							case.suspend_date, case.suspend_file,
							case_suspend_reason.suspend_reason, case.executive_order_date, case.advertisement_date, case.debtor_name,
							case.obligation_value, case.total_debenture_amount, case.region_id, region.name as region_name' );
		$this->db->from ( 'case' );
		$this->db->join ( 'user', 'user.user_id = case.customer_id' );
		$this->db->join ( 'state', 'state.state_code = case.current_state_code' );
		$this->db->join ( 'customer_type', 'customer_type.customer_type_id = case.client_type' );
		$this->db->join ( 'case_suspend_reason', 'case_suspend_reason.suspend_code = case.suspend_reason_code', 'left' );
		$this->db->join ( 'lawyer_case', 'lawyer_case.case_id = case.case_id and lawyer_case.lawyer_office_id = case.lawyer_office_id', 'left' );
		$this->db->join ( 'region', 'region.region_id = case.region_id', 'left' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			if ($android == 0)
				$cases_data = self::getCasesData ( $records, $user_type_code );
			else
				$cases_data = self::getCasesDataToShow ( $records, $user_type_code );
			return $cases_data [0];
		} else {
			return false;
		}
	}
	function getAvailableCustomers($lawyer_office_id) {
		$conditionsString = "lawyer_office_id = '" . $lawyer_office_id . "' AND (user_type_code = 'CUSTOMER')";
		$this->db->select ( 'user_id,user_name,first_name,last_name' );
		$this->db->from ( 'user' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			return $records;
		} else {
			return false;
		}
	}
	function isCaseValid($case_id) {
		$conditionsString = "case.case_id = '" . $case_id . "'";
		$this->db->select ( 'case.case_id' );
		$this->db->from ( 'case' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		
		if ($query->num_rows () > 0) {
			return true;
		} else {
			return false;
		}
	}
	function isTransExist($tran_id) {
		$conditionsString = "case_transactions.transaction_id = '" . $tran_id . "'";
		$this->db->select ( 'case_transactions.transaction_id' );
		$this->db->from ( 'case_transactions' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		
		if ($query->num_rows () > 0) {
			return true;
		} else {
			return false;
		}
	}
	function getCaseStateAfterCreation() {
		$conditionsString = "state.is_initial = '1'";
		$this->db->select ( 'state_transition.target_state' );
		$this->db->from ( 'state' );
		$this->db->join ( 'state_transition', 'state.state_code = state_transition.source_state' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			return $records [0] ['target_state'];
		} else {
			return false;
		}
	}
	function getCaseDataForTransaction($case_id, $lawyer_office_id) {
		$conditionsString = "case.case_id = '" . $case_id . "' and case.lawyer_office_id ='" . $lawyer_office_id . "'";
		$this->db->select ( '*' );
		$this->db->from ( 'case' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			return $records [0];
		} else {
			return false;
		}
	}
	function getCaseTransactions($case_id, $lawyer_office_id) {
		$conditionsString = "case_id = '" . $case_id . "' AND lawyer_office_id = '" . $lawyer_office_id . "' AND transaction_status = 'Success' AND (action_code IS NULL OR action_code != 'REASSIGN_TO_LAWYER') AND edit_action = 'N'";
		$this->db->select ( 'case_transactions.transaction_id, case_transactions.current_state_code, state.state_name, case_transactions.last_update_date, case_transactions.manual_insert' );
		$this->db->from ( 'case_transactions' );
		$this->db->join ( 'state', 'state.state_code = case_transactions.current_state_code' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			$i = 0;
			$case_transactions = array ();
			$duplicate_indices = array ();
			foreach ( $records as $record ) {
				for($j = 0; $j < $i; $j ++) {
					// remove previous modification required and under review since they are repeated which saved in $duplicate_indices
					if ($record ['current_state_code'] == "MODIFICATION_REQUIRED" && $case_transactions [$j] ['state_code'] == $record ['current_state_code']) {
						array_push ( $duplicate_indices, $j );
						array_push ( $duplicate_indices, $j + 1 );
					}
				}
				if ($record ['manual_insert'] == "Y") {
					// get supposed previous transactions
					$main_transactions = array ();
					self::getRemainingStates ( "INITIAL", $main_transactions );
					$last_state_code = "";
					foreach ( $main_transactions as $main_tran ) {
						$closed_state_valid = true;
						// if closed check if passed by state before adding it to progress bar
						if ($record ['current_state_code'] == "CLOSED") {
							$valid = self::checkStateValidForClosedCase ( $case_id, $lawyer_office_id, $main_tran ['state_code'], $last_state_code );
							if (! $valid) {
								$closed_state_valid = false;
							}
						}
						// stop at under review in case of modification required or closed and didnt pass by that state and otherwise stop at equivalent state code
						if ($record ['current_state_code'] == $main_tran ['state_code'] || ($record ['current_state_code'] == "MODIFICATION_REQUIRED" && $main_tran ['state_code'] == "ASSIGNED_TO_LAWYER") || ($record ['current_state_code'] == "CLOSED" && ! $closed_state_valid)) {
							break;
						} else {
							$case_transactions [$i] ['state_code'] = $main_tran ['state_code'];
							$case_transactions [$i] ['state_name'] = $main_tran ['state_name'];
							$case_transactions [$i] ['state_date'] = self::getStateDate ( $case_id, $lawyer_office_id, $main_tran ['state_code'], "", $i );
							$last_state_code = $main_tran ['state_code'];
							$i = $i + 1;
						}
					}
				}
				$case_transactions [$i] ['state_code'] = $record ['current_state_code'];
				$case_transactions [$i] ['state_name'] = $record ['state_name'];
				$state_trans_id = $record ['transaction_id'];
				$case_transactions [$i] ['state_date'] = self::getStateDate ( $case_id, $lawyer_office_id, $record ['current_state_code'], $record ['last_update_date'], $i );
				$i = $i + 1;
			}
			$final_case_transactions = array ();
			$x = 0;
			for($k = 0; $k < $i; $k ++) {
				if (! in_array ( $k, $duplicate_indices )) {
					array_push ( $final_case_transactions, $case_transactions [$k] );
				}
			}
			$case_transactions = $final_case_transactions;
			$i = count ( $case_transactions );
			self::getRemainingStates ( $case_transactions [$i - 1] ['state_code'], $case_transactions );
			return $case_transactions;
		} else {
			return false;
		}
	}
	function getOrderedStates() {
		$conditionsString = "state.is_initial = '1'";
		$this->db->select ( 'state.state_code, state.state_name' );
		$this->db->from ( 'state' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			$i = 0;
			$case_transactions = array ();
			$case_transactions [0] ['state_code'] = $records [0] ['state_code'];
			$case_transactions [0] ['state_name'] = $records [0] ['state_name'];
			$case_transactions [0] ['state_date'] = "";
			$case_transactions [0] ['state_time'] = "";
			self::getRemainingStates ( $case_transactions [0] ['state_code'], $case_transactions );
			return $case_transactions;
		} else {
			return false;
		}
	}
	function getAvailableStatesOrdered() {
		$conditionsString = "state.is_initial = '1'";
		$this->db->select ( 'state.state_code, state.state_name' );
		$this->db->from ( 'state' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			$i = 0;
			$case_transactions = array ();
			$conditionsString = "state.state_code = 'MODIFICATION_REQUIRED'";
			$this->db->select ( 'state.state_code, state.state_name' );
			$this->db->from ( 'state' );
			$this->db->where ( $conditionsString );
			$query = $this->db->get ();
			if ($query->num_rows () > 0) {
				$result = $query->result_array ();
				$j = count ( $case_transactions );
				$case_transactions [$j] ['state_code'] = $result [0] ['state_code'];
				$case_transactions [$j] ['state_name'] = $result [0] ['state_name'];
				$case_transactions [$j] ['state_date'] = "";
				$case_transactions [$j] ['state_time'] = "";
			}
			
			self::getRemainingStates ( $case_transactions [0] ['state_code'], $case_transactions );
			
			$conditionsString = "state.state_code = 'SUSPENDED'";
			$this->db->select ( 'state.state_code, state.state_name' );
			$this->db->from ( 'state' );
			$this->db->where ( $conditionsString );
			$query = $this->db->get ();
			if ($query->num_rows () > 0) {
				$result = $query->result_array ();
				$j = count ( $case_transactions );
				$case_transactions [$j] ['state_code'] = $result [0] ['state_code'];
				$case_transactions [$j] ['state_name'] = $result [0] ['state_name'];
				$case_transactions [$j] ['state_date'] = "";
				$case_transactions [$j] ['state_time'] = "";
			}
			return $case_transactions;
		} else {
			return false;
		}
	}
	function getAllRegions($user_type_code, $lawyer_office_id, $user_id) {
		$this->db->select ( 'region.region_id, region.name' );
		$this->db->where ( 'lawyer_office_id =' . $lawyer_office_id );
		$this->db->from ( 'region' );
		$query = $this->db->get ();
		$regions = array ();
		$sum = 0;
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			$i = 0;
			foreach ( $records as $record ) {
				$regions [$i] ["region_id"] = $record ['region_id'];
				$regions [$i] ["name"] = $record ['name'];
				switch ($user_type_code) {
					case "ADMIN" :
						$regions [$i] ["count"] = self::getCasesRegionCount ( $lawyer_office_id, $regions [$i] ["region_id"] );
						break;
					case "CUSTOMER" :
						$regions [$i] ["count"] = self::getCustomerCasesRegionCount ( $user_id, $regions [$i] ["region_id"] );
						break;
					case "LAWYER" :
						$regions [$i] ["count"] = self::getLawyerCasesRegionCount ( $user_id, $regions [$i] ["region_id"], $lawyer_office_id );
						break;
					case "SECRETARY" :
						$regions [$i] ["count"] = self::getCasesRegionCount ( $lawyer_office_id, $regions [$i] ["region_id"] );
						break;
					default :
						$regions [$i] ["count"] = self::getCasesRegionCount ( $lawyer_office_id, $regions [$i] ["region_id"] );
				}
				$i = $i + 1;
			}
			return $regions;
		}
		return null;
	}
	function getAllOrderedStates() {
		$conditionsString = "state.is_initial = '1'";
		$this->db->select ( 'state.state_code, state.state_name' );
		$this->db->from ( 'state' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			$i = 0;
			$case_transactions = array ();
			$case_transactions [0] ['state_code'] = $records [0] ['state_code'];
			$case_transactions [0] ['state_name'] = $records [0] ['state_name'];
			$case_transactions [0] ['state_date'] = "";
			$case_transactions [0] ['state_time'] = "";
			
			$conditionsString = "state.state_code = 'MODIFICATION_REQUIRED'";
			$this->db->select ( 'state.state_code, state.state_name' );
			$this->db->from ( 'state' );
			$this->db->where ( $conditionsString );
			$query = $this->db->get ();
			if ($query->num_rows () > 0) {
				$result = $query->result_array ();
				$j = count ( $case_transactions );
				$case_transactions [$j] ['state_code'] = $result [0] ['state_code'];
				$case_transactions [$j] ['state_name'] = $result [0] ['state_name'];
				$case_transactions [$j] ['state_date'] = "";
				$case_transactions [$j] ['state_time'] = "";
			}
			
			self::getRemainingStates ( $case_transactions [0] ['state_code'], $case_transactions );
			
			$conditionsString = "state.state_code = 'SUSPENDED'";
			$this->db->select ( 'state.state_code, state.state_name' );
			$this->db->from ( 'state' );
			$this->db->where ( $conditionsString );
			$query = $this->db->get ();
			if ($query->num_rows () > 0) {
				$result = $query->result_array ();
				$j = count ( $case_transactions );
				$case_transactions [$j] ['state_code'] = $result [0] ['state_code'];
				$case_transactions [$j] ['state_name'] = $result [0] ['state_name'];
				$case_transactions [$j] ['state_date'] = "";
				$case_transactions [$j] ['state_time'] = "";
			}
			return $case_transactions;
		} else {
			return false;
		}
	}
	function getRemainingStates($current_state, &$case_transactions, $android = 0) {
		$i = sizeof ( $case_transactions );
		$states_exist = true;
		while ( $states_exist ) {
			// primary to ones appears in progress bar
			// target != source to ignore reassign to lawyer
			$conditionsString = "state_transition.source_state = '" . $current_state . "' AND state_transition.source_state != state_transition.target_state AND state_transition.primary = '1'";
			$this->db->select ( 'state_transition.target_state, state.state_name' );
			$this->db->from ( 'state' );
			$this->db->join ( 'state_transition', 'state.state_code = state_transition.target_state' );
			$this->db->where ( $conditionsString );
			$query = $this->db->get ();
			
			if ($query->num_rows () > 0) {
				$records = $query->result_array ();
				// if more than 1 transitions returned then ignore the modification required since we dont need it and ignore close since there is another transition available and we dont need to close the cycle
				if ($query->num_rows () > 1) {
					foreach ( $records as $record ) {
						if ($record ['target_state'] != "MODIFICATION_REQUIRED" && $record ['target_state'] != "CLOSED") {
							$case_transactions [$i] ['state_code'] = $record ['target_state'];
							$case_transactions [$i] ['state_name'] = $record ['state_name'];
						}
					}
				} else {
					$case_transactions [$i] ['state_code'] = $records [0] ['target_state'];
					$case_transactions [$i] ['state_name'] = $records [0] ['state_name'];
				}
				if ($android == 0) {
					$case_transactions [$i] ['state_date'] = "";
					$case_transactions [$i] ['state_time'] = "";
				}
				$current_state = $case_transactions [$i] ['state_code'];
			} else {
				$states_exist = false;
			}
			$i = $i + 1;
		}
	}
	function getCaseId($lawyer_office_id) {
		$this->db->select ( 'case.case_id' );
		$this->db->from ( 'case' );
		$this->db->where ( 'lawyer_office_id = ' . $lawyer_office_id );
		$this->db->order_by ( 'case_id', 'asc' );
		$query = $this->db->get ();
		$case_num = 0;
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			foreach ( $records as $record ) {
				if (strlen ( $record ['case_id'] ) == 6) {
					$case_num = substr ( $record ['case_id'], 1, 5 );
					$case_num ++;
				}
			}
		}
		$case_num = sprintf ( "%'.05d", $case_num );
		return "C" . $case_num;
	}
	function getTransactionId($where = "") {
		$this->db->select ( 'transaction_id, current_state_code' );
		$this->db->from ( 'case_transactions' );
		if($where != ""){
			$this->db->where($where);
			$this->db->order_by ( 'transaction_id', 'DESC' );
		} else {
			$this->db->order_by ( 'transaction_id', 'asc' );
		}
		$query = $this->db->get ();
		$tran_num = 0;
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			if($where != ""){
				return $records [0];
			}
			foreach ( $records as $record ) {
				if (strlen ( $record ['transaction_id'] ) == 10) {
					$tran_num = substr ( $record ['transaction_id'], 3, 7 );
					$tran_num ++;
				}
			}
		}
		$tran_num = sprintf ( "%'.07d", $tran_num );

		return "TRN" . $tran_num;
	}
	
	function getUserTypeWithActionPrvlg($action_code){
		$query = $this->db->query("SELECT user_type_code FROM `usertype_privileges` WHERE state_transition in (SELECT id FROM `state_transition` WHERE action_code = '".$action_code."')");
		if ($query->num_rows () > 0) {
			$user_types = $query->result_array ();
			return $user_types;
		}
		return null;
	}
	function getCaseDebenture($case_id, $lawyer_office_id) {
		$conditionsString = "case.case_id = '" . $case_id . "' and case.lawyer_office_id ='" . $lawyer_office_id . "'";
		$this->db->select ( 'case.debenture' );
		$this->db->from ( 'case' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			return $records [0] ['debenture'];
		} else {
			return "";
		}
	}
	function getAllOfficeCases($lawyer_office_id) {
		$conditionsString = "case.lawyer_office_id = '" . $lawyer_office_id . "'";
		$this->db->select ( 'case.case_id, case.creation_date' );
		$this->db->from ( 'case' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			return $records;
		} else {
			return false;
		}
	}
	function getStateDate($case_id, $lawyer_office_id, $state_code, $last_update_date, $trans_order) {
		$conditionsString = "case.case_id = '" . $case_id . "' and lawyer_office_id = '" . $lawyer_office_id . "'";
		$this->db->select ( 'case.creation_date, case.executive_order_date, case.decision_34_date, case.advertisement_date, case.decision_46_date, case.suspend_date' );
		$this->db->from ( 'case' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		$state_date = "";
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			switch ($state_code) {
				case "UNDER_REVIEW" :
					//echo "UNDER_REVIEW"." ".$trans_order. " ".$last_update_date;
					if ($trans_order == 0 && $records [0] ['creation_date'] != null && $records [0] ['creation_date'] != "" && $records [0] ['creation_date'] != "0000-00-00") {
						$state_date = $records [0] ['creation_date'];
					} else {
						$state_date = $last_update_date;
					}
					break;
				case "REGISTERED" :
					if ($records [0] ['executive_order_date'] != null && $records [0] ['executive_order_date'] != "" && $records [0] ['executive_order_date'] != "0000-00-00") {
						$state_date = $records [0] ['executive_order_date'];
					}
					break;
				case "DECISION_34" :
					if ($records [0] ['decision_34_date'] != null && $records [0] ['decision_34_date'] != "" && $records [0] ['decision_34_date'] != "0000-00-00") {
						$state_date = $records [0] ['decision_34_date'];
					}
					break;
				case "PUBLICLY_ADVERTISED" :
					if ($records [0] ['advertisement_date'] != null && $records [0] ['advertisement_date'] != "" && $records [0] ['advertisement_date'] != "0000-00-00") {
						$state_date = $records [0] ['advertisement_date'];
					}
					break;
				case "DECISION_46" :
					if ($records [0] ['decision_46_date'] != null && $records [0] ['decision_46_date'] != "" && $records [0] ['decision_46_date'] != "0000-00-00") {
						$state_date = $records [0] ['decision_46_date'];
					}
					break;
				case "SUSPENDED" :
					if ($records [0] ['suspend_date'] != null && $records [0] ['suspend_date'] != "" && $records [0] ['suspend_date'] != "0000-00-00") {
						$state_date = $records [0] ['suspend_date'];
					}
					break;
				default :
					$state_date = $last_update_date;
			}
		}
		return $state_date;
	}
	function getCaseLastStateFromTransactions($case_id, $lawyer_office_id) {
		$conditionsString = "case_id = '" . $case_id . "' AND lawyer_office_id ='" . $lawyer_office_id . "' AND transaction_status = 'Success' AND edit_action = 'N'";
		$this->db->select ( 'case_transactions.transaction_id, case_transactions.current_state_code' );
		$this->db->from ( 'case_transactions' );
		$this->db->order_by ( 'transaction_id', 'desc' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		$last_state_code = "";
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			if (count ( $records ) >= 2) {
				$last_state_code = $records [1] ['current_state_code'];
			}
		}
		return $last_state_code;
	}
	function checkStateValidForClosedCase($case_id, $lawyer_office_id, $state_code, $last_state_code) {
		$conditionsString = "case.case_id = '" . $case_id . "' and case.lawyer_office_id ='" . $lawyer_office_id . "'";
		$this->db->select ( 'case.creation_date, case.executive_order_date, case.decision_34_date, case.advertisement_date, case.decision_46_date' );
		$this->db->from ( 'case' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		$state_valid = true;
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			switch ($state_code) {
				case "REGISTERED" :
					if ($records [0] ['executive_order_date'] == null || $records [0] ['executive_order_date'] == "" || $records [0] ['executive_order_date'] == "0000-00-00") {
						$state_valid = false;
					}
					break;
				case "DECISION_34" :
					if ($records [0] ['decision_34_date'] == null || $records [0] ['decision_34_date'] == "" || $records [0] ['decision_34_date'] == "0000-00-00") {
						$state_valid = false;
					}
					break;
				case "PUBLICLY_ADVERTISED" :
					if ($records [0] ['advertisement_date'] == null || $records [0] ['advertisement_date'] == "" || $records [0] ['advertisement_date'] == "0000-00-00") {
						$state_valid = false;
					}
					break;
				case "DECISION_46" :
					if ($records [0] ['decision_46_date'] == null || $records [0] ['decision_46_date'] == "" || $records [0] ['decision_46_date'] == "0000-00-00") {
						$state_valid = false;
					}
					break;
				default :
					$state_valid = true;
			}
		}
		// update case by correct last state code
		$case_data = array ();
		$case_data ['last_state_code'] = $last_state_code;
		$where = "case_id = '" . $case_id . "'";
		$this->db->update ( "case", $case_data, $where );
		return $state_valid;
	}
	function getExportedCasesDatabaseInsert($lawyer_office_id, $user_type_code, $user_id) {
		if ($user_type_code == "CUSTOMER") {
			$conditionsString = "case.customer_id = '" . $user_id . "'";
		} else {
			$conditionsString = "case.lawyer_office_id = '" . $lawyer_office_id . "'";
		}
		$this->db->select ( 'case.case_id, case.lawyer_office_id, case.customer_id, case.contract_number, case.client_id_number, case.client_name,
							case.client_type, case.number_of_late_instalments, case.due_amount,
							case.remaining_amount, case.debenture, case.id, case.contract, case.others, case.creation_date, case.last_update_date,
							case.creation_user,	case.last_update_user, case.current_state_code, case.executive_order_number,
							case.decision_34_number, case.advertisement_file, case.invoice_file, case.decision_46_date,
							case.closing_reason, case.notes, case.circle_number, case.order_number, case.referral_paper, case.consignment_image, case.decision_34_date,
							case.suspend_reason_code,case.suspend_date,case.suspend_file, case.executive_order_date, case.advertisement_date,
							case.debtor_name, case.obligation_value, case.total_debenture_amount, case.region_id' );
		$this->db->from ( 'case' );
		$this->db->join ( 'lawyer_case', 'lawyer_case.case_id = case.case_id and lawyer_case.lawyer_office_id = case.lawyer_office_id', 'left' );
		$this->db->join ( 'user', 'user.user_id = case.customer_id' );
		$this->db->group_by ( 'case.case_id' ); // add group_by
		$this->db->where ( $conditionsString );
		$this->db->order_by ( 'number_of_late_instalments desc, creation_date asc' );
		$query = $this->db->get ();
		$cases_data = array ();
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			$i = 0;
			foreach ( $records as $record ) {
				$cases_data [$i] ['رمز القضية'] = $record ['case_id'];
				$cases_data [$i] ['العميل'] = $record ['customer_id'];
				$cases_data [$i] ['رقم العقد'] = $record ['contract_number'];
				$cases_data [$i] ['اسم المدين'] = $record ['debtor_name'];
				$cases_data [$i] ['اسم المنفذ ضده'] = $record ['client_name'];
				$cases_data [$i] ['رقم هوية المدين'] = $record ['client_id_number'];
				$cases_data [$i] ['نوع المدين'] = $record ['client_type'];
				$cases_data [$i] ['قيمة السند'] = (strlen ( $record ['obligation_value'] ) > 0 ? $record ['obligation_value'] : "");
				$cases_data [$i] ['عدد الأقساط المتأخرة'] = $record ['number_of_late_instalments'];
				$cases_data [$i] ['المبلغ المستحق'] = (strlen ( $record ['due_amount'] ) > 0 ? $record ['due_amount'] : "");
				$cases_data [$i] ['المبلغ المتبقي'] = (strlen ( $record ['remaining_amount'] ) > 0 ? $record ['remaining_amount'] : "");
				$cases_data [$i] ['القيمة الكلية في السند التنفيذي'] = (strlen ( $record ['total_debenture_amount'] ) > 0 ? $record ['total_debenture_amount'] : "");
				$cases_data [$i] ['تاريخ إنشاء القضية'] = $record ['creation_date'];
				$cases_data [$i] ['رقم القيد'] = $record ['executive_order_number'];
				$cases_data [$i] ['رقم الطلب'] = $record ['order_number'];
				$cases_data [$i] ['رقم الدائرة'] = $record ['circle_number'];
				$cases_data [$i] ['رقم قرار 34'] = $record ['decision_34_number'];
				$cases_data [$i] ['تاريخ قرار 34'] = $record ['decision_34_date'];
				$cases_data [$i] ['تاريخ قرار 46'] = $record ['decision_46_date'];
				$cases_data [$i] ['الحالة'] = $record ['current_state_code'];
				$cases_data [$i] ['المنطقة'] = $record ['region_id'];
				if ($user_type_code != "CUSTOMER") {
					$cases_data [$i] ['المحامي'] = "";
					$case_id = $record ['case_id'];
					$conditionsString = "lawyer_case.case_id = '" . $case_id . "' and lawyer_case.lawyer_office_id = '" . $lawyer_office_id . "'";
					$this->db->select ( 'lawyer_case.lawyer_id' );
					$this->db->from ( 'lawyer_case' );
					$this->db->where ( $conditionsString );
					$query = $this->db->get ();
					
					if ($query->num_rows () > 0) {
						$lawyer_user_records = $query->result_array ();
						$cases_data [$i] ['المحامي'] = $lawyer_user_records [0] ['lawyer_id'];
					}
				}
				
				$cases_data [$i] ['تاريخ القيد'] = $record ['executive_order_date'];
				$cases_data [$i] ['تاريخ الإعلان'] = $record ['advertisement_date'];
				$cases_data [$i] ['سبب الامهال'] = $record ['suspend_reason_code'];
				$cases_data [$i] ['صورة الإرسالية'] = $record ['consignment_image'];
				$cases_data [$i] ['سند لأمر'] = $record ['debenture'];
				$cases_data [$i] ['ورقة الإحالة'] = $record ['referral_paper'];
				$cases_data [$i] ['الهوية'] = $record ['id'];
				$cases_data [$i] ['العقد'] = $record ['contract'];
				
				$others_urls = "";
				if (strlen ( $record ['others'] ) > 0) {
					$others = explode ( ",", $record ["others"] );
					foreach ( $others as $other ) {
						if (strlen ( $other ) > 0) {
							if (strlen ( $others_urls ) > 0) {
								$others_urls .= ", ";
							}
							$others_urls .= $other;
						}
					}
				}
				
				$cases_data [$i] ['ملفات أخرى'] = $others_urls;
				$cases_data [$i] ['ملف الإعلان'] = $record ['advertisement_file'];
				$cases_data [$i] ['ملف الفاتورة'] = $record ['invoice_file'];
				$cases_data [$i] ['سبب انتهاء القضية'] = $record ['closing_reason'];
				$cases_data [$i] ['تاريخ الامهال'] = $record ['suspend_date'];
				$cases_data [$i] ['الشخص المنشئ للقضية'] = $record ['creation_user'];
				$cases_data [$i] ['آخر شخص عدل بالقضية'] = $record ['last_update_user'];
				$cases_data [$i] ['تاريخ آخر تعديل بالقضية'] = $record ['last_update_date'];
				$cases_data [$i] ['ملاحظات'] = $record ['notes'];
				$cases_data [$i] ['ملف الامهال'] = $record ['suspend_file'];
				
				$i = $i + 1;
			}
		}
		return $cases_data;
	}
	function getExportedCasesData($lawyer_office_id, $user_type_code, $user_id) {
		if ($user_type_code == "CUSTOMER") {
			$conditionsString = "case.customer_id = '" . $user_id . "'";
		} else {
			$conditionsString = "case.lawyer_office_id = '" . $lawyer_office_id . "'";
		}
		$this->db->select ( 'case.case_id, case.lawyer_office_id, case.customer_id, user.user_name as customer_name, user.last_name as customer_last_name,
							user.first_name as customer_first_name, case.contract_number, case.client_id_number, case.client_name,
							case.client_type, customer_type.name as customer_type_name, case.number_of_late_instalments, case.due_amount,
							case.remaining_amount, case.debenture, case.id, case.contract, case.others, case.creation_date, case.last_update_date,
							case.creation_user,	case.last_update_user, case.current_state_code, state.state_name, case.executive_order_number,
							case.decision_34_number, case.advertisement_file, case.invoice_file, case.decision_46_date,
							case.closing_reason, case.notes, case.circle_number, case.order_number, case.referral_paper, case.consignment_image, case.decision_34_date,
							case.suspend_reason_code,case.suspend_date, case_suspend_reason.suspend_reason,case.suspend_file, case.executive_order_date, case.advertisement_date,
							case.debtor_name, case.obligation_value, case.total_debenture_amount, region.name as region_name' );
		$this->db->from ( 'case' );
		$this->db->join ( 'case_suspend_reason', 'case_suspend_reason.suspend_code = case.suspend_reason_code', 'left' );
		$this->db->join ( 'lawyer_case', 'lawyer_case.case_id = case.case_id and lawyer_case.lawyer_office_id = case.lawyer_office_id', 'left' );
		$this->db->join ( 'user', 'user.user_id = case.customer_id' );
		$this->db->join ( 'state', 'state.state_code = case.current_state_code' );
		$this->db->join ( 'customer_type', 'customer_type.customer_type_id = case.client_type' );
		$this->db->join ( 'region', 'region.region_id = case.region_id', 'left' );
		$this->db->group_by ( 'case.case_id' ); // add group_by
		$this->db->where ( $conditionsString );
		$this->db->order_by ( 'number_of_late_instalments desc, creation_date asc' );
		$query = $this->db->get ();
		
		$cases_data = array ();
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			$i = 0;
			foreach ( $records as $record ) {
				$cases_data [$i] ['رمز القضية'] = $record ['case_id'];
				$cases_data [$i] ['العميل'] = $record ['customer_first_name'] . " " . $record ['customer_last_name'];
				$cases_data [$i] ['رقم العقد'] = $record ['contract_number'];
				$cases_data [$i] ['اسم المدين'] = $record ['debtor_name'];
				$cases_data [$i] ['اسم المنفذ ضده'] = $record ['client_name'];
				$cases_data [$i] ['رقم هوية المدين'] = $record ['client_id_number'];
				$cases_data [$i] ['نوع المدين'] = $record ['customer_type_name'];
				$cases_data [$i] ['قيمة السند'] = (strlen ( $record ['obligation_value'] ) > 0 ? $record ['obligation_value'] . " ريال سعودى" : "");
				$cases_data [$i] ['عدد الأقساط المتأخرة'] = $record ['number_of_late_instalments'];
				$cases_data [$i] ['المبلغ المستحق'] = (strlen ( $record ['due_amount'] ) > 0 ? $record ['due_amount'] . " ريال سعودى" : "");
				$cases_data [$i] ['المبلغ المتبقي'] = (strlen ( $record ['remaining_amount'] ) > 0 ? $record ['remaining_amount'] . " ريال سعودى" : "");
				$cases_data [$i] ['القيمة الكلية في السند التنفيذي'] = (strlen ( $record ['total_debenture_amount'] ) > 0 ? $record ['total_debenture_amount'] . " ريال سعودى" : "");
				$cases_data [$i] ['تاريخ إنشاء القضية'] = $record ['creation_date'];
				$cases_data [$i] ['رقم القيد'] = $record ['executive_order_number'];
				$cases_data [$i] ['رقم الطلب'] = $record ['order_number'];
				$cases_data [$i] ['رقم الدائرة'] = $record ['circle_number'];
				$cases_data [$i] ['رقم قرار 34'] = $record ['decision_34_number'];
				$cases_data [$i] ['تاريخ قرار 34'] = $record ['decision_34_date'];
				$cases_data [$i] ['تاريخ قرار 46'] = $record ['decision_46_date'];
				$cases_data [$i] ['الحالة'] = $record ['state_name'];
				$cases_data [$i] ['المنطقة'] = $record ['region_name'];
				if ($user_type_code != "CUSTOMER") {
					$cases_data [$i] ['اسم المحامي'] = "";
					$case_id = $record ['case_id'];
					$conditionsString = "lawyer_case.case_id = '" . $case_id . "'";
					$this->db->select ( 'user.user_name as lawyer_user_name, user.last_name, user.first_name' );
					$this->db->from ( 'user' );
					$this->db->join ( 'lawyer_case', 'user.user_id = lawyer_case.lawyer_id' );
					$this->db->where ( $conditionsString );
					$query = $this->db->get ();
					
					if ($query->num_rows () > 0) {
						$lawyer_user_records = $query->result_array ();
						$cases_data [$i] ['اسم المحامي'] = $lawyer_user_records [0] ['first_name'] . " " . $lawyer_user_records [0] ['last_name'];
					}
				}
				
				$cases_data [$i] ['تاريخ القيد'] = $record ['executive_order_date'];
				$cases_data [$i] ['تاريخ الإعلان'] = $record ['advertisement_date'];
				$cases_data [$i] ['سبب الامهال'] = $record ['suspend_reason'];
				$cases_data [$i] ['صورة الإرسالية'] = (strlen ( $record ['consignment_image'] ) > 0 ? base_url () . $record ['consignment_image'] : $record ['consignment_image']);
				$cases_data [$i] ['سند لأمر'] = (strlen ( $record ['debenture'] ) > 0 ? base_url () . $record ['debenture'] : $record ['debenture']);
				$cases_data [$i] ['ورقة الإحالة'] = (strlen ( $record ['referral_paper'] ) > 0 ? base_url () . $record ['referral_paper'] : $record ['referral_paper']);
				$cases_data [$i] ['الهوية'] = (strlen ( $record ['id'] ) > 0 ? base_url () . $record ['id'] : $record ['id']);
				$cases_data [$i] ['العقد'] = (strlen ( $record ['contract'] ) > 0 ? base_url () . $record ['contract'] : $record ['contract']);
				
				$others_urls = "";
				if (strlen ( $record ['others'] ) > 0) {
					$others = explode ( ",", $record ["others"] );
					foreach ( $others as $other ) {
						if (strlen ( $other ) > 0) {
							if (strlen ( $others_urls ) > 0) {
								$others_urls .= ", ";
							}
							$others_urls .= base_url () . $other;
						}
					}
				}
				
				$cases_data [$i] ['ملفات أخرى'] = $others_urls;
				$cases_data [$i] ['ملف الإعلان'] = (strlen ( $record ['advertisement_file'] ) > 0 ? base_url () . $record ['advertisement_file'] : $record ['advertisement_file']);
				$cases_data [$i] ['ملف الفاتورة'] = (strlen ( $record ['invoice_file'] ) > 0 ? base_url () . $record ['invoice_file'] : $record ['invoice_file']);
				$cases_data [$i] ['سبب انتهاء القضية'] = $record ['closing_reason'];
				$cases_data [$i] ['تاريخ الامهال'] = $record ['suspend_date'];
				$cases_data [$i] ['ملف الفاتورة'] = (strlen ( $record ['invoice_file'] ) > 0 ? base_url () . $record ['invoice_file'] : $record ['invoice_file']);
				$cases_data [$i] ['ملف الامهال'] = (strlen ( $record ['suspend_file'] ) > 0 ? base_url () . $record ['suspend_file'] : $record ['suspend_file']);
				$cases_data [$i] ['الشخص المنشئ للقضية'] = "";
				$creation_user_id = $record ['creation_user'];
				if ($creation_user_id != null && $creation_user_id > 0) {
					$conditionsString = "user.user_id = '" . $creation_user_id . "'";
					$this->db->select ( 'user.user_name as creation_user_name, user.last_name,	user.first_name' );
					$this->db->from ( 'user' );
					$this->db->where ( $conditionsString );
					$query = $this->db->get ();
					
					if ($query->num_rows () > 0) {
						$creation_user_records = $query->result_array ();
						$cases_data [$i] ['الشخص المنشئ للقضية'] = $creation_user_records [0] ['first_name'] . " " . $creation_user_records [0] ['last_name'];
					}
				}
				$cases_data [$i] ['آخر شخص عدل بالقضية'] = "";
				$last_update_user_id = $record ['last_update_user'];
				if ($last_update_user_id != null && $last_update_user_id > 0) {
					$conditionsString = "user.user_id = '" . $last_update_user_id . "'";
					$this->db->select ( 'user.user_name as last_update_user_name, user.last_name,	user.first_name' );
					$this->db->from ( 'user' );
					$this->db->where ( $conditionsString );
					$query = $this->db->get ();
					
					if ($query->num_rows () > 0) {
						$last_update_user_records = $query->result_array ();
						$cases_data [$i] ['آخر شخص عدل بالقضية'] = $last_update_user_records [0] ['first_name'] . " " . $last_update_user_records [0] ['last_name'];
					}
				}
				$cases_data [$i] ['تاريخ آخر تعديل بالقضية'] = $record ['last_update_date'];
				$cases_data [$i] ['ملاحظات'] = $record ['notes'];
				$i = $i + 1;
			}
		}
		return $cases_data;
	}
	function get_cases_count($user_id) {
		$this->load->model ( "common" );
		
		$where = "where user_id =" . $user_id;
		$userRecord = $this->common->getOneRow ( 'user', $where );
		$lawyer_office_id = $userRecord ['lawyer_office_id'];
		/*
		 * $data ['profile_first_name'] = $userRecord ['first_name']; $data ['profile_last_name'] = $userRecord ['last_name']; $data ['profile_img'] = $userRecord ['profile_img']; $notifications = $this->notification_model->getLatestDeliveredNotificationsForUser ( $user_id ); $data ['notifications_list'] = $notifications ['notifications_list']; $data ['count_unseen'] = $notifications ['count_unseen']; $data ['page'] = "case_management/view_cases/" . $status; $data ['search_mode'] = $search_mode;
		 */
		
		$user_type_code = $userRecord ['user_type_code'];
		// $data ['user_type_code'] = $user_type_code;
		// $data ['cases_count'] = 0;
		
		/*
		 * $initialState = $this->case_model->getInitialStateData ( $user_type_code ); if ($initialState) { $data ['can_add'] = 1; $data ['add_button_name'] = $initialState; } else { $data ['can_add'] = 0; $data ['add_button_name'] = ""; }
		 */
		
		// $data ['selected_state'] = $status;
		$ordered_states = self::getOrderedStatesToShow ();
		$data ['ordered_states'] = $ordered_states;
		$sum = 0;
		
		foreach ( $ordered_states as $state ) {
			$state_code = $state ['state_code'];
			$state_count_name = $state_code;
			$data [$state_count_name] = 0;
			
			switch ($user_type_code) {
				case "ADMIN" :
					$data ['counts'] [0] [$state_count_name] = self::getCasesStatusCount ( $lawyer_office_id, $state_code );
					break;
				case "CUSTOMER" :
					$data ['counts'] [0] [$state_count_name] = self::getCustomerCasesStatusCount ( $user_id, $state_code );
					break;
				case "LAWYER" :
					$data ['counts'] [0] [$state_count_name] = self::getLawyerCasesStatusCount ( $user_id, $lawyer_office_id, $state_code );
					break;
				case "SECRETARY" :
					$data ['counts'] [0] [$state_count_name] = self::getCasesStatusCount ( $lawyer_office_id, $state_code );
					break;
				default :
					$data ['counts'] [0] [$state_count_name] = self::getCasesStatusCount ( $lawyer_office_id, $state_code );
			}
			$sum = $sum + $data ['counts'] [0] [$state_count_name];
		}
		
		$data ['counts'] [0] ['ALL'] = $sum;
		return $data;
	}
	function getOrderedStatesToShow() {
		$conditionsString = "state.is_initial = '1'";
		$this->db->select ( 'state.state_code, state.state_name' );
		$this->db->from ( 'state' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			$i = 0;
			$case_transactions = array ();
			$case_transactions [0] ['state_code'] = $records [0] ['state_code'];
			$case_transactions [0] ['state_name'] = $records [0] ['state_name'];
			
			$conditionsString = "state.state_code = 'MODIFICATION_REQUIRED'";
			$this->db->select ( 'state.state_code, state.state_name' );
			$this->db->from ( 'state' );
			$this->db->where ( $conditionsString );
			$query = $this->db->get ();
			if ($query->num_rows () > 0) {
				$result = $query->result_array ();
				$j = count ( $case_transactions );
				$case_transactions [$j] ['state_code'] = $result [0] ['state_code'];
				$case_transactions [$j] ['state_name'] = $result [0] ['state_name'];
			}
			
			self::getRemainingStates ( $case_transactions [0] ['state_code'], $case_transactions, 1 );
			
			$conditionsString = "state.state_code = 'SUSPENDED'";
			$this->db->select ( 'state.state_code, state.state_name' );
			$this->db->from ( 'state' );
			$this->db->where ( $conditionsString );
			$query = $this->db->get ();
			if ($query->num_rows () > 0) {
				$result = $query->result_array ();
				$j = count ( $case_transactions );
				$case_transactions [$j] ['state_code'] = $result [0] ['state_code'];
				$case_transactions [$j] ['state_name'] = $result [0] ['state_name'];
			}
			$case_transactions [0] ['state_code'] = 'ALL';
			$case_transactions [0] ['state_name'] = 'كل القضايا';
			return $case_transactions;
		} else {
			return false;
		}
	}
	function quick_search_query($search_query, $conditionsString, $is_lawyer, $with_limit, $offset){
		$limit_string = "";
		$from_table = "";
		if ( $with_limit != 0 ){
			$limit_string ="LIMIT 50 OFFSET ".$offset;
		}
		if ( $is_lawyer != 0 ){
			$from_table = "from lawyer_case

			left OUTER join `case` on lawyer_case.case_id = `case`.case_id and lawyer_case.lawyer_office_id = `case`.lawyer_office_id";
		} else {
			$from_table = "from `case`

			left OUTER join `lawyer_case` on lawyer_case.case_id = `case`.case_id and lawyer_case.lawyer_office_id = `case`.lawyer_office_id";
		}
		return "select `case`.case_id, `case`.lawyer_office_id, `case`.`customer_id`,user.user_name as customer_name, user.last_name as customer_last_name,
							user.first_name as customer_first_name,lawyer.user_name as lawyer_name, lawyer.last_name as lawyer_last_name,
							lawyer.first_name as lawyer_first_name,`case`.contract_number, `case`.client_id_number, `case`.client_name,
							`case`.client_type, customer_type.name as customer_type_name, `case`.number_of_late_instalments, `case`.due_amount, `case`.remaining_amount, `case`.debenture,
							`case`.id, `case`.contract, `case`.others, `case`.creation_date, `case`.last_update_date,	`case`.creation_user,
							`case`.last_update_user, `case`.current_state_code, state.state_name, `case`.executive_order_number, `case`.decision_34_number,
							`case`.decision_34_file, `case`.advertisement_file, `case`.invoice_file, `case`.decision_46_date, `case`.closing_reason, `case`.notes,
							`case`.circle_number, `case`.order_number, `case`.referral_paper, `case`.consignment_image, `case`.decision_34_date, `case`.suspend_reason_code,
							case_suspend_reason.suspend_reason,`case`.suspend_date,`case`.suspend_file, `case`.executive_order_date, `case`.advertisement_date, `case`.debtor_name,
							`case`.obligation_value, `case`.total_debenture_amount, `case`.region_id, region.name as region_name "

			.$from_table.
			" join user on user.user_id = `case`.customer_id
			join customer_type on customer_type.customer_type_id = `case`.client_type
			join state on state.state_code = `case`.current_state_code
			left OUTER join case_suspend_reason on case_suspend_reason.suspend_code = `case`.suspend_reason_code
			left OUTER join user as lawyer on lawyer.user_id = lawyer_case.lawyer_id
			left OUTER join region on region.region_id = `case`.region_id where ".$conditionsString."
			 and (".
			"case.case_id like '%".$search_query."%' or
			case.contract_number like '%".$search_query."%' or
			case.client_id_number like '%".$search_query."%' or
			case.debtor_name like '%".$search_query."%' or
			case.client_name like '%".$search_query."%' or
			case.due_amount like '%".$search_query."%' or
			case.remaining_amount like '%".$search_query."%' or
			case.debenture like '%".$search_query."%' or
			case.id like '%".$search_query."%' or
			case.contract like '%".$search_query."%' or
			case.others like '%".$search_query."%' or
			case.executive_order_number like '%".$search_query."%' or
			case.decision_34_number like '%".$search_query."%' or
			case.obligation_value like '%".$search_query."%' or
			case.number_of_late_instalments like '%".$search_query."%' or
			case.total_debenture_amount like '%".$search_query."%' or
			case.order_number like '%".$search_query."%' or
			case.circle_number like '%".$search_query."%' or
			user.user_name like '%".$search_query."%' or
			user.last_name like '%".$search_query."%' or
			user.first_name like '%".$search_query."%' or
			lawyer.user_name like '%".$search_query."%' or
			lawyer.last_name like '%".$search_query."%' or
			lawyer.first_name like '%".$search_query."%' or
			customer_type.name like '%".$search_query."%' or
			state.state_name like '%".$search_query."%' or
			state.state_name like '%".$search_query."%' or
			case_suspend_reason.suspend_reason like '%".$search_query."%' or
			case.closing_reason like '%".$search_query."%' or
			region.name like '%".$search_query."%' 
			) group by case.case_id ".$limit_string.";";
	}
	
	function get_last_state_date ($case_id,$lawyer_office_id){
		$this->db->select ( 'case_transactions.transaction_id, case_transactions.current_state_code, case_transactions.last_update_date, case_transactions.manual_insert' );
		$this->db->from ( 'case_transactions' );
		$this->db->where ( "case_transactions.case_id ='" . $case_id . "' and case_transactions.lawyer_office_id = " . $lawyer_office_id . " AND case_transactions.edit_action = 'N' " );
		$this->db->order_by ( 'case_transactions.transaction_id', 'desc' );
		$this->db->limit ( 1 );
		$query = $this->db->get ();	
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			$trans_order = 0;
			if($records [0]["current_state_code"] == "UNDER_REVIEW"){
				$this->db->select ( 'case_transactions.transaction_id' );
				$this->db->from ( 'case_transactions' );
				$this->db->where ( "case_id ='" . $case_id . "' and lawyer_office_id = " . $lawyer_office_id . " AND edit_action = 'N' " );
				$query2 = $this->db->get ();	
				if ($query2->num_rows () > 1) {
					$trans_order = $query2->num_rows () - 1;
				}
			}
			return self::getStateDate($case_id, $lawyer_office_id, $records [0]["current_state_code"], $records [0]["last_update_date"], $trans_order);
		}
		return null;
	}
	function create_invoice($trans_values, $old_case_values, $region_id, $ignoreCreatingLawyerInvoice) {
		$this->load->model ( "common" );
		$case_id = $trans_values ['case_id'];
		$lawyer_office_id = $trans_values ['lawyer_office_id'];
		$stage_code = "";
		switch ($trans_values ['current_state_code']) {
			case "REGISTERED" :
				$stage_code = "Registered";
				break;
			case "DECISION_34" :
				$stage_code = "Decision 34";
				break;
			case "PUBLICLY_ADVERTISED" :
				$stage_code = "Public Advertised";
				break;
			case "DECISION_46" :
				$stage_code = "Decision 46";
				break;
			case "CLOSED" :
				$stage_code = "Closed";
				break;
			default :
				$stage_code = "";
		}
		if ($stage_code != "") {
			$stageRecord = $this->common->getOneRecField_Limit ( "stage_order", "stages", "where stage_code ='$stage_code'" );
			$stage_order = $stageRecord->stage_order;
			
			$this->load->model ( "invoices_model" );
			$values ['invoice_id'] = $this->invoices_model->getInvoiceId ();
			$values ['invoice_date'] = $trans_values ['last_update_date'];
			$values ['customer_id'] = $old_case_values ['customer_id'];
			if (isset ( $trans_values ['customer_id'] ) && $trans_values ['customer_id'] != null && $trans_values ['customer_id'] != "") {
				$values ['customer_id'] = $trans_values ['customer_id'];
			}
			
			$lawyerRecord = $this->common->getOneRecField_Limit ( "lawyer_id", "lawyer_case", "where case_id ='$case_id' and lawyer_office_id = " . $lawyer_office_id );
			if ($lawyerRecord) {
				$values ['lawyer_id'] = $lawyerRecord->lawyer_id;
				$values ['case_id'] = $trans_values ['case_id'];
				$values ['lawyer_office_id'] = $trans_values ['lawyer_office_id'];
				$values ['case_state_code'] = $trans_values ['current_state_code'];
				$values ['billing_value'] = $this->invoices_model->getLawyerCost ( $stage_order, $values ['lawyer_id'] );
				$values ['is_paid'] = "N";
				$values ['invoice_user_id'] = $values ['lawyer_id'];
				
				$this->db->trans_begin ();
				
				if ($ignoreCreatingLawyerInvoice == 0) {
					$this->common->insertRecord ( 'invoices', $values );
				}
				$values ['invoice_id'] = $this->invoices_model->getInvoiceId ();
				$values ['billing_value'] = $this->invoices_model->getCustomerCost ( $stage_code, $region_id, $values ['customer_id'] );
				$values ['invoice_user_id'] = $values ['customer_id'];
				
				$this->common->insertRecord ( 'invoices', $values );
				
				if ($this->db->trans_status () === FALSE) {
					$this->db->trans_rollback ();
				} else {
					$this->db->trans_commit ();
				}
			}
		}
	}

	function get_accepted_file_types() {
		return $array = array (
				"pdf",
				"ai",
				"bmp",
				"gif",
				"ico",
				"jpeg",
				"jpg",
				"png",
				"ps",
				"psd",
				"svg",
				"tif",
				"tiff"
		);
	}
}
?>