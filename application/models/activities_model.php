<?php
class activities_model extends CI_Model
{
	function getUsersActivities() {
		$this->db->select('user_activities.user_id, user_activities.activity, user_activities.date_time, user_activities.user_IP,
							user.user_name, user.last_name,	user.first_name, user_type.user_type_name, lawyer_office.name as lawyer_office_name');
		$this->db->from('user_activities');
		$this->db->join('user', 'user.user_id = user_activities.user_id');
		$this->db->join('user_type', 'user_type.user_type_code = user.user_type_code');
		$this->db->join('lawyer_office', 'lawyer_office.lawyer_office_id = user.lawyer_office_id');
		$this->db->order_by('user_activities.date_time', 'desc');
		$query = $this->db->get();
		
		if($query->num_rows()>0) {
			$records = $query->result_array();
			$i = 0;
			$data = array();
			foreach($records as $record ) {
				   $data[$i]['user_name'] = $record['user_name'];
				   $data[$i]['first_last_name'] = $record['first_name']." ".$record['last_name'];
				   $data[$i]['user_type_name'] = $record['user_type_name'];
				   $data[$i]['activity'] = $record['activity'];
				   $date_time = $record['date_time'];
				   $date_time = explode(" ", $record['date_time']);
				   $data[$i]['date'] = $date_time[0];
				   $data[$i]['time'] = $date_time[1];
				   $data[$i]['user_IP'] = $record['user_IP'];
				   $data[$i]['lawyer_office_name'] = $record['lawyer_office_name'];
	               $i = $i + 1;
	        }
			return $data;
		} else {
			return false;
		}
	}
	
	function getDashboardActivities($lawyer_office_id, $user_type_code, $user_id, $current_page, $page_size) {
		if ($user_type_code == "LAWYER") {
			$conditionsString = "user_activities.user_id = '".$user_id."'";
		} else {
			$conditionsString = "user.lawyer_office_id = '".$lawyer_office_id."'";
		}
		$this->db->select('user_activities.user_id, user_activities.activity, user_activities.date_time, user_activities.user_IP,
							user.user_name, user.last_name,	user.first_name, user_type.user_type_name, user.user_type_code');
		$this->db->from('user_activities');
		$this->db->join('user', 'user.user_id = user_activities.user_id');
		$this->db->join('user_type', 'user_type.user_type_code = user.user_type_code');
		$this->db->where($conditionsString);
		$this->db->order_by('user_activities.date_time', 'desc');
		$query = $this->db->get();
		
		$i = 0;
		$data = array();
		if($query->num_rows()>0) {
			$records = $query->result_array();
			foreach($records as $record ) {
				   $data[$i]['user_name'] = $record['first_name']." ".$record['last_name'];
				   $data[$i]['user_type'] = $record['user_type_name'];
				   $data[$i]['activity'] = $record['activity'];
				   $data[$i]['case_id'] = "";
				   if ($record['activity'] == "تسجيل دخول") {
				   		$data[$i]['activity'] =  "LOGIN";
				   		if ($record['user_type_code'] == "ADMIN") {
				   			$data[$i]['message'] =  'قام '.$data[$i]['user_type'].' '.$data[$i]['user_name'].' بتسجيل دخول';
				   		} else {
				   			$data[$i]['message'] =  'قام ال'.$data[$i]['user_type'].' '.$data[$i]['user_name'].' بتسجيل دخول';
				   		}				   		
				   } else if ($record['activity'] == "تسجيل خروج") {
				   		$data[$i]['activity'] =  "LOGOUT";
				   		if ($record['user_type_code'] == "ADMIN") {
				   			$data[$i]['message'] =  'قام '.$data[$i]['user_type'].' '.$data[$i]['user_name'].' بتسجيل خروج';
				   		} else {
				   			$data[$i]['message'] =  'قام ال'.$data[$i]['user_type'].' '.$data[$i]['user_name'].' بتسجيل خروج';
				   		}
				   } else if (strpos($record['activity'], 'تم التعديل فى القضية') !== false) {
				   		continue;
				   }
				   $data[$i]['date_time'] = $record['date_time'];
				   date_default_timezone_set('Asia/Riyadh');
				   $date = new DateTime($record['date_time']);
				   $data[$i]['date_time_diff'] = self::formatDateDiff($date);
				   
				   $data[$i]['lawyer_name'] = "";
				   $data[$i]['debtor_name'] = "";
				   
				   if (!isset($data[$i]["message"]) || $data[$i]["message"] == ''){
				       continue;
				   }
				   
	               $i = $i + 1;
	        }
		}
		if ($user_type_code == "LAWYER") {
			$conditionsString = "lawyer_case.lawyer_id = '".$user_id."' AND case_transactions.transaction_status = 'Success'";
		} else {
			$conditionsString = "user.lawyer_office_id = '".$lawyer_office_id."' AND case_transactions.transaction_status = 'Success'";
		}
		$this->db->select('case_transactions.transaction_id, case_transactions.case_id, case_transactions.last_update_user, case.debtor_name,
						 case_transactions.lawyer_id, case_transactions.action_code, case_transactions.current_state_code,
						 case_transactions.last_state_code, case_transactions.edit_action, case_transactions.trans_date_time,
						 user_type.user_type_name, user.last_name,	user.first_name, user.user_type_code ');
		$this->db->from('case_transactions');
		$this->db->join('case', 'case_transactions.case_id = case.case_id');
		$this->db->join('user', 'user.user_id = case_transactions.last_update_user');
		$this->db->join('user_type', 'user_type.user_type_code = user.user_type_code');
		if ($user_type_code == "LAWYER") {
			$this->db->join('lawyer_case', 'lawyer_case.case_id = case_transactions.case_id');
		}
		$this->db->where($conditionsString);
		$query = $this->db->get();	
		if($query->num_rows()>0) {
			$records = $query->result_array();
			foreach($records as $record ) {
				   $last_update_user_id = $record['last_update_user'];
				   $data[$i]['user_name'] = $record['first_name']." ".$record['last_name'];
	   			   $data[$i]['user_type'] = $record['user_type_name'];
	   			   $user_type_code = $record['user_type_code'];
	               $data[$i]['lawyer_name'] = "";
				   $lawyer_id = $record['lawyer_id'];
	               if ($lawyer_id != null && $lawyer_id > 0) {
		               $conditionsString = "user.user_id = '".$lawyer_id."'";
					   $this->db->select('user.last_name, user.first_name');
					   $this->db->from('user');
					   $this->db->where($conditionsString);
					   $query = $this->db->get();
			
					   if($query->num_rows()>0) {
							$lawyer_records = $query->result_array();
							$data[$i]['lawyer_name'] = $lawyer_records[0]['first_name']." ".$lawyer_records[0]['last_name'];
					   }
	               } else {
	               	   $case_id = $record['case_id'];
		 		 	   $conditionsString = "lawyer_case.case_id = '".$case_id."'";
					   $this->db->select('user.last_name, user.first_name');
					   $this->db->from('lawyer_case');
					   $this->db->join('user', 'user.user_id = lawyer_case.lawyer_id');
					   $this->db->where($conditionsString);
					   $query = $this->db->get();
					   
	               	   if($query->num_rows()>0) {
							$lawyer_records = $query->result_array();
							$data[$i]['lawyer_name'] = $lawyer_records[0]['first_name']." ".$lawyer_records[0]['last_name'];
					   }
	               }
	               
				   $data[$i]['case_id'] = $record['case_id'];
				   date_default_timezone_set('Asia/Riyadh');
				   $data[$i]['date_time'] = $record['trans_date_time'];
				   $date = new DateTime($record['trans_date_time']);
				   $data[$i]['date_time_diff'] = self::formatDateDiff($date);
				   $data[$i]['debtor_name'] = $record['debtor_name'];
				   
				   if ($record['edit_action'] == 'Y') {
				   		$data[$i]['activity'] =  "EDIT";
				  		if ($user_type_code == "ADMIN") {
				   			$data[$i]['message'] =  'قام '.$data[$i]['user_type'].' '.$data[$i]['user_name'].' بتعديل للقضية رقم '.$data[$i]['case_id'].' و التى تخص '.$data[$i]['debtor_name'];
				   		} else {
				   			$data[$i]['message'] =  'قام ال'.$data[$i]['user_type'].' '.$data[$i]['user_name'].' بتعديل للقضية رقم '.$data[$i]['case_id'].' و التى تخص '.$data[$i]['debtor_name'];
				   		}
				   } else if ($record['action_code'] == "REASSIGN_TO_LAWYER") {
				   		$data[$i]['activity'] =  "REASSIGN_TO_LAWYER";
				   		if ($user_type_code == "ADMIN") {
				   			$data[$i]['message'] =  'قام '.$data[$i]['user_type'].' '.$data[$i]['user_name'].' بتغيير المحامى للقضية رقم '.$data[$i]['case_id'].' و التى تخص '.$data[$i]['debtor_name'];
				   		} else {
				   			$data[$i]['message'] =  'قام ال'.$data[$i]['user_type'].' '.$data[$i]['user_name'].' بتغيير المحامى للقضية رقم '.$data[$i]['case_id'].' و التى تخص '.$data[$i]['debtor_name'];
				   		}
				   } else if ($record['action_code'] == "RESUME") {
				   		$data[$i]['activity'] =  "RESUME";
				   		if ($user_type_code == "ADMIN") {
				   			$data[$i]['message'] =  'قام '.$data[$i]['user_type'].' '.$data[$i]['user_name'].' باستكمال القضية رقم '.$data[$i]['case_id'].' و التى تخص '.$data[$i]['debtor_name'];
				   		} else {
				   			$data[$i]['message'] =  'قام ال'.$data[$i]['user_type'].' '.$data[$i]['user_name'].' باستكمال القضية رقم '.$data[$i]['case_id'].' و التى تخص '.$data[$i]['debtor_name'];
				   		}
				   } else if ($record['action_code'] == "REOPEN") {
				   		$data[$i]['activity'] =  "REOPEN";
				   		if ($user_type_code == "ADMIN") {
				   			$data[$i]['message'] =  'قام '.$data[$i]['user_type'].' '.$data[$i]['user_name'].' بإعادة فتح القضية رقم '.$data[$i]['case_id'].' و التى تخص '.$data[$i]['debtor_name'];
				   		} else {
				   			$data[$i]['message'] =  'قام ال'.$data[$i]['user_type'].' '.$data[$i]['user_name'].' بإعادة فتح القضية رقم '.$data[$i]['case_id'].' و التى تخص '.$data[$i]['debtor_name'];
				   		}
				   } else if ($record['action_code'] == "MODIFICATIONS_DONE") {
				   		$data[$i]['activity'] =  "MODIFICATIONS_DONE";
				   		if ($user_type_code == "ADMIN") {
				   			$data[$i]['message'] =  'قام '.$data[$i]['user_type'].' '.$data[$i]['user_name'].' بتعديل للقضية رقم '.$data[$i]['case_id'].' و التى تخص '.$data[$i]['debtor_name'];
				   		} else {
				   			$data[$i]['message'] =  'قام ال'.$data[$i]['user_type'].' '.$data[$i]['user_name'].' بتعديل للقضية رقم '.$data[$i]['case_id'].' و التى تخص '.$data[$i]['debtor_name'];
				   		}
				   } else if ($record['current_state_code'] == "UNDER_REVIEW") {
				   		$data[$i]['activity'] =  "CREATE";
				   		if ($user_type_code == "ADMIN") {
				   			$data[$i]['message'] =  'قام '.$data[$i]['user_type'].' '.$data[$i]['user_name'].' بإنشاء القضية رقم '.$data[$i]['case_id'].' و التى تخص '.$data[$i]['debtor_name'];
				   		} else {
				   			$data[$i]['message'] =  'قام ال'.$data[$i]['user_type'].' '.$data[$i]['user_name'].' بإنشاء القضية رقم '.$data[$i]['case_id'].' و التى تخص '.$data[$i]['debtor_name'];
				   		}
				   } else if ($record['current_state_code'] == "ASSIGNED_TO_LAWYER") {
				   		$data[$i]['activity'] =  "ASSIGN_TO_LAWYER";
				  		if ($user_type_code == "ADMIN") {
				   			$data[$i]['message'] =  'قام '.$data[$i]['user_type'].' '.$data[$i]['user_name'].' بإسناد القضية رقم '.$data[$i]['case_id'].' و التى تخص '.$data[$i]['debtor_name'].' للمحامى '.$data[$i]['lawyer_name'];
				   		} else {
				   			$data[$i]['message'] =  'قام ال'.$data[$i]['user_type'].' '.$data[$i]['user_name'].' بإسناد القضية رقم '.$data[$i]['case_id'].' و التى تخص '.$data[$i]['debtor_name'].' للمحامى '.$data[$i]['lawyer_name'];
				   		}
				   } else if ($record['current_state_code'] == "MODIFICATION_REQUIRED") {
				   		$data[$i]['activity'] =  "NEED_MODIFICATIONS";
				   		if ($user_type_code == "ADMIN") {
				   			$data[$i]['message'] =  'قام '.$data[$i]['user_type'].' '.$data[$i]['user_name'].' بطلب تعديل للقضية رقم '.$data[$i]['case_id'].' و التى تخص '.$data[$i]['debtor_name'];
				   		} else {
				   			$data[$i]['message'] =  'قام ال'.$data[$i]['user_type'].' '.$data[$i]['user_name'].' بطلب تعديل للقضية رقم '.$data[$i]['case_id'].' و التى تخص '.$data[$i]['debtor_name'];
				   		}
				   } else if ($record['current_state_code'] == "REGISTERED") {
				   		$data[$i]['activity'] =  "REGISTER";
				   		if ($user_type_code == "ADMIN") {
				   			$data[$i]['message'] =  'قام '.$data[$i]['user_type'].' '.$data[$i]['user_name'].' بتقييد القضية رقم '.$data[$i]['case_id'].' و التى تخص '.$data[$i]['debtor_name'];
				   		} else {
				   			$data[$i]['message'] =  'قام ال'.$data[$i]['user_type'].' '.$data[$i]['user_name'].' بتقييد القضية رقم '.$data[$i]['case_id'].' و التى تخص '.$data[$i]['debtor_name'];
				   		}
				   } else if ($record['current_state_code'] == "DECISION_34") {
				   		$data[$i]['activity'] =  "MAKE_DECISION_34";
				   		if ($user_type_code == "ADMIN") {
				   			$data[$i]['message'] =  'قام '.$data[$i]['user_type'].' '.$data[$i]['user_name'].' بقرار 34 للقضية رقم '.$data[$i]['case_id'].' و التى تخص '.$data[$i]['debtor_name'];
				   		} else {
				   			$data[$i]['message'] =  'قام ال'.$data[$i]['user_type'].' '.$data[$i]['user_name'].' بقرار 34 للقضية رقم '.$data[$i]['case_id'].' و التى تخص '.$data[$i]['debtor_name'];
				   		}
				   } else if ($record['current_state_code'] == "PUBLICLY_ADVERTISED") {
				   		$data[$i]['activity'] =  "ADVERTISE";
				   		if ($user_type_code == "ADMIN") {
				   			$data[$i]['message'] =  'قام '.$data[$i]['user_type'].' '.$data[$i]['user_name'].' بإعلان القضية رقم '.$data[$i]['case_id'].' و التى تخص '.$data[$i]['debtor_name'];
				   		} else {
				   			$data[$i]['message'] =  'قام ال'.$data[$i]['user_type'].' '.$data[$i]['user_name'].' بإعلان القضية رقم '.$data[$i]['case_id'].' و التى تخص '.$data[$i]['debtor_name'];
				   		}
				   } else if ($record['current_state_code'] == "DECISION_46") {
				   		$data[$i]['activity'] =  "MAKE_DECISION_46";
				   		if ($user_type_code == "ADMIN") {
				   			$data[$i]['message'] =  'قام '.$data[$i]['user_type'].' '.$data[$i]['user_name'].' بقرار 46 للقضية رقم '.$data[$i]['case_id'].' و التى تخص '.$data[$i]['debtor_name'];
				   		} else {
				   			$data[$i]['message'] =  'قام ال'.$data[$i]['user_type'].' '.$data[$i]['user_name'].' بقرار 46 للقضية رقم '.$data[$i]['case_id'].' و التى تخص '.$data[$i]['debtor_name'];
				   		}
				   } else if ($record['current_state_code'] == "CLOSED") {
				   		$data[$i]['activity'] =  "CLOSE";
				   		if ($user_type_code == "ADMIN") {
				   			$data[$i]['message'] =  'قام '.$data[$i]['user_type'].' '.$data[$i]['user_name'].' بإنهاء القضية رقم '.$data[$i]['case_id'].' و التى تخص '.$data[$i]['debtor_name'];
				   		} else {
				   			$data[$i]['message'] =  'قام ال'.$data[$i]['user_type'].' '.$data[$i]['user_name'].' بإنهاء القضية رقم '.$data[$i]['case_id'].' و التى تخص '.$data[$i]['debtor_name'];
				   		}
				   } else if ($record['current_state_code'] == "SUSPENDED") {
				   		$data[$i]['activity'] =  "SUSPEND";
				  		if ($user_type_code == "ADMIN") {
				   			$data[$i]['message'] =  'قام '.$data[$i]['user_type'].' '.$data[$i]['user_name'].' بإمهال القضية رقم '.$data[$i]['case_id'].' و التى تخص '.$data[$i]['debtor_name'];
				   		} else {
				   			$data[$i]['message'] =  'قام ال'.$data[$i]['user_type'].' '.$data[$i]['user_name'].' بأمهال القضية رقم '.$data[$i]['case_id'].' و التى تخص '.$data[$i]['debtor_name'];
				   		}
				   } else {
				   		continue;
				   }
				   
				   if (!isset($data[$i]["message"]) || $data[$i]["message"] == ''){
				       continue;
				   }
				   
	               $i = $i + 1;
	        }
		}	
		
		/*
		 * Handle Pagination 
		 */
		if (count($data) > 0) {
		    $total_activities_num = count ( $data );
		    $offset = ($current_page - 1) * $page_size;
			self::array_sort_by_column($data, "date_time", SORT_DESC);
	        $data = array_slice ( $data, $offset, $page_size );
		    $data [0] ['count_all'] = $total_activities_num;
			return $data;
		} else {
			return false;
		}
	}
	
	function array_sort_by_column(&$array, $column1, $dir1 = SORT_ASC) {
		    $sort_column = array();
		    foreach ($array as $key=> $row) {
		        $sort_column[$column1][$key] = $row[$column1];
		        
		    }
		    array_multisort($sort_column[$column1], $dir1, $array);
	}
	
	/** 
	* A sweet interval formatting, will use the two biggest interval parts. 
	* On small intervals, you get minutes and seconds. 
	* On big intervals, you get months and days. 
	* Only the two biggest parts are used. 
	* 
	* @param DateTime $start 
	* @param DateTime|null $end 
	* @return string 
	*/ 
	
	 function formatDateDiff($start, $end=null) { 
	 	date_default_timezone_set('Asia/Riyadh');
	    if(!($start instanceof DateTime)) { 
	        $start = new DateTime($start); 
	    } 
	    
	    if($end === null) { 
	        $end = new DateTime(); 
	    } 
	    
	    if(!($end instanceof DateTime)) { 
	        $end = new DateTime($start); 
	    } 
	    #$end->setTimezone(new DateTimeZone('Asia/Riyadh'));
	    $interval = $end->diff($start);
	    #echo $end->format('Y-m-d H:i:s').' '.$start->format('Y-m-d H:i:s').' '.($end->format('Y-m-d H:i:s') - $start->format('Y-m-d H:i:s'));
	    
	   
	    
	    
	     $getTimeString = function($nb,$str,$value,$from_str){
	     	
		     $doPlural = function($nb,$str){
		    	if ($nb == 0) {
		    		return "الان";
		    	} else if ($nb == 1 || $nb > 10){
		    		return $str;
		    	} else if ($nb == 2) {
		    		switch($str) {
					    case "سنة":
					    	return  "سنتان";
					        break;
					    case "شهر":
					    	return  "شهران";
					        break;
					    case "يوم":
					    	return  "يومان";
					        break;
					    case "ساعة":
					    	return  "ساعتان";
					        break;
					    case "دقيقة":
					   		return  "دقيقتان";
					        break;
					    case "ثانية":
					    	return  "ثانيتان";	    	
					        break;
					    default:
					    	return  "ثانيتان";
					    	
					}
		    	} else if ($nb >= 3 && $nb <= 10) {
		    		switch($str) {
					    case "سنة":
					    	return  "سنين";
					        break;
					    case "شهر":
					    	return  "شهور";
					        break;
					    case "يوم":
					    	return  "أيام";
					        break;
					    case "ساعة":
					    	return  "ساعات";
					        break;
					    case "دقيقة":
					   		return  "دقائق";
					        break;
					    case "ثانية":
					    	return  "ثوانى";	    	
					        break;
					    default:
					    	return  "ثوانى";
					    	
					}
		    	} else {
		    		return $str;
		    	}
		    }; // adds plurals 
	     	
	     	if ($nb == 0) {
	    		return $doPlural($nb, $str); 
	    	} else if ($nb == 1){
	    		return $from_str.$doPlural($nb, $str);
	    	} else if ($nb > 10){
	    		return $from_str.$value.$doPlural($nb, $str); 
	    	} else if ($nb == 2) {
	    		return $from_str.$doPlural($nb, $str); 
	    	} else if ($nb >= 3 && $nb <= 10) {
	    		return $from_str.$value.$doPlural($nb, $str); 
	    	}
	    }; // getTimeString
	    
	    $format = array(); 
	    if($interval->y !== 0) {
	    	$format[] = $getTimeString($interval->y, "سنة", "%y ", "منذ "); 
	    } 
	    else if($interval->m !== 0) { 
	        $format[] = $getTimeString($interval->m, "شهر", "%m ", "منذ "); 
	    } 
	    else if($interval->d !== 0) { 
	        $format[] = $getTimeString($interval->d, "يوم", "%d ", "منذ "); 
	    } 
	    else if($interval->h !== 0) { 
	        $format[] = $getTimeString($interval->h, "ساعة", "%h ", "منذ "); 
	    } 
	    else if($interval->i !== 0) { 
	        $format[] = $getTimeString($interval->i, "دقيقة", "%i ", "منذ "); 
	    } 
	    else if($interval->s !== 0) { 
	        /*  if(!count($format)) { 
	            return "منذ أقل من دقيقة"; 
	        } else { 
	            $format[] = "%s ".$doPlural($interval->s, "ثانية"); 
	        } */
	    	$format[] = $getTimeString($interval->s, "ثانية", "%s ", "منذ "); 
	    }  else {
	    	$format[] = $getTimeString($interval->s, "ثانية", "%s ", "منذ "); 
	    }
	    
	    // We use the two biggest parts 
	    if(count($format) > 1) { 
	        $format = array_shift($format)." و ".array_shift($format); 
	    } else { 
	        $format = array_pop($format); 
	    } 
	     $format;
	    // Prepend 'since ' or whatever you like 
	     return $interval->format($format); 
	} 
	
	
}
?>