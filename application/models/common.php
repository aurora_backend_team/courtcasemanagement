<?php
class common extends CI_Model
{
	function __construct()
    {
        parent::__construct();
        $this -> load -> database();
    }
    
	
	function getOneRow($table,$where){	
		$q = "select * from $table $where";
		$query=$this->db->query($q);
		return $query->row_array();
	}
			
	function getRow($table,$where){	
		$q = "select * from $table $where";
		$query=$this->db->query($q);
		return $query->row_array();
	}
			
	function getAllRow($table,$where){
		$q = "select * from $table $where";
		$query=$this->db->query($q);
		return $query->result_array();
	}
			
	function getAllRec($select,$table,$where){		 			 

		$q = "select $select from $table $where";
		$query=$this->db->query($q);
		return $query->result_array();
	}
			
	function checkDuplicate($table,$where){
		$q = "select * from $table $where limit 1";
		$query=$this->db->query($q);
		return $query->row();			
	}
			
	function getOneRecField($id,$table,$where){
		$q = "select $id from $table $where limit 0,1";
		$query=$this->db->query($q);
		return $query->row();
	}
	
	function getOneRecArray($id,$table,$where){
		$q = "select $id from $table $where limit 0,1";
		$query=$this->db->query($q);
		return $query->row_array();
	}	
	function getOneRecField_Limit($id,$table,$where){
		$q = "select $id from $table $where";
		
		$query=$this->db->query($q);
		return $query->row();
	}
			
	function numRow($table,$where){
		$q = "select * from $table $where";
		$query=$this->db->query($q);
		return $query->num_rows();
	}
			
	function numRowSelect($select,$table,$where){
		$q = "select $select from $table $where";
		$query=$this->db->query($q);
		return $query->num_rows();
	}
			
	function deleteRecord($table,$where){
		$this->db->delete($table,$where);
		return $this -> db -> affected_Rows(); //added by Prasad
	}
			
	function insertRecord($table,$data){
		$this->db->insert($table,$data);
		return $this -> db -> insert_id(); // added by prasad
	}
	function updateRecord($table,$data,$where){
		$this->db->update($table,$data,$where);
		return $this -> db -> affected_Rows(); //added by Prasad
	}	

	function mysql_safe_string($value){
	    //$value=addslashes($value);
		//$value=$this->security->xss_clean($value);
		//$value=strip_image_tags($value);
		//$value=encode_php_tags($value);
		//$value=mysql_real_escape_string($value);
		return $value;
	}

    function mysql_safe_string1($value){
	    $value=addslashes($value);
		//$value=$this->security->xss_clean($value);
		
		return $value;
	}	
	
	 function get_list($table, $limit, $start)
    {
        $sql = 'select * from '. $table . ' limit ' . $start . ', ' . $limit;
        $query = $this->db->query($sql);
        return $query->result();
    }
	
	function get_all_records($table, $limit, $start)
    {
        $sql = 'select * from '. $table . ' limit ' . $start . ', ' . $limit;
        $query = $this->db->query($sql);
        return $query->result();
    }
    
	function get_records($table)
    {
        $sql = 'select * from '. $table;
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
	function getSumOfField($field,$table,$where){
		$q = "select SUM($field) AS field_sum from $table $where";
		$query=$this->db->query($q);
		return $query->row()->field_sum;
	}
	
	function getCountOfField($field,$table,$where){
		$q = "select COUNT(DISTINCT $field) AS field_count from $table $where";
		$query=$this->db->query($q);
		return $query->row()->field_count;
	}
	
function getCountOfPrimaryField($field,$table,$where){
		$q = "select COUNT($field) AS field_count from $table $where";
		$query=$this->db->query($q);
		return $query->row()->field_count;
	}


//Send SMS API using CURL method
	function sendSMS($sms_active, $mobile, $msg)
	{
		if ($sms_active == "Y") {
			global $arraySendMsg;
			$url = "www.mobily.ws/api/msgSend.php";
			$numbers = substr_replace($mobile,"966",0,1);
			$userAccount = "966504211994";
			$passAccount = "live123";
			$applicationType = "68"; 
			$sender = "IbnAlshaikh";
			$MsgID = "15176";
			$timeSend=0;
			$dateSend=0;
			$deleteKey=0;
			$viewResult=1;
			
			$sender = urlencode($sender);
			$domainName = "altanfeeth.com";
			$stringToPost = "mobile=".$userAccount."&password=".$passAccount."&numbers=".$numbers."&sender=".$sender."&msg=".$msg."&timeSend=".$timeSend."&dateSend=".$dateSend."&applicationType=".$applicationType."&domainName=".$domainName."&msgId=".$MsgID."&deleteKey=".$deleteKey."&lang=3";
		
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_TIMEOUT, 5);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $stringToPost);
			$result = curl_exec($ch);
		
			//if($viewResult)
			//	$result = printStringResult(trim($result) , $arraySendMsg);
			return $result;
		}
		
	}
	
	// Functions added by Prasad

    function getNumOfPassengers($owner_id)
    {
    	$sql = "select ct.passengers from `tbl_user_car_details` uc,tbl_car_types ct where uc.car_type_id=ct.car_type_id and uc.owner_id=".$owner_id.' limit 0,1';

    	$query = $this -> db -> query ($sql);
    	return $query -> result_array();
    }

    function getUserCars($owner_id)
    {
    	$sql = "SELECT * from `tbl_user_car_details` d,tbl_car_types c where c.car_type_id=d.car_type_id and d.owner_id=".$owner_id;
    	$query = $this->db->query($sql);
        return $query->result_array();
    }
	
	  function get_car_trips($time,$date)
        {
            $q="SELECT d.car_type_id,t.car_type_name, COUNT(m.trip_id) as no_trips  FROM  tbl_trip_master m , tbl_user_car_details d , tbl_car_types t, tbl_owner_ratings r WHERE m.car_id = d.detail_id and t.car_type_id = d.car_type_id and r.to_owner_id = m.owner_id and m.journey_start_time2 >='$time' and m.date >= '$date'GROUP BY d.car_type_id ";
            $query = $this->db->query($q);
            return $query->result_array();
        }
	// End of functions added by Prasad
	
	
}


?>