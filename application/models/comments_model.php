<?php
class comments_model extends CI_Model
{
	function getCaseComments($case_id,$lawyer_office_id) {
		$conditionsString = "comment.case_id = '".$case_id."' and comment.lawyer_office_id = ".$lawyer_office_id;	
		$this->db->select('comment.user_id, comment.comment, comment.date_time, comment.id, user.user_name, user.last_name,
							user.first_name, user.profile_img, user.user_type_code, user_type.user_type_name');
		$this->db->from('comment');
		$this->db->join('user', 'user.user_id = comment.user_id');
		$this->db->join('user_type', 'user_type.user_type_code = user.user_type_code');
		$this->db->where($conditionsString);
		$this->db->order_by("date_time", "desc");
		$query = $this->db->get();
		
		if($query->num_rows()>0) {
			$records = $query->result_array();
			$i = 0;
			$comments_data = array();
			foreach($records as $record ) {
				   $comments_data[$i]['id'] = $record['id'];
				   $comments_data[$i]['user_id'] = $record['user_id'];
				   $comments_data[$i]['comment'] = $record['comment'];
				   $comments_data[$i]['date_time'] = $record['date_time'];
				   $comments_data[$i]['user_name'] = $record['user_name'];
				   $comments_data[$i]['first_name'] = $record['first_name'];
				   $comments_data[$i]['last_name'] = $record['last_name'];
				   $comments_data[$i]['profile_img'] = $record['profile_img'];
				   $comments_data[$i]['user_type_code'] = $record['user_type_code'];
				   $comments_data[$i]['user_type_name'] = $record['user_type_name'];    
	              
	               $i = $i + 1;
	        }
			return $comments_data;
		} else {
			return false;
		}
	}
	
	
}
?>