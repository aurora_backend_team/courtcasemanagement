<?php
class lawyer_office_model extends CI_Model {
	function getAllLawyerOffices() {
		$this->db->select ( 'lawyer_office.lawyer_office_id, package.package_name, lawyer_office.name, lawyer_office.address, city.city_name,
							lawyer_office.phone_number, lawyer_office.mobile_number, lawyer_office.email, lawyer_office.contact_person_name,
							lawyer_office.position, lawyer_office.subscription_start_date, lawyer_office.subscription_end_date, lawyer_office.logo, lawyer_office.banner' );
		$this->db->from ( 'lawyer_office' );
		$this->db->join ( 'package', 'lawyer_office.package_code = package.package_code' );
		$this->db->join ( 'city', 'lawyer_office.city = city.city_id' );
		$query = $this->db->get ();
		
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			return $records;
		} else {
			return false;
		}
	}
	function getCustomers($lawyer_office_id) {
		$conditionsString = "user.lawyer_office_id = '" . $lawyer_office_id . "' AND user_type_code = 'CUSTOMER'";
		$this->db->select ( 'user.lawyer_office_id, user.user_id, user.first_name, user.last_name, user.mobile_number, user.email,
							user.user_name, user.password, user.profile_img, customer_type.name as customer_type_name,
							  customer_details.address, customer_details.phone_number, customer_details.contact_person_name,
							 customer_details.position, customer_details.subscription_start_date, customer_details.subscription_end_date,
							  customer_details.number_of_cases_per_month, customer_details.logo, customer_details.banner,
							   customer_details.show_invoice_module, customer_details.show_dashboard, lawyer_office.name as lawyer_office_name' );
		$this->db->from ( 'user' );
		$this->db->join ( 'customer_details', 'user.user_id = customer_details.customer_id' );
		$this->db->join ( 'customer_type', 'customer_details.customer_type = customer_type.customer_type_id' );
		$this->db->join ( 'lawyer_office', 'user.lawyer_office_id = lawyer_office.lawyer_office_id' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			return $records;
		} else {
			return false;
		}
	}
	function getSecretaries($lawyer_office_id) {
		$conditionsString = "user.lawyer_office_id = '" . $lawyer_office_id . "' AND user_type_code = 'SECRETARY'";
		$this->db->select ( 'user.lawyer_office_id, user.user_id, user.first_name, user.last_name, user.mobile_number, user.email,
							user.user_name, user.password, user.profile_img, lawyer_office.name as lawyer_office_name' );
		$this->db->from ( 'user' );
		$this->db->join ( 'lawyer_office', 'user.lawyer_office_id = lawyer_office.lawyer_office_id' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			return $records;
		} else {
			return false;
		}
	}
	function getLawyers($lawyer_office_id) {
		$conditionsString = "user.lawyer_office_id = '" . $lawyer_office_id . "' AND user_type_code = 'LAWYER'";
		$this->db->select ( 'user.lawyer_office_id as user_lawyer_office_id, user.user_id, user.first_name, user.last_name, user.mobile_number, user.email,
							user.user_name, user.password, user.profile_img, lawyer_office.name as lawyer_office_name, lawyer_details.manager, 
							lawyer_details.stage1_cost, lawyer_details.stage2_cost, lawyer_details.stage3_cost, lawyer_details.stage4_cost, lawyer_details.stage5_cost' );
		$this->db->from ( 'user' );
		$this->db->join ( 'lawyer_details', 'user.user_id = lawyer_details.lawyer_id' );
		$this->db->join ( 'lawyer_office', 'user.lawyer_office_id = lawyer_office.lawyer_office_id' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			$i = 0;
			$lawyers_data = array ();
			foreach ( $records as $record ) {
				$lawyers_data [$i] ['lawyer_office_id'] = $record ['user_lawyer_office_id'];
				$lawyers_data [$i] ['user_id'] = $record ['user_id'];
				$lawyers_data [$i] ['first_name'] = $record ['first_name'];
				$lawyers_data [$i] ['last_name'] = $record ['last_name'];
				$lawyers_data [$i] ['mobile_number'] = $record ['mobile_number'];
				$lawyers_data [$i] ['email'] = $record ['email'];
				$lawyers_data [$i] ['user_name'] = $record ['user_name'];
				$lawyers_data [$i] ['password'] = $record ['password'];
				$lawyers_data [$i] ['profile_img'] = $record ['profile_img'];
				$lawyers_data [$i] ['lawyer_office_name'] = $record ['lawyer_office_name'];
				$lawyers_data [$i] ['stage1_cost'] = $record ['stage1_cost'];
				$lawyers_data [$i] ['stage2_cost'] = $record ['stage2_cost'];
				$lawyers_data [$i] ['stage3_cost'] = $record ['stage3_cost'];
				$lawyers_data [$i] ['stage4_cost'] = $record ['stage4_cost'];
				$lawyers_data [$i] ['stage5_cost'] = $record ['stage5_cost'];
				$lawyers_data [$i] ['manager_id'] = $record ['manager'];
				
				$manager_id = $record ['manager'];
				$conditionsString = "user.user_id = '" . $manager_id . "'";
				$this->db->select ( 'user.user_name as manager_name' );
				$this->db->from ( 'user' );
				$this->db->where ( $conditionsString );
				$query = $this->db->get ();
				
				if ($query->num_rows () > 0) {
					$manager_records = $query->result_array ();
					$lawyers_data [$i] ['manager_name'] = $manager_records [0] ['manager_name'];
				}
				
				$lawyer_id = $record ['user_id'];
				$conditionsString = "lawyer_case.lawyer_id = '" . $lawyer_id . "' AND case.current_state_code = 'CLOSED'";
				$this->db->select ( 'case.case_id' );
				$this->db->from ( 'lawyer_case' );
				$this->db->join ( 'case', 'lawyer_case.case_id = case.case_id' );
				$this->db->where ( $conditionsString );
				$query = $this->db->get ();
				$lawyers_data [$i] ['closed_cases_count'] = $query->num_rows ();
				
				$conditionsString = "lawyer_case.lawyer_id = '" . $lawyer_id . "' AND case.current_state_code != 'CLOSED'";
				$this->db->select ( 'case.case_id' );
				$this->db->from ( 'lawyer_case' );
				$this->db->join ( 'case', 'lawyer_case.case_id = case.case_id' );
				$this->db->where ( $conditionsString );
				$query = $this->db->get ();
				$lawyers_data [$i] ['opened_cases_count'] = $query->num_rows ();
				
				$i = $i + 1;
			}
			return $lawyers_data;
		} else {
			return false;
		}
	}
	function getRegions($lawyer_office_id) {
		$this->db->select ( 'region.region_id,region.name' ); // ,city.city_id,city.city_name');
		$this->db->from ( 'region' );
		$this->db->where("lawyer_office_id ='" . $lawyer_office_id . "'");
		// $this->db->join('region_cities', 'region.region_id = region_cities.region_id', 'left');
		// $this->db->join('city', 'region_cities.region_city_id = city.city_id', 'left');
		$query = $this->db->get ();
		
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			return $records;
		} else {
			return false;
		}
	}
	function getRegionData($region_id) {
		$conditionsString = "region.region_id = '" . $region_id . "'";
		$this->db->select ( 'region.region_id,region.name');//,city.city_id,city.city_name' );
		$this->db->from ( 'region' );
		//$this->db->join ( 'region_cities', 'region.region_id = region_cities.region_id' );
		//$this->db->join ( 'city', 'region_cities.region_city_id = city.city_id' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			return $records;
		} else {
			return false;
		}
	}
	function getCostPerStagePerRegion() {
		$this->db->select ( 'cost_per_stage_per_region.id,cost_per_stage_per_region.cost,cost_per_stage_per_region.region_id,
						 cost_per_stage_per_region.stage_code, region.name as region_name, stages.stage_title' );
		$this->db->from ( 'cost_per_stage_per_region' );
		$this->db->join ( 'region', 'cost_per_stage_per_region.region_id = region.region_id' );
		$this->db->join ( 'stages', 'cost_per_stage_per_region.stage_code = stages.stage_code' );
		$query = $this->db->get ();
		
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			return $records;
		} else {
			return false;
		}
	}
	function getCostPerStagePerRegionData($user_id, $stage_order) {
		$conditionsString = "cost_per_stage_per_region.customer_id = '" . $user_id . "' AND stages.stage_order = '" . $stage_order . "'";
		$this->db->select ( 'cost_per_stage_per_region.id,cost_per_stage_per_region.cost,cost_per_stage_per_region.region_id,
						 cost_per_stage_per_region.stage_code, region.name as region_name, stages.stage_title' );
		$this->db->from ( 'cost_per_stage_per_region' );
		$this->db->join ( 'region', 'cost_per_stage_per_region.region_id = region.region_id' );
		$this->db->join ( 'stages', 'cost_per_stage_per_region.stage_code = stages.stage_code' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			return $records;
		} else {
			return false;
		}
	}
	function getAvailableManagers($user_id, $lawyer_office_id) {
		$conditionsString = "user_id != '" . $user_id . "' AND lawyer_office_id = '" . $lawyer_office_id . "' AND (user_type_code = 'ADMIN' OR user_type_code = 'LAWYER')";
		$this->db->select ( 'user_id,user_name,first_name,last_name' );
		$this->db->from ( 'user' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			return $records;
		} else {
			return false;
		}
	}
	function getLawyersInRegion($lawyer_office_id, $region_id, $lawyer_id = 0) {
		$conditionsString = "user.lawyer_office_id = '" . $lawyer_office_id . "' AND user_type_code = 'LAWYER' ";
		if($lawyer_id != 0 && $lawyer_id != null){
			$conditionsString = "lawyer_details.lawyer_id != " . $lawyer_id ;
		}
		$this->db->select ( 'user.user_id, user.first_name, user.last_name, user.user_name, lawyer_details.region_ids' );
		$this->db->from ( 'user' );
		$this->db->join ( 'lawyer_details', 'user.user_id = lawyer_details.lawyer_id' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		
		if ($query->num_rows () > 0) {
			$records = $query->result_array ();
			$i = 0;
			$lawyers_data = array ();
			foreach ( $records as $record ) {
				$region_ids_arr = explode ( ",", $record ['region_ids'] );
				foreach ( $region_ids_arr as $region_id_val ) {
					if ($region_id_val == $region_id) {
						$lawyers_data [$i] ['user_id'] = $record ['user_id'];
						$lawyers_data [$i] ['first_name'] = $record ['first_name'];
						$lawyers_data [$i] ['last_name'] = $record ['last_name'];
						$lawyers_data [$i] ['user_name'] = $record ['user_name'];
						$i = $i + 1;
						break;
					}
				}
			}
			return $lawyers_data;
		} else {
			return false;
		}
	}
	function customerHasCase($user_id) {
		$conditionsString = "case.customer_id = '" . $user_id . "'";
		$this->db->select ( 'case.case_id' );
		$this->db->from ( 'case' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		
		if ($query->num_rows () > 0) {
			return true;
		} else {
			return false;
		}
	}
	function getLawyerOfficePackage($lawyer_office_id) {
		$conditionsString = "lawyer_office.lawyer_office_id = '" . $lawyer_office_id . "'";
		$this->db->select ( 'lawyer_office.lawyer_office_id, package.package_code, package.package_name, package.num_of_lawyer,
			 package.num_of_customer, package.num_of_cases_a_month' );
		$this->db->from ( 'lawyer_office' );
		$this->db->join ( 'package', 'lawyer_office.package_code = package.package_code' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		
		if ($query->num_rows () > 0) {
			$record = $query->row_array ();
			return $record;
		} else {
			return false;
		}
	}
	function getNumOflawyers($lawyer_office_id) {
		$conditionsString = "user.lawyer_office_id = '" . $lawyer_office_id . "' AND user_type_code = 'LAWYER'";
		$this->db->select ( 'COUNT(DISTINCT user_id) AS lawyers_count' );
		$this->db->from ( 'user' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		
		if ($query->num_rows () > 0) {
			$lawyers_count = $query->row ()->lawyers_count;
			return $lawyers_count;
		} else {
			return false;
		}
	}
	function getNumOfCustomers($lawyer_office_id) {
		$conditionsString = "user.lawyer_office_id = '" . $lawyer_office_id . "' AND user_type_code = 'CUSTOMER'";
		$this->db->select ( 'COUNT(DISTINCT user_id) AS customers_count' );
		$this->db->from ( 'user' );
		$this->db->where ( $conditionsString );
		$query = $this->db->get ();
		
		if ($query->num_rows () > 0) {
			$customers_count = $query->row ()->customers_count;
			return $customers_count;
		} else {
			return false;
		}
	}
}
?>