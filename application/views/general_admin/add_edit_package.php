<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<?php
session_start ();
$this->load->view ( 'localization/lang' );
$this->load->view ( 'localization/txt' );
defineLocale ();
defineStrings ();
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<script type="text/javascript">
function checkValid() {
if (textNumViolation())
		return false;
	replacAllArabToEng();
	var package_code = document.forms["myForm"]["package_code"].value;
	var package_name = document.forms["myForm"]["package_name"].value;
	var num_of_cases_a_month = document.forms["myForm"]["num_of_cases_a_month"].value;
	var num_of_region = document.forms["myForm"]["num_of_region"].value;
	var num_of_lawyer = document.forms["myForm"]["num_of_lawyer"].value;
	var num_of_customer = document.forms["myForm"]["num_of_customer"].value;
	var price = document.forms["myForm"]["price"].value;
	if (package_code == "") {
		$("#package_code_tooltip").css("display","block");
		$("#package_code_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#package_code_tooltip").css("display","none");
		$("#package_code_tooltip").css("visibility","none");
	}
	 if (package_name == "") {
		$("#package_name_tooltip").css("display","block");
		$("#package_name_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#package_name_tooltip").css("display","none");
		$("#package_name_tooltip").css("visibility","none");
	}
	if (num_of_cases_a_month == "" || num_of_cases_a_month <=0 || num_of_cases_a_month =="-" || num_of_cases_a_month.length > 7) {
		$("#num_of_cases_a_month_tooltip").css("display","block");
		$("#num_of_cases_a_month_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#num_of_cases_a_month_tooltip").css("display","none");
		$("#num_of_cases_a_month_tooltip").css("visibility","none");
	}
	if (num_of_region == "" || num_of_region <=0 || num_of_region == "-" || num_of_region.length > 7) {
		$("#num_of_region_tooltip").css("display","block");
		$("#num_of_region_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#num_of_region_tooltip").css("display","none");
		$("#num_of_region_tooltip").css("visibility","none");
	}
	if (num_of_lawyer == "" || num_of_lawyer <=0 || num_of_lawyer == "-" || num_of_lawyer.length > 7) {
		$("#num_of_lawyer_tooltip").css("display","block");
		$("#num_of_lawyer_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#num_of_lawyer_tooltip").css("display","none");
		$("#num_of_lawyer_tooltip").css("visibility","none");
	}
	if (num_of_customer == "" || num_of_customer <=0 || num_of_customer  == "-" || num_of_customer.length > 7) {
		$("#num_of_customer_tooltip").css("display","block");
		$("#num_of_customer_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#num_of_customer_tooltip").css("display","none");
		$("#num_of_customer_tooltip").css("visibility","none");
	} 
	if (price == "" || price <= 0 || price  == "-" || price.length > 7) {
		$("#price_tooltip").css("display","block");
		$("#price_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#price_tooltip").css("display","none");
		$("#price_tooltip").css("visibility","none");
	}
}
</script>
<meta charset="utf-8" />
<title><?php echo TITLE; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin RTL Theme #3 for dashboard & statistics"
	name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css"
	rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/global/plugins/morris/morris.css" rel="stylesheet"
	type="text/css" />
<link href="<?=base_url()?>assets/global/plugins/fullcalendar/fullcalendar.min.css"
	rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/jqvmap.css"
	rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?=base_url()?>assets/global/css/components-rtl.min.css"
	rel="stylesheet" id="style_components" type="text/css" />
<link href="<?=base_url()?>assets/global/css/plugins-rtl.min.css" rel="stylesheet"
	type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="<?=base_url()?>assets/layouts/layout3/css/layout-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/layouts/layout3/css/themes/default-rtl.min.css"
	rel="stylesheet" type="text/css" id="style_color" />
<link href="<?=base_url()?>assets/layouts/layout3/css/custom-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon"
	href="<?=base_url()?>assets/layouts/layout3/img/logo-default.jpg" />
</head>
<!-- END HEAD -->
<body class="page-container-bg-solid">
	<div class="page-wrapper">
<?php include 'general_admin_header.php';?>
<br>
				<div class="page-wrapper-row full-height">
			<div class="page-wrapper-middle">
				<!-- BEGIN CONTAINER -->
				<div class="page-container">
					<!-- BEGIN CONTENT -->
					<div class="page-content-wrapper">
						<!-- BEGIN CONTENT BODY -->
						<!-- BEGIN PAGE HEAD-->
						<div class="page-head">
							<div class="container">
								<!-- BEGIN PAGE TOOLBAR -->
								<div class="col-md-14">
									<div class="portlet box green">
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-gift"></i>
												<?php
												if ($package_name) {
													echo $package_name;
												} else {
													echo ADD_RECORD;
												}
												?>
											</div>
										</div>
										<div class="portlet-body form">
											<!-- BEGIN FORM-->
											<form id="myForm" role="form" method="post"
												onsubmit="javascript:return checkValid();"
												action="<?=site_url('admin_panel/add_edit_package/'.$package_code)?>"
												class="form-horizontal">
												<div class="form-body">
													<div class="row">
													<div class="col-md-6">
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo CODE; ?><span class="required"> * </span></label>
														<div class="col-md-6">
															<input type="text" class="no-validate form-control "
																name="package_code" value="<?= $package_code; ?>"
																id="package_code"
																onfocus="javascript:removeTooltip('package_code_tooltip');">
															<span id="package_code_tooltip" class="tooltiptext"
																style="display: none; color:red;"><?php echo FILL_THIS_FIELD; ?></span>
														</div>
													</div>
													</div>
													<div class="col-md-6">
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo NAME; ?><span class="required"> * </span></label>
														<div class="col-md-6">
															<input type="text" class="form-control "
																name="package_name" value="<?= $package_name; ?>"
																onfocus="javascript:removeTooltip('package_name_tooltip');">
															<span id="package_name_tooltip" class="tooltiptext"
																style="display: none; color:red;"><?php echo FILL_THIS_FIELD; ?></span>
														</div>
													</div>
													</div>
													</div>
													<div class="row">
													<div class="col-md-6">
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo NUM_OF_CASES_A_MONTH; ?> <span class="required"> * </span></label>
														<div class="col-md-6">
															<input type="text" act = "yes" class="form-control no-spinners" onkeydown="numberKeyDown(event)"
																name="num_of_cases_a_month"
																value="<?= $num_of_cases_a_month; ?>"
																id="num_of_cases_a_month"
																onfocus="javascript:removeTooltip('num_of_cases_a_month_tooltip');">
															<span id="num_of_cases_a_month_tooltip"
																class="tooltiptext" style="display: none; color:red;"><?php echo NUMBER_INVALID; ?></span>
														</div>
													</div>
													</div>
													<div class="col-md-6">
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo NUM_OF_REGIONS; ?> <span class="required"> * </span> </label> 
														<div class="col-md-6">
															<input type="text" act = "yes" class="form-control no-spinners" onkeydown="numberKeyDown(event)"
																name="num_of_region" value="<?= $num_of_region; ?>"
																id="num_of_region"
																onfocus="javascript:removeTooltip('num_of_region_tooltip');">
															<span id="num_of_region_tooltip" class="tooltiptext"
																style="display: none;color:red;"><?php echo NUMBER_INVALID; ?></span>
														</div>
													</div>
													</div>
													</div>
													<div class="row">
													<div class="col-md-6">
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo NUM_OF_LAWYER; ?> <span class="required"> * </span></label>
														<div class="col-md-6">
															<input type="text" act = "yes" class="form-control no-spinners" onkeydown="numberKeyDown(event)"
																name="num_of_lawyer" value="<?= $num_of_lawyer; ?>"
																id="num_of_lawyer"
																onfocus="javascript:removeTooltip('num_of_lawyer_tooltip');">
															<span id="num_of_lawyer_tooltip" class="tooltiptext"
																style="display: none; color:red;"><?php echo NUMBER_INVALID; ?></span>
														</div>
													</div>
													</div>
													<div class="col-md-6">
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo NUM_OF_CUSTOMER; ?> <span class="required"> * </span> </label>
														<div class="col-md-6">
															<input type="text" act = "yes" class="form-control no-spinners" onkeydown="numberKeyDown(event)"
																name="num_of_customer" value="<?= $num_of_customer; ?>"
																id="num_of_customer"
																onfocus="javascript:removeTooltip('num_of_customer_tooltip');">
															<span id="num_of_customer_tooltip" class="tooltiptext"
																style="display: none; color:red;"><?php echo NUMBER_INVALID; ?></span>
														</div>
													</div>
													</div>
													</div>
													<div class="row">
													<div class="col-md-6">
													<div class="form-group last">
														<label class="col-md-3 control-label"><?php echo PRICE; ?> <span class="required"> * </span></label>
														<div class="col-md-4">
															<input type="text" act = "yes" step="10.0"
																class="form-control no-spinners" onkeydown="numberKeyDown(event)" name="price"
																value="<?= $price; ?>" id="price"
																onfocus="javascript:removeTooltip('price_tooltip');"> <span
																id="price_tooltip" class="tooltiptext"
																style="display: none; color:red;"><?php echo NUMBER_INVALID; ?></span>
														</div>
														<label class="col-md-2 control-label"><?php echo SR; ?></label>
													</div>
													</div>
													</div>
													<div class="row">
													<div class="col-md-9">
													<div class="form-group">
														<label class="col-md-2 control-label"><?php echo DESCRIPTION; ?></label>
														<div class="col-md-9">
														<textarea class="form-control no-validate" rows="4" cols="50" name="package_description"
																form="myForm"><?php echo $package_description?></textarea>
									
														</div>
													</div>
													</div>
													</div>
													
												</div>
												<div class="form-actions">
													<div class="row">
														<div class="col-md-offset-3 col-md-9">
															<button type="submit" class="btn btn-circle green"><?php
															if ($package_name) {
																echo EDIT;
															} else {
																echo INSERT;
															}
															?></button>
															<a
																href="<?=site_url('admin_panel/view_packages')?>"
																class="btn btn-outline btn-circle grey-salsa"><?php echo CANCEL; ?>
															</a>
														</div>
													</div>
												</div>
											</form>
											<!-- END FORM-->
										</div>
									</div>
								</div>
                                                          
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
        <?php $this->load->view('utils/footer');?>
	<!--[if lt IE 9]>
<script src="<?=base_url()?>assets/global/plugins/respond.min.js"></script>
<script src="<?=base_url()?>assets/global/plugins/excanvas.min.js"></script> 
<script src="<?=base_url()?>assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
	<!-- BEGIN CORE PLUGINS -->
	<script src="<?=base_url()?>assets/global/plugins/jquery.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="<?=base_url()?>assets/global/plugins/moment.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/morris/morris.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/morris/raphael-min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/counterup/jquery.waypoints.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/counterup/jquery.counterup.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/fullcalendar/fullcalendar.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.resize.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.categories.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/jquery.sparkline.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="<?=base_url()?>assets/global/scripts/app.min.js"
		type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?=base_url()?>assets/pages/scripts/dashboard.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js"
		type="text/javascript"></script>
	<!-- END THEME LAYOUT SCRIPTS -->

</body>

</html>