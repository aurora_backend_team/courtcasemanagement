<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<?php
session_start ();
$this->load->view('localization/lang');
$this->load->view('localization/txt');
defineLocale ();
defineStrings ();

function encrypt_password($password){
$encrypted = "";
for($i = 0; $i < strlen ($password); $i ++) {
	$encrypted = $encrypted ."*";
}
	echo $encrypted;
}


function set_code ($code){
		$GLOBALS['code'] = $code;
}
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title><?php echo TITLE; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin RTL Theme #3 for dashboard & statistics"
	name="description" />
<meta content="" name="author" />

<link rel="shortcut icon"
	href="<?=base_url()?>assets/layouts/layout3/img/logo-default.jpg" />

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

</head>
<!-- END HEAD -->

<body class="page-container-bg-solid">
<?php include 'general_admin_header.php';?>
<div class="page-wrapper">
  <div class="container"> 
		
	<!-- BEGIN BORDERED TABLE PORTLET-->
	<div class="page-content-inner">
        <div class="row">
            <div class="col-md-13">
                <div class="portlet light portlet-fit ">
					<div class="portlet-title">
						<div class="caption">
							<span class="caption-subject font-dark bold uppercase"><?php echo ACTIVITIES; ?></span>
						</div>
					</div>
				</div>
				<div class="table-scrollable">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th><?php echo USERNAME; ?></th>
							<th><?php echo WHOLE_NAME; ?></th>
							<th><?php echo USER_TYPE; ?></th>
							<th><?php echo ACTION; ?></th>							
							<th><?php echo DATE; ?></th>
							<th><?php echo TIME; ?></th>
							<th><?php echo USER_IP; ?></th>
							<th><?php echo LAWYER_OFFICE; ?></th>
						</tr>
					</thead>
					<tbody>
							<?php
							if ($activities) {
								for($i = 0; $i < count ($activities); $i ++) {?>
						<tr>
							<td><?=$activities[$i]["user_name"];?> </td>
							<td><?=$activities[$i]["first_last_name"];?> </td>
							<td><?=$activities[$i]["user_type_name"];?> </td>
							<td><?=$activities[$i]["activity"];?> </td>
							<td><?=$activities[$i]["date"];?> </td>
							<td><?=$activities[$i]["time"];?> </td>
							<td><?=$activities[$i]["user_IP"];?> </td>
							<td><?=$activities[$i]["lawyer_office_name"];?> </td>
						</tr>	
						<?php
								}
							} else {
								?><tr class="HeadBr2">
							<td colspan="22" style="color: red" align="center"><h4>
							<?php echo NO_RECORDS_FOUND; ?>
							</h4></td>
						</tr> <?php }?>
						</tbody>
				</table>
			</div>
			<!-- END BORDERED TABLE PORTLET-->
			</div>
		</div>
	</div>
  </div>
</div>
								
							
<?php $this->load->view('utils/footer');?>

<!-- BEGIN CORE PLUGINS -->
        <script src="<?=base_url()?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?=base_url()?>assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?=base_url()?>assets/pages/scripts/profile.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
        
</body>

</html>