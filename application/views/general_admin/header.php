<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title><?php echo TITLE; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin RTL Theme #3 for dashboard & statistics"
	name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"
	rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/global/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
	rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css"
	rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/global/plugins/morris/morris.css" rel="stylesheet"
	type="text/css" />
<link href="<?=base_url()?>assets/global/plugins/fullcalendar/fullcalendar.min.css"
	rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/jqvmap.css"
	rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
 <link href="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/global/css/components-rtl.min.css" rel="stylesheet"
	id="style_components" type="text/css" />
<link href="<?=base_url()?>assets/global/css/plugins-rtl.min.css" rel="stylesheet"
	type="text/css" />
<!-- END THEME GLOBAL STYLES -->

<!-- PROFILE STYLE -->	
<link href="<?=base_url()?>assets/pages/css/profile-rtl.min.css" rel="stylesheet" type="text/css" />

<!-- BEGIN THEME LAYOUT STYLES -->
<link href="<?=base_url()?>assets/layouts/layout3/css/layout-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/layouts/layout3/css/themes/default-rtl.min.css"
	rel="stylesheet" type="text/css" id="style_color" />
<link href="<?=base_url()?>assets/layouts/layout3/css/custom-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/global/plugins/bootstrap/css/ccm_css.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon" href="<?=base_url()?>images/logohighquality.png" />
<script>
function numberKeyDown(e,elem) {
	var keynum;
    if(window.event) { // IE                    
      keynum = e.keyCode;
    } else if(e.which){ // Netscape/Firefox/Opera                   
      keynum = e.which;
    }

// Allow: backspace, delete, tab, escape, enter, up and down.
    if ($.inArray(keynum, [46, 8, 9, 27, 13,0,39,38,40]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (keynum === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
        // Allow: Ctrl+C, Command+C
        (keynum === 67 && (e.ctrlKey === true || e.metaKey === true)) || 
        // Allow: Ctrl+X, Command+X
        (keynum === 88 && (e.ctrlKey === true || e.metaKey === true)) ||
     // Allow: Ctrl+V, Command+V
        (keynum === 86 && (e.ctrlKey === true || e.metaKey === true)) || 
         // Allow: home, end, left, right, down, up
        (keynum >= 35 && keynum <= 37)) {
             // let it happen, don't do anything
             return;
    }
    
    if(((e.shiftKey || (keynum < 48 || keynum > 57)) && (keynum < 96 || keynum > 105)) || (keynum >= 38 && keynum <= 40)){
		 e.preventDefault();
	}
}
</script>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/jquery.calendars.picker.css"> 
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?=base_url()?>assets/js/jquery.plugin.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery.calendars.all.js"></script>
<script src="<?=base_url()?>assets/js/jquery.calendars.plus.js"></script>
<script src="<?=base_url()?>assets/js/jquery.calendars.ummalqura.js"></script>
<script src="<?=base_url()?>assets/js/jquery.calendars.ummalqura-ar.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>

  <script>
  $( function() {
    $( ".date" ).calendarsPicker({calendar: $.calendars.instance('ummalqura','ar')});
  } );
  </script>
  
<script>
function textKeyDown(e,elem) {
	var keynum;
    if(window.event) { // IE                    
      keynum = e.keyCode;
    } else if(e.which){ // Netscape/Firefox/Opera                   
      keynum = e.which;
    }
	//alert (keynum);
  // Allow: backspace, delete, tab, escape, enter, up and down.
    if ($.inArray(keynum, [46, 8, 9, 27, 13, 110, 190,39,38,40]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (keynum === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
        // Allow: Ctrl+C, Command+C
        (keynum === 67 && (e.ctrlKey === true || e.metaKey === true)) || 
        // Allow: Ctrl+X, Command+X
        (keynum === 88 && (e.ctrlKey === true || e.metaKey === true)) || 
         // Allow: home, end, left, right, down, up
        (keynum >= 35 && keynum <= 37)) {
             // let it happen, don't do anything
             return;
    }
	
    if(((keynum >= 48 && keynum <= 57) || (keynum >= 96 && keynum <= 105)) || (keynum >= 38 && keynum <= 40)){
		 e.preventDefault();
	}
}

function isValidDate(dateVar){
	//Format: yyyy/mm/dd
	var dateReg = /^\d{4}\/\d{2}\/\d{2}$/;
	var isV = dateReg.test(dateVar);
	if(!isV){
		return false;
	} 
	return true;
}
function parseArabic(str) {
    return ( str.replace(/[٠١٢٣٤٥٦٧٨٩]/g, function(d) {
        return d.charCodeAt(0) - 1632;
    }).replace(/[۰۱۲۳۴۵۶۷۸۹]/g, function(d) {
        return d.charCodeAt(0) - 1776;
    }) );
}

function textNumViolation(){
var violate = false;
	$('*[id*=valid_span]:visible').each(function() {
	    //     alert($(this).text());
		 
		 violate = true;
	});
	return violate ;
}

function replacAllArabToEng(){
	$(':input').each(function(){
	         var attr = $(this).attr('act');
		if (typeof attr !== typeof undefined){
			$(this).val(parseArabic($(this).val()));
			//alert($(this).val());
		 }
	});
}

function handlingTextNumberViolation (item){
	var attr = item.attr('act');
    			 
    			var val = parseArabic(item.val());
    			//alert(val);
				var matches = val.match(/\d+/g);
				
				if (item.attr("class").indexOf("date")>=0 && val!="" &&!dateValidation(val))
					$('#valid_span_'+item.attr('name')).css("display","block");
				else if ((item.attr("class").indexOf("date")<0 && (matches != null && typeof attr == typeof undefined))||(typeof attr !== typeof undefined && !$.isNumeric(val) && val!="")) {
					$('#valid_span_'+item.attr('name')).css("display","block");
					
				}else /*if(!violated || (violated && val!="")||item.attr("type") == "text")*/{
					$('#valid_span_'+item.attr('name')).css("display","none");
					/*if(item.attr("type") == "number"){
						violated  = false;
					}*/
				}
}
 $(document).ready(function(){
	 $(":input").on('input', function() {
   		handlingTextNumberViolation($(this)); 
	});
	 $(':input').each(function(){
		 if($(this).attr("class") && $(this).attr("class").indexOf(" date")>=0)
			 $(this).after('<span id="valid_span_'+$(this).attr('name')+'" style="display:none;color:red;"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>');
		 else if ($(this).attr("type") == "text"&& $(this).attr("class").indexOf("no-validate")<0 )
		 	$(this).after('<span id="valid_span_'+$(this).attr('name')+'" style="display:none;color:red;"><?php echo NO_NUMBERS; ?></span>');
		 else if ($(this).attr("type") == "number")
			 $(this).after('<span id="valid_span_'+$(this).attr('name')+'" style="display:none;color:red;"><?php echo NO_TEXT; ?></span>');
	});
	 $(':input').each(function(){
         var attr = $(this).attr('act');
	 if($(this).attr("class") && $(this).attr("class").indexOf(" date")>=0)
		 $(this).after('<span id="valid_span_'+$(this).attr('name')+'" style="display:none;color:red;"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>');
	 else if (typeof attr == typeof undefined && $(this).attr("class") && $(this).attr("class").indexOf("no-validate")<0 )
	 	$(this).after('<span id="valid_span_'+$(this).attr('name')+'" style="display:none;color:red;"><?php echo NO_NUMBERS; ?></span>');
	 else if (typeof attr !== typeof undefined){
		// $(this).attr("oninvalid", "setCustomValidity('<?php echo NO_TEXT; ?>')");
		// $(this).attr("onchange", "setCustomValidity('')");
		 $(this).after('<span id="valid_span_'+$(this).attr('name')+'" style="display:none;color:red;"><?php echo NO_TEXT; ?></span>');
	 }
});
 $(':input').on('keydown', function(e) {
 var attr = $(this).attr('act');
 
	 if (typeof attr == typeof undefined && $(this).attr("class").indexOf("no-validate")<0 && $(this).attr("class").indexOf("date")<0)
    	textKeyDown(e,$(this));
	 else if (typeof attr !== typeof undefined){
		 numberKeyDown(e,$(this));
		
/*$(this).bind("paste",function(e) {
			  var pastedData = e.originalEvent.clipboardData.getData('text');
			  
//alert(pastedData);
			if(!$.isNumeric(pastedData)){
			   $('#valid_span_'+$(this).attr('name')).css("display","block");
			   violated  = true;
			   // to support safari as it save last legal value
			   
			   
			   $(this).val("");
			   //var value= pastedData.replace(/[^0-9\.]/g, '');
			   //value= value.replace('e', '');
			   //value= value.replace('.', '');
			   //value= value.replace('.', '');
			   //$(this).val(value);
			   //e.preventDefault();
			   } else {
			    $('#valid_span_'+$(this).attr('name')).css("display","none");
			    violated = false;
			   }
			});*/


	}
    	
    });
	
	// Disable scroll when focused on a number input.
    $('form').on('focus', 'input[type=number]', function(e) {
	$(this).on('wheel', function(e) {
		e.preventDefault();
	});
    });

    // Restore scroll on number inputs.
    $('form').on('blur', 'input[type=number]', function(e) {
    	$(this).off('wheel');
    });

    // Disable up and down keys.
    $('form').on('keydown', 'input[type=number]', function(e) {
	if ( e.which == 38 || e.which == 40 )
		e.preventDefault();
    }); 

    //disable mousewheel on a input number field when in focus
    //(to prevent Cromium browsers change the value when scrolling)
    $('form').on('focus', 'input[type=number]', function (e) {
	$(this).on('mousewheel.disableScroll', function (e) {
		e.preventDefault();
	});
    });
    $('form').on('blur', 'input[type=number]', function (e) {
    	$(this).off('mousewheel.disableScroll');
    });	 
	
});
</script>

<style type="text/css">
	.table{
	     max-width:none !important;
	}
	.table-scrollable{
		-webkit-overflow-scrolling: touch;
	}
</style>

</head>
<!-- END HEAD -->

<body class="page-container-bg-solid">
	<!-- BEGIN HEADER TOP -->
	<div class="page-header-top">
		<div class="container">
			<!-- BEGIN LOGO -->
			<div class="page-logo">
				<a href="<?=site_url('admin_panel/view_packages')?>"> <img
					src="<?=base_url()?>images/logohighquality.png" alt="logo"
					class="logo-default">
				</a>
			</div>
			<!-- END LOGO -->
			<!-- BEGIN RESPONSIVE MENU TOGGLER -->
			<a href="javascript:;" class="menu-toggler"></a>
			<!-- END RESPONSIVE MENU TOGGLER -->
			<!-- BEGIN TOP NAVIGATION MENU -->
			<div class="top-menu">
				<ul class="nav navbar-nav pull-right">
					<!-- BEGIN NOTIFICATION DROPDOWN -->
					<!-- DOC: Apply "dropdown-hoverable" class after "dropdown" and remove data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to enable hover dropdown mode -->
					<!-- DOC: Remove "dropdown-hoverable" and add data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to the below A element with dropdown-toggle class -->
					
					<!-- BEGIN QUICK SIDEBAR TOGGLER -->
					<li class="dropdown dropdown-user dropdown-dark"><a
						href="<?=site_url('admin/logoff')?>" class="dropdown-toggle"
						data-hover="dropdown" > <img alt=""
						 src="<?=base_url()?>assets/layouts/layout3/img/logout-icon.png"></a></li>
					<!-- END QUICK SIDEBAR TOGGLER -->
				</ul>
			</div>
			<!-- END TOP NAVIGATION MENU -->
		</div>
	</div>
</body>
</html>