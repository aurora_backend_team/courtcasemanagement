<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title><?php echo TITLE; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin RTL Theme #3 for dashboard & statistics"
	name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"
	rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/global/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
	rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css"
	rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/global/plugins/morris/morris.css" rel="stylesheet"
	type="text/css" />
<link href="<?=base_url()?>assets/global/plugins/fullcalendar/fullcalendar.min.css"
	rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/jqvmap.css"
	rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?=base_url()?>assets/global/css/components-rtl.min.css" rel="stylesheet"
	id="style_components" type="text/css" />
<link href="<?=base_url()?>assets/global/css/plugins-rtl.min.css" rel="stylesheet"
	type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="<?=base_url()?>assets/layouts/layout3/css/layout-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/layouts/layout3/css/themes/default-rtl.min.css"
	rel="stylesheet" type="text/css" id="style_color" />
<link href="<?=base_url()?>assets/layouts/layout3/css/custom-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon"
	href="<?=base_url()?>images/logohighquality.png" />

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
    $("#delBtn").click(function(){
        $("#delNotify").show();
    });
    $("#cancel").click(function(){
        $("#delNotify").hide();
    });
    $("#delNotify").hide();
});
</script>

</head>
<!-- END HEAD -->

<body class="page-container-bg-solid">
	<div class="page-wrapper-row">
		<div class="page-wrapper-top">
			<!-- BEGIN HEADER -->
			<div class="page-header">
		<?php include('header.php');?>
		
				<form  style="visibility:hidden; height: 0px !important;width:0px !important;" class="search-form" action="page_general_search.html"
					method="GET">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search"
							name="query"> <span class="input-group-btn"> <a
							href="javascript:;" class="btn submit"> <i
								class="icon-magnifier"></i>
						</a>
						</span>
					</div>
				</form>
						
				<!-- BEGIN HEADER MENU -->
				<div class="page-header-menu">
					<div class="container">
						<div class="hor-menu  ">
							<ul class="nav navbar-nav">
							<?php if (isset($packages) || isset($package_name)) {?>
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown active">
								<?php } else {?>	
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown">
									<?php } ?>
									<a
									href="<?=site_url('admin_panel/view_packages')?>"> <?php echo PACKAGES; ?> <span
										class="arrow"></span>
								</a></li>
								<?php if (isset($lawyer_offices)|| isset($lawyer_office_id)) {?>
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown active">
								<?php } else {?>	
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown">
									<?php } ?>
								<a
									href="<?=site_url('admin_panel/view_lawyer_offices')?>"> <?php echo LAWYER_OFFICES; ?> <span
										class="arrow"></span>
								</a></li>
																
								<?php if (isset($suspend_reasons) || isset($suspend_code)) {?>
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown active">
								<?php } else {?>	
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown">
									<?php } ?><a
									href="<?=site_url('case_suspend_reasons/view_case_suspend_reasons')?>"> <?php echo SUSPEND_REASONS; ?> <span class="arrow"></span>
								</a></li>
								
								<?php if (isset($activities)) {?>
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown active">
								<?php } else {?>	
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown">
									<?php } ?><a
									href="<?=site_url('users_activities/view_users_activities')?>"> <?php echo ACTIVITIES; ?> <span class="arrow"></span>
								</a></li>
								
								<?php if (isset($dashboardReports)) {?>
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown active">
								<?php } else {?>	
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown">
									<?php } ?><a
									href="<?=site_url('dashboard_reports/view_dashboard_reports/1')?>"> <?php echo DASHBOARD_REPORTS; ?> <span class="arrow"></span>
								</a></li>
								
							</ul>
						</div>
						<!-- END MEGA MENU -->
					</div>
				</div>
				<!-- END HEADER MENU -->
			</div>
			<!-- END HEADER -->
		</div>
	</div>
</body>