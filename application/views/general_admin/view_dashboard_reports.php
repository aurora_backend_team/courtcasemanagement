<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<?php
session_start ();
$this->load->view ( 'localization/lang' );
$this->load->view ( 'localization/txt' );
defineLocale ();
defineStrings ();
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>

<style>
.amcharts-chart-div a {
	display: none !important;
}

#legenddiv {
	width: 100%;
	height: 250px !important;
	overflow: auto;
	position: relative;
}

#dashboard_amchart_report4 {
	width: 100%;
	height: 500px;
}

.table-custom th, .table-custom td { min-width: 50px; word-wrap:break-word;}


.table-custom{
     max-width:none !important;
}
	
.rep2-custom-class{
	max-width:90% !important;
	margin:auto;
	text-align:center;
	 -webkit-overflow-scrolling: touch;
}

.custom-table-header{
	padding-top:8px !important;
	padding-bottom:8px !important;
	padding-left:4px !important;
	padding-right:4px !important;
}

.custom-portlet{
	margin-left: 0px !important;
 	margin-right: 0px !important;
 	padding-left: 0px !important;
 	padding-right: 0px !important;
}

.caption-custom{
	width:75% !important;
}

.container-outer { overflow-x: scroll; width: 1100px;}
.container-inner { width: 1100px;}

@media only screen and (max-width: 1200px) {
	.container-outer { overflow-x: scroll; width: 900px; }
	.container-inner { width: 900px;}
}

@media only screen and (max-width: 1100px) {
	.container-outer { overflow-x: scroll; width: 900px; }
	.container-inner { width: 900px;}
}

@media only screen and (max-width: 1000px) {
	.container-outer { overflow-x: scroll; width: 900px; }
	.container-inner { width: 900px;}
}

@media only screen and (max-width: 900px) {
	.container-outer { overflow-x: scroll; width: 730px; }
	.container-inner { width: 900px;}
}

@media only screen and (max-width: 800px) {
	.container-outer { overflow-x: scroll; width: 630px; }
	.container-inner { width: 900px;}
}

@media only screen and (max-width: 700px) {
	.container-outer { overflow-x: scroll; width: 530px; }
	.container-inner { width: 900px;}
}

@media only screen and (max-width: 600px) {
	.container-outer { overflow-x: scroll; width: 430px; }
}

@media only screen and (max-width: 500px) {
	.container-outer { overflow-x: scroll; width: 330px; }
	.container-inner { width: 900px;}
	.custom-table-cell{
		max-width:70px;
	}
}

@media only screen and (max-width: 400px) {
	.container-outer { overflow-x: scroll; width: 230px; }
	.container-inner { width: 900px;}
	.custom-table-cell{
		max-width:60px;
	}
}

@media only screen and (max-width: 300px) {
	.container-outer { overflow-x: scroll; width: 130px; }
	.container-inner { width: 900px;}
	.custom-table-cell{
		max-width:50px;
	}
}

	#loading_reports {
	   width: 100%;
	   height: 100%;
	   top: 0;
	   left: 0;
	   position: fixed;
	   display: block;
	   opacity: 0.7;
	   background-color: #fff;
	   z-index: 99;
	   text-align: center;
	}

	#loading-image {
	  position: absolute;
	  top: 35%;
	  left: 40%;
	  z-index: 100;
	}
	
	
	#loading-text {
        position: absolute;
        top: 65%;
        left: 46%;
        margin-top: -25px;
        margin-left: -50px;
        font-size: 16px;
        font-weight: bold;
        color: #054A35;
	    z-index: 200;
	}
	
</style>


<script>
window.onload = function() {
	makeAjaxCall(0);
};
</script>


<script>
function makeAjaxCall(flag){
	
	$("#report4Div").css("display","none");
	$("#report4Div").css("visibility","none");

	var fromDate = $('#from_date2').val();
	var toDate = $('#to_date2').val();
	var lawyerOfficeId = $('#filter_lawyer_office_id').val();

	if (lawyerOfficeId != "") {
		$('#lawyers_performance').css("display","block");  
	}

	if(flag == 1){
		if (lawyerOfficeId == "") {
			$("#lawyer_office_id_tooltip").css("display","block");
			$("#lawyer_office_id_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#lawyer_office_id_tooltip").css("display","none");
			$("#lawyer_office_id_tooltip").css("visibility","none");
		}
	
		if (fromDate == "" || toDate == "") {
			$("#report4_date_tooltip2").css("display","block");
			$("#report4_date_tooltip2").css("visibility","visible");
			return false;
		} else{
			$("#report4_date_tooltip2").css("display","none");
			$("#report4_date_tooltip2").css("visibility","none");
		}
	}
	
	//var from_date2 = document.forms["myForm"]["from_date2"].value;
	var from_date2 = document.getElementById("from_date2").value;
	
	//var to_date2 = document.forms["myForm"]["to_date2"].value;
	var to_date2 = document.getElementById("to_date2").value;

	if(from_date2 != ""){
		var isValidD = isValidMiladyDate(from_date2);
	   	if(!isValidD){
			$("#start_date_tooltip2").css("display","block");
			$("#start_date_tooltip2").css("visibility","visible");
	   		return false;
	   	}else{
			$("#start_date_tooltip2").css("display","none");
			$("#start_date_tooltip2").css("visibility","none");
	   	}
	}

	if(to_date2 != ""){
		var isValidD = isValidMiladyDate(to_date2);
	   	if(!isValidD){
			$("#end_date_tooltip2").css("display","block");
			$("#end_date_tooltip2").css("visibility","visible");
	   		return false;
	   	}else{
			$("#end_date_tooltip2").css("display","none");
			$("#end_date_tooltip2").css("visibility","none");
	   	}

	   	var todayMilady =  jqOld.calendars.instance('gregorian','ar').newDate();
	   	var str_array = to_date2.split('/');
		var miladyDate =  jqOld.calendars.instance('gregorian','ar').newDate();
		miladyDate = miladyDate.date(str_array[2], str_array[0], str_array[1]);
		if (miladyDate > todayMilady) {
			$("#end_date_tooltip6").css("display","block");
			$("#end_date_tooltip6").css("visibility","visible");			
			return false;
		} else{
			$("#end_date_tooltip6").css("display","none");
			$("#end_date_tooltip6").css("visibility","none");
		}  		
	}

	if(from_date2 != "" && to_date2 != ""  ){
		
		if(from_date2 > to_date2){
			$("#end_date_tooltip3").css("display","block");
			$("#end_date_tooltip3").css("visibility","visible");
	   		return false;
		}else{
			$("#end_date_tooltip3").css("display","none");
			$("#end_date_tooltip3").css("visibility","none");
		}

		var date1 = new Date(from_date2);
		var date2 = new Date(to_date2);
		var timeDiff = Math.abs(date2.getTime() - date1.getTime());
		var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
		if((diffDays+1) >31){
			$("#report4_date_tooltip").css("display","block");
			$("#report4_date_tooltip").css("visibility","visible");
	   		return false;
		}
		else{
			$("#report4_date_tooltip").css("display","none");
			$("#report4_date_tooltip").css("visibility","none");
		}
	}

	if(fromDate != "" && toDate != "" && lawyerOfficeId !=""){
		if ( $('#loading_reports').hasClass( "hidden" ) ) {
			 $('#loading_reports').removeClass('hidden');
		}
		$("#report4Div").css("display","block");
		$("#report4Div").css("visibility","visible");
	
		 $.ajax({
		  type: "post",
		  url: '<?php echo site_url('dashboard_reports/report4_chart_data'); ?>',
		  cache: false,
		  data: { 
		      'fromDate': fromDate, 
		      'toDate': toDate,
		      'lawyerOfficeId':lawyerOfficeId
		  },
		  success: function(json){      
		  try{  
		   var obj = jQuery.parseJSON(json);
		   initAmChart(obj);
	        $('html, body').animate({
	            scrollTop: $("#dashboard_amchart_report4").offset().top
	        }, 500); 

	   		 $('#loading_reports').addClass('hidden');
		   	
		   //alert( 'Success');
		  }catch(e) {  
		   //alert('Exception while request..');
		  }  
		  },
		  error: function(){      
		   //alert('Error while request..');
		  }
		 });

	}

}
</script>

<script type="text/javascript">
 function initAmChart(data) {
    if (typeof(AmCharts) === 'undefined' || $('#dashboard_amchart_report4').size() === 0) {
        return;
    }
    var chartData = data;
    
    var chart = AmCharts.makeChart("dashboard_amchart_report4", {
        type: "serial",
        fontSize: 12,
        dataDateFormat: "YYYY-MM-DD",
        dataProvider: chartData,
        theme: "light",
        addClassNames: true,
        startDuration: 1,
        gridAboveGraphs: true,
        color: "#6c7b88",
        marginRight: 50,
        marginLeft:50,

        categoryField: "date",
        categoryAxis: {
            autoGridCount: false,
            gridCount: 50,
            gridAlpha: 0.1,
            gridColor: "#FFFFFF",
            axisColor: "#555555",
            labelRotation: 45
        },

        valueAxes: [{
            id: "a1",
            title: "<?php echo CASES_COUNT; ?>",
            gridAlpha: 0.1,
            axisAlpha: 0.1,
            position : "right"
        }],
        graphs: [{
            id: "g1",
            valueField: "created",
            title: "<?php echo R4_CREATED_CASES_COUNT; ?>",
            type: "column",
            fillAlphas: 0.7,
            valueAxis: "a1",
            lineColor: "#08a3cc",
            balloonText: "<span style='font-size:12px;'>[[title]] <?php echo IN; ?> [[category]]:<br><span style='font-size:14px;'>[[value]]</span> <?php echo CASE_TITLE; ?></span>",
            legendValueText: "[[title]] <?php echo IN; ?> [[category]] : [[value]] <?php echo CASE_TITLE; ?>"
        }, {
            id: "g2",
            valueField: "new",
            classNameField: "bulletClass",
            title: "<?php echo R4_NEW_CASES_COUNT; ?>",
            type: "line",
            lineColor: "#786c56",
            lineThickness: 2,
            bullet: "round",
            bulletBorderColor: "#02617a",
            bulletBorderAlpha: 1,
            bulletBorderThickness: 2,
            bulletColor: "#89c4f4",
            showBalloon: true,
            animationPlayed: true,
            useLineColorForBulletBorder: true,
            lineAlpha: 0.5,
            balloonText: "<span style='font-size:12px;'>[[title]] <?php echo IN; ?> [[category]]:<br><span style='font-size:14px;'>[[value]]</span> <?php echo CASE_TITLE; ?></span>",
            legendValueText: "[[title]] <?php echo IN; ?> [[category]] : [[value]] <?php echo CASE_TITLE; ?>"
                
        }, {
            id: "g3",
            valueField: "reg",
            classNameField: "bulletClass",
            title: "<?php echo R4_REG_CASES_COUNT; ?>",
            type: "line",
            lineThickness: 2,
            bullet: "round",
            bulletBorderColor: "#FFFFFF",
            bulletBorderAlpha: 1,
            bulletBorderThickness: 2,
            showBalloon: true,
            lineAlpha: 0.5,
            balloonText: "<span style='font-size:12px;'>[[title]] <?php echo IN; ?> [[category]]:<br><span style='font-size:14px;'>[[value]]</span> <?php echo CASE_TITLE; ?></span>",
            legendValueText: "[[title]] <?php echo IN; ?> [[category]] : [[value]] <?php echo CASE_TITLE; ?>"
                
        }, {
            id: "g4",
            valueField: "dec34",
            classNameField: "bulletClass",
            title: "<?php echo R4_DEC34_CASES_COUNT; ?>",
            type: "line",
            lineThickness: 2,
            bullet: "round",
            bulletBorderColor: "#FFFFFF",
            bulletBorderAlpha: 1,
            bulletBorderThickness: 2,
            showBalloon: true,
            lineAlpha: 0.5,
            balloonText: "<span style='font-size:12px;'>[[title]] <?php echo IN; ?> [[category]]:<br><span style='font-size:14px;'>[[value]]</span> <?php echo CASE_TITLE; ?></span>",
            legendValueText: "[[title]] <?php echo IN; ?> [[category]] : [[value]] <?php echo CASE_TITLE; ?>"
                
        }, {
            id: "g5",
            valueField: "adv",
            classNameField: "bulletClass",
            title: "<?php echo R4_ADV_CASES_COUNT; ?>",
            type: "line",
            lineThickness: 2,
            bullet: "round",
            bulletBorderColor: "#FFFFFF",
            bulletBorderAlpha: 1,
            bulletBorderThickness: 2,
            showBalloon: true,
            lineAlpha: 0.5,
            balloonText: "<span style='font-size:12px;'>[[title]] <?php echo IN; ?> [[category]]:<br><span style='font-size:14px;'>[[value]]</span> <?php echo CASE_TITLE; ?></span>",
            legendValueText: "[[title]] <?php echo IN; ?> [[category]] : [[value]] <?php echo CASE_TITLE; ?>"
                
        }, {
            id: "g6",
            valueField: "dec46",
            classNameField: "bulletClass",
            title: "<?php echo R4_DEC46_CASES_COUNT; ?>",
            type: "line",
            lineThickness: 2,
            bullet: "round",
            bulletBorderColor: "#FFFFFF",
            bulletBorderAlpha: 1,
            bulletBorderThickness: 2,
            showBalloon: true,
            lineAlpha: 0.5,
            balloonText: "<span style='font-size:12px;'>[[title]] <?php echo IN; ?> [[category]]:<br><span style='font-size:14px;'>[[value]]</span> <?php echo CASE_TITLE; ?></span>",
            legendValueText: "[[title]] <?php echo IN; ?> [[category]] : [[value]] <?php echo CASE_TITLE; ?>"
                
        }, {
            id: "g7",
            valueField: "closed",
            classNameField: "bulletClass",
            title: "<?php echo R4_CLOSED_CASES_COUNT; ?>",
            type: "line",
            lineThickness: 2,
            bullet: "square",
            bulletBorderColor: "#e26a6a",
            bulletBorderAlpha: 0.8,
            bulletBorderThickness: 2,
            showBalloon: true,
            lineAlpha: 0.5,
            lineColor: "#e26a6a",
            balloonText: "<span style='font-size:12px;'>[[title]] <?php echo IN; ?> [[category]]:<br><span style='font-size:14px;'>[[value]]</span> <?php echo CASE_TITLE; ?></span>",
            legendValueText: "[[title]] <?php echo IN; ?> [[category]] : [[value]] <?php echo CASE_TITLE; ?>"
                
        }],

        chartCursor: {
            zoomable: false,
            categoryBalloonDateFormat: "DD",
            cursorAlpha: 0,
            categoryBalloonColor: "#e26a6a",
            categoryBalloonAlpha: 0.8,
            valueBalloonsEnabled: false
        },
        legend: {
            divId: "legenddiv",
            align: "center",
            maxColumns: 1,
            useGraphSettings: true,
            bulletType: "round",
            equalWidths: false,
            markerType: "circle",
            color: "#6c7b88",
            markerLabelGap: 150
        }
    });

  
}


function isValidDate(dateVar){
	//Format: yyyy/mm/dd
	var dateReg = /^\d{4}\/\d{2}\/\d{2}$/;
	var isV = dateReg.test(dateVar);
	if(!isV){
		return false;
	} 
	
	var str_array = dateVar.split('/');
	var hijriDate =  jqOld.calendars.instance('ummalqura','ar').newDate();
	try {
	   hijriDate = hijriDate.date(str_array[0], str_array[1], str_array[2]);
	}
	catch(err) {
	   return false;
	}
	return true;
}

function isValidMiladyDate(dateVar){
	//Format: yyyy/mm/dd
	var dateReg = /^\d{2}\/\d{2}\/\d{4}$/;
	var isV = dateReg.test(dateVar);
	if(!isV){
		return false;
	} 
	
	var str_array = dateVar.split('/');
	var miladyDate =  jqOld.calendars.instance('gregorian','ar').newDate();
	try {
		miladyDate = miladyDate.date(str_array[2], str_array[0], str_array[1]);
	}
	catch(err) {
	   return false;
	}
	return true;
}



function checkValid() {

	$("#lawyer_office_id_tooltip").css("display","none");
	$("#lawyer_office_id_tooltip").css("visibility","none");
	$("#start_date_tooltip").css("display","none");
	$("#start_date_tooltip").css("visibility","none");
	$("#end_date_tooltip").css("display","none");
	$("#end_date_tooltip").css("visibility","none");
	$("#end_date_tooltip5").css("display","none");
	$("#end_date_tooltip5").css("visibility","none");
	$("#end_date_tooltip4").css("display","none");
	$("#end_date_tooltip4").css("visibility","none");
	$("#start_date_tooltip2").css("display","none");
	$("#start_date_tooltip2").css("visibility","none");
	$("#end_date_tooltip2").css("display","none");
	$("#end_date_tooltip2").css("visibility","none");
	$("#end_date_tooltip3").css("display","none");
	$("#end_date_tooltip3").css("visibility","none");
	$("#report4_date_tooltip").css("display","none");
	$("#report4_date_tooltip").css("visibility","none");
	
	//var filter_lawyer_office_id = document.forms["myForm"]["filter_lawyer_office_id"].value;
	var filter_lawyer_office_id = $('#filter_lawyer_office_id').val();
	var from_date = document.getElementById("from_date").value; //document.forms["myForm"]["from_date"].value;
	var to_date = document.getElementById("to_date").value;   //document.forms["myForm"]["to_date"].value;

	if (filter_lawyer_office_id== "") {
		$("#lawyer_office_id_tooltip").css("display","block");
		$("#lawyer_office_id_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#lawyer_office_id_tooltip").css("display","none");
		$("#lawyer_office_id_tooltip").css("visibility","none");
	}
	
	if(from_date != ""){
		var isValidD = isValidDate(from_date);
	   	if(!isValidD){
			$("#start_date_tooltip").css("display","block");
			$("#start_date_tooltip").css("visibility","visible");
	   			
	   		return false;
	   	}else{
			$("#start_date_tooltip").css("display","none");
			$("#start_date_tooltip").css("visibility","none");
	   	}
	}

	if(to_date != ""){
		var isValidD = isValidDate(to_date);
	   	if(!isValidD){
			$("#end_date_tooltip").css("display","block");
			$("#end_date_tooltip").css("visibility","visible");

	   		return false;
	   	}else{
			$("#end_date_tooltip").css("display","none");
			$("#end_date_tooltip").css("visibility","none");
	   	}

	   	var todayHj =  jqOld.calendars.instance('ummalqura','ar').newDate();
	   	var str_array = to_date.split('/');
		var hijriDate =  jqOld.calendars.instance('ummalqura','ar').newDate();
		hijriDate = hijriDate.date(str_array[0], str_array[1], str_array[2]);
		if (hijriDate > todayHj) {
			$("#end_date_tooltip5").css("display","block");
			$("#end_date_tooltip5").css("visibility","visible");			
			return false;
		} else{
			$("#end_date_tooltip5").css("display","none");
			$("#end_date_tooltip5").css("visibility","none");
		} 
	}

	if(from_date != "" && to_date != ""  ){
		
		if(from_date > to_date){
			$("#end_date_tooltip4").css("display","block");
			$("#end_date_tooltip4").css("visibility","visible");
	   		return false;
		}else{
			$("#end_date_tooltip4").css("display","none");
			$("#end_date_tooltip4").css("visibility","none");
		}
	}

	var from_date2 = document.getElementById("from_date2").value; //document.forms["myForm"]["from_date2"].value;
	var to_date2 = document.getElementById("to_date2").value; //document.forms["myForm"]["to_date2"].value;


	if(from_date2 != ""){
		var isValidD = isValidMiladyDate(from_date2);
	   	if(!isValidD){
			$("#start_date_tooltip2").css("display","block");
			$("#start_date_tooltip2").css("visibility","visible");
	   		return false;
	   	}else{
			$("#start_date_tooltip2").css("display","none");
			$("#start_date_tooltip2").css("visibility","none");
	   	}
	}

	if(to_date2 != ""){
		var isValidD = isValidMiladyDate(to_date2);
	   	if(!isValidD){
			$("#end_date_tooltip2").css("display","block");
			$("#end_date_tooltip2").css("visibility","visible");
	   		return false;
	   	}else{
			$("#end_date_tooltip2").css("display","none");
			$("#end_date_tooltip2").css("visibility","none");
	   	}
	}

	if(from_date2 != "" && to_date2 != ""  ){
	
		if(from_date2 > to_date2){
			$("#end_date_tooltip3").css("display","block");
			$("#end_date_tooltip3").css("visibility","visible");
   			return false;
		}else{
			$("#end_date_tooltip3").css("display","none");
			$("#end_date_tooltip3").css("visibility","none");
		}
	

		var date1 = new Date(from_date2);
		var date2 = new Date(to_date2);
		var timeDiff = Math.abs(date2.getTime() - date1.getTime());
		var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
		if((diffDays+1) >31){
			$("#report4_date_tooltip").css("display","block");
			$("#report4_date_tooltip").css("visibility","visible");
	   		return false;
		}
		else{
			$("#report4_date_tooltip").css("display","none");
			$("#report4_date_tooltip").css("visibility","none");
		}
	}
	
	return true;
	
}

function ajaxUpdateActivities(){
	$('#loading_reports').removeClass('hidden');
	var current_page = $("#current_page").val();
	var postdata = $("#myForm").serialize();
	var url = "<?=site_url('dashboard_reports/view_dashboard_reports/'.$mode)?>";
	$.post(url, postdata, function(data) {
            var results = JSON.parse(data);
            var newSearchArea = $(results).find("#activities");
            $("#activities").html(newSearchArea);	

            var newSearchArea = $(results).find("#dash_1");
            $("#report1").html(newSearchArea);	
            
            $('#lawyers_performance').css("display","block");            
            $("#lawyers_current_total").html($(results).find("#lawyers_current_total"));	
            $("#lawyers_late_total").html($(results).find("#lawyers_late_total"));
            $("#lawyers_table_1").html($(results).find("#lawyers_table_1"));
            $("#lawyers_paid_total").html($(results).find("#lawyers_paid_total"));
            $("#lawyers_unpaid_total").html($(results).find("#lawyers_unpaid_total"));
            $("#lawyers_table_2").html($(results).find("#lawyers_table_2"));

        	$('#loading_reports').addClass('hidden');
        	
           /*  $("#new_count").html($(results).find("#new_count"));
            $("#new_percnt").html($(results).find("#new_percnt"));
            $("#new_stats").html($(results).find("#new_stats")) */;

        });
}

function display_reports(){
	var valid = checkValid();
	if(valid){
		ajaxUpdateActivities();
	}
}


function next_page(){
	var current_page = parseInt($("#current_page").val()) ;
	var newpage = (current_page+1);
	$("#current_page").val(newpage);
	ajaxUpdateActivities();
}

function prev_page(){
	var current_page = parseInt($("#current_page").val()) ;
	var newpage = (current_page-1);
	$("#current_page").val(newpage);
	ajaxUpdateActivities();
}



</script>
<meta charset="utf-8" />
<title><?php echo DASHBOARD_REPORTS; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin RTL Theme #3 for dashboard & statistics"
	name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css"
	rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/global/plugins/morris/morris.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/fullcalendar/fullcalendar.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/jqvmap.css"
	rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?=base_url()?>assets/global/css/components-rtl.min.css"
	rel="stylesheet" id="style_components" type="text/css" />
<link href="<?=base_url()?>assets/global/css/plugins-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link
	href="<?=base_url()?>assets/layouts/layout3/css/layout-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/layouts/layout3/css/themes/default-rtl.min.css"
	rel="stylesheet" type="text/css" id="style_color" />
<link
	href="<?=base_url()?>assets/layouts/layout3/css/custom-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon"
	href="<?=base_url()?>assets/layouts/layout3/img/logo-default.jpg" />


</head>
<!-- END HEAD -->
<body class="page-container-bg-solid">

	<div id="loading_reports" class="hidden">
	  <img id="loading-image" src="<?php echo base_url(); ?>images/Ajax-loader2.gif" alt="Loading..." />
	  <div id="loading-text"><?php echo DASHBOARD_REPORTS_LOADING;?></div>
	</div>
	<div class="page-wrapper">
	
<?php $this->load->view('utils/date_scripts');?>

<?php
if ($mode == 1){
	$this->load->view ( 'general_admin/general_admin_header' );
}else if ($user_type_code == "ADMIN") {
	$this->load->view ( 'office_admin/office_admin_header' );
} else {
	$this->load->view ( 'secretary_lawyer/secretary_header' );
}
?>

		<div class="page-wrapper-row full-height">
			<div class="page-wrapper-middle">
				<!-- BEGIN CONTAINER -->
				<div class="page-container">
					<!-- BEGIN CONTENT -->
					<div class="page-content-wrapper">
						<!-- BEGIN CONTENT BODY -->

						<div class="page-head">
							<div class="container">
								<!-- BEGIN PAGE TITLE -->
								<div class="page-title" style="padding-top: 0px !important; ">
									<h1>
										<?php echo DASHBOARD_REPORTS; ?><small style="padding-right: 10px !important;"><?php
										echo DASHBOARD_REPORTS_SMALL_TITLE;
										?></small>
									</h1>
								</div>
								<!-- END PAGE TITLE -->
							</div>
						</div>


						<!-- BEGIN PAGE CONTENT BODY -->
						<div class="page-content">
							<div class="container">
								<div class="page-content-inner">
									<div id="repForm" class="row">
										<div class="col-md-12">
											<div class="portlet box green">
												<div class="portlet-title">
													<div class="caption">
														<i class="fa fa-gift"></i>
												<?php
												echo DASHBOARD_REPORTS_CRITERIA;
												?>
											</div>
												</div>
												<div class="portlet-body form">
													<!-- BEGIN FORM-->
													<form id="myForm" role="form" method="post"
														onsubmit="javascript:return checkValid();"
														action="<?=site_url('dashboard_reports/view_dashboard_reports/'.$mode)?>"
														class="form-horizontal">
														<div class="form-body">
														
														
											<input type="hidden" name="current_page"
															id="current_page" value="<?php echo $current_page;?>">
															
															<div class="row">
																<div class="col-md-6">
<?php  if ($mode == 1) {?>
																	<div class="form-group">
																		<label class="col-md-3 control-label"><?php echo LAWYER_OFFICES; ?> <span
																			class="required"> * </span> </label>
																		<div class="col-md-6">
																			<select class="form-control "
																				name="filter_lawyer_office_id"
																				id="filter_lawyer_office_id" class="form-control">
																				<option value="" selected></option>
														  	<?php
																	if ($filter_lawyer_offices) {
																		for($i = 0; $i < count ( $filter_lawyer_offices ); $i ++) {
																			$lawyer_office_id_s = $filter_lawyer_offices [$i] ["lawyer_office_id"];
																			$name = $filter_lawyer_offices [$i] ["name"];
																			?>
																<option value="<?=$lawyer_office_id_s?>"
																					<?php if ($filter_lawyer_office_id == $lawyer_office_id_s){?>
																					selected="selected" <?php }?>> <?=$name?></option>
																			
																			
																<?php
																		}
																	} else {
																		?>
																
																			
																			<option value="" selected></option>
															<?php
																	}
																	?>		
														</select> <span id="lawyer_office_id_tooltip"
																				class="tooltiptext"
																				style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>


																		</div>
																	</div>
<?php }  ?>
															
																</div>

															</div>
															<div class="row">
																<div class="col-md-6">
																	<div class="form-group">
																		<label class="col-md-8 control-label"><?php echo REPORT4_NOTE1; ?></label>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-6">
																	<div class="form-group">
																		<label class="col-md-3 control-label"><?php echo FROM_DATE; ?></label>
																		<div class="col-md-6">
																			<input type="text" placeholder="" id="from_date"
																				class="form-control date" name="from_date"
																				value="<?php echo $from_date?>"> <span
																				id="start_date_tooltip" class="tooltiptext"
																				style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>
																		</div>
																	</div>
																</div>

																<div class="col-md-6">
																	<div class="form-group last">
																		<label class="col-md-3 control-label"><?php echo TO_DATE; ?></label>
																		<div class="col-md-6">
																			<input type="text" class="form-control date"
																				id="to_date" name="to_date"
																				value="<?php echo $to_date?>"> <span
																				id="end_date_tooltip" class="tooltiptext"
																				style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>
																			<span id="end_date_tooltip4" class="tooltiptext"
																				style="display: none; color: red"><?php echo DATE_ERROR_SEQ; ?></span>
																			<span id="end_date_tooltip5" class="tooltiptext"
																				style="display: none; color: red"><?php echo DATE_CAN_NOT_BE_GREATER_THAN_TODAY; ?></span>
																		</div>
																	</div>
																</div>


															</div>

															<div class="row">
																<div class="col-md-6">
																	<div class="form-group">
																		<label class="col-md-8 control-label"><?php echo REPORT4_NOTE2; ?></label>
																	</div>
																</div>
															</div>

															<div class="row">

																<div class="col-md-6">
																	<div class="form-group">
																		<label class="col-md-3 control-label"><?php echo FROM_DATE.' '.DATE_GREGORIAN; ?> <span
																			class="required"> * </span></label>
																		<div class="col-md-6">
																			<input type="text" placeholder="" id="from_date2"
																				class="form-control dateMilady" name="from_date2"
																				value="<?php echo $from_date2?>"> <span
																				id="start_date_tooltip2" class="tooltiptext"
																				style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>
																		</div>
																	</div>
																</div>



																<div class="col-md-6">
																	<div class="form-group last">
																		<label class="col-md-3 control-label"><?php echo TO_DATE.' '.DATE_GREGORIAN; ?><span
																			class="required"> * </span></label>
																		<div class="col-md-6">
																			<input type="text" class="form-control dateMilady"
																				id="to_date2" name="to_date2"
																				value="<?php echo $to_date2?>"> <span
																				id="end_date_tooltip2" class="tooltiptext"
																				style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>
																			<span id="end_date_tooltip3" class="tooltiptext"
																				style="display: none; color: red"><?php echo DATE_ERROR_SEQ; ?></span>
																			<span id="end_date_tooltip6" class="tooltiptext"
																				style="display: none; color: red"><?php echo DATE_CAN_NOT_BE_GREATER_THAN_TODAY; ?></span>
																			<span id="report4_date_tooltip" class="tooltiptext"
																				style="display: none; color: red"><?php echo DATE_ERROR_DIFF; ?></span>
																			<span id="report4_date_tooltip2" class="tooltiptext"
																				style="display: none; color: red"><?php echo DATE_ERROR_REPORT4; ?></span>
																		</div>
																	</div>
																</div>

															</div>

														</div>
														<div class="form-actions">
															<div class="row">
																<div style="margin: auto; text-align: center;">
																	<button type="button" class="btn btn-circle green"
																	onClick="javascript:display_reports()"
																	style="font-size:13px !important; margin-top:5px;margin-bottom:5px;"><?php
																	echo UPDATE_REPORTS;
																	?></button>


																	<input type="button" onclick="makeAjaxCall(1);"
																		class="btn btn-circle green"
																		 style="font-size:13px !important; margin-top:5px;margin-bottom:5px;"
																		value="<?php echo REPORT4_BUTTON;?>" />

																</div>
															</div>
														</div>
													</form>
													<!-- END FORM-->
												</div>
											</div>
										</div>
									</div>
						<div id="report1">
									<div id="dash_1" class="row">
						<?php
						if ($filteredCasesCount && $AllCasesCount) {
							
							?>


										<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
											<div class="dashboard-stat2 ">
												<div class="display">
													<div class="number">
														<h3 class="font-green-dark">
															<span id="new_count" data-counter="counterup"
																data-value="<?=$filteredCasesCount['new']?>"> </span>
														</h3>
														<small><?php echo NEW_CASE_DASHBOARD; ?></small>
													</div>
													<div class="icon">
														<i class="icon-pie-chart"></i>
													</div>
												</div>
												<div class="progress-info">
													<div class="progress">
														<span id="new_percnt"  style="width: <?=$percentageCasesCount['new']?>%;"
															class="progress-bar progress-bar-success green-dark"> <span
															class="sr-only"><?=$percentageCasesCount['new']?>% <?php echo PROGRESS;?></span>
														</span>
													</div>
													<div class="status">
														<div class="status-title"><?php echo PROGRESS;?></div>
														<div id="new_stats" class="status-number"><?=$percentageCasesCount['new']?>%</div>
													</div>
												</div>
											</div>
										</div>

										<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
											<div class="dashboard-stat2 ">
												<div class="display">
													<div class="number">
														<h3 class="font-yellow-lemon">
															<span data-counter="counterup"
																data-value="<?=$filteredCasesCount['reg']?>">0</span>
														</h3>
														<small><?php echo REGISTERED_CASE_DASHBOARD; ?></small>
													</div>
													<div class="icon">
														<i class="icon-pie-chart"></i>
													</div>
												</div>
												<div class="progress-info">
													<div class="progress">
														<span style="width: <?=$percentageCasesCount['reg']?>%;"
															class="progress-bar progress-bar-success yellow-lemon"> <span
															class="sr-only"><?=$percentageCasesCount['reg']?>% <?php echo PROGRESS;?></span>
														</span>
													</div>
													<div class="status">
														<div class="status-title"><?php echo PROGRESS;?></div>
														<div class="status-number"><?=$percentageCasesCount['reg']?>%</div>
													</div>
												</div>
											</div>
										</div>

										<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
											<div class="dashboard-stat2 ">
												<div class="display">
													<div class="number">
														<h3 class="font-green-sharp">
															<span data-counter="counterup"
																data-value="<?=$filteredCasesCount['dec34']?>">0</span>
														</h3>
														<small><?php echo DECISION_34_CASE_DASHBOARD; ?></small>
													</div>
													<div class="icon">
														<i class="icon-pie-chart"></i>
													</div>
												</div>
												<div class="progress-info">
													<div class="progress">
														<span style="width: <?=$percentageCasesCount['dec34']?>%;"
															class="progress-bar progress-bar-success green-sharp"> <span
															class="sr-only"><?=$percentageCasesCount['dec34']?>% <?php echo PROGRESS;?></span>
														</span>
													</div>
													<div class="status">
														<div class="status-title"><?php echo PROGRESS;?></div>
														<div class="status-number"><?=$percentageCasesCount['dec34']?>%</div>
													</div>
												</div>
											</div>
										</div>

										<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
											<div class="dashboard-stat2 ">
												<div class="display">
													<div class="number">
														<h3 class="font-blue-sharp">
															<span data-counter="counterup"
																data-value="<?=$filteredCasesCount['adv']?>">0</span>
														</h3>
														<small><?php echo PUBLICLY_ADVERTISED_CASE_DASHBOARD; ?></small>
													</div>
													<div class="icon">
														<i class="icon-pie-chart"></i>
													</div>
												</div>
												<div class="progress-info">
													<div class="progress">
														<span style="width: <?=$percentageCasesCount['adv']?>%;"
															class="progress-bar progress-bar-success blue-sharp"> <span
															class="sr-only"><?=$percentageCasesCount['adv']?>% <?php echo PROGRESS;?></span>
														</span>
													</div>
													<div class="status">
														<div class="status-title"><?php echo PROGRESS;?></div>
														<div class="status-number"><?=$percentageCasesCount['adv']?>%</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
											<div class="dashboard-stat2 ">
												<div class="display">
													<div class="number">
														<h3 class="font-purple-soft">
															<span data-counter="counterup"
																data-value="<?=$filteredCasesCount['dec46']?>">0</span>
														</h3>
														<small><?php echo DECISION_46_CASE_DASHBOARD; ?></small>
													</div>
													<div class="icon">
														<i class="icon-pie-chart"></i>
													</div>
												</div>
												<div class="progress-info">
													<div class="progress">
														<span style="width: <?=$percentageCasesCount['dec46']?>%;"
															class="progress-bar progress-bar-success purple-soft"> <span
															class="sr-only"><?=$percentageCasesCount['dec46']?>% <?php echo PROGRESS;?></span>
														</span>
													</div>
													<div class="status">
														<div class="status-title"><?php echo PROGRESS;?></div>
														<div class="status-number"><?=$percentageCasesCount['dec46']?>%</div>
													</div>
												</div>
											</div>
										</div>

										<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
											<div class="dashboard-stat2 ">
												<div class="display">
													<div class="number">
														<h3 class="font-red-haze">
															<span data-counter="counterup"
																data-value="<?=$filteredCasesCount['closed']?>">0</span>
														</h3>
														<small><?php echo CLOSED_CASE_DASHBOARD; ?></small>
													</div>
													<div class="icon">
														<i class="icon-pie-chart"></i>
													</div>
												</div>
												<div class="progress-info">
													<div class="progress">
														<span style="width:<?=$percentageCasesCount['closed']?>%;"
															class="progress-bar progress-bar-success red-haze"> <span
															class="sr-only"><?=$percentageCasesCount['closed']?>% <?php echo PROGRESS;?></span>
														</span>
													</div>
													<div class="status">
														<div class="status-title"><?php echo PROGRESS;?></div>
														<div class="status-number"><?=$percentageCasesCount['closed']?>%</div>
													</div>
												</div>
											</div>
										</div>
										
	<script src="<?=base_url()?>assets/global/scripts/app.min.js" type="text/javascript"></script>
	<script src="<?=base_url()?>assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
		
<?php }	?>
									</div>
					</div>
									
								
						<div id="lawyers_performance" style="display: none;">		
							
							<div class="row">
										<div id="col1" class="col-md-6 col-sm-6">
											<div class="portlet light custom-portlet">
												<div class="portlet-title">
													<div class="caption caption-md caption-custom">
														<i class="icon-bar-chart font-dark hide"></i> <span
															class="caption-subject font-green-steel bold uppercase"
															style="padding-right: 15px;">
															<?php echo REPORT2_TITLE; ?></span>
														<span class="caption-helper"></span>
													</div>
												</div>
												<div id="portletBody1" class="portlet-body">
													<div class="row number-stats margin-bottom-30">
														<div class="col-md-6 col-sm-6 col-xs-6">
															<div class="stat-left">
																<div class="stat-chart">
																	<!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->
																	<div id="sparkline_bar2"></div>
																</div>
																<div class="stat-number">
																	<div class="title"><?php echo TOTAL_CASES; ?></div>
																	<div class="title" style="float: right;"><?php echo REPORT2_CURRENT_TOTAL; ?></div>
																	<div id="lawyers_current_total" class="number"><?=$lawyers_current_total?></div>
																</div>
															</div>
														</div>
														<div class="col-md-6 col-sm-6 col-xs-6">
															<div class="stat-right">
																<div class="stat-chart">
																	<!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->
																	<div id="sparkline_bar"></div>
																</div>
																<div class="stat-number">
																	<div class="title"><?php echo TOTAL_CASES; ?></div>
																	<div class="title" style="float: right;"><?php echo REPORT2_LATE_TOTAL; ?></div>
																	<div id="lawyers_late_total" class="number"><?=$lawyers_late_total?></div>
																</div>
															</div>
														</div>
													</div>
													<div id="lawyers_table_1" style="overflow-x: auto !important;" class="rep2-custom-class">
														<table class="table table-hover table-light table-custom">
															<thead >
																<tr class="uppercase">
																	<th class="custom-table-header"><?php echo REPORT2_LAWYER; ?></th>
																	<th class="custom-table-header"><?php echo REPORT2_CURRENT; ?></th>
																	<th class="custom-table-header"><?php echo REPORT2_LATE; ?></th>
																	<th class="custom-table-header"><?php echo REPORT2_CLOSED; ?></th>
																	<th class="custom-table-header"><?php echo REPORT2_RATE; ?></th>
																</tr>
															</thead>
															
													
													<?php
								if (count ( $lawyers_performance ) > 0) {
									for($i = 0; $i < count ( $lawyers_performance ); $i ++) {
										$lawyer_name = $lawyers_performance [$i] ['first_name'] . ' ' . $lawyers_performance [$i] ['last_name'];
										$current_cases_count = $lawyers_performance [$i] ['current_cases_count'];
										$late_cases_count = $lawyers_performance [$i] ['late_cases_count'];
										$closed_cases_count = $lawyers_performance [$i] ['closed_cases_count'];
										$rate = $lawyers_performance [$i] ['rate'];
										?>
			
														<tr>

																<td class="custom-table-cell"><a href="javascript:;" class="primary-link"><?=$lawyer_name?></a>
																</td>
																<td><?=$current_cases_count?></td>
																<td><?=$late_cases_count?></td>
																<td><?=$closed_cases_count?></td>
																<td><span class="bold theme-font"><?=$rate?>%</span></td>
															</tr>
<?php } }	?>

													</table>
													</div>
												</div>
											</div>
										</div>

										<div id="col2" class="col-md-6 col-sm-6">
											<div class="portlet light custom-portlet">
												<div class="portlet-title">
													<div class="caption caption-md caption-custom">
														<i class="icon-bar-chart font-dark hide"></i> <span
															class="caption-subject font-green-steel bold uppercase"
															style="padding-right: 15px;" ><?php echo REPORT3_TITLE; ?></span>
														<span class="caption-helper"></span>
													</div>
												</div>
												<div id="portletBody2" class="portlet-body">
													<div class="row number-stats margin-bottom-30">
														<div class="col-md-6 col-sm-6 col-xs-6">
															<div class="stat-left">
																<div class="stat-chart">
																	<!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->
																	<div id="sparkline_bar5"></div>
																</div>
																<div class="stat-number">
																	<div class="title"><?php echo REPORT3_PAID_TOTAL; ?></div>
																	<div id="lawyers_paid_total" class="number"><?=$lawyers_paid_total?></div>
																</div>
															</div>
														</div>
														<div class="col-md-6 col-sm-6 col-xs-6">
															<div class="stat-right">
																<div class="stat-chart">
																	<!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->
																	<div id="sparkline_bar6"></div>
																</div>
																<div class="stat-number">
																	<div class="title"><?php echo REPORT3_UNPAID_TOTAL; ?></div>
																	<div id="lawyers_unpaid_total" class="number"><?=$lawyers_unpaid_total?></div>
																</div>
															</div>
														</div>
													</div>
													<div id="lawyers_table_2" style="overflow-x: auto !important;" class="rep2-custom-class">
														<table class="table table-hover table-light table-custom">
															<thead>
																<tr class="uppercase">
																	<th><?php echo REPORT3_LAWYER; ?></th>
																	<th><?php echo REPORT3_PAID; ?></th>
																	<th><?php echo REPORT3_UNPAID; ?></th>
																	<th><?php echo REPORT3_RATE; ?></th>
																</tr>
															</thead>
													
													<?php
								if (count ( $lawyers_performance ) > 0) {
									for($i = 0; $i < count ( $lawyers_performance ); $i ++) {
										$lawyer_name = $lawyers_performance [$i] ['first_name'] . ' ' . $lawyers_performance [$i] ['last_name'];
										$paid = $lawyers_performance [$i] ['paid'];
										$unpaid = $lawyers_performance [$i] ['unpaid'];
										$total = $paid + $unpaid;
										$rate = 0;
										if ($total > 0) {
											$rate = round ( ($paid / $total) * 100 );
										}
										?>
			
														<tr>

																<td class="custom-table-cell"><a href="javascript:;" class="primary-link"><?=$lawyer_name?></a>
																</td>
																<td><?=$paid?></td>
																<td><?=$unpaid?></td>
																<td><span class="bold theme-font"><?=$rate?>%</span></td>
															</tr>
<?php } }	?>

													</table>
													</div>
												</div>
											</div>
										</div>


									</div>
					</div>			
								
                               <div id="report4Div" class="row"
										style="display: none; visibility: none;">
										<div class="col-lg-12 col-xs-12 col-sm-12">
											<div class="portlet light">
												<div class="portlet-title">
													<div class="caption caption-md">
														<span class="caption-subject bold uppercase font-dark"><?php echo REPORT4_TITLE; ?></span>
														<span class="caption-helper"><?php echo REPORT4_SMALL_TITLE; ?></span>
													</div>
													<div class="actions">
														<a
															class="btn btn-circle btn-icon-only btn-default fullscreen"
															href="#"> </a>
													</div>
												</div>
												<div class="portlet-body" >
													<div class="container-outer">
													   <div class="container-inner">

													   		<div id="dashboard_amchart_report4"
																class="CSSAnimationChart"></div>
																
															<div id="legenddiv"></div>

													   </div>
													</div>

												</div>
											</div>
										</div>
									</div>

							<div id="activities">
									<!-- START ACTIVITIES -->
								<?php if ($dashboard_activities) {?>
								<div class="row">
										<div class="col-md-12">
											<div class="portlet light portlet-fit ">
												<div class="portlet-title">
													<div class="caption">
														<span class="caption-subject font-dark bold uppercase"><?php echo DASHBOARD_ACTIVITIES; ?></span>
													</div>

												</div>

												<div class="portlet-body">
													<div>
													   <div>

															<div 
																data-always-visible="1" data-rail-visible="0">
																<ul class="feeds">
                                                                    <?php
									if ($dashboard_activities) {
										for($i = 0; $i < count ( $dashboard_activities ); $i ++) {
											if (isset($dashboard_activities[$i]["message"])){
											?>
											
                                                                        <li>
																		<div class="col1">
																			<div class="cont">
																				<div class="cont-col1">
																					<div class="label label-sm label-success">
																						<i class="fa fa-bell-o"></i>
																					</div>
																				</div>
																				<div class="cont-col2">
																					<div class="desc"> <?=$dashboard_activities[$i]["message"];?>
                                                                                        </div>
																				</div>
																			</div>
																		</div>
																		<div class="col2">
																			<div class="date"> <?=$dashboard_activities[$i]["date_time_diff"];?> </div>
																		</div>
																	</li>
                                                                        <?php
											}
										}
									}
									?>
                                                                        
                                                                    </ul>
															</div>
														</div>

													</div>
												</div>

									
                            					<div id="paginationForm" class="row">
                            							<div class="col-md-12">
                            								<div class="portlet">
                            									<div class="portlet-body form">
                            										<!-- BEGIN FORM-->
                            										<form id="myForm2" role="form" method="post"
                            											class="form-horizontal">
                            											<div class="form-body">
                            												<div>
                            													<div class="row">
                            														<div style="text-align: center; margin: auto;">
                            															<input type="button" class="btn btn-circle green"
                            																onClick="javascript:prev_page()" <?php if(!$has_prev){?>
                            																disabled <?php }?> value="<?php echo PREVIOUS ;?>" /> <span
                            																style="margin-left: 5px; margin-right: 5px; text-align: center;"><b><?php echo PAGE_NUM. ": ".$current_page.' / '.$total_pages_num; ?></b></span>
                            
                            															<input type="button" class="btn btn-circle green"
                            																onClick="javascript:next_page()" <?php if(!$has_next){?>
                            																disabled <?php }?> value="<?php echo NEXT ;?>" />
                            
                            														</div>
                            													</div>
                            												</div>
                            											</div>
                            										</form>
                            									</div>
                            								</div>
                            							</div>
                            						</div>
						
											</div>
										</div>

									</div>
				<?php }?>
								<!-- END ACTIVITIES -->
						</div>		
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
        <?php $this->load->view('utils/footer');?>
	<!--[if lt IE 9]>
<script src="<?=base_url()?>assets/global/plugins/respond.min.js"></script>
<script src="<?=base_url()?>assets/global/plugins/excanvas.min.js"></script> 
<script src="<?=base_url()?>assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
	<!-- BEGIN CORE PLUGINS -->
	<script src="<?=base_url()?>assets/global/plugins/jquery.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="<?=base_url()?>assets/global/plugins/moment.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/morris/morris.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/morris/raphael-min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/counterup/jquery.waypoints.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/counterup/jquery.counterup.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/fullcalendar/fullcalendar.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.resize.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.categories.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.sparkline.min.js"
		type="text/javascript"></script>
	
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="<?=base_url()?>assets/global/scripts/app.min.js"
		type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?=base_url()?>assets/pages/scripts/dashboard.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script
		src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js"
		type="text/javascript"></script>
	<!-- END THEME LAYOUT SCRIPTS -->

	<script
		src="<?=base_url()?>assets/global/plugins/amcharts/amcharts/amcharts.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/amcharts/amcharts/serial.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/amcharts/amcharts/pie.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/amcharts/amcharts/radar.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/amcharts/amcharts/themes/light.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/amcharts/amcharts/themes/patterns.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/amcharts/amcharts/themes/chalk.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/amcharts/ammap/ammap.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/amcharts/ammap/maps/js/worldLow.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/amcharts/amstockcharts/amstock.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/fullcalendar/fullcalendar.min.js"
		type="text/javascript"></script>


</body>

</html>