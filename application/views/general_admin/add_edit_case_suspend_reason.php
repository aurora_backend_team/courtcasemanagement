<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<?php
$this->load->view ( 'localization/lang' );
$this->load->view ( 'localization/txt' );
defineLocale ();
defineStrings ();
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<script type="text/javascript">
function checkValid() {
	var suspend_code = document.forms["myForm"]["suspend_code"].value;
	var suspend_reason = document.forms["myForm"]["suspend_reason"].value;
	if (suspend_code == "") {
		$("#suspend_code_tooltip").css("display","block");
		$("#suspend_code_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#suspend_code_tooltip").css("display","none");
		$("#suspend_code_tooltip").css("visibility","none");
	}

	if (suspend_reason == "") {
		$("#suspend_reason_tooltip").css("display","block");
		$("#suspend_reason_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#suspend_reason_tooltip").css("display","none");
		$("#suspend_reason_tooltip").css("visibility","none");
	}
}
</script>
<meta charset="utf-8" />
<title><?php echo TITLE; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin RTL Theme #3 for dashboard & statistics"
	name="description" />
<meta content="" name="author" />

<link rel="shortcut icon"
	href="<?=base_url()?>assets/layouts/layout3/img/logo-default.jpg" />
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
</head>
<!-- END HEAD -->
<body class="page-container-bg-solid">
	<div class="page-wrapper">
<?php include 'general_admin_header.php';?>
<br>
				<div class="page-wrapper-row full-height">
			<!-- BEGIN CONTAINER -->
			<div class="page-container">
				<!-- BEGIN CONTENT -->
				<div class="page-content-wrapper">
					<!-- BEGIN CONTENT BODY -->
					<!-- BEGIN PAGE HEAD-->
					<div class="page-head">
						<div class="col-md-10">
							<div class="portlet box green">
								<div class="portlet-title">
									<div class="caption">
										<i class="fa fa-gift"></i>
												<?php
												if ($suspend_code) {
													echo SUSPEND_REASON;
												} else {
													echo ADD_RECORD;
												}
												?>
											</div>

								</div>
								<div class="portlet-body form">
									<!-- BEGIN FORM-->
									<form id="myForm" role="form" method="post"
										onsubmit="javascript:return checkValid();"
										action="<?=site_url('case_suspend_reasons/add_edit_case_suspend_reason/'.$suspend_code)?>"
										class="form-horizontal">
										<div class="form-body">
										
											<div class="form-group">
												<label class="col-md-3 control-label"><?php echo SUSPEND_REASON; ?> <span
													class="required"> * </span></label>
												<div class="col-md-4">
													<input type="text" class="form-control " name="suspend_reason"
														id="suspend_reason" value="<?=$suspend_reason?>"
														onfocus="javascript:removeTooltip('suspend_reason_tooltip');"> <span
														id="suspend_reason_tooltip" class="tooltiptext"
														style="display: none; color: red;"><?php echo FILL_THIS_FIELD; ?></span>
												</div>
											</div>
										</div>
										<div class="form-actions">
											<div class="row">
												<div class="col-md-offset-3 col-md-9">
													<button type="submit" class="btn btn-circle green"><?php
													if ($suspend_code) {
														echo EDIT;
													} else {
														echo INSERT;
													}
													?></button>
													<a
														href="<?=site_url('case_suspend_reasons/view_case_suspend_reasons')?>"
														class="btn btn-outline btn-circle grey-salsa"><?php echo CANCEL; ?>
													</a>
												</div>
											</div>
										</div>

									</form>
									<!-- END FORM-->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
        <?php $this->load->view('utils/footer');?>
        
	<!-- BEGIN CORE PLUGINS -->
	<script src="<?=base_url()?>assets/global/plugins/jquery.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.sparkline.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="<?=base_url()?>assets/global/scripts/app.min.js"
		type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?=base_url()?>assets/pages/scripts/profile.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script
		src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js"
		type="text/javascript"></script>
	<!-- END THEME LAYOUT SCRIPTS -->

</body>

</html>