<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
    $("#delBtn").click(function(){
        $("#delNotify").removeClass('hidden');
		document.getElementById('delNotify').style.display = 'block';
		document.getElementById('delNotify').style.visibility = 'visible';
    });
    $("#cancel").click(function(){
        $("#delNotify").addClass('hidden');
    });
});

function delBtnClick(code, type) {
        document.getElementById('code_to_delete').value = code;
        document.getElementById('type_to_delete').value = type;
	$("#delNotify").removeClass('hidden');
	document.getElementById('delNotify').style.display = 'block';
	document.getElementById('delNotify').style.visibility = 'visible';
}

function applyBusiness(){
	var code_value = document.getElementById('code_to_delete').value;
    var type_value = document.getElementById('type_to_delete').value;
	 $("#loading").removeClass('hidden');
if (type_value == "packages" ) {
  window.location =  "<?php echo site_url();?>admin_panel/delete_package/"+code_value;
}else if (type_value == "suspend_reasons" ) {
  window.location =  "<?php echo site_url();?>case_suspend_reasons/delete_case_suspend_reason/"+code_value;
} else if (type_value == "lawyer_offices" ) {
  window.location =  "<?php echo site_url();?>admin_panel/delete_lawyer_office/"+code_value;
} else if (type_value == "cities" ) {
  window.location =  "<?php echo site_url();?>admin_panel/delete_city/"+code_value;
} else if (type_value == "customers" ) {
  window.location =  "<?php echo site_url();?>lawyer_office_panel/delete_customer/"+code_value;
} else if (type_value == "lawyers" ) {
  window.location =  "<?php echo site_url();?>lawyer_office_lawyers/delete_lawyer/"+code_value;
} else if (type_value == "secretaries" ) {
  window.location =  "<?php echo site_url();?>lawyer_office_secretaries/delete_secretary/"+code_value;
} else if (type_value == "customer_types" ) {
  window.location =  "<?php echo site_url();?>lawyer_office_customer_types/delete_customer_type/"+code_value;
} else if (type_value == "regions" ) {
  window.location =  "<?php echo site_url();?>lawyer_office_regions/delete_region/"+code_value;
} else if (type_value == "costs_per_stage_per_region" ) {
  window.location =  "<?php echo site_url();?>lawyer_office_cost_per_stage_region/delete_cost_per_stage_per_region/"+code_value;
}
 
}
</script>

</head>

<!-- END HEAD -->

<body class="page-container-bg-solid">
	<div id="delNotify" class="alert alert-block alert-info fade in hidden">
		<h4 class="alert-heading"><?php echo INFO; ?></h4>
		<p><?php
		 echo WANT_DELETE_RECORD;
		  ?></p>
		<p>
<input type="hidden" id="code_to_delete" value="" >
<input type="hidden" id="type_to_delete" value="" >
			<a class="btn purple" href="javascript:applyBusiness();"> <?php echo OK; ?> </a> <a
				id="cancel" class="btn dark" href="javascript:;"> <?php echo CANCEL; ?></a>
		</p>
	</div>
</body>
</html>