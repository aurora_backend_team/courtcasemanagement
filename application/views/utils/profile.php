<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemesnner
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<?php
session_start ();
$this->load->view ( 'localization/lang' );
$this->load->view ( 'localization/txt' );
defineLocale ();
defineStrings ();
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>

<script type="text/javascript">
    function checkBasicInfoValid(email_valdation) {
    	var user_name = document.forms["basic_info_form"]["user_name"].value;
    	var first_name = document.forms["basic_info_form"]["first_name"].value;
    	var last_name = document.forms["basic_info_form"]["last_name"].value;
    	var email = document.forms["basic_info_form"]["email"].value;
    	var mobile_number = document.forms["basic_info_form"]["mobile_number"].value;
    	var mobile_pattern = /05/g;
    	
    	if (user_name == "") {
    		$("#user_name_tooltip").css("display","block");
    		$("#user_name_tooltip").css("visibility","visible");
    		return false;
    	} else{
    		$("#user_name_tooltip").css("display","none");
    		$("#user_name_tooltip").css("visibility","none");
    	}
    	if (first_name == "") {
    		$("#first_name_tooltip").css("display","block");
    		$("#first_name_tooltip").css("visibility","visible");
    		return false;
    	} else{
    		$("#first_name_tooltip").css("display","none");
    		$("#first_name_tooltip").css("visibility","none");
    	}
    	if (last_name == "") {
    		$("#last_name_tooltip").css("display","block");
    		$("#last_name_tooltip").css("visibility","visible");
    		return false;
    	} else{
    		$("#last_name_tooltip").css("display","none");
    		$("#last_name_tooltip").css("visibility","none");
    	}
    	if ((mobile_number != "" && (mobile_number.length < 10 || mobile_number.length > 10 ||
    	        !mobile_pattern.test(mobile_number))) || mobile_number == "" ) {
    		$("#mobile_number_tooltip").css("display","block");
    		$("#mobile_number_tooltip").css("visibility","visible");
    		return false;
    	} else{
    		$("#mobile_number_tooltip").css("display","none");
    		$("#mobile_number_tooltip").css("visibility","none");
    	}
    	if ((email != "" && !validateEmail(email)) || email == "") {
    		$("#email_tooltip").text(email_valdation);
    		$("#email_tooltip").css("display","block");
    		$("#email_tooltip").css("visibility","visible");
    		
    		return false;
    	}
    }

	function checkCustomerInfoValid() {

		var subscription_start_date = document.forms["detailsForm"]["subscription_start_date"].value;
		var subscription_end_date = document.forms["detailsForm"]["subscription_end_date"].value;
		var phone_number = document.forms["detailsForm"]["phone_number"].value;
		var number_of_cases_per_month = document.forms["detailsForm"]["number_of_cases_per_month"].value;

		if (number_of_cases_per_month  == "-" || number_of_cases_per_month.length > 7) {
			$("#number_of_cases_per_month_tooltip").css("display","block");
			$("#number_of_cases_per_month_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#number_of_cases_per_month_tooltip").css("display","none");
			$("#number_of_cases_per_month_tooltip").css("visibility","none");
		}
		if (phone_number != "" && (phone_number.length < 10 || phone_number.length > 10)) {
			$("#phone_number_tooltip").css("display","block");
			$("#phone_number_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#phone_number_tooltip").css("display","none");
			$("#phone_number_tooltip").css("visibility","none");
		}

		if (subscription_end_date != "" && subscription_start_date != "" && subscription_end_date < subscription_start_date) {
			$("#end_date_tooltip").css("display","block");
			$("#end_date_tooltip").css("visibility","visible");
			
			return false;
		}else{
			$("#end_date_tooltip").css("display","none");
			$("#end_date_tooltip").css("visibility","none");
		}
		
	}
	
    function checkOfficeInfoValid(email_valdation) {
    	  var package = document.forms["office_form"]["package"].value;
    		var office_name = document.forms["office_form"]["office_name"].value;
    		var address = document.forms["office_form"]["address"].value;
    		var mobile_number = document.forms["office_form"]["office_mobile_number"].value;
    		var office_email = document.forms["office_form"]["office_email"].value;
    		var contact_person_name = document.forms["office_form"]["contact_person_name"].value;
    		var subscription_start_date = document.forms["office_form"]["subscription_start_date"].value;
    		var subscription_end_date = document.forms["office_form"]["subscription_end_date"].value;
    		var office_phone_number = document.forms["office_form"]["office_phone_number"].value;
    		var mobile_pattern = /05/g;

    		if (subscription_end_date != "" && subscription_start_date != "" && subscription_end_date < subscription_start_date) {
    			$("#end_date_tooltip").css("display","block");
    			$("#end_date_tooltip").css("visibility","visible");
    			
    			return false;
    		}else{
    			$("#end_date_tooltip").css("display","none");
    			$("#end_date_tooltip").css("visibility","none");
    		}
    		if (package == "") {
    			$("#package_tooltip").css("display","block");
    			$("#package_tooltip").css("visibility","visible");
    			return false;
    		} else{
    			$("#package_tooltip").css("display","none");
    			$("#package_tooltip").css("visibility","none");
    		}
    		if (office_name == "") {
    			$("#office_name_tooltip").css("display","block");
    			$("#office_name_tooltip").css("visibility","visible");
    			return false;
    		} else{
    			$("#office_name_tooltip").css("display","none");
    			$("#office_name_tooltip").css("visibility","none");
    		}
    		if (address == "") {
    			$("#address_tooltip").css("display","block");
    			$("#address_tooltip").css("visibility","visible");
    			return false;
    		} else{
    			$("#address_tooltip").css("display","none");
    			$("#address_tooltip").css("visibility","none");
    		}
    		if (office_phone_number != "" && (office_phone_number.length < 10 || office_phone_number.length > 10)) {
    			$("#office_phone_number_tooltip").css("display","block");
    			$("#office_phone_number_tooltip").css("visibility","visible");
    			return false;
    		} else{
    			$("#office_phone_number_tooltip").css("display","none");
    			$("#office_phone_number_tooltip").css("visibility","none");
    		}
    		if (mobile_number != "" && (mobile_number.length < 10 || mobile_number.length > 10 ||
        	        !mobile_pattern.test(mobile_number))) {
    			$("#office_mobile_number_tooltip").css("display","block");
    			$("#office_mobile_number_tooltip").css("visibility","visible");
    			return false;
    		} else{
    			$("#office_mobile_number_tooltip").css("display","none");
    			$("#office_mobile_number_tooltip").css("visibility","none");
    		}
    		if (office_email == "") {
    			$("#office_email_tooltip").css("display","block");
    			$("#office_email_tooltip").css("visibility","visible");
    			return false;
    		} else if (!validateEmail(office_email)) {
    			$("#office_email_tooltip").text(email_valdation);
    			$("#office_email_tooltip").css("display","block");
    			$("#office_email_tooltip").css("visibility","visible");
    			
    			return false;
    		}else{
    			$("#office_email_tooltip").css("display","none");
    			$("#office_email_tooltip").css("visibility","none");
    		}
    		if (contact_person_name == "") {
    			$("#contact_person_name_tooltip").css("display","block");
    			$("#contact_person_name_tooltip").css("visibility","visible");
    			return false;
    		} else{
    			$("#contact_person_name_tooltip").css("display","none");
    			$("#contact_person_name_tooltip").css("visibility","none");
    		}
    }
    function checkLawyerInfoValid()
	{
    	var manager = document.forms["detailsForm"]["manager"].value;
    	var stage1_cost = document.forms["detailsForm"]["stage1_cost"].value;
    	var stage2_cost = document.forms["detailsForm"]["stage2_cost"].value;
    	var stage3_cost = document.forms["detailsForm"]["stage3_cost"].value;
    	var stage4_cost = document.forms["detailsForm"]["stage4_cost"].value;
    	var stage5_cost = document.forms["detailsForm"]["stage5_cost"].value;
    	
    	if (manager == "") {
    		$("#manager_tooltip").css("display","block");
    		$("#manager_tooltip").css("visibility","visible");
    		return false;
    	} else{
    		$("#manager_tooltip").css("display","none");
    		$("#manager_tooltip").css("visibility","none");
    	}
    	if (stage1_cost == "" || stage1_cost.length > 7) {
    		$("#stage1_cost_tooltip").css("display","block");
    		$("#stage1_cost_tooltip").css("visibility","visible");
    		return false;
    	} else{
    		$("#stage1_cost_tooltip").css("display","none");
    		$("#stage1_cost_tooltip").css("visibility","none");
    	}
    	if (stage2_cost == "" || stage2_cost.length > 7) {
    		$("#stage2_cost_tooltip").css("display","block");
    		$("#stage2_cost_tooltip").css("visibility","visible");
    		return false;
    	} else{
    		$("#stage2_cost_tooltip").css("display","none");
    		$("#stage2_cost_tooltip").css("visibility","none");
    	}
    	if (stage3_cost == "" || stage3_cost.length > 7) {
    		$("#stage3_cost_tooltip").css("display","block");
    		$("#stage3_cost_tooltip").css("visibility","visible");
    		return false;
    	} else{
    		$("#stage3_cost_tooltip").css("display","none");
    		$("#stage3_cost_tooltip").css("visibility","none");
    	}
    	if (stage4_cost == "" || stage4_cost.length > 7) {
    		$("#stage4_cost_tooltip").css("display","block");
    		$("#stage4_cost_tooltip").css("visibility","visible");
    		return false;
    	} else{
    		$("#stage4_cost_tooltip").css("display","none");
    		$("#stage4_cost_tooltip").css("visibility","none");
    	}
    	if (stage5_cost == "" || stage5_cost.length > 7) {
    		$("#stage5_cost_tooltip").css("display","block");
    		$("#stage5_cost_tooltip").css("visibility","visible");
    		return false;
    	} else{
    		$("#stage5_cost_tooltip").css("display","none");
    		$("#stage5_cost_tooltip").css("visibility","none");
    	}
    	
	}
	function validateEmail(email) 
	{
		//var re = /\S+@\S+\.\S+/;
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}
	
	
</script>

<meta charset="utf-8" />
<title><?php echo TITLE; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin RTL Theme #3 for user account page"
	name="description" />
<meta content="" name="author" />

<link rel="shortcut icon"
	href="<?=base_url()?>assets/layouts/layout3/img/logo-default.jpg" />
	
<style type="text/css">
	.tabbable-line > .nav-tabs > li.active {
		box-shadow: 0px -4px 0px #c1a73c inset;
		border-bottom: none !important;
	}
    .tabbable-line > .nav-tabs > li.open, .tabbable-line > .nav-tabs > li:hover {
      	background: none;
		box-shadow: 0px -4px 0px #c1a73c inset;
		border-bottom: none !important;
     }
	.caption-custom{
		width:75% !important;
	}
	
	@media only screen and (max-width: 991px) {
		.custom-main-portlet{
			margin-top:20px !important;
		}
	}
</style>

</head>
<!-- END HEAD -->

<body class="page-container-bg-solid">
	<div class="page-wrapper">
        <?php if (isset($office_mobile_number)){?>
           <?php $this->load->view('office_admin/office_admin_header');?>
        <?php }else{ ?>
           <?php $this->load->view('secretary_lawyer/secretary_header');?>
        <?}?>
                 
            <div class="page-wrapper-row full-height">
			<div class="page-wrapper-middle">
				<!-- BEGIN CONTAINER -->
				<div class="page-container">
					<!-- BEGIN CONTENT -->
					<div class="page-content-wrapper">
						<!-- BEGIN CONTENT BODY -->
						<!-- BEGIN PAGE HEAD-->
						<div class="page-head">
							<div class="container">
								<!-- BEGIN PAGE TITLE -->
								<div class="page-title" style="width: 100% !important;">
									<h1><?php echo PROFILE_ACCOUNT; ?>
									 </h1>
                                        <?php if($user_type_code == 'ADMIN' || $user_type_code == 'CUSTOMER') {?>
                                        <br>
                                        <form id="bannerForm"
										method="post"
										action="<?=site_url('user_profile/change_banner/'.$user_id)?>"
										role="form" method='post' enctype="multipart/form-data">
										<div class="form-group">
											<div>
												<style>
												.banner-container{
													width: 100%; height: 130px; position: relative;
												}
												.banner-container:hover .banner-button{display:block}
												.banner-button{
													position: absolute; top: 5px; left: 0px; display:none;  z-index:100;
												}
												</style>
												<div class="banner-container">
														<?php if(isset($banner) && $banner != '') {?>
														<img id="img_banner" src="<?=base_url()?><?=$banner?>"
														alt="" class="img-responsive pic-bordered"
														style="width: 100%; height: 130px;" />
                                                        <?} else {?>	
														<img id="img_banner"
														src="https://trello-attachments.s3.amazonaws.com/5887c2f41df012f67b1e0922/593927d280e050e3600de117/72fc6dc9078438533bf0bbe256ad56fe/tanfiz2.png"
														alt="" class="img-responsive pic-bordered"
														style="width: 100%; height: 130px;" />
														<?}?>
														<input id="upload_banner" name="banner" type="file"
														style="display: none;" onchange="javascript:readBanner(this);" /> 
														 <div style="position: absolute; top: 5px; left: 0px;" class="banner-button">
       														 <a href="javascript:uploadBanner();" target="_top" class="profile-edit" title='<?php echo EDIT; ?>'>
       														 	<img src="https://trello-attachments.s3.amazonaws.com/59453c74121a14ef1be1ea75/595e034cc4627a427bbe8582/bcb50c882e705e988452e00cb782aec0/button.png" style="width: 150px; height: 40px;" />
       														 </a>
    													</div>											
													<div id="banner_buttons" class="hidden">
														<a href="javascript:saveBanner();" class="btn green"> <?php echo SAVE; ?></a>
														<?php if(isset($banner) && $banner != '') {?>
															<a href="javascript:cancelBannerChange('<?=base_url()?><?=$banner?>');"	class="btn default"> <?php echo CANCEL; ?></a>
														<?} else {?>	
															<a href="javascript:cancelBannerChange('https://trello-attachments.s3.amazonaws.com/5887c2f41df012f67b1e0922/593927d280e050e3600de117/72fc6dc9078438533bf0bbe256ad56fe/tanfiz2.png');"	class="btn default"> <?php echo CANCEL; ?></a>
														<?}?>														
													</div>
												</div>
											</div>
										</div>
									</form>
                                        <?}?>
                                    </div>
								<!-- END PAGE TITLE -->
								<!-- BEGIN PAGE TOOLBAR -->

								<!-- END PAGE TOOLBAR -->
							</div>
						</div>
						<!-- END PAGE HEAD-->
						<!-- BEGIN PAGE CONTENT BODY -->
						<div class="page-content">
							<div class="container">
								<div></div>
								<!-- BEGIN PAGE CONTENT INNER -->
								<div class="page-content-inner">
									<div class="row">
										<div class="col-md-12">
											<!-- BEGIN PROFILE SIDEBAR -->
											<div class="profile-sidebar">
												<!-- PORTLET MAIN -->
												<div class="portlet light profile-sidebar-portlet custom-main-portlet">
													<!-- SIDEBAR USERPIC -->
													<div class="profile-userpic">
															<?php if(isset($profile_img) && $profile_img != '') {?>
															<img src="<?=base_url()?><?=$profile_img?>"
															class="img-responsive" alt="">
															<?} else {?>	
															<img
															src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image"
															class="img-responsive" alt="">
															<?}?>
                                                             </div>
													<!-- END SIDEBAR USERPIC -->
													<!-- SIDEBAR USER TITLE -->
													<div class="profile-usertitle">
														<div class="profile-usertitle-name"> <?= $first_name.' '.$last_name ; ?> </div>
														<div class="profile-usertitle-job">  <?php
														if ($user_type_code == "ADMIN") {
															echo OFFICE_ADMIN;
														} else if ($user_type_code == "CUSTOMER") {
															echo CUSTOMER;
														} else if ($user_type_code == "LAWYER") {
															echo LAWYER;
														} else if ($user_type_code == "SECRETARY") {
															echo SECRETARY;
														}
														?> </div>
													</div>
													<!-- END SIDEBAR USER TITLE -->


												</div>
												<!-- END PORTLET MAIN -->

												<!-- PORTLET MAIN -->
                                                    <?php  if ($user_type_code == "ADMIN"){?>
                                                    <div
													class="portlet light ">
													<!-- STAT -->
													<div class="row list-separated profile-stat">
														<div class="col-md-4 col-sm-4 col-xs-6">
															<div class="uppercase profile-stat-title"> <?php echo $cases_count; ?> </div>
															<div class="uppercase profile-stat-text"> <?php echo CASES ; ?> </div>
														</div>
														<div class="col-md-4 col-sm-4 col-xs-6">
															<div class="uppercase profile-stat-title"> <?= $lawyers_count ; ?> </div>
															<div class="uppercase profile-stat-text"> <?php echo LAWYER ; ?> </div>
														</div>
														<div class="col-md-4 col-sm-4 col-xs-6">
															<div class="uppercase profile-stat-title"> <?= $customers_count ; ?>  </div>
															<div class="uppercase profile-stat-text"> <?php echo CUSTOMERS_COUNT ; ?> </div>
														</div>

													</div>
													<!-- END STAT -->
													<div>
														<h4 class="profile-desc-title"><?php echo ABOUT_OFFICE; ?></h4>
														<span class="profile-desc-text"><?php echo STATIC_ABOUT; ?> </span>
													</div>
												</div>
                                                    <?php }?>
                                                    <!-- END PORTLET MAIN -->
											</div>
											<!-- END BEGIN PROFILE SIDEBAR -->
											<!-- BEGIN PROFILE CONTENT -->
											<div class="profile-content">
												<div class="row">
													<div class="col-md-12">
														<div class="portlet light ">
															<div class="portlet-title tabbable-line">
																<div class="caption caption-md caption-custom">
																	<i class="icon-globe theme-font hide"></i> <span
																		class="caption-subject font-blue-madison bold uppercase"><?php echo PROFILE_ACCOUNT; ?></span>
																</div>
																<ul class="nav nav-tabs">
																	<li class="active"><a href="#tab_1_1" data-toggle="tab"> <?php echo BASIC_INFO; ?> </a>
																	</li>
																	<li><a href="#tab_1_2" data-toggle="tab"> <?php echo CHANGE_PROFILE_PIC; ?> </a>
																	</li>
																	<li><a href="#tab_1_3" data-toggle="tab"> <?php echo CHANGE_PASSWORD; ?> </a>
																	</li>
																	<?php if ($user_type_code != "SECRETARY") { ?>
																	<li><a href="#tab_1_4" data-toggle="tab"> <?php
																		if ($user_type_code == "ADMIN") {
																			echo OFFICE_INFO;
																		} else {
																			echo MORE_INFO;
																		}
																		?> </a></li>
																	<?php }?>
																	</a></li>
																</ul>
															</div>
															<div class="portlet-body">
																<div class="tab-content">
																	<!-- PERSONAL INFO TAB -->
																	<div class="tab-pane active" id="tab_1_1">
																		<form id="basic_info_form" class="form-horizontal"
																			role="form" method='post'
																			action="<?=site_url('user_profile/view_edit_general_profile/'.$user_id)?>"
																			onsubmit="javascript:return checkBasicInfoValid('<?php echo INVALID_EMAIL; ?>');">
																			<div class="form-body">
																				<div class="form-group">
																					<label class="col-md-3 control-label"> <?php echo USERNAME; ?> <span
																						class="required"> * </span></label>
																					<div class="col-md-6">
																						<input class="form-control "
																							name="user_name" id="user_name"
																							value="<?php echo $user_name?>"
																							onfocus="javascript:removeTooltip('user_name_tooltip');">
																						<span id="user_name_tooltip" class="tooltiptext"
																							<?php if (isset($profile_username_exists) && $profile_username_exists == 1){?>
																							style="display: block; color: red;"
																							<?php }else {?> style="display: none; color:red;"
																							<?php }?>><?php
																							
																							if (isset ( $profile_username_exists ) && $profile_username_exists == 1) {
																								echo USERNAME_IS_EXIST;
																							} else {
																								echo FILL_THIS_FIELD;
																							}
																							  ?>
																	</span>
																					</div>
																				</div>
																				<div class="form-group">
																					<label class="col-md-3 control-label"> <?php echo FIRST_NAME; ?> <span
																						class="required"> * </span></label>
																					<div class="col-md-6">
																						<input class="form-control "
																							name="first_name" id="first_name"
																							value="<?php echo $first_name?>"
																							onfocus="javascript:removeTooltip('first_name_tooltip');">
																						<span id="first_name_tooltip" class="tooltiptext"
																							style="display: none; color: red;"><?php echo FILL_THIS_FIELD; ?></span>
																					</div>
																				</div>
																				<div class="form-group">
																					<label class="col-md-3 ontrol-label"> <?php echo LAST_NAME; ?> <span
																						class="required"> * </span></label>
																					<div class="col-md-6">
																						<input class="form-control "
																							name="last_name" id="last_name"
																							value="<?php echo $last_name?>"
																							onfocus="javascript:removeTooltip('last_name_tooltip');">
																						<span id="last_name_tooltip" class="tooltiptext"
																							style="display: none; color: red;"><?php echo FILL_THIS_FIELD; ?></span>
																					</div>
																				</div>
																				<div class="form-group">
																					<label class="col-md-3 control-label"><?php echo MOBILE_NUMBER; ?> <span
																						class="required"> * </span></label>
																					<div class="col-md-6">
																						<input type="number"
																							onkeydown="numberKeyDown(event)"
																							class="form-control no-spinners"
																							name="mobile_number" id="mobile_number"
																							value="<?php echo $mobile_number?>"
																							onfocus="javascript:removeTooltip('mobile_number_tooltip');">
																						<span id="mobile_number_tooltip"
																							class="tooltiptext"
																							style="display: none; color: red;"><?php echo MOBILE_NUMBER_INVALID; ?></span>
																					</div>
																				</div>
																				<div class="form-group">
																					<label class="col-md-3 control-label"><?php echo EMAIL; ?> <span
																						class="required"> * </span> </label>
																					<div class="col-md-6">
																						<input type="text"
																							class="form-control " name="email"
																							value="<?php echo $email?>" id="email"
																							onfocus="javascript:removeTooltip('email_tooltip');">
																						<span id="email_tooltip" class="tooltiptext"
																							<?php if (isset($profile_email_exists) && $profile_email_exists == 1){?>
																							style="display: block; color: red;"
																							<?php }else {?> style="display: none; color:red;"
																							<?php }?>><?php
																							
																							if (isset ( $profile_email_exists ) && $profile_email_exists == 1) {
																								echo EMAIL_IS_EXIST;
																							}
																							?></span>
																							
																					</div>
																				</div>

																				<div class="margin-top-10">
																					<button type="submit" class="btn green"><?php echo SAVE ?></button>
																				</div>
																			</div>
																		</form>
																	</div>
																	<!-- END PERSONAL INFO TAB -->
																	<!-- CHANGE AVATAR TAB -->
																	<div class="tab-pane" id="tab_1_2">
																		<p> <?php echo CHANGE_AVATAR_DESCRIPTION; ?>  </p>
																		<form id="avatarForm" method="post"
																			action="<?=site_url('user_profile/change_avatar/'.$user_id)?>"
																			role="form" method='post'
																			enctype="multipart/form-data">
																			<div class="form-group">
																				<div class="fileinput fileinput-new"
																					data-provides="fileinput">
																					<div class="fileinput-new thumbnail"
																						style="width: 200px; height: 150px;">
																							<?php if(isset($profile_img) && $profile_img != '') {?>
																							<img id="img_avatar"
																							src="<?=base_url()?><?=$profile_img?>" alt="" />
                                                                                            <?} else {?>	
																							<img id="img_avatar"
																							src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image"
																							alt="" />
																							<?}?>		
																							
																							</div>
																					<div
																						class="fileinput-preview fileinput-exists thumbnail"
																						style="max-width: 200px; max-height: 150px;"></div>
																					<div>
																						<span class="btn default btn-file"> <span
																							class="fileinput-new"> <?php echo SELECT_IMAGE ; ?></span>
																							<span class="fileinput-exists"> <?php echo CHANGE_PROFILE_PIC; ?></span>
																							<input type="file" name="avatar" accept="image/*">
																						</span> <a href="javascript:;"
																							class="btn default fileinput-exists"
																							data-dismiss="fileinput"> <?php echo DELETE; ?></a>
																					</div>
																				</div>

																			</div>
																			<div class="margin-top-10">
																				<a href="javascript:changeAvatar();"
																					class="btn green"> <?php echo SAVE; ?></a> <a
																					href="javascript:cancelAvatarChange('<?=base_url()?><?=$profile_img?>');"
																					class="btn default"> <?php echo CANCEL; ?></a>
																			</div>
																		</form>
																	</div>
																	<!-- END CHANGE AVATAR TAB -->
																	<!-- CHANGE PASSWORD TAB -->
																	<div class="tab-pane" id="tab_1_3">
																		<div class="form-body">
																			<div class="form-group" id="incorrect_old_password"
																				style="display: none; color: red;">
																				<?php echo INCORRECT_OLD_PASSWORD;?>
																			</div>
																			<div class="form-group"
																				id="incorrect_repeated_password"
																				style="display: none; color: red;">
																				<?php echo INCORRECT_REPEATED_PASSWORD;?>
																			</div>
																			<form id="passwordForm" class="form-horizontal"
																				action="<?=site_url('user_profile/change_password/'.$user_id)?>"
																				role="form" method='post'
																				onsubmit="javascript:return checkPassword();">
																				<div class="form-group">
																					<label class="col-md-3 control-label"><?php echo CURRENT_PASSWORD ; ?> <span
																						class="required"> * </span> </label>
																					<div class="col-md-6">
																						<input type="password" name="old_password"
																							id="old_password"
																							class="form-control "
																							onfocus="javascript:removeTooltip('old_password_tooltip');">
																						<span id="old_password_tooltip"
																							class="tooltiptext"
																							style="display: none; color: red";><?php echo FILL_THIS_FIELD; ?><span
																							class="required"> * </span></span> <input
																							type="hidden" value="<?=$password?>"
																							id="saved_password" />
																					</div>
																				</div>
																				<div class="form-group">
																					<label class="col-md-3 control-label"><?php echo NEW_PASSWORD ; ?><span
																						class="required"> * </span></label>
																					<div class="col-md-6">
																						<input type="password" name="new_password"
																							id="new_password"
																							class="form-control "
																							onfocus="javascript:removeTooltip('new_password_tooltip');">
																						<span id="new_password_tooltip"
																							class="tooltiptext"
																							style="display: none; color: red";><?php echo FILL_THIS_FIELD; ?></span>
																					</div>
																				</div>
																				<div class="form-group">
																					<label class="col-md-3 control-label"><?php echo REPEAT_NEW_PASSWORD ; ?><span
																						class="required"> * </span></label>
																					<div class="col-md-6">
																						<input type="password" name="retyped_new_password"
																							id="retyped_new_password"
																							class="col-md-6 form-control "
																							onfocus="javascript:removeTooltip('retyped_new_password_tooltip');">
																						<span id="retyped_new_password_tooltip"
																							class="tooltiptext"
																							style="display: none; color: red";><?php echo FILL_THIS_FIELD; ?></span>
																					</div>
																				</div>
																				<div class="margin-top-10">
																					<!-- <a href="javascript:changePassword();" class="btn green"> <?php echo CHANGE_PASSWORD ; ?> </a>-->
																					<button type="submit" class="btn green"><?php echo CHANGE_PASSWORD ; ?></button>
																				</div>
																			</form>
																		</div>
																	</div>
																	<!-- END CHANGE PASSWORD TAB -->
																	<!-- OFFICE INFO TAB -->
                                                                        <?php if ($user_type_code == "ADMIN"){?>
                                                                        <div
																		class="tab-pane" id="tab_1_4">
																		<form id="office_form" class="form-horizontal"
																			role="form" method="post"
																			onsubmit="javascript:return checkOfficeInfoValid('<?php echo INVALID_EMAIL; ?>');"
																			action="<?=site_url('user_profile/edit_office_info/'.$user_id)?>">

																			<div class="form-body">
																				<div class="form-group">
																					<label class="col-md-3 control-label"><?php echo PACKAGE_CODE; ?> <span
																						class="required"> * </span></label>
																					<div class="col-md-6">
																						<select class="form-control "
																							name="package_code_1" id="package"
																							class="form-control" disabled>
																					<?php
																						if ($available_packages) {
																							for($i = 0; $i < count ( $available_packages ); $i ++) {
																							$package_code_s = $available_packages [$i] ["package_code"];
																							$package_name = $available_packages [$i] ["package_name"];
																						?>
																							<option value="<?=$package_code_s?>"
																								<?php if ($package_code == $package_code_s){?>
																								selected="selected" <?php }?>> <?=$package_name?></option>
																							<?php
																								}
																							} else {
																								?>
																							<option value="" selected></option> <?php
																									}
																								?>		
																				</select
																					onfocus="javascript:removeTooltip('package_tooltip');">
																						<span id="package_tooltip" class="tooltiptext"
																							style="display: none; color: red;"><?php echo FILL_THIS_FIELD; ?></span>
																					</div>
																				</div>
																				<div class="form-group">
																					<label class="col-md-3 control-label"><?php echo OFFICE_NAME; ?> <span
																						class="required"> * </span></label>
																					<div class="col-md-6">
																						<input type="text"
																							class="form-control "
																							name="office_name" id="office_name"
																							value="<?php echo $name?>"
																							onfocus="javascript:removeTooltip('office_name_tooltip');">
																						<span id="office_name_tooltip" class="tooltiptext"
																							style="display: none; color: red;"><?php echo FILL_THIS_FIELD; ?></span>
																					</div>
																				</div>

																				<div class="form-group">
																					<label class="col-md-3 control-label"><?php echo ADDRESS; ?> <span
																						class="required"> * </span></label>
																					<div class="col-md-6">
																						<textarea class="form-control "
																							rows="4" cols="50" name="address"
																							onfocus="javascript:removeTooltip('address_tooltip');"><?php echo $address?></textarea>
																						<span id="address_tooltip" class="tooltiptext"
																							style="display: none; color: red;"><?php echo FILL_THIS_FIELD; ?></span>
																					</div>
																				</div>
																				<div class="form-group">
																					<label class="col-md-3 control-label"><?php echo CITY; ?></label>
																					<div class="col-md-6">
																						<select class="form-control "
																							name="city" id="city" class="form-control">
																						<?php
																																																																									if ($available_cities) {
																																																																										for($i = 0; $i < count ( $available_cities ); $i ++) {
																																																																											$city_id = $available_cities [$i] ["city_id"];
																																																																											$city_name = $available_cities [$i] ["city_name"];
																																																																											?>
																							<option value="<?=$city_id?>"
																								<?php if ($city == $city_id){?>
																								selected="selected" <?php }?>> <?=$city_name?></option>
																						<?php
																																																																										}
																																																																									} else {
																																																																										?>
																							<option value="" selected></option><?php
																																																																									}
																																																																									?>		
																					</select>
																					</div>
																				</div>
																				<div class="form-group">
																					<label class="col-md-3 control-label"><?php echo PHONE_NUMBER; ?></label>
																					<div class="col-md-6">
																						<input type="number"
																							onkeydown="numberKeyDown(event)"
																							class="form-control no-spinners"
																							name="office_phone_number"
																							value="<?php echo $office_phone_number?>" 
																							onfocus="javascript:removeTooltip('office_phone_number_tooltip');">
															<span id="office_phone_number_tooltip" class="tooltiptext"
																style="display: none; color: red"><?php echo PHONE_NUMBER_INVALID; ?></span>
																					</div>
																				</div>
																				<div class="form-group">
																					<label class="col-md-3 control-label"><?php echo MOBILE_NUMBER; ?> <span
																						class="required"> * </span></label>
																					<div class="col-md-6">
																						<input type="number"
																							onkeydown="numberKeyDown(event)"
																							class="form-control no-spinners"
																							name="office_mobile_number"
																							id="office_mobile_number"
																							value="<?php echo $office_mobile_number?>"
																							onfocus="javascript:removeTooltip('office_mobile_number_tooltip');">
																						<span id="office_mobile_number_tooltip"
																							class="tooltiptext"
																							style="display: none; color: red;"><?php echo MOBILE_NUMBER_INVALID; ?></span>
																					</div>
																				</div>
																				<div class="form-group">
																					<label class="col-md-3 control-label"><?php echo EMAIL; ?></label>
																					<div class="col-md-6">
																						<input type="text"
																							class="form-control "
																							name="office_email"
																							value="<?php echo $office_email?>"
																							id="office_email"
																							onfocus="javascript:removeTooltip('office_email_tooltip');">
																						<span id="office_email_tooltip"
																							class="tooltiptext"
																							<?php if (isset($email_exists_error) && $email_exists_error == 1){?>
																							style="display: block; color: red;"
																							<?php }else {?> style="display: none; color:red;"
																							<?php }?>><?php
																																																																									
																																																																									if (isset ( $email_exists_error ) && $email_exists_error == 1) {
																																																																										echo EMAIL_IS_EXIST;
																																																																									}
																																																																									?></span>
																					</div>
																				</div>
																				<div class="form-group">
																					<label class="col-md-3 control-label"><?php echo CONTACT_PERSON_NAME; ?><span
																						class="required"> * </span></label>
																					<div class="col-md-6">
																						<input type="text"
																							class="form-control "
																							name="contact_person_name"
																							id="contact_person_name"
																							value="<?php echo $contact_person_name?>"
																							onfocus="javascript:removeTooltip('contact_person_name_tooltip');">
																						<span id="contact_person_name_tooltip"
																							class="tooltiptext"
																							style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																					</div>
																				</div>
																				<div class="form-group">
																					<label class="col-md-3 control-label"><?php echo POSITION; ?></label>
																					<div class="col-md-6">
																						<input type="text"
																							class="form-control " name="position"
																							value="<?php echo $position?>">
																					</div>
																				</div>
																				<div class="form-group">
																					<label class="col-md-3 control-label"><?php echo SUBSCRIBTION_START_DATE; ?></label>
																					<div class="col-md-6">
																						<input type="text" disabled
																							class="form-control date"
																							name="subscription_start_date"
																							value="<?php echo $subscription_start_date?>">
																							<span id="start_date_tooltip" class="tooltiptext"
																style="display: none; color: red"><?php echo INVALID_DATES; ?></span>
																					</div>
																				</div>
																				<div class="form-group">
																					<label class="col-md-3 control-label"><?php echo SUBSCRIBTION_END_DATE; ?></label>
																					<div class="col-md-6">
																						<input type="text" disabled
																							class="form-control date"
																							name="subscription_end_date"
																							value="<?php echo $subscription_end_date?>">
																							<span id="end_date_tooltip" class="tooltiptext"
																style="display: none; color: red"><?php echo INVALID_DATES; ?></span>
																					</div>
																				</div>
																			</div>

																			<div class="margin-top-10">
																				<button type="submit" class="btn green"><?php echo SAVE ?></button>
																			</div>

																		</form>
																		<!-- END FORM-->
																	</div>
																	<!-- END OFFICE INFO TAB -->
																	<!-- More Details TAB -->
                                                                        <?php } else if ($user_type_code == "CUSTOMER"){?>
                                                                                   <div
																		class="tab-pane" id="tab_1_4">
																		<form id="detailsForm" class="form-horizontal"
																			role="form" method="post"
																			onsubmit="javascript:return checkCustomerInfoValid();"
																			action="<?=site_url('user_profile/edit_customer_info/'.$user_id)?>">

																			<div class="form-body">

																				<div class="form-group">
																					<label class="col-md-3 control-label"><?php echo CUSTOMER_TYPE; ?></label>
																					<div class="col-md-6">
																						<select class="form-control " disabled
																							name="customer_type" id="customer_type"
																							class="form-control">
																				<?php
																																																																									if ($available_customer_types) {
																																																																										for($i = 0; $i < count ( $available_customer_types ); $i ++) {
																																																																											$customer_type_id = $available_customer_types [$i] ["customer_type_id"];
																																																																											$customer_type_name = $available_customer_types [$i] ["name"];
																																																																											?>
																					<option value="<?=$customer_type_id?>"
																								<?php if ($customer_type == $customer_type_id){?>
																								selected="selected" <?php }?>> <?=$customer_type_name?></option>
																				<?php
																																																																										}
																																																																									} else {
																																																																										?>
																					<option value="" selected></option>
																				<?php
																																																																									}
																																																																									?></select>
																					</div>
																				</div>

																				<div class="form-group">
																					<label class="col-md-3 control-label"><?php echo CITY; ?></label>
																					<div class="col-md-6">
																						<select class="form-control "
																							name="city" id="city" class="form-control">
																				<?php
																																																																									if ($available_cities) {
																																																																										for($i = 0; $i < count ( $available_cities ); $i ++) {
																																																																											$city_id = $available_cities [$i] ["city_id"];
																																																																											$city_name = $available_cities [$i] ["city_name"];
																																																																											?>
																					<option value="<?=$city_id?>"
																								<?php if ($city == $city_id){?>
																								selected="selected" <?php }?>> <?=$city_name?></option>
																				<?php
																																																																										}
																																																																									} else {
																																																																										?>
																					<option value="" selected></option>
																				<?php
																																																																									}
																																																																									?></select>
																					</div>
																				</div>

																				<div class="form-group">
																					<label class="col-md-3 control-label"><?php echo ADDRESS; ?></label>
																					<div class="col-md-6">
																						<textarea class="form-control "
																							rows="4" cols="50" name="address" form="myForm"><?php echo $address?></textarea>

																					</div>
																				</div>

																				<div class="form-group">
																					<label class="col-md-3 control-label"><?php echo PHONE_NUMBER; ?></label>
																					<div class="col-md-6">
																						<input type="number"
																							onkeydown="numberKeyDown(event)"
																							class="form-control no-spinners"
																							name="phone_number"
																							value="<?php echo $phone_number?>"
																							onfocus="javascript:removeTooltip('phone_number_tooltip');">
															<span id="phone_number_tooltip" class="tooltiptext"
																style="display: none; color: red"><?php echo PHONE_NUMBER_INVALID; ?></span>
																					</div>
																				</div>


																				<div class="form-group">
																					<label class="col-md-3 control-label"><?php echo CONTACT_PERSON_NAME; ?></label>
																					<div class="col-md-6">
																						<input type="text"
																							class="form-control "
																							name="contact_person_name"
																							value="<?php echo $contact_person_name?>">

																					</div>
																				</div>
																				<div class="form-group">
																					<label class="col-md-3 control-label"><?php echo POSITION; ?></label>
																					<div class="col-md-6">
																						<input type="text"
																							class="form-control " name="position"
																							value="<?php echo $position?>">
																					</div>
																				</div>
																				<div class="form-group">
																					<label class="col-md-3 control-label"><?php echo SUBSCRIBTION_START_DATE; ?></label>
																					<div class="col-md-6">
																						<input type="text" disabled
																							class="form-control date"
																							name="subscription_start_date"
																							value="<?php echo $subscription_start_date?>">
																							<span id="start_date_tooltip" class="tooltiptext"
																style="display: none; color: red"><?php echo INVALID_DATES; ?></span>
																					</div>
																				</div>
																				<div class="form-group last">
																					<label class="col-md-3 control-label"><?php echo SUBSCRIBTION_END_DATE; ?></label>
																					<div class="col-md-6">
																						<input type="text" disabled
																							class="form-control date"
																							name="subscription_end_date"
																							value="<?php echo $subscription_end_date?>">
																							<span id="end_date_tooltip" class="tooltiptext"
																style="display: none; color: red"><?php echo INVALID_DATES; ?></span>
																					</div>
																				</div>

																				<div class="form-group">
																					<label class="col-md-3 control-label"><?php echo NUM_OF_CASES_A_MONTH; ?></label>
																					<div class="col-md-6">
																						<input type="number" disabled
																							onkeydown="numberKeyDown(event)"
																							class="form-control no-spinners"
																							name="number_of_cases_per_month"
																							value="<?php echo $number_of_cases_per_month?>"
																							onfocus="javascript:removeTooltip('number_of_cases_per_month_tooltip');">
															<span id="number_of_cases_per_month_tooltip" class="tooltiptext"
																style="display: none; color: red"><?php echo NUMBER_INVALID; ?></span>
																					</div>
																				</div>

																				<!-- <div class="form-group">
																					<label class="col-md-3 control-label"><?php echo SHOW_INVOICE_MODULE; ?></label>
																					<div class="col-md-4">
																						<table style="width: 60%">
																							<tr>
																								<td><input type="radio"
																									name="show_invoice_module" value="Y"
																									<?php if($show_invoice_module == 'Y'){?>
																									checked <?php }?>></td>
																								<td><?php echo YES; ?></td>
																								<td><input type="radio"
																									name="show_invoice_module" value="N"
																									<?php if($show_invoice_module == 'N'){?>
																									checked <?php }?>></td>
																								<td><?php echo NO; ?></td>
																							</tr>
																						</table>

																					</div>
																				</div>

																				<div class="form-group">
																					<label class="col-md-3 control-label"><?php echo SHOW_DASHBOARD; ?></label>
																					<div class="col-md-4">
																						<table style="width: 60%">
																							<tr>
																								<td><input type="radio" name="show_dashboard"
																									value="Y" <?php if($show_dashboard == 'Y'){?>
																									checked <?php }?>></td>
																								<td><?php echo YES; ?></td>
																								<td><input type="radio" name="show_dashboard"
																									value="N" <?php if($show_dashboard == 'N'){?>
																									checked <?php }?>></td>
																								<td><?php echo NO; ?></td>
																							</tr>
																						</table>
																					</div>
																				</div>-->

																				<div class="margin-top-10">
																					<button type="submit" class="btn green"><?php echo SAVE ?></button>
																				</div>

																			</div>
																	
																	</div>


																	</form>
																	<!-- END FORM-->
																</div>
																<!-- END More Details TAB -->
                                                                                   
                                                                        <?php } else if ($user_type_code == "LAWYER") { ?>
                                                                                    <div
																	class="tab-pane" id="tab_1_4">
																	<form id="detailsForm" class="form-horizontal"
																		role="form" method="post"
																		onsubmit="javascript:return checkLawyerInfoValid();"
																		action="<?=site_url('user_profile/edit_lawyer_info/'.$user_id)?>">

																		<div class="form-body">

																			<div class="form-group">
																				<label class="col-md-3 control-label"><?php echo MANAGER; ?> <span
																					class="required"> * </span></label>
																				<div class="col-md-6">
																					<select disabled class="form-control "
																						name="manager" id="manager"
																						onfocus="javascript:removeTooltip('manager_tooltip');"
																						class="form-control">
																						<?php
																																																																									if ($available_managers) {
																																																																										for($i = 0; $i < count ( $available_managers ); $i ++) {
																																																																											$manager_id = $available_managers [$i] ["user_id"];
																																																																											$manager_name = $available_managers [$i] ["user_name"];
																																																																											?>
																							<option value="<?=$manager_id?>"
																							<?php if ($manager == $manager_id){?>
																							selected="selected" <?php }?>> <?=$manager_name?></option>
																						<?php
																																																																										}
																																																																									} else {
																																																																										?>
																								<option value="" selected></option>
																							<?php
																																																																									}
																																																																									?>		
																					</select> <span id="manager_tooltip"
																						class="tooltiptext"
																						style="display: none; color: red;"><?php echo FILL_THIS_FIELD; ?></span>
																				</div>
																			</div>

																			<div class="form-group">
																				<label class="col-md-3 control-label"><?php echo STAGE_ONE_COST; ?> <span
																					class="required"> * </span></label>
																				<div class="col-md-6">
																					<input type="number"
																						onkeydown="numberKeyDown(event)"
																						class="form-control no-spinners" readonly="readonly"
																						name="stage1_cost"
																						value="<?php echo $stage1_cost?>" id="stage1_cost"
																						onfocus="javascript:removeTooltip('stage1_cost_tooltip');">
																					<span id="stage1_cost_tooltip" class="tooltiptext"
																						style="display: none; color: red;"><?php echo NUMBER_INVALID; ?></span>
																				</div>
																			</div>

																			<div class="form-group">
																				<label class="col-md-3 control-label"><?php echo STAGE_TWO_COST; ?> <span
																					class="required"> * </span> </label>
																				<div class="col-md-6">
																					<input type="number"
																						onkeydown="numberKeyDown(event)"
																						class="form-control no-spinners" readonly="readonly"
																						name="stage2_cost"
																						value="<?php echo $stage2_cost?>" id="stage2_cost"
																						onfocus="javascript:removeTooltip('stage2_cost_tooltip');">
																					<span id="stage2_cost_tooltip" class="tooltiptext"
																						style="display: none; color: red;"><?php echo NUMBER_INVALID; ?></span>
																				</div>
																			</div>

																			<div class="form-group">
																				<label class="col-md-3 control-label"><?php echo STAGE_THREE_COST; ?><span
																					class="required"> * </span></label>
																				<div class="col-md-6">
																					<input type="number" readonly="readonly"
																						onkeydown="numberKeyDown(event)"
																						class="form-control no-spinners"
																						name="stage3_cost"
																						value="<?php echo $stage3_cost?>" id="stage3_cost"
																						onfocus="javascript:removeTooltip('stage3_cost_tooltip');">
																					<span id="stage3_cost_tooltip" class="tooltiptext"
																						style="display: none; color: red;"><?php echo NUMBER_INVALID; ?></span>
																				</div>
																			</div>

																			<div class="form-group">
																				<label class="col-md-3 control-label"><?php echo STAGE_FOUR_COST; ?> <span
																					class="required"> * </span></label>
																				<div class="col-md-6">
																					<input type="number"
																						onkeydown="numberKeyDown(event)"
																						class="form-control no-spinners" readonly="readonly"
																						name="stage4_cost"
																						value="<?php echo $stage4_cost?>" id="stage4_cost"
																						onfocus="javascript:removeTooltip('stage4_cost_tooltip');">
																					<span id="stage4_cost_tooltip" class="tooltiptext"
																						style="display: none; color: red;"><?php echo NUMBER_INVALID; ?></span>
																				</div>
																			</div>

																			<div class="form-group">
																				<label class="col-md-3 control-label"><?php echo STAGE_FIVE_COST; ?> <span
																					class="required"> * </span></label>
																				<div class="col-md-6">
																					<input type="number"
																						onkeydown="numberKeyDown(event)"
																						class="form-control no-spinners" readonly="readonly"
																						name="stage5_cost"
																						value="<?php echo $stage5_cost?>" id="stage5_cost"
																						onfocus="javascript:removeTooltip('stage5_cost_tooltip');">
																					<span id="stage5_cost_tooltip" class="tooltiptext"
																						style="display: none; color: red;"><?php echo NUMBER_INVALID; ?></span>
																				</div>
																			</div>

																		</div>

																		<div class="margin-top-10">
																			<button type="submit" class="btn green"><?php echo SAVE ?></button>
																		</div>
																	</form>
																	<!-- END FORM-->
																</div>
																<!-- END More Details TAB -->
                                                                        <?php }?>
                                                                    </div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<!-- END PROFILE CONTENT -->
									</div>
								</div>
							</div>
							<!-- END PAGE CONTENT INNER -->
						</div>
					</div>
					<!-- END PAGE CONTENT BODY -->
					<!-- END CONTENT BODY -->
				</div>
				<!-- END CONTENT -->

			</div>
			<!-- END CONTAINER -->
		</div>
	</div>

	</div>
        <?php $this->load->view('utils/footer');?>
       
<script type="text/javascript">
	function readBanner(input) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();
	
	        reader.onload = function (e) {
	            $('#img_banner')
	                .attr('src', e.target.result);
                
	        };
	
	        reader.readAsDataURL(input.files[0]);

	        $("#banner_buttons").removeClass('hidden');
			document.getElementById('banner_buttons').style.display = 'block';
			document.getElementById('banner_buttons').style.visibility = 'visible';
	    }
	}

	function uploadBanner() {
		//e.preventDefault();
        $("#upload_banner:hidden").trigger('click');
	}

	function saveBanner() {
		document.getElementById('bannerForm').submit();
	}

	function cancelBannerChange(banner_src) {
		$("#img_banner").attr("src", banner_src);
	}

	function changeAvatar() {
		document.getElementById('avatarForm').submit();
	}
	
	function cancelAvatarChange(img_src) {
		$("#img_avatar").attr("src", img_src);

	}
	
	function changePassword() {
		document.getElementById('passwordForm').submit();
	}
	
	function checkPassword(){
	
	var old_password= document.forms["passwordForm"]["old_password"].value;
    	var new_password= document.forms["passwordForm"]["new_password"].value;
    	var retyped_new_password= document.forms["passwordForm"]["retyped_new_password"].value;
    	
    	if (old_password== "") {
    		$("#old_password_tooltip").css("display","block");
    		$("#old_password_tooltip").css("visibility","visible");
    		return false;
    	} else{
    		$("#old_password_tooltip").css("display","none");
    		$("#old_password_tooltip").css("visibility","none");
    	}
    	if (new_password== "") {
    		$("#new_password_tooltip").css("display","block");
    		$("#new_password_tooltip").css("visibility","visible");
    		return false;
    	} else{
    		$("#new_password_tooltip").css("display","none");
    		$("#new_password_tooltip").css("visibility","none");
    	}
    	if (retyped_new_password== "") {
    		$("#retyped_new_password_tooltip").css("display","block");
    		$("#retyped_new_password_tooltip").css("visibility","visible");
    		return false;
    	} else{
    		$("#retyped_new_password_tooltip").css("display","none");
    		$("#retyped_new_password_tooltip").css("visibility","none");
    	}
    	
		document.getElementById('incorrect_old_password').style.display = 'none';
		document.getElementById('incorrect_repeated_password').style.display = 'none';
		var saved_password = document.getElementById('saved_password').value;
		var old_password = document.getElementById('old_password').value;
		var new_password = document.getElementById('new_password').value;
		var retyped_new_password = document.getElementById('retyped_new_password').value;
		if(saved_password != old_password){
			document.getElementById('incorrect_old_password').style.display = 'block';
			return false;
		} else if (new_password != retyped_new_password) {
			document.getElementById('incorrect_repeated_password').style.display = 'block';
			return false;
		}
		return true;
	}

</script>


	<!-- BEGIN CORE PLUGINS -->
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.sparkline.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="<?=base_url()?>assets/global/scripts/app.min.js"
		type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?=base_url()?>assets/pages/scripts/profile.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script
		src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js"
		type="text/javascript"></script>
	<!-- END THEME LAYOUT SCRIPTS -->

</body>

</html>