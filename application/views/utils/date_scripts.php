<link rel="stylesheet" type="text/css"
	href="<?=base_url()?>assets/css/jquery.calendars.picker.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?=base_url()?>assets/js/jquery.plugin.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery.calendars.all.js"></script>
<script src="<?=base_url()?>assets/js/jquery.calendars.plus.js"></script>
<script src="<?=base_url()?>assets/js/jquery.calendars.ummalqura.js"></script>
<script src="<?=base_url()?>assets/js/jquery.calendars.ummalqura-ar.js"></script>
<script>
var jqOld = jQuery.noConflict();
jqOld( function() {
	jqOld( "input.date" ).calendarsPicker({calendar: jqOld.calendars.instance('ummalqura','ar')});
  } );
 jqOld( function() {
	    jqOld( "input.dateMilady" ).calendarsPicker({calendar: jqOld.calendars.instance('gregorian','ar')});
  } );
 </script>