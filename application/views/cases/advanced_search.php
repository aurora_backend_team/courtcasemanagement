<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<?php
// session_start ();
$this->load->view ( 'localization/lang' );
$this->load->view ( 'localization/txt' );
defineLocale ();
defineStrings ();
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<script type="text/javascript">

function isValidDate(dateVar){
	//Format: yyyy/mm/dd
	var dateReg = /^\d{4}\/\d{2}\/\d{2}$/;
	var isV = dateReg.test(dateVar);
	if(!isV){
		return false;
	} 
	
	var str_array = dateVar.split('/');
	var hijriDate =  $.calendars.instance('ummalqura','ar').newDate();
	try {
	   hijriDate = hijriDate.date(str_array[0], str_array[1], str_array[2]);
	}
	catch(err) {
	   return false;
	}
	return true;
}

function checkValid() {

	var creation_date = document.forms["myForm"]["creation_date"].value;
	if (creation_date != ""){
		var isValidD = isValidDate(creation_date);
	   	if(!isValidD){
			$("#creation_date_tooltip").css("display","block");
			$("#creation_date_tooltip").css("visibility","visible");
	   		return false;
	   	}else{
			$("#creation_date_tooltip").css("display","none");
			$("#creation_date_tooltip").css("visibility","none");
	   	}
	}else{
		$("#creation_date_tooltip").css("display","none");
		$("#creation_date_tooltip").css("visibility","none");
   	}
   	
	var decision_34_date = document.forms["myForm"]["decision_34_date"].value;
	if (decision_34_date != ""){
		var isValidD = isValidDate(decision_34_date);
	   	if(!isValidD){
			$("#decision_34_date_tooltip").css("display","block");
			$("#decision_34_date_tooltip").css("visibility","visible");
	   		return false;
	   	}else{
			$("#decision_34_date_tooltip").css("display","none");
			$("#decision_34_date_tooltip").css("visibility","none");
	   	}
	}else{
		$("#decision_34_date_tooltip").css("display","none");
		$("#decision_34_date_tooltip").css("visibility","none");
   	}

	var decision_46_date = document.forms["myForm"]["decision_46_date"].value;
	if (decision_46_date != ""){
		var isValidD = isValidDate(decision_46_date);
	   	if(!isValidD){
			$("#decision_46_date_tooltip").css("display","block");
			$("#decision_46_date_tooltip").css("visibility","visible");
	   		return false;
	   	}else{
			$("#decision_46_date_tooltip").css("display","none");
			$("#decision_46_date_tooltip").css("visibility","none");
	   	}
	}else{
		$("#decision_46_date_tooltip").css("display","none");
		$("#decision_46_date_tooltip").css("visibility","none");
   	}
	
	var advertisement_date = document.forms["myForm"]["advertisement_date"].value;
	if (advertisement_date != ""){
		var isValidD = isValidDate(advertisement_date);
	   	if(!isValidD){
			$("#advertisement_date_tooltip").css("display","block");
			$("#advertisement_date_tooltip").css("visibility","visible");
	   		return false;
	   	}else{
			$("#advertisement_date_tooltip").css("display","none");
			$("#advertisement_date_tooltip").css("visibility","none");
	   	}
	}else{
		$("#advertisement_date_tooltip").css("display","none");
		$("#advertisement_date_tooltip").css("visibility","none");
   	}
   	
   	
   	var executive_order_date = document.forms["myForm"]["executive_order_date"].value;
	if (executive_order_date != ""){
		var isValidD = isValidDate(executive_order_date);
	   	if(!isValidD){
			$("#executive_order_date_tooltip").css("display","block");
			$("#executive_order_date_tooltip").css("visibility","visible");
	   		return false;
	   	}else{
			$("#executive_order_date_tooltip").css("display","none");
			$("#executive_order_date_tooltip").css("visibility","none");
	   	}
	}else{
		$("#executive_order_date_tooltip").css("display","none");
		$("#executive_order_date_tooltip").css("visibility","none");
   	}

	return true;
}

function numberKeyDown(e) {
	var keynum;
    if(window.event) { // IE                    
      keynum = e.keyCode;
    } else if(e.which){ // Netscape/Firefox/Opera                   
      keynum = e.which;
    }

 // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(keynum, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (keynum === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
        // Allow: Ctrl+C, Command+C
        (keynum === 67 && (e.ctrlKey === true || e.metaKey === true)) ||  
        // Allow: Ctrl+X, Command+X
        (keynum === 88 && (e.ctrlKey === true || e.metaKey === true)) || 
        // Allow: Ctrl+Z, Command+Z
        (keynum === 90 && (e.ctrlKey === true || e.metaKey === true)) ||
         // Allow: home, end, left, right, down, up
        (keynum >= 35 && keynum <= 37)) {
             // let it happen, don't do anything
             return;
    }
    
    if(((e.shiftKey || (keynum < 48 || keynum > 57)) && (keynum < 96 || keynum > 105)) || (keynum >= 38 && keynum <= 40)){
		 e.preventDefault();
	}
}

$(document).ready(function(){
	  $('#region_id').change(function() {
	    var val = $("#region_id option:selected").val();
	    var lawyers_array = <?php echo json_encode($lawyers_per_region); ?>;
	    var options = "<option value='' selected></option>";
		
		var fetched_lawyer = "";
	    for (var i in lawyers_array) {
		    if (i == val || val == "") {
				var lawyers = lawyers_array[i];
		    	for (j = 0; j < lawyers.length; j++) { 
		    		var lawyer_id = lawyers[j]["user_id"];
					if(fetched_lawyer.includes(lawyers[j]["user_id"])== false){
						fetched_lawyer = lawyers[j]["user_id"]+" "+fetched_lawyer;
						var lawyer_name = lawyers[j]["first_name"]+' '+lawyers[j]["last_name"];
						options += "<option value='"+lawyer_id+"'>"+lawyer_name+"</option>";
					}
		    	}
		    	
		    }
	    }
				 $("#lawyer_id").html(options);
        
	  });

	    $('form').on('focus', 'input[type=number]', function(e) {
	    	$(this).on('wheel', function(e) {
	    		e.preventDefault();
	    	});
	        });

	        // Restore scroll on number inputs.
	        $('form').on('blur', 'input[type=number]', function(e) {
	        	$(this).off('wheel');
	        });

	        // Disable up and down keys.
	        $('form').on('keydown', 'input[type=number]', function(e) {
	    	if ( e.which == 38 || e.which == 40 )
	    		e.preventDefault();
	        }); 

	        //disable mousewheel on a input number field when in focus
	        //(to prevent Cromium browsers change the value when scrolling)
	        $('form').on('focus', 'input[type=number]', function (e) {
	    	$(this).on('mousewheel.disableScroll', function (e) {
	    		e.preventDefault();
	    	});
	        });
	        $('form').on('blur', 'input[type=number]', function (e) {
	        	$(this).off('mousewheel.disableScroll');
	        });	 
	        
	});

</script>
<meta charset="utf-8" />
<title><?php echo TITLE; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin RTL Theme #3 for dashboard & statistics"
	name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->

<link rel="shortcut icon"
	href="<?=base_url()?>assets/layouts/layout3/img/logo-default.jpg" />
</head>
<!-- END HEAD -->
<body class="page-container-bg-solid">
	<div class="page-wrapper">
	<?php
	if ($user_type_code == "ADMIN") {
		$this->load->view ( 'office_admin/office_admin_header' );
	} else {
		$this->load->view ( 'secretary_lawyer/secretary_header' );
	}
	$this->load->view ( 'utils/date_scripts' );
	?>
	<div class="page-wrapper-row full-height">
			<div class="page-wrapper-middle">
				<!-- BEGIN CONTAINER -->
				<div class="page-container">
					<!-- BEGIN CONTENT -->

					<div class="page-content-wrapper">

						<!-- BEGIN CONTENT BODY -->
						<!-- BEGIN PAGE HEAD-->
						<div class="page-head">
							<div class="container">
								<div id="invalid_search" class="form-group"
									style="display: none; color: red;">
								<?php echo SEARCH_DATA_INVALID;?>
							</div>
								<div class="col-md-14">
									<div class="portlet box green">
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-gift"></i>
												<?php
												echo SEARCH_DATA;
												?>
											</div>
										</div>
										<div class="portlet-body form">
											<!-- BEGIN FORM-->
											<form id="myForm" role="form" method="post"
												enctype="multipart/form-data"
												action="<?=site_url('case_management/view_cases')?>"
												class="form-horizontal">
												<input type="hidden" name="search_mode" id="search_mode"
													value="1"> <input type="hidden" name="customer_id"
													value="<?php echo $search_data["customer_id"]?>">
												<div class="form-body">
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo CASE_ID; ?>
																</label>
																<div class="col-md-6">
																	<input type="text" class="form-control no-validate"
																		name="case_id" id="case_id"
																		value="<?php echo $search_data["case_id"]?>">
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo CUSTOMER; ?>
																</label>
																<div class="col-md-6">
																	<select class="form-control " name="customer_id"
																		id="customer_id" class="form-control">
																		<option value="" selected></option>
					  	<?php
								if ($available_customers) {
									for($i = 0; $i < count ( $available_customers ); $i ++) {
										$customer_id_s = $available_customers [$i] ["user_id"];
										$customer_name = $available_customers [$i] ["first_name"] . " " . $available_customers [$i] ["last_name"];
										?>
							<option value="<?=$customer_id_s?>"
																			<?php if ($customer_id_s == $search_data["customer_id"]){?>
																			selected="selected" <?php }?>> <?=$customer_name ?></option>
						<?php
									}
								} else {
									?>
							<option value="" selected></option>
						<?php
								}
								?>		
					</select>
																</div>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo CONTRACT_NUMBER; ?>
																</label>
																<div class="col-md-6">
																	<input type="text" act="yes"
																		onkeydown="numberKeyDown(event)"
																		class="form-control no-spinners"
																		name="contract_number" id="contract_number"
																		value="<?php echo $search_data["contract_number"]?>">
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo DEBTOR_NAME; ?>
																</label>
																<div class="col-md-6">
																	<input type="text" class="form-control "
																		name="debtor_name" id="debtor_name"
																		value="<?php echo $search_data["debtor_name"]?>">
																</div>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo CLIENT_NAME; ?>
																</label>
																<div class="col-md-6">
																	<input type="text" class="form-control "
																		name="client_name" id="client_name"
																		value="<?php echo $search_data["client_name"]?>">
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo CLIENT_ID_NUMBER; ?>
																</label>
																<div class="col-md-6">
																	<input type="text" act="yes"
																		onkeydown="numberKeyDown(event)"
																		class="form-control no-spinners"
																		name="client_id_number" id="client_id_number"
																		value="<?php echo $search_data["client_id_number"]?>">
																</div>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"> <?php echo CLIENT_TYPE; ?> 
																</label>

																<div class="col-md-6">
																	<select class="form-control " name="client_type"
																		id="client_type" class="form-control">
																		<option value="" selected></option>
					  	<?php
								if ($available_customer_types) {
									for($i = 0; $i < count ( $available_customer_types ); $i ++) {
										$customer_type_id = $available_customer_types [$i] ["customer_type_id"];
										$customer_type_name = $available_customer_types [$i] ["name"];
										?>
							<option value="<?=$customer_type_id ?>"
																			<?php if ($customer_type_id == $search_data["client_type"]){?>
																			selected="selected" <?php }?>> <?=$customer_type_name ?></option>
						<?php
									}
								} else {
									?>
							<option value="" selected></option>
						<?php
								}
								?>		
					</select>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo OBLIGATION_VALUE; ?>
																 </label>
																<div class="col-md-4">
																	<input type=type= "text" act="yes"
																		class="form-control no-spinners"
																		onkeydown="numberKeyDown(event)"
																		name="obligation_value"
																		value="<?php echo $search_data["obligation_value"]?>"
																		id="obligation_value">
																</div>
																<label class="col-md-2 control-label"><?php echo SR; ?></label>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo NUMBER_OF_LATE_INSTALMENTS; ?>
																 </label>
																<div class="col-md-6">
																	<input type="text" act="yes"
																		class="form-control no-spinners"
																		onkeydown="numberKeyDown(event)"
																		name="number_of_late_instalments"
																		value="<?php echo $search_data["number_of_late_instalments"]?>"
																		id="number_of_late_instalments">
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo DUE_AMOUNT; ?>
																 </label>
																<div class="col-md-4">
																	<input type="text" act="yes"
																		class="form-control no-spinners"
																		onkeydown="numberKeyDown(event)"
																		value="<?php echo $search_data["due_amount"]?>"
																		id="due_amount" name="due_amount">
																</div>
																<label class="col-md-2 control-label"><?php echo SR; ?></label>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo REMAINING_AMOUNT; ?>
																</label>
																<div class="col-md-4">
																	<input type=type= "text" act="yes"
																		class="form-control no-spinners"
																		onkeydown="numberKeyDown(event)"
																		name="remaining_amount"
																		value="<?php echo $search_data["remaining_amount"]?>"
																		id="remaining_amount">
																</div>
																<label class="col-md-2 control-label"><?php echo SR; ?></label>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo TOTAL_DEBENTURE_AMOUNT; ?></label>
																<div class="col-md-4">
																	<input type="text" act="yes"
																		onkeydown="numberKeyDown(event)"
																		class="form-control  no-spinners"
																		name="total_debenture_amount"
																		id="total_debenture_amount"
																		value="<?php echo $search_data["total_debenture_amount"]?>">
																</div>
																<label class="col-md-2 control-label"><?php echo SR; ?></label>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo CREATION_DATE; ?>
																</label>
																<div class="col-md-6">
																	<input type="text" class="form-control date"
																		name="creation_date" id="creation_date"
																		value="<?php echo $search_data["creation_date"]?>"> <span
																		id="creation_date_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo EXECUTIVE_ORDER_NUMBER; ?>
																</label>
																<div class="col-md-6">
																	<input type="text" act="yes"
																		class="form-control no-spinners"
																		onkeydown="numberKeyDown(event)"
																		name="executive_order_number"
																		id="executive_order_number"
																		value="<?php echo $search_data["executive_order_number"]?>">
																</div>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo ORDER_NUMBER; ?>
																</label>
																<div class="col-md-6">
																	<input type="text" act="yes"
																		class="form-control no-spinners"
																		onkeydown="numberKeyDown(event)" name="order_number"
																		id="order_number"
																		value="<?php echo $search_data["order_number"]?>">
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo CIRCLE_NUMBER; ?>
																</label>
																<div class="col-md-6">
																	<input type="text" act="yes"
																		class="form-control no-spinners"
																		onkeydown="numberKeyDown(event)" name="circle_number"
																		id="circle_number"
																		value="<?php echo $search_data["circle_number"]?>">
																</div>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo DECISION_34_NUMBER; ?>
																	</label>
																<div class="col-md-6">
																	<input type="text" act="yes"
																		class="form-control no-spinners"
																		onkeydown="numberKeyDown(event)"
																		name="decision_34_number" id="decision_34_number"
																		value="<?php echo $search_data["decision_34_number"]?>">
																</div>
															</div>
														</div>

														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo DECISION_34_DATE; ?>
																</label>
																<div class="col-md-6">
																	<input type="text" class="form-control date"
																		name="decision_34_date" id="decision_34_date"
																		value="<?php echo $search_data["decision_34_date"]?>">
																	<span id="decision_34_date_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>
																</div>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo DECISION_46_DATE; ?>
																
																</label>
																<div class="col-md-6">
																	<input type="text" class="form-control date"
																		name="decision_46_date" id="decision_46_date"
																		value="<?php echo $search_data["decision_46_date"]?>">
																	<span id="decision_46_date_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>
																</div>
															</div>
														</div>

														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo CURRENT_STATE_CODE; ?></label>
																<div class="col-md-6">
																	<select class="form-control " name="state_code"
																		id="state_code" class="form-control">
																		<option value="" selected></option>
					  	<?php
								if ($available_states) {
									for($i = 0; $i < count ( $available_states ); $i ++) {
										$state_code = $available_states [$i] ["state_code"];
										$state_name = $available_states [$i] ["state_name"];
										?>
							<option value="<?=$state_code ?>"
																			<?php if ($state_code == $search_data["state_code"]){?>
																			selected="selected" <?php }?>> <?=$state_name ?></option>
						<?php
									}
								} else {
									?>
							<option value="" selected></option>
						<?php
								}
								?>		
					</select>
																</div>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo REGION?></label>
																<div class="col-md-6">
																	<select id="region_id" class="form-control "
																		name="region_id" class="form-control">
																		<option value="" selected></option>
					  	<?php
								if ($available_regions) {
									for($i = 0; $i < count ( $available_regions ); $i ++) {
										$region_id_s = $available_regions [$i] ["region_id"];
										$region_name = $available_regions [$i] ["name"];
										?>
							<option value="<?=$region_id_s ?>"
																			<?php if($search_data["region_id"] == $region_id_s){?>
																			selected="selected" <?php }?>> <?=$region_name ?></option>
						<?php
									}
								} else {
									?>
							<option value="" selected></option>
						<?php
								}
								?>		
					</select>
																</div>
															</div>
														</div>

														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo LAWYER_NAME; ?></label>
																<div class="col-md-6">
																	<select class="form-control " name="lawyer_id"
																		id="lawyer_id" class="form-control"
																		onfocus="javascript:removeTooltip('lawyer_id_tooltip');">
																		<option value="" selected></option>
					  	<?php
								$selected_region = $search_data ["region_id"];
								if ($lawyers_per_region [$selected_region]) {
									for($i = 0; $i < count ( $lawyers_per_region [$selected_region] ); $i ++) {
										$lawyer_id = $lawyers_per_region [$selected_region] [$i] ["user_id"];
										$lawyer_name = $lawyers_per_region [$selected_region] [$i] ["first_name"] . ' ' . $lawyers_per_region [$selected_region] [$i] ["last_name"];
										?>
							<option value="<?=$lawyer_id?>"
																			<?php if($search_data["lawyer_id"] == $lawyer_id){?>
																			selected="selected" <?php }?>> <?=$lawyer_name ?></option>
						<?php
									}
								} else {
									$fetched_lawyers = "";
									foreach ($available_regions as $region){
										for($i = 0; $i < count ( $lawyers_per_region [$region["region_id"]] ); $i ++) {
											$lawyer_id = $lawyers_per_region [$region["region_id"]] [$i] ["user_id"];
											// for no repeation
											if(strpos($fetched_lawyers, $lawyer_id)!== false){
												continue;
											}
											$fetched_lawyers = $fetched_lawyers . " ".$lawyer_id;
											$lawyer_name = $lawyers_per_region [$region["region_id"]] [$i] ["first_name"] . ' ' . $lawyers_per_region [$region["region_id"]] [$i] ["last_name"];
											?>
											<option value="<?=$lawyer_id?>"> <?=$lawyer_name ?></option>
						<?php
										}
									}
								}
								?>		
					</select>
																</div>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo EXECUTIVE_ORDER_DATE; ?>
																
																</label>
																<div class="col-md-6">
																	<input type="text" class="form-control date"
																		id="executive_order_date" name="executive_order_date"
																		value="<?php echo $search_data["executive_order_date"]?>">
																	<span id="executive_order_date_tooltip"
																		class="tooltiptext" style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>
																</div>
															</div>
														</div>

														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo ADVERTISMENT_DATE; ?>
																
																</label>
																<div class="col-md-6">
																	<input type="text" class="form-control date"
																		name="advertisement_date" id="advertisement_date"
																		value="<?php echo $search_data["advertisement_date"]?>">
																	<span id="advertisement_date_tooltip"
																		class="tooltiptext" style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>
																</div>
															</div>
														</div>
													</div>
													<div class="row">

														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo SUSPEND_REASON; ?>
																</label>
																<div class="col-md-6">

																	<select class="form-control "
																		name="suspend_reason_code" id="suspend_reason_code"
																		class="form-control">
																		<option value="" selected></option>
					  	<?php
								if ($suspend_reasons) {
									for($i = 0; $i < count ( $suspend_reasons ); $i ++) {
										$suspend_code = $suspend_reasons [$i] ["suspend_code"];
										$suspend_reason = $suspend_reasons [$i] ["suspend_reason"];
										?>
							<option value="<?=$suspend_code?>"
																			<?php if ($suspend_code == $search_data["suspend_reason_code"]){?>
																			selected="selected" <?php }?>>
							<?=$suspend_reason ?></option>
						<?php
									}
								} else {
									?>
							<option value="" selected></option>
						<?php
								}
								?>		
					</select>
																</div>
															</div>
														</div>


														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo SUSPEND_DATE; ?>
																</label>
																<div class="col-md-6">
																	<input type="text" class="form-control date"
																		name="suspend_date" id="suspend_date"
																		value="<?php echo $search_data["suspend_date"]?>"> <span
																		id="suspend_date_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>
																</div>
															</div>
															<div>
																															</div>
														</div>
															<div class="row">
																	<div class="col-md-9">
																		<label class="col-md-4 control-label"><?php echo CLOSING_REASON; ?></label>
																		<div class="col-md-6">

																			<select class="form-control " name="closing_reason"
																				id="closing_reason"
																				onfocus="javascript:removeTooltip('closing_reason_tooltip');">

																				<option value="" selected></option>
					  	<?php
								if ($close_reasons) {
									for($i = 0; $i < count ( $close_reasons ); $i ++) {
										$close_reason = $close_reasons [$i] ["close_reason"];
										?>
							<option value="<?=$close_reason?>"
																					<?php if ($close_reason == $search_data["closing_reason"]){?>
																					selected="selected" <?php }?>> <?=$close_reason?></option>
						<?php
									}
								}
								?>
									
					</select>
																		</div>
																	</div>
																</div>
														<br>
														<div class="row">
															<div class="col-md-9" align="center"
																style="margin: auto; width: 100%;">


																<button type="button"
																	onClick="javascript:send_search_data()"
																	class="btn btn-circle green">
																	<?php echo SEARCH;?>
																</button>

															</div>
														</div>
													</div>
												</div>

											</form>
											<!-- END FORM-->

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
        <?php $this->load->view('utils/footer');?>

	<!--[if lt IE 9]>
<script src="<?=base_url()?>assets/global/plugins/respond.min.js"></script>
<script src="<?=base_url()?>assets/global/plugins/excanvas.min.js"></script> 
<script src="<?=base_url()?>assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
	<!-- BEGIN CORE PLUGINS -->
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="<?=base_url()?>assets/global/plugins/moment.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/morris/morris.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/morris/raphael-min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/counterup/jquery.waypoints.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/counterup/jquery.counterup.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/fullcalendar/fullcalendar.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.resize.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.categories.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.sparkline.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="<?=base_url()?>assets/global/scripts/app.min.js"
		type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?=base_url()?>assets/pages/scripts/dashboard.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script
		src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js"
		type="text/javascript"></script>
	<!-- END THEME LAYOUT SCRIPTS -->
	<script>

function send_search_data() {
	
/*	try {
		var isValid = checkValid();
		if(!isValid)
			return false;
	window.opener.updateSearchValues(document.getElementById('customer_id').value,
			document.getElementById('contract_number').value,
			document.getElementById('debtor_name').value,
			document.getElementById('client_name').value,
			document.getElementById('client_id_number').value,
			document.getElementById('client_type').value,
			document.getElementById('obligation_value').value,
			document.getElementById('number_of_late_instalments').value,
			document.getElementById('due_amount').value,
			document.getElementById('remaining_amount').value,
			document.getElementById('total_debenture_amount').value,			
			document.getElementById('creation_date').value,
			document.getElementById('executive_order_number').value,
			document.getElementById('circle_number').value,
			document.getElementById('decision_34_number').value,
			document.getElementById('decision_34_date').value,
			document.getElementById('decision_46_date').value,
			document.getElementById('state_code').value,
			document.getElementById('region_id').value,
			document.getElementById('lawyer_id').value,
			document.getElementById('executive_order_date').value,
			document.getElementById('advertisement_date').value,
			document.getElementById('suspend_reason_code').value,
			document.getElementById('closing_reason').value,
			document.getElementById('case_id').value);
	}
    catch (err) {}
    window.close();
    return false;*/
    if ( (document.getElementById('customer_id').value == null || document.getElementById('customer_id').value =="") &&
    		(document.getElementById('contract_number').value == null || document.getElementById('contract_number').value =="") &&
    		(document.getElementById('debtor_name').value == null || document.getElementById('debtor_name').value =="") &&
    		(document.getElementById('client_name').value == null || document.getElementById('client_name').value =="") &&
    		(document.getElementById('client_id_number').value == null || document.getElementById('client_id_number').value =="") &&
    		(document.getElementById('client_type').value == null || document.getElementById('client_type').value =="") &&
    		(document.getElementById('obligation_value').value == null || document.getElementById('obligation_value').value =="") &&
    		(document.getElementById('number_of_late_instalments').value == null || document.getElementById('number_of_late_instalments').value =="") &&
    		(document.getElementById('due_amount').value == null || document.getElementById('due_amount').value =="") &&
    		(document.getElementById('remaining_amount').value == null || document.getElementById('remaining_amount').value =="") &&
    		(document.getElementById('total_debenture_amount').value == null || document.getElementById('total_debenture_amount').value =="") &&
    		(document.getElementById('creation_date').value == null || document.getElementById('creation_date').value =="") &&
    		(document.getElementById('executive_order_number').value == null || document.getElementById('executive_order_number').value =="") &&
    		(document.getElementById('order_number').value == null || document.getElementById('order_number').value =="") &&
    		(document.getElementById('circle_number').value == null || document.getElementById('circle_number').value =="") &&
    		(document.getElementById('decision_34_number').value == null || document.getElementById('decision_34_number').value =="") &&
    		(document.getElementById('decision_34_date').value == null || document.getElementById('decision_34_date').value =="") &&
    		(document.getElementById('decision_46_date').value == null || document.getElementById('decision_46_date').value =="") &&
    		(document.getElementById('state_code').value == null || document.getElementById('state_code').value =="") &&
    		(document.getElementById('region_id').value == null || document.getElementById('region_id').value =="") &&
    		(document.getElementById('lawyer_id').value == null || document.getElementById('lawyer_id').value =="") &&
    		(document.getElementById('executive_order_date').value == null || document.getElementById('executive_order_date').value =="") &&
    		(document.getElementById('advertisement_date').value == null || document.getElementById('advertisement_date').value =="") &&
    		(document.getElementById('suspend_date').value == null || document.getElementById('suspend_date').value =="") &&
    		(document.getElementById('suspend_reason_code').value == null || document.getElementById('suspend_reason_code').value =="") &&
    		(document.getElementById('closing_reason').value == null || document.getElementById('closing_reason').value =="") &&
    		(document.getElementById('case_id').value == null || document.getElementById('case_id').value =="") )
    {
    	document.getElementById('invalid_search').style.display = 'block';
		document.getElementById('invalid_search').style.visibility = 'visible';
    } else {
    	document.getElementById('invalid_search').style.display = 'none';
		document.getElementById('invalid_search').style.visibility = 'invisible';
		if (textNumViolation())
		return false;
		replacAllArabToEng();
		$('#loading').removeClass('hidden');
		$('#search_text').removeClass('hidden');
		document.getElementById('myForm').submit();
    }
	
}
</script>
</body>

</html>