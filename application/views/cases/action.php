<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<?php
// session_start ();
$this->load->view ( 'localization/lang' );
$this->load->view ( 'localization/txt' );
defineLocale ();
defineStrings ();
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>

<script type="text/javascript">

function isValidDate(dateVar){
	//Format: yyyy/mm/dd
	var dateReg = /^\d{4}\/\d{2}\/\d{2}$/;
	var isV = dateReg.test(dateVar);
	if(!isV){
		return false;
	} 
	
	var str_array = dateVar.split('/');
	var hijriDate =  $.calendars.instance('ummalqura','ar').newDate();
	try {
	   hijriDate = hijriDate.date(str_array[0], str_array[1], str_array[2]);
	}
	catch(err) {
	   return false;
	}
	return true;
}

function parseArabic(str) {
	   return ( str.replace(/[٠١٢٣٤٥٦٧٨٩]/g, function(d) {
        return d.charCodeAt(0) - 1632;
	   }).replace(/[۰۱۲۳۴۵۶۷۸۹]/g, function(d) {
        return d.charCodeAt(0) - 1776;
    }) );
}

function textNumViolation(){
var violate = false;
	$('*[id*=valid_span]:visible').each(function() {
	    //     alert($(this).text());
		 
		 violate = true;
	});
	return violate ;
}

function replacAllArabToEng(){
	$(':input').each(function(){
	         var attr = $(this).attr('act');
		if (typeof attr !== typeof undefined){
			$(this).val(parseArabic($(this).val()));
			//alert($(this).val());
		 }
	});
}

function checkValid(action_code) {
	var finishUploading = check_uploading_files();
	if(finishUploading){
		if (textNumViolation())
			return false;
		replacAllArabToEng();
		var todayHj =  $.calendars.instance('ummalqura','ar').newDate();
		var last_state_date = '';//document.forms["myForm"]["last_state_date_value"].value;
		if(action_code == "ASSIGN_TO_LAWYER"){
			var lawyer_id= document.forms["myForm"]["lawyer_id"].value;
			if (lawyer_id== "") {
				$("#lawyer_id_tooltip").css("display","block");
				$("#lawyer_id_tooltip").css("visibility","visible");
					return false;
			} else{
				$("#lawyer_id_tooltip").css("display","none");
				$("#lawyer_id_tooltip").css("visibility","none");
			}
		} else if (action_code == "REGISTER"){
			var executive_order_number = document.forms["myForm"]["executive_order_number"].value;
			if (executive_order_number == "" || executive_order_number <= 0 || executive_order_number.length <3 || executive_order_number.length >15) {
				$("#executive_order_number_tooltip").css("display","block");
				$("#executive_order_number_tooltip").css("visibility","visible");
				return false;
			} else{
				$("#executive_order_number_tooltip").css("display","none");
				$("#executive_order_number_tooltip").css("visibility","none");
			}
			var executive_order_date = document.forms["myForm"]["executive_order_date"].value;
			if (executive_order_date == "") {
				$("#executive_order_date_tooltip").css("display","block");
				$("#executive_order_date_tooltip").css("visibility","visible");

				$("#executive_order_date_tooltip2").css("display","none");
				$("#executive_order_date_tooltip2").css("visibility","none");

				$("#executive_order_date_tooltip3").css("display","none");
				$("#executive_order_date_tooltip3").css("visibility","none");
				
				$("#executive_order_date_tooltip4").css("display","none");
				$("#executive_order_date_tooltip4").css("visibility","none");
				
				return false;
			} else{
				$("#executive_order_date_tooltip").css("display","none");
				$("#executive_order_date_tooltip").css("visibility","none");
			}

			var isValidD = isValidDate(executive_order_date);
			if(!isValidD){
				$("#executive_order_date_tooltip4").css("display","block");
				$("#executive_order_date_tooltip4").css("visibility","visible");
			
				$("#executive_order_date_tooltip2").css("display","none");
				$("#executive_order_date_tooltip2").css("visibility","none");

				$("#executive_order_date_tooltip3").css("display","none");
				$("#executive_order_date_tooltip3").css("visibility","none");
				
				return false;
			}else{
				$("#executive_order_date_tooltip4").css("display","none");
				$("#executive_order_date_tooltip4").css("visibility","none");
			}
			
			if (executive_order_date!= "" && $('#executive_order_date').is('[readonly="readonly"]') == false){
				var str_array = executive_order_date.split('/');
				var hijriDate =  $.calendars.instance('ummalqura','ar').newDate();
				hijriDate = hijriDate.date(str_array[0], str_array[1], str_array[2]);
				if (hijriDate > todayHj) {
					$("#executive_order_date_tooltip2").css("display","block");
					$("#executive_order_date_tooltip2").css("visibility","visible");

					$("#executive_order_date_tooltip3").css("display","none");
					$("#executive_order_date_tooltip3").css("visibility","none");
					
					return false;
				} else{
					$("#executive_order_date_tooltip2").css("display","none");
					$("#executive_order_date_tooltip2").css("visibility","none");
				}
				if (last_state_date !='' && executive_order_date < last_state_date) {
					$("#executive_order_date_tooltip3").css("display","block");
					$("#executive_order_date_tooltip3").css("visibility","visible");
					return false;
				} else{
					$("#executive_order_date_tooltip3").css("display","none");
					$("#executive_order_date_tooltip3").css("visibility","none");
				}
				
			}

			var order_number = document.forms["myForm"]["order_number"].value;
			if (order_number != "" && (order_number<=0  || order_number.length > 15)) {
				$("#order_number_tooltip").css("display","block");
				$("#order_number_tooltip").css("visibility","visible");
				return false;
			} else{
				$("#order_number_tooltip").css("display","none");
				$("#order_number_tooltip").css("visibility","none");
			}
			
			var circle_number = document.forms["myForm"]["circle_number"].value;
			if (circle_number == "" || circle_number<=0  || circle_number.length > 15) {
				$("#circle_number_tooltip").css("display","block");
				$("#circle_number_tooltip").css("visibility","visible");
				return false;
			} else{
				$("#circle_number_tooltip").css("display","none");
				$("#circle_number_tooltip").css("visibility","none");
			}
	   
		}else if (action_code == "MAKE_DECISION_34"){
			var decision_34_number = document.forms["myForm"]["decision_34_number"].value;
			var decision_34_date = document.forms["myForm"]["decision_34_date"].value;
			if (decision_34_number != "" && ( decision_34_number <=0 || decision_34_number.length <3 || decision_34_number.length >15)) {
				$("#decision_34_number_tooltip").css("display","block");
				$("#decision_34_number_tooltip").css("visibility","visible");
				return false;
			} else{
				$("#decision_34_number_tooltip").css("display","none");
				$("#decision_34_number_tooltip").css("visibility","none");
			}
			if (decision_34_date == "") {
				$("#decision_34_date_tooltip").css("display","block");
				$("#decision_34_date_tooltip").css("visibility","visible");

				$("#decision_34_date_tooltip2").css("display","none");
				$("#decision_34_date_tooltip2").css("visibility","none");

				$("#decision_34_date_tooltip3").css("display","none");
				$("#decision_34_date_tooltip3").css("visibility","none");
				
				$("#decision_34_date_tooltip4").css("display","none");
				$("#decision_34_date_tooltip4").css("visibility","none");
					
				return false;
			} else{
				$("#decision_34_date_tooltip").css("display","none");
				$("#decision_34_date_tooltip").css("visibility","none");
			}
			
			var isValidD = isValidDate(decision_34_date);
			if(!isValidD){
				$("#decision_34_date_tooltip4").css("display","block");
				$("#decision_34_date_tooltip4").css("visibility","visible");
			
				$("#decision_34_date_tooltip2").css("display","none");
				$("#decision_34_date_tooltip2").css("visibility","none");

				$("#decision_34_date_tooltip3").css("display","none");
				$("#decision_34_date_tooltip3").css("visibility","none");
				
				return false;
			}else{
				$("#decision_34_date_tooltip4").css("display","none");
				$("#decision_34_date_tooltip4").css("visibility","none");
			}
			
			
			if (decision_34_date!= "" && $('#decision_34_date').is('[readonly="readonly"]') == false){
				var str_array = decision_34_date.split('/');
				var hijriDate =  $.calendars.instance('ummalqura','ar').newDate();
				hijriDate = hijriDate.date(str_array[0], str_array[1], str_array[2]);
				if (hijriDate > todayHj) {
					$("#decision_34_date_tooltip2").css("display","block");
					$("#decision_34_date_tooltip2").css("visibility","visible");

					$("#decision_34_date_tooltip3").css("display","none");
					$("#decision_34_date_tooltip3").css("visibility","none");
					
					return false;
				} else{
					$("#decision_34_date_tooltip2").css("display","none");
					$("#decision_34_date_tooltip2").css("visibility","none");
				}
				if (last_state_date !='' &&  decision_34_date < last_state_date) {
					$("#decision_34_date_tooltip3").css("display","block");
					$("#decision_34_date_tooltip3").css("visibility","visible");
					return false;
				} else{
					$("#decision_34_date_tooltip3").css("display","none");
					$("#decision_34_date_tooltip3").css("visibility","none");
				}
				
			}
			
		}else if (action_code == "ADVERTISE"){
			var invoice_file= document.forms["myForm"]["invoice_file"].value;
			var advertisement_date= document.forms["myForm"]["advertisement_date"].value;
			
			if (invoice_file== "") {
				$("#invoice_file_tooltip").css("display","block");
				$("#invoice_file_tooltip").css("visibility","visible");
				return false;
			} else{
				$("#invoice_file_tooltip").css("display","none");
				$("#invoice_file_tooltip").css("visibility","none");
			}
			if (advertisement_date == ""){
				$("#advertisement_date_tooltip").css("display","block");
				$("#advertisement_date_tooltip").css("visibility","visible");

				$("#advertisement_date_tooltip2").css("display","none");
				$("#advertisement_date_tooltip2").css("visibility","none");

				$("#advertisement_date_tooltip3").css("display","none");
				$("#advertisement_date_tooltip3").css("visibility","none");
					
				$("#advertisement_date_tooltip4").css("display","none");
				$("#advertisement_date_tooltip4").css("visibility","none");
				
				return false;
			}else{
				$("#advertisement_date_tooltip").css("display","none");
				$("#advertisement_date_tooltip").css("visibility","visible");
			}
			
			var isValidD = isValidDate(advertisement_date);
			if(!isValidD){
				$("#advertisement_date_tooltip4").css("display","block");
				$("#advertisement_date_tooltip4").css("visibility","visible");
			
				$("#advertisement_date_tooltip2").css("display","none");
				$("#advertisement_date_tooltip2").css("visibility","none");

				$("#advertisement_date_tooltip3").css("display","none");
				$("#advertisement_date_tooltip3").css("visibility","none");
				
				return false;
			}else{
				$("#advertisement_date_tooltip4").css("display","none");
				$("#advertisement_date_tooltip4").css("visibility","none");
			}
			
			
			if (advertisement_date!= "" && $('#advertisement_date').is('[readonly="readonly"]') == false){
				var str_array = advertisement_date.split('/');
				var hijriDate =  $.calendars.instance('ummalqura','ar').newDate();
				hijriDate = hijriDate.date(str_array[0], str_array[1], str_array[2]);
				if (hijriDate > todayHj) {
					$("#advertisement_date_tooltip2").css("display","block");
					$("#advertisement_date_tooltip2").css("visibility","visible");

					$("#advertisement_date_tooltip3").css("display","none");
					$("#advertisement_date_tooltip3").css("visibility","none");
					
					return false;
				} else{
					$("#advertisement_date_tooltip2").css("display","none");
					$("#advertisement_date_tooltip2").css("visibility","none");
				}
				if (last_state_date !='' && advertisement_date < last_state_date) {
					$("#advertisement_date_tooltip3").css("display","block");
					$("#advertisement_date_tooltip3").css("visibility","visible");
					return false;
				} else{
					$("#advertisement_date_tooltip3").css("display","none");
					$("#advertisement_date_tooltip3").css("visibility","none");
				}
			}
		}else if (action_code == "MAKE_DECISION_46"){
				var decision_46_date= document.forms["myForm"]["decision_46_date"].value;
				if (decision_46_date== "") {
					$("#decision_46_date_tooltip").css("display","block");
					$("#decision_46_date_tooltip").css("visibility","visible");

					$("#decision_46_date_tooltip2").css("display","none");
					$("#decision_46_date_tooltip2").css("visibility","none");

					$("#decision_46_date_tooltip3").css("display","none");
					$("#decision_46_date_tooltip3").css("visibility","none");
					
					$("#decision_46_date_tooltip4").css("display","none");
					$("#decision_46_date_tooltip4").css("visibility","none");
				
					return false;
				} else{
					$("#decision_46_date_tooltip").css("display","none");
					$("#decision_46_date_tooltip").css("visibility","none");
				}

			
			var isValidD = isValidDate(decision_46_date);
			if(!isValidD){
				$("#decision_46_date_tooltip4").css("display","block");
				$("#decision_46_date_tooltip4").css("visibility","visible");
			
				$("#decision_46_date_tooltip2").css("display","none");
				$("#decision_46_date_tooltip2").css("visibility","none");

				$("#decision_46_date_tooltip3").css("display","none");
				$("#decision_46_date_tooltip3").css("visibility","none");
				
				return false;
			}else{
				$("#decision_46_date_tooltip4").css("display","none");
				$("#decision_46_date_tooltip4").css("visibility","none");
			}

				if (decision_46_date!= "" && $('#decision_46_date').is('[readonly="readonly"]') == false){
					var str_array = decision_46_date.split('/');
					var hijriDate =  $.calendars.instance('ummalqura','ar').newDate();
					hijriDate = hijriDate.date(str_array[0], str_array[1], str_array[2]);
					if (hijriDate > todayHj) {
						$("#decision_46_date_tooltip2").css("display","block");
						$("#decision_46_date_tooltip2").css("visibility","visible");

						$("#decision_46_date_tooltip3").css("display","none");
						$("#decision_46_date_tooltip3").css("visibility","none");
						
						return false;
					} else{
						$("#decision_46_date_tooltip2").css("display","none");
						$("#decision_46_date_tooltip2").css("visibility","none");
					}
					if (last_state_date !='' && decision_46_date < last_state_date) {
						$("#decision_46_date_tooltip3").css("display","block");
						$("#decision_46_date_tooltip3").css("visibility","visible");
						return false;
					} else{
						$("#decision_46_date_tooltip3").css("display","none");
						$("#decision_46_date_tooltip3").css("visibility","none");
					}
				}	
			
			}else if (action_code == "CLOSE"){
				var closing_reason= document.forms["myForm"]["closing_reason"].value;
			if (closing_reason== "") {
				$("#closing_reason_tooltip").css("display","block");
				$("#closing_reason_tooltip").css("visibility","visible");
				return false;
			} else{
				$("#closing_reason_tooltip").css("display","none");
				$("#closing_reason_tooltip").css("visibility","none");
			}
		}else if (action_code == "NEED_MODIFICATIONS"){
			var notes = document.forms["myForm"]["notes"].value;
			if (notes== "") {
				$("#notes_tooltip").css("display","block");
				$("#notes_tooltip").css("visibility","visible");
				return false;
			} else{
				$("#notes_tooltip").css("display","none");
				$("#notes_tooltip").css("visibility","none");
			}
		}else if(action_code == "REASSIGN_TO_LAWYER"){
			var lawyer_id= document.forms["myForm"]["reassigned_lawyer_id"].value;
			if (lawyer_id== "") {
				$("#reassigned_lawyer_id_tooltip").css("display","block");
				$("#reassigned_lawyer_id_tooltip").css("visibility","visible");
					return false;
			} else{
				$("#reassigned_lawyer_id_tooltip").css("display","none");
				$("#reassigned_lawyer_id_tooltip").css("visibility","none");
			}
		}else if(action_code == "SUSPEND"){

			var suspend_date= document.forms["myForm"]["suspend_date"].value;
			var lawyer_id= document.forms["myForm"]["suspend_reason_code"].value;
					 
			if (lawyer_id== "") {
				$("#suspend_reason_code_tooltip").css("display","block");
				$("#suspend_reason_code_tooltip").css("visibility","visible");
					return false;
			} else{
				$("#suspend_reason_code_tooltip").css("display","none");
				$("#suspend_reason_code_tooltip").css("visibility","none");
			}

			if (suspend_date == "") {
				$("#suspend_date_tooltip").css("display","block");
				$("#suspend_date_tooltip").css("visibility","visible");

				$("#suspend_date_tooltip2").css("display","none");
				$("#suspend_date_tooltip2").css("visibility","none");

				$("#suspend_date_tooltip3").css("display","none");
				$("#suspend_date_tooltip3").css("visibility","none");
				
				return false;
			} else{
				$("#suspend_date_tooltip").css("display","none");
				$("#suspend_date_tooltip").css("visibility","none");
			}

			var isValidD = isValidDate(suspend_date);
			if(!isValidD){
				$("#suspend_date_tooltip3").css("display","block");
				$("#suspend_date_tooltip3").css("visibility","visible");
			
				$("#suspend_date_tooltip2").css("display","none");
				$("#suspend_date_tooltip2").css("visibility","none");

				
				return false;
			}else{
				$("#suspend_date_tooltip3").css("display","none");
				$("#suspend_date_tooltip3").css("visibility","none");
			}
			
			if (suspend_date!= "" && $('#suspend_date').is('[readonly="readonly"]') == false){
				var str_array = suspend_date.split('/');
				var hijriDate =  $.calendars.instance('ummalqura','ar').newDate();
				hijriDate = hijriDate.date(str_array[0], str_array[1], str_array[2]);
				if (hijriDate > todayHj) {
					$("#suspend_date_tooltip2").css("display","block");
					$("#suspend_date_tooltip2").css("visibility","visible");
					return false;
				} else{
					$("#suspend_date_tooltip2").css("display","none");
					$("#suspend_date_tooltip2").css("visibility","none");
				}
				
			}
			
		}else if(action_code == "SUSPEND_REQUEST"){

			var suspend_reason= document.forms["myForm"]["suspend_reason_code"].value;
					 
			if (suspend_reason== "") {
				$("#req_suspend_reason_code_tooltip").css("display","block");
				$("#req_suspend_reason_code_tooltip").css("visibility","visible");
					return false;
			} else{
				$("#req_suspend_reason_code_tooltip").css("display","none");
				$("#req_suspend_reason_code_tooltip").css("visibility","none");
			}
		}else if(action_code == "RESUME_REQUEST"){

			var suspend_reason= document.forms["myForm"]["resume_notes"].value;
					 
			if (suspend_reason== "") {
				$("#resume_notes_tooltip").css("display","block");
				$("#resume_notes_tooltip").css("visibility","visible");
					return false;
			} else{
				$("#resume_notes_tooltip").css("display","none");
				$("#resume_notes_tooltip").css("visibility","none");
			}	
		}else if(action_code == "RESUME" || action_code == "REOPEN"){
			var return_date = document.forms["myForm"]["return_date"].value;
			if (return_date == "") {
				$("#return_date_tooltip").css("display","block");
				$("#return_date_tooltip").css("visibility","visible");

				$("#return_date_tooltip2").css("display","none");
				$("#return_date_tooltip2").css("visibility","none");

				$("#return_date_tooltip3").css("display","none");
				$("#return_date_tooltip3").css("visibility","none");
				
				return false;
			} else{
				$("#return_date_tooltip").css("display","none");
				$("#return_date_tooltip").css("visibility","none");
			}

			var isValidD = isValidDate(return_date);
			if(!isValidD){
				$("#return_date_tooltip2").css("display","none");
				$("#return_date_tooltip2").css("visibility","none");

				$("#return_date_tooltip3").css("display","none");
				$("#return_date_tooltip3").css("visibility","none");
				
				return false;
			}else{
				$("#return_date_tooltip4").css("display","none");
				$("#return_date_tooltip4").css("visibility","none");
			}
			
			if (return_date_date!= "" && $('#return_date_date').is('[readonly="readonly"]') == false){
				var str_array = return_date.split('/');
				var hijriDate =  $.calendars.instance('ummalqura','ar').newDate();
				hijriDate = hijriDate.date(str_array[0], str_array[1], str_array[2]);
				if (hijriDate > todayHj) {
					$("#return_date_tooltip2").css("display","block");
					$("#return_date_tooltip2").css("visibility","visible");

					$("#return_date_tooltip3").css("display","none");
					$("#return_date_tooltip3").css("visibility","none");
					
					return false;
				} else{
					$("#return_date_tooltip2").css("display","none");
					$("#return_date_tooltip2").css("visibility","none");
				}
				if (last_state_date !='' && return_date < last_state_date) {
					$("#return_date_tooltip3").css("display","block");
					$("#return_date_tooltip3").css("visibility","visible");
					return false;
				} else{
					$("#return_date_tooltip3").css("display","none");
					$("#return_date_tooltip3").css("visibility","none");
				}
			}
		}
		nullifyUploadFields();
	}else{
		return false;
	}
	$('#loading').removeClass('hidden');
}

function check_uploading_files(){
	if((_("consignment_image_uploadingFiles") !== null && _("consignment_image_uploadingFiles") !== undefined && _("consignment_image_uploadingFiles").value == "no_files")&&
	   (_("advertisement_file_uploadingFiles") !== null && _("advertisement_file_uploadingFiles") !== undefined && _("advertisement_file_uploadingFiles").value == "no_files") &&
	   (_("invoice_file_uploadingFiles") !== null && _("invoice_file_uploadingFiles") !== undefined && _("invoice_file_uploadingFiles").value == "no_files") &&
	   (_("referral_paper_uploadingFiles") !== null && _("referral_paper_uploadingFiles") !== undefined && _("referral_paper_uploadingFiles").value == "no_files") &&
	   (_("suspend_file_uploadingFiles") !== null && _("suspend_file_uploadingFiles") !== undefined && _("suspend_file_uploadingFiles").value == "no_files")
	   ){
			return true;
	   }else{
		   bootbox.dialog({
			   message: '<p><i class="fa fa-spin fa-spinner"></i> '+"رجاء انتظار تحميل المرفقات أولا ثم النقر على الزر"+' </p>',
				buttons: {
					confirm: {
						label: "موافق",
						className: 'green'
					}
				}
			});
		   return false;
	   }
}

function nullifyUploadFields(){
	if(_("consignment_image_hidden") !== null && _("consignment_image_hidden") !== undefined &&  _("consignment_image_hidden").value != ""){
		_("consignment_image").value = ""; 
	}
	if(_("advertisement_file_hidden") !== null && _("advertisement_file_hidden") !== undefined && _("advertisement_file_hidden").value != ""){
		_("advertisement_file").value = "";
	}
	if(_("invoice_file_hidden") !== null && _("invoice_file_hidden") !== undefined && _("invoice_file_hidden").value != ""){
		_("invoice_file").value = ""; 
	}
	if(_("referral_paper_hidden") !== null && _("referral_paper_hidden") !== undefined && _("referral_paper_hidden").value != ""){
		_("referral_paper").value = ""; 
	}
	if(_("suspend_file_hidden") !== null && _("suspend_file_hidden") !== undefined && _("suspend_file_hidden").value != ""){
		_("suspend_file").value = ""; 
	}
}


function numberKeyDown(e) {
	var keynum;
    if(window.event) { // IE                    
      keynum = e.keyCode;
    } else if(e.which){ // Netscape/Firefox/Opera                   
      keynum = e.which;
    }

 // Allow: backspace, delete, tab, escape, enter, up and down.
    if ($.inArray(keynum, [46, 8, 9, 27, 13,0,39,38,40]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (keynum === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
        // Allow: Ctrl+C, Command+C
        (keynum === 67 && (e.ctrlKey === true || e.metaKey === true)) || 
        // Allow: Ctrl+X, Command+X
        (keynum === 88 && (e.ctrlKey === true || e.metaKey === true)) ||
    	 // Allow: Ctrl+V, Command+V
        (keynum === 86 && (e.ctrlKey === true || e.metaKey === true)) || 
        // Allow: Ctrl+Z, Command+Z
        (keynum === 90 && (e.ctrlKey === true || e.metaKey === true)) ||
         // Allow: home, end, left, right, down, up
        (keynum >= 35 && keynum <= 37)) {
             // let it happen, don't do anything
             return;
    }
    
    if(((e.shiftKey || (keynum < 48 || keynum > 57)) && (keynum < 96 || keynum > 105)) || (keynum >= 38 && keynum <= 40)){
		 e.preventDefault();
	}
}

function textKeyDown(e,elem) {
	var keynum;
    if(window.event) { // IE                    
      keynum = e.keyCode;
    } else if(e.which){ // Netscape/Firefox/Opera                   
      keynum = e.which;
    }
	//alert (keynum);
  // Allow: backspace, delete, tab, escape, enter, up and down.
    if ($.inArray(keynum, [46, 8, 9, 27, 13, 110, 190,39,38,40]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (keynum === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
        // Allow: Ctrl+C, Command+C
        (keynum === 67 && (e.ctrlKey === true || e.metaKey === true)) || 
        // Allow: Ctrl+X, Command+X
        (keynum === 88 && (e.ctrlKey === true || e.metaKey === true)) || 
         // Allow: home, end, left, right, down, up
        (keynum >= 35 && keynum <= 37)) {
             // let it happen, don't do anything
             return;
    }
	
    if(((keynum >= 48 && keynum <= 57) || (keynum >= 96 && keynum <= 105)) || (keynum >= 38 && keynum <= 40)){
		 e.preventDefault();
	}
}

</script>
<meta charset="utf-8" />
<title><?php echo TITLE; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin RTL Theme #3 for dashboard & statistics"
	name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css"
	rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/global/plugins/morris/morris.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/fullcalendar/fullcalendar.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/jqvmap.css"
	rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?=base_url()?>assets/global/css/components-rtl.min.css"
	rel="stylesheet" id="style_components" type="text/css" />
<link href="<?=base_url()?>assets/global/css/plugins-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link
	href="<?=base_url()?>assets/layouts/layout3/css/layout-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/layouts/layout3/css/themes/default-rtl.min.css"
	rel="stylesheet" type="text/css" id="style_color" />
<link
	href="<?=base_url()?>assets/layouts/layout3/css/custom-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon"
	href="<?=base_url()?>assets/layouts/layout3/img/logo-default.jpg" />

<link
	href="<?=base_url()?>assets/global/plugins/bootstrap/css/ccm_css.css"
	rel="stylesheet" type="text/css" />

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
function handlingTextNumberViolation (item){
	var attr = item.attr('act');
    			 
    			var val = parseArabic(item.val());
    			//alert(val);
				var matches = val.match(/\d+/g);
				
				if (item.attr("class").indexOf("date")>=0 && val!="" &&!dateValidation(val))
					$('#valid_span_'+item.attr('name')).css("display","block");
				else if ((item.attr("class").indexOf("date")<0 && (matches != null && typeof attr == typeof undefined))||(typeof attr !== typeof undefined && !$.isNumeric(val) && val!="")) {
					$('#valid_span_'+item.attr('name')).css("display","block");
					
				}else /*if(!violated || (violated && val!="")||item.attr("type") == "text")*/{
					$('#valid_span_'+item.attr('name')).css("display","none");
					/*if(item.attr("type") == "number"){
						violated  = false;
					}*/
				}
}
$(document).ready(function(){
	$(":input[type=text]").on('input', function() {
   		handlingTextNumberViolation($(this)); 
	});
	$(':input[type=text]').each(function(){
        var attr = $(this).attr('act');
	 if($(this).attr("class") && $(this).attr("class").indexOf(" date")>=0)
		 $(this).after('<span id="valid_span_'+$(this).attr('name')+'" style="display:none;color:red;"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>');
	 else if (typeof attr == typeof undefined && $(this).attr("class") && $(this).attr("class").indexOf("no-validate")<0 )
	 	$(this).after('<span id="valid_span_'+$(this).attr('name')+'" style="display:none;color:red;"><?php echo NO_NUMBERS; ?></span>');
	 else if (typeof attr !== typeof undefined){
		// $(this).attr("oninvalid", "setCustomValidity('<?php echo NO_TEXT; ?>')");
		// $(this).attr("onchange", "setCustomValidity('')");
		 $(this).after('<span id="valid_span_'+$(this).attr('name')+'" style="display:none;color:red;"><?php echo NO_TEXT; ?></span>');
	 }
});
	 $(':input[type=text]').on('keydown', function(e) {
		 var attr = $(this).attr('act');
		 
			 if (typeof attr == typeof undefined && $(this).attr("class").indexOf("no-validate")<0 && $(this).attr("class").indexOf("date")<0)
		    	textKeyDown(e,$(this));
			 else if (typeof attr !== typeof undefined){
				 numberKeyDown(e,$(this));
				
	/*$(this).bind("paste",function(e) {
	  			  var pastedData = e.originalEvent.clipboardData.getData('text');
	  			  
	    //alert(pastedData);
	    			if(!$.isNumeric(pastedData)){
	    			   $('#valid_span_'+$(this).attr('name')).css("display","block");
	    			   violated  = true;
	    			   // to support safari as it save last legal value
	    			   
	    			   
	    			   $(this).val("");
	   			   //var value= pastedData.replace(/[^0-9\.]/g, '');
	   			   //value= value.replace('e', '');
	   			   //value= value.replace('.', '');
	   			   //value= value.replace('.', '');
	   			   //$(this).val(value);
	   			   //e.preventDefault();
	   			   } else {
	   			    $('#valid_span_'+$(this).attr('name')).css("display","none");
	   			    violated = false;
	   			   }
	   			});*/


			}
		    	
		    });

  $('#myOptions').change(function() {
    var val = $("#myOptions option:selected").val();
    window.location="<?=site_url('case_management/manage_case/'.$case_id.'/'.$action_code)?>"+"/"+val;
  });

  $('form').on('focus', 'input[type=number]', function(e) {
  	$(this).on('wheel', function(e) {
  		e.preventDefault();
  	});
      });

      $('form').on('blur', 'input[type=number]', function(e) {
      	$(this).off('wheel');
      });


      $('form').on('focus', 'input[type=number]', function (e) {
  	$(this).on('mousewheel.disableScroll', function (e) {
  		e.preventDefault();
  	});
      });
      $('form').on('blur', 'input[type=number]', function (e) {
      	$(this).off('mousewheel.disableScroll');
      });	 

});
</script>
<link rel="stylesheet" type="text/css"
	href="<?=base_url()?>assets/css/jquery.calendars.picker.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?=base_url()?>assets/js/jquery.plugin.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery.calendars.all.js"></script>
<script src="<?=base_url()?>assets/js/jquery.calendars.plus.js"></script>
<script src="<?=base_url()?>assets/js/jquery.calendars.ummalqura.js"></script>
<script src="<?=base_url()?>assets/js/jquery.calendars.ummalqura-ar.js"></script>
<script>
  $( function() {
    $( ".date" ).calendarsPicker({calendar: $.calendars.instance('ummalqura','ar')});
  } );

function _(el){
    return document.getElementById(el);
}
function uploadFile(name, statusName, progressBarName, field_name, case_id, fragment_div){
    
	var uploadingFiles_id = field_name + "_uploadingFiles";
	_(progressBarName).value = 0;
	var files_count = _(name).files.length;
	var check_undefined = '0';
	for (i = 0; i < files_count; i++) {
		if(typeof _(name).files[i] === "undefined"){
			check_undefined='1';
			break;
		}
	}
 if(check_undefined == '1' || files_count<=0) {
	 _(statusName).innerHTML = "خطأ: برجاء إدخال الملف اولا";
		_(progressBarName).value = 0;
 } else {
		_(fragment_div).style.display = "block";
		_(progressBarName).style.display = "inline";
		_(statusName).style.display = "inline";
		//ajax 1
	    $.ajax({
	        url: '<?php echo site_url('case_management/upload_file_get_max'); ?>',
	        type: 'POST',
	        dataType: 'json',
	        success: function(response) {
				var sizelimit = response.max_size;
		     	var check_exceed_limit = '0';
				for (i = 0; i < files_count; i++) {
					if(sizelimit < _(name).files[i].size){
						check_exceed_limit='1';
						break;
					}
				}
	         if(check_exceed_limit == '0') {
	             var formdata = new FormData();
					for (i = 0; i < files_count; i++) {
						formdata.append("file"+i, _(name).files[i]);
						//formdata.append("size"+i, _(name).files[i].size);
					}
					formdata.append("field_name", field_name);
					formdata.append("case_id", case_id);
	             var ajax = new XMLHttpRequest();

	             ajax.upload.addEventListener("progress", function(event){
							_(uploadingFiles_id).value = "";
						    //_(loadedName).innerHTML = "Uploaded "+event.loaded+" bytes of "+event.total;
							var percent = (event.loaded / event.total) * 100;
							
							if(percent != 100){
								console.log("percent: " + percent+" statusName:"+statusName);
								_(progressBarName).value = Math.round(percent);
								_(statusName).innerHTML = Math.round(percent)+"%";
							}
					}, false);
	             ajax.addEventListener("load", function(event){
						if(event.target.responseText !='0'){
							var hidden_field = field_name+"_hidden";
							_(hidden_field).value = event.target.responseText;
							console.log("hidden_field: " + hidden_field+" statusName:"+statusName);
							_(statusName).innerHTML = '100%';
							_(progressBarName).value = 100;
							_(uploadingFiles_id).value = "no_files";
						}else{
							_(statusName).innerHTML = "خطأ: هناك مشكلة في تحميل الملف";
						}
						
					}, false);
	             ajax.addEventListener("error", function(event){
						_(uploadingFiles_id).value = "no_files";
						_(statusName).innerHTML = "خطأ: هناك مشكلة في تحميل الملف";
					}, false);
	             ajax.addEventListener("abort", function(event){
						_(statusName).innerHTML = "Upload Aborted";
						_(uploadingFiles_id).value = "no_files";
					}, false);
	  	var path= "<?php echo base_url(); ?>" + "index.php/case_management/upload_file_progress";
	             ajax.open("POST", path);
	             ajax.send(formdata);
	             var close_id = "close_"+field_name;
				 
				 var has_delete_listener = $("span[id="+close_id+"]").hasClass ("dummyClass");
	             if(has_delete_listener == false){
					 _(close_id).addEventListener("click", function(){
						 bootbox.confirm({
							 title: "حذف الملف",
							 message:"هل انت متأكد من حذف الملف؟",
							buttons: {
								confirm: {
									label: "نعم",
								className: 'custom-green-button'
								},
								cancel: {
									label: "لا",
									className: 'custom-red-button'
								}
							},
							callback: function (result) {
								if(result){
									ajax.abort();
									deleteFile(field_name, (field_name+'_hidden'), progressBarName, statusName, fragment_div);
									_(uploadingFiles_id).value = "no_files";
								}
							}
						});
						
					$("span[id="+close_id+"]").addClass('dummyClass');
					/*var r = confirm("هل انت متأكد من حذف الملف؟");
					if (r == true) {
						ajax.abort();
						deleteFile(field_name, (field_name+'_hidden'), progressBarName, statusName, fragment_div);
						_(uploadingFiles_id).value = "no_files";
					}*/
				});
				}
	         } else {
	        	 var sizewarn = "خطأ: الملف حجمه اكبر من المتاح للتحميل ";
	             sizewarn += sizelimit/(1024*1024);
	             sizewarn += " ميجا";
	             _(statusName).innerHTML = sizewarn;
	             _(progressBarName).value = 0;

	         }
	           
	        }
	    });
		
	}
}

function deleteFile(field_name, hidden_field_name, progressBarName, statusName, fragment_div){

	_(field_name).value = "";
	_(hidden_field_name).value = "";
	_(progressBarName).value = 0;
	_(progressBarName).style.display = "none";
	_(statusName).innerHTML = "";
	_(statusName).style.display = "none";
	_(fragment_div).style.display = "none";

}	
</script>
<style type="text/css">
.fragment {
	font-size: 12px;
	font-family: tahoma;
	height: 30px;
	width: 187px;
	border: 1px solid #ccc;
	color: #555;
	display: block;
	padding: 5px;
	box-sizing: border-box;
	text-decoration: none;
	background-color: rgba(85, 85, 85, 0.09) !important;
}

.custom-green-button {
	color: #FFF;
	background-color: #007c5a;
	border-color: #007c5a;
	float: right;
}

.custom-red-button {
	color: #fff;
	background-color: #ed6b75;
	border-color: #ea5460;
	margin-right: 1%;
}
</style>
</head>
<!-- END HEAD -->
<body class="page-container-bg-solid">
<div id="loading" class="hidden">
	  <img id="loading-image" src="<?php echo base_url(); ?>images/Ajax-loader2.gif" alt="Loading..." />
	</div>
	<div class="portlet box green">
		<div class="portlet-body form">
			<!-- BEGIN FORM-->
			<form id="myForm" role="form" method="post"
				<?php if($action_code == 'SUSPEND_REQUEST') {?>
				onsubmit="javascript:return checkValid('');"
				action="<?=site_url('case_management/do_case_request/'.$case_id)?>"
				<?php }elseif($action_code == 'RESUME_REQUEST'){?>
				onsubmit="javascript:return checkValid('');"
				action="<?=site_url('case_management/do_case_request/'.$case_id.'/R')?>"
				<?php }else {?>
				onsubmit="javascript:return checkValid('<?php echo $action_code; ?>');"
				action="<?=site_url('case_management/manage_case/'.$case_id.'/'.$action_code)?>"
				<?php }?> class="form-horizontal" enctype="multipart/form-data">
				<input type="hidden" id="consignment_image_uploadingFiles"
					name="consignment_image_uploadingFiles" value="no_files"> <input
					type="hidden" id="referral_paper_uploadingFiles"
					name="referral_paper_uploadingFiles" value="no_files"> <input
					type="hidden" id="advertisement_file_uploadingFiles"
					name="advertisement_file_uploadingFiles" value="no_files"> <input
					type="hidden" id="invoice_file_uploadingFiles"
					name="invoice_file_uploadingFiles" value="no_files"> <input
					type="hidden" id="suspend_file_uploadingFiles"
					name="suspend_file_uploadingFiles" value="no_files">
				<div class="form-body">

					<input type="hidden" id="last_state_date_value" value="">
								
										<?php if($action_code == "ASSIGN_TO_LAWYER"){?>
		

					<div class="form-group">
						<label class="col-md-3 control-label"><?php echo REGION?></label>
						<div class="col-md-4">
							<select id="myOptions" name="region_id" class="form-control "
								class="form-control">
					  	<?php
											if ($available_regions) {
												for($i = 0; $i < count ( $available_regions ); $i ++) {
													$region_id_s = $available_regions [$i] ["region_id"];
													$region_name = $available_regions [$i] ["name"];
													?>
							<option value="<?=$region_id_s ?>"
									<?php if($region_id == $region_id_s){?> selected="selected"
									<?php }?>> <?=$region_name ?></option>
						<?php
												}
											} else {
												?>
							<option value="" selected></option>
						<?php
											}
											?>		
					</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label"><?php echo LAWYER ?><span
							class="required"> * </span></label>
						<div id="lawyer_select" class="col-md-4">
							<select class="form-control " name="lawyer_id" id="lawyer_id"
								class="form-control"
								onfocus="javascript:removeTooltip('lawyer_id_tooltip');">
					  	<?php
											if ($lawyers_per_region) {
												for($i = 0; $i < count ( $lawyers_per_region ); $i ++) {
													$lawyer_id = $lawyers_per_region [$i] ["user_id"];
													$lawyer_name = $lawyers_per_region [$i] ["first_name"] . ' ' . $lawyers_per_region [$i] ["last_name"];
													?>
							<option value="<?=$lawyer_id?>"> <?=$lawyer_name ?></option>
						<?php
												}
											} else {
												?>
							<option value="" selected></option>
						<?php
											}
											?>		
					</select> <span id="lawyer_id_tooltip" class="tooltiptext"
								style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label"><?php echo CONSIGNMENT_IMAGE; ?></label>
						<div class="col-md-2">
							<input type="file" class="form-control " id="consignment_image"
								name="consignment_image"
								onchange="uploadFile('consignment_image', 'consignment_image_status', 'consignment_image_progressBar','consignment_image', '<?php echo $case_id;?>', 'consignment_image_fragment')">
							<input type="hidden" id="consignment_image_hidden"
								name="consignment_image_hidden">

						</div>
						<div class="col-md-2">
							<div id="consignment_image_fragment" class="fragment"
								style="display: none;">
								<span
									style="float: left; display: inline-block; color: #0c0b0b80; cursor: pointer !important; font-weight: bold;"
									id="close_consignment_image">&#10006;</span>
								<progress style="display: none;"
									id="consignment_image_progressBar" value="0" max="100"
									style="width:100px;"></progress>
								<span id="consignment_image_status"></span>
							</div>
						</div>
					</div>

					<div class="form-actions">
						<div class="row">
							<div class="col-md-offset-3 col-md-9">
								<button type="submit" class="btn btn-circle green"><?php
											echo ASSIGNED_TO_LAWYER;
											
											?></button>
							</div>
						</div>
					
											<?php }else if($action_code == "REGISTER"){?>
													
					<div class="form-group">
							<label class="col-md-3 control-label"><?php echo EXECUTIVE_ORDER_NUMBER; ?><span
								class="required"> * </span></label>
							<div class="col-md-4">
								<input type="text" act="yes" class="form-control no-spinners"
									onkeydown="numberKeyDown(event)"
									onkeydown="numberKeyDown(event)" name="executive_order_number"
									id="executive_order_number"
									onfocus="javascript:removeTooltip('executive_order_number_tooltip');">
								<span id="executive_order_number_tooltip" class="tooltiptext"
									style="display: none; color: red"><?php echo CASE_NUMBER_INVALID; ?></span>

							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label"><?php echo EXECUTIVE_ORDER_DATE; ?><span
								class="required"> * </span></label>
							<div class="col-md-4">

								<input type="text" class="form-control date"
									name="executive_order_date" id="executive_order_date"> <span
									id="executive_order_date_tooltip" class="tooltiptext"
									style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
								<span id="executive_order_date_tooltip2" class="tooltiptext"
									style="display: none; color: red"><?php echo DATE_CAN_NOT_BE_GREATER_THAN_TODAY; ?></span>
								<span id="executive_order_date_tooltip3" class="tooltiptext"
									style="display: none; color: red"><?php echo DATE_CAN_NOT_BE_LESS_THAN_LAST_STATE_DATE.' : '. $last_state_date; ?></span>
								<span id="executive_order_date_tooltip4" class="tooltiptext"
									style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>


							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label"><?php echo ORDER_NUMBER; ?> </label>
							<div class="col-md-4">
								<input type="text" act="yes" class="form-control no-spinners"
									onkeydown="numberKeyDown(event)" name="order_number"
									id="order_number"
									onfocus="javascript:removeTooltip('order_number_tooltip');"> <span
									id="order_number_tooltip" class="tooltiptext"
									style="display: none; color: red"><?php echo ORDER_NUMBER_INVALID; ?></span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label"><?php echo CIRCLE_NUMBER; ?><span
								class="required"> * </span></label>
							<div class="col-md-4">
								<input type="text" act="yes" class="form-control no-spinners"
									onkeydown="numberKeyDown(event)" name="circle_number"
									id="circle_number"
									onfocus="javascript:removeTooltip('circle_number_tooltip');"> <span
									id="circle_number_tooltip" class="tooltiptext"
									style="display: none; color: red"><?php echo CIRCLE_NUMBER_INVALID; ?></span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label"><?php echo REFERRAL_PAPER; ?></label>
							<div class="col-md-2">
								<input type="file" class="form-control " name="referral_paper"
									id="referral_paper"
									onchange="uploadFile('referral_paper', 'referral_paper_status', 'referral_paper_progressBar','referral_paper', '<?php echo $case_id;?>', 'referral_paper_fragment')">
								<input type="hidden" id="referral_paper_hidden"
									name="referral_paper_hidden">

							</div>
							<div class="col-md-2">
								<div id="referral_paper_fragment" class="fragment"
									style="display: none;">
									<span
										style="float: left; display: inline-block; color: #0c0b0b80; cursor: pointer !important; font-weight: bold;"
										id="close_referral_paper">&#10006;</span>
									<progress style="display: none;"
										id="referral_paper_progressBar" value="0" max="100"
										style="width:100px;"></progress>
									<span id="referral_paper_status"></span>
								</div>
							</div>
						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									<button type="submit" class="btn btn-circle green"><?php
											echo REGISTERED;
											
											?></button>
								</div>
							</div>
						</div>
				
											<?php }else if($action_code == "MAKE_DECISION_34"){?>
											
					<div class="form-group">
							<label class="col-md-3 control-label"><?php echo DECISION_34_NUMBER; ?></label>
							<div class="col-md-4">
								<input type="text" act="yes" class="form-control no-spinners"
									onkeydown="numberKeyDown(event)" name="decision_34_number"
									id="decision_34_number"
									onfocus="javascript:removeTooltip('decision_34_number_tooltip');">
								<span id="decision_34_number_tooltip" class="tooltiptext"
									style="display: none; color: red"><?php echo CASE_NUMBER_INVALID; ?></span>


							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label"><?php echo DECISION_34_DATE; ?><span
								class="required"> * </span></label>
							<div class="col-md-4">

								<input type="text" class="form-control date"
									name="decision_34_date" id="decision_34_date"> <span
									id="decision_34_date_tooltip" class="tooltiptext"
									style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
								<span id="decision_34_date_tooltip2" class="tooltiptext"
									style="display: none; color: red"><?php echo DATE_CAN_NOT_BE_GREATER_THAN_TODAY; ?></span>
								<span id="decision_34_date_tooltip3" class="tooltiptext"
									style="display: none; color: red"><?php echo DATE_CAN_NOT_BE_LESS_THAN_LAST_STATE_DATE.' : '. $last_state_date; ?></span>
								<span id="decision_34_date_tooltip4" class="tooltiptext"
									style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>




							</div>
						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									<button type="submit" class="btn btn-circle green"><?php
											echo DECISION_34;
											
											?></button>
								</div>
							</div>
						</div>
				
											<?php }else if($action_code == "ADVERTISE"){?>
														<!-- BEGIN FORM-->

						<div class="form-group">
							<label class="col-md-3 control-label"><?php echo ADVERTISEMENT_FILE; ?></label>
							<div class="col-md-2">
								<input type="file" class="form-control "
									name="advertisement_file" id="advertisement_file"
									onchange="uploadFile('advertisement_file', 'advertisement_file_status', 'advertisement_file_progressBar','advertisement_file', '<?php echo $case_id;?>', 'advertisement_file_fragment')">
								<input type="hidden" id="advertisement_file_hidden"
									name="advertisement_file_hidden"> <input type="hidden"
									id="advertisement_file_value"
									value="<?= $case_details["advertisement_file"]?>">

							</div>
							<div class="col-md-2">
								<div id="advertisement_file_fragment" class="fragment"
									style="display: none;">
									<span
										style="float: left; display: inline-block; color: #0c0b0b80; cursor: pointer !important; font-weight: bold;"
										id="close_advertisement_file">&#10006;</span>
									<progress style="display: none;"
										id="advertisement_file_progressBar" value="0" max="100"
										style="width:100px;"></progress>
									<span id="advertisement_file_status"></span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label"><?php echo INVOICE_FILE; ?><span
								class="required"> * </span></label>

							<div class="col-md-2">
								<input type="file" class="form-control " id="invoice_file"
									name="invoice_file"
									onfocus="javascript:removeTooltip('invoice_file_tooltip');"
									onchange="uploadFile('invoice_file', 'invoice_file_status', 'invoice_file_progressBar','invoice_file', '<?php echo $case_id;?>', 'invoice_file_fragment')">
								<input type="hidden" id="invoice_file_hidden"
									name="invoice_file_hidden"> <input type="hidden"
									id="invoice_file_value"
									value="<?= $case_details["invoice_file"]?>"> <span
									id="invoice_file_tooltip" class="tooltiptext"
									style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
							</div>
							<div class="col-md-2">
								<div id="invoice_file_fragment" class="fragment"
									style="display: none;">
									<span
										style="float: left; display: inline-block; color: #0c0b0b80; cursor: pointer !important; font-weight: bold;"
										id="close_invoice_file">&#10006;</span>
									<progress style="display: none;" id="invoice_file_progressBar"
										value="0" max="100" style="width:100px;"></progress>
									<span id="invoice_file_status"></span>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label"><?php echo ADVERTISMENT_DATE; ?><span
								class="required"> * </span></label>
							<div class="col-md-4">
								<input type=text " placeholder="" class="form-control date"
									name="advertisement_date" id="advertisement_date"> <span
									id="advertisement_date_tooltip" class="tooltiptext"
									style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
								<span id="advertisement_date_tooltip2" class="tooltiptext"
									style="display: none; color: red"><?php echo DATE_CAN_NOT_BE_GREATER_THAN_TODAY; ?></span>
								<span id="advertisement_date_tooltip3" class="tooltiptext"
									style="display: none; color: red"><?php echo DATE_CAN_NOT_BE_LESS_THAN_LAST_STATE_DATE.' : '. $last_state_date;?></span>
								<span id="advertisement_date_tooltip4" class="tooltiptext"
									style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>
							</div>
						</div>

						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									<button type="submit" class="btn btn-circle green"><?php
											echo PUBLIC_ADVERTISED;
											
											?></button>
								</div>
							</div>
						</div>
				
											<?php }else if($action_code == "MAKE_DECISION_46"){?>
											
					<div class="form-group">
							<label class="col-md-3 control-label"><?php echo DECISION_46_DATE; ?><span
								class="required"> * </span></label>
							<div class="col-md-4">
								<input type=text " placeholder="" class="form-control date"
									name="decision_46_date" id="decision_46_date"> <span
									id="decision_46_date_tooltip" class="tooltiptext"
									style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
								<span id="decision_46_date_tooltip2" class="tooltiptext"
									style="display: none; color: red"><?php echo DATE_CAN_NOT_BE_GREATER_THAN_TODAY; ?></span>
								<span id="decision_46_date_tooltip3" class="tooltiptext"
									style="display: none; color: red"><?php echo DATE_CAN_NOT_BE_LESS_THAN_LAST_STATE_DATE.' : '. $last_state_date;?></span>
								<span id="decision_46_date_tooltip4" class="tooltiptext"
									style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>
							</div>
						</div>

						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									<button type="submit" class="btn btn-circle green"><?php
											echo DECISION_46;
											
											?></button>
								</div>
							</div>
						</div>
				
											<?php }else if($action_code == "CLOSE"){?>
											<!-- BEGIN FORM-->

						<div class="form-group">
							<label class="col-md-3 control-label"><?php echo CLOSING_REASON; ?><span
								class="required"> * </span></label>
							<div class="col-md-4">
								<select class="form-control " name="closing_reason"
									id="closing_reason"
									onfocus="javascript:removeTooltip('closing_reason_tooltip');">
					  	<?php
											if ($close_reasons) {
												for($i = 0; $i < count ( $close_reasons ); $i ++) {
													$close_reason = $close_reasons [$i] ["close_reason"];
													?>
							<option value="<?=$close_reason?>"> <?=$close_reason?></option>
						<?php
												}
											} else {
												?>
							<option value="" selected></option>
						<?php
											}
											?>		
					</select> <span id="closing_reason_tooltip" class="tooltiptext"
									style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>

							</div>
						</div>

						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									<button type="submit" class="btn btn-circle green"><?php
											echo CLOSED;
											
											?></button>
								</div>
							</div>
						</div>
				
											<?php }else if($action_code == "NEED_MODIFICATIONS"){?>
											<!-- BEGIN FORM-->

						<div class="form-group">
							<label class="col-md-3 control-label"><?php echo NOTES; ?><span
								class="required"> * </span></label>
							<div class="col-md-4">
								<textarea class="form-control " rows="4" cols="50" name="notes"
									id="notes" form="myForm"
									onfocus="javascript:removeTooltip('notes_tooltip');"></textarea>
								<span id="notes_tooltip" class="tooltiptext"
									style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>

							</div>
						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									<button type="submit" class="btn btn-circle green"><?php
											echo NEED_MODIFICATION;
											
											?></button>
								</div>
							</div>
						</div>
											<?php } else if($action_code == "REASSIGN_TO_LAWYER"){?>
		

					<div class="form-group">
							<label class="col-md-3 control-label"><?php echo REGION?></label>
							<div class="col-md-4">
								<select id="myOptions" class="form-control " name="region_id"
									class="form-control">
					  	<?php
											if ($available_regions) {
												for($i = 0; $i < count ( $available_regions ); $i ++) {
													$region_id_s = $available_regions [$i] ["region_id"];
													$region_name = $available_regions [$i] ["name"];
													?>
							<option value="<?=$region_id_s ?>"
										<?php if($region_id == $region_id_s){?> selected="selected"
										<?php }?>> <?=$region_name ?></option>
						<?php
												}
											} else {
												?>
							<option value="" selected></option>
						<?php
											}
											?>		
					</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label"><?php echo LAWYER ?><span
								class="required"> * </span></label>
							<div id="lawyer_select" class="col-md-4">
								<select class="form-control " name="reassigned_lawyer_id"
									id="reassigned_lawyer_id" class="form-control"
									onfocus="javascript:removeTooltip('reassigned_lawyer_id_tooltip');">
					  	<?php
											if ($lawyers_per_region) {
												for($i = 0; $i < count ( $lawyers_per_region ); $i ++) {
													$lawyer_id = $lawyers_per_region [$i] ["user_id"];
													if ($lawyer_id == $assigned_lawyer) {
														continue;
													}
													$lawyer_name = $lawyers_per_region [$i] ["first_name"] . ' ' . $lawyers_per_region [$i] ["last_name"];
													?>
							<option value="<?=$lawyer_id?>"> <?=$lawyer_name ?></option>
						<?php
												}
											} else {
												?>
							<option value="" selected></option>
						<?php
											}
											?>		
					</select> <span id="reassigned_lawyer_id_tooltip"
									class="tooltiptext" style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
							</div>
						</div>

						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									<button type="submit" class="btn btn-circle green"><?php
											echo REASSIGNED_TO_LAWYER;
											
											?></button>
								</div>
							</div>

						</div>
											<?php }else if($action_code == "SUSPEND"){?>
							
								<div class="form-group">
							<label class="col-md-3 control-label"><?php echo SUSPEND_DATE; ?><span
								class="required"> * </span></label>
							<div class="col-md-4">

								<input type="text" class="form-control date" name="suspend_date"
									id="suspend_date"> <span id="suspend_date_tooltip"
									class="tooltiptext" style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
								<span id="suspend_date_tooltip2" class="tooltiptext"
									style="display: none; color: red"><?php echo DATE_CAN_NOT_BE_GREATER_THAN_TODAY; ?></span>
								<span id="suspend_date_tooltip3" class="tooltiptext"
									style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label"><?php echo SUSPEND_REASON; ?><span
								class="required"> * </span></label>
							<div class="col-md-4">
								<select class="form-control " name="suspend_reason_code"
									id="suspend_reason_code" class="form-control"
									onfocus="javascript:removeTooltip('suspend_reason_code_tooltip');">
					  	<?php
											if ($suspend_reasons) {
												for($i = 0; $i < count ( $suspend_reasons ); $i ++) {
													$suspend_code = $suspend_reasons [$i] ["suspend_code"];
													$suspend_reason = $suspend_reasons [$i] ["suspend_reason"];
													?>
							<option value="<?=$suspend_code?>"> <?=$suspend_reason ?></option>
						<?php
												}
											} else {
												?>
							<option value="" selected></option>
						<?php
											}
											?>		
					</select> <span id="suspend_reason_code_tooltip"
									class="tooltiptext" style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label"><?php echo SUSPEND_FILE; ?></label>
							<div class="col-md-2">
								<input type="file" class="form-control " name="suspend_file"
									id="suspend_file"
									onchange="uploadFile('suspend_file', 'suspend_file_status', 'suspend_file_progressBar','suspend_file', '<?php echo $case_id;?>', 'suspend_file_fragment')">
								<input type="hidden" id="suspend_file_hidden"
									name="suspend_file_hidden">

							</div>
							<div class="col-md-2">
								<div id="suspend_file_fragment" class="fragment"
									style="display: none;">
									<span
										style="float: left; display: inline-block; color: #0c0b0b80; cursor: pointer !important; font-weight: bold;"
										id="close_suspend_file">&#10006;</span>
									<progress style="display: none;" id="suspend_file_progressBar"
										value="0" max="100" style="width:100px;"></progress>
									<span id="suspend_file_status"></span>
								</div>
							</div>
						</div>

						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									<button type="submit" class="btn btn-circle green"><?php
											echo SUSPEND_CASE;
											
											?></button>
								</div>
							</div>
						</div>
				<?php }else if($action_code == "RESUME" || $action_code == "REOPEN"){?>
					<div class="form-group">
						<label class="col-md-3 control-label"><?php if($action_code == "RESUME"){echo RESUME_DATE;} else {echo REOPEN_DATE;} ?><span
												class="required"> * </span></label>
						<div class="col-md-4">

							<input type="text" class="form-control date" name="return_date"
													id="return_date"> <span id="return_date_tooltip"
													class="tooltiptext" style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
							<span id="return_date_tooltip2" class="tooltiptext"
													style="display: none; color: red"><?php echo DATE_CAN_NOT_BE_GREATER_THAN_TODAY; ?></span>
							<span id="return_date_tooltip3" class="tooltiptext"
													style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>
						</div>
					</div>
					<div class="form-actions">
						<div class="row">
							<div class="col-md-offset-3 col-md-9">
								<button type="submit" class="btn btn-circle green"><?php
															if($action_code == "RESUME"){echo RESUME_CASE;} else {echo REOPEN_CASE;}
															
															?></button>
							</div>
						</div>
					</div>
				<?php }else if($action_code == "SUSPEND_REQUEST"){?>
							
						<div class="form-group">
							<label class="col-md-3 control-label"><?php echo SUSPND_REASON; ?><span
								class="required"> * </span></label>
							<div class="col-md-4">
								<select class="form-control " name="req_suspend_reason_code"
									id="req_suspend_reason_code" class="form-control"
									onfocus="javascript:removeTooltip('req_suspend_reason_code_tooltip');">
					  	<?php
											if ($suspend_reasons) {
												for($i = 0; $i < count ( $suspend_reasons ); $i ++) {
													$suspend_code = $suspend_reasons [$i] ["suspend_code"];
													$suspend_reason = $suspend_reasons [$i] ["suspend_reason"];
													?>
							<option value="<?=$suspend_code?>"> <?=$suspend_reason ?></option>
						<?php
												}
											} else {
												?>
							<option value="" selected></option>
						<?php
											}
											?>		
					</select> <span id="req_suspend_reason_code_tooltip"
									class="tooltiptext" style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 control-label"><?php echo NOTES; ?></label>
							<div class="col-md-4">
								<textarea class="form-control " rows="4" cols="50" name="suspend_notes"
									id="suspend_notes" form="myForm"></textarea>
							</div>
						</div>

						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									<button type="submit" class="btn btn-circle green"><?php
											echo REQUEST_SUSPEND;
											
											?></button>
								</div>
							</div>
						</div>
					<?php }else if($action_code == "RESUME_REQUEST"){?>
						<div class="form-group">
							<label class="col-md-3 control-label"><?php echo NOTES; ?><span
								class="required"> * </span></label>
							<div class="col-md-4">
								<textarea class="form-control " rows="4" cols="50" name="resume_notes"
									id="resume_notes" form="myForm"
									onfocus="javascript:removeTooltip('resume_notes_tooltip');"></textarea>
								<span id="resume_notes_tooltip" class="tooltiptext"
									style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>

							</div>
						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									<button type="submit" class="btn btn-circle green"><?php
											echo REQUEST_RESUME;
											
											?></button>
								</div>
							</div>
						</div>
				<?php }?>
				
											
				</div>

					<input type="hidden" name="customer_id" value="<? echo $customer_id ?>"> <input
						type="hidden" name="contract_number" value=""> <input
						type="hidden" name="client_id_number" value=""> <input
						type="hidden" name="client_name" value=""> <input type="hidden"
						name="client_type" value=""> <input type="hidden"
						name="number_of_late_instalments" value=""> <input type="hidden"
						name="due_amount" value=""> <input type="hidden"
						name="remaining_amount" value=""> <input type="hidden"
						name="others_count" value=""> <input type="hidden"
						name="debtor_name" value=""> <input type="hidden"
						name="obligation_value" value="">
					<?php if($action_code != "REGISTER"){?>
					<input type="hidden" name="executive_order_number" value=""> <input
						type="hidden" name="circle_number" value=""> <input type="hidden"
						name="order_number" value=""> <input type="hidden"
						name="executive_order_date" value=""> 
					<?php }if($action_code != "MAKE_DECISION_34"){?>
					<input type="hidden" name="decision_34_number" value=""> <input
						type="hidden" name="decision_34_date" value=" ">
					<?php }if($action_code != "MAKE_DECISION_46"){?>
					<input type="hidden" name="decision_46_date" value=""> 
					<?php }if($action_code != "CLOSE"){?>
					<input type="hidden" name="closing_reason" value=""> 
					<?php }if($action_code != "ASSIGN_TO_LAWYER" && $action_code != "REASSIGN_TO_LAWYER"){?>
					<input type="hidden" name="lawyer_id" value=""> <input
						type="hidden" name="region_id" value="">
					<?php }if($action_code != "NEED_MODIFICATIONS"){ ?>
					<input type="hidden" name="notes" value="">
					<?php }if($action_code != "SUSPEND"){ ?>
					<input type="hidden" name="suspend_reason_code" value=""> 
					<input type="hidden" name="suspend_date" value="">
					<?php }if($action_code != "ADVERTISE"){?>
					<input type="hidden" name="advertisement_date" value="">
					<?php }if($action_code != "SUSPEND_REQUEST"){ ?>
					<input type="hidden" name="req_suspend_reason_code" value=""> 
					<input type="hidden" name="suspend_notes" value="">
					<?php }if($action_code != "RESUME_REQUEST"){ ?>
					<input type="hidden" name="resume_notes" value="">
					<?php }?>
			
						
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			</form>
			<!-- END FORM-->
		</div>
	</div>

	<!--[if lt IE 9]>
<script src="<?=base_url()?>assets/global/plugins/respond.min.js"></script>
<script src="<?=base_url()?>assets/global/plugins/excanvas.min.js"></script> 
<script src="<?=base_url()?>assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
	<!-- BEGIN CORE PLUGINS -->
	<script type="text/javascript"
		src="<?php echo base_url()?>js/bootbox.min.js"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="<?=base_url()?>assets/global/plugins/moment.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/morris/morris.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/morris/raphael-min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/counterup/jquery.waypoints.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/counterup/jquery.counterup.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/fullcalendar/fullcalendar.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.resize.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.categories.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.sparkline.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="<?=base_url()?>assets/global/scripts/app.min.js"
		type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?=base_url()?>assets/pages/scripts/dashboard.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script
		src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js"
		type="text/javascript"></script>
	<!-- END THEME LAYOUT SCRIPTS -->

</body>

</html>