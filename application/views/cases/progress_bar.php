<html>
<head>
<script>
// add the highlighting color if the filter is not selected
function mouseOver (state){
	var element = document.getElementById(state);
	if(!element.classList.contains("done"))
	element.className += " mouseover";
}

// remove the highlighting color if the filter is not selected
function mouseOut (state){
	var element = document.getElementById(state);
	if(!element.classList.contains("done"))
        element.classList.remove("mouseover");
	
}

// while clicking in certain filter add the class "done" (Green) and apply the action
function filter(state) {
   // removr the done class from the current filter
   var elements = document.getElementsByClassName("done");
    //for(var i = 0, length = elements.length; i < length; i++) {
         elements[0].classList.remove("done");
    //}
    // remove the mouseover class and add the "done" class for the selected filter
    var selectedDiv= document.getElementById(state);
    selectedDiv.classList.remove("mouseover");
    selectedDiv.className += " done";
    // applying action depending on values
    <?php if (isset($search_mode)){?>
    	if(state == "display_all")
    		window.location="<?=site_url('case_management/view_cases').'/0'.'/'.$search_mode.'/'.'1'.'/'.'0'.'/'.$selected_region?>";
    	else
    		window.location="<?=site_url('case_management/view_cases')?>"+"/"+state+"<?php echo '/'.$search_mode.'/'.'1'.'/'.'0'.'/'.$selected_region?>";
	<?php }?>
}
</script>

<style type="text/css">
<?php if(isset($case_details ["case_id"])) {?>
	.mt-element-step .step-thin .mt-step-col{
		width: 12.5% !important;
		min-width: 125px !important;
		max-width: 150px !important;
		float: right !important;
	}
<?php } ?>
@media only screen and (max-width: 991px) {
	<?php if(!isset($case_details ["case_id"])) {?>
		.status-box {
            width: 48%;
            margin: 1%;
	        float: right !important;
		}
	<?php } ?>
}
</style>

</head>
<body>
	<div class="portlet ">
	<?php if (isset($cases)){?>
		<div class="portlet-title">
			<div class="caption"><span
					class="caption-subject font-green bold uppercase"><?php echo STATE_FILTER;?></span>
			</div>
		</div>
		<?php }?>
		<div class="portlet-body">
			<div class="mt-element-step">
				<div class="row step-thin">
									<?php
									$j = 0;
									// in case of view_case_details set $selected_state to the current state of the selected case
									if (! isset ( $selected_state ) && isset ( $case_details )) {
										$selected_state = $case_details ["current_state_code"];
									}
									
									// in case of view_case_details [تفاصيل القصية] set the $ordered_states equals $case_transactions revise "Google Sheet"
									if (! isset ( $ordered_states ) && isset ( $case_transactions )) {
										$ordered_states = $case_transactions;
										// in case of view_cases [table] start from تحت المراجعة [second item] j=1, but in case of add_case start from the [first item] "قضية جديدة"
									} else if (isset ( $cases )) {
										$j = 1;
										?>
										<div id="display_all" onclick="filter('display_all')"
						onmouseover="mouseOver('display_all')"
						onmouseout="mouseOut('display_all')"
						class="col-pb-2 bg-grey mt-step-col status-box 
					<?php if($selected_state == "0"){?>
					done
					<?php } ?>
					">
						<div class="mt-step-title uppercase font-grey-cascade"><?= DIAPLAY_ALL ?></div>
						<div class="mt-step-content font-grey-cascade"> <?= $UNDER_REVIEW_count+$ASSIGNED_TO_LAWYER_count+$REGISTERED_count+$DECISION_34_count+$PUBLICLY_ADVERTISED_count+$DECISION_46_count+$CLOSED_count+$SUSPENDED_count+$MODIFICATION_REQUIRED_count?></div>
					</div>
											
									<?php
									}
									// for highlight last state only in case of view_cases
									$done_state = 0;
									for($i = $j; $i < count ( $ordered_states ); $i ++) {
										if ((isset ( $selected_state ) && $ordered_states [$i] ["state_code"] == $selected_state)) {
											$done_state = $i;
										}
									}
									// display states in each view [add_case, view_case_details, view_cases]
									for($i = $j; $i < count ( $ordered_states ); $i ++) {
										?>
										<div id="<?= $ordered_states[$i]["state_code"] ?>"
						<?php if(isset($cases)){?>
						onclick="filter('<?= $ordered_states[$i]["state_code"] ?>')"
						onmouseover="mouseOver('<?= $ordered_states[$i]["state_code"] ?>')"
						onmouseout="mouseOut('<?= $ordered_states[$i]["state_code"] ?>')"
						<?php }?>
						class="
											<!-- adjusting each col width depending on the current view and thier states -->
											<?php if(!isset($case_details) && !isset($cases)){?>
											col-pb-2
											<?php }else if((isset($case_details) && count($ordered_states)>7)|| !isset($case_details)){?>
												<?php if(strpos($ordered_states[$i]["state_name"], " ") !== false && strpos($ordered_states[$i]["state_name"], "4") == false ){?>
													col-pb-2
												<?php }else{?>
													col-pb-1
												<?php }?>
											<?php } else {?>
											col-pb-3
											<?php }?>
											bg-grey mt-step-col status-box 
											<?php if(!isset($cases)){ ?>
												view-only
											<?php } ?>
											<?php if(($i == $done_state) || ($i == 0 && !isset($selected_state))){?>
											done
											<?php }?>
											">
						<!-- seeting name of each state -->
						<div class="mt-step-title uppercase font-grey-cascade"><?= $ordered_states[$i]["state_name"] ?></div>


						<div class="mt-step-content font-grey-cascade">
					<?php if(isset($cases)||isset($case_details)){?>
					<?php
											// display date under the state name in case of view_case_details
											if (isset ( $ordered_states [$i] ["state_date"] ) && $ordered_states [$i] ["state_date"] != "") {
												// display the count of cases under each state in case of view_cases
												echo $ordered_states [$i] ["state_date"];
											} else if (isset ( $cases )) {
												switch ($ordered_states [$i] ["state_code"]) {
													case "UNDER_REVIEW" :
														echo $UNDER_REVIEW_count;
														break;
													case "ASSIGNED_TO_LAWYER" :
														echo $ASSIGNED_TO_LAWYER_count;
														break;
													case "MODIFICATION_REQUIRED" :
														echo $MODIFICATION_REQUIRED_count;
														break;
													case "REGISTERED" :
														echo $REGISTERED_count;
														break;
													case "DECISION_34" :
														echo $DECISION_34_count;
														break;
													case "PUBLICLY_ADVERTISED" :
														echo $PUBLICLY_ADVERTISED_count;
														break;
													case "DECISION_46" :
														echo $DECISION_46_count;
														break;
													case "CLOSED" :
														echo $CLOSED_count;
														break;
													case "SUSPENDED" :
														echo $SUSPENDED_count;
														break;
													default :
														echo "";
												}
											} else if (isset ( $case_details )) {
												?>
												<br>
											<?php
											
}
										} else {
											echo "";
										}
										?></div>
					</div>
										<?php }?>
									</div>
			</div>
		</div>
	</div>
</body>
</html>