<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<?php
session_start ();
$this->load->view ( 'localization/lang' );
$this->load->view ( 'localization/txt' );
defineLocale ();
defineStrings ();
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>

<script type="text/javascript">

var scrollup = true;
function checkValid() {
	var finishUploading = check_uploading_files();
	if(finishUploading){
		if (textNumViolation())
			return false;
		replacAllArabToEng();
		var contract_number= document.forms["myForm"]["contract_number"].value;
		var debtor_name= document.forms["myForm"]["debtor_name"].value;
		var client_id_number= document.forms["myForm"]["client_id_number"].value;
		var client_type= document.forms["myForm"]["client_type"].value;
		var number_of_late_instalments= document.forms["myForm"]["number_of_late_instalments"].value;
		var due_amount= document.forms["myForm"]["due_amount"].value;
		var remaining_amount= document.forms["myForm"]["remaining_amount"].value;
		var total_debenture_amount= document.forms["myForm"]["total_debenture_amount"].value;
		var debenture= document.forms["myForm"]["debenture"].value;
		var id= document.forms["myForm"]["id"].value;
		var obligation_value = document.forms["myForm"]["obligation_value"].value;
		
		if (contract_number== "" || contract_number<=0 || contract_number.length< 3 || contract_number.length> 15) {
			$("#contract_number_tooltip").css("display","block");
			$("#contract_number_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#contract_number_tooltip").css("display","none");
			$("#contract_number_tooltip").css("visibility","none");
		}
		if (debtor_name== "" ) {
			$("#debtor_name_tooltip").css("display","block");
			$("#debtor_name_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#debtor_name_tooltip").css("display","none");
			$("#debtor_name_tooltip").css("visibility","none");
		}
		if (client_id_number== "" || client_id_number<=0 || client_id_number.length< 3 || client_id_number.length> 15) {
			$("#client_id_number_tooltip").css("display","block");
			$("#client_id_number_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#client_id_number_tooltip").css("display","none");
			$("#client_id_number_tooltip").css("visibility","none");
		}
		if (client_type== "" ) {
			$("#client_type_tooltip").css("display","block");
			$("#client_type_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#client_type_tooltip").css("display","none");
			$("#client_type_tooltip").css("visibility","none");
		}
		if (number_of_late_instalments== "" || number_of_late_instalments< 0 || number_of_late_instalments.length > 7) {
			$("#number_of_late_instalments_tooltip").css("display","block");
			$("#number_of_late_instalments_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#number_of_late_instalments_tooltip").css("display","none");
			$("#number_of_late_instalments_tooltip").css("visibility","none");
		}
		if (due_amount== "" || due_amount< 0 || due_amount.length > 7) {
			$("#due_amount_tooltip").css("display","block");
			$("#due_amount_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#due_amount_tooltip").css("display","none");
			$("#due_amount_tooltip").css("visibility","none");
		}
		if (remaining_amount== "" || remaining_amount<= 0 || remaining_amount.length > 7) {
			$("#remaining_amount_tooltip").css("display","block");
			$("#remaining_amount_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#remaining_amount_tooltip").css("display","none");
			$("#remaining_amount_tooltip").css("visibility","none");
		}
		if (total_debenture_amount== "" || total_debenture_amount<= 0 || total_debenture_amount.length > 7) {
			$("#total_debenture_amount_tooltip").css("display","block");
			$("#total_debenture_amount_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#total_debenture_amount_tooltip").css("display","none");
			$("#total_debenture_amount_tooltip").css("visibility","none");
		}
		if (obligation_value!= "" && (obligation_value <= 0 || obligation_value.length >7)) {
			$("#obligation_value_tooltip").css("display","block");
			$("#obligation_value_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#obligation_value_tooltip").css("display","none");
			$("#obligation_value_tooltip").css("visibility","none");
		}
		if (debenture== "") {
			scrollup = false;
			$("#debenture_tooltip").css("display","block");
			$("#debenture_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#debenture_tooltip").css("display","none");
			$("#debenture_tooltip").css("visibility","none");
		}
		if (id== "") {
			scrollup = false;
			$("#id_tooltip").css("display","block");
			$("#id_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#id_tooltip").css("display","none");
			$("#id_tooltip").css("visibility","none");
		}
		
		nullifyUploadFields();
	}else{
		return false;
	}
	$('#loading').removeClass('hidden');
}
function resetErrorMsgs (){
	$("#contract_number_tooltip").css("display","none");
	$("#debtor_name_tooltip").css("display","none");
	$("#client_id_number_tooltip").css("display","none");
	$("#client_type_tooltip").css("display","none");
	$("#remaining_amount_tooltip").css("display","none");
	$("#due_amount_tooltip").css("display","none");
	$("#number_of_late_instalments_tooltip").css("display","none");
	$("#total_debenture_amount_tooltip").css("display","none");
	$("#obligation_value_tooltip").css("display","none");
	$("#debenture_tooltip").css("display","none");
	$("#id_tooltip").css("display","none");
}
function scrollUpError(submitType,current_state_code){
	valid= true;
	resetErrorMsgs();
	valid = checkValid();
	
	if (valid == false && scrollup == true){
		/*var errorDiv = $('#myForm').first();
		var scrollPos = errorDiv.offset().top;
		$(window).scrollTop(scrollPos);*/
		$("html, body").animate({ scrollTop: $(window).height()/3}, "slow");
	} else if (scrollup == true){
	//to load cases list in case of searching
	window.opener.location.reload();
	}
	scrollup = true;
	return valid;
}
function nullifyUploadFields(){
	if(_("debenture_hidden") !== null && _("debenture_hidden") !== undefined && _("debenture_hidden").value != ""){
		_("debenture").value = ""; 
	}
	if(_("id_hidden") !== null && _("id_hidden") !== undefined && _("id_hidden").value != ""){
		_("id").value = "";
	}
	if(_("contract_hidden") !== null && _("contract_hidden") !== undefined && _("contract_hidden").value != ""){ 
		_("contract").value = ""; 
	}
	if(_("others_hidden") !== null && _("others_hidden") !== undefined && _("others_hidden").value != ""){
		_("others").value = ""; 
	}
}

function check_uploading_files(){
	if((_("debenture_uploadingFiles") !== null && _("debenture_uploadingFiles") !== undefined && _("debenture_uploadingFiles").value == "no_files") &&
	   (_("id_uploadingFiles") !== null && _("id_uploadingFiles") !== undefined && _("id_uploadingFiles").value == "no_files") &&
	   (_("contract_uploadingFiles") !== null && _("contract_uploadingFiles") !== undefined && _("contract_uploadingFiles").value == "no_files") &&
	   (_("others_uploadingFiles") !== null && _("others_uploadingFiles") !== undefined && _("others_uploadingFiles").value == "no_files")
	   ){
			return true;
	   }else{
		   bootbox.dialog({
				message: '<p><i class="fa fa-spin fa-spinner"></i> '+"رجاء انتظار تحميل المرفقات أولا ثم النقر على الزر"+' </p>',
				buttons: {
					confirm: {
						label: "موافق",
						className: 'green'
					}
				}
			});
		   return false;
	   }
}

</script>
<meta charset="utf-8" />
<title><?php echo TITLE; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin RTL Theme #3 for dashboard & statistics"
	name="description" />
<meta content="" name="author" />

<link rel="shortcut icon"
	href="<?=base_url()?>assets/layouts/layout3/img/logo-default.jpg" />
</head>
<!-- END HEAD -->
<body class="page-container-bg-solid">
	<div class="page-wrapper">
<?php
if ($user_type_code == "ADMIN") {
	$this->load->view ( 'office_admin/office_admin_header' );
} else {
	$this->load->view ( 'secretary_lawyer/secretary_header' );
}
?>
				<div class="page-wrapper-row full-height">
			<div class="page-wrapper-middle">
				<!-- BEGIN CONTAINER -->
				<div class="page-container">
					<!-- BEGIN CONTENT -->
					<div class="page-content-wrapper">
						<!-- BEGIN CONTENT BODY -->
						<!-- BEGIN PAGE HEAD-->
						<div class="page-head">
							<div class="container">
								<div class="col-md-14">
									<div class="portlet box green">
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-gift"></i>
												<?php
												echo ADD_RECORD;
												?>
											</div>
										</div>
										<div class="portlet-body form">
											<br>
										<?php $this->load->view('cases/progress_bar');?>
								<br>
											<!-- BEGIN FORM-->
											<form id="myForm" role="form" method="post"
												onsubmit="javascript:return scrollUpError();"
												enctype="multipart/form-data"
												action="<?=site_url('case_management/add_case/')?>"
												class="form-horizontal">
												<div class="form-body">
												
													<input type="hidden" id="debenture_uploadingFiles" name="debenture_uploadingFiles" value="no_files"> 
													<input type="hidden" id="id_uploadingFiles" name="id_uploadingFiles" value="no_files">
													<input type="hidden" id="contract_uploadingFiles" name="contract_uploadingFiles" value="no_files">
													<input type="hidden" id="others_uploadingFiles" name="others_uploadingFiles" value="no_files">
													
													<div class="row">
													<?php if ($user_type_code != "CUSTOMER") {?>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo CUSTOMER; ?><span
																	class="required"> * </span></label>
																<div class="col-md-6">
																	<select class="form-control " name="customer_id"
																		id="customer_id" class="form-control">
					  	<?php
								if ($available_customers) {
									for($i = 0; $i < count ( $available_customers ); $i ++) {
										$customer_id_s = $available_customers [$i] ["user_id"];
										$customer_name = $available_customers [$i] ["first_name"] . " " . $available_customers [$i] ["last_name"];
										?>
							<option value="<?=$customer_id_s?>"
																			<?php if ($customer_id_s == $customer_id){?>
																			selected="selected" <?php }?>> <?=$customer_name ?></option>
						<?php
									}
								} else {
									?>
							<option value="" selected></option>
						<?php
								}
								?>		
					</select>
																</div>
															</div>
														</div>
														<?php }?>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo CONTRACT_NUMBER; ?><span
																	class="required"> * </span></label>
																<div class="col-md-6">
																	<input type="text" act="yes" onkeydown="numberKeyDown(event)" class="form-control  no-spinners"
																		name="contract_number" id="contract_number"
																		value="<?php echo $contract_number?>"
																		onfocus="javascript:removeTooltip('contract_number_tooltip');">
																	<span id="contract_number_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo CASE_NUMBER_INVALID; ?></span>


																</div>
															</div>
														</div>
													<?php if ($user_type_code != "CUSTOMER") {?>
													</div>													
													<div class="row">
													<?php }?>
													<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo DEBTOR_NAME; ?><span
																	class="required"> * </span></label>
																<div class="col-md-6">
																	<input type="text" class="form-control "
																		name="debtor_name" id="debtor_name"
																		value="<?php echo $debtor_name?>"
																		onfocus="javascript:removeTooltip('debtor_name_tooltip');">
																	<span id="debtor_name_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>

																</div>
															</div>
														</div>
													
													<?php if ($user_type_code == "CUSTOMER") {?>
													</div>													
													<div class="row">
													<?php }?>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo CLIENT_NAME; ?></label>
																<div class="col-md-6">
																	<input type="text" class="form-control "
																		name="client_name" id="client_name"
																		value="<?php echo $client_name?>">
																</div>
															</div>
														</div>
														
													<?php if ($user_type_code != "CUSTOMER") {?>
													</div>													
													<div class="row">
													<?php }?>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo CLIENT_ID_NUMBER; ?><span
																	class="required"> * </span></label>
																<div class="col-md-6">
																	<input type="text" act="yes" onkeydown="numberKeyDown(event)" class="form-control no-spinners"
																		name="client_id_number" id="client_id_number"
																		value="<?php echo $client_id_number?>"
																		onfocus="javascript:removeTooltip('client_id_number_tooltip');">
																	<span id="client_id_number_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo CASE_NUMBER_INVALID; ?></span>

																</div>
															</div>
														</div>
													
													<?php if ($user_type_code == "CUSTOMER") {?>
													</div>													
													<div class="row">
													<?php }?>
															<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"> <?php echo CLIENT_TYPE; ?> <span
																	class="required"> * </span></label>

																<div class="col-md-6">
																	<select class="form-control " name="client_type"
																		id="client_type" class="form-control">
					  	<?php
								if ($available_customer_types) {
									for($i = 0; $i < count ( $available_customer_types ); $i ++) {
										$customer_type_id = $available_customer_types [$i] ["customer_type_id"];
										$customer_type_name = $available_customer_types [$i] ["name"];
										?>
							<option value="<?=$customer_type_id ?>"
																			<?php if ($customer_type_id == $client_type){?>
																			selected="selected" <?php }?>> <?=$customer_type_name ?></option>
						<?php
									}
								} else {
									?>
							<option value="" selected></option>
						<?php
								}
								?>		
					</select>
																</div>
															</div>
														</div>
													<?php if ($user_type_code != "CUSTOMER") {?>
													</div>													
													<div class="row">
													<?php }?>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo NUMBER_OF_LATE_INSTALMENTS; ?> <span
																	class="required"> * </span></label>
																<div class="col-md-6">
																	<input type="text" act="yes" onkeydown="numberKeyDown(event)" class="form-control  no-spinners"
																		name="number_of_late_instalments"
																		id="number_of_late_instalments"
																		value="<?php echo $number_of_late_instalments?>"
																		id="number_of_late_instalments"
																		onfocus="javascript:removeTooltip('number_of_late_instalments_tooltip');">
																	<span id="number_of_late_instalments_tooltip"
																		class="tooltiptext" style="display: none; color: red"><?php echo INVALID_NUMBER_OF_LATE_INSTALMENTS; ?></span>
																</div>
															</div>
														</div>
														
													<?php if ($user_type_code == "CUSTOMER") {?>
													</div>													
													<div class="row">
													<?php }?>
													<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo OBLIGATION_VALUE; ?></label>
																<div class="col-md-4">
																	<input type="text" act="yes" onkeydown="numberKeyDown(event)" class="form-control  no-spinners"
																		value="<?php echo $due_amount?>" id="obligation_value"
																		name="obligation_value" onfocus="javascript:removeTooltip('obligation_value_tooltip');">
																	<span id="obligation_value_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo NUMBER_INVALID; ?></span>
																</div>
																<label class="col-md-2 control-label"><?php echo SR; ?></label>
															</div>
														</div>
													<?php if ($user_type_code != "CUSTOMER") {?>
													</div>													
													<div class="row">
													<?php }?>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo DUE_AMOUNT; ?><span
																	class="required"> * </span> </label>
																<div class="col-md-4">
																	<input type="text" act="yes" onkeydown="numberKeyDown(event)" class="form-control  no-spinners"
																		value="<?php echo $due_amount?>" id="due_amount"
																		name="due_amount"
																		onfocus="javascript:removeTooltip('due_amount_tooltip');">
																	<span id="due_amount_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo NUMBER_INVALID; ?></span>
																</div>
																<label class="col-md-2 control-label"><?php echo SR; ?></label>
															</div>
														</div>
													<?php if ($user_type_code == "CUSTOMER") {?>
													</div>													
													<div class="row">
													<?php }?>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo REMAINING_AMOUNT; ?><span
																	class="required"> * </span></label>
																<div class="col-md-4">
																	<input type="text" act="yes" onkeydown="numberKeyDown(event)" class="form-control  no-spinners"
																		name="remaining_amount" id="remaining_amount"
																		value="<?php echo $remaining_amount?>"
																		onfocus="javascript:removeTooltip('remaining_amount_tooltip');">
																	<span id="remaining_amount_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo NUMBER_INVALID; ?></span>
																</div>
																<label class="col-md-2 control-label"><?php echo SR; ?></label>
															</div>
														</div>
														
													<?php if ($user_type_code != "CUSTOMER") {?>
													</div>													
													<div class="row">
													<?php }?>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo TOTAL_DEBENTURE_AMOUNT; ?><span
																	class="required"> * </span></label>
																<div class="col-md-4">
																	<input type="text" act="yes" onkeydown="numberKeyDown(event)" class="form-control  no-spinners"
																		name="total_debenture_amount"
																		id="total_debenture_amount"
																		value="<?php echo $total_debenture_amount?>"
																		onfocus="javascript:removeTooltip('total_debenture_amount_tooltip');">
																	<span id="total_debenture_amount_tooltip"
																		class="tooltiptext" style="display: none; color: red"><?php echo NUMBER_INVALID; ?></span>
																</div>
																<label class="col-md-2 control-label"><?php echo SR; ?></label>
															</div>
														</div>
													
													</div>													
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo DEBENTURE; ?><span
																	class="required"> * </span></label>
																<div class="col-md-6">
																	<input type="file" class="form-control "
																		name="debenture" id="debenture"
																		onfocus="javascript:removeTooltip('debenture_tooltip');"
																		onchange="uploadFile('debenture', 'debenture_status', 'debenture_progressBar','debenture', '<?php echo $case_id?>', 'debenture_fragment')">
																		
																	<input type="hidden" id="debenture_hidden" name="debenture_hidden"> 
																	<span id="debenture_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
														<div class="col-md-5">
															<div id="debenture_fragment" class="fragment"
																style="display: none;">
																<span
																	style="float: left; display: inline-block; color: #0c0b0b80; cursor: pointer !important; font-weight: bold;"
																	id="close_debenture">&#10006;</span>
																<progress style="display: none;"
																	id="debenture_progressBar" value="0" max="100"
																	style="width:100px;"></progress>
																<span id="debenture_status"></span>
															</div>
														</div>
													</div>													
													<div class="row">	
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo CASE_ID_ADD; ?><span
																	class="required"> * </span></label>
																<div class="col-md-6">
																	<input type="file" class="form-control " name="id"
																		id="id"
																		onfocus="javascript:removeTooltip('id_tooltip');"
																		onchange="uploadFile('id', 'id_status', 'id_progressBar','id', '<?php echo $case_id?>', 'id_fragment')">
																		<input type="hidden" id="id_hidden" name="id_hidden">

																		<span id="id_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
														<div class="col-md-5">
															<div id="id_fragment" class="fragment"
																style="display: none;">
																<span
																	style="float: left; display: inline-block; color: #0c0b0b80; cursor: pointer !important; font-weight: bold;"
																	id="close_id">&#10006;</span>
																<progress style="display: none;" id="id_progressBar"
																	value="0" max="100" style="width:100px;"></progress>
																<span id="id_status"></span>
															</div>
														</div>	
													</div>													
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo CONTRACT; ?></label>
																<div class="col-md-6">
																	<input type="file" class="form-control "
																		name="contract" id="contract"
																		onfocus="javascript:removeTooltip('contract_tooltip');"
																		onchange="uploadFile('contract', 'contract_status', 'contract_progressBar','contract', '<?php echo $case_id?>', 'contract_fragment')">
																	<input type="hidden" id="contract_hidden" name="contract_hidden"> 
																	
																	<span id="contract_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
														<div class="col-md-5">
															<div id="contract_fragment" class="fragment"
																style="display: none;">
																<span
																	style="float: left; display: inline-block; color: #0c0b0b80; cursor: pointer !important; font-weight: bold;"
																	id="close_contract">&#10006;</span>
																<progress style="display: none;"
																	id="contract_progressBar" value="0" max="100"
																	style="width:100px;"></progress>
																<span id="contract_status"></span>
															</div>
														</div>	
													</div>													
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo OTHERS; ?></label>
																<div class="col-md-6">
																	<input type="file" class="form-control "
																		name="others[]" id="others" multiple
																		onfocus="javascript:removeTooltip('others_tooltip');"
																		onchange="uploadFile('others', 'others_status', 'others_progressBar','others', '<?php echo $case_id?>', 'others_fragment')">
																	<input type="hidden" id="others_hidden" name="others_hidden">
																</div>
															</div>
														</div>
														<div class="col-md-5">
															<div id="others_fragment" class="fragment"
																style="display: none;">
																<span
																	style="float: left; display: inline-block; color: #0c0b0b80; cursor: pointer !important; font-weight: bold;"
																	id="close_others">&#10006;</span>
																<progress style="display: none;"
																	id="others_progressBar" value="0" max="100"
																	style="width:100px;"></progress>
																<span id="others_status"></span>
															</div>
														</div>
													</div>
													<div class="form-actions">
														<div class="row">
															<div class="col-md-9" align="center" style="margin: auto; width: 100%;">
																<button type="submit" class="btn btn-circle green"><?php
																echo INSERT;
																
																?></button>
																<a
																	href="<?=site_url('case_management')?>"
																	class="btn btn-outline btn-circle grey-salsa"><?php echo CANCEL; ?>
																</a>
															</div>
														</div>
													</div>
											</div>
											</form>
											<!-- END FORM-->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
        <?php $this->load->view('utils/footer');?>
											<!-- BEGIN CORE PLUGINS -->
	<script src="<?=base_url()?>assets/global/plugins/jquery.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.sparkline.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="<?=base_url()?>assets/global/scripts/app.min.js"
		type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?=base_url()?>assets/pages/scripts/profile.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script
		src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js"
		type="text/javascript"></script>
	<!-- END THEME LAYOUT SCRIPTS -->

	<script type="text/javascript"
		src="<?php echo base_url()?>js/bootbox.min.js"></script>
</body>

</html>