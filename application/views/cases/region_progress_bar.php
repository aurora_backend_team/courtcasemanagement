<html>
<head>
<script>
// add the highlighting color if the filter is not selected
function mouseOver (region){
	var element = document.getElementById(region);
	if(!element.classList.contains("done"))
	element.className += " mouseover";
}

// remove the highlighting color if the filter is not selected
function mouseOut (region){
	var element = document.getElementById(region);
	if(!element.classList.contains("done"))
        element.classList.remove("mouseover");
	
}

// while clicking in certain filter add the class "done" (Green) and apply the action
function regionFilter(region) {
   // removr the done class from the current filter
   var elements = document.getElementsByClassName("done");
    //for(var i = 0, length = elements.length; i < length; i++) {
         elements[1].classList.remove("done");
    //}
    // remove the mouseover class and add the "done" class for the selected filter
    var selectedDiv= document.getElementById(region);
    selectedDiv.classList.remove("mouseover");
    selectedDiv.className += " done";
    // applying action depending on values
    if(region == "region_display_all")
    	window.location="<?=site_url('case_management/view_cases'.'/'.$selected_state.'/'.$search_mode.'/1/0')?>";
    else
    	window.location="<?=site_url('case_management/view_cases'.'/'.$selected_state.'/'.$search_mode.'/1/0')?>"+"/"+region;
}
</script>

<style type="text/css">
@media only screen and (max-width: 991px) {
	.region-box {
		width: 48% !important;
        margin: 1% !important;
        float: right !important;
	}
}
</style>

</head>
<body>
	<div class="portlet ">
		<div class="portlet-title">
			<div class="caption">
				<span class="caption-subject font-green bold uppercase"><?php echo REGION_FILTER;?></span>
			</div>
		</div>
		<div class="portlet-body">
			<div class="mt-element-step">
				<div class="row step-thin outer">
					<div id="region_display_all" onclick="regionFilter('region_display_all')"
					onmouseover="mouseOver('region_display_all')"
					onmouseout="mouseOut('region_display_all')"
					class="col-pb bg-grey mt-step-col region-box 
					<?php if($selected_region == "0"){?>
					done
					<?php } ?>
					 " style="width:<?= 100/(count($case_region_count)+1) ?>%;">
						<div class="mt-step-title uppercase font-grey-cascade"
							style="height: 35px"><?= DIAPLAY_ALL ?></div>
						<div class="mt-step-content font-grey-cascade"> <?
						$sum = 0;
						for($i = 0; $i < count ( $case_region_count ); $i ++) {
							$sum = $sum + $case_region_count [$i] ["count"];
						}
						echo $sum;
						?></div>
					</div>
											
									<?php
									// for highlight last state only in case of view_cases
									for($i = 0; $i < count ( $case_region_count ); $i ++) {
										
										?>
										<div id="<?= $case_region_count[$i]["region_id"] ?>"
					onclick="regionFilter('<?= $case_region_count[$i]["region_id"] ?>')"
					onmouseover="mouseOver('<?= $case_region_count[$i]["region_id"] ?>')"
					onmouseout="mouseOut('<?= $case_region_count[$i]["region_id"] ?>')"
					class="	col-pb bg-grey mt-step-col region-box 
											<?php if($case_region_count [$i] ["region_id"] == $selected_region){?>
											done
											<?php }?>
											 " style="width:<?= 100/(count($case_region_count)+1) ?>%;">
						<!-- seeting name of each state -->
						<div class="mt-step-title uppercase font-grey-cascade"
							style="height: 35px"><?= $case_region_count[$i]["name"] ?></div>


						<div class="mt-step-content font-grey-cascade">
					<?php echo $case_region_count[$i]["count"]; ?></div>
					</div>
										<?php }?>
									</div>
			</div>
		</div>
	</div>
</body>
</html>