<?php
session_start ();
$this->load->view ( 'localization/lang' );
$this->load->view ( 'localization/txt' );
defineLocale ();
defineStrings ();
?>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title><?php echo TITLE; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin RTL Theme #3 for dashboard & statistics"
	name="description" />
<meta content="" name="author" />

<link rel="shortcut icon"
	href="<?=base_url()?>assets/layouts/layout3/img/logo-default.jpg" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">





<script type="text/javascript">
function parentPopupCallback(){
	window.location.reload();
	if(window.opener != null){
		window.opener.location.reload();
	}
}
var scrollup = true;
function scrollUpError(submitType,current_state_code){
	valid= true;
	resetErrorMsgs ();
	if(submitType == "admin"){
	
		valid = adminCheckValid (current_state_code);
	} else {
		valid = checkValid();
	}
	if (valid == false && scrollup == true){
		/*var errorDiv = $('#myForm').first();
		var scrollPos = errorDiv.offset().top;
		$(window).scrollTop(scrollPos);*/
		$("html, body").animate({ scrollTop: $(window).height()/2}, "slow");
	} else if (scrollup == true){
	//to load cases list in case of searching
	window.opener.location.reload();
	}
	scrollup = true;
	return valid;
}
function checkStateTransitionsDatesValid(){
 	var isValid = "";
 	var ordered_states = <?php echo json_encode($case_transactions); ?>;	
	if (ordered_states) {
		var length = ordered_states.length;
		var decision_34_date = document.forms["myForm"]["decision_34_date"].value;
		var decision_46_date= document.forms["myForm"]["decision_46_date"].value;
		var executive_order_date= document.forms["myForm"]["executive_order_date"].value;
		var advertisement_date= document.forms["myForm"]["advertisement_date"].value;

		var breakBigLoop = false;

		//set new values
		for (var i = 0 ; i < length; i++) { 
			var stateCode = ordered_states[i]["state_code"];
			if (stateCode == "DECISION_34" && $('#decision_34_date').is('[readonly="readonly"]') == false) {
				ordered_states [i]["state_date"] = decision_34_date;
			} else if (stateCode == "DECISION_46" && $('#decision_46_date').is('[readonly="readonly"]') == false) {
				ordered_states [i]["state_date"] = decision_46_date;
			} else if (stateCode == "REGISTERED"  && $('#executive_order_date').is('[readonly="readonly"]') == false) {
				ordered_states [i]["state_date"] = executive_order_date;
			}else if (stateCode == "PUBLICLY_ADVERTISED"  && $('#advertisement_date').is('[readonly="readonly"]') == false) {
				ordered_states [i]["state_date"] = advertisement_date;
			}
		}
		
		for (var i = 0 ; i < length; i++) { 
			var stateCode = ordered_states[i]["state_code"];
			var stateDate = ordered_states [i]["state_date"];

			if(stateDate == ""){
				continue;
			}		
			if ((stateCode == "DECISION_34" && $('#decision_34_date').is('[readonly="readonly"]') == false) ||
					(stateCode == "DECISION_46" && $('#decision_46_date').is('[readonly="readonly"]') == false) ||
					(stateCode == "REGISTERED"  && $('#executive_order_date').is('[readonly="readonly"]') == false) ||
					(stateCode == "PUBLICLY_ADVERTISED"  && $('#advertisement_date').is('[readonly="readonly"]') == false)) {
				
				for (var j = i-1 ; j >= 0; j--) { 
					if(ordered_states [j]["state_date"] == "" || ordered_states [j]["state_code"] == "SUSPENDED" || ordered_states [j]["state_code"] == "CLOSED" || ordered_states [j]["state_code"] == ordered_states [i]["state_code"]){
						continue;
					}else{
						if( stateDate < ordered_states [j]["state_date"]){
							isValid = ordered_states [i]["state_code"]+"+"+"less"+"+"+ordered_states [j]["state_code"];
							breakBigLoop = true;
						}
						break;
					}
				}

				if(breakBigLoop)
					break;
			
				for (var j = i+1 ; j < length; j++) { 
					if(ordered_states [j]["state_date"] == "" || ordered_states [j]["state_code"] == "SUSPENDED" || ordered_states [j]["state_code"] == "CLOSED" || ordered_states [j]["state_code"] == ordered_states [i]["state_code"]){
						continue;
					}else{
						if( stateDate > ordered_states [j]["state_date"]){
							isValid = ordered_states [i]["state_code"]+"+"+"greater"+"+"+ordered_states [j]["state_code"];
							breakBigLoop = true;
						}
						break;
					}
				}
			
				if(breakBigLoop)
					break;
		}
		
	}

	}

	return isValid;
}


function isValidDate(dateVar){
	//Format: yyyy/mm/dd
	var dateReg = /^\d{4}\/\d{2}\/\d{2}$/;
	var isV = dateReg.test(dateVar);
	if(!isV){
		return false;
	} 
	
	var str_array = dateVar.split('/');
	var hijriDate =  jqOld.calendars.instance('ummalqura','ar').newDate();
	try {
	   hijriDate = hijriDate.date(str_array[0], str_array[1], str_array[2]);
	}
	catch(err) {
	   return false;
	}
	return true;
}

function checkDates(){

	var todayHj =  jqOld.calendars.instance('ummalqura','ar').newDate();
	var decision_34_date = document.forms["myForm"]["decision_34_date"].value;
	
	if (decision_34_date!= "" && $('#decision_34_date').is('[readonly="readonly"]') == false){

   		var isValidD = isValidDate(decision_34_date);
   		if(!isValidD){
			$("#decision_34_date_tooltip3").css("display","block");
			$("#decision_34_date_tooltip3").css("visibility","visible");

			$("#decision_34_date_tooltip2").css("display","none");
			$("#decision_34_date_tooltip2").css("visibility","none");
		  			
   			return false;
   		}else{
			$("#decision_34_date_tooltip3").css("display","none");
			$("#decision_34_date_tooltip3").css("visibility","none");
   		}
   		
		var str_array = decision_34_date.split('/');
		var hijriDate =  jqOld.calendars.instance('ummalqura','ar').newDate();
		hijriDate = hijriDate.date(str_array[0], str_array[1], str_array[2]);
		if (hijriDate > todayHj) {
			$("#decision_34_date_tooltip2").css("display","block");
			$("#decision_34_date_tooltip2").css("visibility","visible");			
			return false;
		} else{
			$("#decision_34_date_tooltip2").css("display","none");
			$("#decision_34_date_tooltip2").css("visibility","none");
		}  		
	}

   	var decision_46_date= document.forms["myForm"]["decision_46_date"].value;
 	if (decision_46_date!= "" && $('#decision_46_date').is('[readonly="readonly"]') == false){

   		var isValidD = isValidDate(decision_46_date);
   		if(!isValidD){
			$("#decision_46_date_tooltip3").css("display","block");
			$("#decision_46_date_tooltip3").css("visibility","visible");

			$("#decision_46_date_tooltip2").css("display","none");
			$("#decision_46_date_tooltip2").css("visibility","none");
			
   			return false;
   		}else{
			$("#decision_46_date_tooltip3").css("display","none");
			$("#decision_46_date_tooltip3").css("visibility","none");
   		}
   		
		var str_array = decision_46_date.split('/');
		var hijriDate =  jqOld.calendars.instance('ummalqura','ar').newDate();
		hijriDate = hijriDate.date(str_array[0], str_array[1], str_array[2]);
		if (hijriDate > todayHj) {
			$("#decision_46_date_tooltip2").css("display","block");
			$("#decision_46_date_tooltip2").css("visibility","visible");
			return false;
		} else{
			$("#decision_46_date_tooltip2").css("display","none");
			$("#decision_46_date_tooltip2").css("visibility","none");
		}
  		
	}	
   	var executive_order_date= document.forms["myForm"]["executive_order_date"].value;
 	if (executive_order_date!= "" && $('#executive_order_date').is('[readonly="readonly"]') == false){

   		var isValidD = isValidDate(executive_order_date);
   		if(!isValidD){
			$("#executive_order_date_tooltip3").css("display","block");
			$("#executive_order_date_tooltip3").css("visibility","visible");

			$("#executive_order_date_tooltip2").css("display","none");
			$("#executive_order_date_tooltip2").css("visibility","none");
		  			
   			return false;
   		}else{
			$("#executive_order_date_tooltip3").css("display","none");
			$("#executive_order_date_tooltip3").css("visibility","none");
   		}
   		
		var str_array = executive_order_date.split('/');
		var hijriDate =  jqOld.calendars.instance('ummalqura','ar').newDate();
		hijriDate = hijriDate.date(str_array[0], str_array[1], str_array[2]);
		if (hijriDate > todayHj) {
			$("#executive_order_date_tooltip2").css("display","block");
			$("#executive_order_date_tooltip2").css("visibility","visible");
			return false;
		} else{
			$("#executive_order_date_tooltip2").css("display","none");
			$("#executive_order_date_tooltip2").css("visibility","none");
		}

	}
	
	var advertisement_date= document.forms["myForm"]["advertisement_date"].value;
   	if (advertisement_date!= "" && $('#advertisement_date').is('[readonly="readonly"]') == false){
   		var isValidD = isValidDate(advertisement_date);
   		if(!isValidD){
			$("#advertisement_date_tooltip3").css("display","block");
			$("#advertisement_date_tooltip3").css("visibility","visible");

			$("#advertisement_date_tooltip2").css("display","none");
			$("#advertisement_date_tooltip2").css("visibility","none");
		  			
   			return false;
   		}else{
			$("#advertisement_date_tooltip3").css("display","none");
			$("#advertisement_date_tooltip3").css("visibility","none");
   		}
   		
   		var str_array = advertisement_date.split('/');
		var hijriDate =  jqOld.calendars.instance('ummalqura','ar').newDate();
		hijriDate = hijriDate.date(str_array[0], str_array[1], str_array[2]);
  		if (hijriDate > todayHj) {
			$("#advertisement_date_tooltip2").css("display","block");
			$("#advertisement_date_tooltip2").css("visibility","visible");

			return false;
   		} else{
			$("#advertisement_date_tooltip2").css("display","none");
			$("#advertisement_date_tooltip2").css("visibility","none");
   		}

   	}

 	return true;
}

function checkValid() {
	var finishUploading = check_uploading_files();
	if(finishUploading){
		if (textNumViolation())
			return false;
		replacAllArabToEng();
		var contract_number= document.forms["myForm"]["contract_number"].value;
		var client_id_number= document.forms["myForm"]["client_id_number"].value;
		var debtor_name= document.forms["myForm"]["debtor_name"].value;
		var client_type= document.forms["myForm"]["client_type"].value;
		var number_of_late_instalments= document.forms["myForm"]["number_of_late_instalments"].value;
		var due_amount= document.forms["myForm"]["due_amount"].value;
		var remaining_amount= document.forms["myForm"]["remaining_amount"].value;
		var total_debenture_amount = document.forms["myForm"]["total_debenture_amount"].value;
		var obligation_value = document.forms["myForm"]["obligation_value"].value;
		if (contract_number== "" || contract_number<=0 || contract_number.length< 3 || contract_number.length> 15) {
			$("#contract_number_tooltip").css("display","block");
			$("#contract_number_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#contract_number_tooltip").css("display","none");
			$("#contract_number_tooltip").css("visibility","none");
		}
		if (debtor_name== "") {
			$("#debtor_name_tooltip").css("display","block");
			$("#debtor_name_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#debtor_name_tooltip").css("display","none");
			$("#debtor_name_tooltip").css("visibility","none");
		}
		if (client_id_number== "" || client_id_number<=0 || client_id_number.length< 3 || client_id_number.length> 15) {
			$("#client_id_number_tooltip").css("display","block");
			$("#client_id_number_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#client_id_number_tooltip").css("display","none");
			$("#client_id_number_tooltip").css("visibility","none");
		}
		if (client_type== "" ) {
			$("#client_type_tooltip").css("display","block");
			$("#client_type_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#client_type_tooltip").css("display","none");
			$("#client_type_tooltip").css("visibility","none");
		}
		if (remaining_amount== "" || remaining_amount <= 0 || remaining_amount.length >7) {
			$("#remaining_amount_tooltip").css("display","block");
			$("#remaining_amount_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#remaining_amount_tooltip").css("display","none");
			$("#remaining_amount_tooltip").css("visibility","none");
		}
		if (due_amount== "" || due_amount <= 0 || due_amount.length > 7) {
			$("#due_amount_tooltip").css("display","block");
			$("#due_amount_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#due_amount_tooltip").css("display","none");
			$("#due_amount_tooltip").css("visibility","none");
		}
		if (number_of_late_instalments== "" || number_of_late_instalments<0 || number_of_late_instalments.length> 7) {
			$("#number_of_late_instalments_tooltip").css("display","block");
			$("#number_of_late_instalments_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#number_of_late_instalments_tooltip").css("display","none");
			$("#number_of_late_instalments_tooltip").css("visibility","none");
		}
		if (total_debenture_amount== "" || total_debenture_amount <= 0 || total_debenture_amount.length > 7) {
			$("#total_debenture_amount_tooltip").css("display","block");
			$("#total_debenture_amount_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#total_debenture_amount_tooltip").css("display","none");
			$("#total_debenture_amount_tooltip").css("visibility","none");
		}
		if (obligation_value!= "" && (obligation_value < 0 || obligation_value.length >7)) {
			$("#obligation_value_tooltip").css("display","block");
			$("#obligation_value_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#obligation_value_tooltip").css("display","none");
			$("#obligation_value_tooltip").css("visibility","none");
		}
		
		var validDates = checkDates();
		if( validDates == false){ return false;}


		 setNullFields();
		 nullifyUploadFields();
	 }else{
		return false;
	 }
	 $('#loading').removeClass('hidden');
}
function resetErrorMsgs (){
	$("#contract_number_tooltip").css("display","none");
	$("#debtor_name_tooltip").css("display","none");
	$("#client_id_number_tooltip").css("display","none");
	$("#client_type_tooltip").css("display","none");
	$("#remaining_amount_tooltip").css("display","none");
	$("#due_amount_tooltip").css("display","none");
	$("#number_of_late_instalments_tooltip").css("display","none");
	$("#total_debenture_amount_tooltip").css("display","none");
	$("#obligation_value_tooltip").css("display","none");
	$("#executive_order_number_tooltip").css("display","none");
	$("#order_number_tooltip").css("display","none");
	$("#circle_number_tooltip").css("display","none");
	$("#decision_34_number_tooltip").css("display","none");
	$("#debenture_tooltip").css("display","none");
	$("#id_tooltip").css("display","none");
	$("#executive_order_number_tooltip").css("display","none");
	$("#order_number_tooltip").css("display","none");
	$("#circle_number_tooltip").css("display","none");
	$("#decision_34_number_tooltip").css("display","none");
	$("#invoice_file_tooltip").css("display","none");
	$("#closing_reason_tooltip").css("display","none");
	$("#notes_tooltip").css("display","none");
	$("#suspend_reason_code_tooltip").css("display","none");
}
function adminCheckValid (current_state_code){
	var finishUploading = check_uploading_files();
	if(finishUploading){
		if (textNumViolation())
			return false;
		replacAllArabToEng();
		
		var contract_number= document.forms["myForm"]["contract_number"].value;
		var client_id_number= document.forms["myForm"]["client_id_number"].value;
		var debtor_name= document.forms["myForm"]["debtor_name"].value;
		var client_type= document.forms["myForm"]["client_type"].value;
		var number_of_late_instalments= document.forms["myForm"]["number_of_late_instalments"].value;
		var due_amount= document.forms["myForm"]["due_amount"].value;
		var remaining_amount= document.forms["myForm"]["remaining_amount"].value;
		var total_debenture_amount = document.forms["myForm"]["total_debenture_amount"].value;
		var obligation_value = document.forms["myForm"]["obligation_value"].value;
		if (contract_number== "" || contract_number<=0 || contract_number.length< 3 || contract_number.length> 15) {
			$("#contract_number_tooltip").css("display","block");
			$("#contract_number_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#contract_number_tooltip").css("display","none");
			$("#contract_number_tooltip").css("visibility","none");
		}
		if (debtor_name== "") {
			$("#debtor_name_tooltip").css("display","block");
			$("#debtor_name_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#debtor_name_tooltip").css("display","none");
			$("#debtor_name_tooltip").css("visibility","none");
		}
		if (client_id_number== "" || client_id_number<=0 || client_id_number.length< 3 ||client_id_number.length > 15) {
			$("#client_id_number_tooltip").css("display","block");
			$("#client_id_number_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#client_id_number_tooltip").css("display","none");
			$("#client_id_number_tooltip").css("visibility","none");
		}
		if (client_type== "" ) {
			$("#client_type_tooltip").css("display","block");
			$("#client_type_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#client_type_tooltip").css("display","none");
			$("#client_type_tooltip").css("visibility","none");
		}
		if (remaining_amount== "" || remaining_amount <= 0 || remaining_amount.length >7) {
			$("#remaining_amount_tooltip").css("display","block");
			$("#remaining_amount_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#remaining_amount_tooltip").css("display","none");
			$("#remaining_amount_tooltip").css("visibility","none");
		}
		if (due_amount== "" || due_amount < 0 || due_amount.length > 7) {
			$("#due_amount_tooltip").css("display","block");
			$("#due_amount_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#due_amount_tooltip").css("display","none");
			$("#due_amount_tooltip").css("visibility","none");
		}
		if (number_of_late_instalments== "" || number_of_late_instalments<0 || number_of_late_instalments.length> 7) {
			$("#number_of_late_instalments_tooltip").css("display","block");
			$("#number_of_late_instalments_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#number_of_late_instalments_tooltip").css("display","none");
			$("#number_of_late_instalments_tooltip").css("visibility","none");
		}
		if (total_debenture_amount== "" || total_debenture_amount <= 0 || total_debenture_amount.length > 7) {
			$("#total_debenture_amount_tooltip").css("display","block");
			$("#total_debenture_amount_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#total_debenture_amount_tooltip").css("display","none");
			$("#total_debenture_amount_tooltip").css("visibility","none");
		}
		if (obligation_value!= "" && (obligation_value < 0 || obligation_value.length >7)) {
			$("#obligation_value_tooltip").css("display","block");
			$("#obligation_value_tooltip").css("visibility","visible");
			return false;
		} else{
			$("#obligation_value_tooltip").css("display","none");
			$("#obligation_value_tooltip").css("visibility","none");
		}

		var executive_order_number = document.forms["myForm"]["executive_order_number"].value;
		
		if (executive_order_number != "" && ( executive_order_number <= 0 || executive_order_number.length <3 || executive_order_number.length >15)) {
		$("#executive_order_number_tooltip").css("display","block");
		$("#executive_order_number_tooltip").css("visibility","visible");
		return false;
		} else{
		$("#executive_order_number_tooltip").css("display","none");
		$("#executive_order_number_tooltip").css("visibility","none");
		}
		var order_number = document.forms["myForm"]["order_number"].value;
		if (order_number != "" && (order_number<=0 || order_number.length > 15)) {
		$("#order_number_tooltip").css("display","block");
		$("#order_number_tooltip").css("visibility","visible");
		return false;
		} else{
		$("#order_number_tooltip").css("display","none");
		$("#order_number_tooltip").css("visibility","none");
		}
		var circle_number = document.forms["myForm"]["circle_number"].value;
		if (circle_number != "" && (circle_number<=0 || circle_number.length > 15)) {
		$("#circle_number_tooltip").css("display","block");
		$("#circle_number_tooltip").css("visibility","visible");
		return false;
		} else{
		$("#circle_number_tooltip").css("display","none");
		$("#circle_number_tooltip").css("visibility","none");
		}
		var decision_34_number = document.forms["myForm"]["decision_34_number"].value;
		if (decision_34_number != "" && (decision_34_number <=0 || decision_34_number.length <3 || decision_34_number.length >15)) {
		$("#decision_34_number_tooltip").css("display","block");
		$("#decision_34_number_tooltip").css("visibility","visible");
		return false;
		} else{
		$("#decision_34_number_tooltip").css("display","none");
		$("#decision_34_number_tooltip").css("visibility","none");
		}
		
		var validDates = checkDates();
		if( validDates == false){ return false;}
		
		if (current_state_code != "SUSPENDED"){
				var debenture= document.forms["myForm"]["debenture"].value;
				var debenture_value = document.forms["myForm"]["debenture_value"].value;
				var debenture_hidden = document.forms["myForm"]["debenture_hidden"].value;
			if (debenture== "" && debenture_value == "" && debenture_hidden == "") {
				scrollup = false;
				$("#debenture_tooltip").css("display","block");
				$("#debenture_tooltip").css("visibility","visible");
			return false;
			} else{
				$("#debenture_tooltip").css("display","none");
				$("#debenture_tooltip").css("visibility","none");
			}

			var id = document.forms["myForm"]["id"].value;
			var id_value = document.forms["myForm"]["id_value"].value;
			var id_hidden = document.forms["myForm"]["id_hidden"].value;
		if (id== "" && id_value == "" && id_hidden == "") {
			scrollup = false;
			$("#id_tooltip").css("display","block");
			$("#id_tooltip").css("visibility","visible");
		return false;
		} else{
			$("#id_tooltip").css("display","none");
			$("#id_tooltip").css("visibility","none");
		}
		
		 if (current_state_code != "UNDER_REVIEW" && current_state_code != "MODIFICATION_REQUIRED" && current_state_code != "ASSIGNED_TO_LAWYER"){
			var executive_order_number = document.forms["myForm"]["executive_order_number"].value;
			if (executive_order_number == "" || executive_order_number <= 0 || executive_order_number.length <3 || executive_order_number.length >15) {
				$("#executive_order_number_tooltip").css("display","block");
				$("#executive_order_number_tooltip").css("visibility","visible");
				return false;
			} else{
				$("#executive_order_number_tooltip").css("display","none");
				$("#executive_order_number_tooltip").css("visibility","none");
			}
			var order_number = document.forms["myForm"]["order_number"].value;
			if (order_number != "" && (order_number<=0 || order_number.length > 15)) {
			$("#order_number_tooltip").css("display","block");
			$("#order_number_tooltip").css("visibility","visible");
			return false;
			} else{
			$("#order_number_tooltip").css("display","none");
			$("#order_number_tooltip").css("visibility","none");
			}
			var circle_number = document.forms["myForm"]["circle_number"].value;
			if (circle_number == "" || circle_number<=0 || circle_number.length > 15) {
				$("#circle_number_tooltip").css("display","block");
				$("#circle_number_tooltip").css("visibility","visible");
				return false;
			} else{
				$("#circle_number_tooltip").css("display","none");
				$("#circle_number_tooltip").css("visibility","none");
			}
	   
		} if (current_state_code != "UNDER_REVIEW" && current_state_code != "MODIFICATION_REQUIRED" && current_state_code != "ASSIGNED_TO_LAWYER" && current_state_code != "REGISTERED"){
			var decision_34_number = document.forms["myForm"]["decision_34_number"].value;
			var decision_34_date = document.forms["myForm"]["decision_34_date"].value;
			if (decision_34_number != "" && ( decision_34_number <=0 || decision_34_number.length <3 || decision_34_number.length >15)) {
				$("#decision_34_number_tooltip").css("display","block");
				$("#decision_34_number_tooltip").css("visibility","visible");
				return false;
			} else{
				$("#decision_34_number_tooltip").css("display","none");
				$("#decision_34_number_tooltip").css("visibility","none");
			}
		} if (current_state_code == "PUBLICLY_ADVERTISED" || current_state_code == "DECISION_46" || current_state_code == "CLOSED"){
			var invoice_file= document.forms["myForm"]["invoice_file"].value;
			var invoice_file_value= document.forms["myForm"]["invoice_file_value"].value;
			var advertisement_date= document.forms["myForm"]["advertisement_date"].value;
			if (invoice_file== "" && invoice_file_value=="") {
				scrollup = false;
				$("#invoice_file_tooltip").css("display","block");
				$("#invoice_file_tooltip").css("visibility","visible");
				return false;
			} else{
				$("#invoice_file_tooltip").css("display","none");
				$("#invoice_file_tooltip").css("visibility","none");
			}
		} if (current_state_code == "DECISION_46" || current_state_code == "CLOSED"){
			var decision_46_date= document.forms["myForm"]["decision_46_date"].value;
	  
			}
		if (current_state_code == "CLOSED"){
				var closing_reason= document.forms["myForm"]["closing_reason"].value;
			if (closing_reason== "") {
				$("#closing_reason_tooltip").css("display","block");
				$("#closing_reason_tooltip").css("visibility","visible");
				return false;
			} else{
				$("#closing_reason_tooltip").css("display","none");
				$("#closing_reason_tooltip").css("visibility","none");
			}
		} if (current_state_code == "MODIFICATION_REQUIRED"){
			var notes = document.forms["myForm"]["notes"].value;
			if (notes== "") {
				$("#notes_tooltip").css("display","block");
				$("#notes_tooltip").css("visibility","visible");
				return false;
			} else{
				$("#notes_tooltip").css("display","none");
				$("#notes_tooltip").css("visibility","none");
			}
		} if(current_state_code == "SUSPENDED"){
			var suspend_reason_code= document.forms["myForm"]["suspend_reason_code"].value;
					 
			if (suspend_reason_code== "") {
				$("#suspend_reason_code_tooltip").css("display","block");
				$("#suspend_reason_code_tooltip").css("visibility","visible");
					return false;
			} else{
				$("#suspend_reason_code_tooltip").css("display","none");
				$("#suspend_reason_code_tooltip").css("visibility","none");
			}
		}
		}

/* 		var isDateSeqValid = checkStateTransitionsDatesValid();
		if(isDateSeqValid.length > 0){
			var messsage = prepareDateErrorMsg(isDateSeqValid);
			$("#show_error").text(messsage);
			$("#show_error").css("display","block");
			$("#show_error").css("visibility","visible");
			return false;
		}
		else{
			$("#show_error").css("display","none");
			$("#show_error").css("visibility","none");
		}
*/

		 setNullFields();
		 nullifyUploadFields();
		 
			
	}else{
		return false;
	}
	$('#loading').removeClass('hidden');
}

function nullifyUploadFields(){
	if(_("consignment_image_hidden") !== null && _("consignment_image_hidden") !== undefined &&  _("consignment_image_hidden").value != ""){
		_("consignment_image").value = ""; 
	}
	if(_("debenture_hidden") !== null && _("debenture_hidden") !== undefined && _("debenture_hidden").value != ""){
		_("debenture").value = ""; 
	}
	if(_("id_hidden") !== null && _("id_hidden") !== undefined && _("id_hidden").value != ""){
		_("id").value = "";
	}
	if(_("contract_hidden") !== null && _("contract_hidden") !== undefined && _("contract_hidden").value != ""){ 
		_("contract").value = ""; 
	}
	if(_("others_hidden") !== null && _("others_hidden") !== undefined && _("others_hidden").value != ""){
		_("others").value = ""; 
	}
	if(_("advertisement_file_hidden") !== null && _("advertisement_file_hidden") !== undefined && _("advertisement_file_hidden").value != ""){
		_("advertisement_file").value = "";
	}
	if(_("invoice_file_hidden") !== null && _("invoice_file_hidden") !== undefined && _("invoice_file_hidden").value != ""){
		_("invoice_file").value = ""; 
	}
	if(_("referral_paper_hidden") !== null && _("referral_paper_hidden") !== undefined && _("referral_paper_hidden").value != ""){
		_("referral_paper").value = ""; 
	}
	if(_("suspend_file_hidden") !== null && _("suspend_file_hidden") !== undefined && _("suspend_file_hidden").value != ""){
		_("suspend_file").value = ""; 
	}
}

function check_uploading_files(){
	if((_("consignment_image_uploadingFiles") !== null && _("consignment_image_uploadingFiles") !== undefined && _("consignment_image_uploadingFiles").value == "no_files")&&
	   (_("debenture_uploadingFiles") !== null && _("debenture_uploadingFiles") !== undefined && _("debenture_uploadingFiles").value == "no_files")	&&
	   (_("id_uploadingFiles") !== null && _("id_uploadingFiles") !== undefined && _("id_uploadingFiles").value == "no_files") &&
	   (_("contract_uploadingFiles") !== null && _("contract_uploadingFiles") !== undefined && _("contract_uploadingFiles").value == "no_files") &&
	   (_("others_uploadingFiles") !== null && _("others_uploadingFiles") !== undefined && _("others_uploadingFiles").value == "no_files") &&
	   (_("advertisement_file_uploadingFiles") !== null && _("advertisement_file_uploadingFiles") !== undefined && _("advertisement_file_uploadingFiles").value == "no_files") &&
	   (_("invoice_file_uploadingFiles") !== null && _("invoice_file_uploadingFiles") !== undefined && _("invoice_file_uploadingFiles").value == "no_files") &&
	   (_("referral_paper_uploadingFiles") !== null && _("referral_paper_uploadingFiles") !== undefined && _("referral_paper_uploadingFiles").value == "no_files") &&
	   (_("suspend_file_uploadingFiles") !== null && _("suspend_file_uploadingFiles") !== undefined && _("suspend_file_uploadingFiles").value == "no_files")
	   ){
			return true;
	   }else{
		   bootbox.dialog({
				message: '<p><i class="fa fa-spin fa-spinner"></i> '+"<?= WAIT_ATTACH?>"+' </p>',
				buttons: {
					confirm: {
						label: "<?= AGREE?>",
						className: 'green'
					}
				}
			});
		   return false;
	   }
}


function setNullFields(){
	
	var nullFields = "";
 	var case_details = <?php echo json_encode($case_details); ?>;	
	if (case_details) {

		var contract_number = document.forms["myForm"]["contract_number"].value;
		if(contract_number == "" && $('#contract_number').is('[readonly="readonly"]') == false && case_details["contract_number"] != ""){
			nullFields = nullFields+ "contract_number,";
		}
		var client_id_number = document.forms["myForm"]["client_id_number"].value;
		if(client_id_number == "" && $('#client_id_number').is('[readonly="readonly"]') == false && case_details["client_id_number"] != ""){
			nullFields = nullFields+ "client_id_number,";
		}
		var client_name = document.forms["myForm"]["client_name"].value;
		if(client_name == "" && $('#client_name').is('[readonly="readonly"]') == false && case_details["client_name"] != ""){
			nullFields = nullFields+ "client_name,";
		}
		var client_type = document.forms["myForm"]["client_type"].value;
		if(client_type == "" && $('#client_type').is('[readonly="readonly"]') == false && case_details["client_type"] != ""){
			nullFields = nullFields+ "client_type,";
		}
		var number_of_late_instalments = document.forms["myForm"]["number_of_late_instalments"].value;
		if(number_of_late_instalments == "" && $('#number_of_late_instalments').is('[readonly="readonly"]') == false && case_details["number_of_late_instalments"] != ""){
			nullFields = nullFields+ "number_of_late_instalments,";
		}
		var due_amount = document.forms["myForm"]["due_amount"].value;
		if(due_amount == "" && $('#due_amount').is('[readonly="readonly"]') == false && case_details["due_amount"] != ""){
			nullFields = nullFields+ "due_amount,";
		}
		var remaining_amount = document.forms["myForm"]["remaining_amount"].value;
		if(remaining_amount == "" && $('#remaining_amount').is('[readonly="readonly"]') == false && case_details["remaining_amount"] != ""){
			nullFields = nullFields+ "remaining_amount,";
		}
		var total_debenture_amount = document.forms["myForm"]["total_debenture_amount"].value;
		if(total_debenture_amount == "" && $('#total_debenture_amount').is('[readonly="readonly"]') == false && case_details["total_debenture_amount"] != ""){
			nullFields = nullFields+ "total_debenture_amount,";
		}
		var debtor_name = document.forms["myForm"]["debtor_name"].value;
		if(debtor_name == "" && $('#debtor_name').is('[readonly="readonly"]') == false && case_details["debtor_name"] != ""){
			nullFields = nullFields+ "debtor_name,";
		}
		var obligation_value = document.forms["myForm"]["obligation_value"].value;
		if(obligation_value == "" && $('#obligation_value').is('[readonly="readonly"]') == false && case_details["obligation_value"] != ""){
			nullFields = nullFields+ "obligation_value,";
		}
		var executive_order_number = document.forms["myForm"]["executive_order_number"].value;
		if(executive_order_number == "" && $('#executive_order_number').is('[readonly="readonly"]') == false && case_details["executive_order_number"] != ""){
			nullFields = nullFields+ "executive_order_number,";
		}
		var order_number = document.forms["myForm"]["order_number"].value;
		if(order_number == "" && $('#order_number').is('[readonly="readonly"]') == false && case_details["order_number"] != ""){
			nullFields = nullFields+ "order_number,";
		}
		var circle_number = document.forms["myForm"]["circle_number"].value;
		if(circle_number == "" && $('#circle_number').is('[readonly="readonly"]') == false && case_details["circle_number"] != ""){
			nullFields = nullFields+ "circle_number,";
		}
		var executive_order_date = document.forms["myForm"]["executive_order_date"].value;
		if(executive_order_date == "" && $('#executive_order_date').is('[readonly="readonly"]') == false && case_details["executive_order_date"] != ""){
			nullFields = nullFields+ "executive_order_date,";
		}
		var decision_34_number = document.forms["myForm"]["decision_34_number"].value;
		if(decision_34_number == "" && $('#decision_34_number').is('[readonly="readonly"]') == false && case_details["decision_34_number"] != ""){
			nullFields = nullFields+ "decision_34_number,";
		}
		var decision_34_date = document.forms["myForm"]["decision_34_date"].value;
		if(decision_34_date == "" && $('#decision_34_date').is('[readonly="readonly"]') == false && case_details["decision_34_date"] != ""){
			nullFields = nullFields+ "decision_34_date,";
		}
		var closing_reason = document.forms["myForm"]["closing_reason"].value;
		if(closing_reason == "" && $('#closing_reason').is('[readonly="readonly"]') == false && case_details["closing_reason"] != ""){
			nullFields = nullFields+ "closing_reason,";
		}
		var notes = document.forms["myForm"]["notes"].value;
		if(notes == "" && $('#notes').is('[readonly="readonly"]') == false && case_details["notes"] != ""){
			nullFields = nullFields+ "notes,";
		}

		var advertisement_date = document.forms["myForm"]["advertisement_date"].value;
		if(advertisement_date == "" && $('#advertisement_date').is('[readonly="readonly"]') == false && case_details["advertisement_date"] != ""){
			nullFields = nullFields+ "advertisement_date,";
		}
		var decision_46_date = document.forms["myForm"]["decision_46_date"].value;
		if(decision_46_date == "" && $('#decision_46_date').is('[readonly="readonly"]') == false && case_details["decision_46_date"] != ""){
			nullFields = nullFields+ "decision_46_date,";
		}
		
		
		document.forms["myForm"]["nullFields"].value = nullFields; 
	}
}


function prepareDateErrorMsg(strCodes){
	var data = '<?php echo INVALID_DATE_SEQUENCE; ?>'+': ';
	var str_array = strCodes.split('+');
	
	if(str_array[0] == "DECISION_34"){
		data+= '<?php echo DECISION_34_DATE; ?>';
	} else 	if(str_array[0] == "DECISION_46"){
		data+= '<?php echo DECISION_46_DATE; ?>';
	} else 	if(str_array[0] == "REGISTERED"){
		data+= '<?php echo EXECUTIVE_ORDER_DATE; ?>';
	} else 	if(str_array[0] == "PUBLICLY_ADVERTISED"){
		data+= '<?php echo ADVERTISMENT_DATE; ?>';
	}
	
	data+=' <?php echo MUST_BE; ?> ';
	
	if(str_array[1] == "less"){
		data+= '<?php echo GREATER_THAN; ?> ';
	} else 	if(str_array[1] == "greater"){
		data+= '<?php echo LESS_THAN; ?> ';
	}

	if(str_array[2] == "DECISION_34"){
		data+= '<?php echo DECISION_34_DATE; ?>';
	} else 	if(str_array[2] == "DECISION_46"){
		data+= '<?php echo DECISION_46_DATE; ?>';
	} else 	if(str_array[2] == "REGISTERED"){
		data+= '<?php echo EXECUTIVE_ORDER_DATE; ?>';
	} else 	if(str_array[2] == "PUBLICLY_ADVERTISED"){
		data+= '<?php echo ADVERTISMENT_DATE; ?>';
	} else {
		if(str_array[1] == "less"){
			data+= '<?php echo STATE_BEFORE; ?> ';
		} else 	if(str_array[1] == "greater"){
			data+= '<?php echo STATE_AFTER; ?> ';
		}
	}
	
	return data;
}


function commentKeyUp(){
	var comment = document.getElementById("comment");
	var commentSubmit = document.getElementById("comment_submit");
	if (/\S/.test(comment.value)){
		commentSubmit.disabled = false;
	} else{
		commentSubmit.disabled = true;
	}
	
}

  function popup_window(url,action) {
	var finishUploading = check_uploading_files();
	if(finishUploading){
		  w = 640;
		  
		  if (action == "REGISTER"){
			  h = 360;
		  }else if (action == "ASSIGN_TO_LAWYER" || action == "SUSPEND" || action == "SUSPEND_REQUEST") {
			  h = 350;
		  }else if (action == "ADVERTISE"){
			  h = 330;
		  } else if( action == "MAKE_DECISION_46" ) {
			  h = 200;
		  } else if( action == "MAKE_DECISION_34") {
			  h = 320;
		  } else if( action == "REASSIGN_TO_LAWYER") {
			  h = 250;
		  }else {
			  h = 230;
		  }
		  var left = (screen.width/2)-(w/2);
		  var top = (screen.height/2)-(h/2);
		  if (!window.memwin || window.memwin.closed) {
			window.memwin= window.open(url,'actionWin','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width='+w+', height='+h+', top='+top+', left='+left+',directories=no,location=no');
		  }
		  window.memwin.focus();
	  }else{
		return false;
	  }
	  
  }
  
  function suspendSubmit(case_id, customer_id){
	var finishUploading = check_uploading_files();
	if(finishUploading){
		document.getElementById('myForm').action = "<?=site_url('case_management/manage_case')?>"+"/"+case_id+"/RESUME";
		document.forms["myForm"]["customer_id"].value = customer_id;
		document.forms["myForm"]["contract_number"].value = "";
		document.forms["myForm"]["client_id_number"].value = "";
		document.forms["myForm"]["client_name"].value = "";
		document.forms["myForm"]["client_type"].value = "";
		document.forms["myForm"]["number_of_late_instalments"].value = "";
		document.forms["myForm"]["due_amount"].value = "";
		document.forms["myForm"]["remaining_amount"].value = "";
		document.forms["myForm"]["total_debenture_amount"].value = "";
		document.forms["myForm"]["others_count"].value = "";
		document.forms["myForm"]["debtor_name"].value = "";
		document.forms["myForm"]["obligation_value"].value = "";
		document.forms["myForm"]["executive_order_number"].value = "";
		document.forms["myForm"]["order_number"].value = "";
		document.forms["myForm"]["circle_number"].value = "";
		document.forms["myForm"]["executive_order_date"].value = "";
		document.forms["myForm"]["decision_34_number"].value = "";
		document.forms["myForm"]["decision_34_date"].value = " ";
		document.forms["myForm"]["closing_reason"].value = "";
		document.forms["myForm"]["lawyer_id"].value = "";
		document.forms["myForm"]["notes"].value = "";
		document.forms["myForm"]["suspend_reason_code"].value = "";
		document.forms["myForm"]["advertisement_date"].value = " ";
		document.forms["myForm"]["consignment_image"].value = "";
		document.forms["myForm"]["debenture"].value = "";
		document.forms["myForm"]["referral_paper"].value = "";
		document.forms["myForm"]["suspend_file"].value = "";
		document.forms["myForm"]["id"].value = "";
		document.forms["myForm"]["contract"].value = "";
		document.forms["myForm"]["others"].value = "";
		document.forms["myForm"]["decision_34_file"].value = "";
		document.forms["myForm"]["decision_46_date"].value = " ";
		document.forms["myForm"]["advertisement_file"].value = "";
		document.forms["myForm"]["invoice_file"].value = "";
		document.forms["myForm"]["current_state_code"].value = "";
		document.getElementById('myForm').submit();
	}
}

  function reopenSubmit(case_id){
	var finishUploading = check_uploading_files();
	if(finishUploading){
		document.getElementById('myForm').action = "<?=site_url('case_management/manage_case')?>"+"/"+case_id+"/REOPEN";
		document.forms["myForm"]["customer_id"].value = " ";
		document.forms["myForm"]["contract_number"].value = "";
	    document.forms["myForm"]["client_id_number"].value = "";
		document.forms["myForm"]["client_name"].value = "";
		document.forms["myForm"]["client_type"].value = "";
		document.forms["myForm"]["number_of_late_instalments"].value = "";
		document.forms["myForm"]["due_amount"].value = "";
		document.forms["myForm"]["remaining_amount"].value = "";
		document.forms["myForm"]["total_debenture_amount"].value = "";
		document.forms["myForm"]["others_count"].value = "";
		document.forms["myForm"]["debtor_name"].value = "";
		document.forms["myForm"]["obligation_value"].value = "";
		document.forms["myForm"]["executive_order_number"].value = "";
		document.forms["myForm"]["order_number"].value = "";
		document.forms["myForm"]["circle_number"].value = "";
		document.forms["myForm"]["executive_order_date"].value = "";
		document.forms["myForm"]["decision_34_number"].value = "";
		document.forms["myForm"]["decision_34_date"].value = " ";
	    document.forms["myForm"]["closing_reason"].value = "";
		document.forms["myForm"]["lawyer_id"].value = "";
		document.forms["myForm"]["notes"].value = "";
		document.forms["myForm"]["suspend_reason_code"].value = "";
		document.forms["myForm"]["advertisement_date"].value = " ";
		document.forms["myForm"]["consignment_image"].value = "";
		document.forms["myForm"]["debenture"].value = "";
		document.forms["myForm"]["referral_paper"].value = "";
		document.forms["myForm"]["suspend_file"].value = "";
		document.forms["myForm"]["id"].value = "";
		document.forms["myForm"]["contract"].value = "";
		document.forms["myForm"]["others"].value = "";
		document.forms["myForm"]["decision_34_file"].value = "";
		document.forms["myForm"]["decision_46_date"].value = " ";
		document.forms["myForm"]["advertisement_file"].value = "";
		document.forms["myForm"]["invoice_file"].value = "";
		document.forms["myForm"]["current_state_code"].value = "";
		document.getElementById('myForm').submit();
		}
	}

  function changeHiddenInput (objDropDown)
  {
      var objHidden = document.getElementById("closing_reason");
      objHidden.value = objDropDown.value; 
  }
  
  </script>

<style type="text/css">
.transition-button {
	margin-top: 5px;
	margin-bottom: 5px;
}
</style>

</head>
<body class="page-container-bg-solid">
<div id="loading" class="hidden">
	<div id="loading-image">
	  <img  src="<?php echo base_url(); ?>images/Ajax-loader2.gif" alt="Loading..." />
	  
	  
	</div>
	</div>
	<div class="page-wrapper">
<?php
if ($user_type_code == "ADMIN") {
	if ($search_mode == "1") {
		$this->load->view ( 'office_admin/main_header' );
	} else {
		$this->load->view ( 'office_admin/office_admin_header' );
	}
	$this->load->view ( 'utils/date_scripts' );
} else {
	if ($search_mode == "1") {
		$this->load->view ( 'office_admin/main_header' );
	} else {
		$this->load->view ( 'secretary_lawyer/secretary_header' );
	}
}

?>
				<div class="page-wrapper-row full-height">
			<div class="page-wrapper-middle">
				<!-- BEGIN CONTAINER -->
				<div class="page-container">
					<!-- BEGIN CONTENT -->
					<div class="page-content-wrapper">
						<!-- BEGIN CONTENT BODY -->
						<!-- BEGIN PAGE HEAD-->
						<div class="page-head">
							<div class="container">
								<div class="col-md-14">
									<div class="portlet box green">
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-gift"></i>
												<?php
												echo CASE_ID . ' ' . $case_details ["case_id"];
												?>
											</div>
										</div>
										<div class="portlet-body form">
											<br>
										<?php $this->load->view('cases/progress_bar');?>
								<br>

											<div id="show_error" class="form-group"
												style="display: none; color: red;"></div>

											<br>
											<!-- BEGIN FORM-->
											<form id="myForm" role="form" method="post"
												enctype="multipart/form-data"
												<?php if ($user_type_code == "CUSTOMER" && $case_details["current_state_code"] =="MODIFICATION_REQUIRED") { ?>
												onsubmit="javascript:return scrollUpError('','');"
												action="<?=site_url('case_management/manage_case/'.$case_details ["case_id"].'/'."MODIFICATIONS_DONE")?>"
												<?php }else if($user_type_code =="ADMIN") {?>
												onsubmit="javascript:return scrollUpError('admin','<?php echo $case_details["current_state_code"] ?>');"
												action="<?=site_url('office_admin_case_management/edit_case'.'/'.$case_details ["case_id"])?>"
												<?} else {?> action="" <?}?> class="form-horizontal">
												<input type="hidden" name="customer_id"
													value="<?php echo $case_details["customer_id"]?>"> <input
													type="hidden" name="debenture" value=""> <input
													type="hidden" name="id" value=" "> <input type="hidden"
													name="others_count" value=" "> <input type="hidden"
													name="contract" value=" "> <input type="hidden"
													name="decision_34_file" value=" "> <input type="hidden"
													name="advertisement_file" value=" "> <input type="hidden"
													name="invoice_file" value=" "> <input type="hidden"
													name="referral_paper" value=" "><input type="hidden"
													name="suspend_file" value=" "><input type="hidden"
													name="suspend_reason_code" value=""> <input type="hidden"
													id="consignment_image_uploadingFiles"
													name="consignment_image_uploadingFiles" value="no_files"> <input
													type="hidden" id="debenture_uploadingFiles"
													name="debenture_uploadingFiles" value="no_files"> <input
													type="hidden" id="referral_paper_uploadingFiles"
													name="referral_paper_uploadingFiles" value="no_files"> <input
													type="hidden" id="suspend_file_uploadingFiles"
													name="suspend_file_uploadingFiles" value="no_files"> <input
													type="hidden" id="id_uploadingFiles"
													name="id_uploadingFiles" value="no_files"> <input
													type="hidden" id="contract_uploadingFiles"
													name="contract_uploadingFiles" value="no_files"> <input
													type="hidden" id="others_uploadingFiles"
													name="others_uploadingFiles" value="no_files"> <input
													type="hidden" id="advertisement_file_uploadingFiles"
													name="advertisement_file_uploadingFiles" value="no_files">
												<input type="hidden" id="invoice_file_uploadingFiles"
													name="invoice_file_uploadingFiles" value="no_files">
													<?php if($case_details["current_state_code"] =="SUSPENDED" || $case_details["current_state_code"] =="CLOSED") {?>
													<input type="hidden" name="consignment_image" value=""> <input
													type="hidden" id="others_count" name="others_count"
													value=""> <input type="hidden" name="debenture" value=""> <input
													type="hidden" name="referral_paper" value=""> <input
													type="hidden" name="suspend_file" value=""> <input
													type="hidden" name="id" value=" "> <input type="hidden"
													name="contract" value=""> <input type="hidden"
													name="others" value=""><input type="hidden"
													name="lawyer_id" id="lawyer_id" value=""> <input
													type="hidden" name="decision_34_file" id="decision_34_file"
													value=""> <input type="hidden" name="advertisement_file"
													value=""> <input type="hidden" name="invoice_file" value="">
												<input type="hidden" name="current_state_code"
													id="current_state_code" value="">
													
													<?php }?>
												<div class="form-body">
													<input type="hidden" id="nullFields" name="nullFields"
														value="">
													<div class="row">
														<?php if ($user_type_code != "CUSTOMER") {?>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo CUSTOMER; ?>
																</label>
																<div class="col-md-6">
																	<input type="hidden" name="customer_id"
																		value="<?php echo $case_details["customer_id"]?>"> <input
																		readonly="readonly" type="text" class="form-control "
																		value="<?php echo $case_details["customer_name"]?>">
																</div>
															</div>
														</div>
														<?php }?>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo CONTRACT_NUMBER; ?>
																<?php if ($user_type_code == "ADMIN") {?>
																<span class="required"> * </span>
																	<?php }?>
																</label>
																<div class="col-md-6">
																	<input
																		<?php if (($user_type_code != "CUSTOMER" || $case_details["current_state_code"]!="MODIFICATION_REQUIRED") && $user_type_code != "ADMIN") { ?>
																		readonly="readonly" <?}?> type="text" act="yes"
																		onkeydown="numberKeyDown(event)"
																		class="form-control no-spinners"
																		name="contract_number"
																		value="<?php echo $case_details["contract_number"]?>"
																		onfocus="javascript:removeTooltip('contract_number_tooltip');">
																	<span id="contract_number_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo CASE_NUMBER_INVALID; ?></span>

																</div>
															</div>
														</div>
													<?php if ($user_type_code != "CUSTOMER") {?>
													</div>
													<div class="row">
													<?php }?>
													<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo DEBTOR_NAME; ?>
																<?php if ($user_type_code == "ADMIN") {?>
																<span class="required"> * </span>
																	<?php }?>
																</label>
																<div class="col-md-6">
																	<input
																		<?php if (($user_type_code != "CUSTOMER" || $case_details["current_state_code"]!="MODIFICATION_REQUIRED") && $user_type_code != "ADMIN") { ?>
																		readonly="readonly" <?}?> type="text"
																		class="form-control " name="debtor_name"
																		id="debtor_name"
																		value="<?php echo $case_details["debtor_name"]?>"
																		onfocus="javascript:removeTooltip('debtor_name_tooltip');">
																	<span id="debtor_name_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
													<?php if ($user_type_code == "CUSTOMER") {?>
													</div>
													<div class="row">
													<?php }?>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo CLIENT_NAME; ?>
																</label>
																<div class="col-md-6">
																	<input
																		<?php if (($user_type_code != "CUSTOMER" || $case_details["current_state_code"]!="MODIFICATION_REQUIRED") && $user_type_code != "ADMIN") { ?>
																		readonly="readonly" <?}?> type="text"
																		class="form-control " name="client_name"
																		value="<?php echo $case_details["client_name"]?>">
																</div>
															</div>
														</div>
													<?php if ($user_type_code != "CUSTOMER") {?>
													</div>
													<div class="row">
													<?php }?>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo CLIENT_ID_NUMBER; ?>
																<?php if ($user_type_code == "ADMIN") {?>
																<span class="required"> * </span>
																	<?php }?>
																</label>
																<div class="col-md-6">
																	<input
																		<?php if (($user_type_code != "CUSTOMER" || $case_details["current_state_code"]!="MODIFICATION_REQUIRED") && $user_type_code != "ADMIN") { ?>
																		readonly="readonly" <?}?> type="text" act="yes"
																		onkeydown="numberKeyDown(event)"
																		class="form-control no-spinners"
																		name="client_id_number"
																		value="<?php echo $case_details["client_id_number"]?>"
																		onfocus="javascript:removeTooltip('client_id_number_tooltip');">
																	<span id="client_id_number_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo CASE_NUMBER_INVALID; ?></span>
																</div>
															</div>
														</div>
														
													<?php if ($user_type_code == "CUSTOMER") {?>
													</div>
													<div class="row">
													<?php }?>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"> <?php echo CLIENT_TYPE; ?> 
																<?php if ($user_type_code == "ADMIN") {?>
																<span class="required"> * </span>
																	<?php }?>
																</label>

																<div class="col-md-6">
																<?php if ($user_type_code != "ADMIN"){?>
																	<input readonly="readonly" type="text"
																		class="form-control " name="client_type"
																		value="<?php echo $case_details["customer_type_name"]?>"
																		id="client_type">
																		<?php } else {?>
																								<select class="form-control " name="client_type"
																		id="client_type" class="form-control">
					  	<?php
																	if ($available_customer_types) {
																		for($i = 0; $i < count ( $available_customer_types ); $i ++) {
																			$customer_type_id = $available_customer_types [$i] ["customer_type_id"];
																			$customer_type_name = $available_customer_types [$i] ["name"];
																			?>
							<option value="<?=$customer_type_id ?>"
																			<?php if ($customer_type_id == $case_details["client_type"]){?>
																			selected="selected" <?php }?>> <?=$customer_type_name ?></option>
						<?php
																		}
																	} else {
																		?>
							<option value="" selected></option>
						<?php
																	}
																	?>		
					</select>
																		<?php }?>
																</div>
															</div>
														</div>
													<?php if ($user_type_code != "CUSTOMER") {?>
													</div>
													<div class="row">
													<?php }?>
													<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo OBLIGATION_VALUE; ?>
																 </label>
																<div class="col-md-4">
																	<input
																		<?php if (($user_type_code != "CUSTOMER"|| $case_details["current_state_code"]!="MODIFICATION_REQUIRED") && $user_type_code != "ADMIN") { ?>
																		readonly="readonly" <?}?> type="text" act="yes"
																		class="form-control no-spinners"
																		onkeydown="numberKeyDown(event)"
																		name="obligation_value" id="obligation_value"
																		value="<?php echo $case_details["obligation_value"]?>"
																		id="obligation_value"
																		onfocus="javascript:removeTooltip('obligation_value_tooltip');">
																	<span id="obligation_value_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo NUMBER_INVALID; ?></span>
																</div>
																<label class="col-md-2 control-label"><?php echo SR; ?></label>
															</div>
														</div>
														
													<?php if ($user_type_code == "CUSTOMER") {?>
													</div>
													<div class="row">
													<?php }?>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo REMAINING_AMOUNT; ?>
																<?php if ($user_type_code == "ADMIN") {?>
																<span class="required"> * </span>
																	<?php }?>
																</label>
																<div class="col-md-4">
																	<input
																		<?php if (($user_type_code != "CUSTOMER" || $case_details["current_state_code"]!="MODIFICATION_REQUIRED") && $user_type_code != "ADMIN") { ?>
																		readonly="readonly" <?}?> type="text" act="yes"
																		class="form-control no-spinners"
																		onkeydown="numberKeyDown(event)"
																		name="remaining_amount"
																		value="<?php echo $case_details["remaining_amount"]?>"
																		onfocus="javascript:removeTooltip('remaining_amount_tooltip');">
																	<span id="remaining_amount_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo NUMBER_INVALID; ?></span>
																</div>
																<label class="col-md-2 control-label"><?php echo SR; ?></label>
															</div>
														</div>
														
													<?php if ($user_type_code != "CUSTOMER") {?>
													</div>
													<div class="row">
													<?php }?>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo DUE_AMOUNT; ?>
																<?php if ($user_type_code == "ADMIN") {?>
																<span class="required"> * </span>
																	<?php }?>
																 </label>
																<div class="col-md-4">
																	<input
																		<?php if (($user_type_code != "CUSTOMER" || $case_details["current_state_code"]!="MODIFICATION_REQUIRED") && $user_type_code != "ADMIN") { ?>
																		readonly="readonly" <?}?> type="text" act="yes"
																		class="form-control no-spinners"
																		onkeydown="numberKeyDown(event)"
																		value="<?php echo $case_details["due_amount"]?>"
																		id="due_amount" name="due_amount"
																		onfocus="javascript:removeTooltip('due_amount_tooltip');">
																	<span id="due_amount_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo NUMBER_INVALID; ?></span>
																</div>
																<label class="col-md-2 control-label"><?php echo SR; ?></label>
															</div>
														</div>
													<?php if ($user_type_code == "CUSTOMER") {?>
													</div>
													<div class="row">
													<?php }?>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo NUMBER_OF_LATE_INSTALMENTS; ?>
																<?php if ($user_type_code == "ADMIN") {?>
																<span class="required"> * </span>
																	<?php }?>
																 </label>
																<div class="col-md-6">
																	<input
																		<?php if (($user_type_code != "CUSTOMER"|| $case_details["current_state_code"]!="MODIFICATION_REQUIRED") && $user_type_code != "ADMIN") { ?>
																		readonly="readonly" <?}?> type="text" act="yes"
																		class="form-control no-spinners"
																		onkeydown="numberKeyDown(event)"
																		name="number_of_late_instalments"
																		value="<?php echo $case_details["number_of_late_instalments"]?>"
																		id="number_of_late_instalments"
																		onfocus="javascript:removeTooltip('number_of_late_instalments_tooltip');">
																	<span id="number_of_late_instalments_tooltip"
																		class="tooltiptext" style="display: none; color: red"><?php echo INVALID_NUMBER_OF_LATE_INSTALMENTS; ?></span>
																</div>
															</div>
														</div>
														
													<?php if ($user_type_code != "CUSTOMER") {?>
													</div>
													<div class="row">
													<?php }?>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo TOTAL_DEBENTURE_AMOUNT; ?>
																<?php if ($user_type_code == "ADMIN") {?>
																<span class="required"> * </span>
																	<?php }?>
																</label>
																<div class="col-md-4">
																	<input
																		<?php if (($user_type_code != "CUSTOMER" || $case_details["current_state_code"]!="MODIFICATION_REQUIRED") && $user_type_code != "ADMIN") { ?>
																		readonly="readonly" <?}?> type="text" act="yes"
																		onkeydown="numberKeyDown(event)"
																		class="form-control  no-spinners"
																		name="total_debenture_amount"
																		id="total_debenture_amount"
																		value="<?php echo $case_details["total_debenture_amount"]?>"
																		onfocus="javascript:removeTooltip('total_debenture_amount_tooltip');">
																	<span id="total_debenture_amount_tooltip"
																		class="tooltiptext" style="display: none; color: red"><?php echo NUMBER_INVALID; ?></span>
																</div>
																<label class="col-md-2 control-label"><?php echo SR; ?></label>
															</div>
														</div>
													<?php if ($user_type_code == "CUSTOMER") {?>
													</div>
													<div class="row">
													<?php }?>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo CREATION_DATE; ?>
																</label>
																<div class="col-md-6">
																	<input readonly="readonly" type="text"
																		class="form-control" name="creation_date"
																		id="creation_date"
																		value="<?php echo $case_details["creation_date"]?>">
																</div>
															</div>
														</div>
													<?php if ($user_type_code != "CUSTOMER") {?>
													</div>
													<div class="row">
													<?php }?>
														
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo CURRENT_STATE_CODE; ?></label>
																<div class="col-md-6">
																	<input readonly="readonly" type="text"
																		class="form-control " name="state_name"
																		value="<?php echo $case_details["state_name"]?>">
																</div>
															</div>
														</div>
													<?php if ($user_type_code == "CUSTOMER") {?>
													</div>
													<div class="row">
													<?php }?>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo EXECUTIVE_ORDER_NUMBER; ?>
																<?php if ($user_type_code == "ADMIN" && ($case_details["current_state_code"] != "SUSPENDED" && ($case_details["current_state_code"] != "UNDER_REVIEW" && $case_details["current_state_code"] != "ASSIGNED_TO_LAWYER" && $case_details["current_state_code"] != "MODIFICATION_REQUIRED"))) {?>
																<span class="required"> * </span>
																	<?php }?>
																</label>
																<div class="col-md-6">
																	<input
																		<?php if ($user_type_code != "ADMIN" || ($case_details["current_state_code"] != "SUSPENDED" && ($case_details["current_state_code"] == "UNDER_REVIEW" || $case_details["current_state_code"] == "ASSIGNED_TO_LAWYER" || $case_details["current_state_code"] == "MODIFICATION_REQUIRED"))) {?>
																		readonly="readonly" <?php }?> type="text" act="yes"
																		class="form-control no-spinners"
																		onkeydown="numberKeyDown(event)"
																		name="executive_order_number"
																		id="executive_order_number"
																		value="<?php echo $case_details["executive_order_number"]?>"
																		onfocus="javascript:removeTooltip('executive_order_number_tooltip');">
																	<span id="executive_order_number_tooltip"
																		class="tooltiptext" style="display: none; color: red"><?php echo CASE_NUMBER_INVALID; ?></span>
																</div>
															</div>
														</div>
														
													<?php if ($user_type_code != "CUSTOMER") {?>
													</div>
													<div class="row">
													<?php }?>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo DECISION_34_NUMBER; ?>
																
																	</label>
																<div class="col-md-6">
																	<input
																		<?php if ($user_type_code != "ADMIN" || ($case_details["current_state_code"] != "SUSPENDED" && ($case_details["current_state_code"] == "UNDER_REVIEW" || $case_details["current_state_code"] == "ASSIGNED_TO_LAWYER"  || $case_details["current_state_code"] == "MODIFICATION_REQUIRED" || $case_details["current_state_code"] == "REGISTERED"))) {?>
																		readonly="readonly" <?php }?> type="text" act="yes"
																		class="form-control no-spinners"
																		onkeydown="numberKeyDown(event)"
																		name="decision_34_number" id="decision_34_number"
																		value="<?php echo $case_details["decision_34_number"]?>"
																		onfocus="javascript:removeTooltip('decision_34_number_tooltip');">
																	<span id="decision_34_number_tooltip"
																		class="tooltiptext" style="display: none; color: red"><?php echo CASE_NUMBER_INVALID; ?></span>
																</div>
															</div>
														</div>
													<?php if ($user_type_code == "CUSTOMER") {?>
													</div>
													<div class="row">
													<?php }?>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo EXECUTIVE_ORDER_DATE; ?>
																
																</label>
																<div class="col-md-6">
																	<input
																		<?php if ($user_type_code != "ADMIN" || ($case_details["current_state_code"] != "SUSPENDED" && ($case_details["current_state_code"] == "UNDER_REVIEW" || $case_details["current_state_code"] == "ASSIGNED_TO_LAWYER"))) {?>
																		readonly="readonly" class="form-control"
																		<?php } else {?> class="form-control date" <?php } ?>
																		type="text" id="executive_order_date"
																		name="executive_order_date"
																		value="<?php echo $case_details["executive_order_date"]?>"><span
																		id="executive_order_date_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																	<span id="executive_order_date_tooltip2"
																		class="tooltiptext" style="display: none; color: red"><?php echo DATE_CAN_NOT_BE_GREATER_THAN_TODAY; ?></span>
																	<span id="executive_order_date_tooltip3"
																		class="tooltiptext" style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>
																</div>
															</div>
														</div>
														
													<?php if ($user_type_code != "CUSTOMER") {?>
													</div>
													<div class="row">
													<?php }?>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo DECISION_34_DATE; ?>
																</label>
																<div class="col-md-6">
																	<input
																		<?php if ($user_type_code != "ADMIN" || ($case_details["current_state_code"] != "SUSPENDED" && ($case_details["current_state_code"] == "UNDER_REVIEW" || $case_details["current_state_code"] == "ASSIGNED_TO_LAWYER"  || $case_details["current_state_code"] == "MODIFICATION_REQUIRED" || $case_details["current_state_code"] == "REGISTERED"))) {?>
																		readonly="readonly" class="form-control"
																		<?php } else { ?> class="form-control date" <?php } ?>
																		type="text" name="decision_34_date"
																		id="decision_34_date"
																		value="<?php echo $case_details["decision_34_date"]?>"><span
																		id="decision_34_date_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																	<span id="decision_34_date_tooltip2"
																		class="tooltiptext" style="display: none; color: red"><?php echo DATE_CAN_NOT_BE_GREATER_THAN_TODAY; ?></span>
																	<span id="decision_34_date_tooltip3"
																		class="tooltiptext" style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>

																</div>
															</div>
														</div>
														
													<?php if ($user_type_code == "CUSTOMER") {?>
													</div>
													<div class="row">
													<?php }?>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo ORDER_NUMBER; ?>
																</label>
																<div class="col-md-6">
																	<input
																		<?php if ($user_type_code != "ADMIN" || ($case_details["current_state_code"] != "SUSPENDED" && ($case_details["current_state_code"] == "UNDER_REVIEW" || $case_details["current_state_code"] == "ASSIGNED_TO_LAWYER"  || $case_details["current_state_code"] == "MODIFICATION_REQUIRED"))) {?>
																		readonly="readonly" <?php }?> type="text" act="yes"
																		class="form-control no-spinners"
																		onkeydown="numberKeyDown(event)" name="order_number"
																		id="order_number"
																		value="<?php echo $case_details["order_number"]?>"
																		onfocus="javascript:removeTooltip('order_number_tooltip');">
																	<span id="order_number_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo ORDER_NUMBER_INVALID; ?></span>
																</div>
															</div>
														</div>
													<?php if ($user_type_code != "CUSTOMER") {?>
													</div>
													<div class="row">
													<?php }?>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo CIRCLE_NUMBER; ?>
																<?php if ($user_type_code == "ADMIN" && ($case_details["current_state_code"] != "SUSPENDED" &&  ($case_details["current_state_code"] != "UNDER_REVIEW" && $case_details["current_state_code"] != "ASSIGNED_TO_LAWYER"  && $case_details["current_state_code"] != "MODIFICATION_REQUIRED"))) {?>
																<span class="required"> * </span>
																	<?php }?>
																</label>
																<div class="col-md-6">
																	<input
																		<?php if ($user_type_code != "ADMIN" || ($case_details["current_state_code"] != "SUSPENDED" && ($case_details["current_state_code"] == "UNDER_REVIEW" || $case_details["current_state_code"] == "ASSIGNED_TO_LAWYER"  || $case_details["current_state_code"] == "MODIFICATION_REQUIRED"))) {?>
																		readonly="readonly" <?php }?> type="text" act="yes"
																		class="form-control no-spinners"
																		onkeydown="numberKeyDown(event)" name="circle_number"
																		id="circle_number"
																		value="<?php echo $case_details["circle_number"]?>"
																		onfocus="javascript:removeTooltip('circle_number_tooltip');">
																	<span id="circle_number_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo CIRCLE_NUMBER_INVALID; ?></span>
																</div>
															</div>
														</div>
													<?php if ($user_type_code == "CUSTOMER") {?>
													</div>
													<div class="row">
													<?php }?>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo ADVERTISMENT_DATE; ?>
																
																</label>
																<div class="col-md-6">
																	<input
																		<?php if ($user_type_code != "ADMIN" || ($case_details["current_state_code"] != "SUSPENDED" && ($case_details["current_state_code"] != "PUBLICLY_ADVERTISED" && $case_details["current_state_code"] != "DECISION_46" && $case_details["current_state_code"] != "CLOSED"))) {?>
																		readonly="readonly" class="form-control"
																		<?php } else { ?> class="form-control date" <?php } ?>
																		type="text" name="advertisement_date"
																		id="advertisement_date"
																		value="<?php echo $case_details["advertisement_date"]?>"><span
																		id="advertisement_date_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																	<span id="advertisement_date_tooltip2"
																		class="tooltiptext" style="display: none; color: red"><?php echo DATE_CAN_NOT_BE_GREATER_THAN_TODAY; ?></span>
																	<span id="advertisement_date_tooltip3"
																		class="tooltiptext" style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>

																</div>
															</div>
														</div>
														
													<?php if ($user_type_code != "CUSTOMER") {?>
													</div>
													<div class="row">
													<?php }?>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo LAWYER_NAME; ?></label>
																<div class="col-md-6">
																	<input readonly="readonly" type="text"
																		class="form-control " name="state_name"
																		value="<?php echo $case_details["lawyer_name"]?>">
																</div>
															</div>
														</div>
														
													<?php if ($user_type_code == "CUSTOMER") {?>
													</div>
													<div class="row">
													<?php }?>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo DECISION_46_DATE; ?>
																
																</label>
																<div class="col-md-6">
																	<input
																		<?php if ($user_type_code != "ADMIN" || ($case_details["current_state_code"] != "SUSPENDED" && ($case_details["current_state_code"] != "DECISION_46" && $case_details["current_state_code"] != "CLOSED"))) {?>
																		readonly="readonly" class="form-control"
																		<?php } else { ?> class="form-control date" <?php } ?>
																		type="text" name="decision_46_date"
																		id="decision_46_date"
																		value="<?php echo $case_details["decision_46_date"]?>"><span
																		id="decision_46_date_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																	<span id="decision_46_date_tooltip2"
																		class="tooltiptext" style="display: none; color: red"><?php echo DATE_CAN_NOT_BE_GREATER_THAN_TODAY; ?></span>
																	<span id="decision_46_date_tooltip3"
																		class="tooltiptext" style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>

																</div>
															</div>
														</div>
														<?php if ($user_type_code != "CUSTOMER") {?>
													</div>
													<div class="row">
													<?php }?>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo REGION; ?>
																
																</label>
																<div class="col-md-6">
																	<input readonly="readonly" class="form-control"
																		type="text" name="region_name"
																		value="<?php echo $case_details["region_name"]?>">

																</div>
															</div>
														</div>
														
														
														
														
														
														
														
														
														
														
														
														
														
														
														
													<?php if ($user_type_code == "CUSTOMER") {?>
													</div>
													<div class="row">
													<?php }?>
														<div class="col-md-6">
														
														<?php if ($case_details["current_state_code"] == "SUSPENDED") {?>
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo SUSPEND_REASON; ?>
																<?php if ($user_type_code == "ADMIN" && isset($case_details["suspend_reason_code"])) {?>
																<span class="required"> * </span>
																	<?php }?></label>
																<div class="col-md-6">
																<?php if ($user_type_code != "ADMIN" || !isset($case_details["suspend_reason_code"])) {?>
																	<input readonly="readonly" type="text"
																		class="form-control " name="suspend_reason"
																		id="suspend_reason"
																		value="<?php echo $case_details["suspend_reason"]?>">
																		<?php } else {?>
																		<select class="form-control "
																		name="suspend_reason_code" id="suspend_reason_code"
																		class="form-control"
																		onfocus="javascript:removeTooltip('suspend_reason_code_tooltip');">
					  	<?php
																if ($suspend_reasons) {
																	for($i = 0; $i < count ( $suspend_reasons ); $i ++) {
																		$suspend_code = $suspend_reasons [$i] ["suspend_code"];
																		$suspend_reason = $suspend_reasons [$i] ["suspend_reason"];
																		?>
							<option value="<?=$suspend_code?>"
																			<?php if ($suspend_code == $case_details["suspend_reason_code"]){?>
																			selected="selected" <?php }?>>
							<?=$suspend_reason ?></option>
						<?php
																	}
																} else {
																	?>
							<option value="" selected></option>
						<?php
																}
																?>		
					</select> <span id="suspend_reason_code_tooltip"
																		class="tooltiptext" style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																		<?php }?>
																</div>
															</div>
															<?php } else{ ?>
															<input type="hidden" name="suspend_reason"
																id="suspend_reason"
																value="<?php echo $case_details["suspend_reason"]?>" />
															<?php }  ?>
														</div>
														
														<?php if ($user_type_code != "CUSTOMER") {?>
													</div>
													<div class="row">
													<?php }?>
																											<div class="col-md-6">
														
														<?php if ($case_details["current_state_code"] == "SUSPENDED") {?>
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo SUSPEND_DATE; ?>
																<?php if ($user_type_code == "ADMIN" && isset($case_details["suspend_date"])) {?>
															<!-- 	<span class="required"> * </span> -->
																	<?php }?></label>
																<div class="col-md-6">
																<?php if ($user_type_code != "ADMIN" || !isset($case_details["suspend_date"])) {?>
																	<input readonly="readonly" type="text"
																		class="form-control date" name="suspend_date"
																		id="suspend_date"
																		value="<?php echo $case_details["suspend_date"]?>">
																		
																		<?php } else {?>
																			<input readonly="readonly" type="text"
																		class="form-control date" name="suspend_date"
																		id="suspend_date"
																		value="<?php echo $case_details["suspend_date"]?>">
																		<?php }?>
																</div>
															</div>
															<?php } else{ ?>
															<input type="hidden" name="suspend_date"
																id="suspend_date"
																value="<?php echo $case_details["suspend_date"]?>" />
															<?php }  ?>
			
														</div>
													</div>
													<?php
													$trans_assigned_to_lawyer_exist = 0;
													$trans_registered_exist = 0;
													$trans_decision_34_exist = 0;
													$trans_publicly_advertised_exist = 0;
													if (isset ( $case_transactions )) {
														for($i = 0; $i < count ( $case_transactions ); $i ++) {
															if (isset ( $case_transactions [$i] ["state_date"] ) && $case_transactions [$i] ["state_date"] != "") {
																switch ($case_transactions [$i] ["state_code"]) {
																	case "ASSIGNED_TO_LAWYER" :
																		$trans_assigned_to_lawyer_exist = 1;
																		break;
																	case "REGISTERED" :
																		$trans_registered_exist = 1;
																		break;
																	case "DECISION_34" :
																		$trans_decision_34_exist = 1;
																		break;
																	case "PUBLICLY_ADVERTISED" :
																		$trans_publicly_advertised_exist = 1;
																		break;
																	default :
																}
															}
														}
													}
													?>
													<?php if ((isset( $case_details["consignment_image"]) && $case_details["consignment_image"] != "") || ($user_type_code == "ADMIN" && $trans_assigned_to_lawyer_exist == 1)){?>
													<div class="form-group">
														<label class="col-md-2 control-label"><?php echo CONSIGNMENT_IMAGE; ?></label>
														<div <?php if ($user_type_code == "ADMIN") {?>
															class="col-md-1" <?php } else {?> class="col-md-6"
															<?php }?>>
															 <?php if (isset( $case_details["consignment_image"]) && $case_details["consignment_image"] != "") {?>
															<a
																href="<?php echo base_url().$case_details["consignment_image"]?>"
																target="_blank"
																title="<?=$case_details["consignment_image"]?>"> <img
																src="<?=base_url()?>images/Attach-icon.png"
																style="width: 30px; height: 30px;" />
															</a>
															<?php }?>
														</div>
														<?php if ($user_type_code == "ADMIN") {?>
														<div class="col-md-4">
															<input type="file" class="form-control "
																id="consignment_image" name="consignment_image"
																onchange="uploadFile('consignment_image', 'consignment_image_status', 'consignment_image_progressBar','consignment_image', '<?php echo $case_details ["case_id"]?>', 'consignment_image_fragment')">
															<input type="hidden" id="consignment_image_hidden"
																name="consignment_image_hidden">

														</div>
														<div class="col-md-5">
															<div id="consignment_image_fragment" class="fragment"
																style="display: none;">
																<span
																	style="float: left; display: inline-block; color: #0c0b0b80; cursor: pointer !important; font-weight: bold;"
																	id="close_consignment_image">&#10006;</span>
																<progress style="display: none;"
																	id="consignment_image_progressBar" value="0" max="100"
																	style="width:100px;"></progress>
																<span id="consignment_image_status"></span>
															</div>
														</div>					
														<?php }?>
													</div>
													<?php }if ((isset( $case_details["debenture"]) && $case_details["debenture"] != "") || $user_type_code == "ADMIN") {?>
													<div class="form-group">
														<label class="col-md-2 control-label"><?php echo DEBENTURE; ?><?php if ($user_type_code == "ADMIN") {?>
																<span class="required"> * </span>
																	<?php }?></label>

														<div <?php if ($user_type_code == "ADMIN") {?>
															class="col-md-1" <?php } else {?> class="col-md-6"
															<?php }?>>
															<?php if (isset( $case_details["debenture"]) && $case_details["debenture"] != "") {?>
															
															<a
																href="<?php echo base_url().$case_details["debenture"]?>"
																target="_blank" title="<?=$case_details["debenture"]?>">
																<img src="<?=base_url()?>images/Attach-icon.png"
																style="width: 30px; height: 30px;" />
															</a>
															<?php }?>
															</div>
														<?php if ($user_type_code == "ADMIN") {?>
														<div class="col-md-4">
															<input type="file" class="form-control " name="debenture"
																id="debenture" value="<?=$case_details["debenture"]?>"
																onfocus="javascript:removeTooltip('debenture_tooltip');"
																onchange="uploadFile('debenture', 'debenture_status', 'debenture_progressBar','debenture', '<?php echo $case_details ["case_id"]?>', 'debenture_fragment')">
															<span id="debenture_tooltip" class="tooltiptext"
																style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
															<input type="hidden" id="debenture_hidden"
																name="debenture_hidden"> <input type="hidden"
																value="<?=$case_details["debenture"]?>"
																id="debenture_value">
														</div>
														<div class="col-md-5">
															<div id="debenture_fragment" class="fragment"
																style="display: none;">
																<span
																	style="float: left; display: inline-block; color: #0c0b0b80; cursor: pointer !important; font-weight: bold;"
																	id="close_debenture">&#10006;</span>
																<progress style="display: none;"
																	id="debenture_progressBar" value="0" max="100"
																	style="width:100px;"></progress>
																<span id="debenture_status"></span>
															</div>
														</div>
														<?php }?>
													</div>
													<?php } if ((isset( $case_details["referral_paper"]) && $case_details["referral_paper"] != "") || ($user_type_code == "ADMIN" && $trans_registered_exist == 1)){?>
													<div class="form-group">
														<label class="col-md-2 control-label"><?php echo REFERRAL_PAPER; ?></label>
														<div <?php if ($user_type_code == "ADMIN") {?>
															class="col-md-1" <?php } else {?> class="col-md-6"
															<?php }?>>
															<?php if (isset( $case_details["referral_paper"]) && $case_details["referral_paper"] != "") {?>
															<a
																href="<?php echo base_url().$case_details["referral_paper"]?>"
																target="_blank"
																title="<?=$case_details["referral_paper"]?>"> <img
																src="<?=base_url()?>images/Attach-icon.png"
																style="width: 30px; height: 30px;" />
															</a>
															<?php }?>
															</div>
													<?php if ($user_type_code == "ADMIN") {?>
														<div class="col-md-4">
															<input type="file" class="form-control "
																name="referral_paper" id="referral_paper"
																onchange="uploadFile('referral_paper', 'referral_paper_status', 'referral_paper_progressBar','referral_paper', '<?php echo $case_details ["case_id"]?>', 'referral_paper_fragment')">
															<input type="hidden" id="referral_paper_hidden"
																name="referral_paper_hidden">

														</div>
														<div class="col-md-5">
															<div id="referral_paper_fragment" class="fragment"
																style="display: none;">
																<span
																	style="float: left; display: inline-block; color: #0c0b0b80; cursor: pointer !important; font-weight: bold;"
																	id="close_referral_paper">&#10006;</span>
																<progress style="display: none;"
																	id="referral_paper_progressBar" value="0" max="100"
																	style="width:100px;"></progress>
																<span id="referral_paper_status"></span>
															</div>
														</div>	
														<?php }?>
													</div>
												<?php } if ((isset( $case_details["id"]) && $case_details["id"] != "") || $user_type_code == "ADMIN" ){ ?>
													<div class="form-group">
														<label class="col-md-2 control-label"><?php echo CASE_ID_ADD; ?><?php if ($user_type_code == "ADMIN") {?>
																<span class="required"> * </span>
																	<?php }?></label>
														<div <?php if ($user_type_code == "ADMIN") {?>
															class="col-md-1" <?php } else {?> class="col-md-6"
															<?php }?>>
															<?php if (isset( $case_details["id"]) && $case_details["id"] != "") {?>
															<a href="<?php echo base_url().$case_details["id"]?>"
																target="_blank" title="<?=$case_details["id"]?>"> <img
																src="<?=base_url()?>images/Attach-icon.png"
																style="width: 30px; height: 30px;" />
															</a>
															<?php }?>
															</div>
															<?php if ($user_type_code == "ADMIN") {?>
														<div class="col-md-4">
															<input type="file" class="form-control " name="id"
																id="id"
																onfocus="javascript:removeTooltip('id_tooltip');"
																onchange="uploadFile('id', 'id_status', 'id_progressBar','id', '<?php echo $case_details ["case_id"]?>', 'id_fragment')">
															<input type="hidden" id="id_hidden" name="id_hidden"> <span
																id="id_tooltip" class="tooltiptext"
																style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
														<input type="hidden"
																value="<?=$case_details["id"]?>"
																id="id_value">
														</div>
														<div class="col-md-5">
															<div id="id_fragment" class="fragment"
																style="display: none;">
																<span
																	style="float: left; display: inline-block; color: #0c0b0b80; cursor: pointer !important; font-weight: bold;"
																	id="close_id">&#10006;</span>
																<progress style="display: none;" id="id_progressBar"
																	value="0" max="100" style="width:100px;"></progress>
																<span id="id_status"></span>
															</div>
														</div>	
														<?php }?>
													</div>
													<?php } if ((isset( $case_details["contract"]) && $case_details["contract"] != "") || $user_type_code == "ADMIN"){ ?>
													<div class="form-group">
														<label class="col-md-2 control-label"><?php echo CONTRACT; ?></label>
														<div <?php if ($user_type_code == "ADMIN") {?>
															class="col-md-1" <?php } else {?> class="col-md-6"
															<?php }?>>
															<?php if (isset( $case_details["contract"]) && $case_details["contract"] != "") {?>

															<a
																href="<?php echo base_url().$case_details["contract"]?>"
																target="_blank" title="<?=$case_details["contract"]?>">
																<img src="<?=base_url()?>images/Attach-icon.png"
																style="width: 30px; height: 30px;" />
															</a>
															<?php }?>
															</div>
															<?php if ($user_type_code == "ADMIN") {?>
														<div class="col-md-4">
															<input type="file" class="form-control " name="contract"
																id="contract"
																onfocus="javascript:removeTooltip('contract_tooltip');"
																onchange="uploadFile('contract', 'contract_status', 'contract_progressBar','contract', '<?php echo $case_details ["case_id"]?>', 'contract_fragment')">
															<input type="hidden" id="contract_hidden"
																name="contract_hidden"> <span id="contract_tooltip"
																class="tooltiptext" style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
														</div>
														<div class="col-md-5">
															<div id="contract_fragment" class="fragment"
																style="display: none;">
																<span
																	style="float: left; display: inline-block; color: #0c0b0b80; cursor: pointer !important; font-weight: bold;"
																	id="close_contract">&#10006;</span>
																<progress style="display: none;"
																	id="contract_progressBar" value="0" max="100"
																	style="width:100px;"></progress>
																<span id="contract_status"></span>
															</div>
														</div>	
														<?php }?>
													</div>
													
													<?php } if ((isset( $case_details["others"]) && $case_details["others"] != "") || $user_type_code == "ADMIN"){?>
													<div class="form-group">
														<label class="col-md-2 control-label"><?php echo OTHERS; ?></label>
														<div <?php if ($user_type_code == "ADMIN") {?>
															class="col-md-1" <?php } else {?> class="col-md-6"
															<?php }?>>
														<?php
														
														if (isset ( $case_details ["others"] ) && $case_details ["others"] != "") {
															
															$others = explode ( ",", $case_details ["others"] );
															foreach ( $others as $other ) {
																?>
														
															
															<a href="<?php echo base_url().$other?>" target="_blank"
																title="<?=$other?>"> <img
																src="<?=base_url()?>images/Attach-icon.png"
																style="width: 30px; height: 30px;" />
															</a>
															
														<?php } }?>
														</div>
														<?php if ($user_type_code == "ADMIN") {?>
														<div class="col-md-4">
															<input type="file" class="form-control " name="others[]"
																id="others" multiple
																onfocus="javascript:removeTooltip('others_tooltip');"
																onchange="uploadFile('others', 'others_status', 'others_progressBar','others', '<?php echo $case_details ["case_id"]?>', 'others_fragment')">
															<input type="hidden" id="others_hidden"
																name="others_hidden">

														</div>
														<div class="col-md-5">
															<div id="others_fragment" class="fragment"
																style="display: none;">
																<span
																	style="float: left; display: inline-block; color: #0c0b0b80; cursor: pointer !important; font-weight: bold;"
																	id="close_others">&#10006;</span>
																<progress style="display: none;" id="others_progressBar"
																	value="0" max="100" style="width:100px;"></progress>
																<span id="others_status"></span>
															</div>
														</div>
														<?php }?>
													</div>
													
													<?php } if ((isset( $case_details["advertisement_file"]) && $case_details["advertisement_file"] != "")  || ($user_type_code == "ADMIN" && $trans_publicly_advertised_exist == 1)){ ?>
													<div class="form-group">
														<label class="col-md-2 control-label"><?php echo ADVERTISEMENT_FILE; ?></label>
														<div <?php if ($user_type_code == "ADMIN") {?>
															class="col-md-1" <?php } else {?> class="col-md-6"
															<?php }?>>
															<?php if (isset( $case_details["advertisement_file"]) && $case_details["advertisement_file"] != "") {?>
															<a
																href="<?php echo base_url().$case_details["advertisement_file"]?>"
																target="_blank"
																title="<?=$case_details["advertisement_file"]?>"> <img
																src="<?=base_url()?>images/Attach-icon.png"
																style="width: 30px; height: 30px;" />
															</a>
															<?php }?>
															</div>
													<?php if ($user_type_code == "ADMIN") {?>
														<div class="col-md-4">
															<input type="file" class="form-control "
																name="advertisement_file" id="advertisement_file"
																onchange="uploadFile('advertisement_file', 'advertisement_file_status', 'advertisement_file_progressBar','advertisement_file', '<?php echo $case_details ["case_id"]?>', 'advertisement_file_fragment')">
															<input type="hidden" id="advertisement_file_hidden"
																name="advertisement_file_hidden"> <input type="hidden"
																id="advertisement_file_value"
																value="<?= $case_details["advertisement_file"]?>">

														</div>
														<div class="col-md-5">
															<div id="advertisement_file_fragment" class="fragment"
																style="display: none;">
																<span
																	style="float: left; display: inline-block; color: #0c0b0b80; cursor: pointer !important; font-weight: bold;"
																	id="close_advertisement_file">&#10006;</span>
																<progress style="display: none;"
																	id="advertisement_file_progressBar" value="0" max="100"
																	style="width:100px;"></progress>
																<span id="advertisement_file_status"></span>
															</div>
														</div>
														<?php }?>
													</div>
													
													<?php } if ((isset( $case_details["invoice_file"]) && $case_details["invoice_file"] != "")){ ?>
													<div class="form-group">
														<label class="col-md-2 control-label"><?php echo INVOICE_FILE; ?>
														<?php if ($user_type_code == "ADMIN") {?>
																<span class="required"> * </span>
																	<?php }?></label>
														<div <?php if ($user_type_code == "ADMIN") {?>
															class="col-md-1" <?php } else {?> class="col-md-6"
															<?php }?>>
															<?php if (isset( $case_details["invoice_file"]) && $case_details["invoice_file"] != "") {?>
															<a
																href="<?php echo base_url().$case_details["invoice_file"]?>"
																target="_blank"
																title="<?=$case_details["invoice_file"]?>"> <img
																src="<?=base_url()?>images/Attach-icon.png"
																style="width: 30px; height: 30px;" />
															</a>
															<?php }?>
															</div>
															<?php if ($user_type_code == "ADMIN") {?>
														<div class="col-md-4">
															<input type="file" class="form-control "
																id="invoice_file" name="invoice_file"
																onfocus="javascript:removeTooltip('invoice_file_tooltip');"
																onchange="uploadFile('invoice_file', 'invoice_file_status', 'invoice_file_progressBar','invoice_file', '<?php echo $case_details ["case_id"]?>', 'invoice_file_fragment')">
															<input type="hidden" id="invoice_file_hidden"
																name="invoice_file_hidden"> <input type="hidden"
																id="invoice_file_value"
																value="<?= $case_details["invoice_file"]?>"> <span
																id="invoice_file_tooltip" class="tooltiptext"
																style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
														</div>
														<div class="col-md-5">
															<div id="invoice_file_fragment" class="fragment"
																style="display: none;">
																<span
																	style="float: left; display: inline-block; color: #0c0b0b80; cursor: pointer !important; font-weight: bold;"
																	id="close_invoice_file">&#10006;</span>
																<progress style="display: none;"
																	id="invoice_file_progressBar" value="0" max="100"
																	style="width:100px;"></progress>
																<span id="invoice_file_status"></span>
															</div>
														</div>
														<?php }?>
													</div>
													
													
													<?php }if ((isset( $case_details["suspend_file"]) && $case_details["suspend_file"] != "") && $case_details["current_state_code"] == "SUSPENDED") {?>
													<div class="form-group">
														<label class="col-md-2 control-label"><?php echo SUSPEND_FILE; ?></label>

														<div <?php if ($user_type_code == "ADMIN") {?>
															class="col-md-1" <?php } else {?> class="col-md-6"
															<?php }?>>
															<?php if (isset( $case_details["suspend_file"]) && $case_details["suspend_file"] != "") {?>
															
															<a
																href="<?php echo base_url().$case_details["suspend_file"]?>"
																target="_blank"
																title="<?=$case_details["suspend_file"]?>"> <img
																src="<?=base_url()?>images/Attach-icon.png"
																style="width: 30px; height: 30px;" />
															</a>
															<?php }?>
															</div>
														<?php if ($user_type_code == "ADMIN") {?>
														<div class="col-md-4">
															<input type="file" class="form-control "
																name="suspend_file" id="suspend_file"
																value="<?=$case_details["suspend_file"]?>"
																onchange="uploadFile('suspend_file', 'suspend_file_status', 'suspend_file_progressBar','suspend_file', '<?php echo $case_details ["case_id"]?>', 'suspend_file_fragment')">
															<input type="hidden" id="suspend_file_hidden"
																name="suspend_file_hidden"> <input type="hidden"
																value="<?=$case_details["suspend_file"]?>"
																id="suspend_file_value">
														</div>
														<div class="col-md-5">
															<div id="suspend_file_fragment" class="fragment"
																style="display: none;">
																<span
																	style="float: left; display: inline-block; color: #0c0b0b80; cursor: pointer !important; font-weight: bold;"
																	id="close_suspend_file">&#10006;</span>
																<progress style="display: none;"
																	id="suspend_file_progressBar" value="0" max="100"
																	style="width:100px;"></progress>
																<span id="suspend_file_status"></span>
															</div>
														</div>
														<?php }?>
													</div>
													
													
													<?php }?>
													
													
													<?php if($case_details["current_state_code"] == "CLOSED" ){ ?>
													<div class="form-group">
														<label class="col-md-2 control-label"><?php echo CLOSING_REASON; ?><?php if ($user_type_code == "ADMIN") {?>
																<span class="required"> * </span>
																	<?php }?></label>
														<div class="col-md-3">
																	
									<?php if ($user_type_code != "ADMIN") {?>
																	<input readonly="readonly" type="text"
																class="form-control " name="closing_reason"
																id="closing_reason"
																value="<?php echo $case_details["closing_reason"]?>">
																		<?php } else {?>
																	
																		<select class="form-control "
																name="closing_reason_select" id="closing_reason_select"
																class="form-control" onchange="changeHiddenInput(this)"
																onfocus="javascript:removeTooltip('closing_reason_tooltip');">
					  	<?php
															if ($close_reasons) {
																$found = 0;
																for($i = 0; $i < count ( $close_reasons ); $i ++) {
																	$close_reason = $close_reasons [$i] ["close_reason"];
																	?>
							<option value="<?=$close_reason?>"
																	<?php
																	
																	if ($close_reason == $case_details ["closing_reason"]) {
																		$found = 1;
																		?>
																	selected="selected" <?php }?>>
							<?=$close_reason ?></option>
						<?php
																}
																if ($found == 0) {
																	$case_details ["closing_reason"] = "";
																	?> <option value="" selected></option>  	<?php
																}
															} else {
																?>
																	<option value="" selected></option>  	<?php } 		?>
																		
					</select> <input type="hidden" name="closing_reason"
																id="closing_reason"
																value="<?php echo $case_details["closing_reason"]?>" />
															<span id="closing_reason_tooltip" class="tooltiptext"
																style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																		<?php } ?>
														</div>
													</div>
													
													<?php } else{ ?>
													<input type="hidden" name="closing_reason"
														id="closing_reason"
														value="<?php echo $case_details["closing_reason"]?>" />
													<?php } ?>
													
													<div class="form-group">
														<label class="col-md-2 control-label"><?php echo NOTES; ?><?php if ($user_type_code == "ADMIN" && ($case_details["current_state_code"] != "SUSPENDED" && $case_details["current_state_code"] == "MODIFICATION_REQUIRED")) {?>
																<span class="required"> * </span>
																	<?php }?></label>
														<div class="col-md-6">
															<textarea
																<?php if ($user_type_code != "ADMIN" || ($case_details["current_state_code"] != "SUSPENDED" && $case_details["current_state_code"] != "MODIFICATION_REQUIRED")) {?>
																readonly="readonly" <?php }?> class="form-control "
																rows="4" cols="50" name="notes" id="notes" form="myForm"
																onfocus="javascript:removeTooltip('notes_tooltip');">
																<?php echo $case_details["notes"]?></textarea>
															<span id="notes_tooltip" class="tooltiptext"
																style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
														</div>
													</div>
													<div>
														<div class="row">
															<div class="col-md-9" align="center"
																style="margin: auto; width: 100%;">
															
																<?php
																if ($available_transitions) {
																	for($i = 0; $i < count ( $available_transitions ); $i ++) {
																		?>
																
																 
																<?php
																		
																		if (($user_type_code == "CUSTOMER" && $case_details ["current_state_code"] == "MODIFICATION_REQUIRED")) {
																			?>
																<button id = "no-popup" type="submit"
																	class="btn btn-circle green transition-button">
																<?php } else {?>
																<button type="button"
																				onclick="popup_window('<?=site_url('case_management/manage_case/'.$case_details ["case_id"].'/'.$available_transitions[$i]["action_code"])?>','<?= $available_transitions[$i]["action_code"]?>')"
																				class="btn btn-circle green transition-button"> 
															<?}?>
																
																 <?= $available_transitions[$i]["action_button_text"]?>
																	
																</button>
																<?php
																	}
																}
																if ($user_type_code == "ADMIN") {
																	?>
																<button type="submit"
																				class="btn btn-circle green transition-button">
																	<?php echo SAVE;?>
																</button>
																<?php
																} else if ($user_type_code == "CUSTOMER") {
																	if ($case_details ["current_state_code"] != "SUSPENDED" && $case_details ["current_state_code"] != "CLOSED") {
																		?>
																<button type="button"
																				class="btn btn-circle green transition-button"
																				onclick="popup_window('<?=site_url('case_management/do_case_request/'.$case_details ["case_id"])?>','SUSPEND_REQUEST')">
																				
																	<?php echo REQUEST_SUSPEND;?>
																</button>
																<?php
																	} else if ($case_details ["current_state_code"] == "SUSPENDED") {
																		?>
																	<button type="button"
																				class="btn btn-circle green transition-button"
																				onclick="popup_window('<?=site_url('case_management/do_case_request/'.$case_details ["case_id"].'/R')?>','RESUME_REQUEST')">
																	<?php echo REQUEST_RESUME;?>
																</button>
																
																<?php
																	}
																}
																?>
																
															
															
															
															
															</div>
														</div>
													</div>
												</div>

											</form>
											<!-- END FORM-->



										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="page-head">
							<div class="container">
								<div class="col-md-14">
									<div class="portlet box green">
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-gift"></i>
												<?php echo COMMENTS;?>
											</div>
										</div>
										<br>
										<div class="portlet-body form form-horizontal">
											<!-- BEGIN FORM-->
											<form id="commentForm" role="form" method="post"
												onsubmit="javascript:return checkEmptyComment();"
												action="<?=site_url('case_management/add_comment/'.$case_details ["case_id"])?>"
												class="">
												<div class="form-group"></div>
												<div class="form-group">
															
											<?php if(isset($profile_img) && $profile_img != '') {?>				
											<img class="col-md-9 control-label _1ci img"
														src="<?=base_url()?><?=$profile_img?>" alt="">
											<?} else {?>
											<img class="col-md-9 control-label _1ci img"
														src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image"
														alt="">
											<?}?>
											<div class="col-md-6 _1ci_panel">
														<textarea class="form-control " rows="4"
															onkeyup="commentKeyUp()"
															placeholder="<?php echo WRITE_COMMENT;?>" cols="50"
															id="comment" name="comment" form="commentForm"></textarea>
													</div>
												</div>
												<div class="form-group">
													<div class="col-md-9 control-label _1ci_space"></div>
													<div class="col-md-5 _1ci_panel">
														<button disabled="disabled" id="comment_submit"
															type="submit" class="btn btn-circle green"><?php echo POST;?></button>
													</div>
												</div>
											</form>
									<?php
									
									if ($comments) {
										for($i = 0; $i < count ( $comments ); $i ++) {
											?>
									<div class="form-group">
										<?php if(isset($comments[$i]["profile_img"]) && $comments[$i]["profile_img"] != '') {?>				
											<img class="col-md-9 control-label _1ci img"
													src="<?=base_url()?><?=$comments[$i]["profile_img"]?>"
													alt="">
											<?} else {?>
											<img class="col-md-9 control-label _1ci img"
													src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image"
													alt="">
											<?}?>
										<div class="col-md-6 _1ci_panel">
													<span class="tooltiptext"
														style="color: black; font-weight: bold"><?=$comments[$i]["first_name"]." ".$comments[$i]["last_name"]." - "?><span
														style="color: grey"><?=$comments[$i]["user_type_name"]?></span></span>
													<br> <span><em><?=$comments[$i]["comment"]?></em></span> <br>
													<span class="tooltiptext" style="color: grey;"><?=$comments[$i]["date_time"]?></span>

												</div>
											</div>
											<div class="form-group"></div>
									<?php
										}
									} else {
										?>
										<div class="form-group"></div>
									<?php
									}
									?>
								</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
       <?php
							if ($search_mode != "1") {
								$this->load->view ( 'utils/footer' );
							}
							?>
 
 
 <!-- BEGIN CORE PLUGINS -->


	<script src="<?=base_url()?>assets/global/plugins/jquery.min.js"
		type="text/javascript"></script>

	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.sparkline.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="<?=base_url()?>assets/global/scripts/app.min.js"
		type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?=base_url()?>assets/pages/scripts/profile.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script
		src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js"
		type="text/javascript"></script>
	<!-- END THEME LAYOUT SCRIPTS -->

	<script>
$(document).ready(function(){
	$('.date').each(function(){
		if($(this).prop('readonly')){
			    $(this).calendarsPicker({showOnFocus: false});
		}
	});

	
});
</script>

	<script type="text/javascript"
		src="<?php echo base_url()?>js/bootbox.min.js"></script>
</body>
</html>
