<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<?php
session_start ();
$this->load->view ( 'localization/lang' );
$this->load->view ( 'localization/txt' );
defineLocale ();
defineStrings ();
function set_code($code) {
	$GLOBALS ['code'] = $code;
}
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title><?php echo TITLE; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin RTL Theme #3 for dashboard & statistics"
	name="description" />
<meta content="" name="author" />
<link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.min.css">
<link rel="shortcut icon"
	href="<?=base_url()?>assets/layouts/layout3/img/logo-default.jpg" />
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap-table/bootstrap-table.min.css"
	rel="stylesheet" type="text/css" />


<script>

	function win_popup(url) {
		  w = 1040;
		  
		 h = 500;
		  var left = (screen.width/2)-(w/2);
		  var top = (screen.height/2)-(h/2);
			  window.memwin  = window.open(url,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width='+w+', height='+h+', top='+top+', left='+left+',directories=no,location=no');
		  
		  window.memwin.focus();
	  }

	function edit_search() {
		/*w = 1040;		  
		h = 500;
		var left = (screen.width/2)-(w/2);
		var top = (screen.height/2)-(h/2);
		window.open('','formpopup','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width='+w+', height='+h+', top='+top+', left='+left+',directories=no,location=no');
		$('#editSearchForm').attr('target','formpopup');*/
		document.getElementById('editSearchForm').submit();
	}

	function apply_quick_search() {
		$('#loading').removeClass('hidden');
		$('#search_text').removeClass('hidden');
		document.getElementById('quickSearchForm').submit();
	}
	  
</script>

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>	
/*var progress_count  = 0;
function  updateDownloadProgress(){
	progress_count = progress_count + 1;
            percent = progress_count * 10;
            if (percent < 100){
             $('.text-center').css('display','none');
             $('.progress').css('display','block');
             $('.progress-bar').css('width', percent+'%').attr('aria-valuenow', percent);
	     $('.progress-bar').html(percent+'%');
            } 
}
// starting interval
function startDownload(){
 download = setInterval(updateDownloadProgress, 1000);
}*/
	function exportCases(database) {
	var page_url = "";
	var fileName = "";
	var that = this;
		if (database == 0){
			page_url ='<?=site_url('export_cases/excel_export' )?>';
			fileName = "������ �������_" + ".xlsx";
			//window.location.replace('<?=site_url('export_cases/excel_export' )?>','_blank');
		} else {
			page_url ='<?=site_url('export_cases/excel_export/1' )?>';
			fileName = "������ ������� �� ���� ����� ������_" + ".xlsx";
			//window.location.replace('<?=site_url('export_cases/excel_export/1' )?>','_blank');
		}	
		  var dialog = bootbox.dialog({ 
	 title: "<?= DOWNLOADING?>",
	 message: '<div  class="text-center"><i class="fa fa-spin fa-spinner"></i>  <?= ANALYSING?>...</div>' ,
	/* buttons:{
	 	"�����": function() {
	 	ajax.abort();
}
	 }*/});
	 //startDownload();
	var progress_count = 0;
    var req = new XMLHttpRequest();
    req.open("POST", page_url,true);
    /*req.addEventListener("progress", function (evt) {
    console.log(evt.loaded+" "+evt.total);
            progress_count = progress_count + 1;
            percent = progress_count * 10;
            if (percent < 100){
             $('.text-center').css('display','none');
             $('.progress').css('display','block');
             $('.progress-bar').css('width', percent+'%').attr('aria-valuenow', percent);
	     $('.progress-bar').html(percent+'%');
            }
        
    }, false);*/
    req.onreadystatechange = function () {
    /*setTimeout(function(){
    progress_count = 0;
		$('.progress-bar').css('width', '100%').attr('aria-valuenow', '100').html("100%");
		$('.progress-bar').html('100%');
		$('.progress-bar').addClass( "progress-bar-success" );
		$('.text-center').css('display','block');
									}, 500);*/
    	
        if (req.readyState === 4 && req.status === 200) {
        setTimeout(function(){
           dialog.modal('hide');
           }, 500);
	var filename= "";
	var date = new Date();
	var month = date.getMonth()+1;
	if (month < 10){
	month = "0"+month;
	}
	if (database == 0){
			filename= "������ �������_" + date.getFullYear()+month +date.getDate()+".xlsx";
			
		} else {
			
			filename= "������ ������� �� ���� ����� ������_" + date.getFullYear()+month +date.getDate()+ ".xlsx";
			
		}	
            if (typeof window.chrome !== 'undefined') {
                // Chrome version
                var link = document.createElement('a');
                link.href = window.URL.createObjectURL(req.response);
                link.download = filename;
                link.click();
            } else if (typeof window.navigator.msSaveBlob !== 'undefined') {
                // IE version
                var blob = new Blob([req.response], { type: 'application/force-download' });
                window.navigator.msSaveBlob(blob, filename);
            } else {
                // Firefox version
                var file = new File([req.response], filename, { type: 'application/force-download' });
                window.open(URL.createObjectURL(file));
            }
        }
    };
    req.responseType = "blob";
    req.send();
	
	}
</script>
<script>
function addCase(){
    $.ajax({
        url: '<?php echo site_url('case_management/verifyAddCase'); ?>',
        type: 'POST',
        dataType: 'json',
        success: function(response) {
            if(response.status){
				window.location.replace("<?=site_url('case_management/add_case')?>");
            	//$('#hiddenAdd')[0].click();
            }else{
	          	$('#show_error').show();
            }
        }
    });
}
 
 var cases = <?php echo json_encode($cases) ?>;
 
 var allRecordsMap = new Array();
 allRecordsMap["1"] = cases;
 function uploadRecordsPerPage(clickedPage){
	 	var curr_reg = $("#curr_reg").val();
	 	if (allRecordsMap[clickedPage] == null){
		<?php
		
		if ($selected_state == null || $selected_state == "") {
			$selected_state = "0";
		}
		if ($search_mode == null || $search_mode == "") {
			$search_mode = "0";
		}
		?>
	 	<?php
			if ($search_mode == null || $search_mode == "" || $search_mode == "0") {
				
				?>
			$.ajax({
		        url: '<?php echo site_url('case_management/view_cases/'.$selected_state.'/'.$search_mode); ?>'+"/"+clickedPage+"/1/"+curr_reg,
	        success: function(response) {
		        var responseObject = JSON.parse(response);
		       //alert(responseObject["cases"]);
	        	allRecordsMap[clickedPage] = responseObject["cases"];
	        	getNextBatch (clickedPage);
	            
	        },
	        error: function (request, status, error) {
	            alert(request.responseText);
	        }
	    });
		<?php } else if ($search_mode == "quick_search") { ?>
			$.ajax({
				type: 'POST',
		        url: '<?php echo site_url('case_management/quick_search_cases/'.$selected_state); ?>'+"/"+clickedPage+"/1",
				data: { 'query': '<?php echo $quick_search_query?>'},
	        success: function(response) {
		        var responseObject = JSON.parse(response);
		       //alert("done");
	        	allRecordsMap[clickedPage] = responseObject["cases"];
	        	getNextBatch (clickedPage);
	            
	        },
	        error: function (request, status, error) {
	            alert(request.responseText);
	        }
	    });
		    <?php } else { ?>
		    $.ajax({
		    	type: 'POST',
		        url: '<?php echo site_url('case_management/view_cases/'.$selected_state.'/0'); ?>'+"/"+clickedPage+"/1/"+curr_reg,
		        data: { 
			        
		            'search_mode': '1', 
		            'customer_id': '<?php echo $search_values["customer_id"]?>',
		            'contract_number': '<?php echo $search_values["contract_number"]?>',
		            'debtor_name': '<?php echo $search_values["debtor_name"]?>',
		        	'client_name': '<?php echo $search_values["client_name"]?>',
		        	'client_id_number': '<?php echo $search_values["client_id_number"]?>',
		        	'client_type': '<?php echo $search_values["client_type"]?>',
        			'obligation_value': '<?php echo $search_values["obligation_value"]?>',
        		    'number_of_late_instalments': '<?php echo $search_values["number_of_late_instalments"]?>',
        		    'due_amount': '<?php echo $search_values["due_amount"]?>',
        		    'remaining_amount': '<?php echo $search_values["remaining_amount"]?>',
        		    'total_debenture_amount': '<?php echo $search_values["total_debenture_amount"]?>',
        		    'creation_date': '<?php echo $search_values["creation_date"]?>',
        		    'executive_order_number': '<?php echo $search_values["executive_order_number"]?>',
        		    'order_number': '<?php echo $search_values["order_number"]?>',
        		    'circle_number': '<?php echo $search_values["circle_number"]?>',
        		    'decision_34_number': '<?php echo $search_values["decision_34_number"]?>',
        		    'decision_34_date': '<?php echo $search_values["decision_34_date"]?>',
        		    'decision_46_date': '<?php echo $search_values["decision_46_date"]?>',
        			'state_code': '<?php echo $search_values["state_code"]?>',
        		    'region_id': '<?php echo $search_values["region_id"]?>',
        		    'lawyer_id': '<?php echo $search_values["lawyer_id"]?>',
        		    'executive_order_date': '<?php echo $search_values["executive_order_date"]?>',
        		    'advertisement_date': '<?php echo $search_values["advertisement_date"]?>',
        		    'suspend_reason_code': '<?php echo $search_values["suspend_reason_code"]?>',
        		    'closing_reason': '<?php echo $search_values["closing_reason"]?>',
        		    'case_id': '<?php echo $search_values["case_id"]?>'
        		    
		        },
	        success: function(response) {
		        var responseObject = JSON.parse(response);
		       //alert(responseObject["cases"]);
	        	allRecordsMap[clickedPage] = responseObject["cases"];
	        	getNextBatch (clickedPage);
	            
	        },
	        error: function (request, status, error) {
	            alert(request.responseText);
	        }
	    });
		    <?php } ?>
	 	 
	    }else{
	    	getNextBatch (clickedPage);
	    }
 }
 function getNextBatch (clickedPage){
	// alert ("entered");
	// on scucess of the ajax call for the 100 record of clickedpage
	
	//alert (allRecordsMap[clickedPage]);
	cases = allRecordsMap[clickedPage];
	 $('#table-pagination').find('tbody').empty();
	for (i = 0; i<cases.length;i++){
		// choose the current state layout
		var current_state_class="";
		if ( cases[i] ["current_state_code"]=="INITIAL"){
			current_state_class = "label label-sm	label-new";
		}else if ( cases[i] ["current_state_code"]=="ASSIGNED_TO_LAWYER"){
			current_state_class = "label label-sm label-under-action";
		}else if ( cases[i] ["current_state_code"]=="CLOSED"){ 
			current_state_class = "label label-sm label-danger";
		}else if ( cases[i] ["current_state_code"]=="DECISION_34"){
			current_state_class = "label label-sm label-dec-four";
		}else if ( cases[i] ["current_state_code"]=="DECISION_46"){
			current_state_class = "label label-sm label-dec-six";
		}else if ( cases[i] ["current_state_code"]=="MODIFICATION_REQUIRED"){
			current_state_class = "label label-sm label-modf-req";
		}else if ( cases[i] ["current_state_code"]=="PUBLICLY_ADVERTISED"){
			current_state_class = "label label-sm label-adv";
		}else if ( cases[i] ["current_state_code"]=="REGISTERED"){
			current_state_class = "label label-sm label-exec";
		}else if ( cases[i] ["current_state_code"]=="UNDER_REVIEW"){
			current_state_class = "label label-sm label-under-rev";
		}else if ( cases[i] ["current_state_code"]=="SUSPENDED"){
			current_state_class = "label label-sm label-suspend";
		}
		var caseLink = "<?=site_url('case_management/view_case_details')?>" +"/"+cases[i] ["case_id"];
		var caseDetailLink = "'"+caseLink+"/1'";
		var caseDetailLinkCode ="";
		<?php if ($search_mode == "1" || $search_mode == "quick_search"){?>
		caseDetailLinkCode = '<button id="case_detail" type="button" onclick=" win_popup('+caseDetailLink+');"'+
				'class="btn btn-outline btn-circle btn-sm purple"> <i class="fa fa-edit"></i> <?php echo VIEW; ?> </button>';
		<?php } else {?>
		caseDetailLinkCode = '<a href="'+caseLink+'" class="btn btn-outline btn-circle btn-sm purple"> <i class="fa fa-edit"></i> <?php echo VIEW; ?> </a>';
		<?php } ?>
		$('#table-pagination').find('tbody').append('<tr>'+
				 '<td>'+cases[i]["case_id"]+'</td>'+
				 '<td>'+cases[i]["client_name"]+'</td>'+
				 '<td>'+cases[i]["client_id_number"]+'</td>'+
				 '<td>'+cases[i]["number_of_late_instalments"]+'</td>'+
				 '<td>'+cases[i]["lawyer_name"]+'</td>'+
				 '<td>'+cases[i]["last_trans_date"]+'</td>'+
				 '<td><span class="'+current_state_class+'">'+cases[i]["state_name"]+"</span>"+'</td>'+
				 '<td>'+ caseDetailLinkCode+'</td>'+
				 '</tr>');
	}
 }


 function get_prev_page (){
	var current_page = parseInt($("#calculated_current_page").val()) ;
	var newpage = (current_page-1);
	paginate_on_page(newpage);
 }

 function get_next_page(){
	var current_page = parseInt($("#calculated_current_page").val()) ;
	var newpage = (current_page+1);
	paginate_on_page(newpage);
 }

 function paginate_on_page(newpage){
	var no_of_pages = $('#no_of_pages').val();
	if (parseInt(newpage) >= parseInt(no_of_pages)){
		newpage = no_of_pages;
		$("#next_button").prop('disabled', true);
	}else{
		$("#next_button").prop('disabled', false);
	}
	if (parseInt(newpage) > 1){
		$("#prev_button").prop('disabled', false);
	}else{
		$("#prev_button").prop('disabled', true);
	}
	$("#calculated_current_page").val(newpage);
	$("#desired_page_num").val(newpage);
	uploadRecordsPerPage(newpage);
 }

 $(document).on('keyup','#quick_search_input',function(event) { 
	    if(event.keyCode == 13){
		$('#loading').removeClass('hidden');
		$('#search_text').removeClass('hidden');
			
	    }
	});
	
 $(document).on('keyup','#desired_page_num',function(event) { 
	    if(event.keyCode == 13){
			var newpage  = $('#desired_page_num').val();
			paginate_on_page(newpage);
	    }
	});
 $(document).on('change','#upload',function(event) { 
	   import_action();
	});
 
 function upload_action(){
	$("#upload").click();
	
 }
 
 // handling endWith in IE
 if (!String.prototype.endsWith) {
	String.prototype.endsWith = function(search, this_len) {
		if (this_len === undefined || this_len > this.length) {
			this_len = this.length;
		}
		return this.substring(this_len - search.length, this_len) === search;
	};
}

 function import_action(){
	 var upload_val = $("#upload").val();
	 upload_val = upload_val.toLowerCase();
	
	 if(upload_val.endsWith('.xls') || upload_val.endsWith('.xlsx') || upload_val.endsWith('.csv')){
	  
		var ajax = new XMLHttpRequest();
		 bootbox.dialog({ 
		 title: "<?= UPLOADING?>",
		 message: '<div class="progress"><div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">  </div></div><div style="display: none" class="text-center"><i class="fa fa-spin fa-spinner"></i>  <?= ANALYSING?>...</div>' ,
		/* buttons:{
			"�����": function() {
			ajax.abort();
	}
		 }*/});
	 
	
	 var data = new FormData();
	 data.append("upload",$("#upload").file);
	 
	 ajax.upload.addEventListener("progress", function(event){
	 							var percent = (event.loaded / event.total) * 100;
	 							console.log(event.loaded+" "+event.total);
	 							if(percent != 100){
	 								 $('.progress-bar').css('width', percent+'%').attr('aria-valuenow', percent);
	 								 $('.progress-bar').html(Math.floor(percent)+'%');
	 							} else {
	 								setTimeout(function(){
		 								$('.progress-bar').css('width', '100%').attr('aria-valuenow', '100').html("100%");
			 							$('.progress-bar').html('100%');
										$('.progress-bar').addClass( "progress-bar-success" );
			 							$('.text-center').css('display','block');
									}, 500);
		 							
	 							}
	 					}, false);
	
	/*ajax.addEventListener("error", function(event){
						console.log("���: ���� ����� �� ����� �����");
					}, false);
	ajax.addEventListener("abort", function(event){
						console.log("no_files");
					}, false);*/
	 /*ajax.upload.addEventListener("load", function(event){
		 $('.progress-bar').css('width', '100%').attr('aria-valuenow', '100').html("100%");
		 $('.progress-bar').html('100%');
		 $('.progress-bar').addClass( "progress-bar-success" );
		 $('.text-center').css('display','block');
	 }, false);*/
	ajax.addEventListener("load", function(event){
		 window.location.reload();
	 }, false);
	 /*ajax.onreadystatechange = function() {
		var status;
		var data;
		// https://xhr.spec.whatwg.org/#dom-xmlhttprequest-readystate
		if (ajax.readyState == 4) { // `DONE`
			status = ajax.status;
			if (status == 200) {
				data = JSON.parse(ajax.responseText);
				alert(data);
				successHandler && successHandler(data);
			} else {
				alert("error "+status);
				errorHandler && errorHandler(status);
			}
		}
	};*/
	 var path= "<?=site_url('import_cases/excel_import' )?>";
	 ajax.open("POST", path,true);
	 var formdata = new FormData();
		formdata.append("upload",_("upload").files[0]);
	 ajax.send(formdata);	
	 ajax.responseType = "text";	 
} else{
bootbox.dialog({ 
	 message: '<p><i class="fa fa-spin fa-spinner"></i> '+"<?= FILE_ERROR ?>"+' </p>' ,
	buttons: {
			confirm: {
				label: "<?= CLOSE?>",
				className: 'red'
			}
		}});
}
	 }
 
</script>

<style type="text/css">
	@media only screen and (max-width: 500px) {
		.caption-with-search{
			float: none !important;
			display: block !important;
		}
	}
</style>


</head>
<!-- END HEAD -->

<body class="page-container-bg-solid">

	<div class="page-wrapper">
<?php
if ($user_type_code == "ADMIN") {
	$this->load->view ( 'office_admin/office_admin_header' );
} else {
	$this->load->view ( 'secretary_lawyer/secretary_header' );
}
?>

<div class="container">

			<a id="hiddenAdd" href="<?=site_url('case_management/add_case')?>"
				style="visibility: hidden; display: none;"></a>
				
			<input type="hidden" name="curr_reg" id="curr_reg" value="<?php echo $selected_region;?>">

			<div id="show_error" class="form-group"
				style="display: none; color: red;">
			<?php echo MAX_CASES_COUNT_REACHED;?>
	</div>

			<!-- 
<form id="searchForm" role="form" method="post"
												enctype="multipart/form-data"
												
													action="<?=site_url('case_management/view_cases')?>"
												class="form-horizontal">
												<input type="hidden" name="search_mode" id="search_mode" value="1">
												<input type="hidden" name="customer_id" id="customer_id" value="<?php echo $search_values["customer_id"]?>">
												<input type="hidden" name="contract_number" id="contract_number" value="<?php echo $search_values["contract_number"]?>">
												<input type="hidden" name="debtor_name" id="debtor_name" value="<?php echo $search_values["debtor_name"]?>">
												<input type="hidden" name="client_name" id="client_name" value="<?php echo $search_values["client_name"]?>">
												<input type="hidden" name="client_id_number" id="client_id_number" value="<?php echo $search_values["client_id_number"]?>">
												<input type="hidden" name="client_type" id="client_type" value="<?php echo $search_values["client_type"]?>">
												<input type="hidden" name="obligation_value" id="obligation_value" value="<?php echo $search_values["obligation_value"]?>">
												<input type="hidden" name="number_of_late_instalments" id="number_of_late_instalments" value="<?php echo $search_values["number_of_late_instalments"]?>">
												<input type="hidden" name="due_amount" id="due_amount" value="<?php echo $search_values["due_amount"]?>">
												<input type="hidden" name="remaining_amount" id="remaining_amount" value="<?php echo $search_values["remaining_amount"]?>">
												<input type="hidden" name="total_debenture_amount" id="total_debenture_amount" value="<?php echo $search_values["total_debenture_amount"]?>">												
												<input type="hidden" name="creation_date" id="creation_date" value="<?php echo $search_values["creation_date"]?>">
												<input type="hidden" name="executive_order_number" id="executive_order_number" value="<?php echo $search_values["executive_order_number"]?>">
												<input type="hidden" name="circle_number" id="circle_number" value="<?php echo $search_values["circle_number"]?>">
												<input type="hidden" name="decision_34_number" id="decision_34_number" value="<?php echo $search_values["decision_34_number"]?>">
												<input type="hidden" name="decision_34_date" id="decision_34_date" value="<?php echo $search_values["decision_34_date"]?>">
												<input type="hidden" name="decision_46_date" id="decision_46_date" value="<?php echo $search_values["decision_46_date"]?>">
												<input type="hidden" name="state_code" id="state_code" value="<?php echo $search_values["state_code"]?>">
												<input type="hidden" name="region_id" id="region_id" value="<?php echo $search_values["region_id"]?>">
												<input type="hidden" name="lawyer_id" id="lawyer_id" value="<?php echo $search_values["lawyer_id"]?>">
												<input type="hidden" name="executive_order_date" id="executive_order_date" value="<?php echo $search_values["executive_order_date"]?>">
												<input type="hidden" name="advertisement_date" id="advertisement_date" value="<?php echo $search_values["advertisement_date"]?>">
												<input type="hidden" name="suspend_reason_code" id="suspend_reason_code" value="<?php echo $search_values["suspend_reason_code"]?>">
												<input type="hidden" name="closing_reason" id="closing_reason" value="<?php echo $search_values["closing_reason"]?>">
												<input type="hidden" name="case_id" id="case_id" value="<?php echo $search_values["case_id"]?>">
												</form>
												 -->
			<form id="editSearchForm" role="form" method="post"
				enctype="multipart/form-data"
				action="<?=site_url('case_management/view_cases/0/2')?>"
				class="form-horizontal">
				<input type="hidden" name="search_mode" id="search_mode" value="1">
				<input type="hidden" name="customer_id"
					value="<?php echo $search_values["customer_id"]?>"> <input
					type="hidden" name="contract_number"
					value="<?php echo $search_values["contract_number"]?>"> <input
					type="hidden" name="debtor_name"
					value="<?php echo $search_values["debtor_name"]?>"> <input
					type="hidden" name="client_name"
					value="<?php echo $search_values["client_name"]?>"> <input
					type="hidden" name="client_id_number"
					value="<?php echo $search_values["client_id_number"]?>"> <input
					type="hidden" name="client_type"
					value="<?php echo $search_values["client_type"]?>"> <input
					type="hidden" name="obligation_value"
					value="<?php echo $search_values["obligation_value"]?>"> <input
					type="hidden" name="number_of_late_instalments"
					value="<?php echo $search_values["number_of_late_instalments"]?>">
				<input type="hidden" name="due_amount"
					value="<?php echo $search_values["due_amount"]?>"> <input
					type="hidden" name="remaining_amount"
					value="<?php echo $search_values["remaining_amount"]?>"> <input
					type="hidden" name="total_debenture_amount"
					value="<?php echo $search_values["total_debenture_amount"]?>"> <input
					type="hidden" name="creation_date"
					value="<?php echo $search_values["creation_date"]?>"> <input
					type="hidden" name="executive_order_number"
					value="<?php echo $search_values["executive_order_number"]?>"> <input
					type="hidden" name="order_number"
					value="<?php echo $search_values["order_number"]?>"> <input
					type="hidden" name="circle_number"
					value="<?php echo $search_values["circle_number"]?>"> <input
					type="hidden" name="decision_34_number"
					value="<?php echo $search_values["decision_34_number"]?>"> <input
					type="hidden" name="decision_34_date"
					value="<?php echo $search_values["decision_34_date"]?>"> <input
					type="hidden" name="decision_46_date"
					value="<?php echo $search_values["decision_46_date"]?>"> <input
					type="hidden" name="state_code"
					value="<?php echo $search_values["state_code"]?>"> <input
					type="hidden" name="region_id"
					value="<?php echo $search_values["region_id"]?>"> <input
					type="hidden" name="lawyer_id"
					value="<?php echo $search_values["lawyer_id"]?>"> <input
					type="hidden" name="executive_order_date"
					value="<?php echo $search_values["executive_order_date"]?>"> <input
					type="hidden" name="advertisement_date"
					value="<?php echo $search_values["advertisement_date"]?>"> <input
					type="hidden" name="suspend_reason_code"
					value="<?php echo $search_values["suspend_reason_code"]?>"> <input
					type="hidden" name="suspend_date"
					value="<?php echo $search_values["suspend_date"]?>"> <input
					type="hidden" name="closing_reason"
					value="<?php echo $search_values["closing_reason"]?>"> <input
					type="hidden" name="case_id"
					value="<?php echo $search_values["case_id"]?>">
			</form>

			<!-- BEGIN BORDERED TABLE PORTLET-->
			<div class="page-content-inner">
				<div class="row">
					<div class="col-md-13">
						<div class="portlet light portlet-fit ">
							<div class="portlet-title">
							<?php if ($search_mode == "1" || $search_mode == "quick_search"){?>
								<div class="caption">
									<span class="caption-subject font-dark bold uppercase"><?php echo CASES_SEARCH_RESULT; ?></span>
								</div>
								<div class="actions">
									<div style="margin: 0 auto; text-align: left">
										<b><?php echo SEARCH_RESULT_FOUND; ?> (<?php if($search_mode == "1" || $search_mode == "quick_search"){echo $cases_count_all;}else{echo $cases_count;}?>) <?php echo SEARCH_RESULT_CASES; ?></b>
									</div>
								</div>
							<?php } else {?>
								<div class="caption caption-with-search">
									<span class="caption-subject font-dark bold uppercase"><?php echo MY_CASES; ?></span>
									&nbsp;&nbsp;&nbsp;&nbsp;
									<div class="btn-group" style="margin-top: 10px !important;">
										<form id="quickSearchForm" class="search-form"
											action="<?=site_url('case_management/quick_search_cases/'.$selected_state)?>"
											method="POST">
											<div class="input-group" style="width: 300px ">
												<style>
.form-control::-webkit-input-placeholder {
	color: #fff;
}

input:-webkit-autofill {
	-webkit-box-shadow: 0 0 0 1000px #c1a73c inset !important;
	-webkit-text-fill-color: #fff;
}
</style>
												<input id = "quick_search_input" type="text" class="form-control no-validate"
													style="background: #c1a73c; color: #fff; border: #c1a73c;"
													placeholder="<?php echo QUICK_SEARCH; ?>" name="query"
													value=""> <span class="input-group-btn"> <a
													href="javascript:apply_quick_search();" class="btn submit"
													style="background: #c1a73c;"> <i class="icon-magnifier"
														style="color: #fff"></i>
												</a> <a class="btn dark btn-outline btn-circle green btn-sm"
													style="margin-right: 5px "
													href="<?=site_url('case_management/view_cases/0/1')?>"
													data-hover="dropdown" data-close-others="true"> <?php echo ADVANCED_SEARCH; ?>
													</a>
												</span>



											</div>
										</form>

									</div>
								</div>
								<div class="actions" style="margin-left: -20px !important;">
									<?php if ($user_type_code == "ADMIN"){?>
									<div class="btn-group" style="padding: 10px">
										<a class="btn dark btn-outline btn-circle green btn-sm" 
											id = "add_drop-menu"
											data-toggle="dropdown" data-hover="dropdown"
											data-close-others="true" aria-expanded="true"> <?php echo INSERT; ?><i class="fa fa-angle-down"></i>
										</a>
										<ul class="dropdown-menu pull-right">
											<li><a href="javascript:;" onclick="addCase()"> <?php echo ADD_CASE; ?> </a></li>
											<li><a href="javascript:;">
													<label onclick="upload_action();"> <span aria-hidden="true"><?php echo IMPORT_CASES; ?></span></label> 
												</a>
											</li>
										</ul>
									</div>
																		
									<div class="btn-group" style="padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 0px !important;">
										<a class="btn dark btn-outline btn-circle green btn-sm" 
											id = "load_drop-menu"
											data-toggle="dropdown" data-hover="dropdown"
											data-close-others="true" aria-expanded="true"> <?php echo EXPORT; ?><i class="fa fa-angle-down"></i>
										</a>
										<ul class="dropdown-menu pull-right">
											<li><a href="javascript:;" onclick="exportCases(0)"> <?php echo EXPORT_CASES; ?> </a></li>
											<li><a href="javascript:;" onclick="exportCases(1)"> <?php echo EXPORT_CASES_DATA; ?> </a></li>
										</ul>
									</div>
									<?php } else if ($user_type_code == "CUSTOMER"){?>
									<div class="btn-group" style="padding: 10px">
										<a class="btn dark btn-outline btn-circle green btn-sm"
											style="margin-right: 5px" href="javascript:;"
											onclick="addCase()" data-hover="dropdown"
											data-close-others="true"> <?php echo ADD_CASE; ?>
										</a>
									</div>
									
									<?php } ?>
								</div>
								
								<div id="import_div" style="height: 0px !important;width:0px !important;"> 
									<form id="uploadCasesForm" role="form" method="post"
										enctype="multipart/form-data"
										action="<?=site_url('import_cases/excel_import' )?>"
										class="form-horizontal" >
										<input type="file" 
											id="upload" name="upload" style="visibility:hidden;">
										<input type="hidden" name="hide" value="hello">
									</form>
								</div>
								
							<?php } ?>
								
						
						
					</div>
						</div>
				<?php
				
				if ($search_mode == "0") {
					$this->load->view ( 'cases/progress_bar' );
					?>
					<br>
					<?php
					if (isset ( $case_region_count )) {
						$this->load->view ( 'cases/region_progress_bar' );
					}
				}
				?>
				
						<div class="table-scrollable"
							style="height: 500px; overflow-y: scroll">
							<table class="table table-bordered table-hover"
								id="table-pagination">
								<thead>
									<tr>
										<th><?php echo CASE_ID; ?></th>

										<th><?php echo CLIENT_NAME; ?></th>
										<th><?php echo CLIENT_ID_NUMBER; ?></th>
										<th><?php echo NUMBER_OF_LATE_INSTALMENTS; ?></th>
										<th><?php echo ASSIGNED_LAWYER; ?></th>
										<th><?php echo LAST_TRANS_DATE; ?></th>
										<th><?php echo CURRENT_STATE_CODE; ?></th>
										<th></th>
									</tr>
								</thead>
								<tbody>
					<?php
					if ($cases) {
						for($i = 0; $i < count ( $cases ); $i ++) {
							?>
									<tr>
										<td><?=$cases[$i] ["case_id"];?> </td>

										<td><?=$cases[$i] ["client_name"];?> </td>
										<td><?=$cases[$i] ["client_id_number"];?> </td>
										<td><?=$cases[$i] ["number_of_late_instalments"];?> </td>
										<td><?=$cases[$i] ["lawyer_name"];?> </td>
										<td><?=$cases[$i] ["last_trans_date"];?> </td>
										<td><span
											class=<? if ( $cases[$i] ["current_state_code"]=="INITIAL"){?>
											"label label-sm
											label-new"
										<? }else if ( $cases[$i] ["current_state_code"]=="ASSIGNED_TO_LAWYER"){?>
										"label
											label-sm
											label-under-action"
										<? }else if ( $cases[$i] ["current_state_code"]=="CLOSED"){?>
										"label
											label-sm
											label-danger"
										<? }else if ( $cases[$i] ["current_state_code"]=="DECISION_34"){?>
										"label
											label-sm
											label-dec-four"
										<? }else if ( $cases[$i] ["current_state_code"]=="DECISION_46"){?>
										"label
											label-sm
											label-dec-six"
										<? }else if ( $cases[$i] ["current_state_code"]=="MODIFICATION_REQUIRED"){?>
										"label
											label-sm
											label-modf-req"
										<? }else if ( $cases[$i] ["current_state_code"]=="PUBLICLY_ADVERTISED"){?>
										"label
											label-sm
											label-adv"
										<? }else if ( $cases[$i] ["current_state_code"]=="REGISTERED"){?>
										"label
											label-sm
											label-exec"
										<? }else if ( $cases[$i] ["current_state_code"]=="UNDER_REVIEW"){?>
										"label
											label-sm
											label-under-rev"
										<?}else if ( $cases[$i] ["current_state_code"]=="SUSPENDED"){?>
										"label
											label-sm label-suspend"
										<?}?>> <?=$cases[$i] ["state_name"];?></span></td>
										<td>
										<?php if ($search_mode == "1" || $search_mode == "quick_search"){?>
										<button id="case_detail" type="button"
												onclick=" win_popup('<?=site_url('case_management/view_case_details/'.$cases[$i] ["case_id"].'/1')?>');"
												class="btn btn-outline btn-circle btn-sm purple">
												<i class="fa fa-edit"></i> <?php echo VIEW; ?>
										</button>
										<?php } else {?>
										<a
																					href="<?=site_url('case_management/view_case_details/'.$cases[$i] ["case_id"])?>"
																					class="btn btn-outline btn-circle btn-sm purple">
												<i class="fa fa-edit"></i> <?php echo VIEW; ?></a>
										<?php } ?>
									</td>
									</tr>	
						<?php
						}
					} else {
						?><tr class="HeadBr2">
										<td colspan="22" style="color: red" align="center"><h4>
									<?php echo NO_RECORDS_FOUND; ?>
									</h4></td>
									</tr> <?php }?>
						</tbody>
							</table>
						</div>
						<!-- END BORDERED TABLE PORTLET-->

						<!-- START TABLE PAGINATION -->
						<div >
							<ul >
								<?
								$count_all = sizeof ( $cases );
								$state = 0;
								if ($search_mode == "1" || $search_mode == "quick_search") {
									$count_all = $cases_count_all;
								} else {
									if(isset($cases_count_all)){
										$count_all = $cases_count_all;
									}else{
										switch ($selected_state) {
											case "UNDER_REVIEW" :
												$count_all = $UNDER_REVIEW_count;
												break;
											case "ASSIGNED_TO_LAWYER" :
												$count_all = $ASSIGNED_TO_LAWYER_count;
												break;
											case "MODIFICATION_REQUIRED" :
												$count_all = $MODIFICATION_REQUIRED_count;
												break;
											case "REGISTERED" :
												$count_all = $REGISTERED_count;
												break;
											case "DECISION_34" :
												$count_all = $DECISION_34_count;
												break;
											case "PUBLICLY_ADVERTISED" :
												$count_all = $PUBLICLY_ADVERTISED_count;
												break;
											case "DECISION_46" :
												$count_all = $DECISION_46_count;
												break;
											case "CLOSED" :
												$count_all = $CLOSED_count;
												break;
											case "SUSPENDED" :
												$count_all = $SUSPENDED_count;
												break;
											default :
												// $count_all = sizeof($cases);
												$count_all = $UNDER_REVIEW_count + $ASSIGNED_TO_LAWYER_count + $REGISTERED_count + $DECISION_34_count + $PUBLICLY_ADVERTISED_count + $DECISION_46_count + $CLOSED_count + $SUSPENDED_count + $MODIFICATION_REQUIRED_count;
										}
									}
								}
								
								$no_of_pages = ceil ( $count_all / 50 );
								//if ($search_mode != "quick_search") {
									if ($no_of_pages > 1) {
										?>
										
								<input type="hidden" name="calculated_current_page" id="calculated_current_page" value="1">
								<input type="hidden" name="no_of_pages" id="no_of_pages" value="<?php echo $no_of_pages; ?>">
								
								<div id="paginationForm">
									<div style="text-align: center; margin: auto;">
										<input type="button" class="btn btn-circle green"
											id="prev_button"
											onClick="javascript:get_prev_page()"
											disabled value="<?php echo PREVIOUS ;?>" />
											<span
											style="margin-left: 5px; margin-right: 5px; text-align: center;">
											<b><?php echo PAGE_NUM. ": "; ?></b>
											<input type="text" act="yes" onkeydown="numberKeyDown(event)" 
												class="no-spinners"
												id="desired_page_num" 
												name="desired_page_num"
												style="width:40px;"
												value="1"> 
											<b><?php echo ' / '.$no_of_pages; ?></b>
											</span>
										<input type="button" class="btn btn-circle green"
											id="next_button"
											onClick="javascript:get_next_page()" value="<?php echo NEXT ;?>" />
									</div>
									
								</div>
								
									<?php
									
									}
								//}
								?>
									
							</ul>
						</div>
						
						<br>
						
						<!-- END TABLE PAGINATION -->

					<?php if ($search_mode == "1" || $search_mode == "quick_search"){?>
							<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-5 col-md-10">
											<?php if ($search_mode == "1"){?>
											<button type="button" onclick="edit_search()"
										class="btn btn-circle green"><?php
							echo EDIT_SEARCH;
							
							?></button>
											<?php } ?>
													<a class="btn btn-circle green"
										href="<?=site_url('case_management/view_cases')?>"
										data-hover="dropdown" data-close-others="true"> <?php echo CANCEL_SEARCH; ?>
										</a>
								</div>
							</div>
						</div>
						<br>
									<?php } ?>


					</div>

				</div>
			</div>
		</div>
	</div>

<?php $this->load->view('utils/footer');?>

<!-- BEGIN CORE PLUGINS -->
	<script src="<?=base_url()?>assets/global/plugins/jquery.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.sparkline.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-table/bootstrap-table.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="<?=base_url()?>assets/global/scripts/app.min.js"
		type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?=base_url()?>assets/pages/scripts/profile.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script
		src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js"
		type="text/javascript"></script>
        <script type="text/javascript"
	        src="<?php echo base_url()?>js/bootbox.min.js"></script>
  	<!-- END THEME LAYOUT SCRIPTS -->
	<script>
        
/*
	function updateSearchValues(customer_id, contract_number, debtor_name, client_name, client_id_number, client_type, obligation_value,
			number_of_late_instalments, due_amount, remaining_amount, total_debenture_amount, creation_date, executive_order_number, circle_number,
			decision_34_number, decision_34_date, decision_46_date, state_code, region_id, lawyer_id, executive_order_date, advertisement_date,
			suspend_reason_code, closing_reason, case_id) {
		document.getElementById('customer_id').value = customer_id;
		document.getElementById('contract_number').value = contract_number;
		document.getElementById('debtor_name').value = debtor_name;
		document.getElementById('client_name').value = client_name;
		document.getElementById('client_id_number').value = client_id_number;
		document.getElementById('client_type').value = client_type;
		document.getElementById('obligation_value').value = obligation_value;
		document.getElementById('number_of_late_instalments').value = number_of_late_instalments;
		document.getElementById('due_amount').value = due_amount;
		document.getElementById('remaining_amount').value = remaining_amount;
		document.getElementById('total_debenture_amount').value = total_debenture_amount;		
		document.getElementById('creation_date').value = creation_date;
		document.getElementById('executive_order_number').value = executive_order_number;
		document.getElementById('circle_number').value = circle_number;
		document.getElementById('decision_34_number').value = decision_34_number;
		document.getElementById('decision_34_date').value = decision_34_date;
		document.getElementById('decision_46_date').value = decision_46_date;
		document.getElementById('state_code').value = state_code;
		document.getElementById('region_id').value = region_id;
		document.getElementById('lawyer_id').value = lawyer_id;
		document.getElementById('executive_order_date').value = executive_order_date;
		document.getElementById('advertisement_date').value = advertisement_date;
		document.getElementById('suspend_reason_code').value = suspend_reason_code;
		document.getElementById('closing_reason').value = closing_reason;
		document.getElementById('case_id').value = case_id;
		document.getElementById('searchForm').submit();
	}*/
	</script>

</body>

</html>
