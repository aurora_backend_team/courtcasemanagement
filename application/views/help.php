<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<?php
$this->load->view ( 'localization/lang' );
$this->load->view ( 'localization/txt' );
defineLocale ();
defineStrings ();
?>

<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title><?php echo FAQ_TITLE; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin RTL Theme #3 for general faq page"
	name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?=base_url()?>assets/global/css/components-rtl.min.css"
	rel="stylesheet" id="style_components" type="text/css" />
<link href="<?=base_url()?>assets/global/css/plugins-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?=base_url()?>assets/pages/css/faq-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link
	href="<?=base_url()?>assets/layouts/layout3/css/layout-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/layouts/layout3/css/themes/default-rtl.min.css"
	rel="stylesheet" type="text/css" id="style_color" />
<link
	href="<?=base_url()?>assets/layouts/layout3/css/custom-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon"
	href="<?=base_url()?>assets/layouts/layout3/img/logo-default.jpg" />
<link rel="manifest" href="https://altanfeeth.com/manifest.json">
	

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	
<link rel="shortcut icon"
	href="<?=base_url()?>assets/layouts/layout3/img/logo-default.jpg" />
	
<script>
$(document).ready(function(){
    $("#searchTextId").keyup(function(event){	
	    if(event.keyCode == 13){
  		var searchTextVal = $('#searchTextId').val();
		var postdata = {searchTextVal : searchTextVal};
		var url = "<?php echo site_url('help/view_FAQs'); ?>";
		$.post(url, postdata, function(data) {
	            var results = JSON.parse(data);
	            var newSearchArea = $(results).find("#searchResultsDiv");
	            $("#searchResultsDiv").html(newSearchArea);	           
	        });

	    }
    });
       
    /*
    $('#searchTextId').on('keypress', function (e) {
         if(e.which === 13){
         	var searchTextVal = $('#searchTextId').val();
	        $.ajax({
		    	url: '<?php echo site_url('help/view_FAQs'); ?>',
		        type: 'POST',
		        data: {
		            key: searchTextVal 
		        },
		        dataType: 'json',
		        success: function(data) {
		            console.log(data);
		        }
		
		});
         }
    });
    */
});
    	 

</script>

<!-- END HEAD -->
</head>

<body id="allBody" class="page-container-bg-solid">
	<div class="page-wrapper">
	
<?php
if($viewHeaders == '1'){
	if ($user_type_code == "ADMIN") {
		$this->load->view ( 'office_admin/office_admin_header' );
		$this->load->view('utils/date_scripts');
	} else {
		$this->load->view ( 'secretary_lawyer/secretary_header' );
	}
}
?>

		<div class="page-wrapper-row full-height">
			<div class="page-wrapper-middle">
				<!-- BEGIN CONTAINER -->
				<div class="page-container">
					<!-- BEGIN CONTENT -->
					<div class="page-content-wrapper">
						<!-- BEGIN CONTENT BODY -->
						<!-- BEGIN PAGE HEAD-->
						<div class="page-head">
							<div class="container">
							
							<?php
		if($viewHeaders == '0'){ ?>
			<!-- BEGIN LOGO -->
			<span class="page-logo" style="float:right;">
				<a> <img style="margin-left: 50px !important;"
					src="<?=base_url()?>assets/layouts/layout3/img/logo-default.jpg"
					alt="logo" class="logo-default">
				</a>
			</span>
			<!-- END LOGO -->
		<?php	}
		?>
								<!-- BEGIN PAGE TITLE -->
								<span class="page-title">
									<h1>
										<?php echo FAQ_TITLE; ?> <small><?php echo FAQ_Desc; ?></small>
									</h1>
								</span>
								<!-- END PAGE TITLE -->
							</div>
						</div>
						<!-- END PAGE HEAD-->
						<!-- BEGIN PAGE CONTENT BODY -->
						<div class="page-content">
							<div class="container">
								<!-- BEGIN PAGE CONTENT INNER -->
								<div class="page-content-inner">
									<div class="faq-page faq-content-1">
										<div class="search-bar ">
											<div class="input-group">
											
												<input id="searchTextId" class="form-control"
													placeholder="<?php echo FAQ_Search; ?>">
													
											</div>
										</div>
										<div id="searchResultsDiv" class="faq-content-container">
											<div class="row">
                                    
                                                    <div
													class="col-md-6">
									<?php
			                             if ($categories) {
			                             	for($i = 0; $i < count ( $categories); $i ++) {
			                             		$FAQsArr = $categories[$i];
			                             		$catName = $FAQsArr[0]["category_descAr"];
												?>
									 <?php if ($i%2 == 0) { ?>
                                                        <div
														class="faq-section ">
														<h2 class="faq-title uppercase font-blue" style="font-size: 20px !important;"><?=$catName?></h2>
														<div class="panel-group accordion faq-content"
															id="accordion_<?php echo $i;?>">
														  <?php			
															for($j = 0; $j < count ( $FAQsArr); $j ++) {
																$question= $FAQsArr[$j] ["question"];
						                             			$answer= $FAQsArr[$j] ["answer"];
			                             					?>
															<div class="panel panel-default">
																<div class="panel-heading">
																	<h4 class="panel-title">
																		<i class="fa fa-circle"></i> <a
																			class="accordion-toggle" data-toggle="collapse"
																			data-parent="#accordion_<?php echo $i;?>" href="#collapse_<?php echo $i;?>_<?php echo $j;?>"> <?=$question?></a>
																	</h4>
																</div>
																<div id="collapse_<?php echo $i;?>_<?php echo $j;?>" class="panel-collapse collapse">
																	<div class="panel-body">
																		<p><?=$answer?></p>
																	</div>
																</div>
															</div>
															
															<?php } 	?>
															
														</div>
													</div>
                                                       
                                                      <?php } 	?>
                                                      <?php } } 	?>
                                                    </div>
 										

												<div class="col-md-6">
												<?php
			                             if ($categories) {
			                             	for($i = 0; $i < count ( $categories); $i ++) {
			                             		$FAQsArr = $categories[$i];
			                             		$catName = $FAQsArr[0]["category_descAr"];
												?>
												
                                                     <?php if ($i%2 == 1) { ?>
                                                        <div
														class="faq-section ">
														<h2 class="faq-title uppercase font-blue" style="font-size: 20px !important;"><?=$catName?></h2>
														<div class="panel-group accordion faq-content"
															id="accordion_<?php echo $i;?>">
															
														<?php			
															for($j = 0; $j < count ( $FAQsArr); $j ++) {
																$question= $FAQsArr[$j] ["question"];
						                             			$answer= $FAQsArr[$j] ["answer"];
			                             					?>
			                             					
															<div class="panel panel-default">
																<div class="panel-heading">
																	<h4 class="panel-title">
																		<i class="fa fa-circle"></i> <a
																			class="accordion-toggle" data-toggle="collapse"
																			data-parent="#accordion_<?php echo $i;?>" href="#collapse_<?php echo $i;?>_<?php echo $j;?>"><?=$question?></a>
																	</h4>
																</div>
																<div id="collapse_<?php echo $i;?>_<?php echo $j;?>" class="panel-collapse collapse">
																	<div class="panel-body">
																		<p><?=$answer?></p>
																	</div>
																</div>
															</div>
															
															 <?php } 	?>
														</div>
													</div>
                                                        
                                                <?php }	?>
                                                <?php } } 	?>
                                                
                                                    </div>
                                                    
                                                   
											</div>

										</div>
									</div>
								</div>
								<!-- END PAGE CONTENT INNER -->
							</div>
						</div>
						<!-- END PAGE CONTENT BODY -->
						<!-- END CONTENT BODY -->
					</div>
					<!-- END CONTENT -->
					
				</div>
				<!-- END CONTAINER -->
			</div>
		</div>

	</div>

	<!--[if lt IE 9]>
<script src="<?=base_url()?>assets/global/plugins/respond.min.js"></script>
<script src="<?=base_url()?>assets/global/plugins/excanvas.min.js"></script> 
<script src="<?=base_url()?>assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
	<!-- BEGIN CORE PLUGINS -->
	<script src="<?=base_url()?>assets/global/plugins/jquery.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="<?=base_url()?>assets/global/scripts/app.min.js"
		type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script
		src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js"
		type="text/javascript"></script>
	<!-- END THEME LAYOUT SCRIPTS -->
</body>

</html>