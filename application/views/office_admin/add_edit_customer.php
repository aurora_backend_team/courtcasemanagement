<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<?php
// session_start ();
$this->load->view ( 'localization/lang' );
$this->load->view ( 'localization/txt' );
defineLocale ();
defineStrings ();
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>

<script type="text/javascript">

function isValidDate(dateVar){
	//Format: yyyy/mm/dd
	var dateReg = /^\d{4}\/\d{2}\/\d{2}$/;
	var isV = dateReg.test(dateVar);
	if(!isV){
		return false;
	} 
	
	var str_array = dateVar.split('/');
	var hijriDate =  jqOld.calendars.instance('ummalqura','ar').newDate();
	try {
	   hijriDate = hijriDate.date(str_array[0], str_array[1], str_array[2]);
	}
	catch(err) {
	   return false;
	}
	return true;
}


function checkValid() {
	if (textNumViolation())
		return false;
	replacAllArabToEng();
	var user_name = document.forms["myForm"]["user_name"].value;
	var password = document.forms["myForm"]["password"].value;
	var retyped_password = document.forms["myForm"]["retyped_password"].value;
	var email = document.forms["myForm"]["email"].value;
	var mobile_number = document.forms["myForm"]["mobile_number"].value;
	var phone_number = document.forms["myForm"]["phone_number"].value;
	var show_invoice_module = document.forms["myForm"]["show_invoice_module"].value;
	var show_dashboard = document.forms["myForm"]["show_dashboard"].value;
	var stage_region1 = document.forms["myForm"]["stage_region1"].value;
	var stage_region2 = document.forms["myForm"]["stage_region2"].value;
	var stage_region3 = document.forms["myForm"]["stage_region3"].value;
	var stage_region4 = document.forms["myForm"]["stage_region4"].value;
	var stage_region5 = document.forms["myForm"]["stage_region5"].value;
	var stage_cost1 = document.forms["myForm"]["stage_cost1"].value;
	var stage_cost2 = document.forms["myForm"]["stage_cost2"].value;
	var stage_cost3 = document.forms["myForm"]["stage_cost3"].value;
	var stage_cost4 = document.forms["myForm"]["stage_cost4"].value;
	var stage_cost5 = document.forms["myForm"]["stage_cost5"].value;
	var subscription_start_date = document.forms["myForm"]["subscription_start_date"].value;
	var subscription_end_date = document.forms["myForm"]["subscription_end_date"].value;
	var number_of_cases_per_month = document.forms["myForm"]["number_of_cases_per_month"].value;
	var first_name = document.forms["myForm"]["first_name"].value;
	var last_name = document.forms["myForm"]["last_name"].value;
	var mobile_pattern = /05/g;

	if (first_name == "") {
		$("#first_name_tooltip").css("display","block");
		$("#first_name_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#first_name_tooltip").css("display","none");
		$("#first_name_tooltip").css("visibility","none");
	}
	if (last_name == "") {
		$("#last_name_tooltip").css("display","block");
		$("#last_name_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#last_name_tooltip").css("display","none");
		$("#last_name_tooltip").css("visibility","none");
	}
	if (number_of_cases_per_month  == "-" || number_of_cases_per_month.length > 7 || number_of_cases_per_month  == "") {
		$("#number_of_cases_per_month_tooltip").css("display","block");
		$("#number_of_cases_per_month_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#number_of_cases_per_month_tooltip").css("display","none");
		$("#number_of_cases_per_month_tooltip").css("visibility","none");
	}

	if(subscription_start_date != ""){
		var isValidD = isValidDate(subscription_start_date);
	   	if(!isValidD){
			$("#start_date_tooltip2").css("display","block");
			$("#start_date_tooltip2").css("visibility","visible");
	   			
	   		return false;
	   	}else{
			$("#start_date_tooltip2").css("display","none");
			$("#start_date_tooltip2").css("visibility","none");
	   	}
	}

	if(subscription_end_date != ""){
		var isValidD = isValidDate(subscription_end_date);
	   	if(!isValidD){
			$("#end_date_tooltip2").css("display","block");
			$("#end_date_tooltip2").css("visibility","visible");

			$("#end_date_tooltip").css("display","none");
			$("#end_date_tooltip").css("visibility","none");
	   			
	   		return false;
	   	}else{
			$("#end_date_tooltip2").css("display","none");
			$("#end_date_tooltip2").css("visibility","none");
	   	}
	}
   		
	if (subscription_end_date != "" && subscription_start_date != "" && subscription_end_date < subscription_start_date) {
		$("#end_date_tooltip").css("display","block");
		$("#end_date_tooltip").css("visibility","visible");

		$("#end_date_tooltip2").css("display","none");
		$("#end_date_tooltip2").css("visibility","none");
		
		return false;
	}else{
		$("#end_date_tooltip").css("display","none");
		$("#end_date_tooltip").css("visibility","none");
	}
	if (email == "" || !validateEmail(email)) {
		$("#email_tooltip").css("display","block");
		$("#email_tooltip").css("visibility","visible");
		
		return false;
	}else{
		$("#email_tooltip").css("display","none");
		$("#email_tooltip").css("visibility","none");
	}
	if (user_name == "") {
		$("#user_name_tooltip").css("display","block");
		$("#user_name_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#user_name_tooltip").css("display","none");
		$("#user_name_tooltip").css("visibility","none");
	}
	if (password == "") {
		$("#password_tooltip").css("display","block");
		$("#password_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#password_tooltip").css("display","none");
		$("#password_tooltip").css("visibility","none");
	}
	if (retyped_password == "" || password != retyped_password) {
		$("#retyped_password_tooltip").css("display","block");
		$("#retyped_password_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#retyped_password_tooltip").css("display","none");
		$("#retyped_password_tooltip").css("visibility","none");
	}
	if (show_invoice_module == "" ) {
		$("#show_invoice_module_tooltip").css("display","block");
		$("#show_invoice_module_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#show_invoice_module_tooltip").css("display","none");
		$("#show_invoice_module_tooltip").css("visibility","none");
	}
	if (show_dashboard == "") {
		$("#show_dashboard_tooltip").css("display","block");
		$("#show_dashboard_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#show_dashboard_tooltip").css("display","none");
		$("#show_dashboard_tooltip").css("visibility","none");
	}

	
	if (mobile_number != "" && (mobile_number.length < 10 || mobile_number.length > 10 ||
        !mobile_pattern.test(mobile_number))) {
		$("#mobile_number_tooltip").css("display","block");
		$("#mobile_number_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#mobile_number_tooltip").css("display","none");
		$("#mobile_number_tooltip").css("visibility","none");
	}
	if (phone_number != "" && (phone_number.length < 10 || phone_number.length > 10)) {
			$("#phone_number_tooltip").css("display","block");
			$("#phone_number_tooltip").css("visibility","visible");
			return false;
	} else{
			$("#phone_number_tooltip").css("display","none");
			$("#phone_number_tooltip").css("visibility","none");
	}

	if (stage_region1 == "") {
		$("#stage_region1_tooltip").css("display","block");
		$("#stage_region1_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#stage_region1_tooltip").css("display","none");
		$("#stage_region1_tooltip").css("visibility","none");
	}

	if (stage_region2 == "") {
		$("#stage_region2_tooltip").css("display","block");
		$("#stage_region2_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#stage_region2_tooltip").css("display","none");
		$("#stage_region2_tooltip").css("visibility","none");
	}

	if (stage_region3 == "") {
		$("#stage_region3_tooltip").css("display","block");
		$("#stage_region3_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#stage_region3_tooltip").css("display","none");
		$("#stage_region3_tooltip").css("visibility","none");
	}

	if (stage_region4 == "") {
		$("#stage_region4_tooltip").css("display","block");
		$("#stage_region4_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#stage_region4_tooltip").css("display","none");
		$("#stage_region4_tooltip").css("visibility","none");
	}

	if (stage_region5 == "") {
		$("#stage_region5_tooltip").css("display","block");
		$("#stage_region5_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#stage_region5_tooltip").css("display","none");
		$("#stage_region5_tooltip").css("visibility","none");
	}

	if (stage_cost1== "" || stage_cost1 <= 0 || stage_cost1 =="-" || stage_cost1.length > 7) {
		$("#stage_cost1_tooltip").css("display","block");
		$("#stage_cost1_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#stage_cost1_tooltip").css("display","none");
		$("#stage_cost1_tooltip").css("visibility","none");
	}

	if (stage_cost2== "" || stage_cost2 <= 0 || stage_cost2 =="-" || stage_cost2.length > 7) {
		$("#stage_cost2_tooltip").css("display","block");
		$("#stage_cost2_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#stage_cost2_tooltip").css("display","none");
		$("#stage_cost2_tooltip").css("visibility","none");
	}

	if (stage_cost3== "" || stage_cost3 <= 0 || stage_cost3 =="-"  || stage_cost3.length > 7) {
		$("#stage_cost3_tooltip").css("display","block");
		$("#stage_cost3_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#stage_cost3_tooltip").css("display","none");
		$("#stage_cost3_tooltip").css("visibility","none");
	}

	if (stage_cost4== "" || stage_cost4 <= 0 || stage_cost4 =="-"  || stage_cost4.length > 7) {
		$("#stage_cost4_tooltip").css("display","block");
		$("#stage_cost4_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#stage_cost4_tooltip").css("display","none");
		$("#stage_cost4_tooltip").css("visibility","none");
	}

	if (stage_cost5== "" || stage_cost5 <= 0 || stage_cost5 =="-"  || stage_cost5.length > 7) {
		$("#stage_cost5_tooltip").css("display","block");
		$("#stage_cost5_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#stage_cost5_tooltip").css("display","none");
		$("#stage_cost5_tooltip").css("visibility","none");
	}
	$('#loading').removeClass('hidden');
}

function validateEmail(email) 
{
	//var re = /\S+@\S+\.\S+/;
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}

var stage1_count = 2;
var stage2_count = 2;
var stage3_count = 2;
var stage4_count = 2;
var stage5_count = 2;
function addInStage(stage_data_region_id, stage_data_cost, stage_order) {
	var stage_count = 0;
	switch(stage_order) {
	    case "1":
	    	stage_count =  stage1_count;
	        break;
	    case "2":
	    	stage_count =  stage2_count;
	        break;
	    case "3":
	    	stage_count =  stage3_count;
	        break;
	    case "4":
	    	stage_count =  stage4_count;
	        break;
	    case "5":
	    	stage_count =  stage5_count;
	        break;
	    default:
	    	stage_count =  stage1_count;
	}

	
	 var data='<div><label class="col-md-3 control-label"></label>';
	 data+= '<div class="form-group"><label class="col-md-1 control-label">'+'<?php echo REGION; ?>'+'<span	class="required">   </span></label>';
	 data+= '<div class="col-md-3"><select class="form-control " name="stage_region'+stage_order+'[]" id="stage_region'+stage_order+'_'+stage_count+'" class="form-control">';
	 
		var available_regions = <?php echo json_encode($available_regions); ?>;
		if (available_regions) {
			for (var i = 0; i < available_regions.length; i++) { 
   				var region_id_s = available_regions[i]["region_id"];
				var name = available_regions[i]["name"];
				
				data+= '<option value="'+region_id_s+'"';
					if (stage_data_region_id == region_id_s){
					data+= 'selected="selected"';
					}
					data+='> '+name+'</option>';
				
			}
		} else {
		
					data+='<option value="" selected></option>';
				
		}
				
			
		
			data+='</select ></div><label class="col-md-1 control-label">'+'<?php echo STAGE_COST; ?>'+'<span class="required">   </span></label><div class="col-md-3">';
			data+='<input type="text" act="yes" class="form-control  no-spinners" name="stage_cost'+stage_order+'[]" id="stage_cost'+stage_order+'_'+stage_count+'"';
			if (stage_data_cost != "") {
				data+='value="'+stage_data_cost+'"'; 
			} else { 
				data+='value=""'; 
			}
			
			data+=' > </div> <button type="button" onclick="javascript:removeFromStage(this,'+stage_order+')" class="btn btn-outline btn-circle dark btn-sm black add-delete-button"><i class="fa fa-trash-o"></i>'+'<?php echo DELETE; ?>'+'</button></div></div>';

	var div_id = "#costs_for_stage"+stage_order;
  	$(div_id).append(data);

  	var counts_id = "stage_data_count_"+stage_order;
  	document.getElementById(counts_id).value = stage_count;
  	switch(stage_order) {
	    case "1":
	    	stage1_count++;
	        break;
	    case "2":
	    	stage2_count++;
	        break;
	    case "3":
	    	stage3_count++;
	        break;
	    case "4":
	    	stage4_count++;
	        break;
	    case "5":
	    	stage5_count++;
	        break;
	    default:
	    	stage1_count++;
	}
    
}



function removeFromStage(element, stage_order) {
	// alert("here");
	$(element).parent().parent().remove();
	var stage_count = 0;
	
	if (stage_order == 1) {
		stage1_count--;
		stage_count =  stage1_count;
	} else if (stage_order == 2) {
		stage2_count--;
		stage_count =  stage2_count;
	} else if (stage_order == 3) {
		stage3_count--;
		stage_count =  stage3_count;
	} else if (stage_order == 4) {
		stage4_count--;
		stage_count =  stage4_count;
	} else if (stage_order == 5) {
		stage5_count--;
		stage_count =  stage5_count;
	}
	var counts_id = "stage_data_count_"+stage_order;
  	document.getElementById(counts_id).value = stage_count - 1;
	
}


</script>
<meta charset="utf-8" />
<title><?php echo TITLE; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin RTL Theme #3 for dashboard & statistics"
	name="description" />
<meta content="" name="author" />

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css"
	rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/global/plugins/morris/morris.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/fullcalendar/fullcalendar.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/jqvmap.css"
	rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?=base_url()?>assets/global/css/components-rtl.min.css"
	rel="stylesheet" id="style_components" type="text/css" />
<link href="<?=base_url()?>assets/global/css/plugins-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link
	href="<?=base_url()?>assets/layouts/layout3/css/layout-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/layouts/layout3/css/themes/default-rtl.min.css"
	rel="stylesheet" type="text/css" id="style_color" />
<link
	href="<?=base_url()?>assets/layouts/layout3/css/custom-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon"
	href="<?=base_url()?>assets/layouts/layout3/img/logo-default.jpg" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $('#myOptions').change(function() {
    var val = $("#myOptions option:selected").val();
    window.location="<?=site_url('case_management/manage_case/'.$case_id.'/'.$action_code)?>"+"/"+val;
  });
});
</script>
<link rel="stylesheet" type="text/css"
	href="<?=base_url()?>assets/css/jquery.calendars.picker.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?=base_url()?>assets/js/jquery.plugin.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery.calendars.all.js"></script>
<script src="<?=base_url()?>assets/js/jquery.calendars.plus.js"></script>
<script src="<?=base_url()?>assets/js/jquery.calendars.ummalqura.js"></script>
<script src="<?=base_url()?>assets/js/jquery.calendars.ummalqura-ar.js"></script>
<script>
var jqOld = jQuery.noConflict();
jqOld( function() {
	jqOld( "input.date" ).calendarsPicker({calendar: jqOld.calendars.instance('ummalqura','ar')});
  } );
 jqOld( function() {
	    jqOld( "input.dateMilady" ).calendarsPicker({calendar: jqOld.calendars.instance('gregorian','ar')});
  } );
  </script>

<style type="text/css">
	.add-delete-button{
		margin-top: 3px !important;
	}
	@media only screen and (max-width: 991px) {
		.add-delete-button{
			margin-top: 3px !important;
			margin-right: 15px !important;
		}
	}
</style>
</head>
<!-- END HEAD -->
<body class="page-container-bg-solid">
	<div class="page-wrapper">
<?php include 'office_admin_header.php';?>


				<div class="page-wrapper-row full-height">
			<div class="page-wrapper-middle">
				<!-- BEGIN CONTAINER -->
				<div class="page-container">
					<!-- BEGIN CONTENT -->
					<div class="page-content-wrapper">
						<!-- BEGIN CONTENT BODY -->
						<!-- BEGIN PAGE HEAD-->
						<div class="page-head">
							<div class="container">
								<div class="col-md-10">
									<div class="portlet box green">
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-gift"></i>
												<?php
												if ($user_id) {
													echo $first_name . ' ' . $last_name;
												} else {
													echo ADD_RECORD;
												}
												?>
											</div>

										</div>
										<div class="portlet-body form">
											<!-- BEGIN FORM-->
											<form id="myForm" role="form" method="post"
												onsubmit="javascript:return checkValid();"
												action="<?=site_url('lawyer_office_panel/add_edit_customer/'.$user_id)?>"
												class="form-horizontal">
												<div class="form-body">
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo FIRST_NAME; ?><span
																	class="required"> * </span></label>
																<div class="col-md-6">
																	<input type="text" class="form-control "
																		name="first_name" value="<?php echo $first_name?>"> <span
																		id="first_name_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo LAST_NAME; ?><span
																	class="required"> * </span></label>
																<div class="col-md-6">
																	<input type="text" class="form-control "
																		name="last_name" value="<?php echo $last_name?>"> <span
																		id="last_name_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>

																</div>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo MOBILE_NUMBER; ?></label>
																<div class="col-md-6">
																	<input type="text" act="yes" class="form-control  no-spinners"
																		name="mobile_number" id="mobile_number"
																		value="<?php echo $mobile_number?>"
																		onfocus="javascript:removeTooltip('mobile_number_tooltip');">
																	<span id="mobile_number_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo MOBILE_NUMBER_INVALID; ?></span>

																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo EMAIL; ?><span
																	class="required"> * </span></label>
																<div class="col-md-6">
																	<input type="text" class="form-control no-validate"
																		name="email" id="email" value="<?php echo $email?>"
																		id="office_email"
																		onfocus="javascript:removeTooltip('email_tooltip');">
																	<span id="email_tooltip" class="tooltiptext"
																		<?php if (isset($email_exists_error) && $email_exists_error == 1){?>
																		style="display: block; color: red;" <?php }else {?>
																		style="display: none; color:red;" <?php }?>><?php
																		
																		if (isset ( $email_exists_error ) && $email_exists_error == 1) {
																			echo EMAIL_IS_EXIST;
																		} else {
																			echo INVALID_EMAIL;
																		}
																		?></span>
																</div>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"> <?php echo USERNAME; ?> <span
																	class="required"> * </span></label>

																<div class="col-md-6">
																	<input type="text" class="form-control no-validate"
																		name="user_name" value="<?php echo $user_name?>"
																		id="user_name"
																		onfocus="javascript:removeTooltip('user_name_tooltip');">
																	<span id="user_name_tooltip" class="tooltiptext"
																		<?php if (isset($username_exists_error) && $username_exists_error == 1){?>
																		style="display: block; color: red;" <?php }else {?>
																		style="display: none; color:red;" <?php }?>><?php
																		
																		if (isset ( $username_exists_error ) && $username_exists_error == 1) {
																			echo USERNAME_IS_EXIST;
																		} else {
																			echo FILL_THIS_FIELD;
																		}
																		?></span>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo PASSWORD; ?> <span
																	class="required"> * </span></label>
																<div class="col-md-6">
																	<input type="password" class="form-control no-validate"
																		name="password" value="<?php echo $password?>"
																		id="password"
																		onfocus="javascript:removeTooltip('password_tooltip');">
																	<span id="password_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo REPEAT_PASSWORD; ?> <span
																	class="required"> * </span></label>
																<div class="col-md-6">
																	<input type="password" class="form-control no-validate"
																		value="<?php echo $password?>" id="retyped_password"
																		onfocus="javascript:removeTooltip('retyped_password_tooltip');">
																	<span id="retyped_password_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo INCORRECT_REPEATED_PASSWORD; ?></span>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo CUSTOMER_TYPE; ?></label>
																<div class="col-md-6">
																	<select class="form-control " name="customer_type"
																		id="customer_type" class="form-control">
					  	<?php
								if ($available_customer_types) {
									for($i = 0; $i < count ( $available_customer_types ); $i ++) {
										$customer_type_id = $available_customer_types [$i] ["customer_type_id"];
										$customer_type_name = $available_customer_types [$i] ["name"];
										?>
							<option value="<?=$customer_type_id?>"
																			<?php if ($customer_type == $customer_type_id){?>
																			selected="selected" <?php }?>> <?=$customer_type_name?></option>
						<?php
									}
								} else {
									?>
							<option value="" selected></option>
						<?php
								}
								?>		
					</select>
																</div>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo REGION; ?></label>
																<div class="col-md-6">
																	<select class="form-control " name="region" id="region"
																		class="form-control">
					  	<?php
								if ($available_regions) {
									for($i = 0; $i < count ( $available_regions ); $i ++) {
										$region_id = $available_regions [$i] ["region_id"];
										$region_name = $available_regions [$i] ["name"];
										?>
							<option value="<?=$region_id?>" <?php if ($region == $region_id){?>
																			selected="selected" <?php }?>> <?=$region_name?></option>
						<?php
									}
								} else {
									?>
							<option value="" selected></option>
						<?php
								}
								?>		
					</select>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo NUM_OF_CASES_A_MONTH; ?><span
																	class="required"> * </span></label>
																<div class="col-md-6">
																	<input type="text" act="yes" class="form-control  no-spinners"
																		name="number_of_cases_per_month"
																		value="<?php echo $number_of_cases_per_month?>"
																		onfocus="javascript:removeTooltip('number_of_cases_per_month_tooltip');">
																	<span id="number_of_cases_per_month_tooltip"
																		class="tooltiptext" style="display: none; color: red;"><?php echo NUMBER_INVALID; ?></span>
																</div>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo PHONE_NUMBER; ?></label>
																<div class="col-md-6">
																	<input type="text" act="yes" class="form-control  no-spinners"
																		name="phone_number" value="<?php echo $phone_number?>" act="yes"
																		onfocus="javascript:removeTooltip('phone_number_tooltip');">
																	<span id="phone_number_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo PHONE_NUMBER_INVALID; ?></span>
																</div>
															</div>
														</div>

														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo CONTACT_PERSON_NAME; ?></label>
																<div class="col-md-6">
																	<input type="text" class="form-control "
																		name="contact_person_name"
																		value="<?php echo $contact_person_name?>">

																</div>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo SUBSCRIBTION_START_DATE; ?></label>
																<div class="col-md-6">
																	<input type="text" placeholder=""
																		class="form-control date"
																		name="subscription_start_date"
																		value="<?php echo $subscription_start_date?>"> <span
																		id="start_date_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo INVALID_DATES; ?></span>
																	<span id="start_date_tooltip2" class="tooltiptext"
																		style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group last">
																<label class="col-md-4 control-label"><?php echo SUBSCRIBTION_END_DATE; ?></label>
																<div class="col-md-6">
																	<input type="text" class="form-control date"
																		name="subscription_end_date"
																		value="<?php echo $subscription_end_date?>"> <span
																		id="end_date_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo INVALID_DATES; ?></span>
																	<span id="end_date_tooltip2" class="tooltiptext"
																		style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>
																</div>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo POSITION; ?></label>
																<div class="col-md-6">
																	<input type="text" class="form-control no-validate"
																		name="position" value="<?php echo $position?>">
																</div>
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-2 control-label"><?php echo ADDRESS; ?></label>
														<div class="col-md-8">
															<textarea class="form-control no-validate" rows="4" cols="50"
																name="address" form="myForm"><?php echo $address?></textarea>

														</div>
													</div>
														
													<?php
													if ($ordered_stages) {
														for($i = 0; $i < count ( $ordered_stages ); $i ++) {
															$stage_code_s = $ordered_stages [$i] ["stage_code"];
															$stage_title = $ordered_stages [$i] ["stage_title"];
															$stage_order = $ordered_stages [$i] ["stage_order"];
															?>
													<div class="form-group"
														id="costs_for_stage<?=$stage_order?>">
														<label class="col-md-3 control-label"><?php
															
															if ($stage_order == 1) {
																echo STAGE1;
															} else if ($stage_order == 2) {
																echo STAGE2;
															} else if ($stage_order == 3) {
																echo STAGE3;
															} else if ($stage_order == 4) {
																echo STAGE4;
															} else if ($stage_order == 5) {
																echo STAGE5;
															}
															?> (<?=$stage_title?>):</label> <input type="hidden"
															name="stage_code<?=$stage_order?>"
															value="<?=$stage_code_s?>" /> <input type="hidden"
															id="stage_data_count_<?=$stage_order?>" value="1" />
														<div class="form-group">
															<label class="col-md-1 control-label"><?php echo REGION; ?> <span
																class="required"> * </span></label>
															<div class="col-md-3">
																<select class="form-control "
																	name="stage_region<?=$stage_order?>[]"
																	id="stage_region<?=$stage_order?>" class="form-control">
																  	<?php
															if ($available_regions) {
																foreach ( $available_regions as $region ) {
																	$region_id_s = $region ["region_id"];
																	$name = $region ["name"];
																	?>
																		<option value="<?=$region_id_s?>"
																		<?php if ($stages_data != "" && $stages_data[$i][0]['region_id'] == $region_id_s){?>
																		selected="selected" <?php }?>> <?=$name?></option>
																	<?php
																}
															} else {
																?>
																		<option value="" selected></option>
																	<?php
															}
															?>		
																</select onfocus="javascript:removeTooltip('stage_region<?=$stage_order?>_tooltip');">
																<span id="stage_region<?=$stage_order?>_tooltip"
																	class="tooltiptext" style="display: none; color: red;"><?php echo FILL_THIS_FIELD; ?></span>
															</div>

															<label class="col-md-1 control-label"><?php echo STAGE_COST; ?> <span
																class="required"> * </span></label>
															<div class="col-md-3">
																<input type="text" act="yes" class="form-control  no-spinners"
																	name="stage_cost<?=$stage_order?>[]"
																	id="stage_cost<?=$stage_order?>"
																	<?php if ($stages_data != ""){?>
																	value="<?=$stages_data[$i][0]['cost']?>"
																	<?php } else { ?> value="" <?php }?>
																	onfocus="javascript:removeTooltip('stage_cost<?=$stage_order?>_tooltip');">
																<span id="stage_cost<?=$stage_order?>_tooltip"
																	class="tooltiptext" style="display: none; color: red;"><?php echo NUMBER_INVALID; ?></span>
															</div>
															<button type="button"
																class="btn btn-outline btn-circle dark btn-sm black add-delete-button"
																onclick="javascript:addInStage('','', '<?=$stage_order?>')"><?php echo INSERT; ?> </button>
														</div>

													</div>
													<?php
															if ($stages_data != "" && $stages_data [$i] != null && count ( $stages_data [$i] ) > 1) {
																for($k = 1; $k < count ( $stages_data [$i] ); $k ++) {
																	$data_region_id = $stages_data [$i] [$k] ["region_id"];
																	$data_cost = $stages_data [$i] [$k] ["cost"];
																	?>
													<script>addInStage('<?=$data_region_id?>','<?=$data_cost?>','<?=$stage_order?>');</script>
													<?php }	}?>
													<?php }	}?>

													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo SHOW_INVOICE_MODULE; ?><span
															class="required"> * </span></label>
														<div class="col-md-5">
															<table style="width: 60%"
																onfocus="javascript:removeTooltip('show_invoice_module_tooltip');">
																<tr>
																	<td><input type="radio" name="show_invoice_module"
																		id="show_invoice_module" value="Y"
																		<?php if($show_invoice_module == 'Y'){?> checked
																		<?php }?>></td>
																	<td><?php echo YES; ?></td>
																	<td><input type="radio" name="show_invoice_module"
																		id="show_invoice_module" value="N"
																		<?php if($show_invoice_module == 'N'){?> checked
																		<?php }?>></td>
																	<td><?php echo NO; ?></td>
																</tr>
															</table>
															<span id="show_invoice_module_tooltip"
																class="tooltiptext" style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
														</div>
													</div>

													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo SHOW_DASHBOARD; ?><span
															class="required"> * </span></label>
														<div class="col-md-5">
															<table style="width: 60%"
																onfocus="javascript:removeTooltip('show_dashboard_tooltip');">
																<tr>
																	<td><input type="radio" name="show_dashboard"
																		id="show_dashboard" value="Y"
																		<?php if($show_dashboard == 'Y'){?> checked <?php }?>></td>
																	<td><?php echo YES; ?></td>
																	<td><input type="radio" name="show_dashboard"
																		id="show_dashboard" value="N"
																		<?php if($show_dashboard == 'N'){?> checked <?php }?>></td>
																	<td><?php echo NO; ?></td>
																</tr>
															</table>

															<span id="show_dashboard_tooltip" class="tooltiptext"
																style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
														</div>
													</div>

												</div>
												<div class="form-actions">
													<div class="row">
														<div class="col-md-offset-3 col-md-9">
															<button type="submit" class="btn btn-circle green"><?php
															if ($user_id) {
																echo EDIT;
															} else {
																echo INSERT;
															}
															?></button>
															<a
																href="<?=site_url('lawyer_office_panel/view_customers')?>"
																class="btn btn-outline btn-circle grey-salsa"><?php echo CANCEL; ?>
															</a>
														</div>
													</div>
												</div>

											</form>
											<!-- END FORM-->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
        <?php $this->load->view('utils/footer');?>
	<!-- BEGIN CORE PLUGINS -->
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="<?=base_url()?>assets/global/plugins/moment.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/morris/morris.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/morris/raphael-min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/counterup/jquery.waypoints.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/counterup/jquery.counterup.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/fullcalendar/fullcalendar.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.resize.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.categories.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.sparkline.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="<?=base_url()?>assets/global/scripts/app.min.js"
		type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?=base_url()?>assets/pages/scripts/dashboard.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script
		src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js"
		type="text/javascript"></script>
	<!-- END THEME LAYOUT SCRIPTS -->

</body>

</html>
