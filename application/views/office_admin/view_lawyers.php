<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<?php
session_start ();
$this->load->view('localization/lang');
$this->load->view('localization/txt');
defineLocale ();
defineStrings ();

function encrypt_password($password){
$encrypted = "";
for($i = 0; $i < strlen ($password); $i ++) {
	$encrypted = $encrypted ."*";
}
	echo $encrypted;
}


function set_code ($code){
		$GLOBALS['code'] = $code;
}
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title><?php echo TITLE; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin RTL Theme #3 for dashboard & statistics"
	name="description" />
<meta content="" name="author" />

<link rel="shortcut icon"
	href="<?=base_url()?>assets/layouts/layout3/img/logo-default.jpg" />

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

</head>
<!-- END HEAD -->

<body class="page-container-bg-solid">
<?php include 'office_admin_header.php';?>
<div class="page-wrapper">
  <div class="container"> 
		
	<?php $this->load->view('utils/delete_alert');?>
	<div class="form-group" <?php if (isset($delete_error) && $delete_error != ''){?>
								style="display: block; color:red;"
								<?php }else {?>
								style="display: none; color:red;"
								<?php }?>>
		<?php if (isset($delete_error) && $delete_error == 'manager_exist'){echo DELETE_ERROR_LAWYER;}
			else if (isset($delete_error) && $delete_error == 'case_exist'){echo DELETE_ERROR_LAWYER_CASE;}
			else if (isset($delete_error) && $delete_error == 'manager_case_exist'){echo DELETE_ERROR_LAWYER_CASE_MANAGER;}
			else if (isset($delete_error) && $delete_error == 'max_lawyer_count_reached'){echo MAX_LAWYER_COUNT_REACHED;}?>
			
	</div>
	<!-- BEGIN BORDERED TABLE PORTLET-->
	<div class="page-content-inner">
        <div class="row">
            <div class="col-md-13">
                <div class="portlet light portlet-fit ">
					<div class="portlet-title">
						<div class="caption">
							<span class="caption-subject font-dark bold uppercase"><?php echo LAWYERS; ?></span>
						</div>
						<div class="actions">
							<div class="btn-group">
								<a class="btn dark btn-outline btn-circle btn-sm"
									href="<?=site_url('lawyer_office_lawyers/add_edit_lawyer/')?>" data-hover="dropdown"
									data-close-others="true"> <?php echo INSERT; ?>
										</a>

							</div>
						</div>
					</div>
				</div>
				<div class="table-scrollable">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th><?php echo NAME; ?></th>
							<th><?php echo FAMILY_NAME; ?></th>
							<th><?php echo USERNAME; ?></th>
							<th><?php echo MOBILE_NUMBER; ?></th>							
							<th><?php echo EMAIL; ?></th>
							<th><?php echo OPENED_CASES_COUNT; ?></th>
							<th><?php echo CLOSED_CASES_COUNT; ?></th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
							<?php
							if ($lawyers) {
								for($i = 0; $i < count ( $lawyers); $i ++) {?>
									<tr>
							<td><?=$lawyers[$i] ["first_name"];?> </td>
							<td><?=$lawyers[$i] ["last_name"];?> </td>
							<td><?=$lawyers[$i] ["user_name"];?> </td>
							<td><?=$lawyers[$i] ["mobile_number"];?> </td>
							<td><?=$lawyers[$i] ["email"];?> </td>
							<td><?=$lawyers[$i] ["opened_cases_count"];?> </td>
							<td><?=$lawyers[$i] ["closed_cases_count"];?> </td>
							<td><a
											href="<?=site_url('lawyer_office_lawyers/add_edit_lawyer/'.$lawyers[$i] ["user_id"])?>"
											class="btn btn-outline btn-circle btn-sm purple"> <i
												class="fa fa-edit"></i> <?php echo MORE; ?>
										</a></td>
							<td><table>
									<tr>
										<td><a
											href="<?=site_url('lawyer_office_lawyers/add_edit_lawyer/'.$lawyers[$i] ["user_id"])?>"
											class="btn btn-outline btn-circle btn-sm purple"> <i
												class="fa fa-edit"></i> <?php echo EDIT; ?>
										</a></td>
										<td>
										<a id="delBtn" 
											href="javascript:delBtnClick('<?=$lawyers[$i] ["user_id"]?>', 'lawyers');"
											class="btn btn-outline btn-circle dark btn-sm black"> <i
												class="fa fa-trash-o"></i> <?php echo DELETE; ?>
										</a>
										</td>
									</tr>
								</table></td>
						</tr>	
						<?php
								}
							} else {
								?><tr class="HeadBr2">
							<td colspan="22" style="color: red" align="center"><h4>
							<?php echo NO_RECORDS_FOUND; ?>
							</h4></td>
						</tr> <?php }?>
						</tbody>
				</table>
			</div>
			<!-- END BORDERED TABLE PORTLET-->
			</div>
		</div>
	</div>
  </div>
</div>
								
							
<?php $this->load->view('utils/footer');?>

<!-- BEGIN CORE PLUGINS -->
        <script src="<?=base_url()?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?=base_url()?>assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?=base_url()?>assets/pages/scripts/profile.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
        
</body>

</html>