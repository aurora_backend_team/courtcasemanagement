<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title><?php echo TITLE; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin RTL Theme #3 for dashboard & statistics"
	name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css"
	rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/global/plugins/morris/morris.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/fullcalendar/fullcalendar.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/jqvmap.css"
	rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"
	rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/global/css/components-rtl.min.css"
	rel="stylesheet" id="style_components" type="text/css" />
<link href="<?=base_url()?>assets/global/css/plugins-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->

<!-- PROFILE STYLE -->
<link href="<?=base_url()?>assets/pages/css/profile-rtl.min.css" rel="stylesheet"
	type="text/css" />

<!-- BEGIN THEME LAYOUT STYLES -->
<link
	href="<?=base_url()?>assets/layouts/layout3/css/layout-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/layouts/layout3/css/themes/default-rtl.min.css"
	rel="stylesheet" type="text/css" id="style_color" />
<link
	href="<?=base_url()?>assets/layouts/layout3/css/custom-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap/css/ccm_css.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon"
	href="<?=base_url()?>images/logohighquality.png" />
<link rel="manifest" href="https://altanfeeth.com/manifest.json">
<script>
	function popup(url) {
		  w = 1040;
		  
		 h = 500;
		  var left = (screen.width/2)-(w/2);
		  var top = (screen.height/2)-(h/2);
	  	window.open(url,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width='+w+', height='+h+', top='+top+', left='+left+',directories=no,location=no') 
		  
	  }

	  
</script>
<script>


</script>
<link rel="stylesheet" type="text/css"
	href="<?=base_url()?>assets/css/jquery.calendars.picker.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?=base_url()?>assets/js/jquery.plugin.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery.calendars.all.js"></script>
<script src="<?=base_url()?>assets/js/jquery.calendars.plus.js"></script>
<script src="<?=base_url()?>assets/js/jquery.calendars.ummalqura.js"></script>
<script src="<?=base_url()?>assets/js/jquery.calendars.ummalqura-ar.js"></script>
<script>
/*  $( function() {
    $( ".date" ).calendarsPicker({calendar: $.calendars.instance('ummalqura','ar')});
  } );*/
  </script>

<script>
var violated = false;
function numberKeyDown(e) {
	var keynum;
    if(window.event) { // IE                    
      keynum = e.keyCode;
    } else if(e.which){ // Netscape/Firefox/Opera                   
      keynum = e.which;
    }
    //alert (keynum);
 // Allow: backspace, delete, tab, escape, enter, up and down.
    if ($.inArray(keynum, [46, 8, 9, 27, 13,0,39,38,40]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (keynum === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
        // Allow: Ctrl+C, Command+C
        (keynum === 67 && (e.ctrlKey === true || e.metaKey === true)) || 
        // Allow: Ctrl+X, Command+X
        (keynum === 88 && (e.ctrlKey === true || e.metaKey === true)) || 
     // Allow: Ctrl+V, Command+V
        (keynum === 86 && (e.ctrlKey === true || e.metaKey === true)) ||
    	 // Allow: Ctrl+Z, Command+Z
        (keynum === 90 && (e.ctrlKey === true || e.metaKey === true)) || 
         // Allow: home, end, left, right, down, up
        (keynum >= 35 && keynum <= 37)) {
             // let it happen, don't do anything
             return;
    }
    
    if(((keynum < 48 || keynum > 57) && (keynum < 96 || keynum > 105)) || (keynum >= 38 && keynum <= 40)){
		 e.preventDefault();
	}
}
function textKeyDown(e,elem) {
	var keynum;
    if(window.event) { // IE                    
      keynum = e.keyCode;
    } else if(e.which){ // Netscape/Firefox/Opera                   
      keynum = e.which;
    }
//	alert (keynum);
 // Allow: backspace, delete, tab, escape, enter, up and down.
    if ($.inArray(keynum, [46, 8, 9, 27, 13, 110, 190,39,38,40]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (keynum === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
        // Allow: Ctrl+C, Command+C
        (keynum === 67 && (e.ctrlKey === true || e.metaKey === true)) || 
        // Allow: Ctrl+X, Command+X
        (keynum === 88 && (e.ctrlKey === true || e.metaKey === true)) || 
         // Allow: home, end, left, right, down, up
        (keynum >= 35 && keynum <= 37)) {
             // let it happen, don't do anything
             return;
    }
	
    if(((keynum >= 48 && keynum <= 57) || (keynum >= 96 && keynum <= 105)) || (keynum >= 38 && keynum <= 40)){
		 e.preventDefault();
	}
}

function dateValidation(dateVar){
	//Format: yyyy/mm/dd
	var dateReg = /^\d{4}\/\d{2}\/\d{2}$/;
	var isV = dateReg.test(dateVar);
	if(!isV){
		return false;
	} 
	return true;
}
function parseArabic(str) {
    return ( str.replace(/[٠١٢٣٤٥٦٧٨٩]/g, function(d) {
        return d.charCodeAt(0) - 1632;
    }).replace(/[۰۱۲۳۴۵۶۷۸۹]/g, function(d) {
        return d.charCodeAt(0) - 1776;
    }) );
}

function textNumViolation(){
var violate = false;
	$('*[id*=valid_span]:visible').each(function() {
	    //     alert($(this).text());
		 
		 violate = true;
	});
	return violate ;
}

function replacAllArabToEng(){
	$(':input').each(function(){
	         var attr = $(this).attr('act');
		if (typeof attr !== typeof undefined){
			$(this).val(parseArabic($(this).val()));
			//alert($(this).val());
		 }
	});
}

function handlingTextNumberViolation (item){
	var attr = item.attr('act');
    			 
    			var val = parseArabic(item.val());
    			//alert(val);
				var matches = val.match(/\d+/g);
				
				if (item.attr("class").indexOf("date")>=0 && val!="" &&!dateValidation(val))
					$('#valid_span_'+item.attr('name')).css("display","block");
				else if ((item.attr("class").indexOf("date")<0 && (matches != null && typeof attr == typeof undefined))||(typeof attr !== typeof undefined && !$.isNumeric(val) && val!="")) {
					$('#valid_span_'+item.attr('name')).css("display","block");
					
				}else /*if(!violated || (violated && val!="")||item.attr("type") == "text")*/{
					$('#valid_span_'+item.attr('name')).css("display","none");
					/*if(item.attr("type") == "number"){
						violated  = false;
					}*/
				}
}
 $(document).ready(function(){
 	<?php if ($this->session->flashdata('confirmation_message')) { ?>
 	bootbox.dialog({
		message: '<p>'+"<?= $this->session->flashdata('confirmation_message');?>"+' </p>',
		buttons: {
			confirm: {
				label: "<?= HIDE?>",
				className: 'green'
			}
		}
	});
<?php }?>
 	$(":input[type=text]").on('input', function() {
   		handlingTextNumberViolation($(this)); 
	});
	 $(':input[type=text]').each(function(){
	         var attr = $(this).attr('act');
		 if($(this).attr("class") && $(this).attr("class").indexOf(" date")>=0)
			 $(this).after('<span id="valid_span_'+$(this).attr('name')+'" style="display:none;color:red;"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>');
		 else if (typeof attr == typeof undefined && $(this).attr("class") && $(this).attr("class").indexOf("no-validate")<0 )
		 	$(this).after('<span id="valid_span_'+$(this).attr('name')+'" style="display:none;color:red;"><?php echo NO_NUMBERS; ?></span>');
		 else if (typeof attr !== typeof undefined){
			// $(this).attr("oninvalid", "setCustomValidity('<?php echo NO_TEXT; ?>')");
			// $(this).attr("onchange", "setCustomValidity('')");
			 $(this).after('<span id="valid_span_'+$(this).attr('name')+'" style="display:none;color:red;"><?php echo NO_TEXT; ?></span>');
		 }
		/* $(this).bind("paste",function(e) {
  			//  var pastedData = e.originalEvent.clipboardData.getData('text');
			 handlingTextNumberViolation($(this));
    			
   			});*/
	});
	 $(':input[type=text]').on('keydown', function(e) {
	 var attr = $(this).attr('act');
	 
		 if (typeof attr == typeof undefined && $(this).attr("class").indexOf("no-validate")<0 && $(this).attr("class").indexOf("date")<0)
	    	textKeyDown(e,$(this));
		 else if (typeof attr !== typeof undefined){
			 numberKeyDown(e,$(this));
			


		}
	    	
	    });    
	    $('.menu-dropdown , .mt-step-col , .btn-outline  , #no-popup').click(function() {
        	$('#loading').removeClass('hidden');
   	 });
   	 $('#add_drop-menu , #load_drop-menu , .view-only , #case_detail').click(function() {
   	 	$('#loading').addClass('hidden');
   	 });
	    
   // Disable scroll when focused on a number input.
    $('form').on('focus', 'input[type=number]', function(e) {
	$(this).on('wheel', function(e) {
		e.preventDefault();
	});
    });

    // Restore scroll on number inputs.
    $('form').on('blur', 'input[type=number]', function(e) {
    	$(this).off('wheel');
    });

    // Disable up and down keys.
    $('form').on('keydown', 'input[type=number]', function(e) {
	if ( e.which == 38 || e.which == 40 )
		e.preventDefault();
    }); 

    //disable mousewheel on a input number field when in focus
    //(to prevent Cromium browsers change the value when scrolling)
    $('form').on('focus', 'input[type=number]', function (e) {
	$(this).on('mousewheel.disableScroll', function (e) {
		e.preventDefault();
	});
    });
    $('form').on('blur', 'input[type=number]', function (e) {
    	$(this).off('mousewheel.disableScroll');
    });	 
	
});
</script>

<!--  
<script src="https://www.gstatic.com/firebasejs/3.7.0/firebase.js"></script>
<script src="https://www.gstatic.com/firebasejs/3.6.10/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/3.6.10/firebase-auth.js"></script>
<script
	src="https://www.gstatic.com/firebasejs/3.6.10/firebase-database.js"></script>
<script
	src="https://www.gstatic.com/firebasejs/3.6.10/firebase-messaging.js"></script>
-->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
var update = 0;
var pushNotification = false;
var pushNotificationId = "";
var count = 0;
var title = document.title;
// update the tab title the returned count_unseen after login 
<?php if ( $count_unseen > 0 ){?>
	var newTitle = '(' + <?= $count_unseen ?> + ') ' + title;
	document.title = newTitle;
<?php } ?>
// function called on interval every 20 sec
function updateNotifications() {
	// make ajax call to get latest count of unseen notifications and list of them
	$.ajax({  
 		type    : "POST",  
 		url     : "<?=site_url('seen_notifications/get_unseen_notifications' )?>",
 		dataType:"json",
 		success : function(data){
 			console.log(data);
			// count returned from ajax call
 			var count_unseen = data["count_unseen"];
			// current displayed count in red badge
 			var count = $("#notify_count").text();
			//intialize the count in case of there is not displayed count in screen
 			if(count == "") {
   				count = "0";
   			}
			
			// if returned count from the ajax call is more than 0 update tab title and red badge
 			if(count_unseen > 0 ){
 				var newTitle = '(' + count_unseen + ') ' + title;
	    		document.title = newTitle;
 			} else {
				 document.title = title;
			}
		
	   		if ( count_unseen == 0){
	   			$("#notify_count").html("");
	   		}else{
	   			$("#bell").html('<i class="icon-bell" onclick="resetCounter()"></i><span onclick="resetCounter()" id="notify_count" class="badge badge-default">'+count_unseen+ '</span>');
	   		}
 		
	        // the unseen notifications list returned from ajax call
 			var notifications_list = data["notifications_list"];
 			//alert(count_unseen);
 			//alert(count);
 			if ( count_unseen > count){
				// displaying just the notifications which are not displayed in the list 
				// decending to the the newest in the top of the list using "prepend" function
 				for (i = count_unseen-count-1; i >= 0; i--){
 	 				console.log(notifications_list[i]["case_id"]);
	 	   			// 	add new item in the list of id "notifications"
 		   			var listContainer = $('#notifications');
					// link of the href to the related case
					var caseLink = "<?=site_url('case_management/view_case_details')?>"+"/"+notifications_list[i]["case_id"];
 	   				listContainer.prepend('<li><a href="'+caseLink +'"><span class="time">'+notifications_list[i]["sent_date_time"]+'</span><span class="details"> '+notifications_list[i]["message"]+'</span></a></li>');
 				}
				// this two var to check if there are new unseen notifications then send the last id
				// to the sendLastSeenNotificationId to set all notifications till this id as "seen"
 				pushNotification = true;
	   			pushNotificationId = notifications_list[0]["email_id"];
 			}
 		}
	});
}
// starting interval
function newUpdate() {
    update = setInterval(updateNotifications, 20000);
}

newUpdate();
  // this code on clicking on notify bell
  function resetCounter(){
	  console.log("reset");
	  // stoping the interval while updating last seen notification to avoid conflicts
	  clearInterval(update);
	  //reset tab title
	  document.title = title;
	  $("#notify_count").html("");
	  
	 // send the last item id to check last seen item
	 // check the flag of the sent notifications
	 if(pushNotification){
		 sendLastSeenNotificationId (pushNotificationId);
		 pushNotification = false;
		 pushNotificationId = "";
	 }else{
	 <?php  if($notifications_list){?>
	 sendLastSeenNotificationId ('<?= $notifications_list[0]["email_id"]?>');
	 <?php } else {?>
	 // in case of there is no notifications for the current user return the interval
	 update = setInterval(updateNotifications, 20000);
	 <?php }?>
	 }
	   	return;
 }
 // this function to set all notifications till this email_id as "seen"
 function sendLastSeenNotificationId (email_id){
	 
	   	$.ajax({  
 		type    : "POST",  
 		url     : "<?=site_url('seen_notifications/update_seen_notifications' )?>",  
 		data    : {
     	last_id: email_id,
     	page: '<?= $page ?>'
 	},
 	complete : function(data){
		// return the interval after calling LastSeenNotificationId
 		update = setInterval(updateNotifications, 20000);
     	console.log(data);
 	}
	});
		
 }
 
 function _(el){
	    return document.getElementById(el);
	}
var _validFileExtensions = ["jpg", "jpeg", "bmp", "gif", "png","webp","svg","ai","eps","pdf"];    
function validateFileExt(sFileName) {
        if (sFileName.length > 0) {
            var blnValid = false;
            for (var j = 0; j < _validFileExtensions.length; j++) {
                var sCurExtension = _validFileExtensions[j];
				sCurExtension = "."+sCurExtension
                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    blnValid = true;
                    break;
                }
            }
                
            if (!blnValid) {
				bootbox.dialog({ 
					 message: '<p><i class="fa fa-spin fa-spinner"></i> <?= FILE_NOT_IMG_PDF ?> </p>' ,
						buttons: {
							confirm: {
								label: "<?= CLOSE?>",
								className: 'red'
							}
						}});
                return false;
            }
        }
  
    return true;
}
function uploadFile(name, statusName, progressBarName, field_name, case_id, fragment_div){
	var uploadingFiles_id = field_name + "_uploadingFiles";
	_(progressBarName).value = 0;
	var files_count = _(name).files.length;
	var check_undefined = '0';
	for (i = 0; i < files_count; i++) {
		if(typeof _(name).files[i] === "undefined"){
			check_undefined='1';
			break;
		}
	}
 if(check_undefined == '1' || files_count<=0) {
		_(statusName).innerHTML = "خطأ: برجاء إدخال الملف اولا";
		_(progressBarName).value = 0;
 } else {
		_(fragment_div).style.display = "block";
		_(progressBarName).style.display = "inline";
		_(statusName).style.display = "inline";
		//ajax 1
	    $.ajax({
	        url: '<?php echo site_url('case_management/upload_file_get_max'); ?>',
	        type: 'POST',
	        dataType: 'json',
	        success: function(response) {
				var sizelimit = response.max_size;
		     	var check_exceed_limit = '0';
				for (i = 0; i < files_count; i++) {
					if(sizelimit < _(name).files[i].size){
						check_exceed_limit='1';
						break;
					}
				}
	         if(check_exceed_limit == '0') {
	             var formdata = new FormData();
					for (i = 0; i < files_count; i++) {
						if(!validateFileExt(_(name).files[i].name)){ 
							deleteFile(field_name, (field_name+'_hidden'), progressBarName, statusName, fragment_div);
							_(uploadingFiles_id).value = "no_files";
							return;
						}
						formdata.append("file"+i, _(name).files[i]);
						//formdata.append("size"+i, _(name).files[i].size);
					}
					formdata.append("field_name", field_name);
					formdata.append("case_id", case_id);
	             var ajax = new XMLHttpRequest();

	             ajax.upload.addEventListener("progress", function(event){
							_(uploadingFiles_id).value = "";
						    //_(loadedName).innerHTML = "Uploaded "+event.loaded+" bytes of "+event.total;
							var percent = (event.loaded / event.total) * 100;
							
							if(percent != 100){
								console.log("percent: " + percent+" statusName:"+statusName);
								_(progressBarName).value = Math.round(percent);
								_(statusName).innerHTML = Math.round(percent)+"%";
							}
					}, false);
	             ajax.addEventListener("load", function(event){
						if(event.target.responseText !='0'){
							var hidden_field = field_name+"_hidden";
							_(hidden_field).value = event.target.responseText;
							console.log("hidden_field: " + hidden_field+" statusName:"+statusName);
							_(statusName).innerHTML = '100%';
							_(progressBarName).value = 100;
							_(uploadingFiles_id).value = "no_files";
						}else{
							_(statusName).innerHTML = "خطأ: هناك مشكلة في تحميل الملف";
						}
						
					}, false);
	             ajax.addEventListener("error", function(event){
						_(uploadingFiles_id).value = "no_files";
						_(statusName).innerHTML = "خطأ: هناك مشكلة في تحميل الملف";
					}, false);
	             ajax.addEventListener("abort", function(event){
						_(statusName).innerHTML = "Upload Aborted";
						_(uploadingFiles_id).value = "no_files";
					}, false);
	  	var path= "<?php echo base_url(); ?>" + "index.php/case_management/upload_file_progress";
	             ajax.open("POST", path);
	             ajax.send(formdata);
	             var close_id = "close_"+field_name;	             
	             var has_delete_listener = $("span[id="+close_id+"]").hasClass ("dummyClass");
	             if(has_delete_listener == false){
	             	_(close_id).addEventListener("click", function(){
					 
					bootbox.confirm({
						title: "حذف الملف",
						message:"هل انت متأكد من حذف الملف؟",
						buttons: {
							confirm: {
								label: "نعم",
								className: 'custom-green-button'
							},
							cancel: {
								label: "لا",
								className: 'custom-red-button'
							}
						},
						callback: function (result) {
							if(result){
								ajax.abort();
								deleteFile(field_name, (field_name+'_hidden'), progressBarName, statusName, fragment_div);
								_(uploadingFiles_id).value = "no_files";
							}
						}
					});
					
					$("span[id="+close_id+"]").addClass('dummyClass');
					
				   /* var r = confirm("هل انت متأكد من حذف الملف؟");
				    if (r == true) {
				        ajax.abort();
				        deleteFile(field_name, (field_name+'_hidden'), progressBarName, statusName, fragment_div);
						_(uploadingFiles_id).value = "no_files";
				    }*/
				});
			}
	         } else {
	             var sizewarn = "خطأ: الملف حجمه اكبر من المتاح للتحميل ";
	             sizewarn += sizelimit/(1024*1024);
	             sizewarn += " ميجا";
	             _(statusName).innerHTML = sizewarn;
	             _(progressBarName).value = 0;

	         }
	           
	        }
	    });
		
	}
}

function deleteFile(field_name, hidden_field_name, progressBarName, statusName, fragment_div){

	_(field_name).value = "";
	_(hidden_field_name).value = "";
	_(progressBarName).value = 0;
	_(progressBarName).style.display = "none";
	_(statusName).innerHTML = "";
	_(statusName).style.display = "none";
	_(fragment_div).style.display = "none";

}	

</script>
<style type="text/css">
.fragment {
    font-size: 12px;
    font-family: tahoma;
    height: 30px;
    width: 187px;
    border: 1px solid #ccc;
    color: #555;
    display: block;
    padding: 5px;
    box-sizing: border-box;
    text-decoration: none;
	background-color: rgba(85, 85, 85, 0.09) !important;
}

.custom-green-button{
	color: #FFF;
    background-color: #007c5a;
    border-color: #007c5a;
    float: right;
}

.custom-red-button{
	color: #fff;
    background-color: #ed6b75;
    border-color: #ea5460;
	margin-right: 1%;
}


.table{
     max-width:none !important;
}
.table-scrollable{
	-webkit-overflow-scrolling: touch;
}

</style>

</head>
<!-- END HEAD -->

</html>
