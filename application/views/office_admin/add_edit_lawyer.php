<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<?php
session_start ();
$this->load->view ( 'localization/lang' );
$this->load->view ( 'localization/txt' );
defineLocale ();
defineStrings ();
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<script type="text/javascript">
function validateEmail(email) 
{
	//var re = /\S+@\S+\.\S+/;
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}

function isValidDate(dateVar){
	//Format: yyyy/mm/dd
	var dateReg = /^\d{4}\/\d{2}\/\d{2}$/;
	var isV = dateReg.test(dateVar);
	if(!isV){
		return false;
	} 
	
	var str_array = dateVar.split('/');
	var hijriDate =  jqOld.calendars.instance('ummalqura','ar').newDate();
	try {
	   hijriDate = hijriDate.date(str_array[0], str_array[1], str_array[2]);
	}
	catch(err) {
	   return false;
	}
	return true;
}


function checkValid(email_valdation) {
	if (textNumViolation())
		return false;
	replacAllArabToEng();
        var package = document.forms["myForm"]["package"].value;
	var office_name = document.forms["myForm"]["office_name"].value;
	var address = document.forms["myForm"]["address"].value;
	var mobile_number = document.forms["myForm"]["mobile_number"].value;
	var office_email = document.forms["myForm"]["office_email"].value;
	var contact_person_name = document.forms["myForm"]["contact_person_name"].value;
	var mobile_number = document.forms["myForm"]["mobile_number"].value;
	var phone_number = document.forms["myForm"]["phone_number"].value;
	var user_name= null;
	if(document.forms["myForm"]["user_name"] != null){
		user_name = document.forms["myForm"]["user_name"].value;
	}
	var password =null;
	if(document.forms["myForm"]["password"] != null){
		password = document.forms["myForm"]["password"].value;
	}
	var retyped_password =null;
	if(document.forms["myForm"]["retyped_password"] != null){
		retyped_password = document.forms["myForm"]["retyped_password"].value;
	}
	var first_name =null;
	if(document.forms["myForm"]["first_name"] != null){
		first_name = document.forms["myForm"]["first_name"].value;
	}
	var last_name =null;
	if(document.forms["myForm"]["last_name"] != null){
		last_name = document.forms["myForm"]["last_name"].value;
	}
	var subscription_start_date = document.forms["myForm"]["subscription_start_date"].value;
	var subscription_end_date = document.forms["myForm"]["subscription_end_date"].value;
	var mobile_pattern = /05/g;

	if(subscription_start_date != ""){
		var isValidD = isValidDate(subscription_start_date);
	   	if(!isValidD){
			$("#start_date_tooltip2").css("display","block");
			$("#start_date_tooltip2").css("visibility","visible");
	   			
	   		return false;
	   	}else{
			$("#start_date_tooltip2").css("display","none");
			$("#start_date_tooltip2").css("visibility","none");
	   	}
	}

	if(subscription_end_date != ""){
		var isValidD = isValidDate(subscription_end_date);
	   	if(!isValidD){
			$("#end_date_tooltip2").css("display","block");
			$("#end_date_tooltip2").css("visibility","visible");

			$("#end_date_tooltip").css("display","none");
			$("#end_date_tooltip").css("visibility","none");
	   			
	   		return false;
	   	}else{
			$("#end_date_tooltip2").css("display","none");
			$("#end_date_tooltip2").css("visibility","none");
	   	}
	}
	
	if (subscription_end_date != "" && subscription_start_date != "" && subscription_end_date < subscription_start_date) {
		$("#end_date_tooltip").css("display","block");
		$("#end_date_tooltip").css("visibility","visible");
			
		return false;
	}else{
		$("#end_date_tooltip").css("display","none");
		$("#end_date_tooltip").css("visibility","none");
	}
	if (package == "") {
		$("#package_tooltip").css("display","block");
		$("#package_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#package_tooltip").css("display","none");
		$("#package_tooltip").css("visibility","none");
	}
	if (office_name == "") {
		$("#office_name_tooltip").css("display","block");
		$("#office_name_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#office_name_tooltip").css("display","none");
		$("#office_name_tooltip").css("visibility","none");
	}
	if (mobile_number == "" || (mobile_number.length < 10 || mobile_number.length > 10 ||
        !mobile_pattern.test(mobile_number))) {
		$("#mobile_number_tooltip").css("display","block");
		$("#mobile_number_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#mobile_number_tooltip").css("display","none");
		$("#mobile_number_tooltip").css("visibility","none");
	}
	if (phone_number == "" || (phone_number != "" && (phone_number.length < 10 || phone_number.length > 10))) {
		$("#phone_number_tooltip").css("display","block");
		$("#phone_number_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#phone_number_tooltip").css("display","none");
		$("#phone_number_tooltip").css("visibility","none");
	}
	if (office_email == "") {
		$("#email_tooltip").css("display","block");
		$("#email_tooltip").css("visibility","visible");
		return false;
	} else if (!validateEmail(office_email)) {
		$("#email_tooltip").text(email_valdation);
		$("#email_tooltip").css("display","block");
		$("#email_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#email_tooltip").css("display","none");
		$("#email_tooltip").css("visibility","none");
	}
	if (contact_person_name == "") {
		$("#contact_person_name_tooltip").css("display","block");
		$("#contact_person_name_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#contact_person_name_tooltip").css("display","none");
		$("#contact_person_name_tooltip").css("visibility","none");
	}
	if (user_name == "") {
		$("#office_user_name_tooltip").css("display","block");
		$("#office_user_name_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#office_user_name_tooltip").css("display","none");
		$("#office_user_name_tooltip").css("visibility","none");
	}
	if (password == "") {
		$("#office_password_tooltip").css("display","block");
		$("#office_password_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#office_password_tooltip").css("display","none");
		$("#office_password_tooltip").css("visibility","none");
	}
	if (retyped_password == "" || password != retyped_password) {
		$("#retyped_password_tooltip").css("display","block");
		$("#retyped_password_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#retyped_password_tooltip").css("display","none");
		$("#retyped_password_tooltip").css("visibility","none");
	}
	if (first_name == "") {
		$("#office_first_name_tooltip").css("display","block");
		$("#office_first_name_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#office_first_name_tooltip").css("display","none");
		$("#office_first_name_tooltip").css("visibility","none");
	}
	if (last_name == "") {
		$("#office_last_name_tooltip").css("display","block");
		$("#office_last_name_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#office_last_name_tooltip").css("display","none");
		$("#office_last_name_tooltip").css("visibility","none");
	}
	if (address == "") {
		$("#address_tooltip").css("display","block");
		$("#address_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#address_tooltip").css("display","none");
		$("#address_tooltip").css("visibility","none");
	}
	$('#loading').removeClass('hidden');
}


</script>
<meta charset="utf-8" />
<title><?php echo TITLE; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin RTL Theme #3 for dashboard & statistics"
	name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css"
	rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/global/plugins/morris/morris.css" rel="stylesheet"
	type="text/css" />
<link href="<?=base_url()?>assets/global/plugins/fullcalendar/fullcalendar.min.css"
	rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/jqvmap.css"
	rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?=base_url()?>assets/global/css/components-rtl.min.css"
	rel="stylesheet" id="style_components" type="text/css" />
<link href="<?=base_url()?>assets/global/css/plugins-rtl.min.css" rel="stylesheet"
	type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="<?=base_url()?>assets/layouts/layout3/css/layout-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/layouts/layout3/css/themes/default-rtl.min.css"
	rel="stylesheet" type="text/css" id="style_color" />
<link href="<?=base_url()?>assets/layouts/layout3/css/custom-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon"
	href="<?=base_url()?>assets/layouts/layout3/img/logo-default.jpg" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>

</head>
<!-- END HEAD -->
<body class="page-container-bg-solid">
	<div class="page-wrapper">
<?php include 'general_admin_header.php';?>

<?php $this->load->view('utils/date_scripts');?>

				<div class="page-wrapper-row full-height">
			<div class="page-wrapper-middle">
				<!-- BEGIN CONTAINER -->
				<div class="page-container">
					<!-- BEGIN CONTENT -->
					<div class="page-content-wrapper">
						<!-- BEGIN CONTENT BODY -->
						<!-- BEGIN PAGE HEAD-->
						<div class="page-head">
							<div class="container">
								<!-- BEGIN PAGE TOOLBAR -->
								<div class="col-md-12">
									<div class="portlet box green">
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-gift"></i>
												<?php
												if ($lawyer_office_id) {
													echo $name;
												} else {
													echo ADD_RECORD;
												}
												?>
											</div>
										</div>
										<div class="portlet-body form">
											<!-- BEGIN FORM-->
											<form id="myForm" role="form" method="post"
											onsubmit="javascript:return checkValid('<?php echo INVALID_EMAIL; ?>');"
												action="<?=site_url('admin_panel/add_edit_lawyer_office/'.$lawyer_office_id)?>"
												class="form-horizontal">
												<div class="form-body">
												<div class="row">
												<div class="col-md-6">
												<div class="form-group">
														<label class="col-md-3 control-label"><?php echo PACKAGE_CODE; ?> <span class="required"> * </span></label>
														<div class="col-md-6">

															<select class="form-control "
																name="package_code" id="package" class="form-control">
					  	<?php
								if ($available_packages) {
									for($i = 0; $i < count ( $available_packages ); $i ++) {
										$package_code_s = $available_packages [$i] ["package_code"];
										$package_name = $available_packages [$i] ["package_name"];
										?>
							<option value="<?=$package_code_s?>"
																	<?php if ($package_code == $package_code_s){?>
																	selected="selected" <?php }?>> <?=$package_name?></option>
						<?php
									}
								} else {
									?>
							<option value="" selected></option>
						<?php
								}
								?>		
					</select
																onfocus="javascript:removeTooltip('package_tooltip');"> <span
																id="package_tooltip" class="tooltiptext"
																style="display: none; color:red;"><?php echo FILL_THIS_FIELD; ?></span>
														</div>
													</div>
													</div>
													<div class="col-md-6">
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo NAME; ?> <span class="required"> * </span></label>
														<div class="col-md-6">
															<input type="text" class="form-control "
																name="name" id="office_name" value ="<?= $name ?>"
																onfocus="javascript:removeTooltip('office_name_tooltip');"> <span
																id="office_name_tooltip" class="tooltiptext"
																style="display: none; color:red;"><?php echo FILL_THIS_FIELD; ?></span>
														</div>
													</div>
													</div>
													</div>
													<div class="row">
												    <div class="col-md-6">
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo CITY; ?></label>
														<div class="col-md-6">
															<select class="form-control " name="city"
																id="city" class="form-control">
					  	<?php
								if ($available_cities) {
									for($i = 0; $i < count ( $available_cities ); $i ++) {
										$city_id = $available_cities [$i] ["city_id"];
										$city_name = $available_cities [$i] ["city_name"];
										?>
							<option value="<?=$city_id?>" <?php if ($city == $city_id){?>
																	selected="selected" <?php }?>> <?=$city_name?></option>
						<?php
									}
								} else {
									?>
							<option value="" selected></option>
						<?php
								}
								?>		
					</select>
														</div>
													</div>
													</div>
													<div class="col-md-6">
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo MOBILE_NUMBER; ?> <span class="required"> * </span></label>
														<div class="col-md-6">
															<input type="text" act = "yes" class="form-control no-spinners" 
																name="mobile_number" value="<?php echo $mobile_number?>"
																onfocus="javascript:removeTooltip('mobile_number_tooltip');">
															<span id="mobile_number_tooltip" class="tooltiptext"
																style="display: none; color: red"><?php echo MOBILE_NUMBER_INVALID; ?></span>

														</div>
													</div>
													</div>
													</div>
													<div class="row">
													<div class="col-md-6">
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo PHONE_NUMBER; ?> <span class="required"> * </span></label>
														<div class="col-md-6">
															<input type="text" act = "yes" class="form-control no-spinners" onkeydown="numberKeyDown(event)"
																name="phone_number" i="phone_number" value="<?php echo $phone_number?>"
																onfocus="javascript:removeTooltip('phone_number_tooltip');">
															<span id="phone_number_tooltip" class="tooltiptext"
																style="display: none; color: red"><?php echo PHONE_NUMBER_INVALID; ?></span>
														</div>
													</div>
													</div>
													<div class="col-md-6">
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo EMAIL; ?> <span class="required"> * </span></label>
														<div class="col-md-6">
															<input type="text" class="form-control no-validate"
																name="email" value="<?php echo $email?>"
																id="office_email"
																onfocus="javascript:removeTooltip('email_tooltip');"> <span
																id="email_tooltip" class="tooltiptext"
																<?php if (isset($email_exists_error) && $email_exists_error == 1){?>
																style="display: block; color:red;"
																<?php }else {?>
																style="display: none; color:red;"
																<?php }?>
																
																><?php if (isset($email_exists_error) && $email_exists_error == 1){
																		echo EMAIL_IS_EXIST;
																 } else {
																	 echo INVALID_EMAIL;
																 }?>
																 </span>
														</div>
													</div>
													</div>
													</div>
													<div class="row">
													<div class="col-md-6">
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo CONTACT_PERSON_NAME; ?><span class="required"> * </span></label>
														<div class="col-md-6">
															<input type="text" class="form-control "
																name="contact_person_name" id="contact_person_name"
																value="<?php echo $contact_person_name?>"
																onfocus="javascript:removeTooltip('contact_person_name_tooltip');">
															<span id="contact_person_name_tooltip"
																class="tooltiptext" style="display: none; color:red"><?php echo FILL_THIS_FIELD; ?></span>
														</div>
													</div>
													</div>
													<div class="col-md-6">
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo POSITION; ?></label>
														<div class="col-md-6">
															<input type="text" class="form-control no-validate"
																name="position" value="<?php echo $position?>">
														</div>
													</div>
													</div>
													</div>
													<div class="row">
													<div class="col-md-6">
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo SUBSCRIBTION_START_DATE; ?></label>
														<div class="col-md-6">
															<input type="text" class="form-control date"
																name="subscription_start_date"
																value="<?php echo $subscription_start_date?>">
																<span id="start_date_tooltip" class="tooltiptext"
																	style="display: none; color: red"><?php echo INVALID_DATES; ?></span>
																<span id="start_date_tooltip2" class="tooltiptext"
																	style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>
														</div>
													</div>
													</div>
													<div class="col-md-6">
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo SUBSCRIBTION_END_DATE; ?></label>
														<div class="col-md-6">
															<input type="text" class="form-control date"
																name="subscription_end_date"
																value="<?php echo $subscription_end_date?>">
																<span id="end_date_tooltip" class="tooltiptext"
																	style="display: none; color: red"><?php echo INVALID_DATES; ?></span>
																<span id="end_date_tooltip2" class="tooltiptext"
																	style="display: none; color: red"><?php echo INVALID_DATE_VALUE_FORMAT; ?></span>
														</div>
													</div>
													</div>
													</div>
													
													<div class="row">
													<div class="col-md-6">
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo USERNAME; ?> <span class="required"> * </span></label>
														<div class="col-md-6">
															<input type="text" class="form-control "
																name="user_name" id="user_name" value ="<?php echo $user_name ?>"
																onfocus="javascript:removeTooltip('office_user_name_tooltip');"> <span
																id="office_user_name_tooltip" class="tooltiptext"
																style="display: none; color:red;"><?php echo FILL_THIS_FIELD; ?></span>
																
																<span
																id="user_name_error_tooltip" class="tooltiptext"
																<?php if (isset($user_name_exists) && $user_name_exists == 1){?>
																style="display: block; color:red;"
																<?php }else {?>
																style="display: none; color:red;"
																<?php }?>
																
																><?php if (isset($user_name_exists) && $user_name_exists == 1){
																		echo USERNAME_IS_EXIST;
																 } else {
																	 echo INVALID_USERNAME_PASSWORD;
																 }?>
																 </span>
														</div>
													</div>
													</div>
													<div class="col-md-6">
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo PASSWORD; ?> <span class="required"> * </span></label>
														<div class="col-md-6">
															<input type="password" class="form-control "
																name="password" id="password" value ="<?php echo $password ?>"
																onfocus="javascript:removeTooltip('office_password_tooltip');"> <span
																id="office_password_tooltip" class="tooltiptext"
																style="display: none; color:red;"><?php echo FILL_THIS_FIELD; ?></span>
														</div>
													</div>
													</div>
													</div>
													<div class="row">
													<div class="col-md-6">
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo REPEAT_PASSWORD; ?> <span
															class="required"> * </span></label>
														<div class="col-md-6">
															<input type="password" class="form-control "
																value="<?php echo $password?>" id="retyped_password"
																onfocus="javascript:removeTooltip('retyped_password_tooltip');">
															<span id="retyped_password_tooltip" class="tooltiptext"
																style="display: none; color: red"><?php echo INCORRECT_REPEATED_PASSWORD; ?></span>
														</div>
													</div>
													</div>
													<?php if ($lawyer_office_id == 0) {	?>
													<div class="col-md-6">
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo FIRST_NAME; ?> <span class="required"> * </span></label>
														<div class="col-md-6">
															<input type="text" class="form-control "
																name="first_name" id="first_name" value ="<?php echo $first_name ?>"
																onfocus="javascript:removeTooltip('office_first_name_tooltip');"> <span
																id="office_first_name_tooltip" class="tooltiptext"
																style="display: none; color:red;"><?php echo FILL_THIS_FIELD; ?></span>
														</div>
													</div>
													</div>
													<?php } ?>
													</div>
													<?php if ($lawyer_office_id == 0) {	?>
													<div class="row">
													<div class="col-md-6">
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo LAST_NAME; ?> <span class="required"> * </span></label>
														<div class="col-md-6">
															<input type="text" class="form-control "
																name="last_name" id="last_name" value ="<?php echo $last_name ?>"
																onfocus="javascript:removeTooltip('office_last_name_tooltip');"> <span
																id="office_last_name_tooltip" class="tooltiptext"
																style="display: none; color:red;"><?php echo FILL_THIS_FIELD; ?></span>
														</div>
													</div>
													</div>
													</div>
													<?php } ?>
													<div class="row">
													<div class="col-md-9">
													<div class="form-group">
														<label class="col-md-2 control-label"><?php echo ADDRESS; ?> <span class="required"> * </span></label>
														<div class="col-md-9">
														<textarea class="form-control no-validate" rows="4" cols="50" name="address"
																form="myForm" onfocus="javascript:removeTooltip('address_tooltip');"><?php echo $address?></textarea>
															<span id="address_tooltip" class="tooltiptext"
																style="display: none; color:red;"><?php echo FILL_THIS_FIELD; ?></span>
														</div>
													</div>
													</div>
													</div>
													
												</div>
												
												<div class="form-actions">
													<div class="row">
														<div class="col-md-offset-3 col-md-9">
															<button type="submit" class="btn btn-circle green"><?php
												if ($lawyer_office_id) {
													echo EDIT;
												} else {
													echo INSERT;
												}
												?></button>
															<a
																href="<?=site_url('admin_panel/view_lawyer_offices')?>"
																class="btn btn-outline btn-circle grey-salsa"><?php echo CANCEL; ?>
															</a>
														</div>
													</div>
												</div>
											</form>
											<!-- END FORM-->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
        <?php $this->load->view('utils/footer');?>
	<!--[if lt IE 9]>
<script src="<?=base_url()?>assets/global/plugins/respond.min.js"></script>
<script src="<?=base_url()?>assets/global/plugins/excanvas.min.js"></script> 
<script src="<?=base_url()?>assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
	<!-- BEGIN CORE PLUGINS -->
	<script src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="<?=base_url()?>assets/global/plugins/moment.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/morris/morris.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/morris/raphael-min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/counterup/jquery.waypoints.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/counterup/jquery.counterup.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/fullcalendar/fullcalendar.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.resize.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.categories.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/jquery.sparkline.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="<?=base_url()?>assets/global/scripts/app.min.js"
		type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?=base_url()?>assets/pages/scripts/dashboard.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js"
		type="text/javascript"></script>
	<!-- END THEME LAYOUT SCRIPTS -->

</body>

</html>