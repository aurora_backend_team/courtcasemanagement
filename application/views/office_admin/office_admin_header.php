<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title><?php echo TITLE; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin RTL Theme #3 for dashboard & statistics"
	name="description" />
	
<script	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	
<script>
$(document).ready(function(){
    $("#delBtn").click(function(){
        $("#delNotify").show();
    });
    $("#cancel").click(function(){
        $("#delNotify").hide();
    });
    $("#delNotify").hide();

    $('form').on('focus', 'input[type=number]', function(e) {
    	$(this).on('wheel', function(e) {
    		e.preventDefault();
    	});
        });

        $('form').on('blur', 'input[type=number]', function(e) {
        	$(this).off('wheel');
        });

        $('form').on('keydown', 'input[type=number]', function(e) {
    	if ( e.which == 38 || e.which == 40 )
    		e.preventDefault();
        }); 


        $('form').on('focus', 'input[type=number]', function (e) {
    	$(this).on('mousewheel.disableScroll', function (e) {
    		e.preventDefault();
    	});
        });
        $('form').on('blur', 'input[type=number]', function (e) {
        	$(this).off('mousewheel.disableScroll');
        });	 
    	 
});
</script>

</head>
<!-- END HEAD -->

<body class="page-container-bg-solid">
	<div class="page-wrapper-row">
		<div class="page-wrapper-top">
			<!-- BEGIN HEADER -->
			<div class="page-header">
<?php include('header.php');?>
				<!-- BEGIN HEADER MENU -->
				<div class="page-header-menu">
					<div class="container">
						<!-- BEGIN MEGA MENU -->
						<!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
						<!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
						<div class="hor-menu  ">
							<ul class="nav navbar-nav">
								<?php if (isset($customers[0]["user_name"]) || isset($customer_type)) {?>
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown active" >
								<?php } else {?>	
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown">
									<?php } ?>
									<a
									href="<?=site_url('lawyer_office_panel/view_customers')?>"> <?php echo CUSTOMERS; ?> <span
										class="arrow"></span>
								</a></li>
								<?php if (isset($lawyers[0]["user_name"]) || isset($manager)) {?>
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown active">
								<?php } else {?>	
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown">
									<?php } ?>
									
									<a
									href="<?=site_url('lawyer_office_lawyers')?>"> <?php echo LAWYERS; ?> <span
										class="arrow"></span>
								</a></li>
								<?php if (isset($secretaries) || (isset($user_id) && !isset($customer_type) && !isset($manager))) {?>
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown active">
								<?php } else {?>	
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown">
									<?php } ?>
									
									<a
									href="<?=site_url('lawyer_office_secretaries')?>"> <?php echo SECRETARIES; ?> <span class="arrow"></span>
								</a></li>
								<?php if (isset($customer_types) || isset($customer_type_id)) {?>
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown active">
								<?php } else {?>	
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown">
									<?php } ?>
									<a style="width:101%"
									href="<?=site_url('lawyer_office_customer_types')?>"> <?php echo CUSTOMER_TYPES; ?> <span class="arrow"></span>
								</a></li>
								<?php if (isset($regions) || (isset($region_id) && !isset($cost) && !isset($manager))) {?>
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown active">
								<?php } else {?>	
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown">
									<?php } ?>
									<a
									href="<?=site_url('lawyer_office_regions')?>"> <?php echo REGIEONS; ?> <span class="arrow"></span>
								</a></li>
								<?php if (isset($cases) || isset($case_details) || isset($search_data) || (isset($case_id) && !isset($invoice_id))) {?>
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown active">
								<?php } else {?>	
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown">
									<?php } ?>
									<a
									href="<?=site_url('case_management')?>"> <?php echo MY_CASES; ?> <span
										class="arrow"></span>
								</a></li>
								<?php if ((isset($customers) && isset($lawyers)) ||isset($invoices)||isset($invoice_id)) {?>
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown active">
								<?php } else {?>	
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown">
									<?php } ?><a style="width:101%"
									href="<?=site_url('accounting_module/get_customers_and_lawyers')?>"> <?php echo LAWYER_CUSTOMER_INVOICES; ?> <span class="arrow"></span>
								</a></li>
								
								<?php if (isset($notifications) || isset($send_time)) {?>
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown active">
								<?php } else {?>	
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown">
									<?php } ?><a
									href="<?=site_url('notifications/view_notifications')?>"> <?php echo NOTIFICATIONS; ?> <span class="arrow"></span>
								</a></li>
								
								<?php if (isset($dashboardReports)) {?>
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown active">
								<?php } else {?>	
								<li aria-haspopup="true"
									class="menu-dropdown classic-menu-dropdown">
									<?php } ?><a
									href="<?=site_url('dashboard_reports/view_dashboard_reports/2')?>"> <?php echo DASHBOARD_REPORTS; ?> <span class="arrow"></span>
								</a></li>
								
							</ul>
						</div>
						<!-- END MEGA MENU -->
					</div>
				</div>
				<!-- END HEADER MENU -->
			</div>
			<!-- END HEADER -->
		</div>
	</div>
	<br>
</body>
</html>