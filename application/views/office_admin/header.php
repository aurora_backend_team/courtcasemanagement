<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title><?php echo TITLE; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin RTL Theme #3 for dashboard & statistics"
	name="description" />
<meta content="" name="author" />
<?php include('main_header.php');?>
</head>
<!-- END HEAD -->

<body class="page-container-bg-solid">
<div id="loading" class="hidden">
	<div id="loading-image">
	  <img  src="<?php echo base_url(); ?>images/Ajax-loader2.gif" alt="Loading..." />
	  <p id="search_text" class="loading-text"> جاري البحث</p>
	  
	</div>
	</div>
	<!-- BEGIN HEADER TOP -->
	<div class="page-header-top">
		<div class="container">
			<!-- BEGIN LOGO -->
			<div class="page-logo">
				<a
					href="
				<?php
				
if (isset ( $user_type_code ) && $user_type_code != "ADMIN") {
					echo site_url ( 'case_management/view_cases' );
				} else {
					echo site_url ( 'lawyer_office_panel/view_customers' );
				}
				?>
				"> <img
					src="<?=base_url()?>images/logohighquality.png"
					alt="logo" class="logo-default">
				</a>
			</div>
			<!-- END LOGO -->
			<!-- BEGIN RESPONSIVE MENU TOGGLER -->
			<a href="javascript:;" class="menu-toggler"></a>
			<!-- END RESPONSIVE MENU TOGGLER -->
			<!-- BEGIN TOP NAVIGATION MENU -->
			<div class="top-menu">
				<ul class="nav navbar-nav pull-right">
					<!-- BEGIN NOTIFICATION DROPDOWN -->
					<!-- DOC: Apply "dropdown-hoverable" class after "dropdown" and remove data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to enable hover dropdown mode -->
					<!-- DOC: Remove "dropdown-hoverable" and add data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to the below A element with dropdown-toggle class -->
					<!--  <li
						class="dropdown dropdown-extended dropdown-notification dropdown-dark"
						id="header_notification_bar"><a href="javascript:;"
						class="dropdown-toggle" data-toggle="dropdown"
						data-hover="dropdown" data-close-others="true"> <i
							class="icon-bell"></i> <span class="badge badge-default">7</span>
					</a></li>
					
					
					<li class="droddown dropdown-separator"><span class="separator"></span></li>
					-->
					<!-- BEGIN USER LOGIN DROPDOWN -->
					<!-- BEGIN Notifications Bell -->
					<li
						class="dropdown dropdown-extended dropdown-notification dropdown-dark"
						id="header_notification_bar"><a href="javascript:;"
						class="dropdown-toggle" data-toggle="dropdown"
						data-hover="dropdown" data-close-others="true" id="bell"> <i
							class="icon-bell" onclick="resetCounter()"></i>
							                     <?php if ($count_unseen !== 0) {?>
                                                <span id="notify_count"
							onclick="resetCounter()" class="badge badge-default">
                                                	
                                                		<?php echo $count_unseen?>
                                                </span>
                                                <?php }?>
                                            </a>
  
                                            <ul class="dropdown-menu">
							<li>
								<ul id="notifications" class="dropdown-menu-list scroller"
									style="height: 250px;" data-handle-color="#637283">
                                                    <?php
																																													for($i = 0; $i < count ( $notifications_list ); $i ++) {
																																												?>
                                                       <li><a
										href="<?=site_url('case_management/view_case_details/'.$notifications_list[$i]["case_id"])?>" >
											<span class="time"><?php echo $notifications_list[$i]["sent_date_time"]?></span>
											<span class="details">
                                                                    <?= $notifications_list[$i]["message"] ?> </span>
									</a></li>
									
                                                        <?php
																																													}
																																													?>
                                                        
                                                    </ul>
							</li>
						</ul>
                                           
                    </li>
					<!-- END Notifications Bell -->

					<li class="dropdown dropdown-user dropdown-dark"><a
						href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"
						data-hover="dropdown" data-close-others="true"> 
						
						<?php if(isset($profile_img) && $profile_img != '') {?>
															<img alt="" class="img-circle"
							src="<?=base_url()?><?=$profile_img?>" class="img-responsive"
							alt="">
															<?} else {?>	
															<img alt="" class="img-circle"
							src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image"
							class="img-responsive" alt="">
															<?}?>
						
						
							
							
							<span class="username username-hide-mobile"><?=$profile_first_name.' '.$profile_last_name?></span>
					</a>
						<ul class="dropdown-menu dropdown-menu-default">
							<li><a href="<?=site_url('user_profile')?>"> <i class="icon-user"></i>
									<?php echo MY_PROFILE; ?>
							</a></li>
						<!-- 	<li><a href=""> <i class="icon-calendar"></i> 
									<?php echo MY_CALENDAR; ?>
							</a></li> -->
							<li><a href="<?=site_url('help/view_FAQs/1')?>"> <i class="icon-info"></i> 
									<?php echo HELP; ?>
							</a></li>
							<li><a href="<?=site_url('user_login/logoff')?>"> <i
									class="icon-key"></i>
									<?php echo LOGOUT; ?>
							</a></li>
						</ul></li>
					<!-- END USER LOGIN DROPDOWN -->

					<li class="droddown dropdown-separator"><span class="separator"></span></li>
					<li class="dropdown dropdown-user dropdown-dark"><a
						href="<?=site_url('user_login/logoff')?>" class="dropdown-toggle"
						data-hover="dropdown"> <img alt=""
							src="<?=base_url()?>assets/layouts/layout3/img/logout-icon.png"></a></li>
					<!-- END QUICK SIDEBAR TOGGLER -->
				</ul>
			</div>
			<!-- END TOP NAVIGATION MENU -->
		</div>
	</div>


</body>
</html>
