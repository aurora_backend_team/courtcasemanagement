<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<?php
session_start ();
$this->load->view ( 'localization/lang' );
$this->load->view ( 'localization/txt' );
defineLocale ();
defineStrings ();
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<script type="text/javascript">
function checkValid(email_valdation) {
	if (textNumViolation())
		return false;
	replacAllArabToEng();
	var user_name = document.forms["myForm"]["user_name"].value;
	var password = document.forms["myForm"]["password"].value;
	var email = document.forms["myForm"]["email"].value;
	var mobile_number = document.forms["myForm"]["mobile_number"].value;
	var retyped_password = document.forms["myForm"]["retyped_password"].value;
	var first_name = document.forms["myForm"]["first_name"].value;
	var last_name = document.forms["myForm"]["last_name"].value;
	var mobile_pattern = /05/g;

	if (first_name == "") {
		$("#first_name_tooltip").css("display","block");
		$("#first_name_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#first_name_tooltip").css("display","none");
		$("#first_name_tooltip").css("visibility","none");
	}
	if (last_name == "") {
		$("#last_name_tooltip").css("display","block");
		$("#last_name_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#last_name_tooltip").css("display","none");
		$("#last_name_tooltip").css("visibility","none");
	}
	if (mobile_number != "" && (mobile_number.length < 10 || mobile_number.length > 10 ||
        !mobile_pattern.test(mobile_number))) {
		$("#mobile_number_tooltip").css("display","block");
		$("#mobile_number_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#mobile_number_tooltip").css("display","none");
		$("#mobile_number_tooltip").css("visibility","none");
	}
	if (user_name == "") {
		$("#user_name_tooltip").css("display","block");
		$("#user_name_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#user_name_tooltip").css("display","none");
		$("#user_name_tooltip").css("visibility","none");
	}
	if (password == "") {
		$("#password_tooltip").css("display","block");
		$("#password_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#password_tooltip").css("display","none");
		$("#password_tooltip").css("visibility","none");
	}
	if (retyped_password == "" || password != retyped_password) {
		$("#retyped_password_tooltip").css("display","block");
		$("#retyped_password_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#retyped_password_tooltip").css("display","none");
		$("#retyped_password_tooltip").css("visibility","none");
	}
	if (email == "" || !validateEmail(email)) {
		$("#email_tooltip").css("display","block");
		$("#email_tooltip").css("visibility","visible");
		
		return false;
	}else{
		$("#email_tooltip").css("display","none");
		$("#email_tooltip").css("visibility","none");
	}
	$('#loading').removeClass('hidden');
}

function validateEmail(email) 
{
	//var re = /\S+@\S+\.\S+/;
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}
</script>
<meta charset="utf-8" />
<title><?php echo TITLE; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin RTL Theme #3 for dashboard & statistics"
	name="description" />
<meta content="" name="author" />

<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon"
	href="<?=base_url()?>assets/layouts/layout3/img/logo-default.jpg" />
</head>
<!-- END HEAD -->
<body class="page-container-bg-solid">
	<div class="page-wrapper">
<?php include 'office_admin_header.php';?>
				<div class="page-wrapper-row full-height">
			<div class="page-wrapper-middle">
				<!-- BEGIN CONTAINER -->
				<div class="page-container">
					<!-- BEGIN CONTENT -->
					<div class="page-content-wrapper">
						<!-- BEGIN CONTENT BODY -->
						<!-- BEGIN PAGE HEAD-->
						<div class="page-head">
							<div class="container">
								<div class="col-md-10">
									<div class="portlet box green">
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-gift"></i>
												<?php
												if ($user_id) {
													echo $first_name . ' ' . $last_name;
												} else {
													echo ADD_RECORD;
												}
												?>
											</div>

										</div>
										<div class="portlet-body form">
											<!-- BEGIN FORM-->
											<form id="myForm" autocomplete="off" role="form"
												method="post"
												onsubmit="javascript:return checkValid('<?php echo INVALID_EMAIL; ?>');"
												action="<?=site_url('lawyer_office_secretaries/add_edit_secretary/'.$user_id)?>"
												class="form-horizontal">
												<div class="form-body">
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo FIRST_NAME; ?><span
																	class="required"> * </span></label>
																<div class="col-md-6">
																	<input type="text" class="form-control "
																		name="first_name" value="<?php echo $first_name?>">
																		<span id="first_name_tooltip"
																class="tooltiptext" style="display: none; color:red"><?php echo FILL_THIS_FIELD; ?></span>
																
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo LAST_NAME; ?><span
																	class="required"> * </span></label>
																<div class="col-md-6">
																	<input type="text" class="form-control "
																		name="last_name" value="<?php echo $last_name?>">
																		<span id="last_name_tooltip"
																class="tooltiptext" style="display: none; color:red"><?php echo FILL_THIS_FIELD; ?></span>

																</div>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo MOBILE_NUMBER; ?></label>
																<div class="col-md-6">
																	<input type="text" act="yes" class="form-control no-spinners"
																		onkeydown="numberKeyDown(event)" name="mobile_number"
																		value="<?php echo $mobile_number?>"
																		onfocus="javascript:removeTooltip('mobile_number_tooltip');">
																	<span id="mobile_number_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo MOBILE_NUMBER_INVALID; ?></span>

																</div>
															</div>
														</div>

														<div class="col-md-6">

															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo EMAIL; ?><span
																	class="required"> * </span></label>
																<div class="col-md-6">
																	<input type="text" class="form-control no-validate" name="email"
																		id="email" value="<?php echo $email?>"
																		id="office_email"
																		onfocus="javascript:removeTooltip('email_tooltip');">
																	<span id="email_tooltip" class="tooltiptext"
																		<?php if (isset($email_exists_error) && $email_exists_error == 1){?>
																		style="display: block; color: red;" <?php }else {?>
																		style="display: none; color:red;" <?php }?>><?php
																		
																		if (isset ( $email_exists_error ) && $email_exists_error == 1) {
																			echo EMAIL_IS_EXIST;
																		} else {
																			echo INVALID_EMAIL;
																		}
																		?></span>
																</div>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo USERNAME; ?> <span
																	class="required"> * </span></label>
																<div class="col-md-6">
																	<input type="text" class="form-control no-validate"
																		name="user_name" value="<?php echo $user_name?>"
																		id="user_name"
																		onfocus="javascript:removeTooltip('user_name_tooltip');">
																	<span id="user_name_tooltip" class="tooltiptext"
																		<?php if (isset($username_exists_error) && $username_exists_error == 1){?>
																		style="display: block; color: red;" <?php }else {?>
																		style="display: none; color:red;" <?php }?>><?php
																		
																		if (isset ( $username_exists_error ) && $username_exists_error == 1) {
																			echo USERNAME_IS_EXIST;
																		} else {
																			echo FILL_THIS_FIELD;
																		}
																		?></span>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo PASSWORD; ?><span
																	class="required"> * </span></label>
																<div class="col-md-6">
																	<input type="password" class="form-control no-validate"
																		name="password" value="<?php echo $password?>"
																		id="password"
																		onfocus="javascript:removeTooltip('password_tooltip');">
																	<span id="password_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo FILL_THIS_FIELD; ?></span>
																</div>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-md-4 control-label"><?php echo REPEAT_PASSWORD; ?> <span
																	class="required"> * </span></label>
																<div class="col-md-6">
																	<input type="password" class="form-control no-validate"
																		value="<?php echo $password?>" id="retyped_password"
																		onfocus="javascript:removeTooltip('retyped_password_tooltip');">
																	<span id="retyped_password_tooltip" class="tooltiptext"
																		style="display: none; color: red"><?php echo INCORRECT_REPEATED_PASSWORD; ?></span>
																</div>
															</div>
														</div>
													</div>
													<div class="form-actions">
														<div class="row">
															<div class="col-md-offset-3 col-md-9">
																<button type="submit" class="btn btn-circle green"><?php
																if ($user_id) {
																	echo EDIT;
																} else {
																	echo INSERT;
																}
																?></button>
																<a
																	href="<?=site_url('lawyer_office_secretaries')?>"
																	class="btn btn-outline btn-circle grey-salsa"><?php echo CANCEL; ?>
																</a>
															</div>
														</div>
													</div>

												</div>

											</form>
											<!-- END FORM-->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
        <?php $this->load->view('utils/footer');?>
	<!-- BEGIN CORE PLUGINS -->
	<script src="<?=base_url()?>assets/global/plugins/jquery.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.sparkline.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="<?=base_url()?>assets/global/scripts/app.min.js"
		type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?=base_url()?>assets/pages/scripts/profile.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script
		src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js"
		type="text/javascript"></script>
	<!-- END THEME LAYOUT SCRIPTS -->

</body>

</html>