<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<?php
session_start ();
$this->load->view ( 'localization/lang' );
$this->load->view ( 'localization/txt' );
defineLocale ();
defineStrings ();
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
    
    <script type="text/javascript">
    function checkBasicInfoValid(email_valdation) {
    	var user_name = document.forms["basic_info_form"]["user_name"].value;
    	var first_name = document.forms["basic_info_form"]["first_name"].value;
    	var last_name = document.forms["basic_info_form"]["last_name"].value;
    	var email = document.forms["basic_info_form"]["email"].value;
    	var mobile_number = document.forms["basic_info_form"]["mobile_number"].value;
    	if (user_name == "") {
    		$("#user_name_tooltip").css("display","block");
    		$("#user_name_tooltip").css("visibility","visible");
    		return false;
    	} else{
    		$("#user_name_tooltip").css("display","none");
    		$("#user_name_tooltip").css("visibility","none");
    	}
    	if (first_name == "") {
    		$("#first_name_tooltip").css("display","block");
    		$("#first_name_tooltip").css("visibility","visible");
    		return false;
    	} else{
    		$("#first_name_tooltip").css("display","none");
    		$("#first_name_tooltip").css("visibility","none");
    	}
    	if (last_name == "") {
    		$("#last_name_tooltip").css("display","block");
    		$("#last_name_tooltip").css("visibility","visible");
    		return false;
    	} else{
    		$("#last_name_tooltip").css("display","none");
    		$("#last_name_tooltip").css("visibility","none");
    	}
    	if (mobile_number == "") {
    		$("#mobile_number_tooltip").css("display","block");
    		$("#mobile_number_tooltip").css("visibility","visible");
    		return false;
    	} else{
    		$("#mobile_number_tooltip").css("display","none");
    		$("#mobile_number_tooltip").css("visibility","none");
    	}
    	if (email != "" && !validateEmail(email)) {
    		$("#email_tooltip").text(email_valdation);
    		$("#email_tooltip").css("display","block");
    		$("#email_tooltip").css("visibility","visible");
    		
    		return false;
    	}
    }

    function checkOfficeInfoValid(email_valdation) {
    	  var package = document.forms["office_form"]["package"].value;
    		var office_name = document.forms["office_form"]["office_name"].value;
    		var address = document.forms["office_form"]["address"].value;
    		var mobile_number = document.forms["office_form"]["office_mobile_number"].value;
    		var office_email = document.forms["office_form"]["office_email"].value;
    		var contact_person_name = document.forms["office_form"]["contact_person_name"].value;
    		
    		if (package == "") {
    			$("#package_tooltip").css("display","block");
    			$("#package_tooltip").css("visibility","visible");
    			return false;
    		} else{
    			$("#package_tooltip").css("display","none");
    			$("#package_tooltip").css("visibility","none");
    		}
    		if (office_name == "") {
    			$("#office_name_tooltip").css("display","block");
    			$("#office_name_tooltip").css("visibility","visible");
    			return false;
    		} else{
    			$("#office_name_tooltip").css("display","none");
    			$("#office_name_tooltip").css("visibility","none");
    		}
    		if (address == "") {
    			$("#address_tooltip").css("display","block");
    			$("#address_tooltip").css("visibility","visible");
    			return false;
    		} else{
    			$("#address_tooltip").css("display","none");
    			$("#address_tooltip").css("visibility","none");
    		}
    		if (mobile_number == "") {
    			$("#office_mobile_number_tooltip").css("display","block");
    			$("#office_mobile_number_tooltip").css("visibility","visible");
    			return false;
    		} else{
    			$("#office_mobile_number_tooltip").css("display","none");
    			$("#office_mobile_number_tooltip").css("visibility","none");
    		}
    		if (office_email == "") {
    			$("#office_email_tooltip").css("display","block");
    			$("#office_email_tooltip").css("visibility","visible");
    			return false;
    		} else if (!validateEmail(office_email)) {
    			$("#office_email_tooltip").text(email_valdation);
    			$("#office_email_tooltip").css("display","block");
    			$("#office_email_tooltip").css("visibility","visible");
    			
    			return false;
    		}else{
    			$("#office_email_tooltip").css("display","none");
    			$("#office_email_tooltip").css("visibility","none");
    		}
    		if (contact_person_name == "") {
    			$("#contact_person_name_tooltip").css("display","block");
    			$("#contact_person_name_tooltip").css("visibility","visible");
    			return false;
    		} else{
    			$("#contact_person_name_tooltip").css("display","none");
    			$("#contact_person_name_tooltip").css("visibility","none");
    		}
    }
    
	function validateEmail(email) 
	{
		//var re = /\S+@\S+\.\S+/;
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}
	
	
</script>

        <meta charset="utf-8" />
        <title><?php echo TITLE; ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin RTL Theme #3 for user account page" name="description" />
        <meta content="" name="author" />
    
        <link rel="shortcut icon"
	href="<?=base_url()?>assets/layouts/layout3/img/logo-default.jpg" /> </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid">
        <div class="page-wrapper">
           <?php include 'office_admin_header.php';?>
                 
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <!-- BEGIN CONTAINER -->
                    <div class="page-container">
                        <!-- BEGIN CONTENT -->
                        <div class="page-content-wrapper">
                            <!-- BEGIN CONTENT BODY -->
                            <!-- BEGIN PAGE HEAD-->
                            <div class="page-head">
                                <div class="container">
                                    <!-- BEGIN PAGE TITLE -->
                                    <div class="page-title">
                                        <h1><?php echo PROFILE_ACCOUNT; ?>
                                        </h1>
                                    </div>
                                    <!-- END PAGE TITLE -->
                                    <!-- BEGIN PAGE TOOLBAR -->
                                    
                                    <!-- END PAGE TOOLBAR -->
                                </div>
                            </div>
                            <!-- END PAGE HEAD-->
                            <!-- BEGIN PAGE CONTENT BODY -->
                            <div class="page-content">
                                <div class="container">
                                    <div>  </div>
                                    <!-- BEGIN PAGE CONTENT INNER -->
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <!-- BEGIN PROFILE SIDEBAR -->
                                                <div class="profile-sidebar">
                                                    <!-- PORTLET MAIN -->
                                                    <div class="portlet light profile-sidebar-portlet ">
                                                        <!-- SIDEBAR USERPIC -->
                                                        <div class="profile-userpic">
															<?php if(isset($profile_img) && $profile_img != '') {?>
															<img src="<?=base_url()?><?=$profile_img?>" class="img-responsive" alt="">
															<?} else {?>	
															<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" class="img-responsive" alt="">
															<?}?>
                                                             </div>
                                                        <!-- END SIDEBAR USERPIC -->
                                                        <!-- SIDEBAR USER TITLE -->
                                                        <div class="profile-usertitle">
                                                            <div class="profile-usertitle-name"> <?= $first_name.' '.$last_name ; ?> </div>
                                                            <div class="profile-usertitle-job">  <?php echo OFFICE_ADMIN ; ?> </div>
                                                        </div>
                                                        <!-- END SIDEBAR USER TITLE -->
                                                      
                                                    
                                                    </div>
                                                    <!-- END PORTLET MAIN -->
                                                    <!-- PORTLET MAIN -->
                                                    <div class="portlet light ">
                                                        <!-- STAT -->
                                                        <div class="row list-separated profile-stat">
                                                            <div class="col-md-4 col-sm-4 col-xs-6">
                                                                <div class="uppercase profile-stat-title"> <?php echo 0 ; ?> </div>
                                                                <div class="uppercase profile-stat-text"> <?php echo CASES ; ?> </div>
                                                            </div>
                                                            <div class="col-md-4 col-sm-4 col-xs-6">
                                                                <div class="uppercase profile-stat-title"> <?= $lawyers_count ; ?> </div>
                                                                <div class="uppercase profile-stat-text"> <?php echo LAWYER ; ?> </div>
                                                            </div>
                                                            <div class="col-md-4 col-sm-4 col-xs-6">
                                                                <div class="uppercase profile-stat-title"> <?= $customers_count ; ?>  </div>
                                                                <div class="uppercase profile-stat-text"> <?php echo CUSTOMER ; ?> </div>
                                                            </div>
                                                           
                                                        </div>
                                                        <!-- END STAT -->
                                                        <div>
                                                            <h4 class="profile-desc-title"><?php echo ABOUT_OFFICE; ?></h4>
                                                            <span class="profile-desc-text"><?php echo STATIC_ABOUT; ?> </span>
                                                        </div>
                                                    </div>
                                                    <!-- END PORTLET MAIN -->
                                                </div>
                                                <!-- END BEGIN PROFILE SIDEBAR -->
                                                <!-- BEGIN PROFILE CONTENT -->
                                                <div class="profile-content">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="portlet light ">
                                                                <div class="portlet-title tabbable-line">
                                                                    <div class="caption caption-md">
                                                                        <i class="icon-globe theme-font hide"></i>
                                                                        <span class="caption-subject font-blue-madison bold uppercase"><?php echo PROFILE_ACCOUNT; ?></span>
                                                                    </div>
                                                                    <ul class="nav nav-tabs">
                                                                        <li class="active">
                                                                            <a href="#tab_1_1" data-toggle="tab"> <?php echo BASIC_INFO; ?> </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#tab_1_2" data-toggle="tab"> <?php echo CHANGE_PROFILE_PIC; ?> </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#tab_1_3" data-toggle="tab"> <?php echo CHANGE_PASSWORD; ?> </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#tab_1_4" data-toggle="tab"> <?php echo OFFICE_INFO; ?> </a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="portlet-body">
                                                                    <div class="tab-content">
                                                                        <!-- PERSONAL INFO TAB -->
                                                                        <div class="tab-pane active" id="tab_1_1">
                                                                            <form id="basic_info_form" class="form-horizontal" role="form" method='post' action="<?=site_url('user_profile/view_edit_general_profile/'.$user_id)?>"
                                                                            onsubmit="javascript:return checkBasicInfoValid('<?php echo INVALID_EMAIL; ?>');">
                                                                            <div class="form-body">
                                                                           	 	<div class="form-group">
                                                                                    <label  class="col-md-3 control-label"> <?php echo USERNAME; ?> <span class="required"> * </span></label>
																					<div class="col-md-4">
																						<input  class="form-control "
																							name="user_name" id="user_name" value="<?php echo $user_name?>"
																							onfocus="javascript:removeTooltip('user_name_tooltip');">
																						<span id="user_name_tooltip" class="tooltiptext"
																							style="display: none; color:red;"><?php echo FILL_THIS_FIELD; ?></span>
																					</div>
																				</div>
                                                                                <div class="form-group">
                                                                                    <label  class="col-md-3 control-label"> <?php echo FIRST_NAME; ?> <span class="required"> * </span></label>
																					<div class="col-md-4">
																						<input  class="form-control "
																							name="first_name" id="first_name" value="<?php echo $first_name?>"
																							onfocus="javascript:removeTooltip('first_name_tooltip');">
																						<span id="first_name_tooltip" class="tooltiptext"
																							style="display: none; color:red;"><?php echo FILL_THIS_FIELD; ?></span>
																					</div>
																				</div>
                                                                                <div class="form-group">
                                                                                    <label  class="col-md-3 ontrol-label"> <?php echo LAST_NAME; ?> <span class="required"> * </span></label>
																					<div class="col-md-4">
																						<input  class="form-control "
																							name="last_name" id="last_name" value="<?php echo $last_name?>"
																							onfocus="javascript:removeTooltip('last_name_tooltip');">
																						<span id="last_name_tooltip" class="tooltiptext"
																							style="display: none; color:red;"><?php echo FILL_THIS_FIELD; ?></span>
																					</div>
																				</div>
                                                                                <div class="form-group">
																					<label class="col-md-3 control-label"><?php echo MOBILE_NUMBER; ?> <span class="required"> * </span></label>
																					<div class="col-md-4"> 
																						<input type="number" class="form-control "
																							name="mobile_number" id="mobile_number" value="<?php echo $mobile_number?>"
																							onfocus="javascript:removeTooltip('mobile_number_tooltip');">
																						<span id="mobile_number_tooltip" class="tooltiptext"
																							style="display: none; color:red;"><?php echo FILL_THIS_FIELD; ?></span>
																					</div>
																				</div>
                                                                                <div class="form-group">
                                                                                    <label class="col-md-3 control-label"><?php echo EMAIL; ?> <span class="required"> * </span> </label>
                                                                                    <div class="col-md-4">
																							<input type="text" class="form-control "
																								name="email" value="<?php echo $email?>"
																								id="email"
																								onfocus="javascript:removeTooltip('email_tooltip');"> <span
																								id="email_tooltip" class="tooltiptext"
																								<?php if (isset($email_exists_error) && $email_exists_error == 1){?>
																								style="display: block; color:red;"
																								<?php }else {?>
																								style="display: none; color:red;"
																								<?php }?>
																								
																								><?php if (isset($email_exists_error) && $email_exists_error == 1){
																								echo EMAIL_IS_EXIST;
																 								}?></span>
																						</div>
																				</div>
                                                                              
                                                                              	<div class="margin-top-10">
                                                                              		<button type="submit" class="btn green"><?php echo SAVE ?></button>
                                                                                </div>
                                                                              </div>
                                                                            </form>
                                                                        </div>
                                                                        <!-- END PERSONAL INFO TAB -->
                                                                        <!-- CHANGE AVATAR TAB -->
                                                                        <div class="tab-pane" id="tab_1_2">
                                                                            <p> <?php echo CHANGE_AVATAR_DESCRIPTION; ?>  </p>
                                                                            <form id="avatarForm" method= "post" action="<?=site_url('user_profile/change_avatar/'.$user_id)?>" role="form" method='post' enctype="multipart/form-data">
                                                                                <div class="form-group">
                                                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
																							<?php if(isset($profile_img) && $profile_img != '') {?>
																							<img id="img_avatar" src="<?=base_url()?><?=$profile_img?>" alt="" />
                                                                                            <?} else {?>	
																							<img id="img_avatar" src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
																							<?}?>		
																							
																							</div>
                                                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                                        <div>
                                                                                            <span class="btn default btn-file">
                                                                                                <span class="fileinput-new"> <?php echo SELECT_IMAGE ; ?></span>
                                                                                                <span class="fileinput-exists"> <?php echo CHANGE_PROFILE_PIC; ?></span>
                                                                                                <input type="file" name="avatar" accept="image/*"> </span>
                                                                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> <?php echo DELETE; ?></a>
                                                                                        </div>
                                                                                    </div>
                                                                                   
                                                                                </div>
                                                                                <div class="margin-top-10">
																					<a href="javascript:changeAvatar();" class="btn green"> <?php echo SAVE; ?></a>
                                                                                    <a href="javascript:cancelAvatarChange('<?=base_url()?><?=$profile_img?>');" class="btn default"> <?php echo CANCEL; ?></a>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                        <!-- END CHANGE AVATAR TAB -->
                                                                        <!-- CHANGE PASSWORD TAB -->
                                                                        <div class="tab-pane" id="tab_1_3">
																		 <div class="form-body">
																			<div class="form-group" id="incorrect_old_password" style="display: none; color:red;">
																				<?php echo INCORRECT_OLD_PASSWORD;?>
																			</div>
																			<div class="form-group" id="incorrect_repeated_password" style="display: none; color:red;">
																				<?php echo INCORRECT_REPEATED_PASSWORD;?>
																			</div>
                                                                            <form id="passwordForm" class="form-horizontal"  action="<?=site_url('user_profile/change_password/'.$user_id)?>" 
                                                                            	role="form" method='post' onsubmit="javascript:return checkPassword();">
                                                                                <div class="form-group">
                                                                                    <label class="col-md-3 control-label"><?php echo CURRENT_PASSWORD ; ?> <span class="required"> * </span> </label>
																					<div class="col-md-4">
																						<input type="password" name="old_password" id="old_password" class="form-control " 
																onfocus="javascript:removeTooltip('old_password_tooltip');">															<span id="old_password_tooltip" class="tooltiptext"
																style="display: none; color: red";><?php echo FILL_THIS_FIELD; ?><span class="required"> * </span></span>
																						<input type="hidden" value="<?=$password?>" id="saved_password"/>
																					</div>
																				</div>
                                                                                <div class="form-group">
                                                                                    <label class="col-md-3 control-label"><?php echo NEW_PASSWORD ; ?><span class="required"> * </span></label>
                                                                                    <div class="col-md-4">
																						<input type="password" name="new_password" id="new_password" class="form-control " onfocus="javascript:removeTooltip('new_password_tooltip');">															<span id="new_password_tooltip" class="tooltiptext"
																style="display: none; color: red";><?php echo FILL_THIS_FIELD; ?></span>
																					</div>
																				</div>
                                                                                <div class="form-group">
                                                                                    <label class="col-md-3 control-label"><?php echo REPEAT_NEW_PASSWORD ; ?><span class="required"> * </span></label>
																					<div class="col-md-4">
																						<input type="password" name="retyped_new_password" id="retyped_new_password" class="col-md-4 form-control " onfocus="javascript:removeTooltip('retyped_new_password_tooltip');">															<span id="retyped_new_password_tooltip" class="tooltiptext"
																style="display: none; color: red";><?php echo FILL_THIS_FIELD; ?></span> 
																					</div>
																				</div>
                                                                                <div class="margin-top-10">
                                                                                   <!-- <a href="javascript:changePassword();" class="btn green"> <?php echo CHANGE_PASSWORD ; ?> </a>-->
                                                                                    <button type="submit" class="btn green"><?php echo CHANGE_PASSWORD ; ?></button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
																		</div>
                                                                        <!-- END CHANGE PASSWORD TAB -->
                                                                        <!-- OFFICE INFO TAB -->
                                                                        <div class="tab-pane" id="tab_1_4">
                                                                            <form id="office_form" class="form-horizontal" role="form" method="post" 
                                                                            onsubmit="javascript:return checkOfficeInfoValid('<?php echo INVALID_EMAIL; ?>');"
                                                                            action="<?=site_url('user_profile/edit_office_info/'.$user_id)?>">
                                                                            
                                                                         <div class="form-body">
																			<div class="form-group">
																				<label class="col-md-3 control-label"><?php echo PACKAGE_CODE; ?> <span class="required"> * </span></label>
																				<div class="col-md-4"> <select class="form-control "
																					name="package_code" id="package" class="form-control">
																					<?php
																						if ($available_packages) {
																							for($i = 0; $i < count ( $available_packages ); $i ++) {
																								$package_code_s = $available_packages [$i] ["package_code"];
																								$package_name = $available_packages [$i] ["package_name"];
																								?>
																							<option value="<?=$package_code_s?>"
																							<?php if ($package_code == $package_code_s){?>
																							selected="selected" <?php }?>> <?=$package_name?></option>
																							<?php
																							}
																						} 
																						else 
																						{?>
																							<option value="" selected></option> <?php
																						}?>		
																				</select
																					onfocus="javascript:removeTooltip('package_tooltip');"> <span
																					id="package_tooltip" class="tooltiptext"
																					style="display: none; color:red;"><?php echo FILL_THIS_FIELD; ?></span>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-md-3 control-label"><?php echo OFFICE_NAME; ?> <span class="required"> * </span></label>
																				<div class="col-md-4">
																					<input type="text" class="form-control "
																						name="office_name" id="office_name" value ="<?php echo $name?>"
																						onfocus="javascript:removeTooltip('office_name_tooltip');"> <span
																						id="office_name_tooltip" class="tooltiptext"
																						style="display: none; color:red;"><?php echo FILL_THIS_FIELD; ?></span>
																				</div>
																			</div>
													
																			<div class="form-group">
																				<label class="col-md-3 control-label"><?php echo ADDRESS; ?> <span class="required"> * </span></label>
																				<div class="col-md-7">
																				<textarea class="form-control " rows="4" cols="50" name="address"
																						 onfocus="javascript:removeTooltip('address_tooltip');"><?php echo $address?></textarea>
																					<span id="address_tooltip" class="tooltiptext"
																						style="display: none; color:red;"><?php echo FILL_THIS_FIELD; ?></span>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-md-3 control-label"><?php echo CITY; ?></label>
																				<div class="col-md-4">
																					<select class="form-control " name="city"
																						id="city" class="form-control">
																						<?php
																								if ($available_cities) {
																									for($i = 0; $i < count ( $available_cities ); $i ++) {
																										$city_id = $available_cities [$i] ["city_id"];
																										$city_name = $available_cities [$i] ["city_name"];
																										?>
																							<option value="<?=$city_id?>" <?php if ($city == $city_id){?>
																																	selected="selected" <?php }?>> <?=$city_name?></option>
																						<?php
																									}
																								} else 
																								{
																								?>
																							<option value="" selected></option><?php
																								}?>		
																					</select>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-md-3 control-label"><?php echo PHONE_NUMBER; ?></label>
																				<div class="col-md-4">
																					<input type="number" class="form-control "
																						name="office_phone_number" value="<?php echo $office_phone_number?>">
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-md-3 control-label"><?php echo MOBILE_NUMBER; ?> <span class="required"> * </span></label>
																				<div class="col-md-4">
																					<input type="number" class="form-control "
																						name="office_mobile_number" id="office_mobile_number" value="<?php echo $office_mobile_number?>"
																						onfocus="javascript:removeTooltip('office_mobile_number_tooltip');">
																					<span id="office_mobile_number_tooltip" class="tooltiptext"
																						style="display: none; color:red;"><?php echo FILL_THIS_FIELD; ?></span>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-md-3 control-label"><?php echo EMAIL; ?></label>
																				<div class="col-md-4">
																					<input type="text" class="form-control "
																						name="office_email" value="<?php echo $office_email?>"
																						id="office_email"
																						onfocus="javascript:removeTooltip('office_email_tooltip');"> <span
																						id="office_email_tooltip" class="tooltiptext"
																						<?php if (isset($email_exists_error) && $email_exists_error == 1){?>
																						style="display: block; color:red;"
																						<?php }else {?>
																						style="display: none; color:red;"
																						<?php }?>
																						
																						><?php if (isset($email_exists_error) && $email_exists_error == 1){
																								echo EMAIL_IS_EXIST;
																						 }?></span>
																				</div>
																			</div>				
																			<div class="form-group">
																				<label class="col-md-3 control-label"><?php echo CONTACT_PERSON_NAME; ?><span class="required"> * </span></label>
																				<div class="col-md-4">
																					<input type="text" class="form-control "
																						name="contact_person_name" id="contact_person_name"
																						value="<?php echo $contact_person_name?>"
																						onfocus="javascript:removeTooltip('contact_person_name_tooltip');">
																					<span id="contact_person_name_tooltip"
																						class="tooltiptext" style="display: none; color:red"><?php echo FILL_THIS_FIELD; ?></span>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-md-3 control-label"><?php echo POSITION; ?></label>
																				<div class="col-md-4">
																					<input type="text" class="form-control "
																						name="position" value="<?php echo $position?>">
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-md-3 control-label"><?php echo SUBSCRIBTION_START_DATE; ?></label>
																				<div class="col-md-4">
																					<input type="date" class="form-control "
																						name="subscription_start_date"
																						value="<?php echo $subscription_start_date?>">
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-md-3 control-label"><?php echo SUBSCRIBTION_END_DATE; ?></label>
																				<div class="col-md-4">
																					<input type="date" class="form-control "
																						name="subscription_end_date"
																						value="<?php echo $subscription_end_date?>">
																				</div>
																			</div>
																		</div>
												
																		<div class="margin-top-10">
																			<button type="submit" class="btn green"><?php echo SAVE ?></button>
																		 </div>
												
																		</form>
																		<!-- END FORM-->
                                                                        </div>
                                                                        <!-- END OFFICE INFO TAB -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- END PROFILE CONTENT -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END PAGE CONTENT INNER -->
                                </div>
                            </div>
                            <!-- END PAGE CONTENT BODY -->
                            <!-- END CONTENT BODY -->
                        </div>
                        <!-- END CONTENT -->
                       
                    </div>
                    <!-- END CONTAINER -->
                </div>
            </div>
           
        </div>
        <?php $this->load->view('utils/footer');?>
       
<script type="text/javascript">
	function changeAvatar() {
		document.getElementById('avatarForm').submit();
	}
	
	function cancelAvatarChange(img_src) {
		$("#img_avatar").attr("src", img_src);

	}
	
	function changePassword() {
		document.getElementById('passwordForm').submit();
	}
	
	function checkPassword(){
	
	var old_password= document.forms["passwordForm"]["old_password"].value;
    	var new_password= document.forms["passwordForm"]["new_password"].value;
    	var retyped_new_password= document.forms["passwordForm"]["retyped_new_password"].value;
    	
    	if (old_password== "") {
    		$("#old_password_tooltip").css("display","block");
    		$("#old_password_tooltip").css("visibility","visible");
    		return false;
    	} else{
    		$("#old_password_tooltip").css("display","none");
    		$("#old_password_tooltip").css("visibility","none");
    	}
    	if (new_password== "") {
    		$("#new_password_tooltip").css("display","block");
    		$("#new_password_tooltip").css("visibility","visible");
    		return false;
    	} else{
    		$("#new_password_tooltip").css("display","none");
    		$("#new_password_tooltip").css("visibility","none");
    	}
    	if (retyped_new_password== "") {
    		$("#retyped_new_password_tooltip").css("display","block");
    		$("#retyped_new_password_tooltip").css("visibility","visible");
    		return false;
    	} else{
    		$("#retyped_new_password_tooltip").css("display","none");
    		$("#retyped_new_password_tooltip").css("visibility","none");
    	}
    	
		document.getElementById('incorrect_old_password').style.display = 'none';
		document.getElementById('incorrect_repeated_password').style.display = 'none';
		var saved_password = document.getElementById('saved_password').value;
		var old_password = document.getElementById('old_password').value;
		var new_password = document.getElementById('new_password').value;
		var retyped_new_password = document.getElementById('retyped_new_password').value;
		if(saved_password != old_password){
			document.getElementById('incorrect_old_password').style.display = 'block';
			return false;
		} else if (new_password != retyped_new_password) {
			document.getElementById('incorrect_repeated_password').style.display = 'block';
			return false;
		}
		return true;
	}

</script>
       
    
<!-- BEGIN CORE PLUGINS -->
        <script src="<?=base_url()?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?=base_url()?>assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?=base_url()?>assets/pages/scripts/profile.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
        
    </body>

</html>