<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<?php
session_start ();
$this->load->view ( 'localization/lang' );
$this->load->view ( 'localization/txt' );
$this->load->helper('url');
defineLocale ();
defineStrings ();
?>
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<script type="text/javascript">
function checkSubmit(e) {
	   if(e && e.keyCode == 13) {
	      document.forms[0].submit();
	   }
	}
function forgetPassword() {
	var username = document.forms["login_form"]["username"].value;
	if(username == ""){
		$('#username_first').css("display","block");
	} else {
		$('#username_first').css("display","none");
		$.ajax({  
	 		type    : "POST",  
	 		url     : "<?=site_url('user_login/resendPassword')?>",  
	 		data    : {
	     	username: username
	 	},
	 	success : function(data){
	 	if(data != ""){
			$('#username_first').css("display","block");
				
	 	} else {
	 		$('#username_first').css("display","none");
		 	alert("<? echo PASSWORD_SENT ?>");
	 	}
	 	}
		});
	
}
}
</script>
<meta charset="utf-8" />
<title><?php echo TITLE; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="Preview page of Metronic Admin RTL Theme #3 for "
	name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link
	href="<?=base_url()?>assets/global/plugins/select2/css/select2.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/select2/css/select2-bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?=base_url()?>assets/global/css/components-rtl.min.css"
	rel="stylesheet" id="style_components" type="text/css" />
<link href="<?=base_url()?>assets/global/css/plugins-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?=base_url()?>assets/pages/css/login-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon"
	href="<?=base_url()?>images/logohighquality.png" />

<link rel="manifest" href="<?=base_url()?>assets/manifest.json">
<!--  <script src="https://www.gstatic.com/firebasejs/3.7.0/firebase.js"></script>
<script src="https://www.gstatic.com/firebasejs/3.6.10/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/3.6.10/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/3.6.10/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/3.6.10/firebase-messaging.js"></script>

<script>
// Initialize Firebase
  	var config = {
	    apiKey: "AIzaSyAeKBM3JjFmqSFuKWzdKfjXhLe4KV4MdNI",
	    authDomain: "court-case-management.firebaseapp.com",
	    databaseURL: "https://court-case-management.firebaseio.com",
	    storageBucket: "court-case-management.appspot.com",
	    messagingSenderId: "188245098393"
	  };
	  firebase.initializeApp(config);
const messaging = firebase.messaging();
messaging.requestPermission()
.then(function() {
 // alert('Notification permission granted.');
  // TODO(developer): Retrieve an Instance ID token for use with FCM.
  resetUI();
})
.catch(function(err) {
 // alert('Unable to get permission to notify.');
});
function resetUI() {
	    // [START get_token]
	    // Get Instance ID token. Initially this makes a network call, once retrieved
	    // subsequent calls to getToken will return from cache.
	    messaging.getToken()
	    .then(function(currentToken) {
	      if (currentToken) {
		     // alert ($("#token").text());
		      $("#token").val(currentToken);
		    //  alert ($("#token").text());
	      } else {
	        // Show permission request.
	        //alert('No Instance ID token available. Request permission to generate one.');
	      }
	    })
	    .catch(function(err) {
	  //    alert('An error occurred while retrieving token. ');
	    });
	  }
</script>-->
</head>
<!-- END HEAD -->

<body class=" login">
	<!-- BEGIN LOGIN -->
	<div class="content">
		<div style="text-align: center;">
			<img src="<?=base_url()?>images/logohighquality.png"
				class="logo-default">

		</div>
		<!-- BEGIN LOGIN FORM -->
		<form role="form" method="post" id="login_form"
			action="<?=site_url('user_login/login')?>"
			onKeyPress="return checkSubmit(event)">
			<h3 class="form-title font-green"><?php echo SIGN_IN; ?></h3>
			<div class="alert alert-danger display-hide">
				<button class="close" data-close="alert"></button>
				<span> <?php echo INSERT_USERNAME_PASSWORD; ?> </span>
			</div>
			<input id="token" type="hidden" name="token">
			<div class="form-group">
				<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
				<label class="control-label visible-ie8 visible-ie9"
					spellcheck="false"><?php echo USERNAME; ?></label> <input
					class="form-control form-control-solid placeholder-no-fix"
					id="username" type="text" autocomplete="off"
					placeholder="<?php echo USERNAME; ?>" name="user_name" />
			</div>
			<div class="form-group">
				<label class="control-label visible-ie8 visible-ie9"><?php echo PASSWORD; ?></label>
				<input class="form-control form-control-solid placeholder-no-fix"
					type="password" autocomplete="off"
					placeholder="<?php echo PASSWORD; ?>" name="password" />
			</div>

			<div class="form-group" <?php if (isset($wrong) && $wrong!= ""){?>
				style="display: block; color: red;" <?php }else {?>
				style="display: none; color:red;" <?php }?>>
																				<?php echo INVALID_USERNAME_PASSWORD;?>
																			</div>
			<div class="form-group" style="display: none; color: red;"
				id="username_first">
																				<?php echo USERNAME_FIRST;?>
																			</div>

			<div class="form-actions" style="text-align: center">
				<button type="submit" class="btn green uppercase"><?php echo LOGIN; ?></button>
				<br>
				<div style="text-align: center; padding: 5px">
					<a href="<?=site_url('user_login/resendPassword')?>" class="forget-password"> <?php echo FORGET_PASSWORD;?></a>
				</div>
			</div>
			<br>
			<div style="text-align: center">
				<a href="<?=site_url('help/index/')?>"> <?php echo OPEN_HELP; ?></a>
			</div>
			<br />
			<div style="text-align: center">
				<a
					href="https://play.google.com/store/apps/details?id=com.soomline.courtcasemanagement" style="text-decoration: none">
					<img src="<?=base_url()?>images/ic_google_play.png"
					class="logo-default" style="width: 120px; height: 48px;">
				</a>
				<a
					href="https://itunes.apple.com/sa/app/%D8%A7%D9%84%D8%AA%D9%86%D9%81%D9%8A%D8%B0/id1291674330?mt=8&ign-mpt=uo%3D4" style="text-decoration: none">
					<img src="<?=base_url()?>images/app-store.png"
					class="logo-default" style="width: 120px; height: 48px;">
				</a>
			</div>
		</form>

		<!-- END LOGIN FORM -->
	</div>
	<!--[if lt IE 9]>
<script src="<?=base_url()?>assets/global/plugins/respond.min.js"></script>
<script src="<?=base_url()?>assets/global/plugins/excanvas.min.js"></script> 
<script src="<?=base_url()?>assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
	<!-- BEGIN CORE PLUGINS -->
	<script src="<?=base_url()?>assets/global/plugins/jquery.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-validation/js/additional-methods.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/select2/js/select2.full.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="<?=base_url()?>assets/global/scripts/app.min.js"
		type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?=base_url()?>assets/pages/scripts/login.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<!-- END THEME LAYOUT SCRIPTS -->
</body>

</html>