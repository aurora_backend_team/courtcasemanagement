<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title><?php echo TITLE; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin RTL Theme #3 for dashboard & statistics"
	name="description" />
<meta content="" name="author" />

<link rel="shortcut icon"
	href="<?=base_url()?>images/logohighquality.png" />

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
    $("#delBtn").click(function(){
        $("#delNotify").show();
    });
    $("#cancel").click(function(){
        $("#delNotify").hide();
    });
    $("#delNotify").hide();
});
</script>

</head>
<!-- END HEAD -->

<body class="page-container-bg-solid">
	<div class="page-wrapper-row">
		<div class="page-wrapper-top">
			<!-- BEGIN HEADER -->
			<div class="page-header">
<?php $this->load->view('office_admin/header');?>
				<!-- BEGIN HEADER MENU -->
				<div class="page-header-menu">
					<div class="container">
						<!-- BEGIN MEGA MENU -->
						<!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
						<!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
						<div class="hor-menu  ">
							<ul class="nav navbar-nav">
															
								<?php if (isset($cases) || isset($case_details)|| isset($search_data)) {?>
									<li aria-haspopup="true"
										class="menu-dropdown classic-menu-dropdown active">
									<?php } else {?>	
									<li aria-haspopup="true"
										class="menu-dropdown classic-menu-dropdown">
										<?php } ?><a
										href="<?=site_url('case_management')?>"> <?php echo MY_CASES; ?> <span class="arrow"></span>
									</a></li>
									
								
								<?php if (isset($user_type_code) && $user_type_code == "LAWYER") {?>
									<?php if (isset($dashboardReports)) {?>
									<li aria-haspopup="true"
										class="menu-dropdown classic-menu-dropdown active">
									<?php } else {?>	
									<li aria-haspopup="true"
										class="menu-dropdown classic-menu-dropdown">
										<?php } ?><a
										href="<?=site_url('dashboard_reports/view_dashboard_reports/3')?>"> <?php echo DASHBOARD_REPORTS; ?> <span class="arrow"></span>
									</a></li>
								<?php } ?>
								
							</ul>
						</div>
						<!-- END MEGA MENU -->
					</div>
				</div>
				<!-- END HEADER MENU -->
			</div>
			<!-- END HEADER -->
		</div>
	</div>
	<br>
</body>