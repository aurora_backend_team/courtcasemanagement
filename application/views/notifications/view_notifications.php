
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<?php
session_start ();
$this->load->view ( 'localization/lang' );
$this->load->view ( 'localization/txt' );
defineLocale ();
defineStrings ();
function set_code($code) {
	$GLOBALS ['code'] = $code;
}
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title><?php echo TITLE; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin RTL Theme #3 for dashboard & statistics"
	name="description" />
<meta content="" name="author" />

<link rel="shortcut icon"
	href="<?=base_url()?>assets/layouts/layout3/img/logo-default.jpg" />
<script type="text/javascript">
function checkActive (checkboxElem,notification_code) {
	  if (checkboxElem.checked) {
		  update_notification_active_value (notification_code,"Y");
	  } else {
		  update_notification_active_value (notification_code,"N");
	  }
	}
function update_notification_active_value (notification_code,active){
	$.ajax({  
 		type    : "POST",  
 		url     : "<?=site_url('notifications/update_notification_active_value' )?>",  
 		data    : {
 		notification_code: notification_code,
 		active: active
 	},
 	complete : function(data){
     	console.log(data);
 	}
	});
}
function checkSMSActive (checkboxElem,notification_code) {
	  if (checkboxElem.checked) {
		  update_notification_sms_active_value (notification_code,"Y");
	  } else {
		  update_notification_sms_active_value (notification_code,"N");
	  }
	}
function update_notification_sms_active_value (notification_code,sms_active){
	$.ajax({  
		type    : "POST",  
		url     : "<?=site_url('notifications/update_notification_sms_active_value' )?>",  
		data    : {
		notification_code: notification_code,
		sms_active: sms_active
	},
	complete : function(data){
   	console.log(data);
	}
	});
}
</script>
</head>
<!-- END HEAD -->

<body class="page-container-bg-solid">
	<div class="page-wrapper">
<?php
$this->load->view ( 'office_admin/office_admin_header' );
?>

<div class="container">

			<!-- BEGIN BORDERED TABLE PORTLET-->
			<div class="page-content-inner">
				<div class="row">
					<div class="col-md-13">
						<div class="portlet light portlet-fit ">
							<div class="portlet-title">
								<div class="caption">
									<span class="caption-subject font-dark bold uppercase"><?php echo NOTIFICATIONS; ?></span>
								</div>
							</div>
						</div>
						<div style="overflow-x:scroll !important;-webkit-overflow-scrolling: touch !important;" >
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<th><?php echo NOTIFICATION_NAME; ?></th>
										<th><?php echo CASE_STATE; ?></th>
										<th><?php echo RECIEVER; ?></th>
										<th><?php echo SEND_TIME; ?></th>
										<th><?php echo EMAIL_ACTIVE; ?></th>
										<th><?php echo SMS_ACTIVE; ?></th>
										<th></th>
									</tr>
								</thead>
					<?php
					if ($notifications) {
						for($i = 0; $i < count ( $notifications ); $i ++) {
							?>
									<tr>
									<td><?=$notifications[$i] ["notification_name"];?> </td>
									<td>
									<?php if($notifications[$i] ["notification_code"] != 'M008' && $notifications[$i] ["notification_code"] != 'M009' && $notifications[$i] ["notification_code"] != 'M010'){?>
									<span
										class=<? if ( $notifications[$i] ["state_code"]=="INITIAL"){?>
										"label label-sm
										label-new"
										<? }else if ( $notifications[$i] ["state_code"]=="ASSIGNED_TO_LAWYER"){?>
										"label
										label-sm
										label-under-action"
										<? }else if ( $notifications[$i] ["state_code"]=="CLOSED"){?>
										"label
										label-sm
										label-danger"
										<? }else if ( $notifications[$i] ["state_code"]=="DECISION_34"){?>
										"label
										label-sm
										label-dec-four"
										<? }else if ( $notifications[$i] ["state_code"]=="DECISION_46"){?>
										"label
										label-sm
										label-dec-six"
										<? }else if ( $notifications[$i] ["state_code"]=="MODIFICATION_REQUIRED"){?>
										"label
										label-sm
										label-modf-req"
										<? }else if ( $notifications[$i] ["state_code"]=="PUBLICLY_ADVERTISED"){?>
										"label
										label-sm
										label-adv"
										<? }else if ( $notifications[$i] ["state_code"]=="REGISTERED"){?>
										"label
										label-sm
										label-exec"
										<? }else if ( $notifications[$i] ["state_code"]=="UNDER_REVIEW"){?>
										"label
										label-sm label-under-rev"
										<?}?>> <?=$notifications[$i] ["state_name"];?></span>
										<?php }?></td>
									<td><?=$notifications[$i] ["user_type_name"];?> </td>
									<td><?=$notifications[$i] ["send_time"];?> </td>
									<td><input type="checkbox" class="make-switch" onchange="checkActive(this,'<?= $notifications[$i] ["notification_code"] ?>')"
									<?php if($notifications[$i] ["active"] == "Y"){?>
													checked
												<?php }?>  
									data-on-color="primary" data-off-color="default"></td>
									<td><input type="checkbox" class="make-switch" onchange="checkSMSActive(this,'<?= $notifications[$i] ["notification_code"] ?>')"
									<?php if($notifications[$i] ["sms_active"] == "Y"){?>
													checked
												<?php }?>  
									data-on-color="primary" data-off-color="default"></td>
									<td><a
										href="<?=site_url('notifications/edit_notification/'.$notifications[$i] ["notification_code"])?>"
										class="btn btn-outline btn-circle btn-sm purple"> <i
											class="fa fa-edit"></i> <?php echo EDIT; ?>
														</a></td>
								
								</tr>	
						<?php
						}
					} else {
						?><tr class="HeadBr2">
									<td colspan="22" style="color: red" align="center"><h4>
									<?php echo NO_RECORDS_FOUND; ?>
									</h4></td>
								</tr> <?php }?>
						</tbody>
							</table>
						</div>
						<!-- END BORDERED TABLE PORTLET-->



					</div>

				</div>
			</div>
		</div>
	</div>

<?php $this->load->view('utils/footer');?>

<!-- BEGIN CORE PLUGINS -->
	<script src="<?=base_url()?>assets/global/plugins/jquery.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.sparkline.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="<?=base_url()?>assets/global/scripts/app.min.js"
		type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?=base_url()?>assets/pages/scripts/profile.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script
		src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js"
		type="text/javascript"></script>
	<!-- END THEME LAYOUT SCRIPTS -->

</body>

</html>
