<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<?php
session_start ();
$this->load->view ( 'localization/lang' );
$this->load->view ( 'localization/txt' );
defineLocale ();
defineStrings ();
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<script type="text/javascript">
function checkValid() {
	var city_name = document.forms["myForm"]["city_name"].value;
	if (city_name == "") {
		$("#city_name_tooltip").css("display","block");
		$("#city_name_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#city_name_tooltip").css("display","none");
		$("#city_name_tooltip").css("visibility","none");
	}
	$('#loading').removeClass('hidden');
}
function loading(){
	$('#loading').removeClass('hidden');
	return true;
}
</script>
<meta charset="utf-8" />
<title><?php echo TITLE; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin RTL Theme #3 for dashboard & statistics"
	name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link
	href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css"
	rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/global/plugins/morris/morris.css" rel="stylesheet"
	type="text/css" />
<link href="<?=base_url()?>assets/global/plugins/fullcalendar/fullcalendar.min.css"
	rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/jqvmap.css"
	rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?=base_url()?>assets/global/css/components-rtl.min.css"
	rel="stylesheet" id="style_components" type="text/css" />
<link href="<?=base_url()?>assets/global/css/plugins-rtl.min.css" rel="stylesheet"
	type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="<?=base_url()?>assets/layouts/layout3/css/layout-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/layouts/layout3/css/themes/default-rtl.min.css"
	rel="stylesheet" type="text/css" id="style_color" />
<link href="<?=base_url()?>assets/layouts/layout3/css/custom-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon"
	href="<?=base_url()?>assets/layouts/layout3/img/logo-default.jpg" />
</head>
<!-- END HEAD -->
<body class="page-container-bg-solid">
	<div class="page-wrapper">
<?php $this->load->view ( 'office_admin/office_admin_header' );?>
				<div class="page-wrapper-row full-height">
			<!-- BEGIN CONTAINER -->
			<div class="page-container">
				<!-- BEGIN CONTENT -->
				<div class="page-content-wrapper">
					<!-- BEGIN CONTENT BODY -->
					<!-- BEGIN PAGE HEAD-->
					<div class="page-head">
						<div class="container">
							<div class="col-md-10">
								<div class="portlet box green">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-gift"></i>
												<?php
												echo NOTIFICATION_DETAILS;
												?>
											</div>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
										<form id="myForm" role="form" method="post"
										onsubmit = "javascript:return loading();"
											action="<?=site_url('notifications/edit_notification/'.$notification_code)?>"
											class="form-horizontal">
											<div class="form-body">
												<div class="form-group">
													<label class="col-md-3 control-label"><?php echo NOTIFICATION_NAME; ?></label>
													<div class="col-md-4">
														<input readonly="readonly" type="text"
															class="form-control " value="<?= $notification_name?>">

													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label"><?php echo CASE_STATE; ?></label>
													<div class="col-md-4">
														<input readonly="readonly" type="text"
															class="form-control " value="<?= $state_name?>">

													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label"><?php echo RECIEVER; ?></label>
													<div class="col-md-4">
														<input readonly="readonly" type="text"
															class="form-control " value="<?= $user_type_name?>">

													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label"><?php echo SEND_TIME; ?></label>
													<div class="col-pb-2">
														<select class="form-control " name="send_time"
															class="form-control">
					  	<?php
								for($i = 0; $i <= 30; $i ++) {
									?>
							<option value="<?=$i?>" <?php if ($send_time == $i){?>
																selected="selected" <?php }?>> <?=$i?></option>
						<?php
								}
								?>		
					</select>

													</div>
													<label class="col-md-2 control-label"><?php echo DAY; ?></label>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label"><?php echo EMAIL_ACTIVE; ?> </label>
													<div class="col-md-4">
														<table style="width: 60%">
															<tr>
																<td><input type="radio" name="active" id="active"
																	value="Y" <?php if($active == 'Y'){?> checked <?php }?>></td>
																<td><?php echo YES; ?></td>
																<td><input type="radio" name="active" id="active"
																	value="N" <?php if($active == 'N'){?> checked <?php }?>></td>
																<td><?php echo NO; ?></td>
															</tr>
														</table>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label"><?php echo SMS_ACTIVE; ?> </label>
													<div class="col-md-4">
														<table style="width: 60%">
															<tr>
																<td><input type="radio" name="sms_active" id="sms_active"
																	value="Y" <?php if($sms_active == 'Y'){?> checked <?php }?>></td>
																<td><?php echo YES; ?></td>
																<td><input type="radio" name="sms_active" id="sms_active"
																	value="N" <?php if($sms_active == 'N'){?> checked <?php }?>></td>
																<td><?php echo NO; ?></td>
															</tr>
														</table>
													</div>
												</div>
											</div>
											<div class="form-actions">
												<div class="row">
													<div class="col-md-offset-3 col-md-9">
														<button type="submit" class="btn btn-circle green"><?php
														echo EDIT;
														?></button>
														<a
															href="<?=site_url('notifications/view_notifications')?>"
															class="btn btn-outline btn-circle grey-salsa"><?php echo CANCEL; ?>
														</a>
													</div>
												</div>
											</div>
										</form>
										<!-- END FORM-->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
        <?php $this->load->view('utils/footer');?>
	<!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<script src="../assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
	<script src="<?=base_url()?>assets/global/plugins/jquery.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.sparkline.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="<?=base_url()?>assets/global/scripts/app.min.js"
		type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?=base_url()?>assets/pages/scripts/profile.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script
		src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js"
		type="text/javascript"></script>
	<!-- END THEME LAYOUT SCRIPTS -->

</body>

</html>