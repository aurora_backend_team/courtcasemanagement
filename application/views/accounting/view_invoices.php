
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<?php
session_start ();
$this->load->view ( 'localization/lang' );
$this->load->view ( 'localization/txt' );
defineLocale ();
defineStrings ();
function set_code($code) {
	$GLOBALS ['code'] = $code;
}
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title><?php echo TITLE; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin RTL Theme #3 for dashboard & statistics"
	name="description" />
<meta content="" name="author" />

<link rel="shortcut icon"
	href="<?=base_url()?>assets/layouts/layout3/img/logo-default.jpg" />

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">
function checkPaid (checkboxElem,invoice_id) {
	  if (checkboxElem.checked) {
		  update_is_paid_value (invoice_id,"Y");
	  } else {
		  update_is_paid_value (invoice_id,"N");
	  }
	}

function update_is_paid_value (invoice_id,is_paid){
	$.ajax({  
 		type    : "POST",  
 		url     : "<?=site_url('invoices/update_is_paid_value' )?>",  
 		data    : {
 			invoice_id: invoice_id,
 			is_paid: is_paid
 	},
 	complete : function(data){
     	console.log(data);
 	}
	});
}

function ajaxUpdateInvoices(newpage){
	var postdata = {'current_page' : newpage};
	var url = "<?php echo site_url('invoices/get_invoices'.'/'.$user_id_param)?>";
	$.post(url, postdata, function(data) {
            var results = JSON.parse(data);
            var newSearchArea = $(results).find("#data_div");
            $("#data_div").html(newSearchArea);	
            $('.make-switch')['bootstrapSwitch']();
    });
}

function next_page(){
	var current_page = parseInt($("#calculated_current_page").val()) ;
	var newpage = (current_page+1);
	$("#calculated_current_page").val(newpage);
	ajaxUpdateInvoices(newpage);
}

function prev_page(){
	var current_page = parseInt($("#calculated_current_page").val()) ;
	var newpage = (current_page-1);
	$("#calculated_current_page").val(newpage);
	ajaxUpdateInvoices(newpage);
}

$(document).on('keyup','#desired_page_num',function(event) { 
    if(event.keyCode == 13){
		var desired_page_num  = $('#desired_page_num').val();
		$("#calculated_current_page").val(desired_page_num);
		ajaxUpdateInvoices(desired_page_num);
    }
});

</script>
</head>
<!-- END HEAD -->

<body class="page-container-bg-solid">
	<div class="page-wrapper">
<?php
$this->load->view ( 'office_admin/office_admin_header' );
?>

<div class="container">

			<!-- BEGIN BORDERED TABLE PORTLET-->
			<div class="page-content-inner">
				<div class="row">
					<div class="col-md-13">
						<div class="portlet light portlet-fit ">
							<div class="portlet-title">
								<div class="caption">
									<span class="caption-subject font-dark bold uppercase"><?php echo DISPLAY_INVOICE; ?></span>
								</div>
							</div>
						</div>
						
						<div id="data_div">
						
							<input type="hidden" name="calculated_current_page"
								id="calculated_current_page" value="<?php echo $current_page;?>">
							
						
							<div style="overflow-x:scroll !important;-webkit-overflow-scrolling: touch !important;" >
								<table class="table table-bordered table-hover">
									<thead>
										<tr>
											<th><?php echo TRNS_NUMBER; ?></th>
											<th><?php echo DATE; ?></th>
											<th><?php echo CUSTOMER_NAME; ?></th>
											<th><?php echo LAWYER_NAME; ?></th>
											<th><?php echo CASE_NUM; ?></th>
											<th><?php echo CASE_STATE; ?></th>
											<th><?php echo VALUE_SR; ?></th>
											<th><?php echo PAIED; ?></th>
											<th></th>
										</tr>
									</thead>
						<?php
						if ($invoices) {
							for($i = 0; $i < count ( $invoices ); $i ++) {
								?>
										<tr>
										<td><?=$invoices[$i] ["invoice_id"];?> </td>
										<td><?=$invoices[$i] ["invoice_date"];?> </td>
										<td><?=$invoices[$i] ["customer_first_name"].' '.$invoices[$i] ["customer_last_name"];?> </td>
										<td><?=$invoices[$i] ["lawyer_first_name"].' '.$invoices[$i] ["lawyer_last_name"];?> </td>
										<td><?=$invoices[$i] ["case_id"];?> </td>
										<td><span
											class=<? if ( $invoices[$i] ["case_state_code"]=="INITIAL"){?>
											"label label-sm
											label-new"
											<? }else if ( $invoices[$i] ["case_state_code"]=="ASSIGNED_TO_LAWYER"){?>
											"label
											label-sm
											label-under-action"
											<? }else if ( $invoices[$i] ["case_state_code"]=="CLOSED"){?>
											"label
											label-sm
											label-danger"
											<? }else if ( $invoices[$i] ["case_state_code"]=="DECISION_34"){?>
											"label
											label-sm
											label-dec-four"
											<? }else if ( $invoices[$i] ["case_state_code"]=="DECISION_46"){?>
											"label
											label-sm
											label-dec-six"
											<? }else if ( $invoices[$i] ["case_state_code"]=="MODIFICATION_REQUIRED"){?>
											"label
											label-sm
											label-modf-req"
											<? }else if ( $invoices[$i] ["case_state_code"]=="PUBLICLY_ADVERTISED"){?>
											"label
											label-sm
											label-adv"
											<? }else if ( $invoices[$i] ["case_state_code"]=="REGISTERED"){?>
											"label
											label-sm
											label-exec"
											<? }else if ( $invoices[$i] ["case_state_code"]=="UNDER_REVIEW"){?>
											"label
											label-sm label-under-rev"
											<?}?>> <?=$invoices[$i] ["state_name"];?></span></td>
										<td><?=$invoices[$i] ["billing_value"];?> </td>
										<td><input type="checkbox" class="make-switch" onchange="checkPaid(this,'<?= $invoices[$i] ["invoice_id"]?>')"
										<?php if($invoices[$i] ["is_paid"] == "Y"){?>
														checked
													<?php }?>   
										data-on-color="primary" data-off-color="default"></td> <!-- is_paid -->
										<td><a
											href="<?=site_url('invoices/edit_invoice/'.$invoices[$i] ["invoice_id"])?>"
											class="btn btn-outline btn-circle btn-sm purple"> <i
												class="fa fa-edit"></i> <?php echo DETAILS; ?>
															</a></td>
									
									</tr>	
							<?php
							}
						} else {
							?><tr class="HeadBr2">
										<td colspan="22" style="color: red" align="center"><h4>
										<?php echo NO_RECORDS_FOUND; ?>
										</h4></td>
									</tr> <?php }?>
							</tbody>
								</table>
							</div>
							<!-- END BORDERED TABLE PORTLET-->
						
						<br>
							
						<?php if ($invoices) {?>
								<div id="paginationForm">
									<div style="text-align: center; margin: auto;">
										<input type="button" class="btn btn-circle green"
											onClick="javascript:prev_page()" <?php if(!$has_prev){?>
											disabled <?php }?> value="<?php echo PREVIOUS ;?>" />
											<span
											style="margin-left: 5px; margin-right: 5px; text-align: center;">
											<b><?php echo PAGE_NUM. ": "; ?></b>
											<input type="text" act="yes" onkeydown="numberKeyDown(event)" 
												class="no-spinners"
												id="desired_page_num" 
												name="desired_page_num"
												style="width:40px;"
												value="<?php echo $current_page?>"> 
											<b><?php echo ' / '.$total_pages_num; ?></b>
											</span>
										<input type="button" class="btn btn-circle green"
											onClick="javascript:next_page()" <?php if(!$has_next){?>
											disabled <?php }?> value="<?php echo NEXT ;?>" />
									</div>
									
									<div style="margin: auto; text-align: left;"><h5><b><?php echo DISPLAYED_NUMBER_OF_FILES. ": ".$current_page_size." ". FROM." ".$total_files_num; ?></b></h5></div>
								
								</div>
							<?php }?>
							
							<br>
					</div>


					</div>

				</div>
			</div>
		</div>
	</div>

<?php $this->load->view('utils/footer');?>

<!-- BEGIN CORE PLUGINS -->
	<script src="<?=base_url()?>assets/global/plugins/jquery.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.sparkline.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="<?=base_url()?>assets/global/scripts/app.min.js"
		type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?=base_url()?>assets/pages/scripts/profile.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script
		src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js"
		type="text/javascript"></script>
	<!-- END THEME LAYOUT SCRIPTS -->

</body>

</html>
