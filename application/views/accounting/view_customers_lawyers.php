<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<?php
session_start ();
$this->load->view ( 'localization/lang' );
$this->load->view ( 'localization/txt' );
defineLocale ();
defineStrings ();
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title><?php echo TITLE; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin RTL Theme #3 for dashboard & statistics"
	name="description" />
<meta content="" name="author" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin RTL Theme #3 for dashboard & statistics"
	name="description" />
<meta content="" name="author" />

<link rel="shortcut icon"
	href="<?=base_url()?>assets/layouts/layout3/img/logo-default.jpg" />

</head>
<!-- END HEAD -->

<body class="page-container-bg-solid">
	<div class="page-wrapper">
<?php $this->load->view ( 'office_admin/office_admin_header' );?>
		<div class="col-md-6 full-height">
			<div class="page-wrapper-middle">
				<!-- BEGIN CONTAINER -->
				<div class="page-container">
					<!-- BEGIN CONTENT -->
					<div class="page-content-wrapper">
						<!-- BEGIN CONTENT BODY -->
						<!-- BEGIN PAGE HEAD-->
						<div class="page-head">
							<div class="container">

								<div class="col-md-15">
									<!-- BEGIN BORDERED TABLE PORTLET-->
									<div class="portlet light portlet-fit ">
										<div class="portlet-title">
											<div class="caption">
												<span class="caption-subject font-dark bold uppercase"><?php echo LAWYER_CUSTOMER_INVOICES; ?></span>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-5">
											<!-- Customers -->
											<table class="table table-bordered table-hover">
												<thead>
													<tr>
														<th><?php echo CUSTOMER_NAME; ?></th>
														<th></th>
													</tr>
												</thead>
												<tbody>
													<tr>
												<?php
												if ($customers) {
													for($i = 0; $i < count ( $customers ); $i ++) {
														?>
													<td><?=$customers[$i] ["first_name"].' '.$customers[$i] ["last_name"]?></td>
														<td><a
															href="<?=site_url('invoices/get_invoices/'.$customers[$i] ["user_id"])?>"
															class="btn btn-outline btn-circle btn-sm purple"> <i
																class="fa fa-edit"></i> <?php echo DISPLAY_INVOICE; ?>
														</a></td>
													</tr>	
											<?php
													}
												} else {
													?><tr class="HeadBr2">
														<td colspan="16" style="color: red" align="center"><h4>
												<?php echo NO_RECORDS_FOUND; ?>
												</h4></td>
													</tr> <?php }?>
											</tbody>
											</table>
										</div>
										<div class="col-md-5"></div>
										<div class="col-md-5">
											<!-- Lawyers -->
											<table class="table table-bordered table-hover">
												<thead>
													<tr>
														<th><?php echo LAWYER_NAME; ?></th>
														<th></th>
													</tr>
												</thead>
												<tbody>
													<tr>
												<?php
												if ($lawyers) {
													for($i = 0; $i < count ( $lawyers ); $i ++) {
														?>
													<td><?=$lawyers[$i] ["first_name"].' '.$lawyers[$i] ["last_name"]?></td>
														<td><a
															href="<?=site_url('invoices/get_invoices/'.$lawyers[$i] ["user_id"])?>"
															class="btn btn-outline btn-circle btn-sm purple"> <i
																class="fa fa-edit"></i> <?php echo DISPLAY_INVOICE; ?>
															</a></td>
													</tr>	
											<?php
													}
												} else {
													?><tr class="HeadBr2">
														<td colspan="16" style="color: red" align="center"><h4>
												<?php echo NO_RECORDS_FOUND; ?>
												</h4></td>
													</tr> <?php }?>
											</tbody>
											</table>
										</div>
									</div>
									<!-- END BORDERED TABLE PORTLET-->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php $this->load->view('utils/footer');?>
	<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<script src="assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
	<!-- BEGIN CORE PLUGINS -->
		<script src="<?=base_url()?>assets/global/plugins/jquery.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery.sparkline.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="<?=base_url()?>assets/global/scripts/app.min.js"
		type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?=base_url()?>assets/pages/scripts/profile.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script
		src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js"
		type="text/javascript"></script>
	<!-- END THEME LAYOUT SCRIPTS -->
</body>

</html>