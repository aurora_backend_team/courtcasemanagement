<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<?php
//session_start ();
$this->load->view ( 'localization/lang' );
$this->load->view ( 'localization/txt' );
defineLocale ();
defineStrings ();
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>

<script type="text/javascript">
	
function checkValid() {
	var contract_number= document.forms["myForm"]["contract_number"].value;
	var client_id_number= document.forms["myForm"]["client_id_number"].value;
	var client_type= document.forms["myForm"]["client_type"].value;
	var number_of_late_instalments= document.forms["myForm"]["number_of_late_instalments"].value;
	var due_amount= document.forms["myForm"]["due_amount"].value;
	var remaining_amount= document.forms["myForm"]["remaining_amount"].value;
	
	if (contract_number== "" || contract_number<= 0) {
		$("#contract_number_tooltip").css("display","block");
		$("#contract_number_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#contract_number_tooltip").css("display","none");
		$("#contract_number_tooltip").css("visibility","none");
	}
	if (client_id_number== "" || client_id_number<= 0) {
		$("#client_id_number_tooltip").css("display","block");
		$("#client_id_number_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#client_id_number_tooltip").css("display","none");
		$("#client_id_number_tooltip").css("visibility","none");
	}
	if (client_type== "" ) {
		$("#client_type_tooltip").css("display","block");
		$("#client_type_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#client_type_tooltip").css("display","none");
		$("#client_type_tooltip").css("visibility","none");
	}
	if (number_of_late_instalments== "" || number_of_late_instalments<= 0) {
		$("#number_of_late_instalments_tooltip").css("display","block");
		$("#number_of_late_instalments_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#number_of_late_instalments_tooltip").css("display","none");
		$("#number_of_late_instalments_tooltip").css("visibility","none");
	}
	if (due_amount== "" || due_amount<= 0) {
		$("#due_amount_tooltip").css("display","block");
		$("#due_amount_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#due_amount_tooltip").css("display","none");
		$("#due_amount_tooltip").css("visibility","none");
	}
	if (remaining_amount== "" || remaining_amount<= 0) {
		$("#remaining_amount_tooltip").css("display","block");
		$("#remaining_amount_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#remaining_amount_tooltip").css("display","none");
		$("#remaining_amount_tooltip").css("visibility","none");
	}


	
}

</script>
<meta charset="utf-8" />
<title><?php echo TITLE; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin RTL Theme #3 for dashboard & statistics"
	name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link
	href="<?=base_url()?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css"
	rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/global/plugins/morris/morris.css" rel="stylesheet"
	type="text/css" />
<link href="<?=base_url()?>assets/global/plugins/fullcalendar/fullcalendar.min.css"
	rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/jqvmap.css"
	rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?=base_url()?>assets/global/css/components-rtl.min.css"
	rel="stylesheet" id="style_components" type="text/css" />
<link href="<?=base_url()?>assets/global/css/plugins-rtl.min.css" rel="stylesheet"
	type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="<?=base_url()?>assets/layouts/layout3/css/layout-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/layouts/layout3/css/themes/default-rtl.min.css"
	rel="stylesheet" type="text/css" id="style_color" />
<link href="<?=base_url()?>assets/layouts/layout3/css/custom-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<body class="page-container-bg-solid">
	
									<div class="portlet box green">
										<div class="portlet-title">
											<div class="tools">
												<a href="javascript:;" class="collapse"> </a> <a
													href="#portlet-config" data-toggle="modal" class="config">
												</a> <a href="javascript:;" class="reload"> </a>
											</div>
										</div>
										<div class="portlet-body form">
										<?php if($action_code == "ASSIGN_TO_LAWYER"){?>
											<!-- BEGIN FORM-->
											<form id="myForm" role="form" method="post"
												onsubmit="javascript:return checkValid();"
												action="case_management/add_case/"
												class="form-horizontal">
												<div class="form-body">
<div class="form-group">
														<label class="col-md-3 control-label"><?php echo DEBENTURE; ?></label>
														<div class="col-md-4">
														<input  type="file"
																class="form-control input-circle"
																name="debenture">
													</div>

			
													<div class="form-actions">
														<div class="row">
															<div class="col-md-offset-3 col-md-9">
																<button type="submit" class="btn btn-circle green"><?php
																	echo ASSIGNED_TO_LAWYER;
																
																?></button>
															</div>
														</div>
													</div>
											
											</form>
											<!-- END FORM-->
											<?php }else if($action_code == "REGISTER"){?>
													<!-- BEGIN FORM-->
											<form id="myForm" role="form" method="post"
												onsubmit="javascript:return checkValid();"
												action="case_management/add_case/"
												class="form-horizontal">
												<div class="form-group">
														<label class="col-md-3 control-label"><?php echo EXECUTIVE_ORDER_NUMBER; ?></label>
														<div class="col-md-4">
															<input  type="number"
																class="form-control input-circle"
																name="executive_order_number"
																
														</div>
													</div>

			
													<div class="form-actions">
														<div class="row">
															<div class="col-md-offset-3 col-md-9">
																<button type="submit" class="btn btn-circle green"><?php
																	echo REGISTERED;
																
																?></button>
															</div>
														</div>
													</div>
											
											</form>
											<!-- END FORM-->
											<?php }else if($action_code == "MAKE_DECISION_34"){?>
											<!-- BEGIN FORM-->
											<form id="myForm" role="form" method="post"
												onsubmit="javascript:return checkValid();"
												action="case_management/add_case/"
												class="form-horizontal">
												<div class="form-group">
														<label class="col-md-3 control-label"><?php echo DECISION_34_NUMBER; ?></label>
														<div class="col-md-4">
															<input  type="number"
																class="form-control input-circle"
																name="decision_34_number">
																
														
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo DECISION_34_FILE; ?></label>
														<div class="col-md-4">
														<input  type="file"
																class="form-control input-circle"
																name="decision_34_file">
																
												
														</div>
													</div>

			
													<div class="form-actions">
														<div class="row">
															<div class="col-md-offset-3 col-md-9">
																<button type="submit" class="btn btn-circle green"><?php
																	echo DECISION_34;
																
																?></button>
															</div>
														</div>
													</div>
											
											</form>
											<!-- END FORM-->
											<?php }else if($action_code == "ADVERTISE"){?>
														<!-- BEGIN FORM-->
											<form id="myForm" role="form" method="post"
												onsubmit="javascript:return checkValid();"
												action="case_management/add_case/"
												class="form-horizontal">
												<div class="form-group">
														<div class="form-group">
														<label class="col-md-3 control-label"><?php echo ADVERTISEMENT_FILE; ?></label>
														<div class="col-md-4">
														<input  type="file"
																class="form-control input-circle"
																name="advertisement_file">
																
											
			
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo INVOICE_FILE; ?></label>
														<div class="col-md-4">
														<input  type="file"
																class="form-control input-circle"
																name="invoice_file">
																
															
														</div>
													</div>

			
													<div class="form-actions">
														<div class="row">
															<div class="col-md-offset-3 col-md-9">
																<button type="submit" class="btn btn-circle green"><?php
																	echo PUBLIC_ADVERTISED;
																
																?></button>
															</div>
														</div>
													</div>
											
											</form>
											<!-- END FORM-->
											<?php }else if($action_code == "MAKE_DECISION_46"){?>
											<!-- BEGIN FORM-->
											<form id="myForm" role="form" method="post"
												onsubmit="javascript:return checkValid();"
												action="case_management/add_case/"
												class="form-horizontal">
												<div class="form-group">
														<div class="form-group">
														<label class="col-md-3 control-label"><?php echo DECISION_46_STATUS; ?></label>
														<div class="col-md-4">
															<input  type="text"
																class="form-control input-circle"
																name="decision_46_status">
																
														</div>
													</div>

			
													<div class="form-actions">
														<div class="row">
															<div class="col-md-offset-3 col-md-9">
																<button type="submit" class="btn btn-circle green"><?php
																	echo DECISION_46;
																
																?></button>
															</div>
														</div>
													</div>
											
											</form>
											<!-- END FORM-->
											<?php }else if($action_code == "CLOSE"){?>
											<!-- BEGIN FORM-->
											<form id="myForm" role="form" method="post"
												onsubmit="javascript:return checkValid();"
												action="case_management/add_case/"
												class="form-horizontal">
												<div class="form-group">
														<div class="form-group">
														<label class="col-md-3 control-label"><?php echo CLOSING_REASON; ?></label>
														<div class="col-md-4">
															<input  type="text"
																class="form-control input-circle" name="closing_reason">
																
														</div>
													</div>

			
													<div class="form-actions">
														<div class="row">
															<div class="col-md-offset-3 col-md-9">
																<button type="submit" class="btn btn-circle green"><?php
																	echo CLOSED;
																
																?></button>
															</div>
														</div>
													</div>
											
											</form>
											<!-- END FORM-->
											<?php } ?>
										</div>
									</div>
								
	<!--[if lt IE 9]>
<script src="<?=base_url()?>assets/global/plugins/respond.min.js"></script>
<script src="<?=base_url()?>assets/global/plugins/excanvas.min.js"></script> 
<script src="<?=base_url()?>assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
	<!-- BEGIN CORE PLUGINS -->
	<script src="<?=base_url()?>assets/global/plugins/jquery.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/js.cookie.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="<?=base_url()?>assets/global/plugins/moment.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/morris/morris.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/morris/raphael-min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/counterup/jquery.waypoints.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/counterup/jquery.counterup.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/fullcalendar/fullcalendar.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.resize.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/flot/jquery.flot.categories.min.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/jquery.sparkline.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js"
		type="text/javascript"></script>
	<script
		src="<?=base_url()?>assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="<?=base_url()?>assets/global/scripts/app.min.js"
		type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?=base_url()?>assets/pages/scripts/dashboard.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script src="<?=base_url()?>assets/layouts/layout3/scripts/layout.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/layouts/layout3/scripts/demo.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js"
		type="text/javascript"></script>
	<script src="<?=base_url()?>assets/layouts/global/scripts/quick-nav.min.js"
		type="text/javascript"></script>
	<!-- END THEME LAYOUT SCRIPTS -->

</body>

</html>