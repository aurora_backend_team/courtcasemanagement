<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<?php
session_start ();
$this->load->view ( 'localization/lang' );
$this->load->view ( 'localization/txt' );
defineLocale ();
defineStrings ();
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>

<script type="text/javascript">
	
function checkValid() {
	var contract_number= document.forms["myForm"]["contract_number"].value;
	var client_id_number= document.forms["myForm"]["client_id_number"].value;
	var client_type= document.forms["myForm"]["client_type"].value;
	var number_of_late_instalments= document.forms["myForm"]["number_of_late_instalments"].value;
	var due_amount= document.forms["myForm"]["due_amount"].value;
	var remaining_amount= document.forms["myForm"]["remaining_amount"].value;
	
	if (contract_number== "" || contract_number<= 0) {
		$("#contract_number_tooltip").css("display","block");
		$("#contract_number_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#contract_number_tooltip").css("display","none");
		$("#contract_number_tooltip").css("visibility","none");
	}
	if (client_id_number== "" || client_id_number<= 0) {
		$("#client_id_number_tooltip").css("display","block");
		$("#client_id_number_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#client_id_number_tooltip").css("display","none");
		$("#client_id_number_tooltip").css("visibility","none");
	}
	if (client_type== "" ) {
		$("#client_type_tooltip").css("display","block");
		$("#client_type_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#client_type_tooltip").css("display","none");
		$("#client_type_tooltip").css("visibility","none");
	}
	if (number_of_late_instalments== "" || number_of_late_instalments<= 0) {
		$("#number_of_late_instalments_tooltip").css("display","block");
		$("#number_of_late_instalments_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#number_of_late_instalments_tooltip").css("display","none");
		$("#number_of_late_instalments_tooltip").css("visibility","none");
	}
	if (due_amount== "" || due_amount<= 0) {
		$("#due_amount_tooltip").css("display","block");
		$("#due_amount_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#due_amount_tooltip").css("display","none");
		$("#due_amount_tooltip").css("visibility","none");
	}
	if (remaining_amount== "" || remaining_amount<= 0) {
		$("#remaining_amount_tooltip").css("display","block");
		$("#remaining_amount_tooltip").css("visibility","visible");
		return false;
	} else{
		$("#remaining_amount_tooltip").css("display","none");
		$("#remaining_amount_tooltip").css("visibility","none");
	}


	
}

</script>
<meta charset="utf-8" />
<title><?php echo TITLE; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin RTL Theme #3 for dashboard & statistics"
	name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"
	rel="stylesheet" type="text/css" />
<link
	href="../assets/global/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="../assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="../assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="../assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link
	href="../assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css"
	rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/morris/morris.css" rel="stylesheet"
	type="text/css" />
<link href="../assets/global/plugins/fullcalendar/fullcalendar.min.css"
	rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/jqvmap/jqvmap/jqvmap.css"
	rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="../assets/global/css/components-rtl.min.css"
	rel="stylesheet" id="style_components" type="text/css" />
<link href="../assets/global/css/plugins-rtl.min.css" rel="stylesheet"
	type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="../assets/layouts/layout3/css/layout-rtl.min.css"
	rel="stylesheet" type="text/css" />
<link href="../assets/layouts/layout3/css/themes/default-rtl.min.css"
	rel="stylesheet" type="text/css" id="style_color" />
<link href="../assets/layouts/layout3/css/custom-rtl.min.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
<style>
:


:-webkit-datetime-edit-year-field


:not

 

(
[
aria-valuenow
]

 

),
:


:-webkit-datetime-edit-month-field


:not


	

(
[
aria-valuenow
]

 

),
:


:-webkit-datetime-edit-day-field


:not

 

(
[
aria-valuenow
]


	

)
{
color


:

 

transparent


;
}
</style>

</head>
<!-- END HEAD -->
<body class="page-container-bg-solid">
	<div class="page-wrapper">
<?php
if ($user_type_code == "ADMIN") {
	$this->load->view ( 'office_admin/office_admin_header' );
} else {
	$this->load->view ( 'secretary_lawyer/secretary_header' );
}
?>
				<div class="page-wrapper-row full-height">
			<div class="page-wrapper-middle">
				<!-- BEGIN CONTAINER -->
				<div class="page-container">
					<!-- BEGIN CONTENT -->
					<div class="page-content-wrapper">
						<!-- BEGIN CONTENT BODY -->
						<!-- BEGIN PAGE HEAD-->
						<div class="page-head">
							<div class="container">
								<div class="col-md-10">
									<div class="portlet box green">
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-gift"></i>
												<?php
													echo ADD_RECORD;
												?>
											</div>
											<div class="tools">
												<a href="javascript:;" class="collapse"> </a> <a
													href="#portlet-config" data-toggle="modal" class="config">
												</a> <a href="javascript:;" class="reload"> </a>
											</div>
										</div>
										<div class="portlet-body form">
											<!-- BEGIN FORM-->
											<form id="myForm" role="form" method="post"
												onsubmit="javascript:return checkValid();"
												action="case_management/add_case/"
												class="form-horizontal">
												<div class="form-body">
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo CUSTOMER; ?><span class="required"> * </span></label>
														<div class="col-md-4">
															<select class="form-control input-circle"
																name="customer_id" id="customer_id"
																class="form-control">
					  	<?php
								if ($available_customers) {
									for($i = 0; $i < count ( $available_customers); $i ++) {
										$customer_id_s = $available_customers[$i] ["user_id"];
										$customer_name = $available_customers [$i] ["user_name"];
										?>
							<option value="<?=$customer_id_s?>"
																	<?php if ($customer_id_s == $customer_id){?>
																	selected="selected" <?php }?>> <?=$customer_name ?></option>
						<?php
									}
								} else {
									?>
							<option value="" selected></option>
						<?php
								}
								?>		
					</select>
														</div>
													</div>

													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo CONTRACT_NUMBER; ?><span class="required"> * </span></label>
														<div class="col-md-7">
															<input type="text" class="form-control input-circle"
																name="contract_number"
																id="contract_number"
																value="<?php echo $contract_number?>"onfocus="javascript:removeTooltip('contract_number_tooltip');">
															<span id="contract_number_tooltip" class="tooltiptext"
																style="display: none; color: red"><?php echo NUMBER_INVALID; ?></span>


														</div>
													</div>

													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo CLIENT_ID_NUMBER; ?><span class="required"> * </span></label>
														<div class="col-md-7">
															<input type="number" class="form-control input-circle"
																name="client_id_number"
																id="client_id_number"
																value="<?php echo $client_id_number?>"
																onfocus="javascript:removeTooltip('client_id_number_tooltip');">
															<span id="client_id_number_tooltip" class="tooltiptext"
																style="display: none; color: red"><?php echo NUMBER_INVALID; ?></span>

														</div>
													</div>

													<div class="form-group">
														<label class="col-md-3 control-label"> <?php echo CLIENT_TYPE; ?> <span class="required"> * </span></label>

														<div class="col-md-4">
															<input type="text" class="form-control input-circle"
																
																id="client_type"
																
																onfocus="javascript:removeTooltip('user_name_tooltip');">
															<span id="client_type_tooltip" class="tooltiptext"
																style="display: none; color: red";><?php echo FILL_THIS_FIELD; ?></span>
														</div>
													</div>

													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo NUMBER_OF_LATE_INSTALMENTS; ?> <span class="required"> * </span></label>
														<div class="col-md-4">
															<input type="number" class="form-control input-circle"
																name="number_of_late_instalments"
																id="number_of_late_instalments"
																value="<?php echo $number_of_late_instalments?>"
																id="number_of_late_instalments"
																onfocus="javascript:removeTooltip('number_of_late_instalments_tooltip');">
															<span id="number_of_late_instalments_tooltip"
																class="tooltiptext" style="display: none; color: red"><?php echo NUMBER_INVALID; ?></span>
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo DUE_AMOUNT; ?><span class="required"> * </span> </label>
														<div class="col-md-4">
															<input type="number" class="form-control input-circle"
																value="<?php echo $due_amount?>"
																id="due_amount"
																name="due_amount"
																onfocus="javascript:removeTooltip('due_amount_tooltip');">
															<span id="due_amount_tooltip" class="tooltiptext"
																style="display: none; color: red"><?php echo NUMBER_INVALID; ?></span>
														</div>
													</div>

													<div class="form-group">
														<label class="col-md-3 control-label"><?php echo REMAINING_AMOUNT; ?><span class="required"> * </span></label>
														<div class="col-md-4">
															<input type="number" class="form-control input-circle"
																name="remaining_amount"
																id="remaining_amount"
																value="<?php echo $remaining_amount?>"
																onfocus="javascript:removeTooltip('remaining_amount_tooltip');">
															<span id="remaining_amount_tooltip" class="tooltiptext"
																style="display: none; color: red"><?php echo NUMBER_INVALID; ?></span>
														</div>
													</div>


			
													<div class="form-actions">
														<div class="row">
															<div class="col-md-offset-3 col-md-9">
																<button type="submit" class="btn btn-circle green"><?php
																	echo INSERT;
																
																?></button>
																<button type="button"
																	class="btn btn-circle grey-salsa btn-outline">
																	<a
																		href="<?=site_url('lawyer_office_panel/view_customers')?>"><?php echo CANCEL; ?></a>
																</button>
															</div>
														</div>
													</div>
											
											</form>
											<!-- END FORM-->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
        <?php $this->load->view('utils/footer');?>
	<!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<script src="../assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
	<!-- BEGIN CORE PLUGINS -->
	<script src="../assets/global/plugins/jquery.min.js"
		type="text/javascript"></script>
	<script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="../assets/global/plugins/js.cookie.min.js"
		type="text/javascript"></script>
	<script
		src="../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
		type="text/javascript"></script>
	<script src="../assets/global/plugins/jquery.blockui.min.js"
		type="text/javascript"></script>
	<script
		src="../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="../assets/global/plugins/moment.min.js"
		type="text/javascript"></script>
	<script
		src="../assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js"
		type="text/javascript"></script>
	<script src="../assets/global/plugins/morris/morris.min.js"
		type="text/javascript"></script>
	<script src="../assets/global/plugins/morris/raphael-min.js"
		type="text/javascript"></script>
	<script
		src="../assets/global/plugins/counterup/jquery.waypoints.min.js"
		type="text/javascript"></script>
	<script
		src="../assets/global/plugins/counterup/jquery.counterup.min.js"
		type="text/javascript"></script>
	<script src="../assets/global/plugins/fullcalendar/fullcalendar.min.js"
		type="text/javascript"></script>
	<script src="../assets/global/plugins/flot/jquery.flot.min.js"
		type="text/javascript"></script>
	<script src="../assets/global/plugins/flot/jquery.flot.resize.min.js"
		type="text/javascript"></script>
	<script
		src="../assets/global/plugins/flot/jquery.flot.categories.min.js"
		type="text/javascript"></script>
	<script
		src="../assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js"
		type="text/javascript"></script>
	<script src="../assets/global/plugins/jquery.sparkline.min.js"
		type="text/javascript"></script>
	<script src="../assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js"
		type="text/javascript"></script>
	<script
		src="../assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js"
		type="text/javascript"></script>
	<script
		src="../assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js"
		type="text/javascript"></script>
	<script
		src="../assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js"
		type="text/javascript"></script>
	<script
		src="../assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js"
		type="text/javascript"></script>
	<script
		src="../assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js"
		type="text/javascript"></script>
	<script
		src="../assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="../assets/global/scripts/app.min.js"
		type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="../assets/pages/scripts/dashboard.min.js"
		type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script src="../assets/layouts/layout3/scripts/layout.min.js"
		type="text/javascript"></script>
	<script src="../assets/layouts/layout3/scripts/demo.min.js"
		type="text/javascript"></script>
	<script src="../assets/layouts/global/scripts/quick-sidebar.min.js"
		type="text/javascript"></script>
	<script src="../assets/layouts/global/scripts/quick-nav.min.js"
		type="text/javascript"></script>
	<!-- END THEME LAYOUT SCRIPTS -->

</body>

</html>